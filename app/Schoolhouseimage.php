<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schoolhouseimage extends Model
{
    protected $table = 'user_schoolhouse_images';

    protected $guarded = ['id'];

    public $timestamps = false;
    
    public function user() {
        return $this->belongsTo(User::class);
    }
}
