<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistanceHomeWorkRequestResponseLog extends Model {

    protected $table = 'distancehomework_req_res_log';

}
