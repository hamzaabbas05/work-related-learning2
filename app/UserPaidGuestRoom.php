<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaidGuestRoom extends Model {

    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function realRoomType() {
        return $this->hasOne(RealRoomTypes::class, 'id', "room_type_id");
    }

}
