<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'language';

    protected $guarded = ['id'];

    public function getLanguage($isArray = false)
    {
        $languageObj = Language::where('status', 1)->get();
        if ($isArray) {
            return $this->convertToArray($languageObj);
        }
        return $languageObj;
    }

    public function convertToArray($processObj)
    {
        $returnArr[null] = 'Select';
        if (is_object($processObj)) {
            foreach ($processObj as $process) {
                $returnArr[$process->id] = $process->language_code;
            }
            return $returnArr;
        }
        return $returnArr;
    }
}
