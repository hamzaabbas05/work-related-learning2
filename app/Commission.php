<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Constants\AppConstants;

class Commission extends Model
{
    protected $table = 'commission';

    protected $guarded = ['id'];
  
    public static function getById($id, $isActive = false)
    {
        if ($isActive) {
            return Commission::where('id', $id)->where('status', AppConstants::STATUS_ACTIVE)->first();
        }
        return Commission::where('id', $id)->first();
    }

    public static function calculateCommission($commissionObj, $totalAmt)
    {
        if ($commissionObj->type == AppConstants::COMMISSION_TYPE_PERCENTAGE) {
            return $totalAmt * parseInt($commissionObj->amount)/100;
        }
        return  $commissionObj->amount;
    }
}
