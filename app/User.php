<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

    use Authenticatable,
        Authorizable,
        CanResetPassword,
        EntrustUserTrait {
        EntrustUserTrait::can insteadof Authorizable;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /* protected $fillable = [
      'name', 'lastname',  'email', 'password', 'dob'
      ]; */

    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function wishlists() {
        return $this->hasMany(Wishlist::class);
    }

    public function properties() {
        return $this->hasMany(Property::class);
    }
    
    public function paid_service() {
        return $this->hasOne(UserPaidService::class, 'user_id');
    }
    
    public function user_referral() {
        return $this->hasOne(UserReferral::class, 'user_id');
    }
    
    public function user_properties() {
        return $this->hasMany(Property::class)->select("id", "user_id", "title", "exptitle");
    }
    
    public function user_paid_property() {
        return $this->hasOne(Property::class)->where("property_status", "paid")->where("property_type", "0")->where("status", "1");
//        return $this->hasOne(Property::class)->where("property_status", "paid")->where("status", "1");
    }
    
    public function user_paid_property_pending() {
        return $this->hasOne(Property::class)->where("property_status", "paid")->where("property_type", "0")->where("status", "0");
//        return $this->hasOne(Property::class)->where("property_status", "paid")->where("status", "1");
    }

    public function reviews() {
        return $this->hasMany(Review::class, 'reviewed_id', 'id');
    }
    
    public function receivedMessage() {
        return $this->hasMany(MedMessage::class, 'receiverId');
    }

    public function sentMessage() {
        return $this->hasMany(MedMessage::class, 'senderId');
    }

    public function school() {
        return $this->hasOne(School::class, 'id', 'school_name');
    }

    public function school_info() {
        return $this->belongsTo(School::class, 'school_name')->select("id", "name");
    }

    public function roomtype() {
        return $this->belongsTo(RoomType::class, 'orghr');
    }
    
    public function voucher_code() {
        return $this->belongsTo(Voucher::class, 'voucher_id');
    }
    
    public function vouchercredit_code() {
        return $this->belongsTo(VoucherCredit::class, 'vouchercredit_id');
    }
    
    public function userWallet() {
        return $this->hasMany(UserWallet::class);
    }

    public function tutorrelation() {
        return $this->belongsTo(Tutor::class, 'tutorrelation');
    }
    
    public function user_tutorrelation() {
        return $this->belongsTo(Tutor::class, 'tutorrelation');
    }

    public function user_booking() {
        return $this->hasMany(RentalsEnquiry::class, 'user_id')->orderBy('checkin', 'DESC')->with("property_detail");
    }
    
    public function user_email() {
        return $this->hasMany(EmailSent::class, 'user_id');
    }
    
    public function paidServicePropertyImage() {
        return $this->hasMany(Schoolhouseimage::class, 'user_id');
    }
    
    public function paidServiceFamilyImage() {
        return $this->hasMany(Schoolfamily::class, 'user_id');
    }
    
    public function paidServiceFamilyRelation() {
        return $this->hasMany(UserPaidRelation::class, 'user_id');
    }
    
    public function user_guest_rooms() {
        return $this->hasMany(UserPaidGuestRoom::class, 'user_id');
    }
    
    public function group_info() {
        return $this->belongsTo(SecondarySchoolGroups::class, 'secondary_group_id');
    }
    
    public function bank_detail() {
        return $this->hasOne(UserBankDetail::class, 'user_id');
    }
    
    public function user_extrainfo() {
        return $this->hasOne(UserExtraInfo::class, 'user_id');
    }
    
    public function user_verification() {
        return $this->hasOne(UserVerification::class, 'user_id');
    }

    /* public function roomtype()
      {
      return $this->belongsTo(RoomType::class, 'orghr');
      } */

    /* public function tutorrelation()
      {
      return $this->belongsTo(Tutor::class, 'tutorrelation');
      } */
}
