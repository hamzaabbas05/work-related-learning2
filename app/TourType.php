<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourType extends Model {

    public function property() {
        return $this->hasMany(Property::class);
    }

}
