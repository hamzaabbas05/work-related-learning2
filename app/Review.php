<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'fc_review';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function property()
    {
    	return $this->belongsTo(Property::class, 'prd_id', 'id');
    }

    public function reviewer()
    {
        return $this->belongsTo(User::class, 'reviewer_id', 'id');
    }
    
    public function revieweduser()
    {
    	return $this->belongsTo(User::class, 'reviewed_id', 'id');
    }

    public function enquiry()
    {
        return $this->belongsTo(RentalsEnquiry::class, 'enquiryId', 'id');
    }
}


 