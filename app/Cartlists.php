<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartlists extends Model
{
    //

	protected $guarded = ['id'];
	// protected $table = 'cartlists';
    
    public function user()
    {
    	return $this->belongsTo(User::class,'user_id');
    }
    
    public function properties()
    {
        return $this->belongsToMany(Property::class);
    	// return $this->hasMany(Property::class,'id');
    }
}
