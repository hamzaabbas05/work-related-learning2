<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'fc_payment';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function rentals()
    {
    	return $this->belongsTo(RentalsEnquiry::class, 'EnquiryId', 'id');
    }

    public function property()
    {
        return $this->belongsTo(Property::class, 'product_id', 'id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function host()
    {
    	return $this->belongsTo(User::class, 'sell_id', 'id');
    }
    
}


 