<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeDestination extends Model
{
    protected $table = 'homedestinations';

    protected $guarded = ['id'];

 
}
