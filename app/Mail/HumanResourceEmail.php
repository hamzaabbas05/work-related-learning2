<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HumanResourceEmail extends Mailable {

    use Queueable,
        SerializesModels;
	protected $inputs;
    /**
     * Create a new message instance.
     *$inputs
     * @return void
     */
    public function __construct($inputs) {
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject("Human Resource - You have received a request")->view('emails.humanResourceEmail');
    }

}
