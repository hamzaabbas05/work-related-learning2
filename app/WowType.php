<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WowType extends Model
{
    public function property() {
        return $this->hasMany(Property::class);
    }
}
