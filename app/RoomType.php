<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    protected $table = 'room_type';

    protected $guarded = ['id'];
    
    public function property()
    {
        return $this->hasMany(Property::class);
    }
}
