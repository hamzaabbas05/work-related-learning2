<?php
namespace App\Repositories;

use App\Amenity as Amenity;
use DB;
use App\Services\Constants\AppConstants;

class AmenityRepository
{
    public $validationRule = ['name' => 'required|unique:amenities,name'];

    public function editValidationRules($id)
    {
        return array('name' => 'required|unique:amenities,name,'.$id);
    }

    public function getAll()
    {
        return Amenity::all();
    }

    public function getSelectedTypes($typeArray)
    {
        return Amenity::whereIn('type', $typeArray)->get();
    }


    public function create($formData)
    {
        return Amenity::create($formData);
    }

    public function update($formData, $id)
    {
        return Amenity::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return Amenity::where('id', $id)->first();
    }

    public function getByType($type)
    {
        return Amenity::where('type', $type)->where('status', AppConstants::STATUS_ACTIVE)->get();
    }
}
