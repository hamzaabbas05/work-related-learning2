<?php
namespace App\Repositories;

use App\Language as Language;
use DB;

class LanguageRepository
{
    public $validationRule = ['language_name' => 'required|unique:language,language_name', 'language_code' => 'required|unique:language,language_code'];

    public function editValidationRules($id)
    {
        return array('language_name' => 'required|unique:language,language_name,'.$id,'language_code' => 'required|unique:language,language_code,'.$id);
    }


    public function getLanguage($isArray = false)
    {
        $languageObj = Language::where('status', 1)->get();
        if ($isArray) {
            return $this->convertToArray($languageObj);
        }
        return $languageObj;
    }

    public function convertToArray($processObj)
    {
        $returnArr[null] = 'Select';
        if (is_object($processObj)) {
            foreach ($processObj as $process) {
                $returnArr[$process->id] = $process->language_code;
            }
            return $returnArr;
        }
        return $returnArr;
    }

    public function getAll()
    {
        return Language::all();
    }

    public function create($formData)
    {
        return Language::create($formData);
    }

    public function update($formData, $id)
    {
        return Language::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return Language::where('id', $id)->first();
    }
     
}
