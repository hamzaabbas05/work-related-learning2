<?php
namespace App\Repositories;

use App\HomeDestination as HomeDestination;
use App\Services\Constants\AppConstants;
use DB;

class HomeDestinationRepository
{
    public $validationRule = ['name' => 'required|unique:homedestinations,name', 'display_name' => 'required|unique:homedestinations,display_name', 'img_name' => 'mimes:jpeg,bmp,png|required'];

    public function editValidationRules($id)
    {
        return array('name' => 'required|unique:homedestinations,name,'.$id, 'display_name' => 'required|unique:homedestinations,display_name,'.$id);
    }

    public function getAll($isActive = false)
    {
        if ($isActive) {
          return HomeDestination::where('status', AppConstants::STATUS_ACTIVE)->orderByRaw("RAND()")->take(7)->get();
        }
        return HomeDestination::all();
    }

    public function create($formData)
    {
        return HomeDestination::create($formData);
    }

    public function update($formData, $id)
    {
        return HomeDestination::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return HomeDestination::where('id', $id)->first();
    }
}
