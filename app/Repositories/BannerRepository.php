<?php
namespace App\Repositories;

use App\Banner as Banner;
use DB;
use App\Services\Constants\AppConstants;

class BannerRepository
{
    public $validationRule = ['banner_img' => 'mimes:jpeg,bmp,png|required'];

    public function getAll()
    {
        return Banner::all();
    }

    public function create($formData)
    {
        return Banner::create($formData);
    }

    public function update($formData, $id)
    {
        return Banner::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return Banner::where('id', $id)->first();
    }

    public function getByType($type)
    {
        if ($type == AppConstants::HOME_BANNER) {
            return Banner::where('banner_type', $type)->where('status', AppConstants::STATUS_ACTIVE)->first();
        }
        return Banner::where('banner_type', $type)->where('status', AppConstants::STATUS_ACTIVE)->get();
    }
}
