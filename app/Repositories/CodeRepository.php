<?php
namespace App\Repositories;

use App\Code as Code;
use DB;

class CodeRepository
{
    public function getAll()
    {
        return Code::all();
    }

   /* public function getByCodeType($code)
    {
        return Code::where('Code', $code)->get();
    }*/

    public function getByCodeType($isArray = false, $codeType)
    {
        $codeObj = Code::where('CodeType', $codeType)->get();
        if ($isArray) {
            return $this->convertToArray($codeObj);
        }
        return $codeObj;
    }

    public function convertToArray($processObj)
    {
        $returnArr[null] = 'Select';
        if (is_object($processObj)) {
            foreach ($processObj as $process) {
                $returnArr[$process->Code] = $process->Desc;
            }
            return $returnArr;
        }
        return $returnArr;
    }
}
