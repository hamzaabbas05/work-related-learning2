<?php
namespace App\Repositories;

use App\User as User;
use DB;

class UserRepository
{
    public function editValidationRules($id)
    {
        return array('name' => 'required','dob' => 'required','gender' => 'required','about_yourself' => 'required');
    }
    
    public function createUser($postData)
    {
        return User::create($postData);
    }

    public function findUser($id)
    {
        return User::find($id);
    }

    public function getUserRoles($id)
    {
        return User::with('roles')->find($id);
    }
    public function getAll()
    {
        return User::orderBy('created_at', 'desc')->get();
    }

    public function getCurrentUserLoginId()
    {
        return \Auth::user()->id;
    }

    public function getLoginUser()
    {
        return User::where('id', $this->getCurrentUserLoginId())->first();
    }

     public function getById($id)
    {
        return User::where('id', $id)->first();
    }

    public function updateUser($postData)
    {
        return User::where('id', $this->getCurrentUserLoginId())->update($postData);
    }


    public function update($id, $postData)
    {
        return User::where('id', $id)->update($postData);
    }

    public function isAdmin()
    {
        if (\Auth::user()->hasRole('admin')) {
            return true;
        }
        return false;
    }
}
