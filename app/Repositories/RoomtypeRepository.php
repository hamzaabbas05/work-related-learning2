<?php
namespace App\Repositories;

use App\RoomType as RoomType;
use App\Services\Constants\AppConstants;
use DB;

class RoomtypeRepository
{
    public $validationRule = ['name' => 'required|unique:room_type,name'];

    public function editValidationRules($id)
    {
        return array('name' => 'required|unique:room_type,name,'.$id);
    }

    public function getAll($isActive = false)
    {
        if ($isActive) {
          return RoomType::where('status', AppConstants::STATUS_ACTIVE)->get();
        }
        return RoomType::all();
    }

    public function create($formData)
    {
        return RoomType::create($formData);
    }

    public function update($formData, $id)
    {
        return RoomType::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return RoomType::where('id', $id)->first();
    }
}
