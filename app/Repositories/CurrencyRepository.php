<?php
namespace App\Repositories;

use App\Currency as Currency;
use DB;

class CurrencyRepository
{
    public $validationRule = ['currency_name' => 'required|unique:currency,currency_name', 'currency_code' => 'required|unique:currency,currency_code',
        'currency_symbol' => 'required',
        'currency_rate' => 'required'
        ];

    public function editValidationRules($id)
    {
        return array('currency_name' => 'required|unique:currency,currency_name,'.$id,'currency_code' => 'required|unique:currency,currency_code,'.$id,'currency_symbol' => 'required',
        'currency_rate' => 'required');
    }

    public function getCurrency($isArray = false)
    {
        $currencyObj = Currency::where('status', 1)->get();
        if ($isArray) {
            return $this->convertToArray($currencyObj);
        }
        return $currencyObj;
    }

    public function convertToArray($processObj)
    {
        $returnArr[null] = 'Select';
        if (is_object($processObj)) {
            foreach ($processObj as $process) {
                $returnArr[$process->id] = $process->currency_code;
            }
            return $returnArr;
        }
        return $returnArr;
    }

    public function getAll()
    {
        return Currency::all();
    }

    public function create($formData)
    {
        return Currency::create($formData);
    }

    public function update($formData, $id)
    {
        return Currency::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return Currency::where('id', $id)->first();
    }
}
