<?php
namespace App\Repositories;

use App\Settings as Settings;
use DB;

class SettingsRepository
{
	public $generalValidationRule = ['site_title' => 'required', 'site_slogan' => 'required', 'site_meta_keywords' => 'required', 'site_meta_description' => 'required', 'language_id' => 'required', 'currency_id' => 'required'];

	public $emailValidationRule = ['superadmin_email' => 'email|required', 'support_email' => 'email|required', 'noreply_email' => 'email|required', 'contact_email' => 'email|required', 'contact_skype' => 'required', 'open_timings' => 'required'];

	public $socialMediaValidationRule = ['facebook_url' => 'url|required', 'googleplus_url' => 'url|required', 'twitter_url' => 'url|required'];

	public $appValidationRule = ['fb_app_id' => 'required', 'fb_secret_key' => 'required', 'gplus_app_id' => 'required', 'gplus_secret_key' => 'required'];

	public $paypalValidationRule = ['paypal_email' => 'email|required', 'paypal_client_id' => 'required', 'paypal_secret_key' => 'required'];

	public $attributeValidationRule = ['bed_no' => 'required', 'bed_room_no' => 'required', 'accomodates_no' => 'required', 'bath_no' => 'required'];

	

	public function settingsInfo()
    {
        return Settings::all()->first();
    }

    public function update($formData)
    {
        return Settings::where('id', 1)->update($formData);
    }
}
