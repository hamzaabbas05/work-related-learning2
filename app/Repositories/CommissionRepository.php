<?php
namespace App\Repositories;

use App\Commission as Commission;
use DB;
use App\Services\Constants\AppConstants;

class CommissionRepository
{
    public $validationRule = ['type' => 'required',
        'amount' => 'required',
        'name' => 'required|unique:commission,name'
        ];

    public function editValidationRules($id)
    {
        return array('name' => 'required|unique:commission,name,'.$id,'type' => 'required',
        'amount' => 'required',);
    }
    
    public function getAll($isActive = false)
    {
        if ($isActive) {
          return Commission::where('status', AppConstants::STATUS_ACTIVE)->get();
        }
        return Commission::all();
        
    }

    public function getArray()
    {
        $Obj = Commission::where('status', AppConstants::STATUS_ACTIVE)->get();
        return $this->convertToArray($Obj);
    }

    public function convertToArray($processObj)
    {
        $returnArr[null] = 'Select';
        if (is_object($processObj)) {
            foreach ($processObj as $process) {
                $returnArr[$process->id] = $process->name;
            }
            return $returnArr;
        }
        return $returnArr;
    }

    public function create($formData)
    {
        return Commission::create($formData);
    }

    public function update($formData, $id)
    {
        return Commission::where('id', $id)->update($formData);
    }

    public function getById($id, $isActive = false)
    {
        if ($isActive) {
            return Commission::where('id', $id)->where('status', AppConstants::STATUS_ACTIVE)->first();
        }
        return Commission::where('id', $id)->first();
    }

    public function calculateCommission($commissionObj, $totalAmt)
    {
        if ($commissionObj->type == AppConstants::COMMISSION_TYPE_PERCENTAGE) {
            return $totalAmt * parseInt($commissionObj->amount)/100;
        }
        return  $commissionObj->amount;
    }
}
