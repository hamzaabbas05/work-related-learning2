<?php
namespace App\Repositories;

use App\Property as Property;
use App\Services\Constants\AppConstants;
use DB;

class PropertyRepository
{
    
    public function create($formData)
    {
        return Property::create($formData);
    }

    public function update($formData, $id)
    {
        return Property::where('id', $id)->update($formData);
    }
	
	public function delete_prop($id)
    {
        return Property::where('id', $id)->delete();
    }

    public function getById($id)
    {
        return Property::where('id', $id)->first();
    }

    public function getByIdActive($id)
    {
        return Property::with("user")->where('id', $id)->where('status', 1)->first();
    }

    public function getCurrentObject($id)
    {
        return Property::find($id);
    }

    public function getAll($isActive = false) {
        if ($isActive) {
            return Property::OrderByDate()->where('status', 1)->get();
        }
         return Property::OrderByDate()->get();
    }

    public function getPropertyForUser($userId, $status) {

        return Property::where('status', $status)->where('user_id', $userId)->OrderByDate()->get();
    }

    public function updateStatusByUserId($userId, $data)
    {
        return Property::where('user_id', $userId)->update($data);
    }


    public function updateaddressinproperty($uid, $formdata)
    {
        return Property::where('user_id', $uid)->where('addr_work_org_same', 1)->update($formdata);
    }


}
