<?php
namespace App\Repositories;

use App\PropertyType as PropertyType;
use App\Services\Constants\AppConstants;
use DB;

class PropertytypeRepository
{
    public $validationRule = ['name' => 'required|unique:property_type,name'];

    public function editValidationRules($id)
    {
        return array('name' => 'required|unique:property_type,name,'.$id);
    }

    public function getAll($isActive = false)
    {
        if ($isActive) {
          return PropertyType::where('status', AppConstants::STATUS_ACTIVE)->get();
        }
        return PropertyType::all();
    }

    public function create($formData)
    {
        return PropertyType::create($formData);
    }

    public function update($formData, $id)
    {
        return PropertyType::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return PropertyType::where('id', $id)->first();
    }
    /*To get the listing : property type: unpaid etc.*/
    public function getByType($type)
    {
        return PropertyType::where('listing_type', $type)->get();
    }
}
