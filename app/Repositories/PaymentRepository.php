<?php
namespace App\Repositories;

use App\Payment as Payment;
use DB;
use App\Services\Constants\AppConstants;

class PaymentRepository
{
    public function getAll()
    {
        return Payment::orderBy('modified', 'desc')->get();
    }


    public function getByStatus($status)
    {
    	if ($status == "all") {
    		return Payment::orderBy('modified', 'desc')->get();
    	}
    	return Payment::where('status', $status)->orderBy('modified', 'desc')->get();
    }


    public function getTransactionsForUser($userId, $status)
    {
        return Payment::where('sell_id', $userId)->where('status', $status)->orderBy('modified', 'desc')->get();
    }
}
