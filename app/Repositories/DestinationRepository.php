<?php
namespace App\Repositories;

use App\Destination as Destination;
use DB;

class DestinationRepository
{
    public $validationRule = ['name' => 'required|unique:destinations,name', 'display_name' => 'required|unique:destinations,display_name'];

    public function editValidationRules($id)
    {
        return array('name' => 'required|unique:destinations,name,'.$id, 'display_name' => 'required|unique:destinations,display_name,'.$id);
    }

    public function getAll()
    {
        return Destination::all();
    }

    public function create($formData)
    {
        return Destination::create($formData);
    }

    public function update($formData, $id)
    {
        return Destination::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return Destination::where('id', $id)->first();
    }
}
