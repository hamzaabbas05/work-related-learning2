<?php
namespace App\Repositories;

use App\CommissionTrack as CommissionTrack;
use DB;
use App\Services\Constants\AppConstants;

class CommissionTrackingRepository
{
    public function getAll()
    {
        return CommissionTrack::all();
    }
}
