<?php
namespace App\Repositories;

use App\RentalsEnquiry as RentalsEnquiry;
use DB;
use App\Services\Constants\AppConstants;

class ReservationRepository
{
    public function getAll()
    {
        return RentalsEnquiry::orderBy('checkin', 'ASC')->get();
    }

    public function getByEnquiryId($id)
    {
        return RentalsEnquiry::where('id', $id)->first();
    }


    public function getByStatus($status)
    {
    //return RentalsEnquiry::orderBy('dateAdded', 'desc')->get();
    	if ($status == "all") {
    		return RentalsEnquiry::orderBy('checkin', 'ASC')->get();
    	}

        if($status == 'Pending'){
            $last_seven_days = date("Y-m-d",strtotime(" -7 days"));
            $today_date = date("Y-m-d");
           
            $not_accepted_unpaid =    RentalsEnquiry::select("fc_rentalsenquiry.*")
                        ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                        ->where("fc_rentalsenquiry.approval","=","Pending")
                        ->whereRaw("(property.property_status = 'unpaid' OR property.property_status = 'unpaidflash')")
                        ->where("fc_rentalsenquiry.cancelled","=","No")
                        ->where(function($query) use ($today_date,$last_seven_days) {
                                $query->where('checkout', '>=', $today_date); 
                            })->orderBy('fc_rentalsenquiry.checkin', 'ASC')->get(); 
             $not_accepted_paid =    RentalsEnquiry::select("fc_rentalsenquiry.*")
                        ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                        ->where("fc_rentalsenquiry.approval","=","Pending")
                        ->whereRaw("property.property_status = 'paid'")
                        ->where("fc_rentalsenquiry.cancelled","=","No")
                        ->where(function($query) use ($today_date,$last_seven_days) {
                                $query->where('checkout', '>=', $today_date); 
                            })->orderBy('fc_rentalsenquiry.checkin', 'ASC')->get(); 
            $user_paid_accepted = array();
            $user_uppaid_accepted = array();
            $paid_not_accepted = array();
            $unpaid_not_accepted = array();
            foreach ($not_accepted_unpaid as $pending_request) {
                
                $user_id = $pending_request->user_id;
                
                if(in_array($user_id, $user_paid_accepted)){
                    continue;
                }
               
                $unpaid_accepted =  RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $user_id)
                    ->whereRaw("(property.property_status = 'unpaid' OR property.property_status = 'unpaidflash')")
                    ->where("fc_rentalsenquiry.approval","=","Accept")
                    ->where(function($query) use ($today_date) {
                            $query->where('checkout', '>=', $today_date);
                        })->get()->count(); 
                
                if($unpaid_accepted > 0){
                    $user_unpaid_accepted[] = $user_id;
                }else{
                    $unpaid_not_accepted[] = $pending_request;
                }
                
            }

             foreach ($not_accepted_paid as $pending_request) {
                
                $user_id = $pending_request->user_id;
                
                if(in_array($user_id, $user_unpaid_accepted)){
                    continue;
                }
               
               
                $paid_accepted =  RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $user_id)
                    ->whereRaw("property.property_status = 'paid'")
                    ->where("fc_rentalsenquiry.approval","=","Accept")
                    ->where(function($query) use ($today_date) {
                            $query->where('checkout', '>=', $today_date);
                        })->get()->count();
               
                if($paid_accepted > 0){
                    $user_paid_accepted[] = $user_id;
                }else{
                    $paid_not_accepted[] = $pending_request;
                }
                
                
            }


            $requests =  array_merge($paid_not_accepted,$unpaid_not_accepted); 

            usort($requests, function($a,$b){
                $t1 = strtotime($a['checkin']);
                $t2 = strtotime($b['checkin']);
                return $t1-$t2;
            });
            return $requests;
        }
        if($status == 'New'){
            $current_date = date("Y-m-d");
            return RentalsEnquiry::where("checkin" ,">=", $current_date )->orderBy('checkin', 'ASC')->get();
        }
        if($status == 'Accept'){
             return RentalsEnquiry::where('approval', $status)->orderBy('checkin', 'ASC')->get();
        }
        
    }
}