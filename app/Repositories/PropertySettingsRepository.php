<?php
namespace App\Repositories;

use App\PropertySettings as PropertySettings;
use DB;

class PropertySettingsRepository
{
    
     
    public function getAll()
    {
        return PropertySettings::all();
    }

    public function update($formData, $id)
    {
        return PropertySettings::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return PropertySettings::where('id', $id)->first();
    }
}
