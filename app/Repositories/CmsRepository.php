<?php
namespace App\Repositories;

use App\Cms as Cms;
use DB;

class CmsRepository
{
    public $validationRule = ['page_name' => 'required|unique:cms,page_name', 'page_desc' => 'required'];

    public function editValidationRules($id)
    {
        return array('page_desc' => 'required', 'page_name' => 'required|unique:cms,page_name,'.$id);
    }

    public function getAll()
    {
        return Cms::all();
    }

    public function create($formData)
    {
        return Cms::create($formData);
    }

    public function update($formData, $id)
    {
        return Cms::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return Cms::where('id', $id)->first();
    }
}
