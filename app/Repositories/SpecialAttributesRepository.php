<?php
namespace App\Repositories;

use App\SpecialAttribute as SpecialAttribute;
use DB;
use App\Services\Constants\AppConstants;

class SpecialAttributesRepository
{
    public $validationRule = ['name' => 'required|unique:propertyspecialattributes,name'];

    public function editValidationRules($id)
    {
        return array('name' => 'required|unique:propertyspecialattributes,name,'.$id);
    }

    public function getAll()
    {
        return SpecialAttribute::all();
    }

    public function create($formData)
    {
        return SpecialAttribute::create($formData);
    }

    public function update($formData, $id)
    {
        return SpecialAttribute::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return SpecialAttribute::where('id', $id)->first();
    }

    public function getByType($type)
    {
        return SpecialAttribute::where('type', $type)->where('status', AppConstants::STATUS_ACTIVE)->get();
    }

}
