<?php
namespace App\Repositories;

use App\School as School;
use App\Services\Constants\AppConstants;
use DB;

class SchoolRepository
{
    public $validationRule = ['name' => 'required|unique:schools,name', 'tutor_email' => 'required|email'];

   

    public function editValidationRules($id)
    {
        return array('tutor_email' => 'required|email', 'name' => 'required|unique:schools,name,'.$id);
    }

    public function getAll($isActive = false)
    {
        if ($isActive) {
          return School::where('status', AppConstants::STATUS_ACTIVE)->get();
        }
        return School::all();
    }

    public function create($formData)
    {
        return School::create($formData);
    }

    public function update($formData, $id)
    {
        return School::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return School::where('id', $id)->first();
    }
}
