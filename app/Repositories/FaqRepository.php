<?php
namespace App\Repositories;

use App\Faq as Faq;
use DB;

class FaqRepository
{
    public $validationRule = [
        'question' => 'required',
        'answer' => 'required'
        ];

    public function editValidationRules($id)
    {
        return array('question' => 'required',
        'answer' => 'required');
    }

    public function getAll($isActive = false) {
        if ($isActive) {
            return Faq::where('status', 1)->get();
        }
         return Faq::all();
    }
     
    public function create($formData)
    {
        return Faq::create($formData);
    }

    public function update($formData, $id)
    {
        return Faq::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return Faq::where('id', $id)->first();
    }

    public function getByType($id)
    {
        return Faq::where('ftype', $id)->where('status', 1)->get();
    }
}
