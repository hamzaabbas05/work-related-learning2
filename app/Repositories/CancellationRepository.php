<?php
namespace App\Repositories;

use App\Cancellation as Cancellation;
use DB;
use App\Services\Constants\AppConstants;

class CancellationRepository
{
    public $validationRule = ['currency_id' => 'required', 'cancellation_type' => 'required',
        'cancellation_charge' => 'required',
        'name' => 'required|unique:cancellationpolicy,name'
        ];

    public function editValidationRules($id)
    {
        return array('name' => 'required|unique:cancellationpolicy,name,'.$id,'currency_id' => 'required', 'cancellation_type' => 'required',
        'cancellation_charge' => 'required',);
    }
    
    public function getAll($isActive = false)
    {
        if ($isActive) {
          return Cancellation::where('status', AppConstants::STATUS_ACTIVE)->get();
        }
        return Cancellation::all();
        
    }

    public function getArray()
    {
        $Obj = Cancellation::where('status', AppConstants::STATUS_ACTIVE)->get();
        return $this->convertToArray($Obj);
    }

    public function convertToArray($processObj)
    {
        $returnArr[null] = 'Select';
        if (is_object($processObj)) {
            foreach ($processObj as $process) {
                $returnArr[$process->id] = $process->name;
            }
            return $returnArr;
        }
        return $returnArr;
    }

    public function create($formData)
    {
        return Cancellation::create($formData);
    }

    public function update($formData, $id)
    {
        return Cancellation::where('id', $id)->update($formData);
    }

    public function getById($id)
    {
        return Cancellation::where('id', $id)->first();
    }
}
