<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currency';

    protected $guarded = ['id'];
    
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getCurrency($isArray = false)
    {
        $currencyObj = Currency::where('status', 1)->orderBy("currency_order", "ASC")->get();
        if ($isArray) {
            return $this->convertToArray($currencyObj);
        }
        return $currencyObj;
    }

    public function convertToArray($processObj)
    {
        $returnArr[null] = 'Select';
        if (is_object($processObj)) {
            foreach ($processObj as $process) {
                $returnArr[$process->id] = $process->currency_code;
            }
            return $returnArr;
        }
        return $returnArr;
    }
}
