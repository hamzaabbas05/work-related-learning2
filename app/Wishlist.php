<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    //

	protected $guarded = ['id'];
	protected $table = 'wishlists';
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function properties()
    {
    	return $this->belongsToMany(Property::class);
    }
}
