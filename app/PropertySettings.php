<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertySettings extends Model
{
    protected $table = 'property_base_attributes';

    protected $guarded = ['id'];

 	public static function propertySettings()
    {
        return PropertySettings::all()->first();
    }
}
