<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferType extends Model {

    public function property() {
        return $this->hasMany(Property::class);
    }

}
