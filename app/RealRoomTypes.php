<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealRoomTypes extends Model
{
    protected $table = 'room_type_really_room_types';
    
    public function userPaidService() {
        return $this->hasOne(UserPaidService::class, 'real_room_type_id');
    }
    
    public function userGuestRoom() {
        return $this->hasOne(UserPaidGuestRoom::class, 'room_type_id');
    }
}
