<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancellation extends Model
{
    protected $table = 'cancellationpolicy';

    protected $guarded = ['id'];
  
}
