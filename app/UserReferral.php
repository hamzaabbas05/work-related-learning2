<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReferral extends Model {

    public function user() {
        return $this->belongsTo(User::class);
    }

}
