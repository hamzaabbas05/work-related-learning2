<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $table = 'evaluation';

    protected $guarded = ['id'];

    public $timestamps = false;

   
}