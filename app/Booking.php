<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
if($_SERVER['REMOTE_ADDR'] == '93.32.170.9' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
// use DB;    
}
class Booking extends Model {

    protected $table = 'bookings';
    protected $guarded = ['id'];
    public $timestamps = false;

    public static function availabilityCheck($newDateStart, $newDateEnd) {
        // return Booking::where('the_date', '>=', $newDateStart)->where('the_date', '<=', $newDateEnd)->groupBy('PropId')->get();
        return Booking::whereDate('the_date', '>=', $newDateStart)->whereDate('the_date', '<=', $newDateEnd)->groupBy('PropId')->get();
    }

    public static function availabilityCheckForProperty($newDateStart, $newDateEnd, $propertyId) {
        // return Booking::where('the_date', '>=', $newDateStart)->where('the_date', '<=', $newDateEnd)->where('PropId', $propertyId)->get();
        return Booking::whereDate('the_date', '>=', $newDateStart)->whereDate('the_date', '<=', $newDateEnd)->where('PropId', $propertyId)->get();
    }

    public static function bookingForProperty($id) {
//        return Booking::where('PropId', $id)->get();
        return Booking::select('bookings.*')
                ->join("fc_rentalsenquiry", "bookings.PropId", "=", "fc_rentalsenquiry.prd_id")
                ->where('bookings.PropId', $id)
                ->where('fc_rentalsenquiry.cancelled', "No")
                ->whereRaw("(bookings.the_date BETWEEN fc_rentalsenquiry.checkin AND fc_rentalsenquiry.checkout)")
                ->get();
    }

     public static function disabledDatesForProperty($propId) {
//        return Booking::where('PropId', $id)->get();

       // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
            // $data1 = \DB::table('fc_rentalsenquiry')->whereRaw('bookings.PropId = fc_rentalsenquiry.prd_id ')->where('fc_rentalsenquiry.prd_id', '=', $propId)->get();
             // return $data1 = Booking::select('bookings.*')
           // return $data = \DB::table('bookings')->where('bookings.PropId', '=', $propId)->whereNotExists( function ($query) use ($propId) {    
          /* return $data = Booking::select('bookings')->where('bookings.PropId', '=', $propId)->whereNotExists( function ($query) use ($propId) {    
                $query->select(\DB::raw(1))->from('fc_rentalsenquiry')->whereRaw('bookings.PropId = fc_rentalsenquiry.prd_id ')->where('fc_rentalsenquiry.prd_id', '=', $propId);
        })->orderBy('id','DESC')->get();*/
             $data1 = Booking::select('bookings.*')
              // $data1 = \DB::table('bookings.*')
                ->join("fc_rentalsenquiry", "bookings.PropId", "=", "fc_rentalsenquiry.prd_id")
                ->where('fc_rentalsenquiry.prd_id', $propId)
                // ->where('fc_rentalsenquiry.cancelled', "No")
                // ->whereRaw("(bookings.the_date BETWEEN fc_rentalsenquiry.checkin AND fc_rentalsenquiry.checkout)")
                ->get();
                // ->toSQL();
                // echo "<pre>";
                // print_r($data1);
                // exit;
                $excludePropId = array();
                if(count($data1) > 0){

                    foreach($data1 as $prop){
                        array_push($excludePropId, $prop->id);
                        // echo "Prop Id :".$prop->id."<br>";
                        // echo "<pre>";
                        //     print_r($prop);
                        // echo "</pre>";
                    }
                }

               return $finalData = \DB::table('bookings')->where('bookings.PropId', '=', $propId)->whereNotIn('PropId',$excludePropId)->get();
// ->toSql(); 
            // echo "<pre>";
            //     print_r($finalData);
            // echo "</pre>";
            //     exit;
        /*}else{
            $data = \DB::table('bookings')->where('bookings.PropId', '=', $propId)->whereNotExists( function ($query) use ($propId) {    
                $query->select(\DB::raw(1))->from('fc_rentalsenquiry')->whereRaw('bookings.PropId = fc_rentalsenquiry.prd_id ')->where('fc_rentalsenquiry.prd_id', '=', $propId);
        })->orderBy('id','DESC')->get();
            
        }
        if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
        echo "Peroperty :".$propId."<br>";
        exit;
        }*/
        
        return $data;
    }

}
