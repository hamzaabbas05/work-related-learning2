<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedMessage extends Model
{
    protected $table = 'fc_med_message';

    protected $guarded = ['id'];

    public $timestamps = false;



    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiverId', 'id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'senderId', 'id');
    }

    public function enquiry()
    {
        return $this->belongsTo(RentalsEnquiry::class, 'enqid', 'id');
    }

    public function property()
    {
        return $this->belongsTo(Property::class, 'productId', 'id');
    }

    public static function getReceivedMessages($id)
    {
    	return MedMessage::where('receiverId', $id)->orderBy('dateAdded', 'desc')->get();
    }

    public static function getSentMessages($id)
    {
    	return MedMessage::where('senderId', $id)->orderBy('dateAdded', 'desc')->get();
    }
    
    public static function getAdminMessages($id)
    {
    	return MedMessage::where('receiverId', $id)->where('admin_id', '!=', 0)->orderBy('dateAdded', 'desc')->get();
    }

    public static function changeReadStatus($id)
    {
    	return MedMessage::where('receiverId', $id)->update(array('msg_read' => 'Yes'));
    }


    public static function getConversations($id)
    {
        return MedMessage::where('enqid', $id)->orderBy('dateAdded', 'desc')->get();
    }
}


 