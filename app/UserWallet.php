<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWallet extends Model {

    public function user() {
        return $this->hasMany(User::class, "user_id")->select("id", "vouchercredit_id", "name", "email", "lastname");
    }

    public function userCredit() {
        return $this->belongsTo(UserCredit::class, "vouchaercredits_id");
    }

}
