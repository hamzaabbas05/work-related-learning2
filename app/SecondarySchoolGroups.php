<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecondarySchoolGroups extends Model {

    protected $table = 'secondary_school_groups_flash';
    
    public function user() {
        return $this->belongsTo(User::class);
    }

}
