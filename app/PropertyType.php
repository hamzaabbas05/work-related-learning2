<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Constants\AppConstants;

class PropertyType extends Model
{
    protected $table = 'property_type';

    protected $guarded = ['id'];

    public function property()
    {
        return $this->hasMany(Property::class);
    }

    public function getAll($isActive = false)
    {
        if ($isActive) {
          return PropertyType::where('status', AppConstants::STATUS_ACTIVE)->get();
        }
        return PropertyType::all();
    }
}
