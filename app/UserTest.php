<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTest extends Model
{
    protected $table = 'users_test';
    protected $guarded = ['id'];
}
