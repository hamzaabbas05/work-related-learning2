<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestRoomImages extends Model
{
    protected $table = 'guest_room_images';

    protected $guarded = ['id'];

    public $timestamps = false;
    
    public function userpaidguestroom() { 
        return $this->belongsTo(UserPaidGuestRoom::class);
    }
}
