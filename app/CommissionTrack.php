<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommissionTrack extends Model
{
    protected $table = 'fc_commission_tracking';

    protected $guarded = ['id'];

    public $timestamps = false;  
}
