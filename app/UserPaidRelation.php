<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaidRelation extends Model {

    public function user() {
        return $this->belongsTo(User::class);
    }

}
