<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model {

    protected $table = 'property';
    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function user_detail() {
        return $this->belongsTo(User::class)->select("id", "orgname");
    }

    public function user_email() {
        return $this->hasMany(EmailSent::class, 'property_id');
    }

    public function wishlists() {
        return $this->belongsToMany(Wishlist::class);
    }

    public function propertytype() {
        return $this->belongsTo(PropertyType::class, 'property_type');
    }

    public function classtype() {
        return $this->belongsTo(ClassType::class, 'class_type_id');
    }

    public function activitytype() {
        return $this->belongsTo(ActivityType::class, 'activity_type_id');
    }

    public function bedtype() {
        return $this->belongsTo(BedType::class, 'bed_type_id');
    }

    public function transfertype() {
        return $this->belongsTo(TransferType::class, 'transfer_type_id');
    }

    public function tourtype() {
        return $this->belongsTo(TourType::class, 'tour_type_id');
    }

    public function wowtype() {
        return $this->belongsTo(WowType::class, 'wow_type_id');
    }

    public function roomtype() {
        return $this->belongsTo(RoomType::class, 'room_type');
    }

    public function tutorrelation() {
        return $this->belongsTo(Tutor::class, 'relation_with_company');
    }

    public function amenities() {
        return $this->belongsToMany(Amenity::class, 'amenity_property', 'property_id', 'amenity_id');
    }

    public function propertyimages() {
        return $this->hasMany(PropertyImages::class, 'property_id', 'id');
    }

    public function rentals() {
        return $this->hasMany(RentalsEnquiry::class, 'id', 'prd_id');
    }

    public function scopeOrderByDate($query) {
        return $query->orderBy('created_at', 'desc');
    }

    public function enquiries($query) {
        return $this->hasMany(MedMessage::class, 'productId');
    }

    public function reviews() {
        return $this->hasMany(Review::class, 'prd_id', 'id')->orderBy("dateAdded", "DESC");
    }

    public function pricing() {
        return $this->hasOne(PropertyPricing::class, 'property_id');
    }

    public function getBookedProperty() {
        return $this->hasOne(RentalsEnquiry::class, 'prd_id');
    }

    public static function getBookedPropertyByDates($property_id = 0, $checkin_date = "", $checkout_date = "") {
        $bed_rental = array();
        if ($property_id > 0 && $checkin_date != "" && $checkout_date != "") {
            $newDateStart = date("Y-m-d 11:00:00", strtotime($checkin_date));
            $newDateEnd = date("Y-m-d  10:00:00", strtotime($checkout_date));

            //return $newDateStart."----".$newDateEnd;
            $bed_rental = RentalsEnquiry::where("prd_id", $property_id)
                    ->where('checkin', "<=", $newDateStart)
                    ->where('checkout', ">=", $newDateEnd)
                    ->where('approval', "Accept")
                    ->first();
        }


        return $bed_rental;
    }
    
    public static function prepareRestrictedDays($calendarObj) {
        $forbiddenCheckIn = '';
        $forbiddenCheckOut = '';
        $DisableCalDate = "";
        $prev = "";
        $all_dates = array();
        $selected_dates = array();
        if ($calendarObj->count() > 0) {
            foreach ($calendarObj as $CRow) {
                $DisableCalDate .= '"' . $CRow->the_date . '",';
            }
            $forbiddenCheckIn = '' . $DisableCalDate . '';
        }
        foreach ($calendarObj as $date) {
            $all_dates[] = trim($date->the_date);
            $date1 = new \DateTime(trim($date->the_date));
            $date2 = new \DateTime($prev);
            $diff = $date2->diff($date1)->format("%a");
            if ($diff == '1') {
                $selected_dates[] = trim($date->the_date);
            }
            $prev = trim($date->the_date);
            $DisableCalDate = '';
            foreach ($all_dates as $CRow) {
                $DisableCalDate .= '"' . $CRow . '",';
            }
            $forbiddenCheckIn = '' . $DisableCalDate . '';
            $DisableCalDate = '';
            foreach ($selected_dates as $CRow) {
                $DisableCalDate .= '"' . $CRow . '",';
            }
            $forbiddenCheckOut = '' . $DisableCalDate . '';
        }

        return array($forbiddenCheckIn, $forbiddenCheckOut);
    }

}
