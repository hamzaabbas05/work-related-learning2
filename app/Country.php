<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';

    public function currency()
    {
        return $this->hasOne(Currency::class);
    }

    public function getall()
    {
        $codeObj = Country::where('status', 1)->get();
 		return $this->convertToArray($codeObj);
 	}

    public function convertToArray($processObj)
    {
        $returnArr[null] = 'Select';
        if (is_object($processObj)) {
            foreach ($processObj as $process) {
                $returnArr[$process->id] = $process->name;
            }
            return $returnArr;
        }
        return $returnArr;
    }
}
