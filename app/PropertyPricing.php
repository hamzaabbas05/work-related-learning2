<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyPricing extends Model {

    protected $table = 'property_pricing';
    
    public function property() {
        return $this->belongsTo(Property::class, 'property_id');
    }

}
