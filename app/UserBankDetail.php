<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBankDetail extends Model {

    public function user() {
        return $this->belongsTo(User::class);
    }

}
