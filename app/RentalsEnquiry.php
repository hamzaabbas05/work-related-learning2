<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RentalsEnquiry extends Model {

    protected $table = 'fc_rentalsenquiry';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function property() {
        return $this->belongsTo(Property::class, 'prd_id', 'id');
    }

    public function property_detail() {
        return $this->belongsTo(Property::class, 'prd_id', 'id')->select("id", "user_id", "title", "exptitle", "property_type", "property_status")->with("user_email");
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function user_detail() {
        return $this->belongsTo(User::class, 'user_id', 'id')->with("user_booking", "school_info");
    }

    public function host() {
        return $this->belongsTo(User::class, 'renter_id', 'id');
    }

    public function payment() {
        return $this->hasOne(Payment::class, 'EnquiryId', 'id');
    }

    public function review() {
        return $this->hasMany(Review::class, 'enquiryId', 'id');
    }

    public function single_review() {
        return $this->hasOne(Review::class, 'enquiryId', 'id');
    }

    public static function availabilityCheckForProperty($newDateStart, $newDateEnd, $propertyId, $user_id, $rental_id) {
        return RentalsEnquiry::where(['user_id' => $user_id])->where("id", "!=", $rental_id)
                        ->where(function($query) use ($newDateStart, $newDateEnd) {
                            $query->where('checkin', '>=', $newDateStart)
                            ->orWhere('checkout', '<=', $newDateEnd);
                        })->get();
    }

    public static function availabilityDateCheckForProperty($newDateStart, $newDateEnd, $user_id, $rental_id) {
        return RentalsEnquiry::where(['user_id' => $user_id])->where("id", "!=", $rental_id)
                        ->where(function($query) use ($newDateStart, $newDateEnd) {
                            $query->where('checkin', '>=', $newDateStart)
                            ->orWhere('checkout', '<=', $newDateEnd);
                        })->get()->count();
    }

    public static   function userAllreadyAccpetedForPaid($newDateStart, $newDateEnd, $user_id)
    {

         return RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $user_id)->where("fc_rentalsenquiry.approval","=","Accept")
                    ->whereRaw("property.property_status = 'paid'")
                    ->whereRaw("property.property_type = '26'")
                    ->where(function($query) use ($newDateStart, $newDateEnd) {
                            $query->where('checkin', '>=', $newDateStart)
                            ->Where('checkout', '<=', $newDateEnd);
                        })->get()->count();

       
    }
     public static   function userAllreadyAccpetedForUnpaid($newDateStart, $newDateEnd, $user_id)
    {

         return RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $user_id)->where("fc_rentalsenquiry.approval","=","Accept")
                    ->whereRaw("(property.property_status = 'unpaid' OR property.property_status = 'unpaidflash')")
                    ->where(function($query) use ($newDateStart, $newDateEnd) {
                            $query->where('checkin', '>=', $newDateStart)
                            ->Where('checkout', '<=', $newDateEnd);
                        })->get()->count();

       
    }

    public static function getuserUnpaidRequestsForPromptEmailNotOld($user_id,$today_date){
        return RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $user_id)
                    ->whereRaw("(property.property_status = 'unpaid' OR property.property_status = 'unpaidflash')")
                    ->where(function($query) use ($today_date) {
                            $query->where('checkout', '>=', $today_date);
                        })->get()->count();

    }
    public static function getuserUnpaidRequestsForPromptEmailLastSevenDays($user_id,$today_date){
        $last_seven_days = date("Y-m-d",strtotime(" -7 days"));
        // are you seeing logic? not really unpaid_ yes in query i am getting onl
        $accepted =  RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $user_id)->where("fc_rentalsenquiry.approval","=","Accept")
                    ->whereRaw("(property.property_status = 'unpaid' OR property.property_status = 'unpaidflash')")
                    ->where(function($query) use ($today_date) {
                            $query->where('checkout', '>=', $today_date);
                        })->get()->count(); 
        $not_accepted =    RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $user_id)->where("fc_rentalsenquiry.approval","!=","Accept")
                    ->whereRaw("(property.property_status = 'unpaid' OR property.property_status = 'unpaidflash')")
                    ->where(function($query) use ($today_date,$last_seven_days) {
                            $query->where('checkout', '>=', $today_date)
                            ->WhereDate('fc_rentalsenquiry.dateAdded', '=', $last_seven_days); 
                        })->get()->count(); 

        return array("accepted"=>$accepted,"not_accepted"=>$not_accepted);
    }

    public static function getUserRequestSevenDaysPassed($user_id,$today_date){
         // are you seeing logic? not really unpaid_ yes in query i am getting onl
         $last_seven_days = date("Y-m-d",strtotime(" -7 days"));
        $not_accepted_last_seven_days =    RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $user_id)->where("fc_rentalsenquiry.approval","!=","Accept")
                    ->whereRaw("(property.property_status = 'unpaid' OR property.property_status = 'unpaidflash')")
                    ->where(function($query) use ($today_date,$last_seven_days) {
                            $query->where('checkout', '>=', $today_date)
                            ->WhereDate('fc_rentalsenquiry.dateAdded', '=', $last_seven_days); 
                        })->get(); 
        return $not_accepted_last_seven_days;

    }

    public static function getEnquiryDates($enquiresObj, $checkIn, $checkOut) {
        $enquiryDates = array();
        $count = 0;
        foreach ($enquiresObj as $enq) {
            $dates = RentalsEnquiry::getDatesFromRange($enq->checkin, $enq->checkout);
            $i = 1;
            $dateMinus1 = count($dates);
            foreach ($dates as $date) {
                if ($i <= $dateMinus1) {
                    array_push($enquiryDates, $date);
                }
                $i++;
            }
            if (in_array($checkIn, $enquiryDates) || in_array($checkOut, $enquiryDates)) {
                $count++;
            }
        }
        return $count;
    }

    public static function getDatesFromRange($start, $end) {
        $dates = array($start);
        while (end($dates) < $end) {
            $dates[] = date('Y-m-d', strtotime(end($dates) . ' +1 day'));
        }
        return $dates;
    }

    public static function checkFirstChoice($user_id, $rental_id, $property_type = "unpaid") {
        if ($property_type == "unpaid") {
            $getUserFirstChoice = RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $user_id)
                    ->whereRaw("(property.property_status = 'unpaid' OR property.property_status = 'unpaidflash')")
                    ->orderBy("fc_rentalsenquiry.dateAdded", "ASC")
                    ->first();
            //return $getUserFirstChoice;
            if(isset($getUserFirstChoice->id) && ($getUserFirstChoice->id == $rental_id)) {
                return "First Choice";
            } else {
                return "2nd or 3rd Choice";
            }
        } else if($property_type == "paid") {
            $getUserFirstChoice = RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $user_id)
                    ->where("property.property_status", 'paid')
                    ->orderBy("fc_rentalsenquiry.dateAdded", "ASC")
                    ->first();
//            echo "<pre>";
//            echo $rental_id."<br />";
//            print_r($getUserFirstChoice->id);
//            echo "</pre>";
//            exit;
            if(isset($getUserFirstChoice->id) && ($getUserFirstChoice->id == $rental_id)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public static function getSchoolHouseTime($paid_id, $unpaid_id) {
        $distance_row = array();
        if(!empty($paid_id) && !empty($unpaid_id)) {
            $distance_row = DistanceHomeWork::where("paidPropId", $paid_id)->where("unpaidPropId", $unpaid_id)->orderBy("id", "DESC")->first();
        }
        return $distance_row;
    }

}
