<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserExtraInfo extends Model {

    protected $table = 'user_extrainfo';
    
    public function user(){
        return $this->belongsTo(User::class);
    }

}
