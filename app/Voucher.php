<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    public function user(){
        return $this->hasOne(User::class)->select("id", "voucher_id", "name", "email", "lastname");
    }
}
