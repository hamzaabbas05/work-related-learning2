<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AddPropertyAddresses extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:addPropertyAddress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This function will run all the unpiad, unpaidflash and paid listings to get there addresses and stored for gmaps';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $innerCount =0;
        $mainCount = 0;
        $filePath = dirname(dirname(dirname(dirname(__FILE__))))."/distance_cronlog/livetet_chron_log_file_".date('YmdH_i_s').".txt";

        echo "FilePath :".$filePath."<br>";//exit;
        
        // echo "<pre>";
        // print_r($_SERVER);
        // echo "</pre>";
        // exit;

        // $fp = fopen($filePath,"a"); 
        //       chmod($filePath,"0755");
        //       // chmod($finalFilePath,"0755");

        //                       $time = 0;
        //                       $address = '';
        //                     $write  = "\n =========================== Start Here on ".date("Y-m-d H:i:s")." =========================== \n";
        //                     $write .= "\n\n address = $address , time = $time added \n\n";
        //                     $write .= "\n =========================== End Here on ".date("Y-m-d H:i:s")." =========================== \n";

        //                     fwrite($fp, $write);
        //                     fclose($fp);
        //                     exit;
        $all_paid_property = \App\Property::where("property_status", "paid")->where('property_type',0)->where('property.status','!=',2)
                ->join("users", "users.id", "=", "property.user_id")
                ->join("user_paid_services", "user_paid_services.user_id", "=", "property.user_id")
                ->orderBy('user_paid_services.id','DESC')
                // ->where('users.id','=','37')
                 // ->offset(1)
                // ->limit(100) // this for paid . if you with then revert it? there? it will fetch 1 first listing and all unpaid, for now we will not use google api for the intial test, because we will set time = -1 for  all  new inserted and once all are ok then we will open google api after cron set and executable . okay?
                ->get();
                echo "<br> Count Paid property :".count($all_paid_property)."Paid Property :";
                // echo "Paid Property : <pre>";
                //     print_r(($all_paid_property));
                // echo "</pre>";
                // exit;
                // exit; 6 paid 152 unpaid/unpaidflash = 912 total record
        if (sizeof($all_paid_property) > 0) {
            $countLimits = 0;
            foreach ($all_paid_property as $paid_property) {
                $propId = $paid_property->id;
                // to get the houseaddress from user_paid_services table for normal address, not from property table
                $address = isset($paid_property->user->paid_service->houseaddress) ? $paid_property->user->paid_service->houseaddress : '';
                // this is where from frontend we store school house legal address, 
                // check if school house property address is not same
                if (isset($paid_property->user->paid_service->house_property_differ) && $paid_property->user->paid_service->house_property_differ == "1") {
                    $address = isset($paid_property->user->paid_service->propertyaddress) ? $paid_property->user->paid_service->propertyaddress : '';
                }
                if ($paid_property->addr_work_org_same == 0) {
                    $address = $paid_property->workplace_address; // workplace_address is our property, listing, address. Case paid, if checkbox checked, then use this address:workplace_address
                }
                $address = \DB::getPdo()->quote($address);
                // to get unpaid details
                // to get paid details
                if (!empty($address) && $address != "") {
                    $all_unpaid_property = \App\Property::whereRaw("property_status ='unpaid' or property_status ='unpaidflash'")->get(); //offset(1)->limit(25)->limit 10 not here_ this is for unpaid. 
                    if (sizeof($all_unpaid_property) > 0) {
                        // echo "<pre>";
                        //     print_r($all_unpaid_property);
                        // echo "</pre>";
                        // exit;
                        foreach ($all_unpaid_property as $unpaid_property) {
                            // echo "\n Inner Count :".$innerCount." \t Main Count :".$mainCount;

                          /*  $fp = fopen($filePath,"a"); 
                            chmod($filePath,0755);

                              $time = 0;
                            $write  = "\n =========================== Start Here on ".date("Y-m-d H:i:s")." =========================== \n";
                            $write .= "\n\n address = $address , time = $time added \n\n";
                            $write .= "\n =========================== End Here on ".date("Y-m-d H:i:s")." =========================== \n";

                            fwrite($fp, $write);
                            fclose($fp);
                            $innerCount++;
                           
                            continue; */
                            //that will fetch 25 records for unpaid and unpaidflash , sure thought you were making dataset, first need to check why chron is not working , (as you said its not woking). once it works we will work as per discussed. I think we found the reason, the general 25 limit logic was wrong.still chron is not running. Does chron run on live? No idea, think not.because we need also this for update on daily baseis which have -1 time .Yes to do .

                            $unpaidPropId = $unpaid_property->id;
                            $unpaidAddress = isset($unpaid_property->user->raddress18) ? $unpaid_property->user->raddress18 : ''; //this is where we store address for unpaid 
                            if ($unpaid_property->addr_work_org_same == 0) {
                                $unpaidAddress = $unpaid_property->workplace_address; // case unpaid, if checkbox checked, then use this address:workplace_address
                            }
                            /*                             * ****************** */
                            $time = 0;
                            $add1 = $address;
                            $add1 = str_replace(' ', '+', $add1);
                            $add1 = str_replace('#', '', $add1);
                            $add2 = $unpaidAddress;
                            $add2 = str_replace(' ', '+', $add2);
                            $add2 = str_replace('#', '', $add2);
                            if ($add1 != '' && $add2 != '' && $add1 != "''" && $add2 != "''") {
                                //$add1 = '';
                                // $add1 = 'Via Provinciale, 22, Albino, Province of Bergamo, Italy';
                                // $add2 = 'Pradalunga, BG, Italia';
                               // $gmapApi = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyC646z-W9bGxhDtgjILxonsK2KI5TU-z68&origins=$add1&destinations=$add2&mode=transit";
                               $gmapApi = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyC646z-W9bGxhDtgjILxonsK2KI5TU-z68&origins=$add1&destinations=$add2&mode=transit";
                               $today = date('D');
                               // $today = 'Mon';
                               if($today == 'Mon'){
                                $gmapApi .= '&arrivalTime=0900';
                               }
                               $gmapApi .= '&day_sensor=false';

                               $distance_how_work_log = new \App\DistanceHomeWorkRequestResponseLog();
                               $distance_how_work_log->request_url = $gmapApi;
                               $distance_how_work_log->created_at = date("Y-m-d H:i:s");
                               $distance_how_work_log->ip = $_SERVER['REMOTE_ADDR'];
                               $distance_how_work_log->save();
                               // echo "Last Inserted Id :".$distance_how_work_log->id."<br>";
                               // echo "DistanceLog <pre>";
                               //  print_r($distance_how_work_log);
                               // echo "</pre>";
                               // exit;

// exit;
                                //echo $details;die; we had tried with different mode> transport, I don-t know why back at driving mode let me check it
                                //there is no api key in this request, no as i have copied that from Jitendra file.
                                //Is there problem? no ok. if monday it will run with arrival time by 9:00 else normal ._
                               //so we cannot set for now to run on mon_ okay
                               //okay, shall we, for testing, and rather real values anyway, interrogate for arrival time 9am_ it will give us values for tomorrow fri morning, hich will be very siminla tomonday, prblem is Sat and Sun. because time is over correct? because if you interrogte for 9am, you get tomorrowsvalues Yes. so  WIe can proceed Yes, we can. still we need to put more condition is about time if it is before 9AM then only set arrivalTime. set arrival time anyway, whatever time it is it will fetch tomorrows. that I undestood but what if today is Monday this will fetch data correct with arrival time. one more question By which time this cron will run on live? I guess 2am good_ and one time one day correct? As you set if for Mondays it is fine, we will have to limit calls to some thousand *I think total we have is about 5000 combinations, so for 100.000 monthy would be more than enough, we could run all but I prefer to not use up all available. We can add these conditions after getting all vlaues anyway, we can update another time. We can decide. Yes we can decide so for now put this in comment and go ahead. Sure le t-s complete table first of all. okay. testing 1 limit until we get correct values. for now we have all corret data which are in last cron. but arrival tiem only just set is it workign correctly_ Yes, its consider only on Monday . se we haveb tested it will run on mon, Yes but this condition also run on monday only. lets seet on monday for this result. but we will have to wait till monday to see results, coreetc_
                               //yes, so let-s put friday, so we can see tomorrow, set limit for 10, okay_ so limit 1 to increase 10? yes so we see some results, not only 1 listing, agree_. this will be 10*68 active = a bit more result total , okay_ ok so I will remove 25 limit to all (as we have only 68 result unpaid yes in local) okay, local I was thnking live. fine then/. without test we would not work on live because it will affect live data thats why. sure.okay les remove limit for unpaid. nad put limt for paid to 10. and Mon to Fri day limit. yes.

                                // let me check how i can add arrival time.
                               // if($_SERVER['REMOTE_ADDR'] = '93.42.44.112'){
                                    // $json = file_get_contents($gmapApi);
                                    $json = file_get_contents($gmapApi);
                                    $details = json_decode($json, TRUE);

                                    $distance_how_work_log->response_data = $json;
                                    $distance_how_work_log->updated_at = date("Y-m-d H:i:s");
                                    $distance_how_work_log->save();
                                    // exit;
                                //     echo "<br> gMap Url :".$gmapApi."<br>";
                                //     echo "this is testing for one record <pre>";
                                //     print_r($details);
                                //     print_r($json);
                                // echo "</pre>";
                                // exit;
                               // }
                               if ($details['status'] == 'OK' && isset($details['rows'][0]['elements'][0]['duration']['value'])) {
                                    $time = $details['rows'][0]['elements'][0]['duration']['value'];// 

                                    $time = floor($time / 60);
                                    //this will run 75555/60 and remove the decimals yes floor function do this (r) it.
                                } else {
                                    $time = "N/A";
                                    echo "no API Returns \n";
                                }
                                echo "this is testing for one record <pre>";
                                echo "<br> Time : ".$time."<br>";
                                    print_r($details);
                                    print_r($json);
                                echo "</pre>";
                                 $fp = fopen($filePath,"a");
                             chmod($filePath,0777);
                            $write  = "\n =========================== Start Here on ".date("Y-m-d H:i:s")." =========================== \n";
                            $write .= "\n \n Reqest Url : ".$gmapApi." \n \n";
                            $write .= "\n\n address = $address , time = $time added \n\n";
                            $write .= "\n =========================== End Here on ".date("Y-m-d H:i:s")." =========================== \n";

                            fwrite($fp, $write);
                            fclose($fp);
                                // exit;
                            echo "Map Url :" .$gmapApi."<br>";
                            echo "Fall in IF Part <pre>";
                                print_r($add1);
                                echo "Add2 :";
                                print_r($add2);
                            echo "</pre>";
                            echo "<br> paid prop ID :".$propId."<br> Unpaid Id :".$unpaidPropId."<br>";

                            }else {
                                echo "<br> FALL in ELSE Part <pre>";
                                    print_r($unpaid_property);
                                echo "</pre><br> ";
                                continue;
                            }

                            
                            $distance_how_work = \App\DistanceHomeWork::where(["paidPropId" => $propId, "unpaidPropId" => $unpaidPropId])->first();
                            if (!isset($distance_how_work->id)) {
                                $distance_how_work = new \App\DistanceHomeWork();
                            }
                            $distance_how_work->paidAddress = $address;
                            $distance_how_work->paidPropId = $propId;
                            $distance_how_work->unpaidAddress = $unpaidAddress;
                            $distance_how_work->unpaidPropId = $unpaidPropId;
                            $distance_how_work->time = $time;
                           $distance_how_work->save();

                             $fp = fopen($filePath,"a");
                             chmod($filePath,0777);
                            $write  = "\n =========================== Start Here on ".date("Y-m-d H:i:s")." =========================== \n";
                            $write .= "\n \n Reqest Url : ".$gmapApi." \n \n";
                            $write .= "\n\n address = $address , time = $time added \n\n";
                            $write .= "\n =========================== End Here on ".date("Y-m-d H:i:s")." =========================== \n";

                            fwrite($fp, $write);
                            fclose($fp);
                            // echo "address = $address , time = $time added \n\n";
                            // exit;
                        }
                        $mainCount++;
                    }
                }
            }
        }
        $this->info("All the listing addresses are stored successfully");
    }

}
