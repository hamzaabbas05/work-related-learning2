<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Mailer\MailNotificationsForUser;

class JobReviewSystemCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:jobReviewSystem';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Unpaid Job Review Email to Parents or students when Job Date Out is today';
    private $mailNotificationsForUser;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MailNotificationsForUser $mailNotificationsForUser) {
        parent::__construct();
        $this->mailNotificationsForUser = $mailNotificationsForUser;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $propertyObj = \App\Property::select("property.*", "fc_rentalsenquiry.id as rental_id", "fc_rentalsenquiry.user_id as rental_user_id")
                ->join("fc_rentalsenquiry", "property.id", "=", "fc_rentalsenquiry.prd_id")
                ->where("fc_rentalsenquiry.checkout", date("Y-m-d 00:00:00"))
//                ->where("property.status", "1")
                ->where("property.property_status", "unpaid")
                ->where("fc_rentalsenquiry.approval", "Accept")
                ->get();
        if(sizeof($propertyObj) > 0) {
            foreach($propertyObj as $property) {
                if($property->rental_user_id > 0) {
                    $user_detail = \App\User::where("id", $property->rental_user_id)->first();
                    $this->mailNotificationsForUser->sendReviewSystemEmail($property, $user_detail);
                }
            }
        }
        $this->info("Job Reviews email sevices has run successfully");
    }

}
