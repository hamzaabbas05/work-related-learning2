<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        // Commands\JobReviewSystemCommand::class,
        // Commands\JobReviewPaidSystemCommand::class,
        Commands\AddPropertyAddresses::class,
        
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('inspire')->hourly();
        // $schedule->command('command:jobReviewSystem')->daily();
        // $schedule->command('command:JobReviewpaid')->daily();
        // ->fridays()      ->at('17:00')
        $schedule->command('command:addPropertyAddress')->daily();
        

    }
}
