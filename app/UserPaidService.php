<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaidService extends Model {

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function realRoomType() {
        return $this->belongsTo(RealRoomTypes::class);
    }

    static public function roomFacility($facility_id) {
        $facility = RoomFacility::where("id", $facility_id)->first();
        return $facility;
    }

    static public function getRoomFacilityNames($facility_ids) {
        $facilities = "";
        if (!empty($facility_ids)) {
            $ids = explode(",", $facility_ids);
            $facilities = RoomFacility::whereIn("id", $ids)->get();
        }
        return $facilities;
    }

    public static function getRealRoomNames($room_ids) {
        $rooms = "";
        if (!empty($room_ids)) {
            $ids = explode(",", $room_ids);
            $rooms = ActivityType::whereIn("id", $ids)->where(["is_active" => "1", "is_delete" => "0"])->get();
        }
        return $rooms;
    }

    public static function getActivityNames($activity_ids) {
        $activies = "";
        if (!empty($activity_ids)) {
            $ids = explode(",", $activity_ids);
            $activies = ActivityType::whereIn("id", $ids)->where(["is_active" => "1", "is_delete" => "0"])->get();
        }
        return $activies;
    }

    public static function getClassesNames($class_ids) {
        $classes = "";
        if (!empty($class_ids)) {
            $ids = explode(",", $class_ids);
            $classes = ClassType::whereIn("id", $ids)->where(["is_active" => "1", "is_delete" => "0"])->get();
        }
        return $classes;
    }

    public static function getTransferNames($transfer_ids) {
        $transfers = "";
        if (!empty($transfer_ids)) {
            $ids = explode(",", $transfer_ids);
            $transfers = TransferType::whereIn("id", $ids)->where(["is_active" => "1", "is_delete" => "0"])->get();
        }
        return $transfers;
    }

    public static function getTourNames($tour_ids) {
        $tours = "";
        if (!empty($tour_ids)) {
            $ids = explode(",", $tour_ids);
            $tours = TourType::whereIn("id", $ids)->where(["is_active" => "1", "is_delete" => "0"])->get();
        }
        return $tours;
    }

    public static function getWowNames($wow_ids) {
        $wows = "";
        if (!empty($wow_ids)) {
            $ids = explode(",", $wow_ids);
            $wows = WowType::whereIn("id", $ids)->where(["is_active" => "1", "is_delete" => "0"])->get();
        }
        return $wows;
    }

}
