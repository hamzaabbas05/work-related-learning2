<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
  
Route::get('/myTime', function()
{ 
    echo "Current Date :".date("Y-m-d H:i:s");
});

// Route::get('/mapsapi', function()
// { 
//     echo "Current Date :".date("Y-m-d H:i:s");
//     // echo $url = "https://maps.google.com/maps/api/geocode/json?address=Albino,ProvinceofBergamo,Italy&sensor=false&key=AIzaSyA0BQbSzrX8W4conO6nmF9-LzpH4R-y1jU";
//     $add1 = '';
//     $add1 = 'Via Provinciale, 22, Albino, Province of Bergamo, Italy';
//     $add2 = 'Pradalunga, BG, Italia';
//     // echo "GmapApi :".$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyC646z-W9bGxhDtgjILxonsK2KI5TU-z68&origins=$add1&destinations=$add2&mode=transit"; // this API 1 for distancematrix. the first task was to find old API and add new one, to test distanhcematrix as second task to take advantage of google support for final working of ditancematrix, okay then I have changed key for Homepage search module and search2 module. so check for whole folder for old key for task1., all keys were checked months ago, we have them here again just like other broken stuff, we have no idea how many old files you ave reuplaod on server. this whole folders download from live so whatever old or new file on server that we have on local. and also I have checked in old backup folder of local but I dindnt find any new key in that backup folder too. all have old keys .. so that or those days of work went lost some other way, agree_I dont remember that we had worked on this key , distancematrix_ yest for that module we have changed key for distancematrix. It is what i thouhgt we were speaking about.
//     //You have seen google APIs we have and restrictions and google support so you should know what to do, agree_ yes agree thats why I am confirming still have any issue in distancematrix or not because this we use for cron so I dont have any idea about weather its working or not last time we fixed for IP locat, I have updated with url https location as support suggested and correctly so I think. okay let check for first task first. Sure
//     echo "GmapApi : " .$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyC646z-W9bGxhDtgjILxonsK2KI5TU-z68&origins=$add1&destinations=$add2&mode=transit";
//     $getResult = file_get_contents($url);
//     echo "<br>";
//     var_dump($getResult);

// }); 


Route::get('/findDistance', function()
{
   // phpinfo();
   // exit;
    // Artisan::queue('command:addPropertyAddress', []);
     $today = date("D");
   // echo "Date :".date("Y-m-d H:i:s");  

   $filePath = dirname(dirname(dirname(__FILE__)))."/distance_cronlog/testJichron_log_file_".date('YmdH_i_s').".txt";
 $fp = fopen($filePath,"a");
        $write  = "\n =========================== Start Here on ".date("Y-m-d H:i:s")." =========================== \n";
    $write .= "\n \n Reqest Url : \n \n";
    // $write .= "\n\n address = $address , time = $time added \n\n";
    $write .= "\n =========================== End Here on ".date("Y-m-d H:i:s")." =========================== \n";

fwrite($fp, $write);
fclose($fp);
    // exit;
// $today = 'mon';
    if( strtolower($today) == 'mon'){

        Artisan::queue('command:addPropertyAddress', []);
    }else{
        echo "Fall in Else part";
    }
});
// Route::get('/vend_pub', function()
// { 
//     // echo "test";exit;
//     // var_dump(exec('php artisan config:cache'));
//     var_dump(Artisan::call("vendor:publish"));
// });


// Route::get('/udpate_composer', function()
// { 
//     // var_dump(exec('php artisan config:cache'));
//     var_dump(Artisan::call("composer:update"));
// });
Route::get('testmail','RentalsController@send_test_email');
Route::get('mail/{mail}', 'RegisterController@testmail');
Route::get('auth/login', 'HomeController@signIn');
Route::get('/', 'HomeController@index');
// testing route for cron jobs
Route::get('/testTime', 'HomeController@testTime');
Route::get('/addTimeDistance/{offset}', 'HomeController@addTimeDistance');
// end testing 
Route::get('testview/{sdf}', 'CmsController@agree');
Route::get('home', 'HomeController@index');
Route::any('signin', 'HomeController@signIn');
Route::get('signup', 'HomeController@signUp');
Route::get('agree/{id}', 'CmsController@agree');
//Route::get('search', 'RentalsController@search'); there is a double hereunder but with no name. thats why it is commentout

Route::get('gmap/{rental_id}/{property_id}/{user_rental}', 'BookingGeneratorController@gmap');

Route::get('destination/{name}/lat/{lat}/long/{long}', 'RentalsController@searchDestination');
//Route::get('search/{name}', 'RentalsController@search');
Route::get('search/{name}', 'RentalsController@search2');
Route::post('getsearchupdate', 'RentalsController@updateSearch');
Route::post('sendrequest', 'RentalsController@request_booking');
Route::post('send-requests', 'RentalsController@send_paid_request');
Route::post('instantbook', 'RentalsController@instantbook');
Route::get('paymentcheck', 'PaypalController@getDonecheck');
Route::get('checkout', 'RentalsController@checkout');
Route::get('fetchOlderRequest', 'RentalsController@fetch_older_property_request');

// homepage 4 links section.
Route::get('search_WorkAndUsersListingPaid/{name}', 'RentalsController@search2');
// to manage search page ajax search
Route::post('ajaxSearch', 'RentalsController@ajaxsearch');

/* Wishlist Routes */
Route::post('getlistpopup', 'RentalsController@getlistpopup');
Route::post('savewishlists', 'RentalsController@savewishlists');
Route::post('createnewlist', 'RentalsController@createnewlist');
Route::post('removewishlist', 'RentalsController@removewishlist');
Route::post('sendcontactmessage', 'UsersController@sendcontactmessage');

/*addcart routes*/
Route::post('addroombedtocart', 'RentalsController@addRoomBedToCart');
Route::post('addclassroomtocart', 'RentalsController@addClassRoomToCart');
Route::post('addtocart', 'RentalsController@addtocart');
Route::get('getcartlist', 'RentalsController@fetch_cart_data');
Route::post('request_send', 'RentalsController@send_multiple_request');
Route::get('request/multiplebooking/{id}', 'RentalsController@viewRequestMultipleBookingResult');

Route::get('view/rentals/{id}', 'RentalsController@viewRentals');
Route::get('view/rentals_paid/{id}', 'RentalsController@viewRentalsPaid');
Route::get('view/school_house/{id}', 'RentalsController@schoolhouse'); // this is for paid status
Route::get('view/school_house_dev/{id}', 'RentalsController@schoolhousedev'); // this is for paid status

Route::post('showpricedetail', 'RentalsController@showpricedetail');
Route::get('basket', 'RentalsController@showBasket');
Route::get('paid-checkout', 'RentalsController@paidCheckout'); 
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post'); 

Route::get('test', 'RentalsController@testfunc');
Route::get('getDirectoryImages', 'TestController@getDirectoryImages');
Route::get('changeDBImages', 'TestController@changeDBImages');
Route::get('phpinfo', 'TestController@index');
Route::any('rentals/searchBeds', 'RentalsController@searchBeds');


// Authentication routes...
//Registraion and login routes */
Route::get('validatedata', 'RegisterController@validatedata'); //Ajax
Route::get('loginvalidate', 'RegisterController@loginvalidate'); //Ajax
Route::post('user_register', 'RegisterController@register'); //Ajax
Route::post('user_login', 'RegisterController@login'); //Ajax
Route::get('logout', 'RegisterController@doLogout');
Route::get('confirm/{code}', 'RegisterController@confirmEmail');
Route::get('jobEmailConfirm/{code}', 'HomeController@jobEmailConfirm');
//SocialMedia Login Facebook,GooglePlus
Route::get('social/login/redirect/{provider}', 'SocialAuthController@redirectToProvider');
Route::get('social/{provider}/login', 'SocialAuthController@callback');
// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
// CMS Routes
Route::get('cms/{page}', 'CmsController@showPage');
Route::get('aboutus', 'CmsController@aboutus');
Route::get('contactus', 'HomeController@contactus');
Route::get('faq/{type}', 'FaqController@showFaq');
Route::get('secure/{value?}', 'RentalsController@securexecute');
Route::get('evaluate/{token}', 'ReservationController@evaluate');
Route::post('user/evaluatedone', 'ReservationController@evaluatedone');
Route::get('viewuser/{id?}', 'UsersController@viewProfile');
Route::get('user/view/{id?}', 'UsersController@viewProfile');
Route::get('PromptSendUnpaid','UsersController@PromptSendUnpaid'); 
Route::get('updateDistanceTime','UsersController@updateDistanceTime');
Route::get('imageresizer','UsersController@imageResizer');

//Front End Routes
Route::group(['middleware' => 'auth'], function () {

    /* Paypal Routes */
    Route::post('getCheckout', ['as' => 'getCheckout', 'uses' => 'PaypalController@getCheckout']);
    Route::get('getDone', ['as' => 'getDone', 'uses' => 'PaypalController@getDone']);
    Route::get('getCancel', ['as' => 'getCancel', 'uses' => 'PaypalController@getCancel']);
    Route::get('payment/cancel', 'PaypalController@showCancel');
    Route::get('request/booking/{id}', 'RentalsController@viewRequestBookingResult'); 
    Route::get('request/booking-multiple/{id}', 'RentalsController@viewMultipleRequestBookings');
    Route::get('payforpaid/{id}', 'RentalsController@payPendingForPaid');
    Route::post('updatebookingstatus', 'RentalsController@updatebookingstatus');
    Route::post('updateSchoolHousebookingstatus', 'RentalsController@updateSchoolHousebookingstatusDev');
    Route::post('disableThesedates', 'RentalsController@disableThesedates');
    Route::post('notavailable', 'RentalsController@noteavailable');
    Route::post('jobStatusUpdate', 'RentalsController@updatebookingstatus')->name("jobStatusUpdate");
    Route::post('getmessages', 'UsersController@getMessageType');
    Route::get('view/message/{bookingNo}', 'RentalsController@showBookingDetails');
    Route::get('tutorConfirmation/{id}', 'RentalsController@tutorConfirmation');




    // To Show view for add space 
    Route::group(['prefix' => 'rentals'], function () {
        Route::get('add', 'RentalsController@add'); 
        Route::get('add/unpaid-work-experience', 'RentalsController@addUnpaid');
        Route::get('add/unpaid-work-experience-flash', 'RentalsController@addUnpaidFlash');
        Route::get('add/new-school-house', 'RentalsController@addSchooHouse');
        Route::post('saveNewSchoolHouse', 'RentalsController@saveNewSchoolHouse');
        Route::get('add/school-house', 'RentalsController@addPaid');
        Route::get('add/school-house-bed', 'RentalsController@addPaidBed');
        Route::post('uploadroomimages', 'RentalsController@uploadroomimages'); 
        Route::post('uploadclassroomimages', 'RentalsController@uploadclassroomimages'); 
        Route::post('uploadactivityimages', 'RentalsController@uploadActivityImages');
        Route::post('uploaddietimages', 'RentalsController@uploadDietImages');
        Route::post('addnewroom', 'RentalsController@saveNewRoom');
        Route::post('addnewclassroom', 'RentalsController@saveClassRoom'); 
        Route::post('addnewactivity', 'RentalsController@saveActivity');
        Route::post('addnewdiet', 'RentalsController@saveDiet');
        Route::post('editroom', 'RentalsController@updateRoom');
        Route::post('editclassroom', 'RentalsController@updateClassRoom');
        Route::post('editactivity', 'RentalsController@updateActivity');
        Route::post('editdiet', 'RentalsController@updateDiet');
        Route::post('deleteroom', 'RentalsController@deleteRoom');
        Route::post('deleteactivity', 'RentalsController@deleteActivity');
        Route::post('deletediet', 'RentalsController@deleteDiet');
        Route::post('deleteplace', 'RentalsController@deletePlace');
        Route::post('addnewbed', 'RentalsController@saveNewBed'); 
        Route::post('updatebed', 'RentalsController@updateBed');
        Route::post('loadbedsforupdate', 'RentalsController@loadbedsforupdate');
        Route::post('deletebed', 'RentalsController@deleteBed');
        Route::post('saverental', 'RentalsController@saveRentalFirstStep');
        Route::post('saveSchoolHouse', 'RentalsController@saveSchoolHouse');
        //Basic
        Route::get('basic_paid/{id}', 'RentalsController@showBasicPaid');
        Route::post('basic_paid_submit', 'RentalsController@basic_paid_submit');
        Route::get('paid_firt_listing/{id}', 'RentalsController@showFirstListingPaid');
        Route::post('paid_first_submit', 'RentalsController@first_paid_submit');
        Route::get('basic/{id}', 'RentalsController@showBasic');
        Route::post('basic_submit', 'RentalsController@basic_submit');
        //Description
        Route::get('first_description_paid/{id}', 'RentalsController@showFirstDescriptionPaid');
        Route::post('first_description_paid_submit', 'RentalsController@descriptionFirstSubmitPaid');
        
        Route::get('description_paid/{id}', 'RentalsController@showDescriptionPaid');
        Route::post('description_paid_submit', 'RentalsController@descriptionSubmitPaid');
        Route::get('description/{id}', 'RentalsController@showDescription');
        Route::post('description_submit', 'RentalsController@descriptionSubmit');
        //Amenities
        Route::get('amenities/{id}', 'RentalsController@showAmenity');
        Route::post('saveamenitylist', 'RentalsController@amenitySubmit'); //ajax
        //photos
        Route::get('first_photos_paid/{id}', 'RentalsController@showFirstPhotosPaid');
        
        Route::get('photos_paid/{id}', 'RentalsController@showPhotosPaid');
        Route::get('photos/{id}', 'RentalsController@showPhotos');
        Route::post('startfileupload', 'RentalsController@startfileupload');
        Route::post('savephotolist', 'RentalsController@savePhoto'); //ajax
        //location
        Route::get('first_location_paid/{id}', 'RentalsController@showFirstLocationPaid');
        
        Route::get('location_paid/{id}', 'RentalsController@showLocationPaid');
        Route::get('location/{id}', 'RentalsController@showLocation');
        Route::post('savelocationlist', 'RentalsController@saveLocationList'); //ajax
        //pricing
        Route::get('pricing/{id}', 'RentalsController@showPricing');
        Route::post('savePricing', 'RentalsController@savePricing');
        Route::post('savepricelist', 'RentalsController@savepricelist'); //ajax
        //bookings
        Route::get('booking_paid/{id}', 'RentalsController@showBookingPaid');
        Route::get('booking/{id}', 'RentalsController@showBooking');
        Route::post('savebookings', 'RentalsController@saveBookings'); //ajax
        //calendar
        Route::get('first_calendar_paid/{id}', 'RentalsController@showFirstCalendarPaid');
        
        Route::get('calendar_paid/{id}', 'RentalsController@showCalendarPaid');
        Route::get('calendar/{id}', 'RentalsController@showCalendar');
        Route::post('savecalendar', 'RentalsController@saveCalendar'); //ajax
        Route::get('delete/{id}', 'RentalsController@deleteListing');
        Route::post('delete', 'RentalsController@deleteListing');
        Route::get('optionalpaidservicelist/{id}', 'RentalsController@otherpaidservices');
    });
    Route::group(['prefix' => 'user'], function () {
        Route::get('edit', 'UsersController@editProfile');
        Route::get('verification', function(){
            return redirect()->to('user/trust');
        });
        Route::get('extraInfo', 'UsersController@editExtraInfo');
        Route::get('studentGuest', 'UsersController@studentGuest');
        Route::get('parent', 'UsersController@editParent');
        Route::get('preEnrolment', 'UsersController@editPreenrolment');
        Route::get('preEnrolmentOver18', 'UsersController@editPreEnrolmentOver18');
        Route::get('workExperience', 'UsersController@workExperience');
        Route::get('paidServices', 'UsersController@paidServices');
        Route::get('bedstoroom', 'UsersController@bedImagesToRoom'); 

        //Route::get('trust', 'UsersController@viewTrust');
        Route::get('trust', 'UsersController@editVerification');
        Route::get('dashboard', 'UsersController@showDashboard');
        Route::get('changepassword', 'UsersController@changepassword');
        Route::get('wallet', 'UsersController@wallet');
        Route::post('profile_submit', 'UsersController@profile_submit');
        Route::post('update_profile', 'UsersController@update_profile');
        Route::post('image_submit', 'UsersController@image_submit');
        Route::post('resetpassword', 'UsersController@resetpassword');

        Route::get('listing/rentals/{status}', 'RentalsController@listProperty');
        Route::get('listing-flash/rentals/{status}', 'RentalsController@listPropertyFlash');
        Route::get('listing-paid/rentals/{status}', 'RentalsController@listPropertyPaid');
        Route::get('trips', 'TripController@showTrips');
        Route::get('reservations/{status?}', 'TripController@showReservations');
        Route::get('schoolHouses/{status?}', 'TripController@schoolHouses');
        Route::get('messages', 'UsersController@showMessages');
        Route::get('conversation/{id}', 'UsersController@conversation');
        Route::get('transactions/{status}', 'PaymentController@getTransactions');
        Route::get('wishlist', 'RentalsController@getwishlist');
        Route::get('wishlist/{id}', 'RentalsController@getwishlistbyid');
        Route::get('addreview/{bookingno}', 'RentalsController@addreview');
        Route::get('reviews/{type}', 'RentalsController@viewreviews');
        Route::post('review_submit', 'RentalsController@review_submit');
        Route::get('securee/{value?}', 'RentalsController@securexecute');
        Route::any('buynights', 'UsersController@buy_nights');
        Route::any('nightCheckout', 'UsersController@checkout');

        Route::any('buynights-euro', 'UsersController@live_buy_nights');
        Route::any('nightCheckoutEuro', 'UsersController@live_checkout');
        //Route::get('paid_services', 'UserController@paid_services');
    });
});
//Admin BackEnd Routes
Route::get('admin', function () {
    return view('admin.login');
});
Route::post('adminlogin', 'RegisterController@adminLogin');
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function () {
    //Profile
    Route::get('profile', 'ProfileController@showProfile');
    Route::get('profile/password', 'ProfileController@showPassword');
    Route::post('profile/store', 'ProfileController@store');
    Route::post('password/change', 'ProfileController@changePassword');

    Route::get('view/user/{id}', 'UsersController@viewProfileInAdmin');
    Route::get('view/property/{id}', 'RentalsController@viewPropertyInAdmin');

    //Settings
    Route::get('settings/general', 'SettingsController@showGeneral');
    Route::get('settings/socialmedia', 'SettingsController@showSocialMedia');
    Route::get('settings/app', 'SettingsController@showSocialApp');
    Route::get('settings/email', 'SettingsController@showEmail');
    Route::get('settings/noimage', 'SettingsController@showNoImage');
    Route::get('settings/payment', 'SettingsController@showPayment');
    Route::get('settings/propertyattributes', 'SettingsController@showPropertyAttributes');
    //Settings Post URL
    Route::post('generalsettings', 'SettingsController@storeGeneral');
    Route::post('socialmediasettings', 'SettingsController@storeSocialmedia');
    Route::post('appsettings', 'SettingsController@storeApp');
    Route::post('emailsettings', 'SettingsController@storeEmail');
    Route::post('noimagesettings', 'SettingsController@storeNoimage');
    Route::post('paymentsettings', 'SettingsController@storePayment');
    Route::post('propertyattributes', 'SettingsController@storePropertyAttributes');
    Route::resource('roomtype', 'RoomTypeController');
    Route::resource('propertytype', 'PropertyTypeController');
    Route::resource('school', 'SchoolController');
    Route::resource('amenities', 'AmenityController');
    Route::resource('specialattributes', 'SpecialAttributesController');
    Route::resource('banner', 'BannerController');
    Route::resource('cms', 'CmsController');
    Route::resource('language', 'LanguageController');
    Route::resource('currency', 'CurrencyController');
    Route::resource('faq', 'FaqController');
    Route::resource('cancellation', 'CancellationController');
    Route::resource('destination', 'DestinationController');
    Route::resource('homedestination', 'HomeDestinationController');
    Route::resource('commission', 'CommissionController');
    Route::get('payment', 'PaymentController@index');
    Route::get('payment/{status}', 'PaymentController@getPaymentListByStatus');
    Route::get('reservation', 'ReservationController@index');
    Route::get('reservation/{status}', 'ReservationController@getListByStatus');
    Route::resource('role', 'RoleController');
    Route::get('commissiontrack', 'CommissionController@showCommissionTracking');
    Route::get('review', 'AdminRentalsController@showReview');
    Route::resource('permission', 'PermissionController');
    Route::get('property/create', 'AdminRentalsController@create');
    Route::get('property/manage', 'AdminRentalsController@manage');
    Route::get('property/managePaid', 'AdminRentalsController@managePaid');
    Route::get('property/manageDistanceHomeWork', 'AdminRentalsController@manageDistanceHomeWork'); // to get distance home -> work.
    Route::get('property/basic/{id}', 'AdminRentalsController@showBasic');
    Route::get('property/basicPaid/{id}', 'AdminRentalsController@showBasicPaid');
    Route::get('property/description/{id}', 'AdminRentalsController@showDescription');
    Route::get('property/descriptionPaid/{id}', 'AdminRentalsController@showDescriptionPaid');
    Route::get('property/amenities/{id}', 'AdminRentalsController@showAmenity');
    Route::get('property/photos/{id}', 'AdminRentalsController@showPhotos');
    Route::get('property/photosPaid/{id}', 'AdminRentalsController@showPhotosPaid');
    Route::get('property/location/{id}', 'AdminRentalsController@showLocation');
    Route::get('property/locationPaid/{id}', 'AdminRentalsController@showLocationPaid');
    Route::get('property/pricing/{id}', 'AdminRentalsController@showPricing');
    Route::get('property/booking/{id}', 'AdminRentalsController@showBooking');
    Route::get('property/bookingPaid/{id}', 'AdminRentalsController@showBookingPaid');
    Route::get('property/calendar/{id}', 'AdminRentalsController@showCalendar');
    Route::get('property/calendarPaid/{id}', 'AdminRentalsController@showCalendarPaid');
    Route::get('user/manage', 'UsersController@manage');
    Route::get('dashboard', 'AdminRentalsController@dashboard');
    Route::get('reservation/emailsend/{id}', 'ReservationController@sendEmail');


    //Export
    Route::get('payment/export/{status}', 'PaymentController@export');
    Route::get('reservation/export/{status}', 'ReservationController@export');
    Route::get('commisionexport', 'CommissionController@export');
    Route::get('propertyexport', 'AdminRentalsController@propertyexport');
    Route::get('groupexport', 'GroupManagementController@groupexport');
    Route::get('allstudentexport', 'GroupManagementController@allstudentexport');
    Route::get('userlimitedinfo', 'GroupManagementController@userlimitedinfo');
    Route::any('is_confirm_group', 'GroupManagementController@is_confirm_group_export');
    Route::get('workPlaceSchoolHouseExport', 'GroupManagementController@workPlaceSchoolHouseExport');
    Route::get('userexport/{roleid}', 'UsersController@userexport');
    // booking management
    Route::get('bookedJobs/{status?}', 'BookingController@index');
    Route::get('bookedJobs/cancelBooking/{id}', 'BookingController@cancelBooking');
    Route::post('bookedJobs/cancelBookingForm', 'BookingController@cancelBookingForm');

    Route::post('propertystatus', 'AdminRentalsController@changeStatus');
    Route::post('userstatus', 'UsersController@changeStatus');
    Route::post('reviewstatus', 'AdminRentalsController@reviewstatus');

    //Voucher Controller
    Route::resource('voucher', 'VoucherController');
    Route::resource('voucherCredit', 'VoucherCreditController', ['except' => 'show']);
    Route::get('voucherCredit/assignUser', 'VoucherCreditController@assignUser');
    Route::post('voucherCredit/addVoucher', 'VoucherCreditController@addVoucher');
    Route::get('voucherCredit/createforpreenroll', 'VoucherCreditController@createforpreenroll');
    Route::post('voucherCredit/storepreenrolled', 'VoucherCreditController@storepreenrolled');

    //Group Management Controller
    Route::get('groups/management', 'GroupManagementController@index');
    Route::get('booking/generator', 'BookingGeneratorController@index');
    Route::post('booking/create', 'BookingGeneratorController@create');
    Route::get('booking/paidgenerator', 'BookingGeneratorController@paidgenerator');
    Route::post('booking/createpaid', 'BookingGeneratorController@createpaid');
    Route::get('booking/generator/accept', 'BookingGeneratorController@accept');
    Route::post('booking/acceptcreate', 'BookingGeneratorController@acceptcreate');
    Route::post('booking/checkforcredit', 'BookingGeneratorController@check_for_credit');
    Route::get('testExcel', 'GroupManagementController@testExcel');
    Route::get('schoolHousesExcel', 'GroupManagementController@schoolHousesExcel');
    Route::get('newschoolHousesExcel', 'GroupManagementController@newschoolHousesExcel');
    Route::get('updateAddress', 'GroupManagementController@saveLatlong');
    Route::get('getAllusersListing', 'GroupManagementController@getAllusersListing');

    //Secondary School Groups
    Route::resource('schoolGroups', 'SecondarySchoolGroupsController', ['except' => 'show']);
    Route::get('schoolGroups/studentToGroup', 'SecondarySchoolGroupsController@studentToGroup');
    Route::post('schoolGroups/updateStudentToGroups', 'SecondarySchoolGroupsController@updateStudentToGroups');

    //Transferwise Excel shett list
    Route::get('paidlisting', 'TransferwiseListController@index');
    Route::get('exportTransferwiseSheet', 'TransferwiseListController@exportTransferwiseSheet');
    
    // user verifications
    Route::get("user/trust", "UsersController@verifications");
    Route::get("user/verifications", "UsersController@verifications");
    Route::get("user/verifiy_approval/{status}", "UsersController@verifiy_approval");
});



