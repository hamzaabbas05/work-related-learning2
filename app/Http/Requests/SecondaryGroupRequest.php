<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SecondaryGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_name' => "required",
            'group_date_in' => "required",
            'group_date_out' => "required",
//            'pick_up_where' => "required",
//            'drop_off_where' => "required",
//            'drop_off_where' => "required",
        ];
    }
}
