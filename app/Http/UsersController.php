<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\PropertyRepository;
use App\Repositories\RoomtypeRepository;
use Carbon\Carbon;
use App\MedMessage;
use App\School;
use App\Tutor;
use App\RentalsEnquiry;
use App\Services\Mailer\MailNotificationsForUser;

class UsersController extends Controller {

    protected $userRepository;
    protected $propertyRepository;
    protected $roomTypeRepository;
    private $mailNotificationsForUser;

    public function __construct(UserRepository $userRepository, PropertyRepository $propertyRepository, RoomtypeRepository $roomTypeRepository, MailNotificationsForUser $mailNotificationsForUser) {
        $this->userRepository = $userRepository;
        $this->propertyRepository = $propertyRepository;
        $this->roomTypeRepository = $roomTypeRepository;
        $this->mailNotificationsForUser = $mailNotificationsForUser;
    }

    public function sendcontactmessage(Request $request) {
        $dataArr = array(
            'productId' => $request->get('proid'),
            'bookingNo' => $request->get('bno'),
            'enqid' => $request->get('listingid'),
            'senderId' => $request->get('senderid'),
            'receiverId' => $request->get('receiverid'),
            'subject' => '',
            'message' => $request->get('messages')
        );
        MedMessage::create($dataArr);
        $receiverObj = $this->userRepository->getById($request->get('receiverid'));
        $proObj = $this->propertyRepository->getById($request->get('proid'));
        $enquiryObj = RentalsEnquiry::where('id', $request->get('listingid'))->first();

        $this->mailNotificationsForUser->sendemailmessage($receiverObj, $proObj, $enquiryObj);
        return "success";
    }

    public function throwError() {
        flash('Trying to View Others Conversation is restricted', 'success');
        return \Redirect('user/messages');
    }

    public function conversation($enqId) {
        $enquiryObj = RentalsEnquiry::where('id', $enqId)->first();
        if (\Gate::denies('viewconversation', $enquiryObj)) {
            return $this->throwError();
        }
        MedMessage::changeReadStatus(\Auth::user()->id);
        $conversation = MedMessage::getConversations($enqId);

        return view('conversation', compact('conversation', 'enqId', 'enquiryObj'));
    }

    public function manage() {
        $bredCrum = "User Management";
        $userObj = $this->userRepository->getAll();
        return View('admin.user.viewprofile', compact('bredCrum', 'userObj'));
    }

    public function showMessages() {
        //MedMessage::changeReadStatus(\Auth::user()->id);

        $Obj = RentalsEnquiry::where('renter_id', \Auth::user()->id)->orWhere('user_id', \Auth::user()->id)->orderBy('dateAdded', 'desc')->get();
        //$Obj = MedMessage::getReceivedMessages(\Auth::user()->id);
        return View('message', compact('messageObj', 'Obj'));
    }

    public function getMessageType(Request $request) {
        $type = $request->get('msgtype');
        if ($type == "inbox") {
            $Obj = MedMessage::getReceivedMessages(\Auth::user()->id);
        } elseif ($type == "sent") {
            $Obj = MedMessage::getSentMessages(\Auth::user()->id);
        } elseif ($type == "admin") {
            $Obj = MedMessage::getAdminMessages(\Auth::user()->id);
        }
        $view = View('messageajax', compact('Obj', 'type'));
        return $view;
    }

    public function userexport() {
        $filename = "user" . date('Y-m-d H:i:s');
        \Excel::create($filename, function($excel) {
            $sheetName = "user";
            $excel->sheet($sheetName, function($sheet) {
                $userObj = $this->userRepository->getAll();
                $sheet->loadView('admin.export.user')->with('userObj', $userObj);
            });
        })->download('xls');
    }

    public function changeStatus(Request $request) {
        $updateId = $request->get('id');
        $status = $request->get('status');
        $updateData = array('status' => $status);
        $this->userRepository->update($updateId, $updateData);
        $this->propertyRepository->updateStatusByUserId($updateId, $updateData);
        return "success";
    }

    public function editProfile() {
        $userObject = $this->userRepository->getLoginUser();
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        $roleId = 0;
        if ($userObject->hasRole('Organization')) {
            $roleId = 4;
        } elseif ($userObject->hasRole('Parent')) {
            $roleId = 5;
        } elseif ($userObject->hasRole('Student')) {
            $roleId = 6;
        }
        $schools = School::where('status', 1)->get();
        return View('user.editprofile', compact('userObject', 'schools', 'roomTypes', 'roleId', 'tutorRelation'));
    }

    public function evaluate() {
        return View('user.evoluation');
    }
    
    public function update_profile(Request $request) {
        echo "yes i am here";
        exit;
    }

    public function profile_submit(Request $request) {
        /* Creating Roles */
        $userObject = $this->userRepository->getLoginUser();
        $check = 0;
        /* $userwithroles = $this->userRepository->getUserRoles(\Auth::user()->id);
          $rolesarray = array();
          foreach($userwithroles->roles as $rs)  {
          array_push($rolesarray, $rs->id);
          } */
        if ($userObject->hasRole('Student') && $request->get('role') == 6) {
            $check = 1;
        }
        if ($userObject->hasRole('Organization') && $request->get('role') == 4) {
            $check = 1;
        }
        if ($userObject->hasRole('Parent') && $request->get('role') == 5) {
            $check = 1;
        }
        if ($check != 1 && $request->get('role') != 0) {
            $roleId = $request->get('role');
            $roles = array($roleId);
            $userObject->roles()->attach($roles);
            $userObject->save();
        }
        $roleDependsFormData = array();
        $dob = $request->get('bday') . "/" . $request->get('bmonth') . "/" . $request->get('byear');
        $basicinfo = array('name' => $request->get('name'),
            'lastname' => $request->get('lastname'),
            'dob' => $dob,
            'gender' => $request->get('gender'),
            'basicpob' => $request->get('basicpob'),
            'country_id' => $request->get('country_id'),
            'phonenumber' => $request->get('phonenumber'),
            'NIN18' => $request->get('NIN18'),
            'emergency_contact_name' => $request->get('emergency_contact_name'),
            'emergency_contact_phone' => $request->get('emergency_contact_phone'),
            'emergency_contact_email' => $request->get('emergency_contact_email'),
            'emergency_contact_relationship' => $request->get('emergency_contact_relationship')
        );
        if ($request->get('role') == 4) {
            $roleDependsFormData = array('orgname' => $request->get('orgname'),
                'aboutorg' => $request->get('aboutorg'),
                'legal_nature' => $request->get('legal_nature'),
                'orghr' => $request->get('orghr'),
                'orgemail' => $request->get('orgemail'),
                'orgphone' => $request->get('orgphone'),
                'raddress18' => $request->get('orgaddress'),
                'street_numberr18' => $request->get('street_numberorg'),
                'router18' => $request->get('routeorg'),
                'localityr18' => $request->get('localityorg'),
                'administrative_area_level_1r18' => $request->get('administrative_area_level_1org'),
                'countryr18' => $request->get('countryorg'),
                'postal_coder18' => $request->get('postal_codeorg'),
                'why_fit' => $request->get('why_fit'),
                'tutorrelation' => $request->get('tutorrelation'),
                'org_lat' => $request->get('orglatbox'),
                'org_lan' => $request->get('orglonbox'),
            );
            $datalatlon = array(
                'legal_nature' => $request->get('legal_nature'),
                'room_type' => $request->get('orghr'),
                'title' => $request->get('orgname'),
                'represent_name' => $request->get('name'),
                'represent_surname' => $request->get('lastname'),
                'represent_born' => $request->get('basicpob'),
                'represent_email' => \Auth::user()->email,
                'represent_phone' => $request->get('phonenumber'),
                'represent_tax_no' => $request->get('NIN18'),
                'work_tutor_fit' => $request->get('why_fit'),
                'relation_with_company' => $request->get('tutorrelation'),
                'pro_lat' => $request->get('orglatbox'),
                'pro_lon' => $request->get('orglonbox')
            );

            $this->propertyRepository->updateaddressinproperty(\Auth::user()->id, $datalatlon);
        }
        if ($request->get('role') == 6) {

            $student18Data = array(
                'sname16' => $request->get('name'),
                'ssurname16' => $request->get('lastname'),
                'semail16' => \Auth::user()->email,
                'sphone16' => $request->get('sphone16'),
                'dob16' => $dob,
                'pob16' => $request->get('basicpob'),
                'NIN16' => $request->get('NIN18'),
                'about_yourself' => $request->get('about_yourself'),
                'school_name' => $request->get('school_name'),
                'class_letter' => $request->get('class_letter'),
                'class_number' => $request->get('class_number'),
                'student_number' => $request->get('student_number'),
                'medical_learning' => $request->get('medical_learning'),
                'host_family_notes' => $request->get('family_notes18'),
                'work_tutor_notes' => $request->get('tutor_notes18'),
                'academic_qualifications' => $request->get('academic_qualifications'),
                'date_of_attainment' => $request->get('date_of_attainment'),
                'school_tutor_name' => $request->get('school_tutor_name'),
                'school_tutor_email' => $request->get('school_tutor_email'),
                'school_tutor_number' => $request->get('school_tutor_number'),
                'raddress18' => $request->get('over18raddress'),
                'street_numberr18' => $request->get('street_numbersr18'),
                'router18' => $request->get('routesr18'),
                'localityr18' => $request->get('localitysr18'),
                'administrative_area_level_1r18' => $request->get('administrative_area_level_1sr18'),
                'countryr18' => $request->get('countrysr18'),
                'postal_coder18' => $request->get('postal_codesr18'),
                'raddress16' => $request->get('over18raddress'),
                'street_numberr16' => $request->get('street_numbersr18'),
                'router16' => $request->get('routesr18'),
                'localityr16' => $request->get('localitysr18'),
                'administrative_area_level_1r16' => $request->get('administrative_area_level_1sr18'),
                'countryr16' => $request->get('countrysr18'),
                'postal_coder16' => $request->get('postal_codesr18'),
            );

            /* Student18 Domicilary Address */
            if ($request->get('s_res_dom_same') == 'on') {
                $s_res_dom_same = 0;
                $sdomicileaddress = array('daddress18' => $request->get('s18domicileaddress'),
                    'street_numberd18' => $request->get('street_numbersd18'),
                    'routed18' => $request->get('routesd18'),
                    'localityd18' => $request->get('localitysd18'),
                    'administrative_area_level_1d18' => $request->get('administrative_area_level_1sd18'),
                    'countryd18' => $request->get('countrysd18'),
                    'postal_coded18' => $request->get('postal_codesd18'),
                    's_res_dom_same' => $s_res_dom_same,
                    'daddress16' => $request->get('s18domicileaddress'),
                    'street_numberd16' => $request->get('street_numbersd18'),
                    'routed16' => $request->get('routesd18'),
                    'localityd16' => $request->get('localitysd18'),
                    'administrative_area_level_1d16' => $request->get('administrative_area_level_1sd18'),
                    'countryd16' => $request->get('countrysd18'),
                    'postal_coded16' => $request->get('postal_codesd18'),
                    's16_res_dom_same' => $s_res_dom_same
                );
            } else {
                $sdomicileaddress = array('s_res_dom_same' => 1, 's16_res_dom_same' => 1);
            }

            $roleDependsFormData = array_merge($student18Data, $sdomicileaddress);
        }
        if ($request->get('role') == 5) {
            $dob16 = $request->get('bday16') . "/" . $request->get('bmonth16') . "/" . $request->get('byear16');
            $allowcheck = 0;
            if ($request->get('allowcheck') == 'on') {
                $allowcheck = 1;
            }
            $student16Data = array('sname16' => $request->get('sname16'),
                'ssurname16' => $request->get('ssurname16'),
                'semail16' => $request->get('semail16'),
                'sphone16' => $request->get('sphone16'),
                'dob16' => $dob16,
                'pob16' => $request->get('pob16'),
                'school_name' => $request->get('school_name16'),
                'class_letter' => $request->get('class_letter16'),
                'class_number' => $request->get('class_number16'),
                'student_number' => $request->get('student_number16'),
                'medical_learning' => $request->get('medical_learning16'),
                'academic_qualifications' => $request->get('academic_qualifications16'),
                'date_of_attainment' => $request->get('date_of_attainment16'),
                'school_tutor_name' => $request->get('school_tutor_name16'),
                'school_tutor_email' => $request->get('school_tutor_email16'),
                'school_tutor_number' => $request->get('school_tutor_number16'),
                'NIN16' => $request->get('NIN16'),
                'about_yourself' => $request->get('profile-about16'),
                'raddress16' => $request->get('sraddress16'),
                'street_numberr16' => $request->get('street_numbersr16'),
                'router16' => $request->get('routesr16'),
                'localityr16' => $request->get('localitysr16'),
                'administrative_area_level_1r16' => $request->get('administrative_area_level_1sr16'),
                'countryr16' => $request->get('countrysr16'),
                'postal_coder16' => $request->get('postal_codesr16'),
            );
            /* Student16 Domicilary Address */
            if ($request->get('s16_res_dom_same') == 'on') {
                $s16_res_dom_same = 0;
                $sdomicileaddress = array('daddress16' => $request->get('daddress16'),
                    'street_numberd16' => $request->get('street_numberd16'),
                    'routed16' => $request->get('routed16'),
                    'localityd16' => $request->get('localityd16'),
                    'administrative_area_level_1d16' => $request->get('administrative_area_level_1d16'),
                    'countryd16' => $request->get('countryd16'),
                    'postal_coded16' => $request->get('postal_coded16'),
                    's16_res_dom_same' => $s16_res_dom_same
                );
            } else {
                $sdomicileaddress = array('s16_res_dom_same' => 1);
            }


            $studentFinalData = array_merge($student16Data, $sdomicileaddress);



            $parentData = array(
                'student_relationship' => $request->get('student_relationship'),
                'host_family_notes' => $request->get('host_family_notes'),
                'work_tutor_notes' => $request->get('work_tutor_notes'),
                'check_allow' => $allowcheck,
                'raddress18' => $request->get('parentaddress'),
                'street_numberr18' => $request->get('street_numberp'),
                'router18' => $request->get('routep'),
                'localityr18' => $request->get('localityp'),
                'administrative_area_level_1r18' => $request->get('administrative_area_level_1p'),
                'countryr18' => $request->get('countryp'),
                'postal_coder18' => $request->get('postal_codep'),
            );

            /* Domicile Address */
            if ($request->get('p_res_dom_same') == 'on') {
                $s_res_dom_same = 0;
                $pdomicileaddress = array('daddress18' => $request->get('pdomicileaddress'),
                    'street_numberd18' => $request->get('street_numberdp'),
                    'routed18' => $request->get('routedp'),
                    'localityd18' => $request->get('localitydp'),
                    'administrative_area_level_1d18' => $request->get('administrative_area_level_1dp'),
                    'countryd18' => $request->get('countrydp'),
                    'postal_coded18' => $request->get('postal_codedp'),
                    's_res_dom_same' => $s_res_dom_same
                );
            } else {
                $pdomicileaddress = array('s_res_dom_same' => 1);
            }

            $parentfinalarray = array_merge($parentData, $pdomicileaddress);

            $roleDependsFormData = array_merge($studentFinalData, $parentfinalarray);
        }
        $postData = array_merge($basicinfo, $roleDependsFormData);
        $user = $this->userRepository->updateUser($postData);
        flash('Profile Updated Successfully', 'success');
        return \Redirect::to('user/edit');




        /* $dob = $request->get('bday')."/".$request->get('bmonth')."/".$request->get('byear');
          $dob16 = $request->get('bday16')."/".$request->get('bmonth16')."/".$request->get('byear16');
          $checkallow =1;
          if ($request->get('check_allow') == "off"){
          $checkallow =0;
          }
          $postData = array('name' => $request->get('name'),
          'lastname' => $request->get('lastname'),
          'dob' => $dob,
          'gender' => $request->get('gender'),
          'paypalid' => $request->get('paypalid'),
          'about_yourself' => $request->get('about_yourself'),
          'emergency_contact_name' => $request->get('emergency_contact_name'),
          'emergency_contact_phone' => $request->get('emergency_contact_phone'),
          'emergency_contact_email' => $request->get('emergency_contact_email'),
          'emergency_contact_relationship' => $request->get('emergency_contact_relationship'),
          'country_id' => $request->get('country_id'),
          'phonenumber' => $request->get('phonenumber'),
          'student_relationship' => $request->get('student_relationship'),
          'NIN18' => $request->get('NIN18'),
          'daddress18' => $request->get('daddress18'),
          'street_numberd18' => $request->get('street_numberd18'),
          'routed18' => $request->get('routed18'),
          'administrative_area_level_1d18' => $request->get('administrative_area_level_1d18'),
          'localityd18' => $request->get('localityd18'),
          'postal_coded18' => $request->get('postal_coded18'),
          'countryd18' => $request->get('countryd18'),
          'daddress16' => $request->get('daddress16'),
          'street_numberd16' => $request->get('street_numberd16'),
          'routed16' => $request->get('routed16'),
          'administrative_area_level_1d16' => $request->get('administrative_area_level_1d16'),
          'localityd16' => $request->get('localityd16'),
          'postal_coded16' => $request->get('postal_coded16'),
          'countryd16' => $request->get('countryd16'),
          'raddress16' => $request->get('raddress16'),
          'street_numberr16' => $request->get('street_numberr16'),
          'router16' => $request->get('router16'),
          'administrative_area_level_1r16' => $request->get('administrative_area_level_1r16'),
          'postal_coder16' => $request->get('postal_coder16'),
          'countryr16' => $request->get('countryr16'),
          'localityr16' => $request->get('localityr16'),
          'dob16' => $dob16,
          'pob16' => $request->get('pob16'),
          'medical_learning' => $request->get('medical_learning'),
          'host_family_notes' => $request->get('host_family_notes'),
          'work_tutor_notes' => $request->get('work_tutor_notes'),
          'NIN16' => $request->get('NIN16'),
          'sname16' => $request->get('sname16'),
          'ssurname16' => $request->get('ssurname16'),
          'sphone16' => $request->get('sphone16'),
          'semail16' => $request->get('semail16'),
          'school_name' => $request->get('school_name'),
          'class_letter' => $request->get('class_letter'),
          'class_number' => $request->get('class_number'),
          'student_number' => $request->get('student_number'),
          'academic_qualifications' => $request->get('academic_qualifications'),
          'date_of_attainment' => $request->get('date_of_attainment'),
          'school_tutor_name' => $request->get('school_tutor_name'),
          'school_tutor_email' => $request->get('school_tutor_email'),
          'school_tutor_number' => $request->get('school_tutor_number'),
          'check_allow' => $checkallow,
          );
          $validator = Validator::make($postData, $this->userRepository->editValidationRules(\Auth::user()->id));
          if ($validator->fails()) {
          return \Redirect('user/edit')->withErrors($validator);
          }
          $user = $this->userRepository->updateUser($postData);
          flash('Profile Updated Successfully', 'success');
          return \Redirect::to('user/edit'); */
    }

    public function image_submit(Request $request) {
        if ($request->hasFile('profile_img_name')) {
            $file = $request->file('profile_img_name');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp . '.' . $file->getClientOriginalExtension();
            //$image->filePath = $img_name;
            $file->move(public_path() . '/images/profile', $img_name);
            $postData = array('profile_img_name' => $img_name);
            $user = $this->userRepository->updateUser($postData);
            flash('Profile Image Updated Successfully', 'success');
            return \Redirect::to('user/dashboard');
        }
    }

    public function changepassword() {
        return View('user.account_password');
    }

    public function resetpassword(Request $request) {
        $rules = array(
            'password' => 'required',
            'password_confirm' => 'required|same:password');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect('user/changepassword')->withErrors($validator);
        } else {
            $formData = array('password' => \Hash::make($request->get('password')));
            $this->userRepository->updateUser($formData);
            flash('Password Updated Successfully', 'success');
            return \Redirect('user/changepassword');
        }
    }

    public function showDashboard() {
        return View('user.dashboard');
    }

    public function viewProfileInAdmin($id) {
        $userobject = $this->userRepository->getById($id);
        return View('admin.user.viewprofile', compact('userobject'));
    }

    public function viewProfile($id = null) {
        if (!\Auth::check() && is_null($id)) {
            flash('Not Valid URL', 'success');
            return \Redirect('signin');
        }
        if (\Auth::check() && is_null($id)) {
            $userobj = $this->userRepository->getById(\Auth::user()->id);
            return View('user.viewprofile', compact('userobj'));
        }
        $userobj = $this->userRepository->getById($id);
        return View('user.viewprofile', compact('userobj'));
    }

    public function viewTrust() {
        return View('user.trustverify');
    }

    // Cron Jobs for users
    public function PromptSendUnpaid(){
        echo "Setup completed please go ahead"; 
    }

}
