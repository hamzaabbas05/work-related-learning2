<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Mailer\MailNotificationsForUser;
use App\User;
use DateTime;
use Session;

class RegisterController extends Controller {

    /**
     * Instances of Admin Repository
     */
    protected $userRepository;
    private $mailNotificationsForUser;

    /**
     * Access all methods and objects in Repository
     */
    public function __construct(UserRepository $userRepository, MailNotificationsForUser $mailNotificationsForUser) {

        $this->userRepository = $userRepository;
        $this->mailNotificationsForUser = $mailNotificationsForUser;
    }

    public function testmail($email) {
        $this->mailNotificationsForUser->sendtestmail($email);
    }

    public function validatedata(Request $request) {
        $data = array(
            'email' => $request->get('email')
        );
        $validator = Validator::make($data, [
                    'email' => 'required|email|unique:users'
        ]);
        if ($validator->fails()) {
            return "exists";
        }
        return "success";
    }

    public function register(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, [
                    'g-recaptcha-response' => 'required|recaptcha',
//                    'voucher_code' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->to('signup/')->with('errors', $validator->errors())->withInput();
            exit;
        }

        // check voucher code 
//        $voucher_code = $request->get("voucher_code");
//        $voucher_id = "0";
//        $check_voucher = \App\Voucher::where("voucher_code", $voucher_code)->first();
//        if (!isset($check_voucher->id) or $check_voucher->id <= 0) {
//            return redirect()->to('signup/')->with('error', 'Your Voucher Code is not Valid!')->withInput();
//            exit;
//        } else {
//            $check_user_code = User::where("voucher_id", $check_voucher->id)->first();
//            if (isset($check_user_code->id) && $check_user_code->id > 0) {
//                return redirect()->to('signup/')->with('error', 'Your Voucher Code is already used!')->withInput();
//                exit;
//            }
//        }
//        $voucher_id = $check_voucher->id;
        // checking voucher credit code
        $no_voucher = $request->get("no_voucher_code");

        // die("End Here");
        if (!empty($no_voucher) && $no_voucher == "1") {
            $voucher_interest = $request->get("voucher_interest");
            if (!empty($voucher_interest)) {
                $voucher_id = "0";
                $voucherId =  '';//$get_voucher->id;
                // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){

                    // $check_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first(); //wrong spelling here in condition
                    // $voucher_id = $check_voucher->id;
               
/*                    if (isset($get_voucher->id)) {
                        // $user_voucher->vouchercredit_id = $get_voucher->id;
                        // $user_voucher->save();
                        $userWallet = new \App\UserWallet();
                        $userWallet->user_id = $userId;
                        $userWallet->vouchaercredits_id = $get_voucher->id;
                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                        $userWallet->creadit_type = "voucher";
                        $userWallet->save();
                        $allowedEnquiryCount = $get_voucher->send_to_unpaid;
                    }*/
                // }                

               /* $check_voucher = \App\VoucherCredit::where("voucher_code", $voucher_interest)->first();
                if (!isset($check_voucher->id) or $check_voucher->id <= 0) {
                    return redirect()->to('signup/')->with('error', 'Your Voucher Code is not Valid!')->withInput();
                    exit;
                } else {
                    $check_user_code = User::where("vouchercredit_id", $check_voucher->id)->first();
                    echo "<pre>";
                        print_r($check_user_code);
                    echo "</pre>";
                    die();
                    if (isset($check_user_code->id) && $check_user_code->id > 0) {
                        return redirect()->to('signup/')->with('error', 'Your Voucher Code is already used!')->withInput();
                        exit;
                    }
                }
                $voucher_id = $check_voucher->id;*/
            } else {
                return redirect()->to('signup/')->with('error', 'You must select your interest')->withInput();
                exit;
            }
        } else {
            $voucher_code = $request->get("voucher_code");
            $voucher_id = "0";
            $check_voucher = \App\VoucherCredit::where("voucher_code", $voucher_code)->first();
            if (!isset($check_voucher->id) or $check_voucher->id <= 0) {
                return redirect()->to('signup/')->with('error', 'Your Voucher Code is not Valid!')->withInput();
                exit;
            } else {
                $check_user_code = User::where("vouchercredit_id", $check_voucher->id)->first();
                if (isset($check_user_code->id) && $check_user_code->id > 0) {
                    return redirect()->to('signup/')->with('error', 'Your Voucher Code is already used!')->withInput();
                    exit;
                }
            }
            $voucher_id = $check_voucher->id;
        }


        $dob = $request->get('bday') . "/" . $request->get('bmonth') . "/" . $request->get('byear');
        $new_dob = $request->get('byear') . "-" . $request->get('bmonth') . "-" . $request->get('bday');
        //echo "original dob = " . $new_dob . "<br />";

        $dob_converted = date("Y-m-d", strtotime($new_dob));
        //echo "converted dob = " . $dob_converted . "<br />";

        $check_date = date("Y-m-d", strtotime('-18 years'));
        //echo "current date - 18 years = " . $check_date . "<br />";

        if ($dob_converted > $check_date) {
            //echo "You must be over 18 to use this site, your parents must sign you up if you are underage<br />";
            return redirect()->to('signup/')->with('error', "You must be over 18 to use this site, your parents must sign you up if you are underage")->withInput();
            exit;
        } else {
            //echo "<br />you are above 18 years old";
            //exit;
            $confirmation_code = str_random(30);
            $postData = array('name' => $request->get('firstname'),
                'lastname' => $request->get('lastname'),
                'gender'    =>  $request->get('gender'),
                'country_id'    =>  $request->get('country_id'),
                'email' => $request->get('email'),
                'password' => \Hash::make($request->get('password')),
                'dob' => $dob,
                // 'vouchercredit_id'  =>  
                'confirmation_code' => $confirmation_code,
//                'voucher_id' => $voucher_id,
                'vouchercredit_id' => $voucher_id,
                'phonenumber'   => $request->get('phonenumber'),
            );

            $user = $this->userRepository->createUser($postData);

            $user_extrainfo = new \App\UserExtraInfo();
            $user_extrainfo->user_id = $user->id;
            $user_extrainfo->native_language_speaker = $request->get("native_language_speaker");
            $user_extrainfo->save();

            $userWallet = new \App\UserWallet();
            $userWallet->user_id = $user->id;
            $userWallet->vouchaercredits_id = $voucher_id;
            $userWallet->amount = isset($check_voucher->amount) ? $check_voucher->amount : "0";
            $userWallet->send_to_unpaid = isset($check_voucher->send_to_unpaid) ? $check_voucher->send_to_unpaid : "0";
            $userWallet->book_unpaid = isset($check_voucher->book_unpaid) ? $check_voucher->book_unpaid : "0";
            $userWallet->send_to_schoolhouse = isset($check_voucher->send_to_schoolhouse) ? $check_voucher->send_to_schoolhouse : "0";
            $userWallet->book_paid = isset($check_voucher->book_paid) ? $check_voucher->book_paid : "0";
            $userWallet->creadit_type = "voucher";
            $userWallet->save();
            $encryptValue = $confirmation_code . "," . $request->get('email');
            $this->mailNotificationsForUser->sendConfirmationEmail($encryptValue, $user);
            if(isset($check_voucher->voucher_code)) {
                // send email notification for new school house
                if($check_voucher->voucher_code == "NewSchoolHouse") {
                    $this->mailNotificationsForUser->sendVoucherNewSchoolHouseEmail($check_voucher->voucher_code, $user);
                }
                // send email notification for new work tutor
                else if($check_voucher->voucher_code == "NewWorkTutor") {
                    $this->mailNotificationsForUser->sendVoucherNewWorkTutorEmail($check_voucher->voucher_code, $user);
                }
                // send email notification for new parent interest
                else if($check_voucher->voucher_code == "NewParentInterested") {
                    $this->mailNotificationsForUser->sendVoucherNewParentInteretedEmail($check_voucher->voucher_code, $user);
                }
            }
            flash('Registered successfully.Please confirm your EmailAddress', 'success');
            //$this->doLogin($request->get('email'), $request->get('password')); 
            $isLoggedin = $this->doLogin($request->get('email'), $request->get('password'));
            if($isLoggedin){ //this is where we define the variable voucher_interest
                if (!isset($voucher_interest))
                    $voucher_interest = "";
                if($voucher_interest == "NewSchoolHouse"){

                    return \Redirect::to('user/workExperience');

                }else if($voucher_interest == "NewWorkTutor"){
                    // return \Redirect::to('user/paidServices');
                    return \Redirect::to('user/workExperience');

                }elseif($voucher_interest == "NewParentInterested"){

                    // return \Redirect::to('user/parent');
                    return \Redirect::to('user/preEnrolment');
                }else{
                    return \Redirect::to('user/edit');                    
                }
                

            }else{
                return \Redirect::to('signin');
                
            }
            // return \Redirect::to('signin');
        }
    }


    public function doLogin($email, $password) {
        if (\Auth::attempt(['email' => $email, 'password' => $password],true)) {
            return true;
        }
        return false;
    }

    public function loginvalidate(Request $request) {
        $email = $request->get('email');
        $password = $request->get('password');
        if ($this->doLogin($request->get('email'), $request->get('password'))) {
            return "success";
        }
        return "error";
    }

    public function login(Request $request) {

        $email = $request->get('email');
        $password = $request->get('password');
		
        if ($this->doLogin($request->get('email'), $request->get('password'))) {
			Session::put('user', \Auth::user());  
            $previous_url = !empty($request->get("previous_url")) ? $request->get("previous_url") : "/";
            return \Redirect::to($previous_url);
        }
        return \Redirect::to('/');
    }

    public function adminLogin(Request $request) {
        $data = array('email' => $request->get('email'), 'password' => $request->get('password'));
        $validator = Validator::make($data, ['email' => 'required|email', 'password' => 'required']);
        if ($validator->fails()) {
            return \Redirect('admin')->withErrors($validator);
        } else {
            if (!$this->doLogin($request->get('email'), $request->get('password'))) {
                flash('Admin Credentials Wrong', 'danger');
                return \Redirect('admin');
            }
            flash('Logged In Successfully', 'success');
            return \Redirect('admin/dashboard');
        }
    }

    public function doLogout() {
        \Auth::logout();
        flash('Logged out successfully', 'success');
        return \Redirect::to('/');
    }

    public function confirmEmail($code) {
        if (!$code) {
            flash('Invalid Confirmation Code', 'success');
            return \Redirect('signin');
        }
        if (strpos($code, ',') !== false) {
            $arr = explode(",", $code);
            $user = User::where('email', $arr[1])->first();

            if (is_null($user->confirmation_code)) {
                flash('Email Already Confirmed', 'success');
                return \Redirect('signin');
            }
            $user = User::where('confirmation_code', $arr[0])->first();
            if (!$user) {
                flash('Invalid User', 'success');
                return \Redirect('signin');
            }
            $user->email_confirmed = 1;
            $user->confirmation_code = null;
            $user->save();
            flash('Email Confirmed Successfully.Login to your account', 'success');
            return \Redirect('signin');
        }
        flash('Email Already Confirmed', 'success');
        return \Redirect('signin');
    }

}
