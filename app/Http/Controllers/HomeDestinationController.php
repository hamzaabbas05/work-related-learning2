<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\HomeDestinationRepository;
use Carbon\Carbon;

class HomeDestinationController extends Controller
{
    private $bredCrum = "Home Space Banners";
    
    protected $destinationRepository;

    public function __construct(HomeDestinationRepository $destinationRepository)
    {
        $this->destinationRepository = $destinationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->destinationRepository->getAll();
        return View('admin.homebanners.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        return View('admin.homebanners.create', compact('bredCrum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('name' => $request->get('name'),
                        'display_name' => $request->get('display_name'),
                        'lattitude' => $request->get('lattitude'),
                        'longitude' => $request->get('longitude'),
                        'status' => $request->get('status'),
                        'img_name' => $request->file('img_name'),
                    );
        $validator = Validator::make($postData, $this->destinationRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/homedestination/create')->withErrors($validator);
        } else {
            $file = $request->file('img_name');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp. '.' .$file->getClientOriginalExtension();
            //$image->filePath = $img_name;
            $file->move(public_path().'/images/destination', $img_name);
            $formData = array('name' => $request->get('name'),
                        'display_name' => $request->get('display_name'),
                        'lattitude' => $request->get('lattitude'),
                        'longitude' => $request->get('longitude'),
                        'status' => $request->get('status'),
                        'img_name' => $img_name,
                    );
            $Obj = $this->destinationRepository->create($formData);
            flash('Destination Added Successfully', 'success');
            return \Redirect('admin/homedestination');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->destinationRepository->getById($id);
        return View('admin.homebanners.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = array('name' => $request->get('name'),
                        'display_name' => $request->get('display_name'),
                        'lattitude' => $request->get('lattitude'),
                        'longitude' => $request->get('longitude'),
                        'status' => $request->get('status')
                    );
        $validator = Validator::make($postData, $this->destinationRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/homedestination/'.$id.'/edit')->withErrors($validator);
        } else {
            if ($request->hasFile('img_name')) {
                $file = $request->file('img_name');
                //getting timestamp
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $img_name = $timestamp. '.' .$file->getClientOriginalExtension();
                //$image->filePath = $img_name;
                $file->move(public_path().'/images/destination', $img_name);
                $postData = array_merge($postData, array('img_name' => $img_name));
            }
            $Obj = $this->destinationRepository->update($postData, $id);
            flash('Destination Updated Successfully', 'success');
            return \Redirect('admin/homedestination');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
