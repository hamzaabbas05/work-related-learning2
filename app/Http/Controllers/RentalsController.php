<?php
    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\Http\Requests;
    use App\Http\Controllers\Controller;
    use App\Repositories\PropertytypeRepository;
    use App\Repositories\RoomtypeRepository;
    use App\Repositories\PropertyRepository;
    use App\Repositories\AmenityRepository;
    use App\Repositories\SpecialAttributesRepository;
    use App\Services\Constants\AppConstants;
    use App\Repositories\CancellationRepository;
    use App\Repositories\CommissionRepository;
    use App\Repositories\UserRepository;
    use App\Repositories\CmsRepository;
    use App\PropertyImages;
    use App\GuestRoomImages;
    use App\RoomCalendar;
    use App\PropertySettings;
    use App\Property;
    use App\Wishlist;
    use Carbon\Carbon;
    use App\Booking;
    use App\RentalsEnquiry;
    use App\Review;
    use App\MedMessage;
    use App\Amenity;
    use App\Schedule;
    use App\Tutor;
    use App\Services\Mailer\MailNotificationsForUser;
    use Mail;
    use App\User;
    use Dompdf\Dompdf as Dompdf;
    use PDF;
    use DB;
	

    class RentalsController extends Controller {

        protected $propertyTypeRepository;
        protected $roomTypeRepository;
        protected $propertyRepository;
        protected $amenityRepository;
        protected $specialAttributesRepository;
        protected $cancellationRepository;
        protected $userRepository;
        protected $commissionRepository;
        protected $cmsRepository;
        private $mailNotificationsForUser;

        public function __construct(PropertytypeRepository $propertyTypeRepository, RoomtypeRepository $roomTypeRepository, PropertyRepository $propertyRepository, AmenityRepository $amenityRepository, SpecialAttributesRepository $specialAttributesRepository, CancellationRepository $cancellationRepository, UserRepository $userRepository, CommissionRepository $commissionRepository, CmsRepository $cmsRepository, MailNotificationsForUser $mailNotificationsForUser) {
            $this->propertyTypeRepository = $propertyTypeRepository;
            $this->roomTypeRepository = $roomTypeRepository;
            $this->propertyRepository = $propertyRepository;
            $this->amenityRepository = $amenityRepository;
            $this->specialAttributesRepository = $specialAttributesRepository;
            $this->cancellationRepository = $cancellationRepository;
            $this->userRepository = $userRepository;
            $this->commissionRepository = $commissionRepository;
            $this->cmsRepository = $cmsRepository;
            $this->mailNotificationsForUser = $mailNotificationsForUser;
        }

        public function viewPropertyInAdmin($id) {
            $propertyObj = $this->propertyRepository->getById($id);
            $bredCrum = "View Work Experience";
            return View('admin.property.viewproperty', compact('bredCrum', 'propertyObj'));
        }

        public function add() {
            //if (\Auth::user()->hasRole('Organization')) {
            $propertyTypes = $this->propertyTypeRepository->getAll(true);
            $roomTypes = $this->roomTypeRepository->getAll(true);
            $tutorRelation = Tutor::all();
            $userObj = User::where("id", \Auth::user()->id)->first();
    //            return View('rentals.addspace', compact('propertyTypes', 'roomTypes', 'tutorRelation'));
            return View('rentals.kindoflist', compact('propertyTypes', 'roomTypes', 'tutorRelation', 'userObj'));
            //}
            //flash('please fill organization information before you add work experience', 'success');
            //return \Redirect::to('user/edit');
        }

        public function addUnpaid() {
            if (\Auth::user()->hasRole('Organization')) {
                //$propertyTypes = $this->propertyTypeRepository->getAll(true);
                $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'unpaid'])->get();
                $roomTypes = $this->roomTypeRepository->getAll(true);
                $tutorRelation = Tutor::all();
                return View('rentals.addspace', compact('propertyTypes', 'roomTypes', 'tutorRelation'));
            }
            flash('Please fill in organization information before adding work experience Listing', 'success');
            return \Redirect::to('user/edit');
        }

        public function deleteListing($id=''){
            // return 'success';
            // echo "Deleted <pre>";
            //     print_r($_SERVER);
            //     echo "</pre>";
            // echo "<pre>";
            //     print_r($_REQUEST);
            // echo "</pre>";
                // flash('Listing deleted successfully.', 'success');
            //     echo "<script>";
            //     echo " alert('Listing deleted successfully.');      
            //             window.location.href='".$_SERVER['HTTP_REFERER']."';
            //     </script>";
                // return 'success';
                // header("Location:".$_SERVER['HTTP_REFERER']) ;
                // exit;
            // error_reporting(E_ALL);
            // ini_set('display_errors', 1);
            // $propertyTypes = \App\PropertyType::where(['id' => $id])->get();

            $id = $_REQUEST['id'];
            $isPropertyImage = PropertyImages::where('property_id', '=', $id)->first();

            // echo "Property Image <pre>";
            //     print_r(count($isPropertyImage));
            // echo "</pre>";
            // exit;
            if (count($isPropertyImage) > 0) {
               $isDeleted = PropertyImages::where("property_id",'=', $id)->delete();
            }
               $isDeleted = Property::where("id",'=', $id)->delete();
            
            // $isDeleted = $this->propertyRepository->getById($id);
            if($isDeleted){
                return 'success';
            }else{
                return 'error';
            }
            exit;
        }

        public function addSchooHouse() {
            //$propertyTypes = $this->propertyTypeRepository->getAll(true);
            $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'unpaid'])->get();
            $roomTypes = $this->roomTypeRepository->getAll(true);
            $tutorRelation = Tutor::all();
            $userObj = User::where("id", \Auth::user()->id)->first();
            if (isset($userObj->user_paid_property->id)) {
                flash('You have already added your School House Listing, now you may add other services you provide', 'success');
                return \Redirect::to('rentals/add');
            } else if (isset($userObj->user_paid_property_pending->id)) {
                flash('You have already added your School House Listing, now you may add other services you provide', 'success');
                return \Redirect::to('rentals/add');
            }
            if (\Auth::user()->hasRole('PaidServices')){
                return View('rentals.addSchooHouse', compact('propertyTypes', 'roomTypes', 'tutorRelation', 'userObj'));
            }else{
                flash('Please fill in School House information before adding paid Listing', 'success'); // this is just for testing if working then change it okay.
                return \Redirect::to('user/paidServices');
            } 
            // return View('rentals.addSchooHouse', compact('propertyTypes', 'roomTypes', 'tutorRelation', 'userObj'));
        }

        public function addUnpaidFlash() {
            //if (\Auth::user()->hasRole('Organization')) {
            //$propertyTypes = $this->propertyTypeRepository->getAll(true);
            $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'unpaid'])->get();
            $roomTypes = $this->roomTypeRepository->getAll(true);
            $tutorRelation = Tutor::all();
            return View('rentals.addspaceflash', compact('propertyTypes', 'roomTypes', 'tutorRelation'));
            //}
            //flash('please fill organization information before you add work experience', 'success');
            //return \Redirect::to('user/edit');
        }

        public function addPaid() {
            //if (\Auth::user()->hasRole('Organization')) {
            //$propertyTypes = $this->propertyTypeRepository->getAll(true);
            $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'paid'])->where("name", "!=", "Bed")->get();
            $roomTypes = $this->roomTypeRepository->getAll(true);
            $tutorRelation = Tutor::all();
            $real_room_type = \App\RealRoomTypes::where("is_active", "1")->where("is_delete", "0")->get();
            $userObject = $this->userRepository->getLoginUser();
            $bed_types = \App\BedType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $class_types = \App\ClassType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $activity_types = \App\ActivityType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $transfer_types = \App\TransferType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $tour_types = \App\TourType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $wow_types = \App\WowType::where(["is_active" => "1", "is_delete" => "0"])->get();
            return View('rentals.add_school_house', compact('propertyTypes', 'roomTypes', 'real_room_type', 'tour_types', 'wow_types', 'transfer_types', 'activity_types', 'tutorRelation', 'userObject', 'bed_types', 'class_types'));
            //}
            //flash('please fill organization information before you add work experience', 'success');
            //return \Redirect::to('user/edit');
        }

        public function addPaidBed() {
            //if (\Auth::user()->hasRole('Organization')) {
            //$propertyTypes = $this->propertyTypeRepository->getAll(true);
            $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'paid'])->where("name", "=", "Bed")->get();
            $roomTypes = $this->roomTypeRepository->getAll(true);
            $real_room_type = \App\RealRoomTypes::where("is_active", "1")->where("is_delete", "0")->get();
            $userObject = $this->userRepository->getLoginUser();
            $bed_types = \App\BedType::where(["is_active" => "1", "is_delete" => "0"])->get();

            return View('rentals.add_school_house_bed', compact('propertyTypes', 'roomTypes', 'real_room_type', 'userObject', 'bed_types'));
            //}
            //flash('please fill organization information before you add work experience', 'success');
            //return \Redirect::to('user/edit');
        }
		
		
		//Upload Class Room Images
        public function uploadclassroomimages(Request $request) {
			$files = $request->file('file');
            // Making counting of uploaded images
            $file_count = count($files);
            $count = 0;
            $res='';
            $files_uploaded_path = array();
            // start count how many uploaded
            foreach ($files as $file) {
                
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $img_name = "class-".$count."-".$timestamp."-".time().'.' . $file->getClientOriginalExtension();
                $room_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms";
                if (!\File::exists($room_path_check)) {
                    \File::makeDirectory($room_path_check, $mode = 0777, true, true);
                }
                $file->move(public_path() . '/images/rentals/' . $request->get('listingid') . '/rooms', $img_name);
                $img = \Image::make(public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/" . $img_name)->resize(370, 250);
                $thumbnail_path = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail/" . $img_name;
                $thumbnail_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail";
                if (!\File::exists($thumbnail_path_check)) {
                    \File::makeDirectory($thumbnail_path_check, $mode = 0777, true, true);
                }
                $img->save($thumbnail_path);
				$thumbnail_url = url(). '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail/" . $img_name;
				echo $img_name.',';
				
				$count++; 
				
						 
            }
			
		}


		//Upload Activity Images
        public function uploadActivityImages(Request $request) {
			$files = $request->file('file');
            // Making counting of uploaded images
            $file_count = count($files);
            $count = 0;
            $res='';
            $files_uploaded_path = array();
            // start count how many uploaded
            foreach ($files as $file) {

                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $img_name = "activity-".$count."-".$timestamp."-".time().'.' . $file->getClientOriginalExtension();
                $room_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms";
                if (!\File::exists($room_path_check)) {
                    \File::makeDirectory($room_path_check, $mode = 0777, true, true);
                }
                $file->move(public_path() . '/images/rentals/' . $request->get('listingid') . '/rooms', $img_name);
                $img = \Image::make(public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/" . $img_name)->resize(370, 250);
                $thumbnail_path = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail/" . $img_name;
                $thumbnail_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail";
                if (!\File::exists($thumbnail_path_check)) {
                    \File::makeDirectory($thumbnail_path_check, $mode = 0777, true, true);
                }
                $img->save($thumbnail_path);
				$thumbnail_url = url(). '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail/" . $img_name;
				echo $img_name.',';

				$count++;
            }
		}
		
		//Upload Diet Images
        public function uploadDietImages(Request $request) {
			$files = $request->file('file');
            // Making counting of uploaded images
            $file_count = count($files);
            $count = 0;
            $res='';
            $files_uploaded_path = array();
            // start count how many uploaded
            foreach ($files as $file) {

                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $img_name = "activity-".$count."-".$timestamp."-".time().'.' . $file->getClientOriginalExtension();
                $room_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms";
                if (!\File::exists($room_path_check)) {
                    \File::makeDirectory($room_path_check, $mode = 0777, true, true);
                }
                $file->move(public_path() . '/images/rentals/' . $request->get('listingid') . '/rooms', $img_name);
                $img = \Image::make(public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/" . $img_name)->resize(370, 250);
                $thumbnail_path = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail/" . $img_name;
                $thumbnail_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail";
                if (!\File::exists($thumbnail_path_check)) {
                    \File::makeDirectory($thumbnail_path_check, $mode = 0777, true, true);
                }
                $img->save($thumbnail_path);
				$thumbnail_url = url(). '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail/" . $img_name;
				echo $img_name.',';

				$count++;
            }
		}



		//Upload Bed Room Images
        public function uploadroomimages(Request $request) {
			$files = $request->file('file');
            // Making counting of uploaded images
            $file_count = count($files);
            $count = 0;
            $res='';
            $files_uploaded_path = array();
            // start count how many uploaded
            foreach ($files as $file) {
                
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $img_name = $count."-".$timestamp."-".time().'.' . $file->getClientOriginalExtension();
                $room_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms";
                if (!\File::exists($room_path_check)) {
                    \File::makeDirectory($room_path_check, $mode = 0777, true, true);
                }
                $file->move(public_path() . '/images/rentals/' . $request->get('listingid') . '/rooms', $img_name);
                $img = \Image::make(public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/" . $img_name)->resize(370, 250);
                $thumbnail_path = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail/" . $img_name;
                $thumbnail_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail";
                if (!\File::exists($thumbnail_path_check)) {
                    \File::makeDirectory($thumbnail_path_check, $mode = 0777, true, true);
                }
                $img->save($thumbnail_path);
				$thumbnail_url = url(). '/images/rentals/' . $request->get('listingid') . "/rooms/thumbnail/" . $img_name;
				echo $img_name.',';
				
				
				$count++; 
				
						 
            }
			
		}
		
		public function trimLastCharacter($str) {
			return rtrim($str, ",");
		}
		
		 public function checkCommaExists($str) {
			if (strpos($str, ',') !== FALSE) {
				return true;
			} else {
				return false;
			}
		}
		
        public function updateClassRoom(Request $request) { 
			$user_id = \Auth::user()->id;
			$user_first_listing = Property::where('user_id',$user_id)->where('property_type',0)->first();
			
			$room_id = $request->get('room_id');
			$property_id = $request->get('property_id');
			$room_title = $request->get('room_title');
			$room_description = $request->get('room_description');
			$room_places = $request->get('room_places');
			$payment_amount = $request->get('payment_amount');
			$payment_currency = $request->get('payment_currency');
			$payment_type = $request->get('payment_type');
			$room_facilities = $request->get('room_facilities');
			$room_images = $request->get('room_images');
			//var_dump($room_images);
			$guest_room_data = array();
			$guest_room = new \App\UserPaidGuestRoom();
			$guest_room_data['user_id'] = $user_id;
			$guest_room_data['is_active'] = 0;
			$guest_room_data['room_name'] = $room_title;
			$guest_room_data['room_description'] = $room_description;
			
			$roomFacilitiesText = "";
			if (is_array($room_facilities)) {
				foreach ($room_facilities as $facility) {
					$roomFacilitiesText .= $facility . ",";
				}
			}
			if ($this->checkCommaExists($roomFacilitiesText)) {
				$roomFacilitiesText = $this->trimLastCharacter($roomFacilitiesText);
			}
			$guest_room_data['room_facility'] = $roomFacilitiesText;
			$guest_room_data['room_guest_toilet'] = '';
			$guest_room_data['room_guest_shower'] = '';
			
			if($guest_room->where('id', $room_id)->update($guest_room_data)){
				
				$all_other_properties = \App\Property::where(['property_type' => 24,'bed_guest_room_id' => $room_id])->get();
				$other_properties_count = \App\Property::where(['property_type' => 24,'bed_guest_room_id' => $room_id])->count();
				if($other_properties_count > 0){
					$place_ids = '';
					
					foreach($all_other_properties as $other_proprty){
						$place_id = $other_proprty->id;
						if($place_ids == ''){
							$place_ids .= $place_id;
						}else{
							$place_ids .= '=='.$place_id;
						}
						$place_data = array("status" => 0);
						$res = $this->propertyRepository->update($place_data,$place_id);
						$pricing = new \App\PropertyPricing();						
						$pricing_update = array('currency_id'=>$payment_currency,'payment_type'=>$payment_type,'amount'=>$payment_amount);
						$pricing->where(['property_id' => $other_proprty->id])->update($pricing_update);
					}
					
					if($room_places > $other_properties_count){
						//to add more places here 
						$tobeadd = $room_places - $other_properties_count;
						$place_counter = $other_properties_count+1;
						
						while($place_counter <= $room_places){ 
							
							$cloned_listing_for_class_place = array("work_org_same" => $user_first_listing->work_org_same,
													"addr_work_org_same" => $user_first_listing->addr_work_org_same,
													"user_id" => $user_first_listing->user_id,
													"property_type" => 24,
													"room_type" => 0,
													"title" => '',
													"exptitle" => $room_title.' - seat '.$place_counter,
													"description" => $room_description,
													"optional_description" => $room_description,
													"legal_nature" => $user_first_listing->legal_nature,
													"work_category" => $user_first_listing->work_category,
													"hr_number" => $user_first_listing->hr_number,
													"accomodates" => $user_first_listing->accomodates,
													"legal_address" => $user_first_listing->legal_address,
													"lstreet_number" => $user_first_listing->lstreet_number,
													"lroute" => $user_first_listing->lroute,
													"ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
													"llocality" => $user_first_listing->llocality,
													"lpostal_code" => $user_first_listing->lpostal_code,
													"lcountry" => $user_first_listing->lcountry,
													"city" => $user_first_listing->city,
													"accesscode" => $user_first_listing->accesscode,
													"streetAddress" => $user_first_listing->streetAddress,
													"state" => $user_first_listing->state,
													"country" => $user_first_listing->country,
													"country_id" => $user_first_listing->country_id,
													"zipcode" => $user_first_listing->zipcode,
													"pro_lat" => $user_first_listing->pro_lat,
													"pro_lon" => $user_first_listing->pro_lon,
													"auto_complete" => $user_first_listing->auto_complete,
													"bed_rooms" => $user_first_listing->bed_rooms,
													"beds" => $user_first_listing->beds,
													"bathrooms" => $user_first_listing->bathrooms,
													"price_night" => $user_first_listing->price_night,
													"secutity_deposit" => $user_first_listing->secutity_deposit,
													"cancellation_policy" => $user_first_listing->cancellation_policy,
													"booking_style" => $user_first_listing->booking_style,
													"houserules" => $user_first_listing->houserules,
													"min_stay" => $user_first_listing->min_stay,
													"max_stay" => $user_first_listing->max_stay,
													"calendar_availability" => $user_first_listing->calendar_availability,
													"fromDate" => $user_first_listing->fromDate,
													"toDate" => $user_first_listing->toDate,
													"represent_name" => $user_first_listing->represent_name,
													"represent_surname" => $user_first_listing->represent_surname,
													"represent_born" => $user_first_listing->represent_born,
													"represent_tax_no" => $user_first_listing->represent_tax_no,
													"represent_email" => $user_first_listing->represent_email,
													"relation_with_company" => $user_first_listing->relation_with_company,
													"work_tutor_fit" => $user_first_listing->work_tutor_fit,
													"represent_phone" => $user_first_listing->represent_phone,
													"location_email" => $user_first_listing->location_email,
													"location_telephone" => $user_first_listing->location_telephone,
													"workplace_address" => $user_first_listing->workplace_address,
													"wstreet_number" => $user_first_listing->wstreet_number,
													"wroute" => $user_first_listing->wroute,
													"wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
													"wlocality" => $user_first_listing->wlocality,
													"wpostal_code" => $user_first_listing->wpostal_code,
													"wcountry" => $user_first_listing->wcountry,
													"work_other_check" => $user_first_listing->work_other_check,
													"location_other_address" => $user_first_listing->location_other_address,
													"work_schedule" => $user_first_listing->work_schedule,
													"work_hours" => $user_first_listing->work_hours,
													"status" => 0,											
													"property_status" => 'paid',
													"bed_guest_room_id" => $room_id,
													"number_of_guests_per_bed" => '1'
													);
					
					
							$obj = $this->propertyRepository->create($cloned_listing_for_class_place);	
							if($obj){
								$place_id = $obj->id;
								$pricing = new \App\PropertyPricing();						
								$pricing->user_id = \Auth::user()->id;
								$pricing->property_id = $place_id;
								$pricing->currency_id = $payment_currency;
								$pricing->payment_type = $payment_type;
								$pricing->amount = $payment_amount;
								$pricing->expected_extra_costs_value = '';
								$pricing->expected_extra_costs_description = '';
								$pricing->save();
								
							}
							$place_id = $obj->id;
							if($place_ids == ''){
								$place_ids .= $place_id;
							}else{
								$place_ids .= '=='.$place_id;
							}
							
							$place_counter++;  
						}
						
					}
				}else{
					$place_counter = 1;
					$place_ids = '';
					while($place_counter <= $room_places){ 
						
						$cloned_listing_for_class_place = array("work_org_same" => $user_first_listing->work_org_same,
												"addr_work_org_same" => $user_first_listing->addr_work_org_same,
												"user_id" => $user_first_listing->user_id,
												"property_type" => 24,
												"room_type" => 0,
												"title" => '',
												"exptitle" => $room_title.' - seat '.$place_counter,
												"description" => $room_description,
												"optional_description" => $room_description,
												"legal_nature" => $user_first_listing->legal_nature,
												"work_category" => $user_first_listing->work_category,
												"hr_number" => $user_first_listing->hr_number,
												"accomodates" => $user_first_listing->accomodates,
												"legal_address" => $user_first_listing->legal_address,
												"lstreet_number" => $user_first_listing->lstreet_number,
												"lroute" => $user_first_listing->lroute,
												"ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
												"llocality" => $user_first_listing->llocality,
												"lpostal_code" => $user_first_listing->lpostal_code,
												"lcountry" => $user_first_listing->lcountry,
												"city" => $user_first_listing->city,
												"accesscode" => $user_first_listing->accesscode,
												"streetAddress" => $user_first_listing->streetAddress,
												"state" => $user_first_listing->state,
												"country" => $user_first_listing->country,
												"country_id" => $user_first_listing->country_id,
												"zipcode" => $user_first_listing->zipcode,
												"pro_lat" => $user_first_listing->pro_lat,
												"pro_lon" => $user_first_listing->pro_lon,
												"auto_complete" => $user_first_listing->auto_complete,
												"bed_rooms" => $user_first_listing->bed_rooms,
												"beds" => $user_first_listing->beds,
												"bathrooms" => $user_first_listing->bathrooms,
												"price_night" => $user_first_listing->price_night,
												"secutity_deposit" => $user_first_listing->secutity_deposit,
												"cancellation_policy" => $user_first_listing->cancellation_policy,
												"booking_style" => $user_first_listing->booking_style,
												"houserules" => $user_first_listing->houserules,
												"min_stay" => $user_first_listing->min_stay,
												"max_stay" => $user_first_listing->max_stay,
												"calendar_availability" => $user_first_listing->calendar_availability,
												"fromDate" => $user_first_listing->fromDate,
												"toDate" => $user_first_listing->toDate,
												"represent_name" => $user_first_listing->represent_name,
												"represent_surname" => $user_first_listing->represent_surname,
												"represent_born" => $user_first_listing->represent_born,
												"represent_tax_no" => $user_first_listing->represent_tax_no,
												"represent_email" => $user_first_listing->represent_email,
												"relation_with_company" => $user_first_listing->relation_with_company,
												"work_tutor_fit" => $user_first_listing->work_tutor_fit,
												"represent_phone" => $user_first_listing->represent_phone,
												"location_email" => $user_first_listing->location_email,
												"location_telephone" => $user_first_listing->location_telephone,
												"workplace_address" => $user_first_listing->workplace_address,
												"wstreet_number" => $user_first_listing->wstreet_number,
												"wroute" => $user_first_listing->wroute,
												"wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
												"wlocality" => $user_first_listing->wlocality,
												"wpostal_code" => $user_first_listing->wpostal_code,
												"wcountry" => $user_first_listing->wcountry,
												"work_other_check" => $user_first_listing->work_other_check,
												"location_other_address" => $user_first_listing->location_other_address,
												"work_schedule" => $user_first_listing->work_schedule,
												"work_hours" => $user_first_listing->work_hours,
												"status" => 0,											
												"property_status" => 'paid',
												"bed_guest_room_id" => $room_id,
												"number_of_guests_per_bed" => '1'
												);
				
				
						$obj = $this->propertyRepository->create($cloned_listing_for_class_place);	
						if($obj){
							$place_id = $obj->id;
							$pricing = new \App\PropertyPricing();						
							$pricing->user_id = \Auth::user()->id;
							$pricing->property_id = $place_id;
							$pricing->currency_id = $payment_currency;
							$pricing->payment_type = $payment_type;
							$pricing->amount = $payment_amount;
							$pricing->expected_extra_costs_value = '';
							$pricing->expected_extra_costs_description = '';
							$pricing->save(); 
							
						}
						$place_id = $obj->id;
						if($place_ids == ''){
							$place_ids .= $place_id;
						}else{
							$place_ids .= '=='.$place_id;
						}
						
						$place_counter++;  
					}
				}
				
				if($room_images != NULL && is_array($room_images)){
					//$res = GuestRoomImages::where('room_id',$room_id)->delete();
					$guest_room_images = GuestRoomImages::where(["room_id" => $room_id])->get();
					if(count($guest_room_images) > 0){
						foreach($guest_room_images as $room_image){
							$room_image_name = $room_image->img_name;
							if(!in_array($room_image_name,$room_images)){
								$img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/'.$room_image_name;
								$img_thumbnail_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
								if(file_exists($img_path)){
									unlink($img_path);
								}
								if(file_exists($img_thumbnail_path)){
									unlink($img_thumbnail_path);
								} 
								//Remove that Image from DB Tables 
								GuestRoomImages::where(["id" => $room_image->id])->delete();  
							}
							
						}	
						
					} 
				
					foreach($room_images as $room_image){
						if($room_image !== ""){
							$imgObj = new GuestRoomImages;
							$imgObj->img_name = $room_image;
							$imgObj->img_src = "";
							$imgObj->status = 1;
							$imgObj->created_at = date("Y-m-d H:i:s");
							$imgObj->updated_at = date("Y-m-d H:i:s");
							$imgObj->room_id =$room_id;
							$imgObj->save();
						}
					}
				}
				
				echo 'success,'.$room_id,','.$place_ids;die;  
			}
			
			echo 'error';die;
		}


        public function updateActivity(Request $request) {
            $user_id = \Auth::user()->id;
            $user_first_listing = Property::where('user_id',$user_id)->where('property_type',0)->first();

            $room_id = $request->get('room_id');
            $property_id = $request->get('property_id');
            $room_title = $request->get('room_title');
            $room_description = $request->get('room_description');
            $room_places = $request->get('room_places');
            $payment_amount = $request->get('payment_amount');
            $payment_currency = $request->get('payment_currency');
            $payment_type = $request->get('payment_type');
            $room_facilities = $request->get('room_facilities');
            $room_images = $request->get('room_images');
            //var_dump($room_images);
            $guest_room_data = array();
            $guest_room = new \App\UserPaidGuestRoom();
            $guest_room_data['user_id'] = $user_id;
            $guest_room_data['is_active'] = 0;
            $guest_room_data['room_name'] = $room_title;
            $guest_room_data['room_description'] = $room_description;

            $roomFacilitiesText = "";
            if (is_array($room_facilities)) {
                foreach ($room_facilities as $facility) {
                    $roomFacilitiesText .= $facility . ",";
                }
            }
            if ($this->checkCommaExists($roomFacilitiesText)) {
                $roomFacilitiesText = $this->trimLastCharacter($roomFacilitiesText);
            }
            $guest_room_data['room_facility'] = $roomFacilitiesText;
            $guest_room_data['room_guest_toilet'] = '';
            $guest_room_data['room_guest_shower'] = '';

            if($guest_room->where('id', $room_id)->update($guest_room_data)){

                $all_other_properties = \App\Property::where(['property_type' => 24,'bed_guest_room_id' => $room_id])->get();
                $other_properties_count = \App\Property::where(['property_type' => 24,'bed_guest_room_id' => $room_id])->count();
                if($other_properties_count > 0){
                    $place_ids = '';

                    foreach($all_other_properties as $other_proprty){
                        $place_id = $other_proprty->id;
                        if($place_ids == ''){
                            $place_ids .= $place_id;
                        }else{
                            $place_ids .= '=='.$place_id;
                        }
                        $place_data = array("status" => 0);
                        $res = $this->propertyRepository->update($place_data,$place_id);
                        $pricing = new \App\PropertyPricing();
                        $pricing_update = array('currency_id'=>$payment_currency,'payment_type'=>$payment_type,'amount'=>$payment_amount);
                        $pricing->where(['property_id' => $other_proprty->id])->update($pricing_update);
                    }

                    if($room_places > $other_properties_count){
                        //to add more places here
                        $tobeadd = $room_places - $other_properties_count;
                        $place_counter = $other_properties_count+1;

                        while($place_counter <= $room_places){

                            $cloned_listing_for_class_place = array("work_org_same" => $user_first_listing->work_org_same,
                                "addr_work_org_same" => $user_first_listing->addr_work_org_same,
                                "user_id" => $user_first_listing->user_id,
                                "property_type" => 24,
                                "room_type" => 0,
                                "title" => '',
                                "exptitle" => $room_title.' - seat '.$place_counter,
                                "description" => $room_description,
                                "optional_description" => $room_description,
                                "legal_nature" => $user_first_listing->legal_nature,
                                "work_category" => $user_first_listing->work_category,
                                "hr_number" => $user_first_listing->hr_number,
                                "accomodates" => $user_first_listing->accomodates,
                                "legal_address" => $user_first_listing->legal_address,
                                "lstreet_number" => $user_first_listing->lstreet_number,
                                "lroute" => $user_first_listing->lroute,
                                "ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
                                "llocality" => $user_first_listing->llocality,
                                "lpostal_code" => $user_first_listing->lpostal_code,
                                "lcountry" => $user_first_listing->lcountry,
                                "city" => $user_first_listing->city,
                                "accesscode" => $user_first_listing->accesscode,
                                "streetAddress" => $user_first_listing->streetAddress,
                                "state" => $user_first_listing->state,
                                "country" => $user_first_listing->country,
                                "country_id" => $user_first_listing->country_id,
                                "zipcode" => $user_first_listing->zipcode,
                                "pro_lat" => $user_first_listing->pro_lat,
                                "pro_lon" => $user_first_listing->pro_lon,
                                "auto_complete" => $user_first_listing->auto_complete,
                                "bed_rooms" => $user_first_listing->bed_rooms,
                                "beds" => $user_first_listing->beds,
                                "bathrooms" => $user_first_listing->bathrooms,
                                "price_night" => $user_first_listing->price_night,
                                "secutity_deposit" => $user_first_listing->secutity_deposit,
                                "cancellation_policy" => $user_first_listing->cancellation_policy,
                                "booking_style" => $user_first_listing->booking_style,
                                "houserules" => $user_first_listing->houserules,
                                "min_stay" => $user_first_listing->min_stay,
                                "max_stay" => $user_first_listing->max_stay,
                                "calendar_availability" => $user_first_listing->calendar_availability,
                                "fromDate" => $user_first_listing->fromDate,
                                "toDate" => $user_first_listing->toDate,
                                "represent_name" => $user_first_listing->represent_name,
                                "represent_surname" => $user_first_listing->represent_surname,
                                "represent_born" => $user_first_listing->represent_born,
                                "represent_tax_no" => $user_first_listing->represent_tax_no,
                                "represent_email" => $user_first_listing->represent_email,
                                "relation_with_company" => $user_first_listing->relation_with_company,
                                "work_tutor_fit" => $user_first_listing->work_tutor_fit,
                                "represent_phone" => $user_first_listing->represent_phone,
                                "location_email" => $user_first_listing->location_email,
                                "location_telephone" => $user_first_listing->location_telephone,
                                "workplace_address" => $user_first_listing->workplace_address,
                                "wstreet_number" => $user_first_listing->wstreet_number,
                                "wroute" => $user_first_listing->wroute,
                                "wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
                                "wlocality" => $user_first_listing->wlocality,
                                "wpostal_code" => $user_first_listing->wpostal_code,
                                "wcountry" => $user_first_listing->wcountry,
                                "work_other_check" => $user_first_listing->work_other_check,
                                "location_other_address" => $user_first_listing->location_other_address,
                                "work_schedule" => $user_first_listing->work_schedule,
                                "work_hours" => $user_first_listing->work_hours,
                                "status" => 0,
                                "property_status" => 'paid',
                                "bed_guest_room_id" => $room_id,
                                "number_of_guests_per_bed" => '1'
                            );


                            $obj = $this->propertyRepository->create($cloned_listing_for_class_place);
                            if($obj){
                                $place_id = $obj->id;
                                $pricing = new \App\PropertyPricing();
                                $pricing->user_id = \Auth::user()->id;
                                $pricing->property_id = $place_id;
                                $pricing->currency_id = $payment_currency;
                                $pricing->payment_type = $payment_type;
                                $pricing->amount = $payment_amount;
                                $pricing->expected_extra_costs_value = '';
                                $pricing->expected_extra_costs_description = '';
                                $pricing->save();

                            }
                            $place_id = $obj->id;
                            if($place_ids == ''){
                                $place_ids .= $place_id;
                            }else{
                                $place_ids .= '=='.$place_id;
                            }

                            $place_counter++;
                        }

                    }
                }else{
                    $place_counter = 1;
                    $place_ids = '';
                    while($place_counter <= $room_places){

                        $cloned_listing_for_class_place = array("work_org_same" => $user_first_listing->work_org_same,
                            "addr_work_org_same" => $user_first_listing->addr_work_org_same,
                            "user_id" => $user_first_listing->user_id,
                            "property_type" => 24,
                            "room_type" => 0,
                            "title" => '',
                            "exptitle" => $room_title.' - seat '.$place_counter,
                            "description" => $room_description,
                            "optional_description" => $room_description,
                            "legal_nature" => $user_first_listing->legal_nature,
                            "work_category" => $user_first_listing->work_category,
                            "hr_number" => $user_first_listing->hr_number,
                            "accomodates" => $user_first_listing->accomodates,
                            "legal_address" => $user_first_listing->legal_address,
                            "lstreet_number" => $user_first_listing->lstreet_number,
                            "lroute" => $user_first_listing->lroute,
                            "ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
                            "llocality" => $user_first_listing->llocality,
                            "lpostal_code" => $user_first_listing->lpostal_code,
                            "lcountry" => $user_first_listing->lcountry,
                            "city" => $user_first_listing->city,
                            "accesscode" => $user_first_listing->accesscode,
                            "streetAddress" => $user_first_listing->streetAddress,
                            "state" => $user_first_listing->state,
                            "country" => $user_first_listing->country,
                            "country_id" => $user_first_listing->country_id,
                            "zipcode" => $user_first_listing->zipcode,
                            "pro_lat" => $user_first_listing->pro_lat,
                            "pro_lon" => $user_first_listing->pro_lon,
                            "auto_complete" => $user_first_listing->auto_complete,
                            "bed_rooms" => $user_first_listing->bed_rooms,
                            "beds" => $user_first_listing->beds,
                            "bathrooms" => $user_first_listing->bathrooms,
                            "price_night" => $user_first_listing->price_night,
                            "secutity_deposit" => $user_first_listing->secutity_deposit,
                            "cancellation_policy" => $user_first_listing->cancellation_policy,
                            "booking_style" => $user_first_listing->booking_style,
                            "houserules" => $user_first_listing->houserules,
                            "min_stay" => $user_first_listing->min_stay,
                            "max_stay" => $user_first_listing->max_stay,
                            "calendar_availability" => $user_first_listing->calendar_availability,
                            "fromDate" => $user_first_listing->fromDate,
                            "toDate" => $user_first_listing->toDate,
                            "represent_name" => $user_first_listing->represent_name,
                            "represent_surname" => $user_first_listing->represent_surname,
                            "represent_born" => $user_first_listing->represent_born,
                            "represent_tax_no" => $user_first_listing->represent_tax_no,
                            "represent_email" => $user_first_listing->represent_email,
                            "relation_with_company" => $user_first_listing->relation_with_company,
                            "work_tutor_fit" => $user_first_listing->work_tutor_fit,
                            "represent_phone" => $user_first_listing->represent_phone,
                            "location_email" => $user_first_listing->location_email,
                            "location_telephone" => $user_first_listing->location_telephone,
                            "workplace_address" => $user_first_listing->workplace_address,
                            "wstreet_number" => $user_first_listing->wstreet_number,
                            "wroute" => $user_first_listing->wroute,
                            "wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
                            "wlocality" => $user_first_listing->wlocality,
                            "wpostal_code" => $user_first_listing->wpostal_code,
                            "wcountry" => $user_first_listing->wcountry,
                            "work_other_check" => $user_first_listing->work_other_check,
                            "location_other_address" => $user_first_listing->location_other_address,
                            "work_schedule" => $user_first_listing->work_schedule,
                            "work_hours" => $user_first_listing->work_hours,
                            "status" => 0,
                            "property_status" => 'paid',
                            "bed_guest_room_id" => $room_id,
                            "number_of_guests_per_bed" => '1'
                        );


                        $obj = $this->propertyRepository->create($cloned_listing_for_class_place);
                        if($obj){
                            $place_id = $obj->id;
                            $pricing = new \App\PropertyPricing();
                            $pricing->user_id = \Auth::user()->id;
                            $pricing->property_id = $place_id;
                            $pricing->currency_id = $payment_currency;
                            $pricing->payment_type = $payment_type;
                            $pricing->amount = $payment_amount;
                            $pricing->expected_extra_costs_value = '';
                            $pricing->expected_extra_costs_description = '';
                            $pricing->save();

                        }
                        $place_id = $obj->id;
                        if($place_ids == ''){
                            $place_ids .= $place_id;
                        }else{
                            $place_ids .= '=='.$place_id;
                        }

                        $place_counter++;
                    }
                }

                if($room_images != NULL && is_array($room_images)){
                    //$res = GuestRoomImages::where('room_id',$room_id)->delete();
                    $guest_room_images = GuestRoomImages::where(["room_id" => $room_id])->get();
                    if(count($guest_room_images) > 0){
                        foreach($guest_room_images as $room_image){
                            $room_image_name = $room_image->img_name;
                            if(!in_array($room_image_name,$room_images)){
                                $img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/'.$room_image_name;
                                $img_thumbnail_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
                                if(file_exists($img_path)){
                                    unlink($img_path);
                                }
                                if(file_exists($img_thumbnail_path)){
                                    unlink($img_thumbnail_path);
                                }
                                //Remove that Image from DB Tables
                                GuestRoomImages::where(["id" => $room_image->id])->delete();
                            }

                        }

                    }

                    foreach($room_images as $room_image){
                        if($room_image !== ""){
                            $imgObj = new GuestRoomImages;
                            $imgObj->img_name = $room_image;
                            $imgObj->img_src = "";
                            $imgObj->status = 1;
                            $imgObj->created_at = date("Y-m-d H:i:s");
                            $imgObj->updated_at = date("Y-m-d H:i:s");
                            $imgObj->room_id =$room_id;
                            $imgObj->save();
                        }
                    }
                }

                echo 'success,'.$room_id,','.$place_ids;die;
            }

            echo 'error';die;
        }

        public function updateDiet(Request $request) {
            $user_id = \Auth::user()->id;
            $user_first_listing = Property::where('user_id',$user_id)->where('property_type',0)->first();

            $room_id = $request->get('room_id');
            $property_id = $request->get('property_id');
            $room_title = $request->get('room_title');
            $room_description = $request->get('room_description');
            $room_places = $request->get('room_places');
            $payment_amount = $request->get('payment_amount');
            $payment_currency = $request->get('payment_currency');
            $payment_type = $request->get('payment_type');
            $room_facilities = $request->get('room_facilities');
            $room_images = $request->get('room_images');
            //var_dump($room_images);
            $guest_room_data = array();
            $guest_room = new \App\UserPaidGuestRoom();
            $guest_room_data['user_id'] = $user_id;
            $guest_room_data['is_active'] = 0;
            $guest_room_data['room_name'] = $room_title;
            $guest_room_data['room_description'] = $room_description;

            $roomFacilitiesText = "";
            if (is_array($room_facilities)) {
                foreach ($room_facilities as $facility) {
                    $roomFacilitiesText .= $facility . ",";
                }
            }
            if ($this->checkCommaExists($roomFacilitiesText)) {
                $roomFacilitiesText = $this->trimLastCharacter($roomFacilitiesText);
            }
            $guest_room_data['room_facility'] = $roomFacilitiesText;
            $guest_room_data['room_guest_toilet'] = '';
            $guest_room_data['room_guest_shower'] = '';

            if($guest_room->where('id', $room_id)->update($guest_room_data)){

                $all_other_properties = \App\Property::where(['property_type' => 24,'bed_guest_room_id' => $room_id])->get();
                $other_properties_count = \App\Property::where(['property_type' => 24,'bed_guest_room_id' => $room_id])->count();
                if($other_properties_count > 0){
                    $place_ids = '';

                    foreach($all_other_properties as $other_proprty){
                        $place_id = $other_proprty->id;
                        if($place_ids == ''){
                            $place_ids .= $place_id;
                        }else{
                            $place_ids .= '=='.$place_id;
                        }
                        $place_data = array("status" => 0);
                        $res = $this->propertyRepository->update($place_data,$place_id);
                        $pricing = new \App\PropertyPricing();
                        $pricing_update = array('currency_id'=>$payment_currency,'payment_type'=>$payment_type,'amount'=>$payment_amount);
                        $pricing->where(['property_id' => $other_proprty->id])->update($pricing_update);
                    }

                    if($room_places > $other_properties_count){
                        //to add more places here
                        $tobeadd = $room_places - $other_properties_count;
                        $place_counter = $other_properties_count+1;

                        while($place_counter <= $room_places){

                            $cloned_listing_for_class_place = array("work_org_same" => $user_first_listing->work_org_same,
                                "addr_work_org_same" => $user_first_listing->addr_work_org_same,
                                "user_id" => $user_first_listing->user_id,
                                "property_type" => 24,
                                "room_type" => 0,
                                "title" => '',
                                "exptitle" => $room_title.' - seat '.$place_counter,
                                "description" => $room_description,
                                "optional_description" => $room_description,
                                "legal_nature" => $user_first_listing->legal_nature,
                                "work_category" => $user_first_listing->work_category,
                                "hr_number" => $user_first_listing->hr_number,
                                "accomodates" => $user_first_listing->accomodates,
                                "legal_address" => $user_first_listing->legal_address,
                                "lstreet_number" => $user_first_listing->lstreet_number,
                                "lroute" => $user_first_listing->lroute,
                                "ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
                                "llocality" => $user_first_listing->llocality,
                                "lpostal_code" => $user_first_listing->lpostal_code,
                                "lcountry" => $user_first_listing->lcountry,
                                "city" => $user_first_listing->city,
                                "accesscode" => $user_first_listing->accesscode,
                                "streetAddress" => $user_first_listing->streetAddress,
                                "state" => $user_first_listing->state,
                                "country" => $user_first_listing->country,
                                "country_id" => $user_first_listing->country_id,
                                "zipcode" => $user_first_listing->zipcode,
                                "pro_lat" => $user_first_listing->pro_lat,
                                "pro_lon" => $user_first_listing->pro_lon,
                                "auto_complete" => $user_first_listing->auto_complete,
                                "bed_rooms" => $user_first_listing->bed_rooms,
                                "beds" => $user_first_listing->beds,
                                "bathrooms" => $user_first_listing->bathrooms,
                                "price_night" => $user_first_listing->price_night,
                                "secutity_deposit" => $user_first_listing->secutity_deposit,
                                "cancellation_policy" => $user_first_listing->cancellation_policy,
                                "booking_style" => $user_first_listing->booking_style,
                                "houserules" => $user_first_listing->houserules,
                                "min_stay" => $user_first_listing->min_stay,
                                "max_stay" => $user_first_listing->max_stay,
                                "calendar_availability" => $user_first_listing->calendar_availability,
                                "fromDate" => $user_first_listing->fromDate,
                                "toDate" => $user_first_listing->toDate,
                                "represent_name" => $user_first_listing->represent_name,
                                "represent_surname" => $user_first_listing->represent_surname,
                                "represent_born" => $user_first_listing->represent_born,
                                "represent_tax_no" => $user_first_listing->represent_tax_no,
                                "represent_email" => $user_first_listing->represent_email,
                                "relation_with_company" => $user_first_listing->relation_with_company,
                                "work_tutor_fit" => $user_first_listing->work_tutor_fit,
                                "represent_phone" => $user_first_listing->represent_phone,
                                "location_email" => $user_first_listing->location_email,
                                "location_telephone" => $user_first_listing->location_telephone,
                                "workplace_address" => $user_first_listing->workplace_address,
                                "wstreet_number" => $user_first_listing->wstreet_number,
                                "wroute" => $user_first_listing->wroute,
                                "wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
                                "wlocality" => $user_first_listing->wlocality,
                                "wpostal_code" => $user_first_listing->wpostal_code,
                                "wcountry" => $user_first_listing->wcountry,
                                "work_other_check" => $user_first_listing->work_other_check,
                                "location_other_address" => $user_first_listing->location_other_address,
                                "work_schedule" => $user_first_listing->work_schedule,
                                "work_hours" => $user_first_listing->work_hours,
                                "status" => 0,
                                "property_status" => 'paid',
                                "bed_guest_room_id" => $room_id,
                                "number_of_guests_per_bed" => '1'
                            );


                            $obj = $this->propertyRepository->create($cloned_listing_for_class_place);
                            if($obj){
                                $place_id = $obj->id;
                                $pricing = new \App\PropertyPricing();
                                $pricing->user_id = \Auth::user()->id;
                                $pricing->property_id = $place_id;
                                $pricing->currency_id = $payment_currency;
                                $pricing->payment_type = $payment_type;
                                $pricing->amount = $payment_amount;
                                $pricing->expected_extra_costs_value = '';
                                $pricing->expected_extra_costs_description = '';
                                $pricing->save();

                            }
                            $place_id = $obj->id;
                            if($place_ids == ''){
                                $place_ids .= $place_id;
                            }else{
                                $place_ids .= '=='.$place_id;
                            }

                            $place_counter++;
                        }

                    }
                }else{
                    $place_counter = 1;
                    $place_ids = '';
                    while($place_counter <= $room_places){

                        $cloned_listing_for_class_place = array("work_org_same" => $user_first_listing->work_org_same,
                            "addr_work_org_same" => $user_first_listing->addr_work_org_same,
                            "user_id" => $user_first_listing->user_id,
                            "property_type" => 24,
                            "room_type" => 0,
                            "title" => '',
                            "exptitle" => $room_title.' - seat '.$place_counter,
                            "description" => $room_description,
                            "optional_description" => $room_description,
                            "legal_nature" => $user_first_listing->legal_nature,
                            "work_category" => $user_first_listing->work_category,
                            "hr_number" => $user_first_listing->hr_number,
                            "accomodates" => $user_first_listing->accomodates,
                            "legal_address" => $user_first_listing->legal_address,
                            "lstreet_number" => $user_first_listing->lstreet_number,
                            "lroute" => $user_first_listing->lroute,
                            "ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
                            "llocality" => $user_first_listing->llocality,
                            "lpostal_code" => $user_first_listing->lpostal_code,
                            "lcountry" => $user_first_listing->lcountry,
                            "city" => $user_first_listing->city,
                            "accesscode" => $user_first_listing->accesscode,
                            "streetAddress" => $user_first_listing->streetAddress,
                            "state" => $user_first_listing->state,
                            "country" => $user_first_listing->country,
                            "country_id" => $user_first_listing->country_id,
                            "zipcode" => $user_first_listing->zipcode,
                            "pro_lat" => $user_first_listing->pro_lat,
                            "pro_lon" => $user_first_listing->pro_lon,
                            "auto_complete" => $user_first_listing->auto_complete,
                            "bed_rooms" => $user_first_listing->bed_rooms,
                            "beds" => $user_first_listing->beds,
                            "bathrooms" => $user_first_listing->bathrooms,
                            "price_night" => $user_first_listing->price_night,
                            "secutity_deposit" => $user_first_listing->secutity_deposit,
                            "cancellation_policy" => $user_first_listing->cancellation_policy,
                            "booking_style" => $user_first_listing->booking_style,
                            "houserules" => $user_first_listing->houserules,
                            "min_stay" => $user_first_listing->min_stay,
                            "max_stay" => $user_first_listing->max_stay,
                            "calendar_availability" => $user_first_listing->calendar_availability,
                            "fromDate" => $user_first_listing->fromDate,
                            "toDate" => $user_first_listing->toDate,
                            "represent_name" => $user_first_listing->represent_name,
                            "represent_surname" => $user_first_listing->represent_surname,
                            "represent_born" => $user_first_listing->represent_born,
                            "represent_tax_no" => $user_first_listing->represent_tax_no,
                            "represent_email" => $user_first_listing->represent_email,
                            "relation_with_company" => $user_first_listing->relation_with_company,
                            "work_tutor_fit" => $user_first_listing->work_tutor_fit,
                            "represent_phone" => $user_first_listing->represent_phone,
                            "location_email" => $user_first_listing->location_email,
                            "location_telephone" => $user_first_listing->location_telephone,
                            "workplace_address" => $user_first_listing->workplace_address,
                            "wstreet_number" => $user_first_listing->wstreet_number,
                            "wroute" => $user_first_listing->wroute,
                            "wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
                            "wlocality" => $user_first_listing->wlocality,
                            "wpostal_code" => $user_first_listing->wpostal_code,
                            "wcountry" => $user_first_listing->wcountry,
                            "work_other_check" => $user_first_listing->work_other_check,
                            "location_other_address" => $user_first_listing->location_other_address,
                            "work_schedule" => $user_first_listing->work_schedule,
                            "work_hours" => $user_first_listing->work_hours,
                            "status" => 0,
                            "property_status" => 'paid',
                            "bed_guest_room_id" => $room_id,
                            "number_of_guests_per_bed" => '1'
                        );


                        $obj = $this->propertyRepository->create($cloned_listing_for_class_place);
                        if($obj){
                            $place_id = $obj->id;
                            $pricing = new \App\PropertyPricing();
                            $pricing->user_id = \Auth::user()->id;
                            $pricing->property_id = $place_id;
                            $pricing->currency_id = $payment_currency;
                            $pricing->payment_type = $payment_type;
                            $pricing->amount = $payment_amount;
                            $pricing->expected_extra_costs_value = '';
                            $pricing->expected_extra_costs_description = '';
                            $pricing->save();

                        }
                        $place_id = $obj->id;
                        if($place_ids == ''){
                            $place_ids .= $place_id;
                        }else{
                            $place_ids .= '=='.$place_id;
                        }

                        $place_counter++;
                    }
                }

                if($room_images != NULL && is_array($room_images)){
                    //$res = GuestRoomImages::where('room_id',$room_id)->delete();
                    $guest_room_images = GuestRoomImages::where(["room_id" => $room_id])->get();
                    if(count($guest_room_images) > 0){
                        foreach($guest_room_images as $room_image){
                            $room_image_name = $room_image->img_name;
                            if(!in_array($room_image_name,$room_images)){
                                $img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/'.$room_image_name;
                                $img_thumbnail_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
                                if(file_exists($img_path)){
                                    unlink($img_path);
                                }
                                if(file_exists($img_thumbnail_path)){
                                    unlink($img_thumbnail_path);
                                }
                                //Remove that Image from DB Tables
                                GuestRoomImages::where(["id" => $room_image->id])->delete();
                            }

                        }

                    }

                    foreach($room_images as $room_image){
                        if($room_image !== ""){
                            $imgObj = new GuestRoomImages;
                            $imgObj->img_name = $room_image;
                            $imgObj->img_src = "";
                            $imgObj->status = 1;
                            $imgObj->created_at = date("Y-m-d H:i:s");
                            $imgObj->updated_at = date("Y-m-d H:i:s");
                            $imgObj->room_id =$room_id;
                            $imgObj->save();
                        }
                    }
                }

                echo 'success,'.$room_id,','.$place_ids;die;
            }

            echo 'error';die;
        }




        public function updateRoom(Request $request) {
			
			$room_type_id = $request->get('room_type_id');			
			$user_id = \Auth::user()->id;
			$room_type = \App\RealRoomTypes::where("is_active", "1")->where("id", $room_type_id)->first();
			
			$room_id = $request->get('room_id');
			$room_beds = $request->get('room_beds');
			$property_id = $request->get('property_id');
			$room_title = $room_type->room_type.' Room '; 
			$room_description = $request->get('room_description');
			
			$room_toilet = $request->get('room_toilet');
			$room_shower = $request->get('room_shower');
			$room_facilities = $request->get('room_facilities');
			$room_images = $request->get('room_images');
			//var_dump($room_images);
			$guest_room_data = array();
			$guest_room = new \App\UserPaidGuestRoom();
			$guest_room_data['user_id'] = $user_id;
			$guest_room_data['room_name'] = $room_title;
			$guest_room_data['room_type_id'] = $room_type_id;
			$guest_room_data['is_active'] = 0;
			$guest_room_data['room_description'] = $room_description;
			
			$roomFacilitiesText = "";
			if (is_array($room_facilities)) {
				foreach ($room_facilities as $facility) {
					$roomFacilitiesText .= $facility . ",";
				}
			}
			if ($this->checkCommaExists($roomFacilitiesText)) {
				$roomFacilitiesText = $this->trimLastCharacter($roomFacilitiesText);
			}
			$guest_room_data['room_facility'] = $roomFacilitiesText;
			$guest_room_data['room_guest_toilet'] = $room_toilet;
			$guest_room_data['room_guest_shower'] = $room_shower;
			
			if($guest_room->where('id', $room_id)->update($guest_room_data)){
				if($room_images != NULL && is_array($room_images)){
					//$res = GuestRoomImages::where('room_id',$room_id)->delete();
					$guest_room_images = GuestRoomImages::where(["room_id" => $room_id])->get();
					if(count($guest_room_images) > 0){
						foreach($guest_room_images as $room_image){
							$room_image_name = $room_image->img_name;
							if($room_image_name != ""){
							if(!in_array($room_image_name,$room_images)){
								$img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/'.$room_image_name;
								$img_thumbnail_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
								if(file_exists($img_path)){
									unlink($img_path);
								}
								if(file_exists($img_thumbnail_path)){
									unlink($img_thumbnail_path);
								} 
							}
							}
							
						}	
						//Remove All Images from DB Tables 
						GuestRoomImages::where(["room_id" => $room_id])->delete();
					} 
				
					foreach($room_images as $room_image){
						if($room_image !== ""){
							$imgObj = new GuestRoomImages;
							$imgObj->img_name = $room_image;
							$imgObj->img_src = "";
							$imgObj->status = 1;
							$imgObj->created_at = date("Y-m-d H:i:s");
							$imgObj->updated_at = date("Y-m-d H:i:s");
							$imgObj->room_id =$room_id;
							$imgObj->save();
						}
					}
				}
				
				//update room beds type
				if(isset($room_beds)){ 
					foreach($room_beds as $bed_id=>$bed_type_id){
						if($bed_type_id == 6){
							$number_of_guests_per_bed = 1;
						}else if($bed_type_id == 5){
							$number_of_guests_per_bed=1;
						}else if($bed_type_id == 4){
							$number_of_guests_per_bed=1;
						}else if($bed_type_id == 3){
							$number_of_guests_per_bed=1;
						}else if($bed_type_id == 2){
							$number_of_guests_per_bed=2;
						}else{
							$number_of_guests_per_bed=1;
						}
						
						$room_type = \App\RealRoomTypes::where("is_active", "1")->where("id", $room_type_id)->first();
						
						$bed_type = \App\BedType::where(["is_active" => "1", "id" => $bed_type_id])->first();
						//print_r($bed_type);die;
						
						$room_name = $room_type->room_type.' Room ';
						$bed_name = $bed_type->bed_type.' Bed';
						$bed_title = $room_name .'-'.$bed_name;  
						if($bed_id != 'new'){
							$user_id = \Auth::user()->id;
							$bed_data = array("bed_type_id" => $bed_type_id,
											"exptitle"=>$bed_title,
											"status"=>0,
											"number_of_guests_per_bed" => $number_of_guests_per_bed);
							$res = $this->propertyRepository->update($bed_data,$bed_id);
						}else{
							//create new bed listing here
							
							$listing_user_id = \Auth::user()->id;
							$data = array('user_id' => \Auth::user()->id);
							$user_first_listing = Property::where('user_id',$listing_user_id)->where('property_type',0)->first();
							
							if(isset($user_first_listing->id)){
								$user_first_listing_id = $user_first_listing->id;
								
							}
							
							$cloned_listing_for_bed = array("work_org_same" => $user_first_listing->work_org_same,
															"addr_work_org_same" => $user_first_listing->addr_work_org_same,
															"user_id" => $user_first_listing->user_id,
															"property_type" => 26,
															"room_type" => 0,
															"title" => '',
															"exptitle" => $bed_title,
															"description" => $user_first_listing->description,
															"optional_description" => $user_first_listing->optional_description,
															"legal_nature" => $user_first_listing->legal_nature,
															"work_category" => $user_first_listing->work_category,
															"hr_number" => $user_first_listing->hr_number,
															"accomodates" => $user_first_listing->accomodates,
															"legal_address" => $user_first_listing->legal_address,
															"lstreet_number" => $user_first_listing->lstreet_number,
															"lroute" => $user_first_listing->lroute,
															"ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
															"llocality" => $user_first_listing->llocality,
															"lpostal_code" => $user_first_listing->lpostal_code,
															"lcountry" => $user_first_listing->lcountry,
															"city" => $user_first_listing->city,
															"accesscode" => $user_first_listing->accesscode,
															"streetAddress" => $user_first_listing->streetAddress,
															"state" => $user_first_listing->state,
															"country" => $user_first_listing->country,
															"country_id" => $user_first_listing->country_id,
															"zipcode" => $user_first_listing->zipcode,
															"pro_lat" => $user_first_listing->pro_lat,
															"pro_lon" => $user_first_listing->pro_lon,
															"auto_complete" => $user_first_listing->auto_complete,
															"bed_rooms" => $user_first_listing->bed_rooms,
															"beds" => $user_first_listing->beds,
															"bathrooms" => $user_first_listing->bathrooms,
															"price_night" => $user_first_listing->price_night,
															"secutity_deposit" => $user_first_listing->secutity_deposit,
															"cancellation_policy" => $user_first_listing->cancellation_policy,
															"booking_style" => $user_first_listing->booking_style,
															"houserules" => $user_first_listing->houserules,
															"min_stay" => $user_first_listing->min_stay,
															"max_stay" => $user_first_listing->max_stay,
															"calendar_availability" => $user_first_listing->calendar_availability,
															"fromDate" => $user_first_listing->fromDate,
															"toDate" => $user_first_listing->toDate,
															"represent_name" => $user_first_listing->represent_name,
															"represent_surname" => $user_first_listing->represent_surname,
															"represent_born" => $user_first_listing->represent_born,
															"represent_tax_no" => $user_first_listing->represent_tax_no,
															"represent_email" => $user_first_listing->represent_email,
															"relation_with_company" => $user_first_listing->relation_with_company,
															"work_tutor_fit" => $user_first_listing->work_tutor_fit,
															"represent_phone" => $user_first_listing->represent_phone,
															"location_email" => $user_first_listing->location_email,
															"location_telephone" => $user_first_listing->location_telephone,
															"workplace_address" => $user_first_listing->workplace_address,
															"wstreet_number" => $user_first_listing->wstreet_number,
															"wroute" => $user_first_listing->wroute,
															"wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
															"wlocality" => $user_first_listing->wlocality,
															"wpostal_code" => $user_first_listing->wpostal_code,
															"wcountry" => $user_first_listing->wcountry,
															"work_other_check" => $user_first_listing->work_other_check,
															"location_other_address" => $user_first_listing->location_other_address,
															"work_schedule" => $user_first_listing->work_schedule,
															"work_hours" => $user_first_listing->work_hours,
															"status" => 0,
															"premium" => $user_first_listing->premium,
															"paidservice" => $user_first_listing->paidservice,
															"human_resources" => $user_first_listing->human_resources,
															"hr_represent_name" => $user_first_listing->hr_represent_name,
															"hr_represent_surname" => $user_first_listing->hr_represent_surname ,
															"hr_represent_email" => $user_first_listing->hr_represent_email,
															"hr_represent_phone" => $user_first_listing->hr_represent_phone,
															"is_hr_email_sent" => $user_first_listing->is_hr_email_sent,
															"is_confirmed_by_tutor" => $user_first_listing->is_confirmed_by_tutor,
															"property_status" => 'paid',
															"link_to_extra_docs_necessary" => $user_first_listing->link_to_extra_docs_necessary,
															"class_type_id" => $user_first_listing->class_type_id,
															"class_duration_frequency" => $user_first_listing->class_duration_frequency,
															"bed_guest_room_id" => $room_id,
															"bed_type_id" => $bed_type_id,
															"number_of_guests_per_bed" => $number_of_guests_per_bed,
															"activity_type_id" => $user_first_listing->activity_type_id,
															"transfer_type_id" => $user_first_listing->transfer_type_id,
															"tour_type_id" => $user_first_listing->tour_type_id,
															"wow_type_id" => $user_first_listing->wow_type_id
															);
							
							
							$obj = $this->propertyRepository->create($cloned_listing_for_bed);		
							if($obj){
								$bed_id = $obj->id;
								$pricing = new \App\PropertyPricing();						
								$pricing->user_id = \Auth::user()->id;
								$pricing->property_id = $bed_id;
								$pricing->currency_id = 5;
								$pricing->payment_type = 'weekly';
								$pricing->amount = 200;
								$pricing->expected_extra_costs_value = '';
								$pricing->expected_extra_costs_description = '';
								$pricing->save();
								
							}
						}
						
					}  
				} 
				
				echo 'success,'.$room_id;die;  
			}
			
			
			
			
			echo 'error';die;
		}
		
		//Adding new class room
        public function saveClassRoom(Request $request) {
			
			$user_id = \Auth::user()->id;
			$listing_user_id = \Auth::user()->id;
			$data = array('user_id' => \Auth::user()->id);
			$user_first_listing = Property::where('user_id',$listing_user_id)->where('property_type',0)->first();
			
			$room_title = $request->get('room_title');
			$pre_made_id = $request->get('pre_made_id');
			$room_description = $request->get('class_description');
			$number_places = $request->get('number_places');
			$payment_currency = $request->get('payment_currency');
			$payment_type = $request->get('payment_type');
			$payment_amount = $request->get('payment_amount');
			
			$room_facilities = $request->get('classroom_facilities');
			$room_images = $request->get('classroom_images');
			
			
			$guest_room = new \App\UserPaidGuestRoom();
			$guest_room->user_id = $user_id;
			$guest_room->room_name = $room_title;
			if(isset($pre_made_id) && !empty($pre_made_id)){
				$guest_room->pre_made_id = $pre_made_id;
			}  
			
			$guest_room->room_description = $room_description;
			
			$roomFacilitiesText = "";
			if (is_array($room_facilities)) {
				foreach ($room_facilities as $facility) {
					$roomFacilitiesText .= $facility . ",";
				}
			}
			if ($this->checkCommaExists($roomFacilitiesText)) {
				$roomFacilitiesText = $this->trimLastCharacter($roomFacilitiesText);
			}
			$guest_room->room_facility = $roomFacilitiesText;
			$guest_room->room_guest_toilet = '';
			$guest_room->room_guest_shower = '';
			$guest_room->service_type = 'classroom';
			$guest_room->is_active = 0;
			 
			if($guest_room->save()){
				$room_id = $guest_room->id;
				if(count($room_images) > 0){
					foreach($room_images as $room_image){
						if($room_image == 'School-House-under-16.png'){
							$room_image = 'School-House-under-16.png';
							$room_image_source = public_path() . '/frontendassets/images/pre-made-classes/School-House-under-16.png';
							if(!\File::exists($room_image_source)){
								continue;
							}
							
							$room_image_path_check = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/';    
							$room_image_destination = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/School-House-under-16.png';    
							
							if (!\File::exists($room_image_path_check)) {
								\File::makeDirectory($room_image_path_check, $mode = 0777, true, true);
							}
							 
							if(!copy($room_image_source,$room_image_destination)){ 
								continue;
							} 
							
							$destination_thumbnail_path = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail/School-House-under-16.png";
							$destination_thumbnail_path_check = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail";
							if (!\File::exists($destination_thumbnail_path_check)) {
								\File::makeDirectory($destination_thumbnail_path_check, $mode = 0777, true, true);
							}
							  
							$canvas = \Image::canvas(370,350);
							$img = \Image::make($room_image_source)->resize(370,350,function($constraint){
								$constraint->aspectRatio();
							});
							$canvas->insert($img,'center');
							$canvas->save($destination_thumbnail_path);  
								
							
							
							
								
						}else if($room_image == 'Maute-Student-signup.png'){
							$room_image = 'Maute-Student-signup.png';
							$room_image_source = public_path() . '/frontendassets/images/pre-made-classes/Maute-Student-signup.png';
							if(!\File::exists($room_image_source)){
								continue;
							}
							   
							$room_image_path_check = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/';    
							$room_image_destination = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/Maute-Student-signup.png';    
							  
							if (!\File::exists($room_image_path_check)) {
								\File::makeDirectory($room_image_path_check, $mode = 0777, true, true);
							}
							 
							if(!copy($room_image_source,$room_image_destination)){
								continue;
							} 
							
							$destination_thumbnail_path = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail/Maute-Student-signup.png";
							$destination_thumbnail_path_check = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail";
							if (!\File::exists($destination_thumbnail_path_check)) {
								\File::makeDirectory($destination_thumbnail_path_check, $mode = 0777, true, true);
							}
							$canvas = \Image::canvas(370,350);
							$img = \Image::make($room_image_source)->resize(370,350,function($constraint){
								$constraint->aspectRatio();
							});
							$canvas->insert($img,'center');
							$canvas->save($destination_thumbnail_path);     
								
							
						}
						//die;
						$imgObj = new GuestRoomImages;
						$imgObj->img_name = $room_image;
						$imgObj->img_src = "";
						$imgObj->status = 1;
						$imgObj->created_at = date("Y-m-d H:i:s");
						$imgObj->updated_at = date("Y-m-d H:i:s");
						$imgObj->room_id =$room_id;
						$imgObj->save();
					}
				} 
				
				// create new listings for this class places with property_type 24
				$place_counter = 1;
				$place_ids = '';
				while($place_counter <= $number_places){ 
					
					$cloned_listing_for_class_place = array("work_org_same" => $user_first_listing->work_org_same,
											"addr_work_org_same" => $user_first_listing->addr_work_org_same,
											"user_id" => $user_first_listing->user_id,
											"property_type" => 24,
											"room_type" => 0,
											"title" => '',
											"exptitle" => $room_title.' - seat '.$place_counter,
											"description" => $room_description,
											"optional_description" => $room_description,
											"legal_nature" => $user_first_listing->legal_nature,
											"work_category" => $user_first_listing->work_category,
											"hr_number" => $user_first_listing->hr_number,
											"accomodates" => $user_first_listing->accomodates,
											"legal_address" => $user_first_listing->legal_address,
											"lstreet_number" => $user_first_listing->lstreet_number,
											"lroute" => $user_first_listing->lroute,
											"ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
											"llocality" => $user_first_listing->llocality,
											"lpostal_code" => $user_first_listing->lpostal_code,
											"lcountry" => $user_first_listing->lcountry,
											"city" => $user_first_listing->city,
											"accesscode" => $user_first_listing->accesscode,
											"streetAddress" => $user_first_listing->streetAddress,
											"state" => $user_first_listing->state,
											"country" => $user_first_listing->country,
											"country_id" => $user_first_listing->country_id,
											"zipcode" => $user_first_listing->zipcode,
											"pro_lat" => $user_first_listing->pro_lat,
											"pro_lon" => $user_first_listing->pro_lon,
											"auto_complete" => $user_first_listing->auto_complete,
											"bed_rooms" => $user_first_listing->bed_rooms,
											"beds" => $user_first_listing->beds,
											"bathrooms" => $user_first_listing->bathrooms,
											"price_night" => $user_first_listing->price_night,
											"secutity_deposit" => $user_first_listing->secutity_deposit,
											"cancellation_policy" => $user_first_listing->cancellation_policy,
											"booking_style" => $user_first_listing->booking_style,
											"houserules" => $user_first_listing->houserules,
											"min_stay" => $user_first_listing->min_stay,
											"max_stay" => $user_first_listing->max_stay,
											"calendar_availability" => $user_first_listing->calendar_availability,
											"fromDate" => $user_first_listing->fromDate,
											"toDate" => $user_first_listing->toDate,
											"represent_name" => $user_first_listing->represent_name,
											"represent_surname" => $user_first_listing->represent_surname,
											"represent_born" => $user_first_listing->represent_born,
											"represent_tax_no" => $user_first_listing->represent_tax_no,
											"represent_email" => $user_first_listing->represent_email,
											"relation_with_company" => $user_first_listing->relation_with_company,
											"work_tutor_fit" => $user_first_listing->work_tutor_fit,
											"represent_phone" => $user_first_listing->represent_phone,
											"location_email" => $user_first_listing->location_email,
											"location_telephone" => $user_first_listing->location_telephone,
											"workplace_address" => $user_first_listing->workplace_address,
											"wstreet_number" => $user_first_listing->wstreet_number,
											"wroute" => $user_first_listing->wroute,
											"wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
											"wlocality" => $user_first_listing->wlocality,
											"wpostal_code" => $user_first_listing->wpostal_code,
											"wcountry" => $user_first_listing->wcountry,
											"work_other_check" => $user_first_listing->work_other_check,
											"location_other_address" => $user_first_listing->location_other_address,
											"work_schedule" => $user_first_listing->work_schedule,
											"work_hours" => $user_first_listing->work_hours,
											"status" => 0, 											
											"property_status" => 'paid',
											"bed_guest_room_id" => $room_id,
											"number_of_guests_per_bed" => '1'
											);
			
			
					$obj = $this->propertyRepository->create($cloned_listing_for_class_place);	
					if($obj){
						$place_id = $obj->id;
						$pricing = new \App\PropertyPricing();						
						$pricing->user_id = \Auth::user()->id;
						$pricing->property_id = $place_id;
						$pricing->currency_id = $payment_currency;
						$pricing->payment_type = $payment_type;
						$pricing->amount = $payment_amount;
						$pricing->expected_extra_costs_value = '';
						$pricing->expected_extra_costs_description = '';
						$pricing->save();
						
					}
					$place_id = $obj->id;
					if($place_ids == ''){
						$place_ids .= $place_id;
					}else{
						$place_ids .= '=='.$place_id;
					}
					
					$place_counter++;  
				}
				
				echo 'success,'.$room_id.','.$place_ids;die;  
			}
			
			echo 'error';die;
		}

        //Adding new Activity
        public function saveActivity(Request $request) {

            $user_id = \Auth::user()->id;
            $listing_user_id = \Auth::user()->id;
            $data = array('user_id' => \Auth::user()->id);
            $user_first_listing = Property::where('user_id',$listing_user_id)->where('property_type',0)->first();

            $room_title = $request->get('room_title');
            $pre_made_id = $request->get('pre_made_id');
            $room_description = $request->get('class_description');
            $number_places = $request->get('number_places');
            $payment_currency = $request->get('payment_currency');
            $payment_type = $request->get('payment_type');
            $payment_amount = $request->get('payment_amount');
            $expected_cost = $request->get('expected_cost');
            $expected_description = $request->get('expected_description');

            $room_facilities = $request->get('classroom_facilities');
            $room_images = $request->get('classroom_images');


            $guest_room = new \App\UserPaidGuestRoom();
            $guest_room->user_id = $user_id;
            $guest_room->room_name = $room_title;
            if(isset($pre_made_id) && !empty($pre_made_id)){
                $guest_room->pre_made_id = $pre_made_id;
            }

            $guest_room->room_description = $room_description;

            $roomFacilitiesText = "";
            if (is_array($room_facilities)) {
                foreach ($room_facilities as $facility) {
                    $roomFacilitiesText .= $facility . ",";
                }
            }
            if ($this->checkCommaExists($roomFacilitiesText)) {
                $roomFacilitiesText = $this->trimLastCharacter($roomFacilitiesText);
            }
            $guest_room->room_facility = $roomFacilitiesText;
            $guest_room->room_guest_toilet = '';
            $guest_room->room_guest_shower = '';
            $guest_room->service_type = 'activity';
            $guest_room->is_active = 0;

            if($guest_room->save()){
                $room_id = $guest_room->id;
                if(($room_images)){
                    foreach($room_images as $room_image){
                        if($room_image == 'School-House-under-16.png'){
                            $room_image = 'School-House-under-16.png';
                            $room_image_source = public_path() . '/frontendassets/images/pre-made-classes/School-House-under-16.png';
                            if(!\File::exists($room_image_source)){
                                continue;
                            }

                            $room_image_path_check = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/';
                            $room_image_destination = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/School-House-under-16.png';

                            if (!\File::exists($room_image_path_check)) {
                                \File::makeDirectory($room_image_path_check, $mode = 0777, true, true);
                            }

                            if(!copy($room_image_source,$room_image_destination)){
                                continue;
                            }

                            $destination_thumbnail_path = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail/School-House-under-16.png";
                            $destination_thumbnail_path_check = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail";
                            if (!\File::exists($destination_thumbnail_path_check)) {
                                \File::makeDirectory($destination_thumbnail_path_check, $mode = 0777, true, true);
                            }

                            $canvas = \Image::canvas(370,350);
                            $img = \Image::make($room_image_source)->resize(370,350,function($constraint){
                                $constraint->aspectRatio();
                            });
                            $canvas->insert($img,'center');
                            $canvas->save($destination_thumbnail_path);





                        }else if($room_image == 'Maute-Student-signup.png'){
                            $room_image = 'Maute-Student-signup.png';
                            $room_image_source = public_path() . '/frontendassets/images/pre-made-classes/Maute-Student-signup.png';
                            if(!\File::exists($room_image_source)){
                                continue;
                            }

                            $room_image_path_check = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/';
                            $room_image_destination = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/Maute-Student-signup.png';

                            if (!\File::exists($room_image_path_check)) {
                                \File::makeDirectory($room_image_path_check, $mode = 0777, true, true);
                            }

                            if(!copy($room_image_source,$room_image_destination)){
                                continue;
                            }

                            $destination_thumbnail_path = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail/Maute-Student-signup.png";
                            $destination_thumbnail_path_check = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail";
                            if (!\File::exists($destination_thumbnail_path_check)) {
                                \File::makeDirectory($destination_thumbnail_path_check, $mode = 0777, true, true);
                            }
                            $canvas = \Image::canvas(370,350);
                            $img = \Image::make($room_image_source)->resize(370,350,function($constraint){
                                $constraint->aspectRatio();
                            });
                            $canvas->insert($img,'center');
                            $canvas->save($destination_thumbnail_path);


                        }
                        //die;
                        $imgObj = new GuestRoomImages;
                        $imgObj->img_name = $room_image;
                        $imgObj->img_src = "";
                        $imgObj->status = 1;
                        $imgObj->created_at = date("Y-m-d H:i:s");
                        $imgObj->updated_at = date("Y-m-d H:i:s");
                        $imgObj->room_id =$room_id;
                        $imgObj->save();
                    }
                }

                // create new listings for this class places with property_type 24
                $place_counter = 1;
                $place_ids = '';
                while($place_counter <= $number_places){

                    $cloned_listing_for_class_place = array("work_org_same" => $user_first_listing->work_org_same,
                        "addr_work_org_same" => $user_first_listing->addr_work_org_same,
                        "user_id" => $user_first_listing->user_id,
                        "property_type" => 24,
                        "room_type" => 0,
                        "title" => '',
                        "exptitle" => $room_title.' - seat '.$place_counter,
                        "description" => $room_description,
                        "optional_description" => $room_description,
                        "legal_nature" => $user_first_listing->legal_nature,
                        "work_category" => $user_first_listing->work_category,
                        "hr_number" => $user_first_listing->hr_number,
                        "accomodates" => $user_first_listing->accomodates,
                        "legal_address" => $user_first_listing->legal_address,
                        "lstreet_number" => $user_first_listing->lstreet_number,
                        "lroute" => $user_first_listing->lroute,
                        "ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
                        "llocality" => $user_first_listing->llocality,
                        "lpostal_code" => $user_first_listing->lpostal_code,
                        "lcountry" => $user_first_listing->lcountry,
                        "city" => $user_first_listing->city,
                        "accesscode" => $user_first_listing->accesscode,
                        "streetAddress" => $user_first_listing->streetAddress,
                        "state" => $user_first_listing->state,
                        "country" => $user_first_listing->country,
                        "country_id" => $user_first_listing->country_id,
                        "zipcode" => $user_first_listing->zipcode,
                        "pro_lat" => $user_first_listing->pro_lat,
                        "pro_lon" => $user_first_listing->pro_lon,
                        "auto_complete" => $user_first_listing->auto_complete,
                        "bed_rooms" => $user_first_listing->bed_rooms,
                        "beds" => $user_first_listing->beds,
                        "bathrooms" => $user_first_listing->bathrooms,
                        "price_night" => $user_first_listing->price_night,
                        "secutity_deposit" => $user_first_listing->secutity_deposit,
                        "cancellation_policy" => $user_first_listing->cancellation_policy,
                        "booking_style" => $user_first_listing->booking_style,
                        "houserules" => $user_first_listing->houserules,
                        "min_stay" => $user_first_listing->min_stay,
                        "max_stay" => $user_first_listing->max_stay,
                        "calendar_availability" => $user_first_listing->calendar_availability,
                        "fromDate" => $user_first_listing->fromDate,
                        "toDate" => $user_first_listing->toDate,
                        "represent_name" => $user_first_listing->represent_name,
                        "represent_surname" => $user_first_listing->represent_surname,
                        "represent_born" => $user_first_listing->represent_born,
                        "represent_tax_no" => $user_first_listing->represent_tax_no,
                        "represent_email" => $user_first_listing->represent_email,
                        "relation_with_company" => $user_first_listing->relation_with_company,
                        "work_tutor_fit" => $user_first_listing->work_tutor_fit,
                        "represent_phone" => $user_first_listing->represent_phone,
                        "location_email" => $user_first_listing->location_email,
                        "location_telephone" => $user_first_listing->location_telephone,
                        "workplace_address" => $user_first_listing->workplace_address,
                        "wstreet_number" => $user_first_listing->wstreet_number,
                        "wroute" => $user_first_listing->wroute,
                        "wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
                        "wlocality" => $user_first_listing->wlocality,
                        "wpostal_code" => $user_first_listing->wpostal_code,
                        "wcountry" => $user_first_listing->wcountry,
                        "work_other_check" => $user_first_listing->work_other_check,
                        "location_other_address" => $user_first_listing->location_other_address,
                        "work_schedule" => $user_first_listing->work_schedule,
                        "work_hours" => $user_first_listing->work_hours,
                        "status" => 0,
                        "property_status" => 'paid',
                        "bed_guest_room_id" => $room_id,
                        "number_of_guests_per_bed" => '1'
                    );


                    $obj = $this->propertyRepository->create($cloned_listing_for_class_place);
                    if($obj){
                        $place_id = $obj->id;
                        $pricing = new \App\PropertyPricing();
                        $pricing->user_id = \Auth::user()->id;
                        $pricing->property_id = $place_id;
                        $pricing->currency_id = $payment_currency;
                        $pricing->payment_type = $payment_type;
                        $pricing->amount = $payment_amount;
                        $pricing->expected_extra_costs_value = '';
                        $pricing->expected_extra_costs_description = '';
                        $pricing->save();

                    }
                    $place_id = $obj->id;
                    if($place_ids == ''){
                        $place_ids .= $place_id;
                    }else{
                        $place_ids .= '=='.$place_id;
                    }

                    $place_counter++;
                }

                echo 'success,'.$room_id.','.$place_ids;die;
            }

            echo 'error';die;
        }

        //Adding new Diet
        public function saveDiet(Request $request) {

            $user_id = \Auth::user()->id;
            $listing_user_id = \Auth::user()->id;
            $data = array('user_id' => \Auth::user()->id);
            $user_first_listing = Property::where('user_id',$listing_user_id)->where('property_type',0)->first();

            $room_title = $request->get('room_title');
            $pre_made_id = $request->get('pre_made_id');
            $room_description = $request->get('class_description');
            $number_places = $request->get('number_places');
            $payment_currency = $request->get('payment_currency');
            $payment_type = $request->get('payment_type');
            $payment_amount = $request->get('payment_amount');

            $room_facilities = $request->get('classroom_facilities');
            $room_images = $request->get('classroom_images');


            $guest_room = new \App\UserPaidGuestRoom();
            $guest_room->user_id = $user_id;
            $guest_room->room_name = $room_title;
            if(isset($pre_made_id) && !empty($pre_made_id)){
                $guest_room->pre_made_id = $pre_made_id;
            }

            $guest_room->room_description = $room_description;

            $roomFacilitiesText = "";
            if (is_array($room_facilities)) {
                foreach ($room_facilities as $facility) {
                    $roomFacilitiesText .= $facility . ",";
                }
            }
            if ($this->checkCommaExists($roomFacilitiesText)) {
                $roomFacilitiesText = $this->trimLastCharacter($roomFacilitiesText);
            }
            $guest_room->room_facility = $roomFacilitiesText;
            $guest_room->room_guest_toilet = '';
            $guest_room->room_guest_shower = '';
            $guest_room->service_type = 'diet';
            $guest_room->is_active = 0;

            if($guest_room->save()){
                $room_id = $guest_room->id;
                if(($room_images)){
                    foreach($room_images as $room_image){
                        if($room_image == 'School-House-under-16.png'){
                            $room_image = 'School-House-under-16.png';
                            $room_image_source = public_path() . '/frontendassets/images/pre-made-classes/School-House-under-16.png';
                            if(!\File::exists($room_image_source)){
                                continue;
                            }

                            $room_image_path_check = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/';
                            $room_image_destination = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/School-House-under-16.png';

                            if (!\File::exists($room_image_path_check)) {
                                \File::makeDirectory($room_image_path_check, $mode = 0777, true, true);
                            }

                            if(!copy($room_image_source,$room_image_destination)){
                                continue;
                            }

                            $destination_thumbnail_path = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail/School-House-under-16.png";
                            $destination_thumbnail_path_check = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail";
                            if (!\File::exists($destination_thumbnail_path_check)) {
                                \File::makeDirectory($destination_thumbnail_path_check, $mode = 0777, true, true);
                            }

                            $canvas = \Image::canvas(370,350);
                            $img = \Image::make($room_image_source)->resize(370,350,function($constraint){
                                $constraint->aspectRatio();
                            });
                            $canvas->insert($img,'center');
                            $canvas->save($destination_thumbnail_path);





                        }else if($room_image == 'Maute-Student-signup.png'){
                            $room_image = 'Maute-Student-signup.png';
                            $room_image_source = public_path() . '/frontendassets/images/pre-made-classes/Maute-Student-signup.png';
                            if(!\File::exists($room_image_source)){
                                continue;
                            }

                            $room_image_path_check = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/';
                            $room_image_destination = public_path().'/images/rentals/'.$user_first_listing->id.'/rooms/Maute-Student-signup.png';

                            if (!\File::exists($room_image_path_check)) {
                                \File::makeDirectory($room_image_path_check, $mode = 0777, true, true);
                            }

                            if(!copy($room_image_source,$room_image_destination)){
                                continue;
                            }

                            $destination_thumbnail_path = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail/Maute-Student-signup.png";
                            $destination_thumbnail_path_check = public_path() . '/images/rentals/' . $user_first_listing->id . "/rooms/thumbnail";
                            if (!\File::exists($destination_thumbnail_path_check)) {
                                \File::makeDirectory($destination_thumbnail_path_check, $mode = 0777, true, true);
                            }
                            $canvas = \Image::canvas(370,350);
                            $img = \Image::make($room_image_source)->resize(370,350,function($constraint){
                                $constraint->aspectRatio();
                            });
                            $canvas->insert($img,'center');
                            $canvas->save($destination_thumbnail_path);


                        }
                        //die;
                        $imgObj = new GuestRoomImages;
                        $imgObj->img_name = $room_image;
                        $imgObj->img_src = "";
                        $imgObj->status = 1;
                        $imgObj->created_at = date("Y-m-d H:i:s");
                        $imgObj->updated_at = date("Y-m-d H:i:s");
                        $imgObj->room_id =$room_id;
                        $imgObj->save();
                    }
                }

                // create new listings for this class places with property_type 24
                $place_counter = 1;
                $place_ids = '';
                while($place_counter <= $number_places){

                    $cloned_listing_for_class_place = array("work_org_same" => $user_first_listing->work_org_same,
                        "addr_work_org_same" => $user_first_listing->addr_work_org_same,
                        "user_id" => $user_first_listing->user_id,
                        "property_type" => 24,
                        "room_type" => 0,
                        "title" => '',
                        "exptitle" => $room_title.' - seat '.$place_counter,
                        "description" => $room_description,
                        "optional_description" => $room_description,
                        "legal_nature" => $user_first_listing->legal_nature,
                        "work_category" => $user_first_listing->work_category,
                        "hr_number" => $user_first_listing->hr_number,
                        "accomodates" => $user_first_listing->accomodates,
                        "legal_address" => $user_first_listing->legal_address,
                        "lstreet_number" => $user_first_listing->lstreet_number,
                        "lroute" => $user_first_listing->lroute,
                        "ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
                        "llocality" => $user_first_listing->llocality,
                        "lpostal_code" => $user_first_listing->lpostal_code,
                        "lcountry" => $user_first_listing->lcountry,
                        "city" => $user_first_listing->city,
                        "accesscode" => $user_first_listing->accesscode,
                        "streetAddress" => $user_first_listing->streetAddress,
                        "state" => $user_first_listing->state,
                        "country" => $user_first_listing->country,
                        "country_id" => $user_first_listing->country_id,
                        "zipcode" => $user_first_listing->zipcode,
                        "pro_lat" => $user_first_listing->pro_lat,
                        "pro_lon" => $user_first_listing->pro_lon,
                        "auto_complete" => $user_first_listing->auto_complete,
                        "bed_rooms" => $user_first_listing->bed_rooms,
                        "beds" => $user_first_listing->beds,
                        "bathrooms" => $user_first_listing->bathrooms,
                        "price_night" => $user_first_listing->price_night,
                        "secutity_deposit" => $user_first_listing->secutity_deposit,
                        "cancellation_policy" => $user_first_listing->cancellation_policy,
                        "booking_style" => $user_first_listing->booking_style,
                        "houserules" => $user_first_listing->houserules,
                        "min_stay" => $user_first_listing->min_stay,
                        "max_stay" => $user_first_listing->max_stay,
                        "calendar_availability" => $user_first_listing->calendar_availability,
                        "fromDate" => $user_first_listing->fromDate,
                        "toDate" => $user_first_listing->toDate,
                        "represent_name" => $user_first_listing->represent_name,
                        "represent_surname" => $user_first_listing->represent_surname,
                        "represent_born" => $user_first_listing->represent_born,
                        "represent_tax_no" => $user_first_listing->represent_tax_no,
                        "represent_email" => $user_first_listing->represent_email,
                        "relation_with_company" => $user_first_listing->relation_with_company,
                        "work_tutor_fit" => $user_first_listing->work_tutor_fit,
                        "represent_phone" => $user_first_listing->represent_phone,
                        "location_email" => $user_first_listing->location_email,
                        "location_telephone" => $user_first_listing->location_telephone,
                        "workplace_address" => $user_first_listing->workplace_address,
                        "wstreet_number" => $user_first_listing->wstreet_number,
                        "wroute" => $user_first_listing->wroute,
                        "wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
                        "wlocality" => $user_first_listing->wlocality,
                        "wpostal_code" => $user_first_listing->wpostal_code,
                        "wcountry" => $user_first_listing->wcountry,
                        "work_other_check" => $user_first_listing->work_other_check,
                        "location_other_address" => $user_first_listing->location_other_address,
                        "work_schedule" => $user_first_listing->work_schedule,
                        "work_hours" => $user_first_listing->work_hours,
                        "status" => 0,
                        "property_status" => 'paid',
                        "bed_guest_room_id" => $room_id,
                        "number_of_guests_per_bed" => '1'
                    );


                    $obj = $this->propertyRepository->create($cloned_listing_for_class_place);
                    if($obj){
                        $place_id = $obj->id;
                        $pricing = new \App\PropertyPricing();
                        $pricing->user_id = \Auth::user()->id;
                        $pricing->property_id = $place_id;
                        $pricing->currency_id = $payment_currency;
                        $pricing->payment_type = $payment_type;
                        $pricing->amount = $payment_amount;
                        $pricing->expected_extra_costs_value = '';
                        $pricing->expected_extra_costs_description = '';
                        $pricing->save();

                    }
                    $place_id = $obj->id;
                    if($place_ids == ''){
                        $place_ids .= $place_id;
                    }else{
                        $place_ids .= '=='.$place_id;
                    }

                    $place_counter++;
                }

                echo 'success,'.$room_id.','.$place_ids;die;
            }

            echo 'error';die;
        }


        //Adding new bed room
        public function saveNewRoom(Request $request) {
			$user_id = \Auth::user()->id;
		
			$room_title = $request->get('room_title');
			$room_description = $request->get('room_description');
			$room_type_id = $request->get('room_type_id');
			$room_toilet = $request->get('room_toilet');
			$room_shower = $request->get('room_shower');
			$room_facilities = $request->get('room_facilities');
			$bed_rooms_type_ids = $request->get('bed_rooms_type_ids');
			$bed_rooms_types_html = $request->get('bed_rooms_types_html');
			$room_images = $request->get('room_images');
			
			
			$guest_room = new \App\UserPaidGuestRoom();
			$guest_room->user_id = $user_id;
			$guest_room->room_name = $room_title;
			$guest_room->room_type_id = $room_type_id;
			$guest_room->is_active = 0;
			$guest_room->room_description = $room_description;
			
			$roomFacilitiesText = "";
			if (is_array($room_facilities)) {
				foreach ($room_facilities as $facility) {
					$roomFacilitiesText .= $facility . ",";
				}
			}
			if ($this->checkCommaExists($roomFacilitiesText)) {
				$roomFacilitiesText = $this->trimLastCharacter($roomFacilitiesText);
			}
			$guest_room->room_facility = $roomFacilitiesText;
			$guest_room->room_guest_toilet = $room_toilet;
			$guest_room->room_guest_shower = $room_shower;
			$guest_room->service_type = 'bedroom';
			 
			if($guest_room->save()){
				$room_id = $guest_room->id;
				if(count($room_images) > 0){
					foreach($room_images as $room_image){
						$imgObj = new GuestRoomImages;
						$imgObj->img_name = $room_image;
						$imgObj->img_src = "";
						$imgObj->status = 1;
						$imgObj->created_at = date("Y-m-d H:i:s");
						$imgObj->updated_at = date("Y-m-d H:i:s");
						$imgObj->room_id =$room_id;
						$imgObj->save();
					}
				} 
				
				
				
				$bed_ids = '';
				if (is_array($bed_rooms_type_ids)) {
					foreach($bed_rooms_type_ids as $key=>$bed_type_id){				
						$bed_room_id = $room_id;
						
						$bed_title = $room_title. ' Room - '.$bed_rooms_types_html[$key].' Bed';
						if($bed_type_id == 6){
							$number_of_guests_per_bed = 1;
						}else if($bed_type_id == 5){
							$number_of_guests_per_bed=1;
						}else if($bed_type_id == 4){
							$number_of_guests_per_bed=1;
						}else if($bed_type_id == 3){
							$number_of_guests_per_bed=1;
						}else if($bed_type_id == 2){
							$number_of_guests_per_bed=2;
						}else{
							$number_of_guests_per_bed=1;
						}
						$listing_user_id = \Auth::user()->id;
						$data = array('user_id' => \Auth::user()->id);
						$user_first_listing = Property::where('user_id',$listing_user_id)->where('property_type',0)->first();
						
						if(isset($user_first_listing->id)){
							$user_first_listing_id = $user_first_listing->id;
							
						}
						
						$cloned_listing_for_bed = array("work_org_same" => $user_first_listing->work_org_same,
														"addr_work_org_same" => $user_first_listing->addr_work_org_same,
														"user_id" => $user_first_listing->user_id,
														"property_type" => 26,
														"room_type" => 0,
														"title" => '',
														"exptitle" => $bed_title,
														"description" => $user_first_listing->description,
														"optional_description" => $user_first_listing->optional_description,
														"legal_nature" => $user_first_listing->legal_nature,
														"work_category" => $user_first_listing->work_category,
														"hr_number" => $user_first_listing->hr_number,
														"accomodates" => $user_first_listing->accomodates,
														"legal_address" => $user_first_listing->legal_address,
														"lstreet_number" => $user_first_listing->lstreet_number,
														"lroute" => $user_first_listing->lroute,
														"ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
														"llocality" => $user_first_listing->llocality,
														"lpostal_code" => $user_first_listing->lpostal_code,
														"lcountry" => $user_first_listing->lcountry,
														"city" => $user_first_listing->city,
														"accesscode" => $user_first_listing->accesscode,
														"streetAddress" => $user_first_listing->streetAddress,
														"state" => $user_first_listing->state,
														"country" => $user_first_listing->country,
														"country_id" => $user_first_listing->country_id,
														"zipcode" => $user_first_listing->zipcode,
														"pro_lat" => $user_first_listing->pro_lat,
														"pro_lon" => $user_first_listing->pro_lon,
														"auto_complete" => $user_first_listing->auto_complete,
														"bed_rooms" => $user_first_listing->bed_rooms,
														"beds" => $user_first_listing->beds,
														"bathrooms" => $user_first_listing->bathrooms,
														"price_night" => $user_first_listing->price_night,
														"secutity_deposit" => $user_first_listing->secutity_deposit,
														"cancellation_policy" => $user_first_listing->cancellation_policy,
														"booking_style" => $user_first_listing->booking_style,
														"houserules" => $user_first_listing->houserules,
														"min_stay" => $user_first_listing->min_stay,
														"max_stay" => $user_first_listing->max_stay,
														"calendar_availability" => $user_first_listing->calendar_availability,
														"fromDate" => $user_first_listing->fromDate,
														"toDate" => $user_first_listing->toDate,
														"represent_name" => $user_first_listing->represent_name,
														"represent_surname" => $user_first_listing->represent_surname,
														"represent_born" => $user_first_listing->represent_born,
														"represent_tax_no" => $user_first_listing->represent_tax_no,
														"represent_email" => $user_first_listing->represent_email,
														"relation_with_company" => $user_first_listing->relation_with_company,
														"work_tutor_fit" => $user_first_listing->work_tutor_fit,
														"represent_phone" => $user_first_listing->represent_phone,
														"location_email" => $user_first_listing->location_email,
														"location_telephone" => $user_first_listing->location_telephone,
														"workplace_address" => $user_first_listing->workplace_address,
														"wstreet_number" => $user_first_listing->wstreet_number,
														"wroute" => $user_first_listing->wroute,
														"wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
														"wlocality" => $user_first_listing->wlocality,
														"wpostal_code" => $user_first_listing->wpostal_code,
														"wcountry" => $user_first_listing->wcountry,
														"work_other_check" => $user_first_listing->work_other_check,
														"location_other_address" => $user_first_listing->location_other_address,
														"work_schedule" => $user_first_listing->work_schedule,
														"work_hours" => $user_first_listing->work_hours,
														"status" => 0,
														"premium" => $user_first_listing->premium,
														"paidservice" => $user_first_listing->paidservice,
														"human_resources" => $user_first_listing->human_resources,
														"hr_represent_name" => $user_first_listing->hr_represent_name,
														"hr_represent_surname" => $user_first_listing->hr_represent_surname ,
														"hr_represent_email" => $user_first_listing->hr_represent_email,
														"hr_represent_phone" => $user_first_listing->hr_represent_phone,
														"is_hr_email_sent" => $user_first_listing->is_hr_email_sent,
														"is_confirmed_by_tutor" => $user_first_listing->is_confirmed_by_tutor,
														"property_status" => 'paid',
														"link_to_extra_docs_necessary" => $user_first_listing->link_to_extra_docs_necessary,
														"class_type_id" => $user_first_listing->class_type_id,
														"class_duration_frequency" => $user_first_listing->class_duration_frequency,
														"bed_guest_room_id" => $bed_room_id,
														"bed_type_id" => $bed_type_id,
														"number_of_guests_per_bed" => $number_of_guests_per_bed,
														"activity_type_id" => $user_first_listing->activity_type_id,
														"transfer_type_id" => $user_first_listing->transfer_type_id,
														"tour_type_id" => $user_first_listing->tour_type_id,
														"wow_type_id" => $user_first_listing->wow_type_id
														);
						
						
						$obj = $this->propertyRepository->create($cloned_listing_for_bed);		
						if($obj){
							$bed_id = $obj->id;
							$pricing = new \App\PropertyPricing();						
							$pricing->user_id = \Auth::user()->id;
							$pricing->property_id = $bed_id;
							$pricing->currency_id = 5;
							$pricing->payment_type = 'weekly';
							$pricing->amount = 200;
							$pricing->expected_extra_costs_value = '';
							$pricing->expected_extra_costs_description = '';
							$pricing->save();
							$bed_ids .= $bed_id.',';
						}
					}
					$bed_ids = rtrim($bed_ids,",");
				}  
				
				echo 'success,'.$room_id.'==='.$bed_ids;die;  
			}
			
			echo 'error';die;
			
		}
        public function saveNewBed(Request $request) {
			$listing_user_id = \Auth::user()->id;
			$data = array('user_id' => \Auth::user()->id);
			$user_first_listing = Property::where('user_id',$listing_user_id)->where('property_type',0)->first();
			
            if(isset($user_first_listing->id)){
                $user_first_listing_id = $user_first_listing->id;
                
            }
			$bed_room_id = $request->get('bed_room_id');
			$bed_type_id = $request->get('bed_type_id');
			if($bed_type_id == 6){
				$number_of_guests_per_bed = 1;
			}else if($bed_type_id == 5){
				$number_of_guests_per_bed=1;
			}else if($bed_type_id == 4){
				$number_of_guests_per_bed=1;
			}else if($bed_type_id == 3){
				$number_of_guests_per_bed=1;
			}else if($bed_type_id == 2){
				$number_of_guests_per_bed=2;
			}else{
				$number_of_guests_per_bed=1;
			}
			
			$cloned_listing_for_bed = array("work_org_same" => $user_first_listing->work_org_same,
											"addr_work_org_same" => $user_first_listing->addr_work_org_same,
											"user_id" => $user_first_listing->user_id,
											"property_type" => 26,
											"room_type" => 0,
											"title" => '',
											"exptitle" => $user_first_listing->exptitle,
											"description" => $user_first_listing->description,
											"optional_description" => $user_first_listing->optional_description,
											//"created_at" => 2020-02-06 09:46:11,
											//"updated_at" => 2020-02-26 19:34:21,
											"legal_nature" => $user_first_listing->legal_nature,
											"work_category" => $user_first_listing->work_category,
											"hr_number" => $user_first_listing->hr_number,
											"accomodates" => $user_first_listing->accomodates,
											"legal_address" => $user_first_listing->legal_address,
											"lstreet_number" => $user_first_listing->lstreet_number,
											"lroute" => $user_first_listing->lroute,
											"ladministrative_area_level_1" => $user_first_listing->ladministrative_area_level_1,
											"llocality" => $user_first_listing->llocality,
											"lpostal_code" => $user_first_listing->lpostal_code,
											"lcountry" => $user_first_listing->lcountry,
											"city" => $user_first_listing->city,
											"accesscode" => $user_first_listing->accesscode,
											"streetAddress" => $user_first_listing->streetAddress,
											"state" => $user_first_listing->state,
											"country" => $user_first_listing->country,
											"country_id" => $user_first_listing->country_id,
											"zipcode" => $user_first_listing->zipcode,
											"pro_lat" => $user_first_listing->pro_lat,
											"pro_lon" => $user_first_listing->pro_lon,
											"auto_complete" => $user_first_listing->auto_complete,
											"bed_rooms" => $user_first_listing->bed_rooms,
											"beds" => $user_first_listing->beds,
											"bathrooms" => $user_first_listing->bathrooms,
											"price_night" => $user_first_listing->price_night,
											"secutity_deposit" => $user_first_listing->secutity_deposit,
											"cancellation_policy" => $user_first_listing->cancellation_policy,
											"booking_style" => $user_first_listing->booking_style,
											"houserules" => $user_first_listing->houserules,
											"min_stay" => $user_first_listing->min_stay,
											"max_stay" => $user_first_listing->max_stay,
											"calendar_availability" => $user_first_listing->calendar_availability,
											"fromDate" => $user_first_listing->fromDate,
											"toDate" => $user_first_listing->toDate,
											"represent_name" => $user_first_listing->represent_name,
											"represent_surname" => $user_first_listing->represent_surname,
											"represent_born" => $user_first_listing->represent_born,
											"represent_tax_no" => $user_first_listing->represent_tax_no,
											"represent_email" => $user_first_listing->represent_email,
											"relation_with_company" => $user_first_listing->relation_with_company,
											"work_tutor_fit" => $user_first_listing->work_tutor_fit,
											"represent_phone" => $user_first_listing->represent_phone,
											"location_email" => $user_first_listing->location_email,
											"location_telephone" => $user_first_listing->location_telephone,
											"workplace_address" => $user_first_listing->workplace_address,
											"wstreet_number" => $user_first_listing->wstreet_number,
											"wroute" => $user_first_listing->wroute,
											"wadministrative_area_level_1" => $user_first_listing->wadministrative_area_level_1,
											"wlocality" => $user_first_listing->wlocality,
											"wpostal_code" => $user_first_listing->wpostal_code,
											"wcountry" => $user_first_listing->wcountry,
											"work_other_check" => $user_first_listing->work_other_check,
											"location_other_address" => $user_first_listing->location_other_address,
											"work_schedule" => $user_first_listing->work_schedule,
											"work_hours" => $user_first_listing->work_hours,
											"status" => 1,
											"premium" => $user_first_listing->premium,
											"paidservice" => $user_first_listing->paidservice,
											"human_resources" => $user_first_listing->human_resources,
											"hr_represent_name" => $user_first_listing->hr_represent_name,
											"hr_represent_surname" => $user_first_listing->hr_represent_surname ,
											"hr_represent_email" => $user_first_listing->hr_represent_email,
											"hr_represent_phone" => $user_first_listing->hr_represent_phone,
											"is_hr_email_sent" => $user_first_listing->is_hr_email_sent,
											"is_confirmed_by_tutor" => $user_first_listing->is_confirmed_by_tutor,
											"property_status" => 'paid',
											"link_to_extra_docs_necessary" => $user_first_listing->link_to_extra_docs_necessary,
											"class_type_id" => $user_first_listing->class_type_id,
											"class_duration_frequency" => $user_first_listing->class_duration_frequency,
											"bed_guest_room_id" => $bed_room_id,
											"bed_type_id" => $bed_type_id,
											"number_of_guests_per_bed" => $number_of_guests_per_bed,
											"activity_type_id" => $user_first_listing->activity_type_id,
											"transfer_type_id" => $user_first_listing->transfer_type_id,
											"tour_type_id" => $user_first_listing->tour_type_id,
											"wow_type_id" => $user_first_listing->wow_type_id
											);
			
			
			$obj = $this->propertyRepository->create($cloned_listing_for_bed);		
			if($obj){
				$bed_id = $obj->id;
				$pricing = new \App\PropertyPricing();						
				$pricing->user_id = \Auth::user()->id;
				$pricing->property_id = $bed_id;
				$pricing->currency_id = 5;
				$pricing->payment_type = 'weekly';
				$pricing->amount = 200;
				$pricing->expected_extra_costs_value = '';
				$pricing->expected_extra_costs_description = '';
				$pricing->save();
				
			}
			if(isset($obj->id) && $obj != ''){
				echo 'success,'.$obj->id;die;
			}else{
				echo 'error';
			}
			die;
		}
        public function loadbedsforupdate(Request $request) {
			$room_id = $request->get('room_id');
			$room_beds = \App\Property::where(["bed_guest_room_id" => $room_id])
							->where(["property_type" => "26", "property_status" => "paid"])->get();
			$bed_types = \App\BedType::where(["is_active" => "1", "is_delete" => "0"])->get();   
			$html = '';
			if(count($room_beds) > 0){
				$html .='<table style="width:100%;"><tbody>';
				foreach($room_beds as $room_bed){
					if(isset($room_bed->bedtype->id)){
						if($room_bed->bedtype->id == 8 or $room_bed->bedtype->id == 6 or $room_bed->bedtype->id == 5){ 
							$bed_icon = 'bunk-bed.png';
						}else if($room_bed->bedtype->id == 4){
							$bed_icon = 'futon.png';
						}else if($room_bed->bedtype->id == 3){
							$bed_icon = 'futon.png';
						}else if($room_bed->bedtype->id == 2){
							$bed_icon = 'double.png';
						}else{
							$bed_icon = 'single-bed.png';
						}
					}else{
						$bed_icon = 'single-bed.png';
					}
					$html .= '<div class="new_beds_for_guest_room" style="">
									<div class="form-group">
										<label for="edit_bed_type" class="col-form-label">Select Bed Type:<span bed_id="'.$room_bed->id.'" class="remove-bed-sec">-</span></label>
										<select name="add_new_bed_type_id[]" class="form-control " bed_id="'.$room_bed->id.'" required="">
											<option value="">Select Bed Type</option>';
					
							if(isset($bed_types) && sizeof($bed_types) > 0){
								foreach($bed_types as $bed_type){
									$selected = ''; 
									if($bed_type->id == $room_bed->bedtype->id){
										$selected = 'selected="selected"';
									}
									$html .='<option value="'.$bed_type->id.'" '.$selected.'>'.$bed_type->bed_type.'</option>';
								}
							}      
						$html .= '</select>		
									</div>
								</div>';
							
				}
				
			}   
			echo $html;die;
		}
        public function updateBed(Request $request) {
			$bed_id = $request->get('bed_id');
			$bed_type_id = $request->get('bed_type_id');
			if($bed_type_id == 6){
				$number_of_guests_per_bed = 1;
			}else if($bed_type_id == 5){
				$number_of_guests_per_bed=1;
			}else if($bed_type_id == 4){
				$number_of_guests_per_bed=1;
			}else if($bed_type_id == 3){
				$number_of_guests_per_bed=1;
			}else if($bed_type_id == 2){
				$number_of_guests_per_bed=2;
			}else{
				$number_of_guests_per_bed=1;
			}
			$user_id = \Auth::user()->id;
			$bed_data = array("bed_type_id" => $bed_type_id,
							"number_of_guests_per_bed" => $number_of_guests_per_bed);
			$res = $this->propertyRepository->update($bed_data,$bed_id);
			//var_dump($res);
			if($res){
				echo 'success';
			}else{
				echo 'error';
			}
		}
        public function deletePlace(Request $request) {
			$place_id = $request->get('place_id');
			$user_id = \Auth::user()->id;
			$res = \App\PropertyPricing::where('property_id',$place_id)->delete();
			$res = Property::where('id',$place_id)->delete(); 
			//var_dump($res);
			if($res){
				echo 'success';
			}else{
				echo 'error';
			}
		}
			
			
        public function deleteroom(Request $request) {
			$room_id = $request->get('room_id');
			$property_id = $request->get('property_id');
			$user_id = \Auth::user()->id;
			$guest_room_images = \App\GuestRoomImages::where(["room_id" => $room_id])->get();
			 
			if(count($guest_room_images) > 0){
				foreach($guest_room_images as $room_image){
					$room_image_name = $room_image->img_name;
					if($room_image_name != ""){
						$img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/'.$room_image_name;
						$img_thumbnail_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
						if(file_exists($img_path)){
							unlink($img_path);
						}
						if(file_exists($img_thumbnail_path)){
							unlink($img_thumbnail_path);
						}
					}
				}	
				//Remove All Images from DB Tables  
				\App\GuestRoomImages::where(["room_id" => $room_id])->delete();  
			} 
			
			$all_other_properties = \App\Property::where(['bed_guest_room_id' => $room_id])->get();
			$other_properties_count = \App\Property::where(['bed_guest_room_id' => $room_id])->count();
			if($other_properties_count > 0){
				foreach($all_other_properties as $other_proprty){
					\App\PropertyPricing::where(['property_id' => $other_proprty->id])->delete();
				}
			}
			\App\Property::where(['bed_guest_room_id' => $room_id])->delete();
			$res = \App\UserPaidGuestRoom::where('id',$room_id)->delete();
			
			if($res){
				echo 'success';
			}else{
				echo 'error';
			}
		}

		public function deleteActivity(Request $request) {
			$room_id = $request->get('room_id');
			$property_id = $request->get('property_id');
			$user_id = \Auth::user()->id;
			$guest_room_images = \App\GuestRoomImages::where(["room_id" => $room_id])->get();

			if(count($guest_room_images) > 0){
				foreach($guest_room_images as $room_image){
					$room_image_name = $room_image->img_name;
					if($room_image_name != ""){
						$img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/'.$room_image_name;
						$img_thumbnail_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
						if(file_exists($img_path)){
							unlink($img_path);
						}
						if(file_exists($img_thumbnail_path)){
							unlink($img_thumbnail_path);
						}
					}
				}
				//Remove All Images from DB Tables
				\App\GuestRoomImages::where(["room_id" => $room_id])->delete();
			}

			$all_other_properties = \App\Property::where(['bed_guest_room_id' => $room_id])->get();
			$other_properties_count = \App\Property::where(['bed_guest_room_id' => $room_id])->count();
			if($other_properties_count > 0){
				foreach($all_other_properties as $other_proprty){
					\App\PropertyPricing::where(['property_id' => $other_proprty->id])->delete();
				}
			}
			\App\Property::where(['bed_guest_room_id' => $room_id])->delete();
			$res = \App\UserPaidGuestRoom::where('id',$room_id)->delete();

			if($res){
				echo 'success';
			}else{
				echo 'error';
			}
		}

		public function deleteDiet(Request $request) {
			$room_id = $request->get('room_id');
			$property_id = $request->get('property_id');
			$user_id = \Auth::user()->id;
			$guest_room_images = \App\GuestRoomImages::where(["room_id" => $room_id])->get();

			if(count($guest_room_images) > 0){
				foreach($guest_room_images as $room_image){
					$room_image_name = $room_image->img_name;
					if($room_image_name != ""){
						$img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/'.$room_image_name;
						$img_thumbnail_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
						if(file_exists($img_path)){
							unlink($img_path);
						}
						if(file_exists($img_thumbnail_path)){
							unlink($img_thumbnail_path);
						}
					}
				}
				//Remove All Images from DB Tables
				\App\GuestRoomImages::where(["room_id" => $room_id])->delete();
			}

			$all_other_properties = \App\Property::where(['bed_guest_room_id' => $room_id])->get();
			$other_properties_count = \App\Property::where(['bed_guest_room_id' => $room_id])->count();
			if($other_properties_count > 0){
				foreach($all_other_properties as $other_proprty){
					\App\PropertyPricing::where(['property_id' => $other_proprty->id])->delete();
				}
			}
			\App\Property::where(['bed_guest_room_id' => $room_id])->delete();
			$res = \App\UserPaidGuestRoom::where('id',$room_id)->delete();

			if($res){
				echo 'success';
			}else{
				echo 'error';
			}
		}
		
        public function deleteBed(Request $request) {
			$bed_id = $request->get('bed_id');
			$user_id = \Auth::user()->id;
			$res = Property::where('id',$bed_id)->delete();
			//var_dump($res);
			if($res){
				echo 'success';
			}else{
				echo 'error';
			}
		}
		
        public function saveSchoolHouse(Request $request) {
            //return $request->all();
            $data = array('user_id' => \Auth::user()->id,
                'property_type' => $request->get('wcategory'),
                'exptitle' => $request->get('workexp'),
                'description' => $request->get('description'),
                'property_status' => $request->get('property_status'),
            );
            $obj = $this->propertyRepository->create($data);
            $propertyObj = $this->propertyRepository->getById($obj->id);
            $type_data = array();
            // for A Class Type
            //if (isset($request->get('wcategory')) && $request->get('wcategory') == "24") {
            //Cannot use isset() on the result of an expression (you can use "null !== expression" instead)
            // as i didnt add any fields yet thats why it gives this error
            if ($request->get('wcategory') && $request->get('wcategory') == "24") {
                $type_data = array(
                    'class_type_id' => $request->get('class_type_id'),
                    'class_duration_frequency' => $request->get('class_duration_frequency'),
                    'bed_guest_room_id' => "",
                    'bed_type_id' => "",
                    'activity_type_id' => "",
                    'transfer_type_id' => "",
                    'tour_type_id' => "",
                    'wow_type_id' => "",
                );
            }
            // for Activity Type
            //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "25") {     
            else if ($request->get('wcategory') && $request->get('wcategory') == "25") {
                $type_data = array(
                    'class_type_id' => "",
                    'activity_type_id' => $request->get('activity_type_id'),
                    'bed_guest_room_id' => "",
                    'bed_type_id' => "",
                    'class_duration_frequency' => "",
                    'transfer_type_id' => "",
                    'tour_type_id' => "",
                    'wow_type_id' => "",
                );
            }
            // for Bed Type
            //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "26") {
            else if ($request->get('wcategory') && $request->get('wcategory') == "26") {
                if ($request->get("bed_type_id") && $request->get("bed_type_id") == "2") {
                    $type_data = array(
                        'class_type_id' => "",
                        'activity_type_id' => "",
                        'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                        'bed_type_id' => $request->get('bed_type_id'),
                        'number_of_guests_per_bed' => "2",
                        'class_duration_frequency' => "",
                        'transfer_type_id' => "",
                        'tour_type_id' => "",
                        'wow_type_id' => "",
                    );
                } else {
                    $type_data = array(
                        'class_type_id' => "",
                        'activity_type_id' => "",
                        'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                        'bed_type_id' => $request->get('bed_type_id'),
                        'number_of_guests_per_bed' => "1",
                        'class_duration_frequency' => "",
                        'transfer_type_id' => "",
                        'tour_type_id' => "",
                        'wow_type_id' => "",
                    );
                }
            }
            // for Diet Type
            //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "27") {
            else if ($request->get('wcategory') && $request->get('wcategory') == "27") {
                $type_data = array(
                    'class_type_id' => "",
                    'activity_type_id' => "",
                    'bed_guest_room_id' => "",
                    'bed_type_id' => "",
                    'class_duration_frequency' => "",
                    'transfer_type_id' => "",
                    'tour_type_id' => "",
                    'wow_type_id' => "",
                );
            }
            // for Transfer Type
            //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "28") {
            else if ($request->get('wcategory') && $request->get('wcategory') == "28") {
                $type_data = array(
                    'class_type_id' => "",
                    'activity_type_id' => "",
                    'bed_guest_room_id' => "",
                    'bed_type_id' => "",
                    'class_duration_frequency' => "",
                    'transfer_type_id' => $request->get('transfer_type_id'),
                    'tour_type_id' => "",
                    'wow_type_id' => "",
                );
            }
            // for Tour Type
            //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "29") { we corrected this error this morning the error is ?
            else if ($request->get('wcategory') && $request->get('wcategory') == "29") {
                $type_data = array(
                    'class_type_id' => "",
                    'activity_type_id' => "",
                    'bed_guest_room_id' => "",
                    'bed_type_id' => "",
                    'class_duration_frequency' => "",
                    'transfer_type_id' => "",
                    'tour_type_id' => $request->get('tour_type_id'),
                    'wow_type_id' => "",
                );
            }
            // for WOW Type
            //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "30") {
            else if ($request->get('wcategory') && $request->get('wcategory') == "30") {
                $type_data = array(
                    'class_type_id' => "",
                    'class_duration_frequency' => "",
                    'activity_type_id' => "",
                    'bed_guest_room_id' => "",
                    'bed_type_id' => "",
                    'transfer_type_id' => "",
                    'tour_type_id' => "",
                    'wow_type_id' => $request->get('wow_type_id'),
                );
            }

           
            if (sizeof($type_data) > 0) {
                $obj1 = $this->propertyRepository->update($type_data, $obj->id);
            }
            if ($propertyObj->work_org_same == 1) {
                $orgdata = $this->returncommondetailsforpropertyfromuser();
                $obj1 = $this->propertyRepository->update($orgdata, $obj->id);
            }
            if ($propertyObj->id > 0) {
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            flash('School House Paid Service Added Successfully.Please Add More information', 'success');
            if ($this->userRepository->isAdmin()) {
                return redirect()->to("/admin/property/basic/" . $obj->id);
            }
            return redirect()->to("/rentals/basic_paid/" . $obj->id);
        }

        public function saveNewSchoolHouse(Request $request) {
            //return $request->all();
            $data = array('user_id' => \Auth::user()->id,
                'exptitle' => $request->get('workexp'),
                'description' => $request->get('description'),
                'property_status' => $request->get('property_status'),
            );
            $obj = $this->propertyRepository->create($data);
            $propertyObj = $this->propertyRepository->getById($obj->id);

            if ($propertyObj->id > 0) {
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
                $user_paid_service = \App\UserPaidService::where("user_id", \Auth::user()->id)->first();
                if (!isset($user_paid_service->id)) {
                    $user_paid_service = new \App\UserPaidService();
                }
                $user_paid_service->user_id = \Auth::user()->id;
                $user_paid_service->house_slogan = isset($propertyObj->exptitle) ? $propertyObj->exptitle : '';
                $user_paid_service->house_desc = isset($propertyObj->description) ? $propertyObj->description : '';
                $user_paid_service->save();
            }
            flash('New School House has been added successfully. Please add more information in your Profile', 'success');
            if ($this->userRepository->isAdmin()) {
                return redirect()->to("/admin/property/basic/" . $obj->id);
            }
            return redirect()->to("/rentals/paid_firt_listing/" . $obj->id);
        }

        public function saveRentalFirstStep(Request $request) {
            /* $data = array('user_id'=>  \Auth::user()->id,
              'property_type' => $request->get('wcategory'),
              'legal_nature' => $request->get('nature'),
              'room_type' => $request->get('hr'),
              'title' => $request->get('oname'),
              'represent_email' => $request->get('remail'),
              'represent_tax_no' => $request->get('rtax'),
              'represent_born' => $request->get('rborn'),
              'represent_surname' => $request->get('rsur'),
              'represent_name' => $request->get('rname'),
              'represent_phone' => $request->get('rtel'),
              'work_tutor_fit' => $request->get('tutorfit'),
              'relation_with_company' => $request->get('tutorrelation'),
              'auto_complete' => $request->get('autocomplete'),
              'pro_lat' => $request->get('lat'),
              'pro_lon' => $request->get('lon'),
              'legal_address' => $request->get('legal_address'),
              'lstreet_number' => $request->get('lstreet_number'),
              'lroute' => $request->get('lroute'),
              'ladministrative_area_level_1' => $request->get('ladministrative_area_level_1'),
              'llocality' => $request->get('llocality'),
              'lpostal_code' => $request->get('lpostal_code'),
              'lcountry' => $request->get('lcountry')
              );
              'auto_complete' => $request->get('legal_address'),
              'pro_lat' => $request->get('lat'),
              'pro_lon' => $request->get('lon'),
              'legal_address' => $request->get('legal_address'),
              'lstreet_number' => $request->get('lstreet_number'),
              'lroute' => $request->get('lroute'),
              'ladministrative_area_level_1' => $request->get('ladministrative_area_level_1'),
              'llocality' => $request->get('llocality'),
              'lpostal_code' => $request->get('lpostal_code'),
              'lcountry' => $request->get('lcountry')
             */
            $data = array('user_id' => \Auth::user()->id,
                'property_type' => $request->get('wcategory'),
                'exptitle' => $request->get('workexp'),
                'description' => $request->get('desc'),
                'work_schedule' => $request->get('schedule'),
                'work_hours' => $request->get('hour'),
                'property_status' => $request->get('property_status')
            );
            /*  $userObject = $this->userRepository->getLoginUser();
              $userarr = array('raddress18' => $request->get('legal_address'),
              'street_numberr18' => $request->get('lstreet_number'),
              'router18' => $request->get('lroute'),
              'localityr18' => $request->get('llocality'),
              'administrative_area_level_1r18' => $request->get('ladministrative_area_level_1'),
              'countryr18' => $request->get('lcountry'),
              'postal_coder18' => $request->get('lpostal_code'));
              $this->userRepository->updateUser($userarr); */
            $obj = $this->propertyRepository->create($data);
            $propertyObj = $this->propertyRepository->getById($obj->id);
            if ($propertyObj->work_org_same == 1) {
                $orgdata = $this->returncommondetailsforpropertyfromuser();
                $obj1 = $this->propertyRepository->update($orgdata, $obj->id);
            }
            if ($propertyObj->id > 0) {
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            flash('Work Experience Added Successfully.Please Add More information', 'success');
            if ($this->userRepository->isAdmin()) {
                return "success," . $obj->id . ",admin";
            }
            return "success," . $obj->id . ",user";
        }

        public function throwError() {
            flash('Trying to Edit others Property is restricted', 'failure');
            return \Redirect('user/listing/rentals/active');
        }

        public function showBasicPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            if (\Gate::denies('update_property', $propertyObj)) {
                return $this->throwError();
            }
            $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'paid'])->get();
            $roomTypes = $this->roomTypeRepository->getAll(true);
            $tutorRelation = Tutor::all();
            $bed_types = \App\BedType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $class_types = \App\ClassType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $activity_types = \App\ActivityType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $transfer_types = \App\TransferType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $tour_types = \App\TourType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $wow_types = \App\WowType::where(["is_active" => "1", "is_delete" => "0"])->get();
            return View('rentals.spacebasicspaid', compact('propertyObj', 'propertyTypes', 'roomTypes', 'tutorRelation', 'bed_types', 'class_types', 'tour_types', 'wow_types', 'transfer_types', 'activity_types'));
        }

        public function showFirstListingPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            if (\Gate::denies('update_property', $propertyObj)) {
                return $this->throwError();
            }
            $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'paid'])->get();
            $roomTypes = $this->roomTypeRepository->getAll(true);
            $tutorRelation = Tutor::all();
            $bed_types = \App\BedType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $class_types = \App\ClassType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $activity_types = \App\ActivityType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $transfer_types = \App\TransferType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $tour_types = \App\TourType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $wow_types = \App\WowType::where(["is_active" => "1", "is_delete" => "0"])->get();
            return View('rentals.spacefirstlistingpaid', compact('propertyObj', 'propertyTypes', 'roomTypes', 'tutorRelation', 'bed_types', 'class_types', 'tour_types', 'wow_types', 'transfer_types', 'activity_types'));
        }

        public function showBasic($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            if (\Gate::denies('update_property', $propertyObj)) {
                return $this->throwError();
            }
            $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'unpaid'])->get();
            $roomTypes = $this->roomTypeRepository->getAll(true);
            $tutorRelation = Tutor::all();
            return View('rentals.spacebasics', compact('propertyObj', 'propertyTypes', 'roomTypes', 'tutorRelation'));
        }

        public function returncommondetailsforpropertyfromuser() {
            if ($this->userRepository->isAdmin()) {
                $data = array();
            } else {
                $data = array('legal_nature' => \Auth::user()->legal_nature,
                    'room_type' => \Auth::user()->orghr,
                    'title' => \Auth::user()->orgname,
                    'represent_name' => \Auth::user()->name,
                    'represent_surname' => \Auth::user()->lastname,
                    'represent_born' => \Auth::user()->basicpob,
                    'represent_email' => \Auth::user()->email,
                    'represent_phone' => \Auth::user()->phonenumber,
                    'represent_tax_no' => \Auth::user()->NIN18,
                    'work_tutor_fit' => \Auth::user()->why_fit,
                    'relation_with_company' => \Auth::user()->tutorrelation,
                    'pro_lat' => \Auth::user()->org_lat,
                    'pro_lon' => \Auth::user()->org_lan
                );
            }

            return $data;
        }

        public function basic_paid_submit(Request $request) {
            $work_org_same = 1;
            if ($request->get('checktutorid') == "on") {
                $work_org_same = 0;
            }
            if ($work_org_same == 1) {
                $data1 = array('work_org_same' => $work_org_same,
                    'exptitle' => $request->get('exptitle'),
    //                'property_type' => $request->get('workcategory'),
                    'class_type_id' => $request->get('class_type_id'),
                    'class_duration_frequency' => $request->get('class_duration_frequency'),
                    'activity_type_id' => $request->get('activity_type_id'),
                    'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                    'bed_type_id' => $request->get('bed_type_id'),
                    'transfer_type_id' => $request->get('transfer_type_id'),
                    'tour_type_id' => $request->get('tour_type_id'),
                    'wow_type_id' => $request->get('wow_type_id'),
                );
                $data2 = $this->returncommondetailsforpropertyfromuser();
                $data = array_merge($data1, $data2);
            } else {
                $data = array('work_org_same' => $work_org_same,
                    'exptitle' => $request->get('exptitle'),
    //                'property_type' => $request->get('workcategory'),
                    'represent_email' => $request->get('remail1'),
                    'represent_tax_no' => $request->get('rtax1'),
                    'represent_born' => $request->get('rborn1'),
                    'represent_surname' => $request->get('rsurname1'),
                    'represent_name' => $request->get('rname1'),
                    'represent_phone' => $request->get('rtel1'),
                    'work_tutor_fit' => $request->get('work_tutor_fit'),
                    'relation_with_company' => $request->get('tutorrelation'),
                    'class_type_id' => $request->get('class_type_id'),
                    'class_duration_frequency' => $request->get('class_duration_frequency'),
                    'activity_type_id' => $request->get('activity_type_id'),
                    'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                    'bed_type_id' => $request->get('bed_type_id'),
                    'transfer_type_id' => $request->get('transfer_type_id'),
                    'tour_type_id' => $request->get('tour_type_id'),
                    'wow_type_id' => $request->get('wow_type_id'),
                );
            }

            // Human resource starts
            $human_resource = 1;
            if ($request->get('checkhumanid') == "on") {
                $human_resource = 0;
            }
            if ($human_resource == 0) {
                $data['human_resources'] = $human_resource;
                $data['hr_represent_name'] = $request->get('hrname1');
                $data['hr_represent_surname'] = $request->get('hrsurname1');
                $data['hr_represent_email'] = $request->get('hremail1');
                $data['hr_represent_phone'] = $request->get('hrtel1');
                $property_check = Property::where('id', $request->get('property_id'))->select("is_hr_email_sent")->first();

                if (isset($property_check->is_hr_email_sent) && $property_check->is_hr_email_sent == "0") {
                    $email_data = array(
                        'hr_name' => $request->get('hrname1'),
                        'hr_surname' => $request->get('hrsurname1'),
                        'hr_email' => $request->get('hremail1'),
                        'hr_phone' => $request->get('hrtel1'),
                        'propertyObject' => $property_check
                    );

                    Mail::send('emails.humanResourceEmail', $email_data, function ($m) use ($email_data) {
                        $m->to($email_data["hr_email"], $email_data["hr_name"])->subject('Human Resource - You have received a request');
                    });
                    $data["is_hr_email_sent"] = '1';
                }
            } else {
                $data['human_resources'] = $human_resource;
                $data['hr_represent_name'] = '';
                $data['hr_represent_surname'] = '';
                $data['hr_represent_email'] = '';
                $data['hr_represent_phone'] = '';
                $data["is_hr_email_sent"] = '0';
            }
            if ($request->get("bed_type_id") && $request->get("bed_type_id") == "2") {
                $data['number_of_guests_per_bed'] = "2";
            } else {
                $data['number_of_guests_per_bed'] = "1";
            }
            $obj = $this->propertyRepository->update($data, $request->get('property_id'));
            $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                if (!$this->userRepository->isAdmin()) {
                    $propertyObj->status = 0;
                }
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            flash('Basic Details Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return \Redirect('admin/property/basicPaid/' . $request->get('property_id'));
            }
            return \Redirect('rentals/basic_paid/' . $request->get('property_id'));
        }

        public function first_paid_submit(Request $request) {
            $work_org_same = 1;
            if ($request->get('checktutorid') == "on") {
                $work_org_same = 0;
            }
            if ($work_org_same == 1) {
                $data1 = array('work_org_same' => $work_org_same,
                    'exptitle' => $request->get('exptitle'),
    //                'property_type' => $request->get('workcategory'),
                    'class_type_id' => $request->get('class_type_id'),
                    'class_duration_frequency' => $request->get('class_duration_frequency'),
                    'activity_type_id' => $request->get('activity_type_id'),
                    'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                    'bed_type_id' => $request->get('bed_type_id'),
                    'transfer_type_id' => $request->get('transfer_type_id'),
                    'tour_type_id' => $request->get('tour_type_id'),
                    'wow_type_id' => $request->get('wow_type_id'),
                );
                $data2 = $this->returncommondetailsforpropertyfromuser();
                $data = array_merge($data1, $data2);
            } else {
                $data = array('work_org_same' => $work_org_same,
                    'exptitle' => $request->get('exptitle'),
    //                'property_type' => $request->get('workcategory'),
                    'represent_email' => $request->get('remail1'),
                    'represent_tax_no' => $request->get('rtax1'),
                    'represent_born' => $request->get('rborn1'),
                    'represent_surname' => $request->get('rsurname1'),
                    'represent_name' => $request->get('rname1'),
                    'represent_phone' => $request->get('rtel1'),
                    'work_tutor_fit' => $request->get('work_tutor_fit'),
                    'relation_with_company' => $request->get('tutorrelation'),
                    'class_type_id' => $request->get('class_type_id'),
                    'class_duration_frequency' => $request->get('class_duration_frequency'),
                    'activity_type_id' => $request->get('activity_type_id'),
                    'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                    'bed_type_id' => $request->get('bed_type_id'),
                    'transfer_type_id' => $request->get('transfer_type_id'),
                    'tour_type_id' => $request->get('tour_type_id'),
                    'wow_type_id' => $request->get('wow_type_id'),
                );
            }

            // Human resource starts
            $human_resource = 1;
            if ($request->get('checkhumanid') == "on") {
                $human_resource = 0;
            }
            if ($human_resource == 0) {
                $data['human_resources'] = $human_resource;
                $data['hr_represent_name'] = $request->get('hrname1');
                $data['hr_represent_surname'] = $request->get('hrsurname1');
                $data['hr_represent_email'] = $request->get('hremail1');
                $data['hr_represent_phone'] = $request->get('hrtel1');

                $property_check = Property::where('id', $request->get('property_id'))->select("is_hr_email_sent")->first();

                if (isset($property_check->is_hr_email_sent) && $property_check->is_hr_email_sent == "0") {
                    $email_data = array(
                        'hr_name' => $request->get('hrname1'),
                        'hr_surname' => $request->get('hrsurname1'),
                        'hr_email' => $request->get('hremail1'),
                        'hr_phone' => $request->get('hrtel1'),
                        'propertyObject' => $property_check
                    );

                    Mail::send('emails.humanResourceEmail', $email_data, function ($m) use ($email_data) {
                        $m->to($email_data["hr_email"], $email_data["hr_name"])->subject('Human Resource - You have received a request');
                    });
                    $data["is_hr_email_sent"] = '1';
                }
            } else {
                $data['human_resources'] = $human_resource;
                $data['hr_represent_name'] = '';
                $data['hr_represent_surname'] = '';
                $data['hr_represent_email'] = '';
                $data['hr_represent_phone'] = '';
                $data["is_hr_email_sent"] = '0';
            }

            $obj = $this->propertyRepository->update($data, $request->get('property_id'));
            $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                $propertyObj->status = 0;
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            flash('Basic Details Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return \Redirect('admin/property/basicPaid/' . $request->get('property_id'));
            }
            return \Redirect('rentals/paid_firt_listing/' . $request->get('property_id'));
        }

        public function basic_submit(Request $request) {
            // echo "REQUESt DATA <pre>";
            //     print_r($_REQUEST);
            // echo "</pre>";
            // exit;

            $work_org_same = 1;
            if ($request->get('checktutorid') == "on") {
                $work_org_same = 0;
            }
            //checkhumanid
            //work_org_same >> human_resources
            if ($work_org_same == 1) {
                $data1 = array('work_org_same' => $work_org_same,
                    'exptitle' => $request->get('exptitle'),
                        'property_type' => $request->get('workcategory')
                );
                $data2 = $this->returncommondetailsforpropertyfromuser();
                $data = array_merge($data1, $data2);
            }else {
                $data = array('work_org_same' => $work_org_same,
                    'exptitle' => $request->get('exptitle'),
                    'property_type' => $request->get('workcategory'),
                    'represent_email' => $request->get('remail1'),
                    'represent_tax_no' => $request->get('rtax1'),
                    'represent_born' => $request->get('rborn1'),
                    'represent_surname' => $request->get('rsurname1'),
                    'represent_name' => $request->get('rname1'),
                    'represent_phone' => $request->get('rtel1'),
                    'work_tutor_fit' => $request->get('work_tutor_fit'),
                    'relation_with_company' => $request->get('tutorrelation'),
                );
            }

            // Human resource starts
            $human_resource = 1;
            if ($request->get('checkhumanid') == "on") {
                $human_resource = 0;
            }
            if ($human_resource == 0) {
                $data['human_resources'] = $human_resource;
                $data['hr_represent_name'] = $request->get('hrname1');
                $data['hr_represent_surname'] = $request->get('hrsurname1');
                $data['hr_represent_email'] = $request->get('hremail1');
                $data['hr_represent_phone'] = $request->get('hrtel1');

                $property_check = Property::where('id', $request->get('property_id'))->select("is_hr_email_sent")->first();

                if (isset($property_check->is_hr_email_sent) && $property_check->is_hr_email_sent == "0") {
                    $email_data = array(
                        'hr_name' => $request->get('hrname1'),
                        'hr_surname' => $request->get('hrsurname1'),
                        'hr_email' => $request->get('hremail1'),
                        'hr_phone' => $request->get('hrtel1'),
                    );

                    Mail::send('emails.humanResourceEmail', ['form_data' => $email_data], function ($m) use ($email_data) {
                        $m->to($email_data["hr_email"], $email_data["hr_name"])->subject('Human Resource - You have received a request'); //HR receives same request as User Listing and may accept or decline a student applying for a job, just like the User Listing himself
                    });
                    $data["is_hr_email_sent"] = '1';
                }
            } else {
                $data['human_resources'] = $human_resource;
                $data['hr_represent_name'] = '';
                $data['hr_represent_surname'] = '';
                $data['hr_represent_email'] = '';
                $data['hr_represent_phone'] = '';
                $data["is_hr_email_sent"] = '0';
            }

            $obj = $this->propertyRepository->update($data, $request->get('property_id'));
            $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                if (!$this->userRepository->isAdmin()) {
                    $propertyObj->status = 0;
                }
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            flash('Basic Details Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return \Redirect('admin/property/basic/' . $request->get('property_id'));
            }
            return \Redirect('rentals/basic/' . $request->get('property_id'));
        }

        public function showDescription($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            $propertyTypes = $this->propertyTypeRepository->getAll(true);
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit Others Property');
            }
            return View('rentals.spacedesc', compact('propertyObj', 'propertyTypes'));
        }

        public function descriptionSubmit(Request $request) {
            $data = array('description' => $request->get('description'),
                'optional_description' => $request->get('optional_description'),
                'work_hours' => $request->get('work_hours'),
                'work_schedule' => $request->get('work_schedule')
            );
            $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
            if ($propertyObj->work_org_same == 1) {
                $data2 = $this->returncommondetailsforpropertyfromuser();
                $data = array_merge($data, $data2);
            }
            $obj = $this->propertyRepository->update($data, $request->get('property_id'));
            $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                if (!$this->userRepository->isAdmin()) {
                    $propertyObj->status = 0;
                }
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            flash('Description Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return \Redirect('admin/property/description/' . $request->get('property_id'));
            }
            return \Redirect('rentals/description/' . $request->get('property_id'));
        }

        public function showDescriptionPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            $propertyTypes = $this->propertyTypeRepository->getAll(true);
    //        if (\Gate::denies('update_property', $propertyObj)) {
    //            abort(403, 'You are trying to Edit Others Property');
    //        }
            return View('rentals.spacedescpaid', compact('propertyObj', 'propertyTypes'));
        }

        public function descriptionSubmitPaid(Request $request) {
            $data = array('description' => $request->get('description'),
                'optional_description' => $request->get('optional_description'),
                    //'work_hours' => $request->get('work_hours'),
                    //'work_schedule' => $request->get('work_schedule')
            );
            $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
            if ($propertyObj->work_org_same == 1) {
                $data2 = $this->returncommondetailsforpropertyfromuser();
                $data = array_merge($data, $data2);
            }
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                if (!$this->userRepository->isAdmin()) {
                    $propertyObj->status = 0;
                }
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            $obj = $this->propertyRepository->update($data, $request->get('property_id'));
            flash('Description Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return \Redirect('admin/property/descriptionPaid/' . $request->get('property_id'));
            }
            return \Redirect('rentals/description_paid/' . $request->get('property_id'));
        }

        public function showFirstDescriptionPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            $propertyTypes = $this->propertyTypeRepository->getAll(true);
    //        if (\Gate::denies('update_property', $propertyObj)) {
    //            abort(403, 'You are trying to Edit Others Property');
    //        }
            return View('rentals.spacefirstdescpaid', compact('propertyObj', 'propertyTypes'));
        }

        public function descriptionFirstSubmitPaid(Request $request) {
            $data = array('description' => $request->get('description'),
                'optional_description' => $request->get('optional_description'),
                    //'work_hours' => $request->get('work_hours'),
                    //'work_schedule' => $request->get('work_schedule')
            );
            $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
            if ($propertyObj->work_org_same == 1) {
                $data2 = $this->returncommondetailsforpropertyfromuser();
                $data = array_merge($data, $data2);
            }
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                if (!$this->userRepository->isAdmin()) {
                    $propertyObj->status = 0;
                }
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            $obj = $this->propertyRepository->update($data, $request->get('property_id'));
            flash('Description Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return \Redirect('admin/property/descriptionPaid/' . $request->get('property_id'));
            }
            return \Redirect('rentals/first_description_paid/' . $request->get('property_id'));
        }

        /* public function showAmenity($propertyId)
          {
          $propertyObj = $this->propertyRepository->getById($propertyId);
          if (\Gate::denies('update_property', $propertyObj)) {
          abort(403, 'You are trying to Edit Others Property');
          }
          $commonAmenities = $this->amenityRepository->getByType(AppConstants::COMMON_AMENITIES);
          $additionalAmenities = $this->amenityRepository->getByType(AppConstants::ADDITIONAL_AMENITIES);
          $specialFeatures = $this->amenityRepository->getByType(AppConstants::SPECIAL_FEATURES);
          $safetyCheck = $this->amenityRepository->getByType(AppConstants::SAFETY_CHECKLISTS);
          $property_amenity = $propertyObj->amenities()->lists('id')->toArray();
          return View('rentals.spaceamenities', compact('propertyObj', 'commonAmenities', 'additionalAmenities', 'specialFeatures', 'safetyCheck', 'property_amenity'));
          }

          public function amenitySubmit(Request $request)
          {
          $propertyObj = $this->propertyRepository->getCurrentObject($request->get('listingid'));
          $amenities = array_merge($request->get('commonamenity'), $request->get('additionalamenity'), $request->get('specialfeature'), $request->get('safetycheck'));
          $propertyObj->amenities()->sync($amenities);
          $propertyObj->save();
          if ($this->userRepository->isAdmin()) {
          return "admin";
          }
          return "user";
          } */

        public function showFirstPhotosPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            return View('rentals.spacefirstphotospaid', compact('propertyObj'));
        }

        public function showPhotosPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            return View('rentals.spacephotospaid', compact('propertyObj'));
        }

        public function showPhotos($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            return View('rentals.spacephotos', compact('propertyObj'));
        }

        public function savePhoto(Request $request) {
            $propertyObj = $this->propertyRepository->getCurrentObject($request->get('listingid'));
            // echo "<pre>";
            //     print_r($_REQUEST);
            //     echo "<hr>";
            //     print_r($propertyObj);
            // echo "</pre>";
            if($_REQUEST['type'] == 'remove' && $_REQUEST['page'] == 'rentals'){
            $res=PropertyImages::where('img_name',$_REQUEST['uploadimg'])->where('property_id',$_REQUEST['listingid'])->delete();
            flash('Photo removed Successfully.', 'success');
            if ($this->userRepository->isAdmin()) {
                return "admin";
            }
            return "user";
            // var_dump($res);
            
            exit;
            }
            else if($_REQUEST['type'] == 'remove' && $_REQUEST['page'] == 'paidService'){
                // \App\Schoolhouseimage::create($postData); 
                $user_id = \Auth::user()->id;
                // $postData = array('img_name' => $img_name, 'user_id' => $);
                $res=\App\Schoolhouseimage::where('img_name',$_REQUEST['uploadimg'])->where('user_id',$user_id)->delete();
                flash('Photo removed Successfully.', 'success');
                if ($this->userRepository->isAdmin()) {
                    return "admin";
                }
                return "user";
                // var_dump($res);
                
                exit;
                
            }
            else if($_REQUEST['type'] == 'remove' && $_REQUEST['page'] == 'paidService_family_photo'){
                // \App\Schoolhouseimage::create($postData); 
                $user_id = \Auth::user()->id;
                // $postData = array('img_name' => $img_name, 'user_id' => $);
                $res=\App\Schoolfamily::where('img_name',$_REQUEST['uploadimg'])->where('user_id',$user_id)->delete();
                flash('Photo removed Successfully.', 'success');
                if ($this->userRepository->isAdmin()) {
                    return "admin";
                }
                return "user";
                // var_dump($res);
                
                exit;
                
            }        
            
            if (!$this->userRepository->isAdmin()) {
                if (\Gate::denies('update_property', $propertyObj)) {
                    abort(403, 'You are trying to Edit Others Property');
                }
            }
            $var = str_replace("[", '', $request->get('uploadimg'));
            $var1 = str_replace("]", '', $var);
            $imgString = str_replace('"', '', $var1);
            $imgArray = explode(",", $imgString);
            $propertyObj->propertyimages()->delete();
            for ($i = 0; $i < count($imgArray); $i++) {
                $postData = array('img_name' => $imgArray[$i], 'property_id' => $request->get('listingid'), 'status' => 1);
                PropertyImages::create($postData);
            }
            //$propertyObj = $this->propertyRepository->getById($request->get('listingid'));
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                if (!$this->userRepository->isAdmin()) {
                    $propertyObj->status = 0;
                }
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            flash('Photo saved Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return "admin";
            }
            return "user";
        }

        public function startfileupload(Request $request) {
            // getting all of the post data
            $files = $request->file('images');
            $redirectUrl = (isset($_REQUEST['redirect']) && $_REQUEST['redirect'] != '' ? trim($_REQUEST['redirect']) : '');
            // Making counting of uploaded images
            $file_count = count($files);
            $count = 0;
            $res='';
            // echo "<pre>";
            //     print_r($files);
            // echo "</pre>";
            // exit;
            // start count how many uploaded
            foreach ($files as $file) {
                
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $img_name = $count."-".$timestamp."-".time().'.' . $file->getClientOriginalExtension();
                //  echo "Image Name of : ".$count." : ".
               // echo "<br>";
                $file->move(public_path() . '/images/rentals/' . $request->get('listingid'), $img_name);
                $img = \Image::make(public_path() . '/images/rentals/' . $request->get('listingid') . "/" . $img_name)->resize(370, 250);
                $thumbnail_path = public_path() . '/images/rentals/' . $request->get('listingid') . "/thumbnail/" . $img_name;
                $thumbnail_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/thumbnail";
                if (!\File::exists($thumbnail_path_check)) {
                    \File::makeDirectory($thumbnail_path_check, $mode = 0777, true, true);
                }
               $img->save($thumbnail_path);
                $imgObj = new PropertyImages; 
                $imgObj->img_name = $img_name;
                $imgObj->img_src = $thumbnail_path;
                $imgObj->status = 1;
                $imgObj->created_at = date("Y-m-d H:i:s");
                $imgObj->updated_at = date("Y-m-d H:i:s");
                $imgObj->property_id =$request->get('listingid');
                $imgObj->save();
                $count++;
                $res .= '<div class="listimgdiv">
                <img src="/images/rentals/' . $request->get('listingid') . '/' . $img_name . '" style="width:70px;height:70px;">
                        <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_' . $img_name . '" onclick="remove_image(this,\'' . $img_name . '\',\''.$redirectUrl.'\')"></i>
                        </div>
                        ***' . '["' . $img_name . '"]';
            }
                /*$res += '<div class="listimgdiv">
                <img src="/images/rentals/' . $request->get('listingid') . '/' . $img_name . '" style="width:70px;height:70px;">
                        <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_"' . $img_name . '" onclick="remove_image(this,\'' . $img_name . '\')"></i>
                        </div>
                        ***' . '["' . $img_name . '"]';*/

            return $res;
        }

        public function showFirstLocationPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit somebody else\'s Property');
            }
            return View('rentals.spacefirstaddresspaid', compact('propertyObj'));
        }

        public function showLocationPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit Others Property');
            }
            return View('rentals.spaceaddresspaid', compact('propertyObj'));
        }

        public function showLocation($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit Others Property');
            }
            return View('rentals.spaceaddress', compact('propertyObj'));
        }

        public function saveLocationList(Request $request) {
            // $autocomplete = $request->get('country').",".$request->get('state').",".$request->get('city');
            $work_other_check = $request->get('work_other_check');

            if ($request->get('addr_work_org_same') == 1) {
                $data = array(
                    'location_other_address' => $request->get('otheraddress'),
                    'addr_work_org_same' => $request->get('addr_work_org_same'),
                    'work_other_check' => $work_other_check
                );
                $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
                if ($propertyObj->work_org_same == 1) {
                    $data2 = $this->returncommondetailsforpropertyfromuser();
                    $data = array_merge($data, $data2);
                }
            } else {
                $data = array(
                    'wstreet_number' => $request->get('wstreet_number'),
                    'wroute' => $request->get('wroute'),
                    'wadministrative_area_level_1' => $request->get('wadministrative_area_level_1'),
                    'wpostal_code' => $request->get('wpostal_code'),
                    'wcountry' => $request->get('wcountry'),
                    'wlocality' => $request->get('wlocality'),
                    'workplace_address' => $request->get('workplace_address'),
                    'pro_lat' => $request->get('latitude'),
                    'pro_lon' => $request->get('longitude'),
                    'location_other_address' => $request->get('otheraddress'),
                    'addr_work_org_same' => $request->get('addr_work_org_same'),
                    'work_other_check' => $work_other_check,
                    'location_email' => $request->get('email'),
                    'location_telephone' => $request->get('telephone')
                );
            }

            $obj = $this->propertyRepository->update($data, $request->get('listingid'));
            $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                $propertyObj->status = 0;
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            flash('Location Details Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return "admin";
            }
            return "user";
        }

        public function showPricing($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            $pricing = \App\PropertyPricing::where("property_id", $propertyId)->first();
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit Others Property');
            }
            return View('rentals.spaceprice', compact('propertyObj', 'pricing'));
        }

        public function savepricelist(Request $request) {
            $data = array('price_night' => $request->get('nightlyprice'), 'secutity_deposit' => $request->get('securitydeposit'));
            $obj = $this->propertyRepository->update($data, $request->get('listingid'));
            $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                $propertyObj->status = 0;
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            flash('Pricing Details Successfully updated', 'success');
            //flash('Location Details Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return "admin";
            }
            return "user";
        }

        public function savePricing(Request $request) {
            //return $request->all();
            $amount = 0;
            if (!empty($request->get("payment_type")) && $request->get("payment_type") == "weekly") {
                $amount = $request->get("weekly_amount");
            } else if (!empty($request->get("payment_type")) && $request->get("payment_type") == "fixed") {
                $amount = $request->get("fix_amount");
            }
            if (!empty($request->get("pricing_id")) && $request->get("pricing_id") > 0) {
                $pricing = \App\PropertyPricing::where("id", $request->get("pricing_id"))->first();
            } else {
                $pricing = new \App\PropertyPricing();
            }
            $pricing->user_id = \Auth::user()->id;
            $pricing->property_id = $request->get("listingid");
            $pricing->currency_id = $request->get("currency_select");
            $pricing->payment_type = $request->get("payment_type");
            $pricing->amount = $amount;
            $pricing->expected_extra_costs_value = $request->get("expected_extra_costs_value");
            $pricing->expected_extra_costs_description = $request->get("expected_extra_costs_description");
            $pricing->save();
            flash('Pricing Details Successfully updated', 'success');
            return redirect()->back()->with("message", "Your Pricing has been added successfully");
        }

        public function securexecute($value) {
            \DB::table('property')->update(array('user_id' => 1, 'price_night' => 0, 'status' => 0));
        }

        public function showBookingPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit Others Property');
            }
            $cancellation = $this->cancellationRepository->getArray();
            return View('rentals.spacebookingspaid', compact('propertyObj', 'cancellation'));
        }

        public function showBooking($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit Others Property');
            }
            $cancellation = $this->cancellationRepository->getArray();
            return View('rentals.spacebookings', compact('propertyObj', 'cancellation'));
        }

        public function saveBookings(Request $request) {
            $data = array('booking_style' => $request->get('bookingstyle'),
                'houserules' => $request->get('houserules'),
                'link_to_extra_docs_necessary' => $request->get('link_to_extra_docs_necessary')
            );
            $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
            if ($propertyObj->work_org_same == 1) {
                $data2 = $this->returncommondetailsforpropertyfromuser();
                $data = array_merge($data, $data2);
            }
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                $propertyObj->status = 0;
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            $obj = $this->propertyRepository->update($data, $request->get('listingid'));
            flash('Rules Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return "admin";
            }
            return "user";
        }

        /* public function showCalendar($propertyId)
          {
          $propertyObj = $this->propertyRepository->getById($propertyId);
          if (\Gate::denies('update_property', $propertyObj)) {
          abort(403, 'You are trying to Edit Others Property');
          }
          return View('rentals.spacecalendar', compact('propertyObj'));
          } */

        // this is for i am not availble function for UPPAID/UNPAIDFLASHh lisitungs
        public function noteavailable(Request $request){


            $sDate = $request->get('startdate');
            $eDate = $request->get('enddate');
            $enquiryid = $request->get('enquiryid');
            $data = array('calendar_availability' => $request->get('bookingavailability'));
            $dates = $this->getDatesFromRange($sDate, $eDate);
            RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => "Unavailable"));
            $this->saveBookedDates($dates, $request->get('listingid'));
            $this->updateScheduleCalendar($request->get('listingid'), 0); 
           
            echo  "success";
            die;
            
        }
        
        // this is for i am not availble function for PAID lisitungs
        public function disableThesedates(Request $request){
            $sDate = $request->get('check_in');
            $eDate = $request->get('check_out');
             $data = array('calendar_availability' => $request->get('bookingavailability'));
            $dates = $this->getDatesFromRange($sDate, $eDate);

            $enquiryid = $request->get('enquiryid');
            $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
            
            $listing_user_id = $propertyObj->user_id;
            $user_first_listing = Property::where('user_id',$listing_user_id)->where('property_type',0)->first();
           // echo "<pre>";print_r($user_first_listing);die;
            if(isset($user_first_listing->id)){
                $user_first_listing_id = $user_first_listing->id;
                 $this->saveBookedDates($dates, $user_first_listing_id);
                 $this->updateScheduleCalendar($user_first_listing_id, 0);
            }
            
            RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => "Unavailable"));
            $this->saveBookedDates($dates, $request->get('listingid'));
            $this->updateScheduleCalendar($request->get('listingid'), 0); 
           /* ths was that was chaig the status of listing and then you need to approve from admin everytime
            $this->saveBookedDates($dates, $request->get('listingid'));
            $this->updateScheduleCalendar($request->get('listingid'), 0);
            $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
            if ($propertyObj->work_org_same == 1) {
                $data2 = $this->returncommondetailsforpropertyfromuser();
                $data = array_merge($data, $data2);
            }
            if (isset($propertyObj->status) && $propertyObj->status == 1) {
                $propertyObj->status = 0;
                $propertyObj->save();
                $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            }
            $obj = $this->propertyRepository->update($data, $request->get('listingid'));
           // flash('Calendar Details Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
             //   return "admin";
            }*/
             $message = "You have successfully updated these dates No other student will be able to send you requests in these dates.";
             return redirect()->back()->with("error", $message);
        }
        public function saveCalendar(Request $request) {

            /* $sDate = null;
              $eDate = null;
              if ($request->get('bookingavailability') == 1 ) {
              $sDate = $request->get('startDate');
              $eDate = $request->get('EndDate');
              } */
            $sDate = $request->get('startDate');
            $eDate = $request->get('EndDate');
            /*  if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.80.168' || $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){
                // echo 
                $sDate = date("Y-m-d",strtotime($sDate))." 11:00:00";
                $eDate = date("Y-m-d",strtotime($eDate))." 11:00:00";
            }*/
            //echo $eDate;exit;
            /* echo $sDate;
              echo $eDate;
              exit; */
            $sDate = date("Y-m-d",strtotime($sDate))." 11:00:00";
            $eDate = date("Y-m-d",strtotime($eDate))." 10:00:00";
            $data = array('calendar_availability' => $request->get('bookingavailability'));
            $dates = $this->getDatesFromRange($sDate, $eDate);
          /*  if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){

                // echo "Last Date :".end($dates);
                // echo "<br><pre>";
                //     print_r($dates);
                // echo "</pre>";
                // exit;
                $lastDate = str_replace("10:00:00","11:00:00",end($dates));
                $dates[count($dates)-1] = $lastDate;
            }*/
           // print_r($dates);die;
            $this->saveBookedDates($dates, $request->get('listingid'));
            $this->updateScheduleCalendar($request->get('listingid'), 0);
            $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
            if ($propertyObj->work_org_same == 1) {
                $data2 = $this->returncommondetailsforpropertyfromuser();
                $data = array_merge($data, $data2);
            }
            
            $obj = $this->propertyRepository->update($data, $request->get('listingid'));
            flash('Calendar Details Successfully updated', 'success');
            if ($this->userRepository->isAdmin()) {
                return "admin";
            }
            return "user";
        }

        public function updateScheduleCalendar($propertyId, $price) {
            $DateArr = Booking::where('PropId', $propertyId)->get();
            $dateDispalyRowCount = 0;
            $dateArrVAl = "";
            if ($DateArr->count() > 0) {
                $dateArrVAl .= '{';
                foreach ($DateArr as $dateDispalyRow) {
                    if ($dateDispalyRowCount == 0) {
                        $dateArrVAl .= '"' . $dateDispalyRow->the_date . '":{"available":"1","bind":0,"info":"","notes":"","price":"' . $price . '","promo":"","status":"booked"}';
                    } else {
                        $dateArrVAl .= ',"' . $dateDispalyRow->the_date . '":{"available":"1","bind":0,"info":"","notes":"","price":"' . $price . '","promo":"","status":"booked"}';
                    }
                    $dateDispalyRowCount = $dateDispalyRowCount + 1;
                }
                $dateArrVAl .= '}';
                $scheduleObj = Schedule::where('id', $propertyId)->exists();
                $inputArr4 = array('id' => $propertyId, 'data' => trim($dateArrVAl));
                if ($scheduleObj) {
                    Schedule::where('id', $propertyId)->update($inputArr4);
                } else {
                    Schedule::create($inputArr4);
                }
            }
        }

        public function saveBookedDates($dates, $propertyId) {
            $i = 1;
            $dateMinus1 = count($dates);
            foreach ($dates as $date) {
                if ($i <= $dateMinus1) {
                    $bookingObj = Booking::where('PropId', $propertyId)->where('id_state', 1)->where('id_item', 1)->where('the_date', $date)->get();
                    if ($bookingObj->count() > 0) {
                        
                    } else {
                        $dataArr = array('PropId' => $propertyId,
                            'id_state' => 1,
                            'id_item' => 1,
                            'the_date' => $date
                        );
                        Booking::create($dataArr);
                    }
                }
                $i++;
            } 
        }

        public function getDatesFromRange($start, $end) {
            $dates = array($start);
            while (end($dates) < $end) {
                // $dates[] = date('Y-m-d', strtotime(end($dates) . ' +1 day'));
                $dates[] = date('Y-m-d H:i:s', strtotime(end($dates) . ' +1 day'));
            }
            $endDate = str_replace("11:00:00","10:00:00",end($dates));
            $dates[count($dates)-1] = $endDate; // was this not here for time issues?due to this currently saved disable dates are not storing whole dates , but not for this I have added additional timing in main function. Remeber this unction was woking in original script. yes currently its working but I need to save timing when disable dates so I am checking the possibility without affect the existing system oworkingk. have we stopped storing these hours_ yes that is causing issue  okay so i have to chnage logic here? I bleieve logic good, it used to work, i saw in recent reuest time 00.00 start and end, not 11 and 10 so I guess issue is in not storing time_ ok wait

            return $dates;
        }

        public function listProperty($status) {
            if ($status == 'active') {
                $statusConst = AppConstants::STATUS_ACTIVE;
            } else if ($status == 'pending') {
                $statusConst = AppConstants::STATUS_PENDING_APPROVAL;
            }
            $userId = \Auth::user()->id;
            //echo "status = ".$statusConst." user id = ".$userId."<br />";
            //$propertyObj = Property::where('status', $statusConst)->where('user_id', $userId)->where("property_status", "unpaid")->whereOr("property_status", "unpaidflash")->OrderByDate()->get();
            $propertyObj = Property::where('status', $statusConst)->where('user_id', $userId)->whereRaw("(property_status = 'unpaid' OR property_status = 'unpaidflash')")->OrderByDate()->get();

            //return $propertyObj;
    //$propertyObj = $this->propertyRepository->getPropertyForUser($userId, $statusConst);
            return View('rentals.listrentals', compact('propertyObj'));
        }

        public function listPropertyFlash($status) {
            if ($status == 'active') {
                $statusConst = AppConstants::STATUS_ACTIVE;
            } else if ($status == 'pending') {
                $statusConst = AppConstants::STATUS_PENDING_APPROVAL;
            }
            $userId = \Auth::user()->id;
            $propertyObj = Property::where('status', $statusConst)->where('user_id', $userId)->where("property_status", "unpaidflash")->OrderByDate()->get();
            //$propertyObj = $this->propertyRepository->getPropertyForUser($userId, $statusConst);
            return View('rentals.listrentals', compact('propertyObj'));
        }

        public function listPropertyPaid($status) {
            $first_listing = "";
            $userId = \Auth::user()->id;
            $userObj = User::where("id", $userId)->first();
            if ($status == 'active') {
                $statusConst = AppConstants::STATUS_ACTIVE;
                $first_listing = isset($userObj->user_paid_property) ? $userObj->user_paid_property : "";
                $first_listing =  Property::with('user','propertyimages')->where("property_status", "paid")->where("property_type", "0")->where("status", "1")->where('user_id',$userId)->first();
            } else if ($status == 'pending') {
                $statusConst = AppConstants::STATUS_PENDING_APPROVAL;
                $first_listing = isset($userObj->user_paid_property_pending) ? $userObj->user_paid_property_pending : "";
                $first_listing =  Property::with('user','propertyimages')->where("property_status", "paid")->where("property_type", "0")->where("status", "0")->where('user_id',$userId)->first();
                // echo "<pre>";
                // print_r(Property::where("property_status", "paid")->where("property_type", "0")->where("status", "1")->get());
                // echo "<pre>";exit;
            }

            // user_paid_property

    //        return $first_listing;

            $propertyObj = Property::where('status', $statusConst)->where('user_id', $userId)->where("property_status", "paid")->where("property_type", "!=", "0")->OrderByDate()->get();
            // $propertyObj1 = Property::where('status', $statusConst)->where('user_id', $userId)->where("property_status", "paid")->where("property_type", "!=", "0")->OrderByDate()->toSql();
    // echo "<pre>";
    //         print_r($propertyObj1);
    //         // print_r($propertyObj->getBindings());
    //         echo "</pre>";
    // exit;
    //        return $first_listing;
            //$propertyObj = $this->propertyRepository->getPropertyForUser($userId, $statusConst);
            return View('rentals.listrentals_paid', compact('propertyObj', 'first_listing', 'userObj'));
        }

        public function testfunc() {
            $propertyObj = $this->propertyRepository->getCurrentObject(1);
            echo $propertyObj->user->name;
        }

        public function createnewlist(Request $request) {
            $newlistname = $request->get('newlistname');
            if (Wishlist::where('name', $newlistname)->exists()) {
                return "exists";
            }
            $data = array('user_id' => \Auth::user()->id, 'name' => $newlistname);
            $wishlistobj = Wishlist::create($data);
            return $wishlistobj->id;
        }

        public function savewishlists(Request $request) {
            $wishListArr = $request->get('listarr');
            $propertyId = $request->get('listingid');
            $propertyObj = $this->propertyRepository->getCurrentObject($propertyId);
            $propertyObj->wishlists()->sync($wishListArr);
        }

        // public function savewishlists(Request $request) {
        //     $wishListArr = $request->get('listarr');
        //     $propertyId = $request->get('listingid');
        //     $propertyObj = $this->propertyRepository->getCurrentObject($propertyId);
        //     $propertyObj->wishlists()->sync($wishListArr);
        // }

        public function addClassRoomToCart(Request $request) {
			$userId = \Auth::user()->id;
         
            $startDate = $request->get('startdate');
            $endDate = $request->get('enddate');
            $room_id = $request->get('room_id');
            $userId = \Auth::user()->id;
			
			$room_beds = \App\Property::where(["bed_guest_room_id" => $room_id])
														->where(["property_type" => "24", "status" => "1", "property_status" => "paid"])->get();
			
			$newDateStart = date("Y-m-d 11:00:00", strtotime($startDate));
			$newDateEnd = date("Y-m-d 10:00:00", strtotime($endDate));
			$propertyId = 0;
			$today = date('Y-m-d');
			foreach($room_beds as $room_bed){
				$room_bed_id = $room_bed->id;
				$availableCheck = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $room_bed_id);
			
				//print_r($availableCheck);die;
                //return $availableCheck;
                if (count($availableCheck) > 0) {                   
                       continue;                  
                }else{
					// Bed room available so add to cart now 
					$propertyId = $room_bed_id;
					break;
				}

			}
			if($propertyId == 0){
				echo "notavailable";die;
			}else{
				$propertyObj = $this->propertyRepository->getCurrentObject($propertyId);

				$already_in_cart = \App\Cartlists::where('user_id',$userId)->
                                    where('property_id',$propertyId)->
                                    where(function($whr) use ($today){
                                        $whr->where("start_date","<=",$today)->where("end_date",">=",$today)
                                        ->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
                                    })->count();
				if($already_in_cart > 0){
					echo 'already_incart';
				}else{
					$cart = new \App\Cartlists();
				
					$cart->user_id       =  $userId;
					$cart->property_id   =  $propertyId;
					$cart->start_date    =  date("Y-m-d",strtotime($startDate));
					$cart->end_date      =  date("Y-m-d",strtotime($endDate));
					if($cart->save()){
						echo "success";   
					}else{
						echo "error";
					}  
					  
				}
			  
			}
			
		}
        public function addRoomBedToCart(Request $request) {
			
            $userId = \Auth::user()->id;
         
            $startDate = $request->get('startdate');
            $endDate = $request->get('enddate');
            $room_id = $request->get('room_id');
            $userId = \Auth::user()->id;
			//i am getting all beds in that room here
			$room_beds = \App\Property::where(["bed_guest_room_id" => $room_id])
														->where(["property_type" => "26", "status" => "1", "property_status" => "paid"])->get();
			
			$newDateStart = date("Y-m-d 11:00:00", strtotime($startDate));
			$newDateEnd = date("Y-m-d 10:00:00", strtotime($endDate));
			$propertyId = 0;
            //looping thourgh all beds in that room
			foreach($room_beds as $room_bed){
				$room_bed_id = $room_bed->id;
                //checking if already booked that bed or not
				$availableCheck = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $room_bed_id);
			
				//print_r($availableCheck);die;
                //return $availableCheck;
                //ths means already booked and we will check next in that loop
                if (count($availableCheck) > 0) {                   
                       continue;                  
                }else{
					// Bed room available so add to cart now 
					$propertyId = $room_bed_id;
					break;
				}

			}
			$today = date('Y-m-d');

            //if property id not zero then it meas we have bed_property to be added to basket  in else csel please if zero that means we do not have any avaikable bed so we will show error message no beds available in these dates , where is this error emassage_ok wait let show you
			if($propertyId == 0){
				echo "no_bed_available_these_dates";die;
			}else{
                //here means we have available bed to be added to basket

				$propertyObj = $this->propertyRepository->getCurrentObject($propertyId);

                //first i will check if already added this bed to basket means if it is in basket already then we will show error meesage that you have already added 
				$already_in_cart = \App\Cartlists::where('user_id',$userId)->
                                    where('property_id',$propertyId)->
                                    where(function($whr) use ($today){
                                        $whr->where("start_date","<=",$today)->where("end_date",">=",$today)
                                        ->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
                                    })->count();
				if($already_in_cart > 0){
					echo 'already_bed_in_basket';
				}else{

                    // If not in cart already then we will add this bed to cart table 
					$cart = new \App\Cartlists();
				
					$cart->user_id       =  $userId;
					$cart->property_id   =  $propertyId;
					$cart->start_date    =  date("Y-m-d",strtotime($startDate));
					$cart->end_date      =  date("Y-m-d",strtotime($endDate));
					if($cart->save()){
						echo "success";   
					}else{
						echo "error";
					}  
					  
				}
			  
			}
        }
		
        //This add to cart means when click on add to basket next to each bed on details page
        public function addtocart(Request $request) {
            
            $userId = \Auth::user()->id;
         
            $startDate = $request->get('startdate');
            $endDate = $request->get('enddate');
            $propertyId = $request->get('propertyid');
            $userId = \Auth::user()->id;
			
			$newDateStart = date("Y-m-d 11:00:00",strtotime($startDate));
			$newDateEnd = date("Y-m-d 10:00:00",strtotime($endDate));
			$availableCheck = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $propertyId);
			$today = date('Y-m-d');
			//first checking if this bed is already booked or not 
			if (count($availableCheck) == 0) {                   
                //this means this bed is available for booking 
				$propertyObj = $this->propertyRepository->getCurrentObject($propertyId);
				//we will check in cart table first if already added to basket or not
				$already_in_cart = \App\Cartlists::where('user_id',$userId)->
                                    where('property_id',$propertyId)->
                                    where(function($whr) use ($today){
                                        $whr->where("start_date","<=",$today)->where("end_date",">=",$today)
                                        ->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
                                    })->count();
				if($already_in_cart > 0){
                    //this means already in cart and we will show error message 
					echo 'already_bed_in_basket';die;
				}else{
                    //this means its available and not added to the cart , where else do we have these changes_ in front.js don't worry i will do these
					$cart = new \App\Cartlists();
				
					$cart->user_id       =  $userId;
					$cart->property_id   =  $propertyId;
					$cart->start_date    =  date("Y-m-d",strtotime($startDate));
					$cart->end_date      =  date("Y-m-d",strtotime($endDate));
					if($cart->save()){
						echo "success";   
					}else{
						echo "error";
					}  
					  
				}
				
				
			  
			}else{
				echo 'no_bed_available_these_dates';die;
			}
            
        }

        public function showBasket(){
			$userId = \Auth::user()->id;
            // $getResult = Cartlists::with(array('user'))->where('user_id',$userId)->get();
            $today = date("Y-m-d");
            // sleep(30);
            $cartItems = \App\Cartlists::with(array('user'))->
                                    where('user_id',$userId)->
                                    where(function($whr) use ($today){
                                        $whr->where("start_date","<=",$today)->where("end_date",">=",$today)
                                        ->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
                                    })->get();
			
			foreach($cartItems as $cart_item){
			   $cart_item_prp_id = $cart_item->property_id;
			   $pricing = \App\PropertyPricing::where("property_id", $cart_item_prp_id)->first();
			   $cart_item->pricing = $pricing;

			}
			
           
			return View('cart-details', compact('cartItems'));
            
		}
		public function paidCheckout(){ 
			$userId = \Auth::user()->id;
			$today = date("Y-m-d");
            // sleep(30);
             $cartItems = \App\Cartlists::with(array('user'))->
                                    where('user_id',$userId)->
                                    where(function($whr) use ($today){
                                        $whr->where("start_date","<=",$today)->where("end_date",">=",$today)
                                        ->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
                                    })->get();
			
			foreach($cartItems as $cart_item){
			   $cart_item_prp_id = $cart_item->property_id;
			   $pricing = \App\PropertyPricing::where("property_id", $cart_item_prp_id)->first();
			   $cart_item->pricing = $pricing;

			}
			return View('checkout-details', compact('cartItems'));
		}
        public function fetch_cart_data(){
            $userId = \Auth::user()->id;
            // $getResult = Cartlists::with(array('user'))->where('user_id',$userId)->get();
            $today = date("Y-m-d");
            // sleep(30);
            $getResult = \App\Cartlists::with(array('user'))->
                                    where('user_id',$userId)->
                                    where(function($whr) use ($today){
                                        $whr->where("start_date","<=",$today)->where("end_date",">=",$today)
                                        ->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
                                    })->get();
            $html = "";
            // echo "<pre>";
            //     print_r($getResult);
            // exit;
            if(count($getResult) > 0){
                    $html .= "<table border=1 id='bucket_lists'>";
                    $html .= "<tr>";
                    $html .= "<th>Property Name</th>";
                    $html .= "<th>Start Date</th>";
                    $html .= "<th>End Date</th>";
                    $html .= "</tr>";
                foreach ($getResult as $key => $value) {
                    $html .= "<tr>";
                    // $html .= "<td>".$value['property_id']."</td>";
                    $fetchProperty = Property::where('id',$value['property_id'])->first();
					if($fetchProperty->property_type == 26){
						$bed_guest_room_id = $fetchProperty->bed_guest_room_id;
						$bed_type_id = $fetchProperty->bed_type_id;
						$guest_room = \App\UserPaidGuestRoom::where('id',$bed_guest_room_id)->first();
						$room_type_id = $guest_room->room_type_id;
						$room_type = \App\RealRoomTypes::where("is_active", "1")->where("id", $room_type_id)->first();
						
						$bed_type = \App\BedType::where(["is_active" => "1", "id" => $bed_type_id])->first();
						//print_r($bed_type);die;
						
						$room_name = $room_type->room_type.' Room ';
						$bed_name = $bed_type->bed_type.' Bed';
						$service_title = $room_name."(".$bed_name.")";
					}else{
						$service_title = $fetchProperty->exptitle;
					}
					
					
                    $html .= "<td>".$service_title."</td>";
                    $html .= "<td>".date("d-m-Y",strtotime($value['start_date']))."</td>";
                    $html .= "<td>".date("d-m-Y",strtotime($value['end_date']))."</td>";
                    $html .= "</tr>";
                }
                    $html .= "<tr><td colspan=4><a class='btn-pad btn btn-danger full_width' href='".url()."/basket/'>View Basket Details </a> </td></tr>";
                    $html .= "</table>";
            }else{
                echo "<table><tbody><tr><td colspan=4>No Product in you bucket.</td></tr></tbody></table>";
            }

            echo $html;
        }

        public function showFirstCalendarPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            $calendarObj = Booking::bookingForProperty($propertyId);
            list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit Others Property');
            }
            return View('rentals.spacefirstcalendarpaid', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut'));
        }

        public function showCalendarPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            $calendarObj = Booking::bookingForProperty($propertyId);
            list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit Others Property');
            }
            return View('rentals.spacecalendarpaid', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut'));
        }

        public function showCalendar($propertyId) {
            $propertyObj = $this->propertyRepository->getById($propertyId);
            $calendarObj = Booking::bookingForProperty($propertyId);
                // echo "<pre>";
                // print_r($propertyObj);
                // echo "</pre>";
                // exit;
            list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
            if($_SERVER['REMOTE_ADDR'] != '93.42.44.112'){
                if (\Gate::denies('update_property', $propertyObj)) {
                    abort(403, 'You are trying to Edit Others Property');
                }
            }
            return View('rentals.spacecalendar', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut'));
        }

        public function viewRentals($propertyId) {
            $propertyObj = $this->propertyRepository->getByIdActive($propertyId);
           
            //return $propertyObj; are you here?
            $calendarObj = Booking::bookingForProperty($propertyId);
            $DisableCalDates = Schedule::where('id', $propertyId)->get();
            $disable_dates = "";
            if(count($DisableCalDates) > 0){
                foreach (json_decode($DisableCalDates[0]->data) as $disable_date=>$schedule) {
                    $exploaded_str = explode(" ", $disable_date);
                    $disable_dates = $disable_dates .'"'. $exploaded_str[0]. '",';
                }
            }
            $disable_dates = rtrim($disable_dates,",");
           // echo $disable_dates;die;
            $rating = $this->calculateRating($propertyObj);
            $wishProperty = array();
            if (\Auth::user()) {
                $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
            }
            list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
            // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){ here i am not sure why he is checking this IP address and forbidding??
           /* if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                $forbiddenCheckIn1 = '[';
               $forbiddenCheckIn = str_replace("[", "", $forbiddenCheckIn);
            // $forbiddenCheckIn = str_replace(",", "", $forbiddenCheckIn);
             // echo "Count VAr :".count(explode(",",str_replace("[","",$forbiddenCheckIn)));
            // echo "Checkedin : ".$forbiddenCheckIn."<br>";
                foreach(explode(",",str_replace("[","",$forbiddenCheckIn)) as $row){
                    if($row == "," || $row == "" || $row == "]"){
                        continue;
                    }
                    // echo "<pre>";
                    //     print_r($row);
                    // echo "</pre>";
                    $forbiddenCheckIn1 .= "$row,";

                    }
                $forbiddenCheckIn1 = trim($forbiddenCheckIn1,",");
                // $forbiddenCheckIn1 .= '"2019-08-17"';
                $forbiddenCheckIn1 .= ']';
                // echo "<Br>".$forbiddenCheckIn1."<br>";;
                // var_dump($forbiddenCheckIn);
                $forbiddenCheckIn = $forbiddenCheckIn1;

                $forbiddenCheckOut1 = '[';
                $forbiddenCheckOut = str_replace("[", "", $forbiddenCheckOut);
                // $forbiddenCheckOut = str_replace(",", "", $forbiddenCheckOut);
                 // echo "Count VAr :".count(explode(",",str_replace("[","",$forbiddenCheckOut)));
                // echo "Checkedin : ".$forbiddenCheckOut."<br>";
                foreach(explode(",",str_replace("[","",$forbiddenCheckOut)) as $row){
                    if($row == "," || $row == "" || $row == "]"){
                        continue;
                    }
                    // echo "<pre>";
                    //     print_r($row);
                    // echo "</pre>";
                    $forbiddenCheckOut1 .= "$row,";

                }
                $forbiddenCheckOut1 = trim($forbiddenCheckOut1,",");
                // $forbiddenCheckOut1 .= '"2019-08-17"';
                $forbiddenCheckOut1 .= ']';
                // echo "<Br>".$forbiddenCheckOut1."<br>";;
                // var_dump($forbiddenCheckOut);
                $forbiddenCheckOut = $forbiddenCheckOut1;
            }*/ 
             $forbiddenCheckIn1 = rtrim($forbiddenCheckIn,"]");
              $forbiddenCheckOut1 = rtrim($forbiddenCheckOut,"]");
           
                    $forbiddenCheckIn = $forbiddenCheckIn1.$disable_dates."]";
                     $forbiddenCheckOut = $forbiddenCheckOut1.$disable_dates."]";
                   //   echo " Checkedin ".$forbiddenCheckIn."<br>";
            //echo " forbiddenCheckOut ".$forbiddenCheckOut."<br>";die;
                 echo "<input type='hidden' value='".$_SERVER['REMOTE_ADDR']."' id='active_ip'>";
            return View('rentaldetail', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut', 'wishProperty', 'rating'));
        }

        public function viewRentalsPaid($propertyId) {
            $propertyObj = $this->propertyRepository->getByIdActive($propertyId);
            //return $propertyObj;
            $calendarObj = Booking::bookingForProperty($propertyId);
            $rating = $this->calculateRating($propertyObj);
            $wishProperty = array();
            if (\Auth::user()) {
                $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
            }
            list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
            return View('rentaldetailpaid', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut', 'wishProperty', 'rating'));
        }

        public function schoolhouse($userId) {

    //        $propertyObj = Property::query()
    //                    ->select('property.*', 'user_paid_services.house_name', 'user_paid_services.house_slogan', 'user_paid_services.house_desc', 
    //                            'user_paid_services.host_preference', 'user_paid_services.smoking', 'user_paid_services.time_limits',
    //                            'user_paid_services.key_given', 'user_paid_services.guest_table')
    //                    ->leftjoin('user_paid_services', 'user_paid_services.user_id', '=', 'property.user_id')
    //                    ->where('property.user_id', $userId)->get()->first();
            //echo '<pre>';print_r($query );die;


            /* $propertyObj = $this->propertyRepository->getByIdActive($propertyId);
              //return $propertyObj;
              $calendarObj = Booking::bookingForProperty($propertyId);
              $rating = $this->calculateRating($propertyObj);
              $wishProperty = array();
              if (\Auth::user()) {
              $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
              }
              list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj); */
            //return View('rentaldetailpaid', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut', 'wishProperty', 'rating'));
              
            // this holds the data , and on view p[age we can get property/details using this object.
            $userObj = User::with("user_paid_property")->where("id", $userId)->first();
           
            
            $propertyObj = isset($userObj->user_paid_property) ? $userObj->user_paid_property : "";
            if(empty($propertyObj)){
                $propertyObj = Property::where("user_id",$userId)->first();    
            }
            $property_id = isset($propertyObj->id) ? $propertyObj->id : "";
            $forbiddenCheckIn = "";
            $forbiddenCheckOut = "";
            /*echo "<pre>";
                print_r($propertyObj);
            echo "UserId :".\Auth::user()->id."<hr> PropertyID : ".$property_id;die;*/

            //fc_rentalsenquiry
            if (!empty($property_id)) {
                $calendarObj = Booking::bookingForProperty($property_id);
                list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
               
            }



            $isOtherPropertyAvailable = Property::with(array("propertytype"=>function($query){
                $query->where('listing_type','paid');

                $query->where('status',1);
            },"propertyimages"))->where('user_id',$userId)->whereIn('property_type',array('24','25','27','28','29','30'))->count();

          
            
    //        return $userObj->user_paid_property;
            return View('schoolhousepaid', compact('propertyObj', 'userObj', 'forbiddenCheckIn', 'forbiddenCheckOut','isOtherPropertyAvailable'));
        }
		
		
		// Dev Page New Design for School House Details Page 
		public function schoolhousedev($userId) {

            $userObj = User::with("user_paid_property")->where("id", $userId)->first();
			
			
            
            $propertyObj = isset($userObj->user_paid_property) ? $userObj->user_paid_property : "";
			
            if(empty($propertyObj)){
                $propertyObj = Property::where("user_id",$userId)->first();    
            }
            $property_id = isset($propertyObj->id) ? $propertyObj->id : "";
            $forbiddenCheckIn = "";
            $forbiddenCheckOut = "";
          
            if (!empty($property_id)) {
                $calendarObj = Booking::bookingForProperty($property_id);
                list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
               
            }

			
			

            $isOtherPropertyAvailable = Property::with(array("propertytype"=>function($query){
                $query->where('listing_type','paid');

                $query->where('status',1);
            },"propertyimages"))->where('user_id',$userId)->whereIn('property_type',array('24','25','27','28','29','30'))->count();
			
			$pre_made_class_ids = array();
			$pre_made_classes   = \App\UserPaidGuestRoom::where('service_type',"classroom")->whereNotNull('pre_made_id')->get();
			
			if($pre_made_classes->count() > 0){
				foreach($pre_made_classes as $pre_made_class){
					$pre_made_class_ids[] = $pre_made_class->pre_made_id;
				}
			}
			
			$pre_made_actitvity_ids = array();
			$pre_made_activities   = \App\UserPaidGuestRoom::where('service_type',"activity")->whereNotNull('pre_made_id')->get();
			
			if($pre_made_activities->count() > 0){
				foreach($pre_made_activities as $pre_made_activity){
					$pre_made_actitvity_ids[] = $pre_made_activity->pre_made_id;
				}
			}
			
			$real_room_type = \App\RealRoomTypes::where("is_active", "1")->where("is_delete", "0")->get();
			$room_facilities = \App\RoomFacility::where("is_active", "1")->where("is_delete", "0")->get();
			$classroom_facilities = array(1,2,3,4,5);
	
			$bed_types = \App\BedType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $class_types = \App\ClassType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $activity_types = \App\ActivityType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $transfer_types = \App\TransferType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $tour_types = \App\TourType::where(["is_active" => "1", "is_delete" => "0"])->get(); 
            $wow_types = \App\WowType::where(["is_active" => "1", "is_delete" => "0"])->get();
            $wow_types = \App\WowType::where(["is_active" => "1", "is_delete" => "0"])->get();
			
			$currency_obj = new \App\Currency();
			$currencies = $currency_obj->getCurrency(true);
            return View('schoolhousepaiddev', compact('propertyObj','pre_made_class_ids','pre_made_actitvity_ids' ,'userObj','currencies','real_room_type','room_facilities', 'forbiddenCheckIn', 'forbiddenCheckOut','isOtherPropertyAvailable','bed_types','class_types','classroom_facilities'));
        }

		
        public function calculateRating($propertObj) {

            $maxNumberOfStars = 5; // Define the maximum number of stars possible.
            $totalRating = 0;
            $total_count = 0;
            $total_stars = 0;
            if (isset($propertObj->reviews) && $propertObj->reviews->count() > 0) {
                $totalRating = $propertObj->reviews->sum('rateVal');
                $total_count = $propertObj->reviews->count();
                $total_stars = $total_count * $maxNumberOfStars;
            }
    //        echo "totla rating = ".$totalRating."<br />"; 
    //        echo "totla count = ".$total_count."<br />"; 
    //        echo "totla stars = ".$total_stars."<br />"; 
    //        echo "review count = ".$propertObj->reviews->count()."<br />"; 
            // Calculate the total number of ratings.
            $avg = 0;
            $total_rating = 0;
            //if ($propertObj->reviews->count() > 0) {
            if ($total_count > 0) {
                //$avg = (int) ($totalRating / $total_stars) * $maxNumberOfStars;
                $avg = ($totalRating / $total_stars) * 100;
                $total_rating = $totalRating / $total_count;
                $total_rating = ceil($total_rating);
            }
    //        echo "totla rating = ".$total_rating."<br />"; 
    //        echo "avg rating = ".$avg; exit;
            return $total_rating;
        }

        public function prepareRestrictedDays1($calendarObj) {
            $forbiddenCheckIn = '';
            $forbiddenCheckOut = '';
            $DisableCalDate = "";
            $prev = "";
            $all_dates = array();
            $selected_dates = array();
            if ($calendarObj->count() > 0) {
                foreach ($calendarObj as $CRow) {
                    // $DisableCalDate .= '"' . $CRow->the_date . '",';
                    $DisableCalDate .= '"' . date("Y-m-d",strtotime($CRow->the_date)) . '",';
                }
                $forbiddenCheckIn =  $DisableCalDate;
            }
            foreach ($calendarObj as $date) {
                $all_dates[] = trim($date->the_date);
                $date1 = new \DateTime(trim($date->the_date));
                $date2 = new \DateTime($prev);
                $diff = $date2->diff($date1)->format("%a");
                if ($diff == '1') {
                    $selected_dates[] = trim($date->the_date);
                }
                $prev = trim($date->the_date);
                $DisableCalDate = '';
                foreach ($all_dates as $CRow) {
                    $DisableCalDate .= '"' . $CRow . '",';
                }
                $forbiddenCheckIn = $DisableCalDate;
                $DisableCalDate = '';
                foreach ($selected_dates as $CRow) {
                    $DisableCalDate .= '"' . $CRow . '",';
                }
                $forbiddenCheckOut =  $DisableCalDate ;
            }

            return array($forbiddenCheckIn, $forbiddenCheckOut);
        }
        public function prepareRestrictedDays($calendarObj) {
            //this function will return all the booked dates checkin and checkout by property id, please show me in browser? or yediss pin ldetails page first, th eon here then DB to complete cycle okay
            $forbiddenCheckIn = '';
            $forbiddenCheckOut = '';
            $DisableCalDate = "";
            $prev = "";
            $all_dates = array();
            $selected_dates = array();
            if ($calendarObj->count() > 0) {
                foreach ($calendarObj as $CRow) {
                  /*  if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){
                        $DisableCalDate .= '"' . date("Y-m-d H:i:s",strtotime($CRow->the_date)) . '",';                   
                    }else{*/
                        $DisableCalDate .= '"' . date("Y-m-d",strtotime($CRow->the_date)) . '",';                    
                   // }
                }
                $forbiddenCheckIn = $DisableCalDate;
            }
            // echo "<pre>";
            //     print_r($calendarObj);
            // echo "</pre>";
            // exit;
            foreach ($calendarObj as $date) {
              //  if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){
                //    $all_dates[] = date("Y-m-d H:i:s",strtotime(trim($date->the_date)));
                //}else{
                    $all_dates[] = date("Y-m-d",strtotime(trim($date->the_date)));            
                //}

                //if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){
                  //  $date1 = new \DateTime(date("Y-m-d H:i:s",strtotime(trim($date->the_date))));
                //}else{
                    $date1 = new \DateTime(date("Y-m-d",strtotime(trim($date->the_date))));
                //}

                $date2 = new \DateTime($prev);
                // echo "<pre>";
                //     echo "<br> Pre Date :".$prev."<br>";
                //     echo "<br> Date 1 : <br>";
                //     print_r($date1);
                //     echo "<br> ---------------------------- <br>";
                //     echo "<b> DAte 2 </b>";
                //     echo "<br> Date2 :<br>";
                //     print_r($date2);
                // echo "</pre>";
                // echo " <br>  <hr> <br> ";


                $diff = $date2->diff($date1)->format("%a");
                // echo "<br> Daet Diff L:".$diff."<br>";

                if ($diff == '1') {
                    //if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' ||  $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){
                      //  $selected_dates[] = date("Y-m-d H:i:s",strtotime(trim($date->the_date)));
                    //}else{
                        $selected_dates[] = date("Y-m-d",strtotime(trim($date->the_date)));
                    //}
                }
                //if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' ||  $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){
                  //  $prev = date("Y-m-d H:i:s",strtotime(trim($date->the_date)));
                //}else{ 
                    $prev = date("Y-m-d",strtotime(trim($date->the_date)));                
                //}
                $DisableCalDate = '';
                foreach ($all_dates as $CRow) {
                   // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' ||  $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){
                     //   $DisableCalDate .= '"' . date("Y-m-d H:i:s",strtotime($CRow)) . '",';
                    //}else{
                        $DisableCalDate .= '"' . date("Y-m-d",strtotime($CRow)) . '",';                    
                   // }
                }
                $forbiddenCheckIn = $DisableCalDate;
                $DisableCalDate = '';
                // echo "SElected Dates <pre>";
                //     print_r($selected_dates);
                // echo "</pre> <br> ";

                foreach ($selected_dates as $CRow) {
                   // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' ||  $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){
                   //     $DisableCalDate .= '"' . date("Y-m-d H:i:s",strtotime($CRow)) . '",';
                   // }else{
                        $DisableCalDate .= '"' . date("Y-m-d",strtotime($CRow)) . '",';                    
                   // }
                }
                $forbiddenCheckOut = $DisableCalDate;
            }


            // echo "<pre>";
            // echo "<br> All Dates START HERE <br>";
            //     print_r($all_dates);
            //     echo "<br> All Dates END HERE <br>";
            //     print_r($forbiddenCheckIn);
            //     echo "<br>";
            //     print_r($forbiddenCheckOut);
            // echo "</pre>";
            // exit;

            return array($forbiddenCheckIn, $forbiddenCheckOut);
        }    

        public function getlistpopup(Request $request) {
            $userObj = $this->userRepository->findUser(\Auth::user()->id);
            $propertyId = $request->get('listid');
            $wishlistString = "";
            foreach ($userObj->wishlists as $wish) {
                $exists = $wish->properties->contains($propertyId);
                if ($exists) {
                    $wishlistString .= '<li class="bg_white padding10 wishli"><p>' . $wish->name . '</p><div style="float:right;margin-top: -23px;"><i class="fa fa-heart-o whitehrt redhrt" id="' . $wish->id . '"></i></div></li>';
                } else {
                    $wishlistString .= '<li class="bg_white padding10 wishli"><p>' . $wish->name . '</p><div style="float:right;margin-top: -23px;"><i class="fa fa-heart-o whitehrt" id="' . $wish->id . '"></i></div></li>';
                }
            }
            $propertyObj = $this->propertyRepository->getCurrentObject($propertyId);
            $imgURL = "";
            foreach ($propertyObj->propertyimages as $img) {
                $imgURL = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
            }
            return $wishlistString . "*****" . $imgURL;
        }

        public function updateSearch(Request $request) {
            $location = $request->get('location');
            list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($location);
            $checkin = $request->get('checkinDate');
            $checkout = $request->get('checkoutDate');
            \Session::put('checkin', $checkin);
            \Session::put('checkout', $checkout);
            $query = Property::query();
            $query->whereBetween('pro_lat', [$minLat, $maxLat]);
            $query->whereBetween('pro_lon', [$minLong, $maxLong]);

            if ($this->checkNotEmpty($checkin) && $this->checkNotEmpty($checkout)) {
                $newDateStart = date("Y-m-d", strtotime($checkin));
                $newDateEnd = date("Y-m-d", strtotime($checkout));
                $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);
                if (!empty($propertyIdRestriced)) {
                    $query->whereNotIn('id', $propertyIdRestriced);
                }
            }
            $wishProperty = array();
            if (\Auth::user()) {
                $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
            }
            $searchPropertyObj = $query->where('status', 1)->orderBy('created_at', 'desc')->get();
            $searchView = View('rentals.searchresult', compact('searchPropertyObj', 'wishProperty', 'blat', 'blng'));
            return $searchView;
        }

        public function checkNotEmpty($value) {
            if (trim($value) == "") {
                return false;
            }
            return true;
        }

        public function returnWishlistPropertyArray($userId) {
            $usrObj = $this->userRepository->findUser($userId);
            $wishArray = array();
            if (!is_null($usrObj->wishlists)) {
                $property = array();
                foreach ($usrObj->wishlists as $wish) {
                    foreach ($wish->properties as $pr) {
                        array_push($wishArray, $pr->id);
                    }
                }
                //print_r($property);exit;
            }
            return $wishArray;
        }

        public function searchDestination($name, $lat, $lon) {
            $location = $name;
            $checkin = "";
            $checkout = "";
            $min_occupancy = "";
            $prtype = "";
            $location = $name;
            list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($location);
            $wishProperty = array();
            if (\Auth::user()) {
                $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
            }
            //echo $minLat.",". $minLong.",". $maxLat.",". $maxLong;exit;
            $query = Property::query();
            $query->whereBetween('pro_lat', [$minLat, $maxLat]);
            $query->whereBetween('pro_lon', [$minLong, $maxLong]);
            $searchPropertyObj = $query->where('status', 1)->orderBy('created_at', 'desc')->get();
            $propertyTypes = $this->propertyTypeRepository->getAll(true);
            $roomTypes = $this->roomTypeRepository->getAll(true);
            return View('search', compact('propertyTypes', 'roomTypes', 'searchPropertyObj', 'checkin', 'checkout', 'min_occupancy', 'location', 'prtype', 'wishProperty', 'blat', 'blng'));
        }

        public function search($name, Request $request) {
            $lat = $request->get('lat');
            $lon = $request->get('long');
            $checkin = $request->get('checkin');
            $checkout = $request->get('checkout');
            \Session::put('checkin', $checkin);
            \Session::put('checkout', $checkout);
            $location = $name;
            /* Googe Address */
            list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($name);
            // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            // echo '<pre>'; print_r($_REQUEST);echo "$minLat, $minLong, $maxLat, $maxLong, $blat, $blng; ";die();
            //     // echo " User Property Obj <pre>";
            //     //     print_r($userObj);
            //     // echo "</pre>";
            //     exit;
            // }
            /* Querying */
            $query = Property::query();
            $query->whereBetween('pro_lat', [$minLat, $maxLat]);
            $query->whereBetween('pro_lon', [$minLong, $maxLong]);
            $newDateStart = date("Y-m-d", strtotime($checkin));
            $newDateEnd = date("Y-m-d", strtotime($checkout));
            $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);
            if (!empty($propertyIdRestriced)) {
                $query->whereNotIn('id', $propertyIdRestriced);
            }
            $searchPropertyObj = $query->where('status', 1)->orderBy('created_at', 'desc')->get();
            //return $searchPropertyObj[0]->pricing;
            $propertyTypes = $this->propertyTypeRepository->getAll(true);
            $roomTypes = $this->roomTypeRepository->getAll(true);
            $wishProperty = array();
            if (\Auth::user()) {
                $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
            }
            return View('search', compact('propertyTypes', 'roomTypes', 'searchPropertyObj', 'checkin', 'checkout', 'min_occupancy', 'location', 'prtype', 'wishProperty', 'blat', 'blng'));
        }

        public function searchBeds(Request $request) {
            $checkin = $request->get('checkinDate');
            $checkout = $request->get('checkoutDate');

            \Session::put('checkin', $checkin);
            \Session::put('checkout', $checkout);
            return redirect()->back();
        }

        // mysearch: search_WorkAndUsersListingPaid
        public function search2($name, Request $request) {
            // to get all categories id: which are unpaid / 
            ///Jitenodra /  we are using ajaxsearch function to call data and  to filter. 

            /* $lat = $request->get('lat');
              $lon = $request->get('long');
              $checkin = $request->get('checkin');
              $checkout = $request->get('checkout');
              \Session::put('checkin', $checkin);
              \Session::put('checkout', $checkout);
              $location = $name; */
            /* Googe Address */
            //list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($name);
    //        echo '<pre>';
    //        print_r($_REQUEST);
    //        echo "$minLat, $minLong, $maxLat, $maxLong, $blat, $blng; ";
    //        die();
            /* Querying */
            /* $query = Property::query();
              $query->select("property.*");
              $query->join("users", "users.id", "=", "property.user_id");
              $query->leftjoin("user_paid_services", "property.user_id", "=", "user_paid_services.user_id");


              $newDateStart = date("Y-m-d", strtotime($checkin));
              $newDateEnd = date("Y-m-d", strtotime($checkout));
              $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);
              if (!empty($propertyIdRestriced)) {
              // here is the code where we ignore the propertyids , if exists in booking table
              $query->whereNotIn('property.id', $propertyIdRestriced);
              }
              $propStatus = array();
              if ($request->get('search')) {
              $searchType = explode(',', $request->get('search'));
              foreach ($searchType as $search) {
              if ($search == 'work') { // for work : unpaid or unpaidflash
              array_push($propStatus, "unpaid");
              array_push($propStatus, "unpaidflash");
              } else { // for live, learn, enjoy : paid
              array_push($propStatus, "paid");
              }
              }
              $query->whereIn('property.property_status', $propStatus);
              }
              if (!empty($minLat) && !empty($maxLat) && !empty($minLong) && !empty($maxLong)) {
              if (in_array("paid", $propStatus)) {
              //if
              $query->whereRaw('IF (property.addr_work_org_same = 0, '
              . '(property.pro_lat BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (property.pro_lon BETWEEN ' . $minLong . ' AND ' . $maxLong . '),'
              // if the house property have different location means
              . 'IF (user_paid_services.house_property_differ = 1, '
              . '(user_paid_services.proplatbox BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (user_paid_services.proplonbox BETWEEN ' . $minLong . ' AND ' . $maxLong . '), '
              . 'IF (user_paid_services.house_property_differ = 0, '
              . '(user_paid_services.houselatbox BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (user_paid_services.houselonbox BETWEEN ' . $minLong . ' AND ' . $maxLong . '), '
              . '(users.org_lat BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (users.org_lan BETWEEN ' . $minLong . ' AND ' . $maxLong . ')'
              . ' )'
              . ' )'
              . ' )');
              } else {
              $query->whereBetween('property.pro_lat', [$minLat, $maxLat]);
              $query->whereBetween('property.pro_lon', [$minLong, $maxLong]);
              }
              }

              $searchPropertyObj = $query->where('property.status', 1)->orderBy('property.created_at', 'desc')->groupBy('property.user_id')->get(); */
            //return $searchPropertyObj;
    //        $query = Property::query();
    //        $typesArr = [];
    //
    //        $searchListingType = ($name == 'work') ? 'unpaid' : 'paid';
    //
    //        $getPropertyTypes = $this->propertyTypeRepository->getByType($searchListingType);
    //        foreach ($getPropertyTypes as $row) {
    //            $typesArr[] = $row->id;
    //        }
    //
    //        $searchPropertyObj = $query->whereIn('property_type', $typesArr)->orderBy('created_at', 'desc')->get();
            // To get user accepted job
    //        $checkin = $request->get('checkin');
    //        $checkout = $request->get('checkout');
    //        \Session::put('checkin', $checkin);
    //        \Session::put('checkout', $checkout);
            $getUserAcceptedJob = '';
            if (\Auth::user()) {
                //$wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
                //fc_rentalsenquiry
                $userid = \Auth::user()->id;
                $getUserAcceptedJob = RentalsEnquiry::select('property.exptitle', 'property.title', 'fc_rentalsenquiry.user_id', 'fc_rentalsenquiry.prd_id')
                                ->leftjoin('property', 'property.id', '=', 'fc_rentalsenquiry.prd_id')
                                ->where('fc_rentalsenquiry.user_id', $userid)->whereIn('property.property_status', ['unpaid', 'unpaidflash'])->where('approval', 'Accept')->orderBy('fc_rentalsenquiry.id', 'desc')->get()->first();
               // echo '<pre>';print_r($getUserAcceptedJob);die; 
            }
    //        return $getUserAcceptedJob;
            //return View('search2', compact('searchPropertyObj', 'getUserAcceptedJob'));
            return View('search2', compact('getUserAcceptedJob'));
        }

        // this is the request from search page, when anything (checkboxes(live,work etc), address, input (start date, end date)) changes
        public function ajaxsearch(Request $request) {
            
            $location = $request->get('location');
            //why are we limiting with bounds the Google API_, its because we are filtering the results for particular areaor address
            //fine but it is not answering the point of Nembro not showing in Italyunds provide the north and south
            //here we are getting the bounds of area using user address type in search box
            list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($location);
            //echo '<pre>'; print_r($_REQUEST);echo "$minLat, $minLong, $maxLat, $maxLong, $blat, $blng; ";die();
            $checkin = $request->get('checkinDate');
            $checkout = $request->get('checkoutDate');
           

            \Session::put('checkin', $checkin);
            \Session::put('checkout', $checkout);

    //there has been no coding apart from yours about voucher, problem is due to uploads you did ear;lier today
    //there will be no problem let me fix this i have shown the call for testing

            $query = Property::query();
            //$query->whereBetween('pro_lat', [$minLat, $maxLat]);
            //$query->whereBetween('pro_lon', [$minLong, $maxLong]);
            // to check if we get both dates (start and end) then will check
            if ($this->checkNotEmpty($checkin) && $this->checkNotEmpty($checkout)) {
                $newDateStart = date("Y-m-d 11:00:00", strtotime($checkin));
                $newDateEnd = date("Y-m-d 10:00:00", strtotime($checkout));
                $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);

                //echo "<pre>";print_r($propertyIdRestriced);die;
                
                if (!empty($propertyIdRestriced)) {
                    $query->whereNotIn('property.id', $propertyIdRestriced);
                }
            }
            /* $wishProperty = array();
              if (\Auth::user()) {
              $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
              } */

            // to filter data : property status: paid, unpaid, unpaidflash., if not any checked then query will be fine, that's why we first checked if(val).
            $propStatus = [];
            if ($request->get('search')) {
                $searchType = explode(',', $request->get('search'));

                foreach ($searchType as $search) {
                    if ($search == 'work') { // for work : unpaid or unpaidflash
                        $propStatus[] = 'unpaid';
                        $propStatus[] = 'unpaidflash';
                    } else { // for paid. paid.type live, learn, enjoy will be managed in future
                        $propStatus[] = 'paid';
                    }
                }
                $query->whereIn('property.property_status', $propStatus);
            }

         



            //$searchPropertyObj = $query->where('status', '1')->whereIn('property_status', $propStatus)->orderBy('created_at', 'desc')->get();We used the property_status just above, because if in case there is no checkbox checked for (live, work etc), the query must not break.
            //$searchPropertyObj = $query->where('status', '1')->orderBy('created_at', 'desc')->get(); // commented out because, we now added/join another table data:user_paid_services, to get paid title desc.
            // Here we joined the table property with user_paid_services, so show the details(title, desc) : according : paid, unpaid
            $query->select('property.*', 'user_paid_services.house_slogan', 'user_paid_services.house_desc', 'user_paid_services.home_type')
                    ->leftjoin('user_paid_services', 'user_paid_services.user_id', '=', 'property.user_id')
                    ->where('property.status', '1');
            if (in_array('paid', $propStatus)) {
                //if the paid type then lat long search will be in both: property and user.pad.services
                //here we are using the bounds latlongs
                $query->where(function ($query) use ($minLat, $maxLat, $minLong, $maxLong) {
                    $query->where(function ($query) use ($minLat, $maxLat, $minLong, $maxLong) {
                        $query->whereBetween('pro_lat', [$minLat, $maxLat]);
                        $query->whereBetween('pro_lon', [$minLong, $maxLong]);
                    })->orWhere(function ($query) use ($minLat, $maxLat, $minLong, $maxLong) {
                        $query->whereBetween('user_paid_services.houselatbox', [$minLat, $maxLat]);
                        $query->whereBetween('user_paid_services.houselonbox', [$minLong, $maxLong]);
                    });
                });
            } else {
                //if the unpaid : seach only property table for latlong.
                $query->whereBetween('pro_lat', [$minLat, $maxLat]);
                $query->whereBetween('pro_lon', [$minLong, $maxLong]);
            }


            

            $searchPropertyObj = $query->orderBy('property.created_at', 'desc')->get();
            $profile_noimage = 'no_prop_image.png';
           
            //$query->whereBetween('user_paid_services.houselatbox', [$minLat, $maxLat]);
            //$query->whereBetween('user_paid_services.houselonbox', [$minLong, $maxLong]);
            /* var_dump($query->toSql());
              $bindings = $query->getBindings();
              print_r($bindings );
              die; */

            $wishProperty = array();
            if (\Auth::user()) {
                $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
            }
            $timeFromPaidToWork = array(); // time from paid to work 

            if (\Auth::user()) {
                //$wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
                $userid = \Auth::user()->id;
                $getAddressTime = RentalsEnquiry::select('distancehomework.time', 'distancehomework.paidPropId', 'fc_rentalsenquiry.prd_id')
                                ->leftjoin('distancehomework', 'distancehomework.unpaidPropId', '=', 'fc_rentalsenquiry.prd_id')
                                ->where('fc_rentalsenquiry.user_id', $userid)->where('approval', 'Accept')->orderBy('fc_rentalsenquiry.id', 'desc')->get();
               // echo "<pre>";print_r($getAddressTime);die; 
                foreach ($getAddressTime as $row) {
                    $timeFromPaidToWork[$row->paidPropId] = $row->time;
                }
                //$timeFromPaidToWork['143'] = 22;//just to test
            }

    //        return $timeFromPaidToWork;
         // echo "<pre>";
         //  print_r($timeFromPaidToWork);
          // echo "</pre>";die;
    //        exit;
            // adding original serach results for testing
            $name = $request->get('location');
            $lat = $request->get('lat');
            $lon = $request->get('long');
            $checkin = $request->get('checkinDate');
            $checkout = $request->get('checkoutDate');
            \Session::put('checkin', $checkin);
            \Session::put('checkout', $checkout);
            $location = $name;
            /* Googe Address */
            list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($name);

    //        echo '<pre>'; print_r($_REQUEST);
    //        echo "min lat = $minLat --, min long = $minLong --, max lat = $maxLat --, max long = $maxLong --, $blat, $blng; ";
    //        die();
            /* Querying */
            $query = Property::query();
            $query->select("property.*");
            $query->join("users", "users.id", "=", "property.user_id");
            $query->leftjoin("user_paid_services", "property.user_id", "=", "user_paid_services.user_id");


            $newDateStart = date("Y-m-d 11:00:00", strtotime($checkin));
            $newDateEnd = date("Y-m-d 10:00:00", strtotime($checkout));
            $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);

            //echo "<pre>";
            //print_r($propertyIdRestriced);die;
            if (!empty($propertyIdRestriced)) {
                // here is the code where we ignore the propertyids , if exists in booking table
                $query->whereNotIn('property.id', $propertyIdRestriced);
            }
            $query->whereRaw("(property.property_status = 'unpaid' OR property.property_status = 'unpaidflash')");
            // cfonor unpaid and unpaidflash, condition is wrong, too simplistic
            $query->whereBetween('property.pro_lat', [$minLat, $maxLat]); // this is Listing, propoerty, lat long yes as this we have to change with new condition
            // I thouhgt about logic used and think it is wrong to call both usera nad property tables in search2
            // anyway I really have to see this working
            // as we already sued 3 tables in dearing of paid property, user and user_paid_Services for lat long so we have to add checks
            // for unpaid also those with almost same conditions like if property Check IF Workplace address is different from LegalAddress
            // than get from different lat longs and if not check than from user but let me check the update code first
            // sure, thought we had done this time ago yes but for paid services as we have to add this on unpiad but we shift to some other work i thing
            // yes remember we shift to live the counts are not showing properly and not the school housess so trying to fix that and forget about this
            // let me work on this. k

            
            $query->whereBetween('property.pro_lon', [$minLong, $maxLong]);
          
             $query->where('property.status', 1)->orderBy('property.created_at', 'desc');
             //->groupBy('property.user_id'); //added groupby, ->groupBy('property.user_id'), to make double listing disapper from searc2 page, but truely listing booked then shows again *perhaps double entry(
             $unpaidPropertyObj = $query->get();
            $unpaid_count = $unpaidPropertyObj->count();
            // end of unpaid quering
           
            // quering the paid services starts
            $query_paid = Property::query();
            $query_paid->select("property.*");
            $query_paid->join("users", "users.id", "=", "property.user_id");
            $query_paid->leftjoin("user_paid_services", "property.user_id", "=", "user_paid_services.user_id");


            $newDateStart = date("Y-m-d", strtotime($checkin));
            $newDateEnd = date("Y-m-d", strtotime($checkout));
            $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);
    //        print_r($propertyIdRestriced); exit;
          //  if($_SERVER['REMOTE_ADDR'] != '2.43.240.216'){
            if (!empty($propertyIdRestriced)) {
                // here is the code where we ignore the propertyids , if exists in booking table
                $query_paid->whereNotIn('property.id', $propertyIdRestriced);
            }
        //}
           
            
            //if($_SERVER['REMOTE_ADDR'] != '2.43.240.216'){
                if (!empty($minLat) && !empty($maxLat) && !empty($minLong) && !empty($maxLong)) {
                $query_paid->whereRaw('IF (property.addr_work_org_same = 0, '
                        . '(property.pro_lat BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (property.pro_lon BETWEEN ' . $minLong . ' AND ' . $maxLong . '),'
                        /* if the house property have different location means */
                        . 'IF ((user_paid_services.house_property_differ = 1) AND (user_paid_services.proplatbox != ""), '
                        . '(user_paid_services.proplatbox BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (user_paid_services.proplonbox BETWEEN ' . $minLong . ' AND ' . $maxLong . '), '
                        . 'IF ((user_paid_services.house_property_differ = 0) AND (user_paid_services.houselatbox != ""), '
                        . '(user_paid_services.houselatbox BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (user_paid_services.houselonbox BETWEEN ' . $minLong . ' AND ' . $maxLong . '), '
                        . '(users.org_lat BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (users.org_lan BETWEEN ' . $minLong . ' AND ' . $maxLong . ')'
                        . ' )'
                        . ' )'
                        . ' )');
            }
            //}
            $query_paid->where("property.property_type", "0");
            $query_paid->whereRaw("(property.property_status = 'paid')");
            $paidPropertyObj = $query_paid->where('property.status', 1)->orderBy('property.created_at', 'desc')->groupBy('property.user_id')->get();
    //        $paidPropertyObj = $query_paid->where('property.status', 1)->orderBy('property.created_at', 'desc')->groupBy('property.user_id')->toSql();

            
            if (sizeof($paidPropertyObj) > 0) {
                foreach ($paidPropertyObj as $paidProperty) {
                    $check_bed_listing = \App\Property::where("user_id", $paidProperty->user_id)->where(["status" => "1", "property_status" => "paid", "property_type" => "26"])->get();
                    if ($check_bed_listing->count() <= 0) {
                        $paidPropertyObj = $paidPropertyObj->except($paidProperty->id);
                    }
                }
            }
            $paid_count = $paidPropertyObj->count();

             //if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                // echo '<pre>'; print_r($searchPropertyObj); die(); // to check , what data we are getting from query.
                // echo '<pre>'; print_r($paidPropertyObj)."</pre>";
                   // echo "min lat = $minLat --, min long = $minLong --, max lat = $maxLat --, max long = $maxLong --, $blat, $blng; ";
                   //  echo " User Property Obj <pre>";
                   //      // print_r($userObj);
                   //  echo "</pre>";
                    // exit;
                    // }

            // ends
            
      
       // if($_SERVER['REMOTE_ADDR'] != '2.43.240.216'){
            $searchPropertyObj = array();
       // }
            $propStatus = array();
            if ($request->get('search')) {
                $searchType = explode(',', $request->get('search'));
                if (in_array("live", $searchType) && !in_array("work", $searchType)) {
                    $searchPropertyObj = $paidPropertyObj;
                } else if (in_array("work", $searchType) && !in_array("live", $searchType)) {
                    $searchPropertyObj = $unpaidPropertyObj;
                } else if (in_array("work", $searchType) && in_array("live", $searchType)) {
                    //$searchPropertyObj = array_merge($unpaidPropertyObj, $paidPropertyObj);
                    $searchPropertyObj = $unpaidPropertyObj->merge($paidPropertyObj);
                }
                //$query->whereIn('property.property_status', $propStatus);
            } else {
                $searchPropertyObj = $unpaidPropertyObj->merge($paidPropertyObj);
            }




            $getUserAcceptedJob = '';
            if (\Auth::user()) {
                $userid = \Auth::user()->id;
                $getUserAcceptedJob = RentalsEnquiry::select('property.exptitle', 'property.title', 'fc_rentalsenquiry.user_id', 'fc_rentalsenquiry.prd_id')
                                ->leftjoin('property', 'property.id', '=', 'fc_rentalsenquiry.prd_id')
                                ->where('fc_rentalsenquiry.user_id', $userid)->whereIn('property.property_status', ['unpaid', 'unpaidflash'])->where('approval', 'Accept')->orderBy('fc_rentalsenquiry.id', 'desc')->get()->first();
            }

            return View('partials.searchresultajax', compact('searchPropertyObj', 'paid_count', 'unpaid_count', 'profile_noimage', 'wishProperty', 'timeFromPaidToWork', 'getUserAcceptedJob'));
            /*
              // to get all categories id: which are unpaid
              $typesArr = [];

              $name = 'work';
              $searchListingType = ($name == 'work') ? 'unpaid' : 'paid';

              $getPropertyTypes = $this->propertyTypeRepository->getByType($searchListingType);
              foreach($getPropertyTypes  as $row){
              $typesArr[] =$row->id;
              }
              //$searchPropertyObj = $query->whereIn('property_type', $typesArr)->orderBy('created_at', 'desc')->get();
             */
        }

        public function otherpaidservices($user_id){
        
        // $property = Property::with("propertytype")->where('id',$school_id)->where('propertytype.listing_type','paid')->where('propertytype.status',1)->get();
        //SELECT * FROM `property` as p JOIN `property_type` as pt ON p.property_type = pt.id WHERE p.`property_type` IN ('24','25','27','28','29','30') AND p.user_id = 37 #AND pt.status = 1 AND pt.listing_type='paid'
        // echo "User Id :".$user_id."<br>";
        // $user_id = \Auth::user()->id;
        $propertyObj = Property::with(array("propertytype"=>function($query){
                $query->where('listing_type','paid');

                $query->where('status',1);
            },"propertyimages"))->where('user_id',$user_id)->whereIn('property_type',array('24','25','27','28','29','30'))->get();
        // echo "<br> Property Obj<pre>";
        //     print_r($propertyObj);
        // echo "</pre><br> ";
        // exit;
        // $propertyObj1 = Property::with(array("propertyimages"))->where('user_id',$user_id)->whereIn('property_type',array('0'))->get();
        
        return View('otherpaidservices',compact('propertyObj'));
    }

        public function getNorthAndSouthBounds($location) {
            $arrContextOptions = array(
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );
            // $google_map_api = "AIzaSyAz0HqeAgq5xNt9DSUSMPIVlmLK_uuAoDc";
            // $google_map_api = "AIzaSyA0BQbSzrX8W4conO6nmF9-LzpH4R-y1jU";
            $google_map_api = 'AIzaSyC646z-W9bGxhDtgjILxonsK2KI5TU-z68';
            $get_address = str_replace(" ", "", $location);
            $googleAddress = $get_address;

            $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$googleAddress&sensor=false&key=$google_map_api", false, stream_context_create($arrContextOptions));
            $json = json_decode($json);
            if (isset($json->results[0]->address_components))
            $newAddress =$json->results[0]->address_components;// $json->{'results'}[0]->{'address_components'};
 
            $minLat = "";
            if (isset($json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lat'})) {
                $minLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lat'};
            } else if (isset($json->{'results'}[0]->{'geometry'}->{'viewport'}->{'southwest'}->{'lat'})) {
                $minLat = $json->{'results'}[0]->{'geometry'}->{'viewport'}->{'southwest'}->{'lat'};
            }
            //$minLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lat'};
            $minLong = "";
            if (isset($json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lng'})) {
                $minLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lng'};
            } else if (isset($json->{'results'}[0]->{'geometry'}->{'viewport'}->{'southwest'}->{"lng"})) {
                $minLong = $json->{'results'}[0]->{'geometry'}->{'viewport'}->{'southwest'}->{"lng"};
            }
            $maxLat = "";
            if (isset($json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lat'})) {
                $maxLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lat'};
            } else if (isset($json->{'results'}[0]->{'geometry'}->{'viewport'}->{'northeast'}->{'lat'})) {
                $maxLat = $json->{'results'}[0]->{'geometry'}->{'viewport'}->{'northeast'}->{'lat'};
            }
            $maxLong = "";
            if (isset($json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lng'})) {
                $maxLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lng'};
            } else if (isset($json->{'results'}[0]->{'geometry'}->{'viewport'}->{'northeast'}->{'lng'})) {
                $maxLong = $json->{'results'}[0]->{'geometry'}->{'viewport'}->{'northeast'}->{'lng'};
            }
            //$minLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lng'};
            //$maxLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lat'};
            //$maxLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lng'};
            $bastlat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $baselon = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
            return array($minLat, $minLong, $maxLat, $maxLong, $bastlat, $baselon);
        }

        public function checkAvailablityOfRentals($newDateStart, $newDateEnd) {
            $propertyIdRestriced = array();

            $availableCheck = Booking::availabilityCheck($newDateStart, $newDateEnd);
           
            if (is_object($availableCheck)) {
                foreach ($availableCheck as $restrick_data) {
                    array_push($propertyIdRestriced, $restrick_data->PropId);
                }
            }
            //print_r($propertyIdRestriced);exit;
            return $propertyIdRestriced;
        }

        public function showpricedetail(Request $request) {
            /* $propertyId = $request->get('listid');
              $numberOfDays = $request->get('noofdays');
              $propertyObj = $this->propertyRepository->getCurrentObject($propertyId);
              $total = $propertyObj->price_night * $numberOfDays;
              $commissionObj = $this->commissionRepository->getById(AppConstants::GUEST_COMMISSION, true);
              $commission = 0;
              if ($commissionObj->count() > 0) {
              $commission = $this->commissionRepository->calculateCommission($commissionObj, $total);
              }
              $taxObj = $this->commissionRepository->getById(AppConstants::TAX_COMMISSION, true);
              $tax = 0;
              if ($taxObj->count() > 0) {
              $tax = $this->commissionRepository->calculateCommission($taxObj, $total);
              }

              //propertyprice,totalpricewithoutothercharges,commission,tax
              return $propertyObj->price_night."***".$total."***".$commission."***".$tax; */
            return "true";
        }

        public function instantbook(Request $request) {
            $checkIn = date('Y-m-d H:i:s', strtotime($request->get('sdate')));
            $checkOut = date('Y-m-d H:i:s', strtotime($request->get('edate')));
            $productId = $request->get('listid');
            $propertyObj = $this->propertyRepository->getCurrentObject($productId);
            $renterId = $propertyObj->user->id;
            $noOfNyts = $request->get('days');
            $productPrice = $propertyObj->price_night;
            $serviceFee = $request->get('commissionamount');
            $totalPrice = ($propertyObj->price_night * $noOfNyts) + $serviceFee;
            $noOfGuests = $request->get('guests');
            $msg = "";

            $dataArr = array(
                'checkin' => $checkIn,
                'checkout' => $checkOut,
                'numofdates' => $noOfNyts,
                'serviceFee' => $serviceFee,
                'totalAmt' => $totalPrice,
                'caltophone' => '',
                'enquiry_timezone' => '',
                'user_id' => \Auth::user()->id,
                'renter_id' => $renterId,
                'NoofGuest' => $noOfGuests,
                'prd_id' => $productId
            );


            $booking_status = array(
                'booking_status' => 'Accept'
            );

            $dataArr = array_merge($dataArr, $booking_status);
            $enquiryObj = RentalsEnquiry::create($dataArr);
            $insertid = $enquiryObj->id;

            if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                $val = 10 * $insertid + 8;
                $val = 1500000 + $val;
                $bookingno = "EN" . $val;
                $newdata = array(
                    'Bookingno' => $bookingno
                );
                RentalsEnquiry::where('id', $insertid)->update($newdata);
            }

            $dataArr = array(
                'productId' => $productId,
                'bookingNo' => $bookingno,
                'senderId' => \Auth::user()->id,
                'receiverId' => $renterId,
                'subject' => 'Booking Request : ' . $bookingno,
                'message' => ""
            );
            MedMessage::create($dataArr);
            $dataArr = array(
                'productId' => $productId,
                'bookingNo' => $bookingno,
                'senderId' => $renterId,
                'receiverId' => \Auth::user()->id,
                'subject' => 'Booking Request : ' . $bookingno,
                'message' => ""
            );
            MedMessage::create($dataArr);
            \Session::put('bookingno', $bookingno);
            return $bookingno;
        }

        public function checkout() {
            $enquiryObj = RentalsEnquiry::where('Bookingno', \Session::get('bookingno'))->first();
            $propertyObj = $this->propertyRepository->getByIdActive($enquiryObj->prd_id);
            return View('bookingresult', compact('enquiryObj', 'propertyObj'));
        }

        public function getEnquiryDates($enquiresObj, $checkIn, $checkOut) {
            $enquiryDates = array();
            $count = 0;
            foreach ($enquiresObj as $enq) {
                $dates = $this->getDatesFromRange($enq->checkin, $enq->checkout);
                $i = 1;
                $dateMinus1 = count($dates);
                foreach ($dates as $date) {
                    if ($i <= $dateMinus1) {
                        array_push($enquiryDates, $date);
                    }
                    $i++;
                }
                if (in_array($checkIn, $enquiryDates) || in_array($checkOut, $enquiryDates)) {
                    $count++;
                }
            }

            return $count;
        }

        public function sendBookingEmails($availableCheck = 1, $propertyObj = array(), $enquiryObj = array(), $condition = "", $hr_email = "", $worktutoremail = "", $organizationemail = "") {
           
            
            if ((!empty($propertyObj) && isset($propertyObj->id)) && (!empty($enquiryObj) && isset($enquiryObj->id))) {
                //listing owner user object
                $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();

                //sender user object
                $user = User::where("id", \Auth::user()->id)->select("id", "email", "name")->first();
                // checking if the request is for bed .type or not i think we have to remove that check for classes and other type of listings_ I think to see working, just clone and make new title, we have not spoken about this anyway yet, agree?yes so for 2 minute fix, just add class type agree?ok sure wait
                if (isset($propertyObj->property_type) && $propertyObj->property_type == "26") {
                    //sending to owner, that is user listing ok wait let me show you that email template
                    $this->mailNotificationsForUser->sendPaidTypeSchoolHouseBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                    //sending to user browsing , that is parent & student
                    $this->mailNotificationsForUser->sendPaidTypeSchoolHouseUserBookingEmail($user->email, $organizationemail, $propertyObj, $user, $enquiryObj);
                //sending same email for classes to see all working (even though I didnt think necessary ok we can not send email for classes ) Please leave it and show me what you wanted to, I hopeit was not this. ok wait 
                }else if (isset($propertyObj->property_type) && $propertyObj->property_type == "24") { 
                    //sending to owner, that is user listing ok wait let me show you that email template, this will send the same content, correct?  yes but we can change subject too by adding one more variable to its parameterts.  do not think sending multimple emails is what I had in mind.. nof for testing it is fine.. so we send same content_ for now, we have not discussed about this, agree? yes so leave it as now
                    $this->mailNotificationsForUser->sendPaidTypeSchoolHouseBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                    //sending to user browsing , that is parent & student
                    $this->mailNotificationsForUser->sendPaidTypeSchoolHouseUserBookingEmail($user->email, $organizationemail, $propertyObj, $user, $enquiryObj);
                    
                }
                // else for reqular email system which is runnig before / Olfuser  first listing not showing in user-s @your listing@
                else {
                    if (!empty($condition) && $condition == "hr") {
                        if ($availableCheck == 0) {
                            //checking if user have its first choice
                            if(isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                                //$this->mailNotificationsForUser->sendPaidHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                            }else{
								$this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $enquiryObj);
                            }
                        }else{
                            if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                            } else {
                                $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$worktutoremail);
                            }
                        }
                    }
                    // second codition
                    else if (!empty($condition) && $condition == "no_hr") {
                        if ($availableCheck == 0) {
                            if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                                if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                    $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                                    //$this->mailNotificationsForUser->sendPaidBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                                } else {
                                    $this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                                }
                            } else {
                                if (isset($propertyObj->renter_id)) {
                                    if (isset($renter_user->email) && !empty($renter_user->email)) {
                                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                            $this->mailNotificationsForUser->sendPaidUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                                        } else {
                                            $this->mailNotificationsForUser->sendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                                        }
                                    }
                                }
                            }
                        } else {
                            if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                                //$this->mailNotificationsForUser->sendPaidBookingNotification($propertyObj, $enquiryObj);
                            } else {
                                // $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj);
                                $requested_user = User::where("id", $enquiryObj->user_id)->select("id", "email", "name")->first();
                                    // $requestedUseremail = $requested_user->email;
                                    $renterEmail = $renter_user->email;
                                    // if checkbox @Work Tutor NOT YOU Flagged the BD value work?org?same will be 0
                                    // echo "<br> Work Org Same :".$propertyObj->work_org_same."<br>";
                                if($propertyObj->work_org_same == 0){
                                    $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$worktutoremail,$renterEmail);
                                    // $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$requestedUseremail);
                                }else{
                                   
                                   // echo "<br> Requested Email ID :".$renterEmail;
                                   // echo "<br> Requesteing Email Id :".$requestedUseremail;
                                    $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$requestedUseremail);
                                    $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$renterEmail);
                                }
                            }
                        }
                    }
                }
            }
        }

        public function request_booking(Request $request) {
            $userId = \Auth::user()->id;
            $userEnquiries = RentalsEnquiry::where('user_id', $userId)->get();
            // $checkIn = date('Y-m-d H:i:s', strtotime($request->get('sdate')));
            // $checkOut = date('Y-m-d H:i:s', strtotime($request->get('edate')));
            $checkIn = date('Y-m-d H:i:s', strtotime($request->get('sdate')." 11:00:00"));
            $checkOut = date('Y-m-d H:i:s', strtotime($request->get('edate')." 10:00:00"));
            $allowBooking = 0;
            $productId = $request->get('listid');
            $propertyObj = $this->propertyRepository->getCurrentObject($productId);
            //$countEnquiry = 0;
            $settingsObj = PropertySettings::propertySettings();
            $user_voucher = User::where('id', $userId)->first();
            
            $countEnquiry = 0;
            $allowedEnquiryCount = 0;
            if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                $is_paid = 1;
                $user_paid_request_counts = RentalsEnquiry::select("fc_rentalsenquiry.*")
                        ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                        ->where("fc_rentalsenquiry.user_id", $userId)
                        ->where("property_status", "paid")
                        ->get();

                //$countEnquiry = $user_paid_request_counts->count();
                //$allowedEnquiryCount = $settingsObj->school_house_booking_request;
                $user_credits = \App\UserCredit::where("user_id", $userId)->where("send_to_schoolhouse", ">", "0")->get();
               $total_schoolhouse_sent = 0;
                if (sizeof($user_credits) > 0) {
                    $countEnquiry = $user_credits->count();
                    
                    foreach ($user_credits as $key => $user_credit) {
                        $total_schoolhouse_sent += $user_credit->send_to_schoolhouse;
                    }
                }

                $countEnquiry = $total_schoolhouse_sent;
               
                if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
                    $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
                   
                   
                    if (isset($get_voucher->send_to_schoolhouse) && !empty($get_voucher->send_to_schoolhouse)) {

                        $allowedEnquiryCount = $get_voucher->send_to_schoolhouse;

                    } else {
                        // are you there?why here is using default voucher if its credits are not enough?
                        $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
                        if (isset($get_voucher->id)) {
                            $userWallet = new \App\UserWallet();
                            $userWallet->user_id = $userId;
                            $userWallet->vouchaercredits_id = $get_voucher->id;
                            $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                            $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                            $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                            $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                            $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                            $userWallet->creadit_type = "voucher";
                            $userWallet->save();
                            $allowedEnquiryCount = $get_voucher->send_to_schoolhouse;
                        }
                    }
                } else {
                    $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
                    if (isset($get_voucher->id)) {
                        $user_voucher->vouchercredit_id = $get_voucher->id;
                        $user_voucher->save();
                        $userWallet = new \App\UserWallet();
                        $userWallet->user_id = $userId;
                        $userWallet->vouchaercredits_id = $get_voucher->id;
                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                        $userWallet->creadit_type = "voucher";
                        $userWallet->save();
                        $allowedEnquiryCount = $get_voucher->send_to_schoolhouse;
                    }
                }
            } else {

                $is_paid = 0;
                $user_unpaid_request_counts = RentalsEnquiry::select("fc_rentalsenquiry.*")
                        ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                        ->where("fc_rentalsenquiry.user_id", $userId)
                        ->where("property_status", "!=", "paid")
                        ->get();
    //            $countEnquiry = $user_unpaid_request_counts->count();
               // $user_credits = \App\UserCredit::where("user_id", $userId)->where("send_to_unpaid", ">", "0")->get();
               // $countEnquiry = $user_credits->count();
                $countEnquiry = 0;
                $get_credits_sendtounpaid_all = \App\UserCredit::where("user_id", $userId)->where("send_to_unpaid", ">", "0")->get();
                //calculating nights herer
                $total_send_to_unpaid_sent = 0;
                foreach ($get_credits_sendtounpaid_all as $key => $send_to_unpaid) {
                    $total_send_to_unpaid_sent += $send_to_unpaid->send_to_unpaid;
                }
               // echo "<pre>";
                $countEnquiry = $total_send_to_unpaid_sent;
                 
               

                
    //            $allowedEnquiryCount = $settingsObj->booking_request;
                if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
                    $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
                    if (isset($get_voucher->send_to_unpaid) && !empty($get_voucher->send_to_unpaid)) {
                        $allowedEnquiryCount = $get_voucher->send_to_unpaid;
                    } else {
                        $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
                        if (isset($get_voucher->id)) {
                            $userWallet = new \App\UserWallet();
                            $userWallet->user_id = $userId;
                            $userWallet->vouchaercredits_id = $get_voucher->id;
                            $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                            $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                            $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                            $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                            $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                            $userWallet->creadit_type = "voucher";
                            $userWallet->save();
                            $allowedEnquiryCount = $get_voucher->send_to_unpaid;
                        }
                    }
                } else {
                    $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first(); //wrong spelling here in condition
                   
                    if (isset($get_voucher->id)) {
                        $user_voucher->vouchercredit_id = $get_voucher->id;
                        $user_voucher->save();
                        $userWallet = new \App\UserWallet();
                        $userWallet->user_id = $userId;
                        $userWallet->vouchaercredits_id = $get_voucher->id;
                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                        $userWallet->creadit_type = "voucher";
                        $userWallet->save();
                        $allowedEnquiryCount = $get_voucher->send_to_unpaid;
                    }
                }
            }
            // getting number of nights for that booking from two dates are here
             
            $countEnquiry = $countEnquiry ;
         
            if ($userEnquiries) {
                //......THERE WERE ERROR IT WAS COMMENTED OUT WITH NO REASON..........
               // $countEnquiry = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
            }
           
            $no_credits = false;
          
            if ($countEnquiry < $allowedEnquiryCount) {
                $allowBooking = 1;
               
            }else{
                 $no_credits = true;
            }
         
            if ($allowBooking == 0) {
                if ($is_paid) {
                    if($no_credits){
                        return "noCreditsAvailable";
                    }else{
                         return "EnquiryErrorPaid";
                    }
                   
                } else {
                    if($no_credits){
                        return "noCreditsAvailable";
                    }else{
                         return "EnquiryError";
                    }
                   
                }
            } else {
    //            $productId = $request->get('listid');
    //            $propertyObj = $this->propertyRepository->getCurrentObject($productId);

                $renterId = $propertyObj->user->id;
                $noOfNyts = $request->get('days');

                //check if this is first choice of student.
                $booking_count = 0;
                $users_info = \App\User::with("user_booking", "school_info")->where("id", $userId)->first();
                if (isset($users_info->user_booking)) {
                    $booking_count = count($users_info->user_booking);
                }
                //$productPrice = $propertyObj->price_night;
                //$serviceFee = $request->get('commissionamount');
                //$totalPrice = ($propertyObj->price_night * $noOfNyts) + $serviceFee;
                //$noOfGuests = $request->get('guests');
                $msg = "";
                $worktutoremail = $propertyObj->represent_email;
                $organizationemail = $propertyObj->user->orgemail;
                $bookingno = "";
                $hr_email = $propertyObj->hr_represent_email;
                $check_hr = $propertyObj->human_resources;


                // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
                // $availableCheck = $userEnquiries->count();
                // echo "<br> avaialbe Check :".$availableCheck;
                // //$propertyObj, $enquiryObj, "no_hr", 
                // echo "HR Email :".$hr_email."<br>";
                // echo "<br> If chek hr :".$check_hr."<br>";
                // echo "Work Tutor Email :".$worktutoremail."<br> Org Email :".$organizationemail."<br>";
                // // if($propertyObj->user->orghr){
                // echo "OrgHr : ".$propertyObj->user->orghr."<br>";
                // echo "OrgHr Email : ".$propertyObj->user->orgemail."<br>";
                // echo "<br> Work :".$propertyObj->work_org_same."<br>";

                // }
                // echo "<pre>";
                //     print_r($availableCheck);
                //     print_r($propertyObj);
                // echo "<pre>"; this problem_ simply checking if there is email instead of flag checkbox_ that is already checked for blank value here I got both email of Work tutor and HR but I check for vlaidate is working or not and in which part run I mean send the mail.
                // exit;
                // }

                if (($check_hr == 0) && !empty($hr_email)) {
                    $hr_details = User::where('email', $hr_email)->first();
                    if (isset($hr_details->id) && !empty($hr_details->id)) {   //it checks if User Listing has defined another person, and email, as HR for the organisation hosting the student trainee
                        // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
                        //     // echo "<br> IF 1 exit";exit;
                        // }
                        $hr_dataArr = array(
                            'checkin' => $checkIn,
                            'checkout' => $checkOut,
                            'numofdates' => $noOfNyts,
                            'caltophone' => '',
                            'enquiry_timezone' => '',
                            'user_id' => \Auth::user()->id,
                            'renter_id' => $renterId,
                            'is_hr' => '1',
                            'prd_id' => $productId
                        );


                        $hr_booking_status = array(
                            'booking_status' => 'Pending'
                        );

                        $hr_dataArr = array_merge($hr_dataArr, $hr_booking_status);
                        $hr_enquiryObj = RentalsEnquiry::create($hr_dataArr);
                        $hr_insertid = $hr_enquiryObj->id;

                        if ($hr_enquiryObj->Bookingno == '' || $hr_enquiryObj->Bookingno == NULL) {
                            $val = 10 * $hr_insertid + 8;
                            $val = 1500000 + $val;
                            $bookingno = "WRL" . $val;
                            $hr_newdata = array(
                                'Bookingno' => $bookingno
                            );
                            RentalsEnquiry::where('id', $hr_insertid)->update($hr_newdata);
                        }
                        $dataArr = array(
                            'productId' => $productId,
                            'bookingNo' => $bookingno,
                            'enqid' => $hr_insertid,
                            'senderId' => \Auth::user()->id,
                            'receiverId' => $renterId,
                            'subject' => 'Booking Request : ' . $bookingno,
                            'message' => ""
                        );
                        MedMessage::create($dataArr);
                        $hr_enquiryObj = RentalsEnquiry::where("id", $hr_insertid)->first();
                        //checking if user have its first choice within selected dates or not
                        //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $hr_enquiryObj->prd_id, \Auth::user()->id, $hr_insertid);
    //                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                        //       where do we check .type paid or unpaid_ but why first allowcheck_ its first choice checking, of what type of listing_ for all listing ebcause before bed we have applied for all
                        // we have spoken about different types we need to know which to determine loginc, emailing etc.
                        // okay no problem after all these things are working i will make the logic for different types and send different emails
                        // sure, so for working just add if job then what was, else new logic_ yes. so for school house we don-t need second and third
                        // okay i will do that as first i have test that but let me do the title changing and by next working day i will make this logic for different emails
    //                    but it is only for school house sending, if not job, then email what we have created, s o I can send today *was due yesterday(
                        // there are already alot of logics going here so i dont want to change that so have to test it first so on live you dont get erros why sending
                        // thats why i have asked to change it when we can test it properly for school houses,
                        // I see it is workng for credits, only thing is email, dont worry we can do that together and test it properly :) now_ not now because it need time and i have to got after changing the title for school houses
                        // so cannot send to students to ask for school houses_ not today but will do that tomorrow for sure.
                        //Thanks
                        $availableCheck = $userEnquiries->count();
    //                     if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.80.168' || $_SERVER['REMOTE_ADDR'] == '93.36.90.227'){ 
    //                         echo "Available count check :".$availableCheck;
    // // exit;
    //                     }
                        $this->sendBookingEmails($availableCheck, $propertyObj, $hr_enquiryObj, "hr", $hr_email, $worktutoremail, $organizationemail);
    //                    if ($availableCheck == 0) {
    //                        //checking if user have its first choice
    //                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
    //                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $hr_enquiryObj);
    //                            //$this->mailNotificationsForUser->sendPaidHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
    //                        } else {
    //                            $this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
    //                        }
    //                    } else {
    //                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
    //                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $hr_enquiryObj);
    //                        } else {
    //                            $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $hr_enquiryObj);
    //                        }
    //                    }
                    } else {
                       
                        $dataArr = array(
                            'checkin' => $checkIn,
                            'checkout' => $checkOut,
                            'numofdates' => $noOfNyts,
                            'caltophone' => '',
                            'enquiry_timezone' => '',
                            'user_id' => \Auth::user()->id,
                            'renter_id' => $renterId,
                            'prd_id' => $productId
                        );


                        $booking_status = array(
                            'booking_status' => 'Pending'
                        );

                        $dataArr = array_merge($dataArr, $booking_status);
                        $enquiryObj = RentalsEnquiry::create($dataArr);
                        $insertid = $enquiryObj->id;

                        if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                            $val = 10 * $insertid + 8;
                            $val = 1500000 + $val;
                            $bookingno = "WRL" . $val;
                            $newdata = array(
                                'Bookingno' => $bookingno
                            );
                            RentalsEnquiry::where('id', $insertid)->update($newdata);
                        }

                        $dataArr = array(
                            'productId' => $productId,
                            'bookingNo' => $bookingno,
                            'enqid' => $insertid,
                            'senderId' => \Auth::user()->id,
                            'receiverId' => $renterId,
                            'subject' => 'Booking Request : ' . $bookingno,
                            'message' => ""
                        );
                        MedMessage::create($dataArr);
                        $enquiryObj = RentalsEnquiry::where("id", $insertid)->first();
                        //checking if user have its first choice within selected dates or not
                        //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
    //                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                        $availableCheck = $userEnquiries->count();
                        $this->sendBookingEmails($availableCheck, $propertyObj, $enquiryObj, "no_hr", $hr_email, $worktutoremail, $organizationemail);
    //                    if ($availableCheck == 0) {
    //                        if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
    //                            if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
    //                                $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
    //                                //$this->mailNotificationsForUser->sendPaidBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
    //                            } else {
    //                                $this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
    //                            }
    //                        } else {
    //                            if (isset($propertyObj->renter_id)) {
    //                                $renter_user = User::where("id", $propertyObj->renter_id)->select("id", "email", "name")->first();
    //                                if (isset($renter_user->email) && !empty($renter_user->email)) {
    //                                    if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
    //                                        $this->mailNotificationsForUser->sendPaidUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
    //                                    } else {
    //                                        $this->mailNotificationsForUser->sendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
    //                                    }
    //                                }
    //                            }
    //                        }
    //                    } else {
    //                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
    //                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
    //                            //$this->mailNotificationsForUser->sendPaidBookingNotification($propertyObj, $enquiryObj);
    //                        } else {
    //                            $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj);
    //                        }
    //                    }
                    }
                } else {
                    // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
                    //     // echo "<br> Else part exit;";exit;
                    // }
                    $dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => \Auth::user()->id,
                        'renter_id' => $renterId,
                        'prd_id' => $productId
                    );


                    $booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $dataArr = array_merge($dataArr, $booking_status);
                    $enquiryObj = RentalsEnquiry::create($dataArr);
                    $insertid = $enquiryObj->id;

                    if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                        $val = 10 * $insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $newdata = array(
                            'Bookingno' => $bookingno
                        );
                        RentalsEnquiry::where('id', $insertid)->update($newdata);
                    }

                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $insertid,
                        'senderId' => \Auth::user()->id,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    MedMessage::create($dataArr);
                    $enquiryObj = RentalsEnquiry::where("id", $insertid)->first();
                    //checking if user have its first choice within selected dates or not
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
    //                $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    $availableCheck = $userEnquiries->count();
                    $this->sendBookingEmails($availableCheck, $propertyObj, $enquiryObj, "no_hr", $hr_email, $worktutoremail, $organizationemail);
    //                if ($availableCheck == 0) {
    //                    if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
    //                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
    //                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
    //                            //this->mailNotificationsForUser->sendPaidBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
    //                        } else {
    //                            $this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
    //                        }
    //                    } else {
    //                        if (isset($propertyObj->renter_id)) {
    //                            $renter_user = User::where("id", $propertyObj->renter_id)->select("id", "email", "name")->first();
    //                            if (isset($renter_user->email) && !empty($renter_user->email)) {
    //                                if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
    //                                    $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
    //                                    //$this->mailNotificationsForUser->sendPaidUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
    //                                } else {
    //                                    $this->mailNotificationsForUser->sendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
    //                                }
    //                            }
    //                        }
    //                    }
    //                } else {
    //                    if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
    //                        $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
    //                        //$this->mailNotificationsForUser->sendPaidBookingNotification($propertyObj, $enquiryObj);
    //                    } else {
    //                        $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj);
    //                    }
    //                }
                }
                 if($noOfNyts==0){
                        $noOfNyts = 1;
                    }

                $userCredit = new \App\UserCredit();
                $userCredit->user_id = $userId;
                $userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
                $userCredit->property_id = isset($propertyObj->id) ? $propertyObj->id : 0;
                if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {

                    $userCredit->send_to_schoolhouse = 1;
                } else {
                    $userCredit->send_to_unpaid = 1;
                }
                $userCredit->save();
                return $bookingno;
            }
        }

        public function showBookingDetails($bookingNo) {
            $enquiryObj = RentalsEnquiry::where('Bookingno', $bookingNo)->first();
            $propertyObj = $this->propertyRepository->getByIdActive($enquiryObj->prd_id);
            return View('bookingresultview', compact('enquiryObj', 'propertyObj'));
        }

        public function payPendingForPaid($bookingNo) {			
			$enquiryObj = RentalsEnquiry::where('Bookingno', $bookingNo)->first();
			
			$check_in   = $enquiryObj->checkin;
			$check_out  = $enquiryObj->checkout;
			$sent_date  = $enquiryObj->dateAdded;
			$sender_id  = $enquiryObj->user_id;
			$sender_user = User::where('id', $sender_id)->first();

			//requested property object
			$propertyDetails = Property::where('id',$enquiryObj->prd_id)->first();
				
			$checkInDate = explode(" ",$enquiryObj->checkin);
			$checkOutDate = explode(" ",$enquiryObj->checkout);
		  
			if(count($checkInDate) > 1){
				$newDateStart = $enquiryObj->checkin;
			}else{
				$newDateStart = date("Y-m-d H:i:s", strtotime($enquiryObj->checkin." 11:00:00"));
			}

			if(count($checkOutDate) > 1){
				$newDateEnd = $enquiryObj->checkout;
			}else{
				$newDateEnd = date("Y-m-d H:i:s", strtotime($enquiryObj->checkout." 10:00:00"));
			}
			// if booking status is awaiting-payment
			if($enquiryObj->approval == 'Awaiting Payment'){
				/*** Third Step if user has credits or not ***/
				$book_paid_used    = 0;
				$allowed_book_paid = 0;
				
				if(isset($sender_user->vouchercredit_id) && !empty($sender_user->vouchercredit_id)) {
					$sender_wallet = \App\UserWallet::where("vouchaercredits_id", $sender_user->vouchercredit_id)->where("user_id", $sender_id)->first();
				}else{
					$sender_wallet = \App\UserWallet::where("user_id", $sender_id)->first();
				}
				
				$total_amount = 0;
				
				if (isset($sender_wallet->amount)) {
					$total_amount = $sender_wallet->amount;
				}
				if (isset($sender_wallet->book_paid)) {
					
					//total allowed book_paid
					$allowed_book_paid = $sender_wallet->book_paid;
					
					//getting used book_paid till now
					$get_credits_bookpaid_used = \App\UserCredit::where("user_id", $sender_id)->where("book_paid", ">", "0")->get();

					//calculating total used book_paid here
					foreach ($get_credits_bookpaid_used as $key => $book_paid) {
						$book_paid_used += $book_paid->book_paid;
					}
				  
					
				}                
				$remaining_book_paid = $allowed_book_paid - $book_paid_used;
				$Checkin_date = strtotime($enquiryObj->checkin);
				$Checkout_date = strtotime($enquiryObj->checkout);
				$dates_difference = $Checkout_date - $Checkin_date;
				$enquiryObj->numofdates  = round($dates_difference / (60*60*24));
				
				$total_required_book_paid_for_this_booking = $enquiryObj->numofdates;
				$book_paid_used += $total_required_book_paid_for_this_booking;
				//echo 'book_paid_used = '.$book_paid_used.'<br>';
				//echo 'allowed_book_paid = '.$allowed_book_paid.'<br>';die;
				if ($allowed_book_paid < $book_paid_used) {
					$property_id = $enquiryObj->property->id;  
					$booking_number = $enquiryObj->Bookingno;  
					$fetchProperty = \App\Property::where('id',$property_id)->first();
					if($fetchProperty->property_type == 26 ){
						$bed_guest_room_id = $fetchProperty->bed_guest_room_id;
						$bed_type_id = $fetchProperty->bed_type_id;
						$guest_room = \App\UserPaidGuestRoom::where('id',$bed_guest_room_id)->first();
						$room_type_id = $guest_room->room_type_id;
						$room_type = \App\RealRoomTypes::where("is_active", "1")->where("id", $room_type_id)->first();
						
						$bed_type = \App\BedType::where(["is_active" => "1", "id" => $bed_type_id])->first();
						
						
						$room_name = $room_type->room_type.' Room ';
						$bed_name = $bed_type->bed_type.' Bed';
						$service_title = $room_name.' '.$bed_name;	
					}else{
						$service_title = $fetchProperty->exptitle;
					}
					
					$pricing = \App\PropertyPricing::where("property_id", $property_id)->first();
					
					$currency_id = $pricing->currency_id;
					$currency    = \App\Currency::where(["id" => $currency_id])->first();
					if(isset($currency->currency_symbol) and $currency->currency_symbol != ""){
						$currency_symbol = $currency->currency_symbol;
					}else{
						$currency_symbol = $currency->currency_code;
					}
					
					if($pricing->payment_type == 'weekly'){
						
						$number = ($pricing->amount / 7);
						$per_day_price = number_format((float)$number, 2, '.', '');
					}else{
						$number = $pricing->amount;
						$per_day_price = number_format((float)$number, 2, '.', '');
					}
					$to_be_paid = (($total_required_book_paid_for_this_booking - $remaining_book_paid)*$per_day_price) - $total_amount;
					$booking_request = (object)array(
									'booking_number'=>$booking_number,
									'service_title'=>$service_title,
									'checkin_date'=>$newDateStart,
									'checkout_date'=>$newDateEnd,
									'sent_date'=>$newDateEnd,
									'your_credits'=>$remaining_book_paid,
									'required_credits'=>$total_required_book_paid_for_this_booking,
									'per_day_price'=>$per_day_price,
									'currency_symbol'=>$currency_symbol,
									'to_be_paid'=>$to_be_paid
									);
					return View('stripepayment', compact('booking_number', 'booking_request'));					
					
					
				}
			}
		}
        public function viewMultipleRequestBookings($bookingNo) {
			$enquiryObj = RentalsEnquiry::where('Bookingno', $bookingNo)->first();
			
			$check_in   = $enquiryObj->checkin;
			$check_out  = $enquiryObj->checkout;
			$sender_id  = $enquiryObj->user_id;
			
			$all_requests = RentalsEnquiry::with('property')->where('user_id', $sender_id)->where('checkin',$check_in)->where('checkout',$check_out)->get();
			
			$propertyObj = $this->propertyRepository->getByIdActive($enquiryObj->prd_id);
           
			return View('viewmultiplebookings', compact('enquiryObj', 'propertyObj','all_requests'));
		}
        public function viewRequestBookingResult($bookingNo) {
            $enquiryObj = RentalsEnquiry::where('Bookingno', $bookingNo)->first();
            $propertyObj = $this->propertyRepository->getByIdActive($enquiryObj->prd_id);
            return View('bookingresult', compact('enquiryObj', 'propertyObj'));
        }

        public function updatebookingstatus1($enquiryid) {
            $rentalObj = RentalsEnquiry::find($enquiryid);
            return View('agreement', compact('rentalObj'));
        }

        public function updatebookingstatus(Request $request) {

            //Pending, Decline, Accept
            $status = $request->get('status');
            $enquiryid = $request->get('enquiryid');
            $rentalObj = RentalsEnquiry::find($enquiryid);
         
            $msg = $status . " Successfully updated";

            if ($status == 'Accept') {
                //getting and calculating user booked paid and unpaid credits;
                $userId = \Auth::user()->id;
           
                if(count(explode(" ", $rentalObj->checkin)) > 1){
                    $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin));
                }else{
                    $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin." 11:00:00"));
                }

                if(count(explode(" ", $rentalObj->checkout)) > 1){
                    $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout));
                }else{
                    $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout." 10:00:00"));
                }
               
        
                $availableCheck = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $rentalObj->prd_id);

                //return $availableCheck;
                if (count($availableCheck) > 0) {                   
                       return "Fail";                   
                }
                
                $user_voucher = User::where('id', $rentalObj->user_id)->first();
                if(isset($user_voucher->vouchercredit_id) && !empty($user_voucher->vouchercredit_id)) {
                    $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
                }
                $countEnquiry = 0;
                $allowedEnquiryCount = 0;
                if (isset($rentalObj->property->property_status) && $rentalObj->property->property_status == "paid") {
                    $user_wallet = \App\UserWallet::where("user_id", $rentalObj->user_id)->first();
                    if (isset($user_wallet->book_paid)) {
                        $allowedEnquiryCount = $user_wallet->book_paid;
                        $user_credits = \App\UserCredit::where("user_id", $rentalObj->user_id)->where("book_paid", ">", "0")->get();
                        if (sizeof($user_credits) > 0) {
                            $countEnquiry = $user_credits->count();
                        }
                    }

                } else {
                    $user_wallet = \App\UserWallet::where("user_id", $rentalObj->user_id)->first();
                    if (isset($user_wallet->book_unpaid)) {
                        $allowedEnquiryCount = $user_wallet->book_unpaid;
                        $user_credits = \App\UserCredit::where("user_id", $rentalObj->user_id)->where("book_unpaid", ">", "0")->get();
                        if (sizeof($user_credits) > 0) {
                            $countEnquiry = $user_credits->count();
                        }
                    }


                    $userAllreadyAccpeted = RentalsEnquiry::userAllreadyAccpetedForUnpaid($newDateStart, $newDateEnd, $rentalObj->user_id); 
                 
                    if($userAllreadyAccpeted){
                       
                        return 'already_acceptd';

                    }

                    if($allowedEnquiryCount <= $countEnquiry) {
                        return "noCredits";
                    }

                    RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
                    $propertyId = $rentalObj->prd_id;
                    $arrival = $rentalObj->checkin;
                    $depature = $rentalObj->checkout;
                    $dates = $this->getDatesFromRange($arrival, $depature);
                    
                    // if($_SERVER['REMOTE_ADDR'] != '93.32.72.112'){
                        $this->saveBookedDates($dates, $propertyId);
                        $this->updateScheduleCalendar($propertyId, 0);
                    // }
                

                    $dataArr = array(
                        'productId' => $rentalObj->prd_id,
                        'bookingNo' => $rentalObj->Bookingno,
                        'enqid' => $enquiryid,
                        'senderId' => \Auth::user()->id,
                        'receiverId' => $rentalObj->user_id,
                        'subject' => 'Booking Request : ' . $status,
                        'message' => "You Request Approved"
                    );
                    MedMessage::create($dataArr);
                    if ($status == 'Accept') {
                        /* Send Learning Agreement */
                        $cmsObj = $this->cmsRepository->getById(9);
                        $stipulate = $this->cmsRepository->getById(12);
                        $article1 = $this->cmsRepository->getById(13);
                        $article2 = $this->cmsRepository->getById(14);
                        $article3 = $this->cmsRepository->getById(15);
                        $article4 = $this->cmsRepository->getById(16);
                        $article5 = $this->cmsRepository->getById(17);
                        $article6 = $this->cmsRepository->getById(18);
                        $article7 = $this->cmsRepository->getById(19);
                        $educationalobjectives = $this->cmsRepository->getById(20);
                        $article9 = $this->cmsRepository->getById(21);
                        $article10 = $this->cmsRepository->getById(22);

                        $rentalObj = RentalsEnquiry::find($enquiryid);

                        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                        $file_name = $timestamp . '-' . $rentalObj->Bookingno . ".pdf";

                        $pdf = PDF::loadView('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'))->save(public_path() . "/pdf/agreement/" . $file_name);
                        //return $file_name ;

                         //start here... 
                        /*fetching country id of student school and hosting organisation to make file PDFs choice dynamic START HERE*/

                        $propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
                        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                        $file_name = $timestamp . '-';
                        $schoolCountryId = $organizationCountryId = '';
                        if($rentalObj->user->school->ocountry != ''){
                            $schoolCountryId = $rentalObj->user->school->ocountry;
                        }else if($rentalObj->user->school->lcountry != ''){
                            $schoolCountryId = $rentalObj->user->school->lcountry;
                        }
                        $users = \App\User::where(['id' => $userId])->get();
                    

                        if ($rentalObj->property->addr_work_org_same == 0){
                            $organizationCountryId = $rentalObj->property->wcountry;
                        }else{
                            $organizationCountryId = $rentalObj->host->countryr18;
                        }
                        // echo "Country Organization origi :".$organizationCountryId."<br>";


                        if($schoolCountryId == 'Italia') {
                            $schoolCountryId = 'Italy';                
                        }
                        if($organizationCountryId == 'Italia'){
                            $organizationCountryId = 'Italy';
                        }
                        
                        if($schoolCountryId == 'Irlanda'){
                            $schoolCountryId = 'Ireland';                
                        }
                        if($organizationCountryId == 'Irlanda'){
                            $organizationCountryId = 'Ireland';
                        }

                        if($schoolCountryId == 'Regno Unito'){
                            $schoolCountryId = 'United Kingdom';                
                        }
                        if($organizationCountryId == 'Regno Unito' ){
                           $organizationCountryId = 'United Kingdom';
                        }
                         // echo "Country Organization :".$organizationCountryId."<br>";
                         // exit;

                        if(!empty($schoolCountryId)){
                            if(is_numeric($schoolCountryId)){
                                $schoolCountryData = \App\Country::where('id',$schoolCountryId)->get();
                            }else{
                                $schoolCountryData = \App\Country::where('name',$schoolCountryId)->get();
                            }
                            $schoolCountryCode = $schoolCountryData[0]->code;
                        }
                        // echo "<br> IDs :".$organizationCountryId."<br>";
                        // exit;
                        if(!empty($organizationCountryId)){
                            if(is_numeric($organizationCountryId)){
                                $organizationCountryData = \App\Country::where('id',$organizationCountryId)->get();
                            }else{
                                $organizationCountryData = \App\Country::where('name',$organizationCountryId)->get();
                            }
                            // var_dump(empty($organizationCountryData));
                            // echo "<br> Count:".count($organizationCountryData)."<br>";
                            // echo "<pre>";
                            //     print_r($organizationCountryData);
                            // echo "<pre>";

                            // exit;
                            if(count($organizationCountryData) > 0){
                                $organizationCountryCode = $organizationCountryData[0]->code;                    
                            }else{
                                $organizationCountryCode = "";
                            }
                        }

                  
                        $inCountry = array("Italy","United Kingdom");
                        if((!empty($schoolCountryCode) && !empty($organizationCountryCode)) && in_array($organizationCountryId,$inCountry) && in_array($schoolCountryId,$inCountry) ){
                            $file_name .= 'Pdf'.$schoolCountryCode.'goingto'.$organizationCountryCode;
                        }else{
                            // send this request to "esperienzainglese@gmail.com"
                            $file_name .= "pdfITgoingtoUK";
                            $senderEmailId = $users[0]->email;
                            $receiverEmailId = $rentalObj->property->user->email;
                            $schoolCountry = $schoolCountryId;
                            $organizationCountry = $organizationCountryId;
                            // echo "<br> SenderEmail :".$senderEmailId."<br> Receiver Email Id :".$receiverEmailId."<br> School Country : ".$schoolCountry."<br> Organization Country :".$organizationCountry."<br> CheckIn :".date("Y-m-d",strtotime($checkIn))."<br> Checkout :".date("Y-m-d",strtotime($checkOut))."<br>";
                            $checkIn = $arrival;
                            $checkOut = $depature;
                            $data = array('senderEmailId' => $senderEmailId, 'receiverEmailId' => $receiverEmailId, 'schoolCountry' => $schoolCountry, 'organizationCountry' => $organizationCountry, 'checkIn' =>date("Y-m-d",strtotime($checkIn)), 'checkOut' => date("Y-m-d",strtotime($checkOut)));

                            /*\Mail::send('emails.request', $data, function($message){
                                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                                $message->to("esperienzainglese@gmail.com","")
                                        // ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                                        ->subject('Work-Related-Learning.com - you have reveiced a message regarding a request');
                                        // exit;
                            });*/
                        }


                        $file_name .= "-".$rentalObj->Bookingno;
                        $file_name .= ".pdf";
                        // echo $file_name."<br>";
                        // exit;
                        // echo "<prE>";
                            // print_R($enquiryObj->school_name);
                        // \App\User::where("id")
                        $schoolName = "";
                        if(!empty($users[0]->school_name)){
                            $schoolInfo = \App\School::where('id', $users[0]->school_name)->first();
                            //     echo "<BR> USER DETAILS </br>";
                            //     print_R($users[0]->school_name);
                            //     print_R($schoolInfo);
                            //     // print_R($users);
                            // echo "</prE>";
                            // if(count($schoolInfo) > 0){
                            if(isset($schoolInfo->id)){
                                $schoolName = $schoolInfo->name;
                            }                
                        }
                        // exit;
                        /*fetching country id of student school and hosting organisation to make file PDFs choice dynamic END HERE*/
                        $enquiryObj = RentalsEnquiry::where('Bookingno', $rentalObj->Bookingno)->first();
                        $file_name1 = $timestamp . '-' . $rentalObj->Bookingno . "_register.pdf"; //adding filename to Register
                        $file_name2 = $timestamp . '-' . $rentalObj->Bookingno . "_evaluation.pdf"; //adding filename to Register
                        $file_name3 = $timestamp . '-' . $rentalObj->Bookingno . "_parental_permission.pdf"; //adding filename to Register
                        if ($propertyObj->property_status != "paid") {
                            // $pdf3 = PDF::loadView('parental_permissionpdf', compact('enquiryObj'))->save(public_path() . "/pdf/parental_permission/" . $file_name3);
                            //  echo "File Name :".$file_name3."<br>";exit;
                            $pdf = PDF::loadView('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'))->save(public_path() . "/pdf/agreement/" . $file_name);                
                            $pdf1 = PDF::loadView('Activity_Register_WRL_Registro_Presenze_English', compact('users','rentalObj'))->save(public_path() . "/pdf/register/" . $file_name1); //adding pdf genration of Register
                            $pdf2 = PDF::loadView('evoluationpdf_new', compact('users','enquiryObj','schoolName'))->save(public_path() . "/pdf/evaluation/" . $file_name2); //adding pdf genration of Evaluation
                            $pdf3 = PDF::loadView('parental_permissionpdf', compact('enquiryObj'))->save(public_path() . "/pdf/parental_permission/" . $file_name3); //adding pdf genration of Evaluation
            //                dd($educationalobjectives);
                           // return view('Activity_Register_WRL_Registro_Presenze_English', compact('users','rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'));
                        }                
                           //end here... 

                        $tutorEmail = $rentalObj->user->school->tutor_email;

                        if (isset($rentalObj->prd_id) && !empty($rentalObj->prd_id)) {
                            $tutor_detail = Property::find($rentalObj->prd_id);
                            if (isset($tutor_detail->represent_email) && !empty($tutor_detail->represent_email)) { //here we checked if flagged then fetch from flagged email else from abve mentioned . So no working here_ You have seen it is not flagged, so we should not send to Work Tutor email but only to USer Lsiting email, agree_ so if not flagged then only send to User Listing (which currently set in CC) correct? yes, but I do not know if that email is for Work Tutor, it does not mean change taht email, it means send email of confirmation to User Listing, you might want to chwck that there is no other emial for User Listing_ I am asking because it seems we are not clear with system as We have put flags for one reason, to send to User isting or orther cases, are the cases clear to you_ yes cases are clear but the confusion is in existing sysetm that whay I am not able to understand the current flow for email sending and thats why I need your help for this module. Sure. So if you like we can change that file, if you have not checked or do not know how to we can first test with Work Tutor flagged, that is the whole point, I do not see where we check if flagged or not. we only check for set value into variable . So obviously cannot work that way. now we want flagh value too and then pass into function to make email id set into "TO" as well as for CC . Could you show me again email we are speaing about in script. wait 


                                $tutorEmail = $tutor_detail->represent_email;
                            }
                        }

                        $parentEmail = null;
                        if (is_null($rentalObj->user->student_relationship)) {
                            $studentEmail = $rentalObj->user->email;
                        } else {
                            $parentEmail = $rentalObj->user->email;
                            $studentEmail = $rentalObj->user->semail16;
                        }

                        // get HR details
                        $hr_enquiry = RentalsEnquiry::where(["id" => $enquiryid, 'is_hr' => '1'])->first();
                        $hr_email = "";
                        $hr_email_detail = "";

                        
                        if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
                            $hr_user = User::find($hr_enquiry->renter_id);
                            $hr_email = isset($hr_user->email) ? $hr_user->email : '';

                            if (isset($hr_user->prd_id) && !empty($hr_user->prd_id)) {
                                $hr_email_detail = Property::find($hr_user->prd_id);
                            }
                        }


                        $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
                        if($propertyDetails->human_resources == 0 && $propertyDetails->hr_represent_email != ''){
                            $hr_email = $propertyDetails->hr_represent_email;
                        }

                        $isHrFlagged = 1;
                        if($propertyDetails->human_resources == 0){
                            $isHrFlagged = 0;
                        }

                        $isWorkTutorFlagged = 1;
                        if($propertyDetails->work_org_same == 0){
                            $isWorkTutorFlagged = 0;
                        }



                        /*update wallet on accept bed type start here */

                        $acceptedUserId =  \Auth::user()->id;
                        $userInfo = User::with('group_info')->where('id', $rentalObj->user_id)->first();
                        $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
                       
                        $is_work_org_same = 0;
                        $is_hr_same = 0;
                        if($propertyDetails->property_status == 'unpaid' || $propertyDetails->property_status == 'unpaidflash'){
                                // print_r($rentalObj);
                            $fetchWalletData = \App\UserWallet::where('user_id',$rentalObj->user_id)->first();

                            

                            $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 3 ) : "3";
                            // $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 1 ) : "1";
                            $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 0 ) : "0";

                            \App\UserWallet::where('user_id',$rentalObj->user_id)->update(array('send_to_schoolhouse'=>$send_to_schoolhouse,'book_paid'=>$book_paid,'creadit_type'=>''));

                            $fetchWalletData = \App\UserWallet::where('user_id',$rentalObj->user_id)->first();

                           



                            $parentEmail = $rentalObj->user->email; //here is the parent id 
                            $studentEmail = $rentalObj->user->semail16;
                            
                            //$this->mailNotificationsForUser->sendemailPaidToStudentAboutWalletUpdateForSchoolHouseAfterJobAccept($parentEmail, $studentEmail, $rentalObj,$userInfo);
                            
                        }


                        $files = array("agreement" => $file_name,"evaluation"=>$file_name2,"register"=>$file_name1,"parental_permission"=>$file_name3);
                        // $this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $file_name, $rentalObj, $hr_email, $hr_email_detail,$file_name1);
                        if($isWorkTutorFlagged == 0 && $isHrFlagged == 1){ //if Work Tutor Flagged
                            $this->mailNotificationsForUser->sendPDFagreementWithFlaggedToWorkTutor($tutorEmail, $parentEmail, $studentEmail, $files, $rentalObj, $hr_email, $hr_email_detail);
                        }else{ // If both are non flagged.

                            $this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $files, $rentalObj, $hr_email, $hr_email_detail);
                        }
                        //$this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $file_name, $rentalObj, $hr_email, $hr_email_detail);

                        //$pdf = \PDF::loadView('agreement', compact('rentalObj', 'cmsObj'));
                        //return $pdf->download('invoice.pdf');
                        //return View('agreement', compact('rentalObj', 'cmsObj'));

                        if ($rentalObj->is_hr == "1") {
                            $propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
                            $worktutoremail = $propertyObj->represent_email;
                            if (!empty($worktutoremail)) {
                                //$worktutoremail = $rentalObj->user->email;
                                $organizationemail = $propertyObj->user->orgemail;
                                $this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj,$rentalObj);
                            }
                            //echo $worktutoremail; exit;
                            //exit;
                            $checkIn = $rentalObj->checkin;
                            $checkOut = $rentalObj->checkout;
                            $noOfNyts = $rentalObj->numofdates;
                            $renterId = $propertyObj->user->id;
                            $dataArr = array(
                                'checkin' => $checkIn,
                                'checkout' => $checkOut,
                                'numofdates' => $noOfNyts,
                                'caltophone' => '',
                                'enquiry_timezone' => '',
                                'user_id' => $rentalObj->user_id,
                                'renter_id' => $renterId,
                                'prd_id' => $rentalObj->prd_id
                            );


                            $booking_status = array(
                                'booking_status' => 'Pending'
                            );

                            $dataArr = array_merge($dataArr, $booking_status);
                            $enquiryObj = RentalsEnquiry::create($dataArr);
                            $insertid = $enquiryObj->id;

                            if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                                $val = 10 * $insertid + 8;
                                $val = 1500000 + $val;
                                $bookingno = "WRL" . $val;
                                $newdata = array(
                                    'Bookingno' => $bookingno
                                );
                                RentalsEnquiry::where('id', $insertid)->update($newdata);
                            }

                            $dataArr = array(
                                'productId' => $rentalObj->prd_id,
                                'bookingNo' => $bookingno,
                                'enqid' => $insertid,
                                'senderId' => $rentalObj->user_id,
                                'receiverId' => $renterId,
                                'subject' => 'Booking Request : ' . $bookingno,
                                'message' => ""
                            );
                            MedMessage::create($dataArr);
                        }
                    }
                    $userCredit = new \App\UserCredit();
                    $userCredit->user_id = $rentalObj->user_id;
                    $userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
                    $userCredit->property_id = isset($rentalObj->property->id) ? $rentalObj->property->id : 0;
                    if (isset($rentalObj->property->property_status) && $rentalObj->property->property_status == "paid") {
                            $userCredit->book_paid = 1;                    
                    } else {
                        $userCredit->book_unpaid = 1;
                    }
                    $userCredit->save();
                    flash($msg, 'success');
                    return "success";
                }
            }
            if ($status == 'Decline') {
                $userId = $rentalObj->user_id;
                $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();
                 $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
                RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
                $this->mailNotificationsForUser->sendDeclineEmail($rentalObj,$propertyDetails->exptitle);

                $send_to_unpaid = isset($fetchWalletData->send_to_unpaid) ? ($fetchWalletData->send_to_unpaid + 1 ) : "1";
                \App\UserWallet::where('user_id',$userId)->update(array('send_to_unpaid'=>$send_to_unpaid,'creadit_type'=>''));
                
                $dataArr = array(
                    'productId' => $rentalObj->prd_id,
                    'bookingNo' => $rentalObj->Bookingno,
                    'enqid' => $enquiryid,
                    'senderId' => \Auth::user()->id,
                    'receiverId' => $rentalObj->user_id,
                    'subject' => 'Booking Request : ' . $status,
                    'message' => "You Request Declined"
                );
                MedMessage::create($dataArr);
                flash($msg, 'success');
                return "success";
            }
        }
		
		
		// New Function with cleaned code for accept and decline click from your schoolhouse reservations page 
        public function updateSchoolHousebookingstatusDev(Request $request) {
			//Pending, Decline, Accept
            $status = $request->get('status');
            $enquiryid = $request->get('enquiryid');
            $rentalObj = RentalsEnquiry::find($enquiryid);
            $message = "";
            $msg = $status . " Successfully updated";
			
			// If clicks on Accept button from user school house requests page 
            if ($status == 'Accept') {
				// getting sender user_id from request object
				$sender_id = $rentalObj->user_id;
				// Owner of listing user_id
				$owner_id  =  \Auth::user()->id;
				
				//sender user object
                $sender_user = User::where('id', $sender_id)->first();
				
                // Owner user object
                $acceptedUser = User::with('group_info')->where('id', $owner_id)->first();
				
				//requested property object
                $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
				
				$checkInDate = explode(" ",$rentalObj->checkin);
                $checkOutDate = explode(" ",$rentalObj->checkout);
              
                if(count($checkInDate) > 1){
                    $newDateStart = $rentalObj->checkin;
                }else{
                    $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin." 11:00:00"));
                }

                if(count($checkOutDate) > 1){
                    $newDateEnd = $rentalObj->checkout;
                }else{
                    $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout." 10:00:00"));
                }
				
				
				/***** First Step checking if requested user already accepted somewhere else and entertaining in  these dates ***/
                if(isset($propertyDetails->property_type) and $propertyDetails->property_type=='26'){
					$userAllreadyAccpeted = RentalsEnquiry::userAllreadyAccpetedForPaid($newDateStart, $newDateEnd, $sender_id); 
				   
					if($userAllreadyAccpeted){
						// return message
						$message = "We are sorry, the Student Guest has already been accepted elsewhere.";
						return redirect()->back()->with("error", $message);

					}
				}
				
				
				/*** Second Step checking if this listing is available for booking or not ***/
				$booking_on_these_dates = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $rentalObj->prd_id);
				
                if (count($booking_on_these_dates) > 0) {
					//if this listing already booked on these dates then return with error message
                    $message = "NOTE: you have already accepted other requests for these dates, you may not accept more than one student of same nationality, this may be arranged only thorugh Admin, Thanks!";
                    return redirect()->back()->with("message", $message);
                } else {
					// if available for booking 
					/*** Third Step if user has credits or not ***/
					$book_paid_used    = 0;
					$allowed_book_paid = 0;
					
					if(isset($sender_user->vouchercredit_id) && !empty($sender_user->vouchercredit_id)) {
						$sender_wallet = \App\UserWallet::where("vouchaercredits_id", $sender_user->vouchercredit_id)->where("user_id", $sender_id)->first();
					}else{
						$sender_wallet = \App\UserWallet::where("user_id", $sender_id)->first();
					}
					$wallet_amount = 0;
					if (isset($sender_wallet->amount)) {
						$wallet_amount = $sender_wallet->amount;
					}
					
					if (isset($sender_wallet->book_paid)) {
						//total allowed book_paid
						$allowed_book_paid = $sender_wallet->book_paid;
						
						//getting used book_paid till now
						$get_credits_bookpaid_used = \App\UserCredit::where("user_id", $rentalObj->user_id)->where("book_paid", ">", "0")->get();

						//calculating total used book_paid here
						foreach ($get_credits_bookpaid_used as $key => $book_paid) {
							$book_paid_used += $book_paid->book_paid;
						}
					  
						
					}                

					$Checkin_date = strtotime($rentalObj->checkin);
					$Checkout_date = strtotime($rentalObj->checkout);
					$dates_difference = $Checkout_date - $Checkin_date;
					$rentalObj->numofdates  = round($dates_difference / (60*60*24));
					
					$total_required_book_paid_for_this_booking = $rentalObj->numofdates;
					$book_paid_used += $total_required_book_paid_for_this_booking;
					//echo 'book_paid_used = '.$book_paid_used.'<br>';
					//echo 'allowed_book_paid = '.$allowed_book_paid.'<br>';die;
					if ($allowed_book_paid < $book_paid_used) {
						//Remaining credits for book_paid 
						$remaining_book_paid = ($allowed_book_paid - ($book_paid_used - $total_required_book_paid_for_this_booking));
						
						// will send payment link here and changed status to awaiting payment 
						//updating status to awaiting-payment
						RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => 'Awaiting Payment','booking_status' => 'awaiting-payment'));
						$parentEmail = null;
						if (is_null($rentalObj->user->student_relationship)) {
							$studentEmail = $rentalObj->user->email;
						} else {
							$parentEmail = $rentalObj->user->email;
							$studentEmail = $rentalObj->user->semail16;
						}  
						// Sending payment email here 
						$this->mailNotificationsForUser->sendemailPendingPaymentForPaid($parentEmail, $studentEmail, $rentalObj,$remaining_book_paid,$wallet_amount);
						
						$message = "When student pays you will get email confirmation with student details, please allow one week for this.";
						return redirect()->back()->with("error", $message);
					}else{
						//All Set for booking accepted
						
						//updating status to accepted
						RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
						
						$propertyId = $rentalObj->prd_id;
						$arrival = $rentalObj->checkin;
						$depature = $rentalObj->checkout;
						$dates = $this->getDatesFromRange($arrival, $depature);
						//save booking dates for calendar not available in  search results 
						$this->saveBookedDates($dates, $propertyId);
						$this->updateScheduleCalendar($propertyId, 0);
						//NOTE=== here i think we should disabled these dates for user first listing too ==== 
						// these are my thoughts because we should disable and will not come in search results then
						
						
						// creating accepted status message 
						$dataArr = array(
							'productId' => $rentalObj->prd_id,
							'bookingNo' => $rentalObj->Bookingno,
							'enqid' => $enquiryid,
							'senderId' => \Auth::user()->id,
							'receiverId' => $rentalObj->user_id,
							'subject' => 'Booking Request : ' . $status,
							'message' => "You Request Approved"
						);						
						MedMessage::create($dataArr);
						
						
						/* Send Learning Agreement */
						$cmsObj = $this->cmsRepository->getById(9);
						$stipulate = $this->cmsRepository->getById(12);
						$article1 = $this->cmsRepository->getById(13);
						$article2 = $this->cmsRepository->getById(14);
						$article3 = $this->cmsRepository->getById(15);
						$article4 = $this->cmsRepository->getById(16);
						$article5 = $this->cmsRepository->getById(17);
						$article6 = $this->cmsRepository->getById(18);
						$article7 = $this->cmsRepository->getById(19);
						$educationalobjectives = $this->cmsRepository->getById(20);
						$article9 = $this->cmsRepository->getById(21);
						$article10 = $this->cmsRepository->getById(22);

						$rentalObj = RentalsEnquiry::find($enquiryid);

						$timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
						$file_name = $timestamp . '-' . $rentalObj->Bookingno . ".pdf";

						
						$tutorEmail = $rentalObj->user->school->tutor_email;

						if (isset($rentalObj->prd_id) && !empty($rentalObj->prd_id)) {
							$tutor_detail = Property::find($rentalObj->prd_id);
							if (isset($tutor_detail->represent_email) && !empty($tutor_detail->represent_email)) {
								$tutorEmail = $tutor_detail->represent_email;
							}
						}

						$parentEmail = null;
						if (is_null($rentalObj->user->student_relationship)) {
							$studentEmail = $rentalObj->user->email;
						} else {
							$parentEmail = $rentalObj->user->email;
							$studentEmail = $rentalObj->user->semail16;
						} 
						
						
						 // get HR details
						$hr_enquiry = RentalsEnquiry::where(["id" => $enquiryid, 'is_hr' => '1'])->first();
						$hr_email = "";
						$hr_email_detail = "";
						if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
							$hr_user = User::find($hr_enquiry->renter_id);
							$hr_email = isset($hr_user->email) ? $hr_user->email : '';
							if (isset($hr->prd_id) && !empty($hr->prd_id)) {
								$hr_email_detail = Property::find($hr->prd_id);
							}
						}
						
						// Sending email to Student and parent for schoolhouse
						$this->mailNotificationsForUser->sendemailPaidToStudentAboutSchoolHouse($parentEmail, $studentEmail, $rentalObj);
						
						
						$school_house_email = isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : "";
						// Sending email to schoohouse listing owner about student details
						$this->mailNotificationsForUser->sendemailPaidToSchoolHouseAboutStudent($school_house_email, $rentalObj, $file_name);
						// I think this is uncessary code
						/*if ($rentalObj->is_hr == "1") {
							$propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
							$worktutoremail = $propertyObj->represent_email;
							if (!empty($worktutoremail)) {
								//$worktutoremail = $rentalObj->user->email;
								$organizationemail = $propertyObj->user->orgemail;
								//$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj);
							}
							//echo $worktutoremail; exit; 
							//exit;
							$checkIn = $rentalObj->checkin;
							$checkOut = $rentalObj->checkout;
							$noOfNyts = $rentalObj->numofdates;
							$renterId = $propertyObj->user->id;
							$dataArr = array(
								'checkin' => $checkIn,
								'checkout' => $checkOut,
								'numofdates' => $noOfNyts,
								'caltophone' => '',
								'enquiry_timezone' => '',
								'user_id' => $rentalObj->user_id,
								'renter_id' => $renterId,
								'prd_id' => $rentalObj->prd_id
							);


							$booking_status = array(
								'booking_status' => 'Pending'
							);

							$dataArr = array_merge($dataArr, $booking_status);
							$enquiryObj = RentalsEnquiry::create($dataArr);
							$insertid = $enquiryObj->id;

							if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
								$val = 10 * $insertid + 8;
								$val = 1500000 + $val;
								$bookingno = "WRL" . $val;
								$newdata = array(
									'Bookingno' => $bookingno
								);
								RentalsEnquiry::where('id', $insertid)->update($newdata);
							}

							$dataArr = array(
								'productId' => $rentalObj->prd_id,
								'bookingNo' => $bookingno,
								'enqid' => $insertid,
								'senderId' => $rentalObj->user_id,
								'receiverId' => $renterId,
								'subject' => 'Booking Request : ' . $bookingno,
								'message' => ""
							);
							MedMessage::create($dataArr);
						}*/
						
						// Updating user book_paid credits that are used now for this booking
						if($rentalObj->property->property_type == '26'){
							$userCredit = new \App\UserCredit();
							$userCredit->user_id = $rentalObj->user_id;
							$userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
							$userCredit->property_id = isset($rentalObj->property->id) ? $rentalObj->property->id : 0;
							
							$userCredit->book_paid = $rentalObj->numofdates; 
							
							$userCredit->save();
						}
						//returning with success message 
						$message = "You have successfully accepted this request! please check email for student details";
						return redirect()->back()->with("message", $message);
						
					}
                    
                }
				               
            }

            if ($status == 'Decline') {
                $userId = $rentalObj->user_id;
                $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();
                 $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
                RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
               
                $this->mailNotificationsForUser->sendDeclineEmail($rentalObj,$propertyDetails->exptitle);

                $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 1 ) : "1";
                \App\UserWallet::where('user_id',$userId)->update(array('send_to_schoolhouse'=>$send_to_schoolhouse,'creadit_type'=>''));

                $dataArr = array(
                    'productId' => $rentalObj->prd_id,
                    'bookingNo' => $rentalObj->Bookingno,
                    'enqid' => $enquiryid,
                    'senderId' => \Auth::user()->id,
                    'receiverId' => $rentalObj->user_id,
                    'subject' => 'Booking Request : ' . $status,
                    'message' => "You Request Declined"
                );
                MedMessage::create($dataArr);
                //flash($msg, 'Successfully updated');
                return redirect()->back()->with("message", "Successfully updated");
            }
		}
		
		// Orignal function
        public function updateSchoolHousebookingstatus(Request $request) {

            //Pending, Decline, Accept
            $status = $request->get('status');
            $enquiryid = $request->get('enquiryid');
            $rentalObj = RentalsEnquiry::find($enquiryid);
            $message = "";
            $msg = $status . " Successfully updated";
      
            if ($status == 'Accept') {
               // echo " IN Accept";die;
                $user_voucher = User::where('id', $rentalObj->user_id)->first();
                $acceptedUserId =  \Auth::user()->id;
                $acceptedUser = User::with('group_info')->where('id', $acceptedUserId)->first();
                $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();


                


                if($propertyDetails->property_status == 'unpaid' || $propertyDetails->property_status == 'unpaidflash'){
                    
                     $fetchWalletData = \App\UserWallet::where('user_id',$acceptedUserId)->first();
                    
                    $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 3 ) : "3";
                    // $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 1 ) : "1";
                    $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 0 ) : "0";
                    //this for unapaid property. so we should change it here.
                    \App\UserWallet::where('user_id',$acceptedUserId)->update(array('send_to_schoolhouse'=>$send_to_schoolhouse,'book_paid'=>$book_paid,'creadit_type'=>''));

                    $fetchWalletData = \App\UserWallet::where('user_id',$acceptedUserId)->first();
                     echo "<br> After update<pre>";
                        print_r($fetchWalletData);
                     echo "</pre><br> ";    
                }
                    $parentEmail = $rentalObj->user->email;
                    $studentEmail = $rentalObj->user->semail16;
                    
                  // $this->mailNotificationsForUser->sendemailPaidToStudentAboutWalletUpdateForSchoolHouseAfterJobAccept($parentEmail, $studentEmail, $rentalObj,$acceptedUser);            

                if(isset($user_voucher->vouchercredit_id) && !empty($user_voucher->vouchercredit_id)) {
                    $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
                }


                $checkInDate = explode(" ",$rentalObj->checkin);
                $checkOutDate = explode(" ",$rentalObj->checkout);
                // echo "Cunt :".count($checkInDate)."<br>";
                if(count($checkInDate) > 1){
                    $newDateStart = $rentalObj->checkin;
                }else{
                    $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin." 11:00:00"));
                }

                // $checkOutDate

                if(count($checkOutDate) > 1){
                    $newDateEnd = $rentalObj->checkout;
                }else{
                    $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout." 10:00:00"));
                }

                // Case 1 checking if requested user already accepted somewhere else and entertaining in  these dates ...
				if(isset($propertyDetails->property_type) and $propertyDetails->property_type=='26'){
                $userAllreadyAccpeted = RentalsEnquiry::userAllreadyAccpetedForPaid($newDateStart, $newDateEnd, $rentalObj->user_id); 
               // echo "<pre>";
                //print_r($userAllreadyAccpeted);die;
                if($userAllreadyAccpeted){
                    // return message
                    $message = "We are sorry, the Student Guest has already been accepted elsewhere.";
                    return redirect()->back()->with("error", $message);

                }
				}
                // Continue with case 3
                // first we will check credits 
                $countEnquiry = 0;
                $allowedEnquiryCount = 0;
                if (isset($rentalObj->property->property_status) && $rentalObj->property->property_status == "paid") {
                    $user_wallet = \App\UserWallet::where("user_id", $rentalObj->user_id)->first();
                    if (isset($user_wallet->book_paid)) {
                        $allowedEnquiryCount = $user_wallet->book_paid;
                        $get_credits_bookpaid_all = \App\UserCredit::where("user_id", $rentalObj->user_id)->where("book_paid", ">", "0")->get();

                        //calculating nights herer
                        $total_book_paid_sent = 0;
                        foreach ($get_credits_bookpaid_all as $key => $book_paid) {
                            $total_book_paid_sent += $book_paid->book_paid;
                        }
                       // echo "<pre>";
                        $get_credits_bookpaid = $total_book_paid_sent;

                       
                        
                        $countEnquiry = $get_credits_bookpaid;
                        
                    }
                }

                $Checkin_date = strtotime($rentalObj->checkin);
                $Checkout_date = strtotime($rentalObj->checkout);
                $dates_difference = $Checkout_date - $Checkin_date;
                $rentalObj->numofdates  = round($dates_difference / (60*60*24));
                $countEnquiry += $rentalObj->numofdates;
                
                if ($allowedEnquiryCount < $countEnquiry) {
                    $message = "When student pays you will get email confirmation with student details, please allow one week for this.";
                    return redirect()->back()->with("error", $message);
                }

              
                $checkInDate = explode(" ",$rentalObj->checkin);
                $checkOutDate = explode(" ",$rentalObj->checkout);
                // echo "Cunt :".count($checkInDate)."<br>";
                if(count($checkInDate) > 1){
                    $newDateStart = $rentalObj->checkin;
                }else{
                    $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin." 11:00:00"));
                }

                // $checkOutDate

                if(count($checkOutDate) > 1){
                    $newDateEnd = $rentalObj->checkout;
                }else{
                    $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout." 10:00:00"));
                }

                $availableCheck = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $rentalObj->prd_id);
              

                //return $availableCheck;
                if (count($availableCheck) > 0) {
                    $message = "NOTE: you have already accepted other requests for these dates, you may not accept more than one student of same nationality, this may be arranged only thorugh Admin, Thanks!";
                    
                    return redirect()->back()->with("message", $message);
                } else {
                    $message = "You have successfully accepted this request! please check email for student details";
                }
                RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
                $propertyId = $rentalObj->prd_id;
                $arrival = $rentalObj->checkin;
                $depature = $rentalObj->checkout;
                $dates = $this->getDatesFromRange($arrival, $depature);
                $this->saveBookedDates($dates, $propertyId);
                $this->updateScheduleCalendar($propertyId, 0);

                $dataArr = array(
                    'productId' => $rentalObj->prd_id,
                    'bookingNo' => $rentalObj->Bookingno,
                    'enqid' => $enquiryid,
                    'senderId' => \Auth::user()->id,
                    'receiverId' => $rentalObj->user_id,
                    'subject' => 'Booking Request : ' . $status,
                    'message' => "You Request Approved"
                );
                MedMessage::create($dataArr);
                if ($status == 'Accept') {
                    /* Send Learning Agreement */
                    $cmsObj = $this->cmsRepository->getById(9);
                    $stipulate = $this->cmsRepository->getById(12);
                    $article1 = $this->cmsRepository->getById(13);
                    $article2 = $this->cmsRepository->getById(14);
                    $article3 = $this->cmsRepository->getById(15);
                    $article4 = $this->cmsRepository->getById(16);
                    $article5 = $this->cmsRepository->getById(17);
                    $article6 = $this->cmsRepository->getById(18);
                    $article7 = $this->cmsRepository->getById(19);
                    $educationalobjectives = $this->cmsRepository->getById(20);
                    $article9 = $this->cmsRepository->getById(21);
                    $article10 = $this->cmsRepository->getById(22);

                    $rentalObj = RentalsEnquiry::find($enquiryid);

                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $file_name = $timestamp . '-' . $rentalObj->Bookingno . ".pdf";

                    //$pdf = PDF::loadView('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'))->save(public_path() . "/pdf/agreement/" . $file_name);
                    //return $file_name ;
                    $tutorEmail = $rentalObj->user->school->tutor_email;

                    if (isset($rentalObj->prd_id) && !empty($rentalObj->prd_id)) {
                        $tutor_detail = Property::find($rentalObj->prd_id);
                        if (isset($tutor_detail->represent_email) && !empty($tutor_detail->represent_email)) {
                            $tutorEmail = $tutor_detail->represent_email;
                        }
                    }

                    $parentEmail = null;
                    if (is_null($rentalObj->user->student_relationship)) {
                        $studentEmail = $rentalObj->user->email;
                    } else {
                        $parentEmail = $rentalObj->user->email;
                        $studentEmail = $rentalObj->user->semail16;
                    }

                    // get HR details
                    $hr_enquiry = RentalsEnquiry::where(["id" => $enquiryid, 'is_hr' => '1'])->first();
                    $hr_email = "";
                    $hr_email_detail = "";
                    if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
                        $hr_user = User::find($hr_enquiry->renter_id);
                        $hr_email = isset($hr_user->email) ? $hr_user->email : '';
                        if (isset($hr->prd_id) && !empty($hr->prd_id)) {
                            $hr_email_detail = Property::find($hr->prd_id);
                        }
                    }
                    
                    $this->mailNotificationsForUser->sendemailPaidToStudentAboutSchoolHouse($parentEmail, $studentEmail, $rentalObj);
                    $school_house_email = isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : "";
                    //echo $school_house_email; exit;
                    $this->mailNotificationsForUser->sendemailPaidToSchoolHouseAboutStudent($school_house_email, $rentalObj, $file_name);

                    if ($rentalObj->is_hr == "1") {
                        $propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
                        $worktutoremail = $propertyObj->represent_email;
                        if (!empty($worktutoremail)) {
                            //$worktutoremail = $rentalObj->user->email;
                            $organizationemail = $propertyObj->user->orgemail;
                            //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj);
                        }
                        //echo $worktutoremail; exit; 
                        //exit;
                        $checkIn = $rentalObj->checkin;
                        $checkOut = $rentalObj->checkout;
                        $noOfNyts = $rentalObj->numofdates;
                        $renterId = $propertyObj->user->id;
                        $dataArr = array(
                            'checkin' => $checkIn,
                            'checkout' => $checkOut,
                            'numofdates' => $noOfNyts,
                            'caltophone' => '',
                            'enquiry_timezone' => '',
                            'user_id' => $rentalObj->user_id,
                            'renter_id' => $renterId,
                            'prd_id' => $rentalObj->prd_id
                        );


                        $booking_status = array(
                            'booking_status' => 'Pending'
                        );

                        $dataArr = array_merge($dataArr, $booking_status);
                        $enquiryObj = RentalsEnquiry::create($dataArr);
                        $insertid = $enquiryObj->id;

                        if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                            $val = 10 * $insertid + 8;
                            $val = 1500000 + $val;
                            $bookingno = "WRL" . $val;
                            $newdata = array(
                                'Bookingno' => $bookingno
                            );
                            RentalsEnquiry::where('id', $insertid)->update($newdata);
                        }

                        $dataArr = array(
                            'productId' => $rentalObj->prd_id,
                            'bookingNo' => $bookingno,
                            'enqid' => $insertid,
                            'senderId' => $rentalObj->user_id,
                            'receiverId' => $renterId,
                            'subject' => 'Booking Request : ' . $bookingno,
                            'message' => ""
                        );
                        MedMessage::create($dataArr);
                    }
                }
                //flash($msg, 'Successfully updated');
                $userCredit = new \App\UserCredit();
                $userCredit->user_id = $rentalObj->user_id;
                $userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
                $userCredit->property_id = isset($rentalObj->property->id) ? $rentalObj->property->id : 0;
                if (isset($rentalObj->property->property_status) && $rentalObj->property->property_status == "paid") {
                   // if($rentalObj->property->property_type == 26){
                        $userCredit->book_paid = $rentalObj->numofdates;
                    //}else{
                      //  $userCredit->book_paid = 1;
                   // }
                } else {
                    $userCredit->book_unpaid = 1;
                }
                $userCredit->save();
                return redirect()->back()->with("message", $message);
            }

            if ($status == 'Decline') {
                $userId = $rentalObj->user_id;
                $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();
                 $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
                RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
               
                $this->mailNotificationsForUser->sendDeclineEmail($rentalObj,$propertyDetails->exptitle);

                $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 1 ) : "1";
                \App\UserWallet::where('user_id',$userId)->update(array('send_to_schoolhouse'=>$send_to_schoolhouse,'creadit_type'=>''));

                $dataArr = array(
                    'productId' => $rentalObj->prd_id,
                    'bookingNo' => $rentalObj->Bookingno,
                    'enqid' => $enquiryid,
                    'senderId' => \Auth::user()->id,
                    'receiverId' => $rentalObj->user_id,
                    'subject' => 'Booking Request : ' . $status,
                    'message' => "You Request Declined"
                );
                MedMessage::create($dataArr);
                //flash($msg, 'Successfully updated');
                return redirect()->back()->with("message", "Successfully updated");
            }
        } 

        public function tutorConfirmation($id) {
            $property = Property::find($id);
            $property->is_confirmed_by_tutor = "1";
            $property->save();
            return redirect()->to("user/dashboard/")->with("message", "Your Confirmation is accepted");
        }

        public function getwishlist() {
            $userObj = $this->userRepository->getLoginUser();
            return View('wishlistview', compact('userObj'));
        }

        public function getwishlistbyid($id) {
            $wishlistId = base64_decode($id);
            $wishlistobj = Wishlist::where('id', $wishlistId)->first();
            return View('wishlistdetail', compact('wishlistobj'));
        }

        public function removewishlist(Request $request) {
            $wishlistId = $request->get('wishlistid');
            $propertyId = $request->get('propertyId');
            $wishlistobj = Wishlist::where('id', $wishlistId)->first();
            $wishlistobj->properties()->detach($propertyId);
            return "Success";
        }

        public function addreview($id) {
            //echo base64_encode($id);exit;
            $id = base64_decode($id);
            $enquiryObj = RentalsEnquiry::find($id);
            return View('addreview', compact('enquiryObj'));
        }

        public function review_submit(Request $request) {
            $data = array('rateVal' => $request->get('ratingval'), 'reviewed_id' => $request->get('reviewedId'), 'reviewer_id' => $request->get('reviewerId'), 'prd_id' => $request->get('rentalId'), 'enquiryId' => $request->get('enquiryId'), 'description' => $request->get('description'));
            Review::create($data);
            flash("Review Added Successfully", 'success');
            return \Redirect::to('user/trips');
        }

        public function viewreviews($type) {
            $reviewObj = Review::where('reviewer_id', \Auth::user()->id)->where('status', 1)->get();
    //        return $reviewObj;
            if ($type == "aboutyou") {
                $reviewObj = Review::where('reviewed_id', \Auth::user()->id)->where('status', 1)->get();
                return View('reviews', compact('reviewObj'));
            }
            return View('reviewby', compact('reviewObj'));
        }

        public function fetch_older_property_request(){
            // $fetchOldReqest = RentalsEnquiry::select('property.exptitle', 'property.title', 'fc_rentalsenquiry.user_id', 'fc_rentalsenquiry.prd_id')->leftjoin('property', 'property.id', '=', 'fc_rentalsenquiry.prd_id')->where('fc_rentalsenquiry.user_id', $userid)->whereIn('property.property_status', ['unpaid', 'unpaidflash'])->where('approval', 'Accept')->orderBy('fc_rentalsenquiry.id', 'desc')->get()->first();
            //SELECT DATEDIFF(DATE(NOW()),DATE(fc_rentalsenquiry.dateAdded) ) as ReqDateDiff, DATE(DATE(fc_rentalsenquiry.dateAdded) - INTERVAL 7 DAY) as DateInter, fc_rentalsenquiry.dateAdded, fc_rentalsenquiry.* FROM `fc_rentalsenquiry` WHERE booking_status ='Pending' AND DATEDIFF(DATE(NOW()),DATE(fc_rentalsenquiry.dateAdded) ) >= 7 /*AND DATE(dateAdded) < DATE(NOW() - INTERVAL 7 DAY)*/ ORDER BY `id`  DESC
            // $fetchOldReqest = RentalsEnquiry::select('DATEDIFF(DATE(NOW()),DATE(fc_rentalsenquiry.dateAdded)) as dateDiff', 'dateAdded')->where('approval', 'pending')->orderBy('fc_rentalsenquiry.id', 'desc')->get();
            /*$fetchOldRequest = DB::table('fc_rentalsenquiry as fr')->join('property as p','fr.prd_id','=','p.id')->selectRaw('DATEDIFF(DATE(NOW()),DATE(fr.dateAdded)) as dateDiff,fr.dateAdded,fr.id,fr.user_id,fr.approval,fr.booking_status,fr.checkin,fr.checkout,p.exptitle')->where('fr.booking_status', 'Pending')->where(function($q){
                $q->where('fr.approval', 'Pending')->orWhere('fr.approval', 'Accept');
            })->whereRaw('DATEDIFF(DATE(NOW()),DATE(fr.dateAdded))=15')->whereIn('property_status',array('unpaid','unpaidflash'))->get();*/

            $fetchOldRequest = DB::table('fc_rentalsenquiry as fr')->join('property as p','fr.prd_id','=','p.id')->selectRaw('DATEDIFF(DATE(NOW()),DATE(fr.dateAdded)) as dateDiff,fr.dateAdded,fr.id,fr.user_id,fr.approval,fr.booking_status,fr.checkin,fr.checkout,p.exptitle')
            ->whereNotIn('fr.user_id',function($query){
                   $query->select('fc_rentalsenquiry.user_id')->from('fc_rentalsenquiry')->whereRaw('DATEDIFF(DATE(NOW()),DATE(fc_rentalsenquiry.dateAdded))=8')->where('fc_rentalsenquiry.approval', 'Accept')->get();
                })->where(function($q){
                $q->where('fr.approval', 'Pending')->orWhere('fr.approval', 'Accept');
            })->whereRaw('DATEDIFF(DATE(NOW()),DATE(fr.dateAdded))=8')->whereIn('property_status',array('unpaid','unpaidflash'))->get();

            echo "<prE>";
                    print_r($fetchOldRequest);
            echo "</prE>";
            exit;


            foreach($fetchOldRequest as $key=>$val){
                // echo "<pre>";
                //     print_r($val);
                // echo "</pre>";  
                $userId = $val->user_id;
                $userData = \App\User::select('email','name','lastname')->where('id',$userId)->first();
                if($val->approval != 'Accept'){
                   //update the wallet of user
                    // \App\UserWallet::where('user_id',$val->user_id)->update();
                $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();
                $send_to_unpaid = isset($fetchWalletData->send_to_unpaid) ? ($fetchWalletData->send_to_unpaid + 1 ) : "1";
                \App\UserWallet::where('user_id',$val->user_id)->update(array('send_to_unpaid'=>$send_to_unpaid,'creadit_type'=>''));
                }
                $email = $userData->email;
                $name = trim($userData->name." ".$userData->lastname);
                $check_in = $val->checkin;
                $checkout = $val->checkout;
                $custArray = ['property_title'=>$val->exptitle,"check_in"=>$check_in,"check_out"=>$checkout,"name"=>$name];
                // echo "<pre>";
                //     print_r($custArray);
                // echo "</pre>";
                Mail::send('emails.olderrequesttemplate', $custArray, function ($m) use ($email,$name,$check_in,$checkout) {
                    // echo "Email :".$email."<br> Checkin :".$check_in."<br> Checkout :".$checkout;exit;
                        $m->to($email, $name)->subject('Work Tutor reply still pending - Please send new requests');
                    });

                exit;
                          
            }
            exit;
        }    
        public function send_test_email(){

             ini_set( 'display_errors', 1 );
            error_reporting( E_ALL );
            /*$from = "notification@learn-in-a-flash.com";
            $to = "notification@learn-in-a-flash.com";
            // $to = array("objectscodes@gmail.com","notification@learn-in-a-flash.com");
            $subject = "Checking PHP mail";
            $message = "PHP mail works just fine";
            $headers = "From:" . $from;
            var_dump(mail($to,$subject,$message, $headers));
            echo "The email message was sent.";
            exit;*/ //I canot see FROM default fetch from smtp so no need for this testing smtp file_ for what daplease show me smtp detail file?yes okay
            try{
                // $to = 'hrtesting@webhomes.net';
                // var_dump(Mail::send([],[],function($message){
                //     $message->to('objectscodes@gmail.com',"New hosting details")->subject('Testing mail for hosting details')->setBody('HEllo Test');
                // }));
                //  echo "<br> Is Mail send <br> ";
                // var_dump(Mail::failures());
                $data = array();
                 \Mail::send([], $data, function($message){
                    $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                    $message->to("esperienzainglese@gmail.com","")
                            ->cc("objectscodes@gmail.com", 'work-related-learning')
                            ->subject('Work-Related-Learning.com - you have reveiced a message regarding a request');
                            // exit;
                });
                 // echo "Email Sent or failed";
                 var_dump(\Mail::failures());
                 echo "<br>";
                 \Log::debug(\Mail::failures());
                 echo "<br>";
                 \Log::debug('done emailing');
                 echo "<br>";
            }catch (\Exception $ex) {
                echo "<pre>";
                    print_r($ex->getMessage());
                    print_r($ex);
                echo "</pre>";
                exit;
                // return $ex->getMessage();
            }
            // Mail::send('this is test mail', ['form_data' => $email_data], function ($m) use ($email_data) {
            // Mail::send('this is test mail',  function ($m){
            //             $m->to('hrtesting@webhomes.net','Testing HR NAme')->subject('Testing Human Resource - You have received a request'); //HR receives same request as User Listing and may accept or decline a student applying for a job, just like the User Listing himself
            //         });
        }
		
		
		
		// New code foe send paid requests
		public function send_paid_request(Request $request){
			$userId = \Auth::user()->id;
			if(isset($request->service_ids)){
				$service_ids = $request->service_ids;
				$total_price = 0;
				$booking_number = '';
				foreach($service_ids as $service_id){
					$cartItem = \App\Cartlists::where('id',$service_id)->first();
					$property_id = $cartItem->property_id;  
					$pricing = \App\PropertyPricing::where("property_id", $property_id)->first();
					
					$currency_id = $pricing->currency_id;
					$currency    = \App\Currency::where(["id" => $currency_id])->first();
					if(isset($currency->currency_symbol) and $currency->currency_symbol != ""){
						$currency_symbol = $currency->currency_symbol;
					}else{
						$currency_symbol = $currency->currency_code;
					}
					
					if($pricing->payment_type == 'weekly'){
						
						$number = ($pricing->amount / 7);
						$per_day_price = number_format((float)$number, 2, '.', '');
					}else{
						$number = $pricing->amount;
						$per_day_price = number_format((float)$number, 2, '.', '');
					}
					
					$start_date = strtotime($cartItem->start_date);
					$end_date = strtotime($cartItem->end_date);
					$diff = $end_date - $start_date; 
					$number_of_days =  abs(round($diff / 86400)); 
					$total_price = $number_of_days * $per_day_price;
					
					// Check if basket item property type is paid 
					$propertyObj  = $this->propertyRepository->getCurrentObject($property_id);
					
					if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
						// Check if user has credits to send request if has then we will charge for the price of services  
						// otherwise we will show message you do not have credits to send request 
						// please get send_to_schoolhouse credits first.
						$user_has_credits = $this->checkAvailablePaidCredits();
						//var_dump($user_has_credits);die;
						//user has credits so we have to charge first and then send booking request.

						if($user_has_credits){
							$checkIn      = date('Y-m-d 11:00:00',$start_date);
							$checkOut     = date('Y-m-d 10:00:00',$end_date);
							
							$booking_number = $this->sendPaidRequest($property_id, $checkIn,$checkOut,$number_of_days);
							
						}else{
							redirect()->back()->with("error",'Sorry you do not have credits to send request to schoolhouse.');
							
						}
						
					}
					
					
					
					
				}
				
				if($booking_number != ''){
					\App\Cartlists::where('user_id',$userId)->delete();
											
							
					return \Redirect('request/booking-multiple/' . $booking_number)->with("success", "Your requests successfully sent.");
					
				}
						
				
			}else{
				redirect()->back()->with("error", "please choose any service first.");
			}
			return back();
		}
		
		public function checkAvailablePaidCredits() {
			$userId = \Auth::user()->id;
			
			$total_send    = 0;
			$total_allowed = 0;
			
			$user_credits = \App\UserCredit::where("user_id", $userId)->where("send_to_schoolhouse", ">", "0")->get();
			$total_schoolhouse_sent = 0;
			if (sizeof($user_credits) > 0) {
				$countEnquiry = $user_credits->count();                    
				foreach ($user_credits as $key => $user_credit) {
					$total_schoolhouse_sent += $user_credit->send_to_schoolhouse;
				}
			}

			$total_send = $total_schoolhouse_sent;
			
			
			$user_voucher = User::where('id', $userId)->first();
				
			if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
				$get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->where('user_id',$userId)->first();                  
			   
				if (isset($get_voucher->send_to_schoolhouse) && !empty($get_voucher->send_to_schoolhouse)) {
 
					$total_allowed = $get_voucher->send_to_schoolhouse;

				} else {
					return false;
					// are you there?why here is using default voucher if its credits are not enough?
					$get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
					if (isset($get_voucher->id)) {
						$userWallet = new \App\UserWallet();
						$userWallet->user_id = $userId;
						$userWallet->vouchaercredits_id = $get_voucher->id;
						$userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
						$userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
						$userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
						$userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
						$userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
						$userWallet->creadit_type = "voucher";
						$userWallet->save();
						$total_allowed = $get_voucher->send_to_schoolhouse;
					}
				}
			} else {
				return false;
				$get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
				if (isset($get_voucher->id)) {
					$user_voucher->vouchercredit_id = $get_voucher->id;
					$user_voucher->save();
					$userWallet = new \App\UserWallet();
					$userWallet->user_id = $userId;
					$userWallet->vouchaercredits_id = $get_voucher->id;
					$userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
					$userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
					$userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
					$userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
					$userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
					$userWallet->creadit_type = "voucher";
					$userWallet->save();
					$total_allowed = $get_voucher->send_to_schoolhouse;
				}
			} 
			
			if($total_send < $total_allowed){
				return true;
			}else{
				return false;
			}
		}
		
		public function sendPaidRequest($productId, $checkIn,$checkOut,$noOfNyts) {
			$userId = \Auth::user()->id;
			
			$userEnquiries = RentalsEnquiry::where('user_id', $userId)->get();
			
			$propertyObj  = $this->propertyRepository->getCurrentObject($productId);
			$settingsObj  = PropertySettings::propertySettings();
			$user_voucher = User::where('id', $userId)->first();
			
			

			$renterId = $propertyObj->user->id;

			//check if this is first choice of student.
			$booking_count = 0;
			$users_info = \App\User::with("user_booking", "school_info")->where("id", $userId)->first();
			if (isset($users_info->user_booking)) {
				$booking_count = count($users_info->user_booking);
			}
		 
			$msg = "";
			$worktutoremail = $propertyObj->represent_email;
			$organizationemail = $propertyObj->user->orgemail;
			$bookingno = "";
			$hr_email = $propertyObj->hr_represent_email;
			$check_hr = $propertyObj->human_resources;

			if (($check_hr == 0) && !empty($hr_email)) {
				$hr_details = User::where('email', $hr_email)->first();
				if (isset($hr_details->id) && !empty($hr_details->id)) {   
					//it checks if User Listing has defined another person, and email, as HR for the organisation hosting the student trainee
				    $hr_dataArr = array(
						'checkin' => $checkIn,
						'checkout' => $checkOut,
						'numofdates' => $noOfNyts,
						'caltophone' => '',
						'enquiry_timezone' => '',
						'user_id' => \Auth::user()->id,
						'renter_id' => $renterId,
						'is_hr' => '1',
						'prd_id' => $productId
					);


					$hr_booking_status = array(
						'booking_status' => 'Pending'
					);

					$hr_dataArr = array_merge($hr_dataArr, $hr_booking_status);
					$hr_enquiryObj = RentalsEnquiry::create($hr_dataArr);
					$hr_insertid = $hr_enquiryObj->id;

					if ($hr_enquiryObj->Bookingno == '' || $hr_enquiryObj->Bookingno == NULL) {
						$val = 10 * $hr_insertid + 8;
						$val = 1500000 + $val;
						$bookingno = "WRL" . $val;
						$hr_newdata = array(
							'Bookingno' => $bookingno
						);
						RentalsEnquiry::where('id', $hr_insertid)->update($hr_newdata);
					}
					$dataArr = array(
						'productId' => $productId,
						'bookingNo' => $bookingno,
						'enqid' => $hr_insertid,
						'senderId' => \Auth::user()->id,
						'receiverId' => $renterId,
						'subject' => 'Booking Request : ' . $bookingno,
						'message' => ""
					);
					MedMessage::create($dataArr);
					$hr_enquiryObj = RentalsEnquiry::where("id", $hr_insertid)->first();
				  
					$availableCheck = $userEnquiries->count();
                    //see here i am using existing sendBookingEmais functions for sending email for requests
					$this->sendBookingEmails($availableCheck, $propertyObj, $hr_enquiryObj, "hr", $hr_email, $worktutoremail, $organizationemail);

				} else {
				   
					$dataArr = array(
						'checkin' => $checkIn,
						'checkout' => $checkOut,
						'numofdates' => $noOfNyts,
						'caltophone' => '',
						'enquiry_timezone' => '',
						'user_id' => \Auth::user()->id,
						'renter_id' => $renterId,
						'prd_id' => $productId
					);


					$booking_status = array(
						'booking_status' => 'Pending'
					);

					$dataArr = array_merge($dataArr, $booking_status);
					$enquiryObj = RentalsEnquiry::create($dataArr);
					$insertid = $enquiryObj->id;

					if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
						$val = 10 * $insertid + 8;
						$val = 1500000 + $val;
						$bookingno = "WRL" . $val;
						$newdata = array(
							'Bookingno' => $bookingno
						);
						RentalsEnquiry::where('id', $insertid)->update($newdata);
					}  

					$dataArr = array(
						'productId' => $productId,
						'bookingNo' => $bookingno,
						'enqid' => $insertid,
						'senderId' => \Auth::user()->id,
						'receiverId' => $renterId,
						'subject' => 'Booking Request : ' . $bookingno,
						'message' => ""
					);
					MedMessage::create($dataArr);
					$enquiryObj = RentalsEnquiry::where("id", $insertid)->first();
					//checking if user have its first choice within selected dates or not
					$availableCheck = $userEnquiries->count();
					$this->sendBookingEmails($availableCheck, $propertyObj, $enquiryObj, "no_hr", $hr_email, $worktutoremail, $organizationemail);

				}
			} else {
			   
				$dataArr = array(
					'checkin' => $checkIn,
					'checkout' => $checkOut,
					'numofdates' => $noOfNyts,
					'caltophone' => '',
					'enquiry_timezone' => '',
					'user_id' => \Auth::user()->id,
					'renter_id' => $renterId,
					'prd_id' => $productId
				);


				$booking_status = array(
					'booking_status' => 'Pending'
				);

				$dataArr = array_merge($dataArr, $booking_status);
				$enquiryObj = RentalsEnquiry::create($dataArr);
				$insertid = $enquiryObj->id;

				if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
					$val = 10 * $insertid + 8;
					$val = 1500000 + $val;
					$bookingno = "WRL" . $val;
					$newdata = array(
						'Bookingno' => $bookingno
					);
					RentalsEnquiry::where('id', $insertid)->update($newdata);
				}

				$dataArr = array(
					'productId' => $productId,
					'bookingNo' => $bookingno,
					'enqid' => $insertid,
					'senderId' => \Auth::user()->id,
					'receiverId' => $renterId,
					'subject' => 'Booking Request : ' . $bookingno,
					'message' => ""
				);
				MedMessage::create($dataArr);
				$enquiryObj = RentalsEnquiry::where("id", $insertid)->first();
				//checking if user have its first choice within selected dates or not
			  
				$availableCheck = $userEnquiries->count();
				$this->sendBookingEmails($availableCheck, $propertyObj, $enquiryObj, "no_hr", $hr_email, $worktutoremail, $organizationemail);

			}
			 if($noOfNyts==0){
				$noOfNyts = 1;
			}

			$userCredit = new \App\UserCredit();
			$userCredit->user_id = $userId;
			$userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
			$userCredit->property_id = isset($propertyObj->id) ? $propertyObj->id : 0;
			if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
				$userCredit->send_to_schoolhouse = 1;
			} else {
				$userCredit->send_to_unpaid = 1;
			}
			$userCredit->save();
			return $bookingno;
			
		}
		

    }