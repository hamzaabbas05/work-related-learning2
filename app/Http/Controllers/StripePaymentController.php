<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Http\Requests;
	use App\Http\Controllers\Controller;
	use App\Repositories\PropertytypeRepository;
	use App\Repositories\RoomtypeRepository;
	use App\Repositories\PropertyRepository;
	use App\Repositories\AmenityRepository;
	use App\Repositories\SpecialAttributesRepository;
	use App\Services\Constants\AppConstants;
	use App\Repositories\CancellationRepository;
	use App\Repositories\CommissionRepository; 
	use App\Repositories\UserRepository;
	use App\Repositories\CmsRepository;
	use App\PropertyImages;
	use App\GuestRoomImages;
	use App\RoomCalendar;
	use App\PropertySettings;
	use App\Property;
	use App\Wishlist;
	use Carbon\Carbon;
	use App\Booking;
	use App\RentalsEnquiry;
	use App\Review;
	use App\MedMessage;
	use App\Amenity;
	use App\Schedule;
	use App\Tutor;
	use App\Services\Mailer\MailNotificationsForUser;
	use Mail;
	use App\User;
	use Dompdf\Dompdf as Dompdf;
	use PDF;
	use Session;
	use Stripe;

	class StripePaymentController extends Controller
	{
		public function __construct(PropertytypeRepository $propertyTypeRepository, RoomtypeRepository $roomTypeRepository, PropertyRepository $propertyRepository, AmenityRepository $amenityRepository, SpecialAttributesRepository $specialAttributesRepository, CancellationRepository $cancellationRepository, UserRepository $userRepository, CommissionRepository $commissionRepository, CmsRepository $cmsRepository, MailNotificationsForUser $mailNotificationsForUser) {
			$this->propertyTypeRepository = $propertyTypeRepository;
			$this->roomTypeRepository = $roomTypeRepository;
			$this->propertyRepository = $propertyRepository;
			$this->amenityRepository = $amenityRepository;
			$this->specialAttributesRepository = $specialAttributesRepository;
			$this->cancellationRepository = $cancellationRepository;
			$this->userRepository = $userRepository;
			$this->commissionRepository = $commissionRepository;
			$this->cmsRepository = $cmsRepository;
			$this->mailNotificationsForUser = $mailNotificationsForUser;
		}

		/**
		 * success response method.
		 *
		 * @return \Illuminate\Http\Response
		 */ 
		public function stripe()
		{
			return view('stripe');
		}
	  
		/**
		 * success response method.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function stripePost(Request $request){
			$userId = \Auth::user()->id;
			if(isset($request->booking_number) && !empty($request->booking_number)){
				
				$enquiryObj = RentalsEnquiry::where('Bookingno', $request->booking_number)->first();
			
				$check_in   = $enquiryObj->checkin;
				$check_out  = $enquiryObj->checkout;
				$sent_date  = $enquiryObj->dateAdded;
				$sender_id  = $enquiryObj->user_id;
				$enquiryid  = $enquiryObj->id;
				$sender_user = User::where('id', $sender_id)->first();

				//requested property object
				$propertyDetails = Property::where('id',$enquiryObj->prd_id)->first();
					
				$checkInDate = explode(" ",$enquiryObj->checkin);
				$checkOutDate = explode(" ",$enquiryObj->checkout);
			  
				if(count($checkInDate) > 1){
					$newDateStart = $enquiryObj->checkin;
				}else{
					$newDateStart = date("Y-m-d H:i:s", strtotime($enquiryObj->checkin." 11:00:00"));
				}

				if(count($checkOutDate) > 1){
					$newDateEnd = $enquiryObj->checkout;
				}else{
					$newDateEnd = date("Y-m-d H:i:s", strtotime($enquiryObj->checkout." 10:00:00"));
				}
				// if booking status is awaiting-payment
				if($enquiryObj->approval == 'Awaiting Payment'){ 
					/*** Third Step if user has credits or not ***/
					$book_paid_used    = 0;
					$allowed_book_paid = 0;
					
					if(isset($sender_user->vouchercredit_id) && !empty($sender_user->vouchercredit_id)) {
						$sender_wallet = \App\UserWallet::where("vouchaercredits_id", $sender_user->vouchercredit_id)->where("user_id", $sender_id)->first();
					}else{
						$sender_wallet = \App\UserWallet::where("user_id", $sender_id)->first();
					}
					
					$total_amount = 0;
					if (isset($sender_wallet->amount)) {
						$total_amount = $sender_wallet->amount;
					}
					
					if (isset($sender_wallet->book_paid)) {
						//total allowed book_paid
						$allowed_book_paid = $sender_wallet->book_paid;
						
						
						//getting used book_paid till now
						$get_credits_bookpaid_used = \App\UserCredit::where("user_id", $sender_id)->where("book_paid", ">", "0")->get();

						//calculating total used book_paid here
						foreach ($get_credits_bookpaid_used as $key => $book_paid) {
							$book_paid_used += $book_paid->book_paid;
						}
					  
						
					}                
					$remaining_book_paid = $allowed_book_paid - $book_paid_used;
					$Checkin_date = strtotime($enquiryObj->checkin);
					$Checkout_date = strtotime($enquiryObj->checkout);
					$dates_difference = $Checkout_date - $Checkin_date;
					$enquiryObj->numofdates  = round($dates_difference / (60*60*24));
					 
					$total_required_book_paid_for_this_booking = $enquiryObj->numofdates;
					$book_paid_used += $total_required_book_paid_for_this_booking;
					//echo 'book_paid_used = '.$book_paid_used.'<br>';
					//echo 'allowed_book_paid = '.$allowed_book_paid.'<br>';die;
					if ($allowed_book_paid < $book_paid_used) {
						$property_id = $enquiryObj->property->id;  
						$booking_number = $enquiryObj->Bookingno;  
						$fetchProperty = \App\Property::where('id',$property_id)->first();
						if($fetchProperty->property_type == '26' ){
							$bed_guest_room_id = $fetchProperty->bed_guest_room_id;
							$bed_type_id = $fetchProperty->bed_type_id;
							$guest_room = \App\UserPaidGuestRoom::where('id',$bed_guest_room_id)->first();
							$room_type_id = $guest_room->room_type_id;
							$room_type = \App\RealRoomTypes::where("is_active", "1")->where("id", $room_type_id)->first();
							
							$bed_type = \App\BedType::where(["is_active" => "1", "id" => $bed_type_id])->first();
							
							
							$room_name = $room_type->room_type.' Room ';
							$bed_name = $bed_type->bed_type.' Bed';
							$service_title = $room_name.' '.$bed_name;	
						}else{
							$service_title = $fetchProperty->exptitle;
						}
						
						$pricing = \App\PropertyPricing::where("property_id", $property_id)->first();
						
						$currency_id = $pricing->currency_id;
						$currency    = \App\Currency::where(["id" => $currency_id])->first();
						if(isset($currency->currency_symbol) and $currency->currency_symbol != ""){
							$currency_symbol = $currency->currency_symbol;
						}else{
							$currency_symbol = $currency->currency_code;
						}
						$currency_code = $currency->currency_code;
						if($currency_code != 'GBP'){				
							$amount_currency = 'GBP';
							$url = "https://api.exchangeratesapi.io/latest?base=$amount_currency";  
							$ch = curl_init();
							  curl_setopt($ch, CURLOPT_URL, $url);
							  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
							  $data = curl_exec($ch);
							  curl_close($ch); 
							
							$converted = json_decode($data);
							$converted_amount = $converted->rates->$currency_code;   
							$total_amount = number_format((float)($total_amount*$converted_amount), 2, '.', '');  
							
						}    
						if($pricing->payment_type == 'weekly'){
							
							$number = ($pricing->amount / 7);
							$per_day_price = number_format((float)$number, 2, '.', '');
						}else{
							$number = $pricing->amount;
							$per_day_price = number_format((float)$number, 2, '.', '');
						}
						 
						$to_be_paid = (($total_required_book_paid_for_this_booking - $remaining_book_paid)*$per_day_price) - $total_amount;
						 
						$booking_request = (object)array(
										'booking_number'=>$booking_number,
										'service_title'=>$service_title,
										'checkin_date'=>$newDateStart,
										'checkout_date'=>$newDateEnd,
										'sent_date'=>$newDateEnd,
										'your_credits'=>$remaining_book_paid,
										'required_credits'=>$total_required_book_paid_for_this_booking,
										'per_day_price'=>$per_day_price,
										'currency_symbol'=>$currency_symbol,
										'to_be_paid'=>$to_be_paid
										);
						
						
							Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
						   
							$response = Stripe\Charge::create ([								   
																"amount" => $to_be_paid * 100,
																"currency" => strtolower($currency_code),
																"source" => $request->stripeToken,
																"description" => "SchooHouse Payment Againts Bookingno #".$booking_number,
																'metadata' => array(
																	'Bookingno' => $booking_number
																)
															]);	
							
							if(isset($response->status) and $response->status == 'succeeded'){
								
								//Add credits to user wallet for that payment
								if(isset($sender_user->vouchercredit_id) && !empty($sender_user->vouchercredit_id)) {
									$sender_wallet = \App\UserWallet::where("vouchaercredits_id", $sender_user->vouchercredit_id)->where("user_id", $sender_id)->first();
								}else{
									$sender_wallet = \App\UserWallet::where("user_id", $sender_id)->first();
								}
								
								if (isset($sender_wallet->book_paid)) {
									$new_book_paid = $sender_wallet->book_paid + ($total_required_book_paid_for_this_booking - $remaining_book_paid);
									$new_wallet_data = array('book_paid'=>$new_book_paid,
															'amount'=>0
															);
									\App\UserWallet::where("user_id", $sender_id)->update($new_wallet_data);
									
								}
						    	
								//If payment is successful
								//All Set for booking accepted
						
								//updating status to accepted
								RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => 'Accept','booking_status' => 'Accept'));
								
								$propertyId = $enquiryObj->prd_id;
								$arrival = $enquiryObj->checkin;
								$depature = $enquiryObj->checkout;
								$dates = $this->getDatesFromRange($arrival, $depature);
								//save booking dates for calendar not available in  search results 
								$this->saveBookedDates($dates, $propertyId);
								$this->updateScheduleCalendar($propertyId, 0);
								//NOTE=== here i think we should disabled these dates for user first listing too ==== 
								// these are my thoughts because we should disable and will not come in search results then
								$status = 'Accept';
								
								// creating accepted status message 
								$dataArr = array(
									'productId' => $enquiryObj->prd_id,
									'bookingNo' => $enquiryObj->Bookingno,
									'enqid' => $enquiryid,
									'senderId' => \Auth::user()->id,
									'receiverId' => $enquiryObj->user_id,
									'subject' => 'Booking Request : ' . $status,
									'message' => "You Request Approved"
								);						
								MedMessage::create($dataArr);
								
								
								/* Send Learning Agreement */
								$cmsObj = $this->cmsRepository->getById(9);
								$stipulate = $this->cmsRepository->getById(12);
								$article1 = $this->cmsRepository->getById(13);
								$article2 = $this->cmsRepository->getById(14);
								$article3 = $this->cmsRepository->getById(15);
								$article4 = $this->cmsRepository->getById(16);
								$article5 = $this->cmsRepository->getById(17);
								$article6 = $this->cmsRepository->getById(18);
								$article7 = $this->cmsRepository->getById(19);
								$educationalobjectives = $this->cmsRepository->getById(20);
								$article9 = $this->cmsRepository->getById(21);
								$article10 = $this->cmsRepository->getById(22);

								$rentalObj = RentalsEnquiry::find($enquiryid);

								$timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
								$file_name = $timestamp . '-' . $rentalObj->Bookingno . ".pdf";

								
								$tutorEmail = $rentalObj->user->school->tutor_email;

								if (isset($rentalObj->prd_id) && !empty($rentalObj->prd_id)) {
									$tutor_detail = Property::find($rentalObj->prd_id);
									if (isset($tutor_detail->represent_email) && !empty($tutor_detail->represent_email)) {
										$tutorEmail = $tutor_detail->represent_email;
									}
								}

								$parentEmail = null;
								if (is_null($rentalObj->user->student_relationship)) {
									$studentEmail = $rentalObj->user->email;
								} else {
									$parentEmail = $rentalObj->user->email;
									$studentEmail = $rentalObj->user->semail16;
								} 
								
								
								 // get HR details
								$hr_enquiry = RentalsEnquiry::where(["id" => $enquiryid, 'is_hr' => '1'])->first();
								$hr_email = "";
								$hr_email_detail = "";
								if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
									$hr_user = User::find($hr_enquiry->renter_id);
									$hr_email = isset($hr_user->email) ? $hr_user->email : '';
									if (isset($hr->prd_id) && !empty($hr->prd_id)) {
										$hr_email_detail = Property::find($hr->prd_id);
									}
								}
								
								// Sending email to Student and parent for schoolhouse
								$this->mailNotificationsForUser->sendemailPaidToStudentAboutSchoolHouse($parentEmail, $studentEmail, $rentalObj);
								
								
								$school_house_email = isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : "";
								// Sending email to schoohouse listing owner about student details
								$this->mailNotificationsForUser->sendemailPaidToSchoolHouseAboutStudent($school_house_email, $rentalObj, $file_name);

								
								
								// Updating user book_paid credits that are used now for this booking
								if($rentalObj->property->property_type == '26'){ 
									$userCredit = new \App\UserCredit();
									$userCredit->user_id = $rentalObj->user_id;
									$userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
									$userCredit->property_id = isset($rentalObj->property->id) ? $rentalObj->property->id : 0;
									
									$userCredit->book_paid = $rentalObj->numofdates;
									
									$userCredit->save();
								}
								
								//returning with success message 
								$message = "Your Booking Confirmed please check email for more details";
								return redirect()->to('/user/trips')->with("success", $message);
								 
							}else{
								//payment not succeeded
								return redirect()->back()->with("error", 'Sorry there is any error for payment');
							}
						
					}
				}else{
					return redirect()->back()->with("error", 'Your Booking Already Booked.');
				}
			}
		}
		
		public function getDatesFromRange($start, $end) {
            $dates = array($start);
            while (end($dates) < $end) {
                // $dates[] = date('Y-m-d', strtotime(end($dates) . ' +1 day'));
                $dates[] = date('Y-m-d H:i:s', strtotime(end($dates) . ' +1 day'));
            }
            $endDate = str_replace("11:00:00","10:00:00",end($dates));
            $dates[count($dates)-1] = $endDate; // was this not here for time issues?due to this currently saved disable dates are not storing whole dates , but not for this I have added additional timing in main function. Remeber this unction was woking in original script. yes currently its working but I need to save timing when disable dates so I am checking the possibility without affect the existing system oworkingk. have we stopped storing these hours_ yes that is causing issue  okay so i have to chnage logic here? I bleieve logic good, it used to work, i saw in recent reuest time 00.00 start and end, not 11 and 10 so I guess issue is in not storing time_ ok wait

            return $dates;
        }
		public function updateScheduleCalendar($propertyId, $price) {
            $DateArr = Booking::where('PropId', $propertyId)->get();
            $dateDispalyRowCount = 0;
            $dateArrVAl = "";
            if ($DateArr->count() > 0) {
                $dateArrVAl .= '{';
                foreach ($DateArr as $dateDispalyRow) {
                    if ($dateDispalyRowCount == 0) {
                        $dateArrVAl .= '"' . $dateDispalyRow->the_date . '":{"available":"1","bind":0,"info":"","notes":"","price":"' . $price . '","promo":"","status":"booked"}';
                    } else {
                        $dateArrVAl .= ',"' . $dateDispalyRow->the_date . '":{"available":"1","bind":0,"info":"","notes":"","price":"' . $price . '","promo":"","status":"booked"}';
                    }
                    $dateDispalyRowCount = $dateDispalyRowCount + 1;
                }
                $dateArrVAl .= '}';
                $scheduleObj = Schedule::where('id', $propertyId)->exists();
                $inputArr4 = array('id' => $propertyId, 'data' => trim($dateArrVAl));
                if ($scheduleObj) {
                    Schedule::where('id', $propertyId)->update($inputArr4);
                } else {
                    Schedule::create($inputArr4);
                }
            }
        }
		public function saveBookedDates($dates, $propertyId) {
            $i = 1;
            $dateMinus1 = count($dates);
            foreach ($dates as $date) {
                if ($i <= $dateMinus1) {
                    $bookingObj = Booking::where('PropId', $propertyId)->where('id_state', 1)->where('id_item', 1)->where('the_date', $date)->get();
                    if ($bookingObj->count() > 0) {
                        
                    } else {
                        $dataArr = array('PropId' => $propertyId,
                            'id_state' => 1,
                            'id_item' => 1,
                            'the_date' => $date
                        );
                        Booking::create($dataArr);
                    }
                }
                $i++;
            } 
        }
		
		public function stripePostOldForFrontEnd(Request $request)
		{
			$userId = \Auth::user()->id;
			if(isset($request->service_ids)){
				$service_ids = $request->service_ids;
				$total_price = 0;
				foreach($service_ids as $service_id){
					$cartItem = \App\Cartlists::where('id',$service_id)->first();
					$property_id = $cartItem->property_id;
					$pricing = \App\PropertyPricing::where("property_id", $property_id)->first();
					
					$currency_id = $pricing->currency_id;
					$currency    = \App\Currency::where(["id" => $currency_id])->first();
					if(isset($currency->currency_symbol) and $currency->currency_symbol != ""){
						$currency_symbol = $currency->currency_symbol;
					}else{
						$currency_symbol = $currency->currency_code;
					}
					
					if($pricing->payment_type == 'weekly'){
						
						$number = ($pricing->amount / 7);
						$per_day_price = number_format((float)$number, 2, '.', '');
					}else{
						$number = $pricing->amount;
						$per_day_price = number_format((float)$number, 2, '.', '');
					}
					
					$start_date = strtotime($cartItem->start_date);
					$end_date = strtotime($cartItem->end_date);
					$diff = $end_date - $start_date; 
					$number_of_days =  abs(round($diff / 86400)); 
					$total_price = $number_of_days * $per_day_price;
					
					// Check if basket item property type is paid 
					$propertyObj  = $this->propertyRepository->getCurrentObject($property_id);
					
					if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
						// Check if user has credits to send request if has then we will charge for the price of services  
						// otherwise we will show message you do not have credits to send request 
						// please get send_to_schoolhouse credits first.
						$user_has_credits = $this->checkAvailablePaidCredits();
						//user has credits so we have to charge first and then send booking request.
						if($user_has_credits){
							$checkIn      = date('Y-m-d 11:00:00',$start_date);
							$checkOut     = date('Y-m-d 10:00:00',$end_date);
							
							$booking_number = $this->request_booking($property_id, $checkIn,$checkOut,$number_of_days);
							
							Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
						   /* $customerDetails  = \Stripe\Customer::create([
												  'description' => 'Abbas Jami',
												  'email' => 'abbasjami47@gmail.com',
												  'source' => $request->stripeToken
												]);*/
							
							
							
							
							
							
							Stripe\Charge::create ([
								   // "customer"=>$customerDetails->id,
									"amount" => $total_price * 100,
									"currency" => env('STRIPE_CURRENCY'),
									"source" => $request->stripeToken,
									"description" => "Test payment from abbas jami for work related learning site",
									'metadata' => array(
										'order_id' => 'Booking ID will be here'
									)
							]);			
							
							\App\Cartlists::where('user_id',$userId)->delete();
							
							
							Session::flash('success', 'Payment successful!');
							
							return \Redirect('request/booking/' . $booking_number);
							
						}else{
							Session::flash('error', 'Sorry you do not have credits to send request to schoolhouse (send_to_schoolhouse).');
						}
						
					}
					
					
					
					
				}
				
				
			}else{
				Session::flash('error', 'Please Select Services first.');
			}
			return back();
		}
		
		
		public function checkAvailablePaidCredits() {
			$userId = \Auth::user()->id;
			
			$total_send    = 0;
			$total_allowed = 0;
			
			$user_credits = \App\UserCredit::where("user_id", $userId)->where("send_to_schoolhouse", ">", "0")->get();
			$total_schoolhouse_sent = 0;
			if (sizeof($user_credits) > 0) {
				$countEnquiry = $user_credits->count();                    
				foreach ($user_credits as $key => $user_credit) {
					$total_schoolhouse_sent += $user_credit->send_to_schoolhouse;
				}
			}

			$total_send = $total_schoolhouse_sent;
			
			
			$user_voucher = User::where('id', $userId)->first();
				
			if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
				$get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();                  
			   
				if (isset($get_voucher->send_to_schoolhouse) && !empty($get_voucher->send_to_schoolhouse)) {

					$total_allowed = $get_voucher->send_to_schoolhouse;

				} else {
					return false;
					// are you there?why here is using default voucher if its credits are not enough?
					$get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
					if (isset($get_voucher->id)) {
						$userWallet = new \App\UserWallet();
						$userWallet->user_id = $userId;
						$userWallet->vouchaercredits_id = $get_voucher->id;
						$userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
						$userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
						$userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
						$userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
						$userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
						$userWallet->creadit_type = "voucher";
						$userWallet->save();
						$total_allowed = $get_voucher->send_to_schoolhouse;
					}
				}
			} else {
				return false;
				$get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
				if (isset($get_voucher->id)) {
					$user_voucher->vouchercredit_id = $get_voucher->id;
					$user_voucher->save();
					$userWallet = new \App\UserWallet();
					$userWallet->user_id = $userId;
					$userWallet->vouchaercredits_id = $get_voucher->id;
					$userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
					$userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
					$userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
					$userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
					$userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
					$userWallet->creadit_type = "voucher";
					$userWallet->save();
					$total_allowed = $get_voucher->send_to_schoolhouse;
				}
			}
			
			if($total_send < $total_allowed){
				return true;
			}else{
				return false;
			}
		}
		public function request_booking($productId, $checkIn,$checkOut,$noOfNyts) {
			$userId = \Auth::user()->id;
			$userEnquiries = RentalsEnquiry::where('user_id', $userId)->get();
			
			$propertyObj  = $this->propertyRepository->getCurrentObject($productId);
			$settingsObj  = PropertySettings::propertySettings();
			$user_voucher = User::where('id', $userId)->first();
			
			

			$renterId = $propertyObj->user->id;

			//check if this is first choice of student.
			$booking_count = 0;
			$users_info = \App\User::with("user_booking", "school_info")->where("id", $userId)->first();
			if (isset($users_info->user_booking)) {
				$booking_count = count($users_info->user_booking);
			}
		 
			$msg = "";
			$worktutoremail = $propertyObj->represent_email;
			$organizationemail = $propertyObj->user->orgemail;
			$bookingno = "";
			$hr_email = $propertyObj->hr_represent_email;
			$check_hr = $propertyObj->human_resources;

			if (($check_hr == 0) && !empty($hr_email)) {
				$hr_details = User::where('email', $hr_email)->first();
				if (isset($hr_details->id) && !empty($hr_details->id)) {   //it checks if User Listing has defined another person, and email, as HR for the organisation hosting the student trainee
				   
					$hr_dataArr = array(
						'checkin' => $checkIn,
						'checkout' => $checkOut,
						'numofdates' => $noOfNyts,
						'caltophone' => '',
						'enquiry_timezone' => '',
						'user_id' => \Auth::user()->id,
						'renter_id' => $renterId,
						'is_hr' => '1',
						'prd_id' => $productId
					);


					$hr_booking_status = array(
						'booking_status' => 'Pending'
					);

					$hr_dataArr = array_merge($hr_dataArr, $hr_booking_status);
					$hr_enquiryObj = RentalsEnquiry::create($hr_dataArr);
					$hr_insertid = $hr_enquiryObj->id;

					if ($hr_enquiryObj->Bookingno == '' || $hr_enquiryObj->Bookingno == NULL) {
						$val = 10 * $hr_insertid + 8;
						$val = 1500000 + $val;
						$bookingno = "WRL" . $val;
						$hr_newdata = array(
							'Bookingno' => $bookingno
						);
						RentalsEnquiry::where('id', $hr_insertid)->update($hr_newdata);
					}
					$dataArr = array(
						'productId' => $productId,
						'bookingNo' => $bookingno,
						'enqid' => $hr_insertid,
						'senderId' => \Auth::user()->id,
						'receiverId' => $renterId,
						'subject' => 'Booking Request : ' . $bookingno,
						'message' => ""
					);
					MedMessage::create($dataArr);
					$hr_enquiryObj = RentalsEnquiry::where("id", $hr_insertid)->first();
				  
					$availableCheck = $userEnquiries->count();

					$this->sendBookingEmails($availableCheck, $propertyObj, $hr_enquiryObj, "hr", $hr_email, $worktutoremail, $organizationemail);

				} else {
				   
					$dataArr = array(
						'checkin' => $checkIn,
						'checkout' => $checkOut,
						'numofdates' => $noOfNyts,
						'caltophone' => '',
						'enquiry_timezone' => '',
						'user_id' => \Auth::user()->id,
						'renter_id' => $renterId,
						'prd_id' => $productId
					);


					$booking_status = array(
						'booking_status' => 'Pending'
					);

					$dataArr = array_merge($dataArr, $booking_status);
					$enquiryObj = RentalsEnquiry::create($dataArr);
					$insertid = $enquiryObj->id;

					if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
						$val = 10 * $insertid + 8;
						$val = 1500000 + $val;
						$bookingno = "WRL" . $val;
						$newdata = array(
							'Bookingno' => $bookingno
						);
						RentalsEnquiry::where('id', $insertid)->update($newdata);
					}

					$dataArr = array(
						'productId' => $productId,
						'bookingNo' => $bookingno,
						'enqid' => $insertid,
						'senderId' => \Auth::user()->id,
						'receiverId' => $renterId,
						'subject' => 'Booking Request : ' . $bookingno,
						'message' => ""
					);
					MedMessage::create($dataArr);
					$enquiryObj = RentalsEnquiry::where("id", $insertid)->first();
					//checking if user have its first choice within selected dates or not
					$availableCheck = $userEnquiries->count();
					$this->sendBookingEmails($availableCheck, $propertyObj, $enquiryObj, "no_hr", $hr_email, $worktutoremail, $organizationemail);

				}
			} else {
			   
				$dataArr = array(
					'checkin' => $checkIn,
					'checkout' => $checkOut,
					'numofdates' => $noOfNyts,
					'caltophone' => '',
					'enquiry_timezone' => '',
					'user_id' => \Auth::user()->id,
					'renter_id' => $renterId,
					'prd_id' => $productId
				);


				$booking_status = array(
					'booking_status' => 'Pending'
				);

				$dataArr = array_merge($dataArr, $booking_status);
				$enquiryObj = RentalsEnquiry::create($dataArr);
				$insertid = $enquiryObj->id;

				if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
					$val = 10 * $insertid + 8;
					$val = 1500000 + $val;
					$bookingno = "WRL" . $val;
					$newdata = array(
						'Bookingno' => $bookingno
					);
					RentalsEnquiry::where('id', $insertid)->update($newdata);
				}

				$dataArr = array(
					'productId' => $productId,
					'bookingNo' => $bookingno,
					'enqid' => $insertid,
					'senderId' => \Auth::user()->id,
					'receiverId' => $renterId,
					'subject' => 'Booking Request : ' . $bookingno,
					'message' => ""
				);
				MedMessage::create($dataArr);
				$enquiryObj = RentalsEnquiry::where("id", $insertid)->first();
				//checking if user have its first choice within selected dates or not
			  
				$availableCheck = $userEnquiries->count();
				$this->sendBookingEmails($availableCheck, $propertyObj, $enquiryObj, "no_hr", $hr_email, $worktutoremail, $organizationemail);

			}
			 if($noOfNyts==0){
					$noOfNyts = 1;
				}

			$userCredit = new \App\UserCredit();
			$userCredit->user_id = $userId;
			$userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
			$userCredit->property_id = isset($propertyObj->id) ? $propertyObj->id : 0;
			if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {

				$userCredit->send_to_schoolhouse = 1;
			} else {
				$userCredit->send_to_unpaid = 1;
			}
			$userCredit->save();
			return $bookingno;
			
		}
		
		public function sendBookingEmails($availableCheck = 1, $propertyObj = array(), $enquiryObj = array(), $condition = "", $hr_email = "", $worktutoremail = "", $organizationemail = "") {
           
             // if ((!empty($propertyObj) && sizeof($propertyObj)) && (!empty($enquiryObj) && sizeof($enquiryObj))) {
             if ((!empty($propertyObj) && isset($propertyObj->id)) && (!empty($enquiryObj) && isset($enquiryObj->id))) {
                $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();
                $user = User::where("id", \Auth::user()->id)->select("id", "email", "name")->first();
                // checking if the request is for bed .type or not
                if (isset($propertyObj->property_type) && $propertyObj->property_type == "26") {
                    //if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                        //if (isset($renter_user->email) && !empty($renter_user->email)) {
                    $this->mailNotificationsForUser->sendPaidTypeSchoolHouseBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                    $this->mailNotificationsForUser->sendPaidTypeSchoolHouseUserBookingEmail($user->email, $organizationemail, $propertyObj, $user, $enquiryObj);
                       // }
                    //}
                }
                // else for reqular email system which is runnig before / Olfuser  first listing not showing in user-s @your listing@
                else {
                    if (!empty($condition) && $condition == "hr") {
                        if ($availableCheck == 0) {
                            //checking if user have its first choice
                            if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                                //$this->mailNotificationsForUser->sendPaidHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                            } else {


                                $this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $enquiryObj);
                            }
                        } else {
                            if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                            } else {
                              
                                $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$worktutoremail);
                            }
                        }
                    }
                    // second codition
                    else if (!empty($condition) && $condition == "no_hr") {
                        if ($availableCheck == 0) {
                            if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                                if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                    $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                                    //$this->mailNotificationsForUser->sendPaidBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                                } else {
                                    $this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                                }
                            } else {
                                if (isset($propertyObj->renter_id)) {
                                    if (isset($renter_user->email) && !empty($renter_user->email)) {
                                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                            $this->mailNotificationsForUser->sendPaidUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                                        } else {
                                            $this->mailNotificationsForUser->sendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                                        }
                                    }
                                }
                            }
                        } else {
                            if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                                //$this->mailNotificationsForUser->sendPaidBookingNotification($propertyObj, $enquiryObj);
                            } else {
                                // $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj);
                                $requested_user = User::where("id", $enquiryObj->user_id)->select("id", "email", "name")->first();
                                    // $requestedUseremail = $requested_user->email;
                                    $renterEmail = $renter_user->email;
                                    // if checkbox @Work Tutor NOT YOU Flagged the BD value work?org?same will be 0
                                    // echo "<br> Work Org Same :".$propertyObj->work_org_same."<br>";
                                if($propertyObj->work_org_same == 0){
                                    $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$worktutoremail,$renterEmail);
                                    // $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$requestedUseremail);
                                }else{
                                   
                                   // echo "<br> Requested Email ID :".$renterEmail;
                                   // echo "<br> Requesteing Email Id :".$requestedUseremail;
                                   //  $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$requestedUseremail);
                                    $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$renterEmail);
                                }
                            }
                        }
                    }
                }
            }
        }
	}