<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CommissionRepository;
use App\Repositories\CommissionTrackingRepository;
use App\Repositories\CodeRepository;

class CommissionController extends Controller
{
    private $bredCrum = "Commission";
    
    protected $commissionRepository;

    protected $commissionTrackingRepository;

    protected $codeRepository;

    public function __construct(
        CommissionRepository $commissionRepository,
        CodeRepository $codeRepository, CommissionTrackingRepository $commissionTrackingRepository
    ) {
        $this->commissionRepository = $commissionRepository;
         
        $this->codeRepository = $codeRepository;

        $this->commissionTrackingRepository = $commissionTrackingRepository;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->commissionRepository->getAll();
         
        return View('admin.commission.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        $codeArr = $this->codeRepository->getByCodeType(true, 'cancellation');
        
        return View('admin.commission.create', compact('bredCrum', 'codeArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('type' => $request->get('type'),'amount' => $request->get('amount'),'name' => $request->get('name'), 'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->commissionRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/commission/create')->withErrors($validator);
        } else {
            $Obj = $this->commissionRepository->create($postData);
            flash('Commission Added Successfully', 'success');
            return \Redirect('admin/commission');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->commissionRepository->getById($id);
        $codeArr = $this->codeRepository->getByCodeType(true, 'cancellation');
        
        return View('admin.commission.edit', compact('bredCrum', 'editObj', 'codeArr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $postData = array('type' => $request->get('type'),'amount' => $request->get('amount'),'name' => $request->get('name'), 'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->commissionRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/commission/'.$id.'/edit')->withErrors($validator);
        } else {
            $Obj = $this->commissionRepository->update($postData, $id);
            flash('Commission Updated Successfully', 'success');
            return \Redirect('admin/commission');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function showCommissionTracking()
    {
        $bredCrum = "Commission Tracking";
        $trackObj = $this->commissionTrackingRepository->getAll();
        return View('admin.commission.track', compact('trackObj', 'bredCrum'));
    }

    public function getExport()
    {
        $filename = "trackcommission".date('Y-m-d H:i:s');
        \Excel::create($filename, function($excel){
            $sheetName = "trackcommission";
            $excel->sheet($sheetName, function($sheet) {
                $trackObj = $this->commissionTrackingRepository->getAll();
                $sheet->loadView('admin.export.commissiontrack')->with('trackObj', $trackObj);
            });
        })->download('xls');
    }
}
