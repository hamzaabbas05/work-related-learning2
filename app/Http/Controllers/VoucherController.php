<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Voucher;

class VoucherController extends Controller {

    
    private $bredCrum = "Vouchers";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $bredCrum = $this->bredCrum;
        $currentObj = Voucher::with("user")->get();
        //return $currentObj;
        return View('admin.vouchers.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        $bredCrum = $this->bredCrum;
        return View('admin.vouchers.create', compact('bredCrum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $messages = [
            'voucher_code.required' => "Voucher Code cannot be empty",
            'voucher_code.max' => "Voucher Code cannot be greater than 255 characters",
            'voucher_code.unique' => "Voucher Code already exists",
        ];
        
        $this->validate($request, [
            'voucher_code' => 'required|max:255|unique:vouchers',
        ],$messages);
        
        $voucher = new Voucher();
        $voucher->voucher_code = $request->get("voucher_code");
        $voucher->save();
        flash('Voucher Code has been added Successfully', 'success');
        return redirect()->to('admin/voucher');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $bredCrum = $this->bredCrum;
        $editObj = Voucher::find($id);
        return View('admin.vouchers.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $messages = [
            'voucher_code.required' => "Voucher Code cannot be empty",
            'voucher_code.max' => "Voucher Code cannot be greater than 255 characters",
            'voucher_code.unique' => "Voucher Code already exists",
        ];
        
        $this->validate($request, [
            'voucher_code' => 'required|max:255|unique:vouchers,voucher_code,'.$id
        ],$messages);
        
        $voucher = Voucher::find($id);
        $voucher->voucher_code = $request->get("voucher_code");
        $voucher->save();
        flash('Voucher Code has been updated Successfully', 'success');
        return redirect()->to('admin/voucher');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
