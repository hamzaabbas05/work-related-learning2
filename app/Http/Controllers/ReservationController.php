<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ReservationRepository;
use App\Services\Mailer\MailNotificationsForUser;
use App\RentalsEnquiry as RentalsEnquiry;
use App\Evaluation as Evaluation;
use Carbon\Carbon;

class ReservationController extends Controller {

    private $bredCrum = "Reservation Management";
    protected $reservationRepository;
    private $mailNotificationsForUser;

    public function __construct(ReservationRepository $reservationRepository, MailNotificationsForUser $mailNotificationsForUser) {
        $this->reservationRepository = $reservationRepository;
        $this->mailNotificationsForUser = $mailNotificationsForUser;
    }

    public function evaluate($token) {
        $token1 = base64_decode($token);
        $tokenArray = explode(",", $token1);
        $enquiryId = $tokenArray[0];
        $email = $tokenArray[1];
        $enquiryObj = $this->reservationRepository->getByEnquiryId($enquiryId);
        return View('user.evoluation', compact('enquiryObj'));
    }

    public function evaluatedone(Request $request) {
        $enquiryId = $request->get('enquiryId');
        $enquiryObj = $this->reservationRepository->getByEnquiryId($enquiryId);
        
        
        $evaluateError = array('booking_id' => $request->get('enquiryId'), 'user_id' => $enquiryObj->user_id, 'property_id' => $enquiryObj->prd_id, 'renter_id' => $enquiryObj->renter_id, 'mark_1' => $request->get('mark_1'), 'mark_2' => $request->get('mark_2'), 'mark_3' => $request->get('mark_3'), 'mark_4' => $request->get('mark_4'), 'type_experience' => $request->get('intern'), 'further_observe' => $request->get('further_observe'), 'date' => $request->get('date'), 'hours_done' => $request->get('hours_done'), 'evaluate_period' => $request->get('period'));
        $en = array('evaluation_done' => 1);
        RentalsEnquiry::where('id', $enquiryId)->update($en);
        Evaluation::create($evaluateError);
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp . '-' . $enquiryObj->user->student_number . ".pdf";
        $pdf = \PDF::loadView('evoluationpdf', compact('evaluateError', 'enquiryObj'))->save(public_path() . "/pdf/evaluation/$file_name");
        $this->mailNotificationsForUser->sendPDF($enquiryObj->user->school_tutor_email, $file_name);
        flash('Evaluated Successfully', 'success');
        return \Redirect('/');
        //return $pdf->download('invoice.pdf');
    }

    public function sendEmail($enquiryId) {
        $enquiryObj = $this->reservationRepository->getByEnquiryId($enquiryId);
        $udata = array('email_sent' => 1);
        $this->mailNotificationsForUser->sendEvaluationEmail($enquiryObj);
        RentalsEnquiry::where('id', $enquiryId)->update($udata);
        flash('Email Sent Successfully', 'success');
        return \Redirect('admin/reservation');
    }

    public function index() {
        ini_set('memory_limit','512M');

        $bredCrum = $this->bredCrum;
        $reserveObj = $this->reservationRepository->getByStatus('all');
        $status = "all";
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        //     echo"<pre>";
        //     print_r($reserveObj);
        //     exit;
        // }
        return View('admin.reservation.manage', compact('reserveObj', 'bredCrum', 'status'));
    }

    public function getListByStatus($status) { 
        if($status == "Pending"){
            $bredCrum = $this->bredCrum." Pending (Not Old)"; 
        }else{ 
            $bredCrum = $this->bredCrum." ".$status; 
        }        
        $reserveObj = $this->reservationRepository->getByStatus($status);

        return View('admin.reservation.manage', compact('reserveObj', 'bredCrum', 'status'));
    }

    public function export($status) {
        $filename = "reservation" . date('Y-m-d H:i:s');
        \Excel::create($filename, function($excel) use($status) {
            $sheetName = "reservation";
            $excel->sheet($sheetName, function($sheet) use($status) {
                $reserveObj = $this->reservationRepository->getByStatus($status);
                $sheet->loadView('admin.export.reservation')->with('reserveObj', $reserveObj);
            });
        })->download('xls');
    }

}
