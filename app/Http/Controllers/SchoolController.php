<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\SchoolRepository;

class SchoolController extends Controller
{
    private $bredCrum = "schools";
    
    protected $schoolRepository;

    public function __construct(SchoolRepository $schoolRepository)
    {
        $this->schoolRepository = $schoolRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->schoolRepository->getAll();
        return View('admin.school.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        return View('admin.school.create', compact('bredCrum'));
        /*$pdf = \PDF::loadView('admin.school.create', compact('bredCrum'));
        return $pdf->download('invoice.pdf');*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$dob = $request->get('bday')."/".$request->get('bmonth')."/".$request->get('byear');
        $sameaddr = 1;
        if ($request->get('same') == 'on')
        {
            $sameaddr = 0;
        }
        $postData = array('name' => $request->get('name'),
            'school_email' => $request->get('school_email'),
            'legal_address' => $request->get('legal_address'),
            'lstreet_number' => $request->get('lstreet_number'),
            'lroute' => $request->get('lroute'),
            'ladministrative_area_level_1' => $request->get('ladministrative_area_level_1'),
            'llocality' => $request->get('llocality'),
            'lpostal_code' => $request->get('lpostal_code'),
            'lcountry' => $request->get('lcountry'),
            'operative_address' => $request->get('operative_address'),
            'ostreet_number' => $request->get('ostreet_number'),
            'oroute' => $request->get('oroute'),
            'oadministrative_area_level_1' => $request->get('oadministrative_area_level_1'),
            'olocality' => $request->get('olocality'),
            'opostal_code' => $request->get('opostal_code'),
            'ocountry' => $request->get('ocountry'),
            'same_address' => $sameaddr,
            'Mecanographiccode' => $request->get('Mecanographiccode'),
            'phonenumber' => $request->get('phonenumber'),
            'faxnumber' => $request->get('faxnumber'),
            'HeadMaster' => $request->get('HeadMaster'),
            'insurancewithcompany' => $request->get('insurancewithcompany'),
            'noofpolicy' => $request->get('noofpolicy'),
            'noofpolicythirdparty' => $request->get('noofpolicythirdparty'),
            'legalexpcovernumber' => $request->get('legalexpcovernumber'),
            'tutor_name' => $request->get('tutor_name'),//person responsible for Work atSecondary School
            'tutor_pob' => $request->get('tutor_pob'),
            'tutor_surname' => $request->get('tutor_surname'),
            'tutor_pan' => $request->get('tutor_pan'),
            'tutor_email' => $request->get('tutor_email'),
            'tutor_phone' => $request->get('tutor_phone'),
            'tutor_emergency_phone' => $request->get('tutor_emergency_phone'),
            'name' => $request->get('name'),
            'status' => $request->get('status'),
            'insurance_contact_detail' => $request->get('insurance_contact_detail'),
            'insurance_procedure_report' => $request->get('insurance_procedure_report'),
                    );
        $validator = Validator::make($postData, $this->schoolRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/school/create')->withErrors($validator)->withInput();
        } else {
            $propertyTypeObj = $this->schoolRepository->create($postData);
            flash('School Added Successfully', 'success');
            return \Redirect('admin/school');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->schoolRepository->getById($id);
        return View('admin.school.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
        $sameaddr = 1;
        if ($request->get('same') == 'on')
        {
            $sameaddr = 0;
        }
        $postData = array('name' => $request->get('name'),
            'school_email' => $request->get('school_email'),
            'legal_address' => $request->get('legal_address'),
            'lstreet_number' => $request->get('lstreet_number'),
            'lroute' => $request->get('lroute'),
            'ladministrative_area_level_1' => $request->get('ladministrative_area_level_1'),
            'llocality' => $request->get('llocality'),
            'lpostal_code' => $request->get('lpostal_code'),
            'lcountry' => $request->get('lcountry'),
            'operative_address' => $request->get('operative_address'),
            'ostreet_number' => $request->get('ostreet_number'),
            'oroute' => $request->get('oroute'),
            'oadministrative_area_level_1' => $request->get('oadministrative_area_level_1'),
            'olocality' => $request->get('olocality'),
            'opostal_code' => $request->get('opostal_code'),
            'ocountry' => $request->get('ocountry'),
            'same_address' => $sameaddr,
            'Mecanographiccode' => $request->get('Mecanographiccode'),
            'phonenumber' => $request->get('phonenumber'),
            'faxnumber' => $request->get('faxnumber'),
            'HeadMaster' => $request->get('HeadMaster'),
            'insurancewithcompany' => $request->get('insurancewithcompany'),
            'noofpolicy' => $request->get('noofpolicy'),
            'noofpolicythirdparty' => $request->get('noofpolicythirdparty'),
            'legalexpcovernumber' => $request->get('legalexpcovernumber'),
            'tutor_name' => $request->get('tutor_name'),//person responsible for Work atSecondary School
            'tutor_pob' => $request->get('tutor_pob'),
            'tutor_surname' => $request->get('tutor_surname'),
            'tutor_pan' => $request->get('tutor_pan'),
            'tutor_email' => $request->get('tutor_email'),
            'tutor_phone' => $request->get('tutor_phone'),
            'tutor_emergency_phone' => $request->get('tutor_emergency_phone'),
            'name' => $request->get('name'),
            'status' => $request->get('status'),
            'insurance_contact_detail' => $request->get('insurance_contact_detail'),
            'insurance_procedure_report' => $request->get('insurance_procedure_report'),
                    );
        $validator = Validator::make($postData, $this->schoolRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/school/'.$id.'/edit')->withErrors($validator);
        } else {
            $propertyTypeObj = $this->schoolRepository->update($postData, $id);   
            flash('School Updated Successfully', 'success');
            return \Redirect('admin/school');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
