<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GroupManagementController extends Controller {

    private $bredCrum = "Group Management";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $bredCrum = $this->bredCrum;
        //$users_info = \App\User::with("user_booking", "school_info")->select("id", "name", "lastname", "school_name", "dob", "dob16", "gender", "class_letter", "class_number", "phonenumber", "email", "about_yourself")->get();
        $users_info = \App\RentalsEnquiry::where("checkin", ">=", date("Y-m-d"))->orderBy('dateAdded', 'ASC')->get();
        $groups = \App\SecondarySchoolGroups::where(["is_delete" => "0"])->get();
//         $users_info = \App\RentalsEnquiry::where("checkin", ">=", "2018-07-27")->orderBy('dateAdded', 'ASC')->get();
        // return $users_info;


        return View('admin.group_management.manage', compact('bredCrum', 'users_info', 'groups'));
    }

    public function groupexport() {

        // $users_info = \App\RentalsEnquiry::with("user_detail", "property_detail")->where("checkin", ">=", date("Y-m-d"))->where('user_id',2860)->orderBy('checkin', 'ASC')->paginate('10');
        //return $users_info;
        // $users_info = \App\RentalsEnquiry::where("checkin", ">=", date("Y-m-d"))->where('user_id',2860)->orderBy('dateAdded', 'ASC')->get();
        $filename = "Group_Management_" . date('Y-m-d H:i:s');
        \Excel::create($filename, function($excel) {
            $sheetName = "Group_Management";
            $excel->sheet($sheetName, function($sheet) {
                //$users_info = \App\User::with("user_booking", "school_info")->get();
                // $users_info = \App\RentalsEnquiry::where("checkin", ">=", date("Y-m-d"))->orderBy('dateAdded', 'ASC')->get();
                
                $users_info = \App\RentalsEnquiry::with("user_detail", "property_detail")->where("checkin", ">=", date("Y-m-d"))->orderBy('checkin', 'ASC')->get();//->paginate('10');
                //->where('user_id',2860)
//                $users_info = \App\RentalsEnquiry::where("checkin", ">=", "2018-07-27")->orderBy('dateAdded', 'ASC')->get();
                $sheet->loadView('admin.export.groupmanagement')->with('users_info', $users_info);
            });
        })->download('xls');
    }


     public function allstudentexport() {

        // $userObj =  \App\User::with('group_info')->orderBy('created_at','ASC')->get();
        // foreach($userObj as $obj){
        // echo "<pre>";
        //     print_r($obj);
        // echo "</pre>";
            
        // }
        //     exit;
        $filename = "Group_Management_all_list_" . date('Y-m-d H:i:s');
         \Excel::create($filename, function($excel) {
            $sheetName = "user";
           $excel->sheet($sheetName, function($sheet){
                // $userObj = \App\Role::find($roleId)->users()->get();
                $userObj =  \App\User::with('group_info')->orderBy('created_at','ASC')->get();
                // $userObj = \App\User::orderBy('created_at', 'ASC')->get();
                // $sheet->loadView('admin.export.user')->with('userObj', $userObj)->with('roleId', $roleId);
                $sheet->loadView('admin.export.userall')->with('userObj', $userObj);//->with('roleId', $roleId);
                // return View('admin.export.userall')->with('userObj', $userObj);//->with('roleId', $roleId);
           });
        })->download('xls');
        
    }

     public function userlimitedinfo() {
        
        $filename = "Group_Management_limited_list_" . date('Y-m-d H:i:s');
         \Excel::create($filename, function($excel) {
            $sheetName = "user";
           $excel->sheet($sheetName, function($sheet){
                // $userObj = \App\Role::find($roleId)->users()->get();
                $userObj =  \App\User::with('group_info')->orderBy('created_at','ASC')->get();
                
                $sheet->loadView('admin.export.userlimitedinfo')->with('userObj', $userObj);//->with('roleId', $roleId);
                // return View('admin.export.userlimitedinfo')->with('userObj', $userObj);//->with('roleId', $roleId);
           });
        })->download('xls');
    }

    public function is_confirm_group_export(Request $request) {
//        return $request->all();
        $filename = "Group_Management_" . date('Y-m-d H:i:s');
//        return $request->all();
        $group_id = $request->get("group_id");
        \Excel::create($filename, function($excel) use ($request) {
            $sheetName = "Group_Management";
            $excel->sheet($sheetName, function($sheet) use ($request) {
                if (!empty($request->get("group_id")) && $request->get("group_id") != "no_group") {
                    $users_info = \App\RentalsEnquiry::select("fc_rentalsenquiry.*", "users.email")
                                    ->join("users", "users.id", "=", "fc_rentalsenquiry.user_id")
                                    ->where("users.secondary_group_id", $request->get("group_id"))
                                    ->where("fc_rentalsenquiry.checkin", ">=", date("Y-m-d"))
                                    ->where("fc_rentalsenquiry.approval", "Accept")
                                    ->orderBy('users.email', 'ASC')->get();
                } else if (!empty($request->get("group_id")) && $request->get("group_id") == "no_group") {
                    $users_info = \App\RentalsEnquiry::select("fc_rentalsenquiry.*", "users.email")
                                    ->join("users", "users.id", "=", "fc_rentalsenquiry.user_id")
                                    ->where("users.secondary_group_id", "0")
                                    ->where("fc_rentalsenquiry.checkin", ">=", date("Y-m-d"))
                                    ->where("fc_rentalsenquiry.approval", "Accept")
                                    ->orderBy('users.email', 'ASC')->get();
                } else {
                    $users_info = \App\RentalsEnquiry::select("fc_rentalsenquiry.*", "users.email")
                                    ->join("users", "users.id", "=", "fc_rentalsenquiry.user_id")
                                    ->where("fc_rentalsenquiry.checkin", ">=", date("Y-m-d"))
                                    ->where("fc_rentalsenquiry.approval", "Accept")
                                    ->orderBy('users.email', 'ASC')->get();
//                    $users_info = \App\RentalsEnquiry::where("checkin", ">=", date("Y-m-d"))->orderBy('dateAdded', 'ASC')->get();
                }
                $sheet->loadView('admin.export.isConfirmedGroupExport')->with('users_info', $users_info);
            });
        })->download('xls');
    }

    public function workPlaceSchoolHouseExport() {
        //$users_info = \App\RentalsEnquiry::with("user_detail", "property_detail")->where("checkin", ">=", date("Y-m-d"))->orderBy('checkin', 'ASC')->paginate('20');
//        echo "<pre>";
//        print_r($users_info);
//        exit;
        //return $users_info;
        $filename = "Group_Management_" . date('Y-m-d H:i:s');
        \Excel::create($filename, function($excel) {
            $sheetName = "Group_Management";
            $excel->sheet($sheetName, function($sheet) {
                //$users_info = \App\User::with("user_booking", "school_info")->get();
                $users_info = \App\RentalsEnquiry::where("checkin", ">=", date("Y-m-d"))->where("approval", "Accept")->orderBy('dateAdded', 'ASC')->get();
//                $users_info = \App\RentalsEnquiry::where("checkin", ">=", "2018-07-27")->orderBy('dateAdded', 'ASC')->get();
                $sheet->loadView('admin.export.workPlaceSchoolHouseExport')->with('users_info', $users_info);
            });
        })->download('xls');
    }

    public function getLatLong($address) {
        //$address = "India+Panchkula";
        if ($address != "") {
            $address = str_replace(" ", "+", $address);
            $address = str_replace(",", "", $address);
            $url = "https://maps.google.com/maps/api/geocode/json?key=AIzaSyB11l5QoR0jEa8h50ZLoVf-P_1kLU4sCgQ&address=$address&sensor=false";
            //echo $url; exit;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            //curl_setopt($ch, CURLOPT_TIMEOUT_MS, 120000); //in miliseconds
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response);
            return $response_a;
        }
        //echo $lat = $response_a->results[0]->geometry->location->lat;
        //echo "<br />";
        //echo $long = $response_a->results[0]->geometry->location->lng;
    }

    public function testExcel() {
        //Employee with regular contract
        $result = \Excel::load('excel/Upload Work Places.xls', function($reader) {
                    
                })->toArray();

//        echo "<pre>";
//        print_r($result);
//        echo "</pre>";
//        exit;
        //return $result;
        $user_array = array();
        $listing_array = array();
        if (sizeof($result) > 0) {

            $count = 0;
            foreach ($result as $res) {
                foreach ($res as $key => $user) {
                    $count++;
                    if ($count > 80) {
                        break;
                    }
                    //return $user;
                    $user_id = 0;
                    $user_email = "";
                    if (isset($user["email"]) && !empty($user["email"])) {
                        $user_email = $user["email"];
                    } elseif (isset($user["orgemail"]) && !empty($user["orgemail"])) {
                        $user_email = $user["orgemail"];
                    }
                    if (!empty($user_email)) {
                        $check_user = \App\User::where("email", $user_email)->first();
                        $legal_address = isset($user["orgaddress"]) ? $user["orgaddress"] : "";
                        $lat = "";
                        $long = "";
                        $gmap_address = array();
                        if (!empty($legal_address)) {
                            $address_result = $this->getLatLong($legal_address);
                            if (isset($address_result->results[0]->geometry->location->lat)) {
                                $lat = $address_result->results[0]->geometry->location->lat;
                            }
                            if (isset($address_result->results[0]->geometry->location->lng)) {
                                $long = $address_result->results[0]->geometry->location->lng;
                            }

                            if (isset($address_result->results[0]->address_components)) {
                                foreach ($address_result->results[0]->address_components as $g_address) {
                                    if (isset($g_address->types[0]) && $g_address->types[0] == "street_number") {
                                        $gmap_address["street_numberr18"] = isset($g_address->long_name) ? $g_address->long_name : "";
                                    } elseif (isset($g_address->types[0]) && $g_address->types[0] == "route") {
                                        $gmap_address["router18"] = isset($g_address->long_name) ? $g_address->long_name : "";
                                    } elseif (isset($g_address->types[0]) && $g_address->types[0] == "postal_town") {
                                        $gmap_address["localityr18"] = isset($g_address->long_name) ? $g_address->long_name : "";
                                    } elseif (isset($g_address->types[0]) && $g_address->types[0] == "administrative_area_level_2") {
                                        $gmap_address["administrative_area_level_1r18"] = isset($g_address->long_name) ? $g_address->long_name : "";
                                    } elseif (isset($g_address->types[0]) && $g_address->types[0] == "postal_code") {
                                        $gmap_address["postal_coder18"] = isset($g_address->long_name) ? $g_address->long_name : "";
                                    } elseif (isset($g_address->types[0]) && $g_address->types[0] == "administrative_area_level_1") {
                                        $gmap_address["countryr18"] = isset($g_address->long_name) ? $g_address->long_name : "";
                                    }
                                }
                            }
                        }
                        $country_id = 0;
                        if (isset($user["country_id"])) {
                            $country = \App\Country::where("name", $user["country_id"])->first();
                            $country_id = isset($country->id) ? $country->id : "0";
                        }
                        $legal_nature = '';
                        if (isset($user["legal_nature"])) {
                            if ($user["legal_nature"] == "Private") {
                                $legal_nature = 0;
                            } elseif ($user["legal_nature"] == "Public") {
                                $legal_nature = 1;
                            }
                        }
                        $hr_id = 0;
                        if (isset($user["orghr"])) {
                            $hr_detail = \App\RoomType::where("name", $user["orghr"])->first();
                            $hr_id = isset($hr_detail->id) ? $hr_detail->id : "0";
                        }
                        $tutor_relations_id = 0;
                        if (isset($user["tutorrelation"])) {
                            $tutor_relations = \App\Tutor::where("name", $user["tutorrelation"])->first();
                            $tutor_relations_id = isset($tutor_relations->id) ? $tutor_relations->id : "0";
                        }
                        $property_type = 0;
                        if (isset($user["workcategory"])) {
                            $type = \App\PropertyType::where("name", $user["workcategory"])->first();
                            $property_type = isset($type->id) ? $type->id : "0";
                        }
                        $human_resource = 1;
                        if (isset($user["checkhumanid"]) && $user["checkhumanid"] == "y") {
                            $human_resource = 0;
                        }
                        if (isset($check_user->id) && $check_user->id > 0) {
                            $user_id = $check_user->id;
//                            $check_user->name = (isset($user["name"]) and ! empty($user["name"]) ) ? $user["name"] : $check_user->name;
//                            $check_user->lastname = (isset($user["lastname"]) and ! empty($user["lastname"]) ) ? $user["lastname"] : $check_user->lastname;
//                            $check_user->gender = (isset($user["gender"]) and ! empty($user["gender"]) ) ? $user["gender"] : $check_user->gender;
//                            $check_user->dob = (isset($check_user->dob) and ! empty($check_user->dob) ) ? $check_user->dob : "1/1/1001";
//                            $check_user->basicpob = (isset($user["basicpob"]) and ! empty($user["basicpob"]) ) ? $user["basicpob"] : $check_user->basicpob;
//                            $check_user->country_id = (isset($check_user->country_id) and ! empty($check_user->country_id) ) ? $check_user->country_id : $country_id;
//                            $check_user->phonenumber = (isset($user["phonenumber"]) and ! empty($user["phonenumber"]) ) ? $user["phonenumber"] : $check_user->phonenumber;
//                            $check_user->NIN18 = (isset($user["nin18"]) and ! empty($user["nin18"]) ) ? $user["nin18"] : $check_user->nin18;
//                            $check_user->organisation_tax_registration_number = (isset($user["organisation_tax_registration_number"]) and ! empty($user["organisation_tax_registration_number"]) ) ? $user["organisation_tax_registration_number"] : $check_user->organisation_tax_registration_number;
//                            $check_user->orgname = (isset($user["orgname"]) and ! empty($user["orgname"]) ) ? $user["orgname"] : $check_user->orgname;
//                            $check_user->aboutorg = (isset($user["aboutorg"]) and ! empty($user["aboutorg"]) ) ? $user["aboutorg"] : $check_user->aboutorg;
//                            $check_user->legal_nature = (isset($legal_nature) and ! empty($legal_nature) ) ? $legal_nature : $check_user->legal_nature;
//                            $check_user->orghr = (isset($hr_id) and ( $hr_id > 0)) ? $hr_id : $check_user->orghr;
//                            $check_user->orgemail = (isset($user["orgemail"]) and ! empty($user["orgemail"]) ) ? $user["orgemail"] : $check_user->orgemail;
//                            $check_user->orgphone = (isset($user["orgphone"]) and ! empty($user["orgphone"]) ) ? $user["orgphone"] : $check_user->orgphone;
//                            $check_user->raddress18 = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_user->raddress18;
//                            $check_user->tutorrelation = (isset($tutor_relations_id) and ( $tutor_relations_id > 0)) ? $tutor_relations_id : $check_user->tutorrelation;
//                            $check_user->why_fit = (isset($user["why_fit"]) and ! empty($user["why_fit"]) ) ? $user["why_fit"] : $check_user->why_fit;
//                            $check_user->emergency_contact_name = (isset($user["emergency_contact_name"]) and ! empty($user["emergency_contact_name"]) ) ? $user["emergency_contact_name"] : $check_user->emergency_contact_name;
//                            $check_user->emergency_contact_phone = (isset($user["emergency_contact_phone"]) and ! empty($user["emergency_contact_phone"]) ) ? $user["emergency_contact_phone"] : $check_user->emergency_contact_phone;
//                            $check_user->emergency_contact_email = (isset($user["emergency_contact_email"]) and ! empty($user["emergency_contact_email"]) ) ? $user["emergency_contact_email"] : $check_user->emergency_contact_email;
//                            $check_user->emergency_contact_relationship = (isset($user["emergency_contact_relationship"]) and ! empty($user["emergency_contact_relationship"]) ) ? $user["emergency_contact_relationship"] : $check_user->emergency_contact_relationship;
//                            $check_user->router18 = (isset($gmap_address["router18"]) and ! empty($gmap_address["router18"]) ) ? $gmap_address["router18"] : $check_user->router18;
//                            $check_user->street_numberr18 = (isset($gmap_address["street_numberr18"]) and ! empty($gmap_address["street_numberr18"]) ) ? $gmap_address["street_numberr18"] : $check_user->street_numberr18;
//                            $check_user->localityr18 = (isset($gmap_address["localityr18"]) and ! empty($gmap_address["localityr18"]) ) ? $gmap_address["localityr18"] : $check_user->localityr18;
//                            $check_user->administrative_area_level_1r18 = (isset($gmap_address["administrative_area_level_1r18"]) and ! empty($gmap_address["administrative_area_level_1r18"]) ) ? $gmap_address["administrative_area_level_1r18"] : $check_user->administrative_area_level_1r18;
//                            $check_user->postal_coder18 = (isset($gmap_address["postal_coder18"]) and ! empty($gmap_address["postal_coder18"]) ) ? $gmap_address["postal_coder18"] : $check_user->postal_coder18;
//                            $check_user->countryr18 = (isset($gmap_address["countryr18"]) and ! empty($gmap_address["countryr18"]) ) ? $gmap_address["countryr18"] : $check_user->countryr18;
//                            $check_user->save();
                            echo $key . " => User has been update = " . $check_user->id . "<br />";
                            $listing_array["user"] = $check_user;

                            //Listing variables
                            if (isset($user["exptitle"]) && !empty($user["exptitle"])) {
                                $check_property = \App\Property::where("exptitle", $user["exptitle"])->first();
                                if (isset($check_property->id) && $check_property->id > 0) {
                                    $check_property->user_id = $check_user->id;
//                                    $check_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : $check_property->exptitle;
//                                    $check_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : $check_property->description;
//                                    $check_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : $check_property->optional_description;
//                                    $check_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : $check_property->houserules;
//                                    $check_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : $check_property->work_schedule;
//                                    $check_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : $check_property->work_hours;
//                                    $check_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : $check_property->link_to_extra_docs_necessary;
//                                    $check_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : $check_property->hr_represent_name;
//                                    $check_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : $check_property->hr_represent_surname;
//                                    $check_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : $check_property->hr_represent_email;
//                                    $check_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : $check_property->hr_represent_phone;
//                                    $check_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : $check_property->pro_lat;
//                                    $check_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : $check_property->pro_lon;
//                                    $check_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : $check_property->property_type;
//                                    $check_property->human_resources = (isset($human_resource)) ? $human_resource : $check_property->human_resources;
                                    //$check_property->status = "1";
                                    //$check_property->property_status = "unpaidflash";
                                    $check_property->save();
                                    echo $key . " => Property has been update = " . $check_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $check_property;
                                } else {
                                    $new_property = new \App\Property();
                                    $new_property->user_id = (isset($user_id) and ! empty($user_id) ) ? $user_id : "";
//                                    $new_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : "";
//                                    $new_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : "";
//                                    $new_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : "";
//                                    $new_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : "";
//                                    $new_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : "";
//                                    $new_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : "";
//                                    $new_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : "";
//                                    $new_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : "";
//                                    $new_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : "";
//                                    $new_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : "";
//                                    $new_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : "";
//                                    $new_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : "";
//                                    $new_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : "";
//                                    $new_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : "";
//                                    $new_property->human_resources = (isset($human_resource)) ? $human_resource : "";
//                                    $new_property->status = "1";
//                                    $new_property->property_status = "unpaidflash";
                                    $new_property->save();
                                    echo $key . " => New Property has been added = " . $new_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $new_property;
                                }
                            }
                        } else {
                            //echo bcrypt('f34f4f'); exit;
                            $new_user = new \App\User();
                            $new_user->email = $user_email;
                            $new_user->password = bcrypt('f34f4f');
                            $new_user->name = (isset($user["name"]) and ! empty($user["name"]) ) ? $user["name"] : "";
                            $new_user->lastname = (isset($user["lastname"]) and ! empty($user["lastname"]) ) ? $user["lastname"] : "";
                            $new_user->gender = (isset($user["gender"]) and ! empty($user["gender"]) ) ? $user["gender"] : "";
                            $new_user->dob = "1/1/1001";
                            $new_user->basicpob = (isset($user["basicpob"]) and ! empty($user["basicpob"]) ) ? $user["basicpob"] : "";
                            $new_user->country_id = $country_id;
                            $new_user->phonenumber = (isset($user["phonenumber"]) and ! empty($user["phonenumber"]) ) ? $user["phonenumber"] : "";
                            $new_user->NIN18 = (isset($user["nin18"]) and ! empty($user["nin18"]) ) ? $user["nin18"] : "";
                            $new_user->organisation_tax_registration_number = (isset($user["organisation_tax_registration_number"]) and ! empty($user["organisation_tax_registration_number"]) ) ? $user["organisation_tax_registration_number"] : "";
                            $new_user->orgname = (isset($user["orgname"]) and ! empty($user["orgname"]) ) ? $user["orgname"] : "";
                            $new_user->aboutorg = (isset($user["aboutorg"]) and ! empty($user["aboutorg"]) ) ? $user["aboutorg"] : "";
                            $new_user->legal_nature = (isset($legal_nature) and ! empty($legal_nature) ) ? $legal_nature : "";
                            $new_user->orghr = $hr_id;
                            $new_user->orgemail = (isset($user["orgemail"]) and ! empty($user["orgemail"]) ) ? $user["orgemail"] : "";
                            $new_user->orgphone = (isset($user["orgphone"]) and ! empty($user["orgphone"]) ) ? $user["orgphone"] : "";
                            $new_user->raddress18 = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : "";
                            $new_user->tutorrelation = (isset($tutor_relations_id) and ( $tutor_relations_id > 0)) ? $tutor_relations_id : "";
                            $new_user->why_fit = (isset($user["why_fit"]) and ! empty($user["why_fit"]) ) ? $user["why_fit"] : "";
                            $new_user->emergency_contact_name = (isset($user["emergency_contact_name"]) and ! empty($user["emergency_contact_name"]) ) ? $user["emergency_contact_name"] : "";
                            $new_user->emergency_contact_phone = (isset($user["emergency_contact_phone"]) and ! empty($user["emergency_contact_phone"]) ) ? $user["emergency_contact_phone"] : "";
                            $new_user->emergency_contact_email = (isset($user["emergency_contact_email"]) and ! empty($user["emergency_contact_email"]) ) ? $user["emergency_contact_email"] : "";
                            $new_user->emergency_contact_relationship = (isset($user["emergency_contact_relationship"]) and ! empty($user["emergency_contact_relationship"]) ) ? $user["emergency_contact_relationship"] : "";
                            $new_user->router18 = (isset($gmap_address["router18"]) and ! empty($gmap_address["router18"]) ) ? $gmap_address["router18"] : "";
                            $new_user->street_numberr18 = (isset($gmap_address["street_numberr18"]) and ! empty($gmap_address["street_numberr18"]) ) ? $gmap_address["street_numberr18"] : "";
                            $new_user->localityr18 = (isset($gmap_address["localityr18"]) and ! empty($gmap_address["localityr18"]) ) ? $gmap_address["localityr18"] : "";
                            $new_user->administrative_area_level_1r18 = (isset($gmap_address["administrative_area_level_1r18"]) and ! empty($gmap_address["administrative_area_level_1r18"]) ) ? $gmap_address["administrative_area_level_1r18"] : "";
                            $new_user->postal_coder18 = (isset($gmap_address["postal_coder18"]) and ! empty($gmap_address["postal_coder18"]) ) ? $gmap_address["postal_coder18"] : "";
                            $new_user->countryr18 = (isset($gmap_address["countryr18"]) and ! empty($gmap_address["countryr18"]) ) ? $gmap_address["countryr18"] : "";
                            $new_user->save();
                            echo $key . " => New User has been added = " . $new_user->id . "<br />";
                            $listing_array["user"] = $new_user;
                            if ($new_user->id) {
                                $check_property = \App\Property::where("exptitle", $user["exptitle"])->first();
                                if (isset($check_property->id) && $check_property->id > 0) {
                                    $check_property->user_id = $new_user->id;
                                    $check_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : $check_property->exptitle;
                                    $check_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : $check_property->description;
                                    $check_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : $check_property->optional_description;
                                    $check_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : $check_property->houserules;
                                    $check_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : $check_property->work_schedule;
                                    $check_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : $check_property->work_hours;
                                    $check_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : $check_property->link_to_extra_docs_necessary;
                                    $check_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : $check_property->hr_represent_name;
                                    $check_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : $check_property->hr_represent_surname;
                                    $check_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : $check_property->hr_represent_email;
                                    $check_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : $check_property->hr_represent_phone;
                                    $check_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : $check_property->pro_lat;
                                    $check_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : $check_property->pro_lon;
                                    $check_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : $check_property->property_type;
                                    $check_property->human_resources = (isset($human_resource)) ? $human_resource : $check_property->human_resources;
                                    //$check_property->status = "1";
                                    //$check_property->property_status = "unpaidflash";
                                    $check_property->save();
                                    echo $key . " => Property has been update = " . $check_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $check_property;
                                } else {
                                    $user_id = isset($new_user->id) ? $new_user->id : "0";
                                    $new_property = new \App\Property();
                                    $new_property->user_id = (isset($user_id) and ! empty($user_id) ) ? $user_id : "";
                                    $new_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : "";
                                    $new_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : "";
                                    $new_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : "";
                                    $new_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : "";
                                    $new_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : "";
                                    $new_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : "";
                                    $new_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : "";
                                    $new_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : "";
                                    $new_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : "";
                                    $new_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : "";
                                    $new_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : "";
                                    $new_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : "";
                                    $new_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : "";
                                    $new_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : "";
                                    $new_property->human_resources = (isset($human_resource)) ? $human_resource : "";
                                    $new_property->status = "1";
                                    $new_property->property_status = "unpaidflash";
                                    $new_property->save();
                                    echo $key . " => New Property has been added = " . $new_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $new_property;
                                }
                            }
                        }
                        $user_array[] = $listing_array;
                    }
                }
            }
        }
        //return $user_array;
        exit;
        //last user id = 1945
        //last property id = 107;
    }

    public function schoolHousesExcel() {
        //Employee with regular contract
        $result = \Excel::load('excel/school_houses_new.xls', function($reader) {
                    
                })->toArray();

//        echo "<pre>";
//        print_r($result);
//        echo "</pre>";
//        exit;
//        return $result;
        $user_array = array();
        $listing_array = array();
        if (sizeof($result) > 0) {
            $count = 0;
            foreach ($result as $res) {
                foreach ($res as $user) {
                    $count++;
                    if ($count > 71) {
                        break;
                    }
                    //return $user;
                    $user_id = 0;
                    $user_email = "";
                    if (isset($user["email"]) && !empty($user["email"])) {
                        $user_email = $user["email"];
                    } elseif (isset($user["orgemail"]) && !empty($user["orgemail"])) {
                        $user_email = $user["orgemail"];
                    }
                    if (!empty($user_email)) {
                        $check_user = \App\User::where("email", $user_email)->first();
                        $legal_address = isset($user["orgaddress"]) ? $user["orgaddress"] : "";
                        $lat = "";
                        $long = "";
                        if (!empty($legal_address)) {
                            $address_result = $this->getLatLong($legal_address);
                            if (isset($address_result->results[0]->geometry->location->lat)) {
                                $lat = $address_result->results[0]->geometry->location->lat;
                            }
                            if (isset($address_result->results[0]->geometry->location->lng)) {
                                $long = $address_result->results[0]->geometry->location->lng;
                            }
                        }
                        $country_id = 0;
                        if (isset($user["country_id"])) {
                            $country = \App\Country::where("name", $user["country_id"])->first();
                            $country_id = isset($country->id) ? $country->id : "0";
                        }
                        $legal_nature = '';
                        if (isset($user["legal_nature"])) {
                            if ($user["legal_nature"] == "Private") {
                                $legal_nature = 0;
                            } elseif ($user["legal_nature"] == "Public") {
                                $legal_nature = 1;
                            }
                        }
                        $hr_id = 0;
                        if (isset($user["orghr"])) {
                            $hr_detail = \App\RoomType::where("name", $user["orghr"])->first();
                            $hr_id = isset($hr_detail->id) ? $hr_detail->id : "0";
                        }
                        $tutor_relations_id = 0;
                        if (isset($user["tutorrelation"])) {
                            $tutor_relations = \App\Tutor::where("name", $user["tutorrelation"])->first();
                            $tutor_relations_id = isset($tutor_relations->id) ? $tutor_relations->id : "0";
                        }
                        $property_type = 0;
                        if (isset($user["workcategory"])) {
                            $type = \App\PropertyType::where("name", $user["workcategory"])->first();
                            $property_type = isset($type->id) ? $type->id : "0";
                        }
                        $human_resource = 1;
                        if (isset($user["checkhumanid"]) && $user["checkhumanid"] == "y") {
                            $human_resource = 0; //so unchecked is 1, therefore user has not specified different HR IF 1, else 0 means there is different HR. 
                        }
                        if (isset($check_user->id) && $check_user->id > 0) {
                            $user_id = $check_user->id;
                            $check_user->name = (isset($user["name"]) and ! empty($user["name"]) ) ? $user["name"] : $check_user->name;
                            $check_user->lastname = (isset($user["lastname"]) and ! empty($user["lastname"]) ) ? $user["lastname"] : $check_user->lastname;
                            $check_user->gender = (isset($user["gender"]) and ! empty($user["gender"]) ) ? $user["gender"] : $check_user->gender;
                            $check_user->dob = (isset($check_user->dob) and ! empty($check_user->dob) ) ? $check_user->dob : "1/1/1001";
                            $check_user->basicpob = (isset($user["basicpob"]) and ! empty($user["basicpob"]) ) ? $user["basicpob"] : $check_user->basicpob;
                            $check_user->country_id = (isset($check_user->country_id) and ! empty($check_user->country_id) ) ? $check_user->country_id : $country_id;
                            $check_user->phonenumber = (isset($user["phonenumber"]) and ! empty($user["phonenumber"]) ) ? $user["phonenumber"] : $check_user->phonenumber;
                            $check_user->NIN18 = (isset($user["nin18"]) and ! empty($user["nin18"]) ) ? $user["nin18"] : $check_user->nin18;
                            $check_user->organisation_tax_registration_number = (isset($user["organisation_tax_registration_number"]) and ! empty($user["organisation_tax_registration_number"]) ) ? $user["organisation_tax_registration_number"] : $check_user->organisation_tax_registration_number;
                            $check_user->orgname = (isset($user["orgname"]) and ! empty($user["orgname"]) ) ? $user["orgname"] : $check_user->orgname;
                            $check_user->aboutorg = (isset($user["aboutorg"]) and ! empty($user["aboutorg"]) ) ? $user["aboutorg"] : $check_user->aboutorg;
                            $check_user->legal_nature = (isset($legal_nature) and ! empty($legal_nature) ) ? $legal_nature : $check_user->legal_nature;
                            $check_user->orghr = (isset($hr_id) and ( $hr_id > 0)) ? $hr_id : $check_user->orghr;
                            $check_user->orgemail = (isset($user["orgemail"]) and ! empty($user["orgemail"]) ) ? $user["orgemail"] : $check_user->orgemail;
                            $check_user->orgphone = (isset($user["orgphone"]) and ! empty($user["orgphone"]) ) ? $user["orgphone"] : $check_user->orgphone;
                            // this si the orgaddress from excel upload to be stored in raddress18, 
                            // so this same address will store in houseaddress in user_paid_service table

                            $check_user->raddress18 = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_user->raddress18;
                            $check_user->tutorrelation = (isset($tutor_relations_id) and ( $tutor_relations_id > 0)) ? $tutor_relations_id : $check_user->tutorrelation;
                            $check_user->why_fit = (isset($user["why_fit"]) and ! empty($user["why_fit"]) ) ? $user["why_fit"] : $check_user->why_fit;
                            $check_user->emergency_contact_name = (isset($user["emergency_contact_name"]) and ! empty($user["emergency_contact_name"]) ) ? $user["emergency_contact_name"] : $check_user->emergency_contact_name;
                            $check_user->emergency_contact_phone = (isset($user["emergency_contact_phone"]) and ! empty($user["emergency_contact_phone"]) ) ? $user["emergency_contact_phone"] : $check_user->emergency_contact_phone;
                            $check_user->emergency_contact_email = (isset($user["emergency_contact_email"]) and ! empty($user["emergency_contact_email"]) ) ? $user["emergency_contact_email"] : $check_user->emergency_contact_email;
                            $check_user->emergency_contact_relationship = (isset($user["emergency_contact_relationship"]) and ! empty($user["emergency_contact_relationship"]) ) ? $user["emergency_contact_relationship"] : $check_user->emergency_contact_relationship;
                            $check_user->save();
                            echo "User has been update = " . $check_user->id . "<br />";
                            $listing_array["user"] = $check_user;
                            $user_paid_service = \App\UserPaidService::where("user_id", $user_id)->first();
                            if (!isset($user_paid_service->id)) {
                                $user_paid_service = new \App\UserPaidService();
                            }
                            $user_paid_service->user_id = $user_id;
                            $user_paid_service->houseaddress = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_user->raddress18;
                            $user_paid_service->houselatbox = (isset($lat) and ! empty($lat) ) ? $lat : "";
                            $user_paid_service->houselonbox = (isset($long) and ! empty($long) ) ? $long : "";
                            $user_paid_service->save();
                            //Listing variables
                            if (isset($user["exptitle"]) && !empty($user["exptitle"])) {
                                $check_property = \App\Property::where("exptitle", $user["exptitle"])->first();
                                if (isset($check_property->id) && $check_property->id > 0) {
                                    $check_property->user_id = $check_user->id;
                                    $check_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : $check_property->exptitle;
                                    $check_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : $check_property->description;
                                    $check_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : $check_property->optional_description;
                                    $check_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : $check_property->houserules;
                                    $check_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : $check_property->work_schedule;
                                    $check_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : $check_property->work_hours;
                                    $check_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : $check_property->link_to_extra_docs_necessary;
                                    $check_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : $check_property->hr_represent_name;
                                    $check_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : $check_property->hr_represent_surname;
                                    $check_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : $check_property->hr_represent_email;
                                    $check_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : $check_property->hr_represent_phone;
                                    $check_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : $check_property->pro_lat;
                                    $check_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : $check_property->pro_lon;
                                    $check_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : $check_property->property_type;
                                    $check_property->human_resources = (isset($human_resource)) ? $human_resource : $check_property->human_resources;
                                    $check_property->status = "2";
                                    $check_property->property_status = "paid";
                                    $check_property->legal_address = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_property->legal_address;
                                    $check_property->save();
                                    echo "Property has been update = " . $check_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $check_property;
                                } else {
                                    $new_property = new \App\Property();
                                    $new_property->user_id = (isset($user_id) and ! empty($user_id) ) ? $user_id : "";
                                    $new_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : "";
                                    $new_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : "";
                                    $new_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : "";
                                    $new_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : "";
                                    $new_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : "";
                                    $new_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : "";
                                    $new_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : "";
                                    $new_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : "";
                                    $new_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : "";
                                    $new_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : "";
                                    $new_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : "";
                                    $new_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : "";
                                    $new_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : "";
                                    $new_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : "";
                                    $new_property->human_resources = (isset($human_resource)) ? $human_resource : "";
                                    $new_property->status = "2";
                                    $new_property->property_status = "paid";
                                    $new_property->legal_address = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : "";
                                    $new_property->save();
                                    echo "New Property has been added = " . $new_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $new_property;
                                }
                            }
                        } else {
                            //echo bcrypt('f34f4f'); exit;
                            $new_user = new \App\User();
                            $new_user->email = $user_email;
                            $new_user->password = bcrypt('f34f4f');
                            $new_user->name = (isset($user["name"]) and ! empty($user["name"]) ) ? $user["name"] : "";
                            $new_user->lastname = (isset($user["lastname"]) and ! empty($user["lastname"]) ) ? $user["lastname"] : "";
                            $new_user->gender = (isset($user["gender"]) and ! empty($user["gender"]) ) ? $user["gender"] : "";
                            $new_user->dob = "1/1/1001";
                            $new_user->basicpob = (isset($user["basicpob"]) and ! empty($user["basicpob"]) ) ? $user["basicpob"] : "";
                            $new_user->country_id = $country_id;
                            $new_user->phonenumber = (isset($user["phonenumber"]) and ! empty($user["phonenumber"]) ) ? $user["phonenumber"] : "";
                            $new_user->NIN18 = (isset($user["nin18"]) and ! empty($user["nin18"]) ) ? $user["nin18"] : "";
                            $new_user->organisation_tax_registration_number = (isset($user["organisation_tax_registration_number"]) and ! empty($user["organisation_tax_registration_number"]) ) ? $user["organisation_tax_registration_number"] : "";
                            $new_user->orgname = (isset($user["orgname"]) and ! empty($user["orgname"]) ) ? $user["orgname"] : "";
                            $new_user->aboutorg = (isset($user["aboutorg"]) and ! empty($user["aboutorg"]) ) ? $user["aboutorg"] : "";
                            $new_user->legal_nature = (isset($legal_nature) and ! empty($legal_nature) ) ? $legal_nature : "";
                            $new_user->orghr = $hr_id;
                            $new_user->orgemail = (isset($user["orgemail"]) and ! empty($user["orgemail"]) ) ? $user["orgemail"] : "";
                            $new_user->orgphone = (isset($user["orgphone"]) and ! empty($user["orgphone"]) ) ? $user["orgphone"] : "";
                            $new_user->raddress18 = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : "";
                            $new_user->tutorrelation = (isset($tutor_relations_id) and ( $tutor_relations_id > 0)) ? $tutor_relations_id : "";
                            $new_user->why_fit = (isset($user["why_fit"]) and ! empty($user["why_fit"]) ) ? $user["why_fit"] : "";
                            $new_user->emergency_contact_name = (isset($user["emergency_contact_name"]) and ! empty($user["emergency_contact_name"]) ) ? $user["emergency_contact_name"] : "";
                            $new_user->emergency_contact_phone = (isset($user["emergency_contact_phone"]) and ! empty($user["emergency_contact_phone"]) ) ? $user["emergency_contact_phone"] : "";
                            $new_user->emergency_contact_email = (isset($user["emergency_contact_email"]) and ! empty($user["emergency_contact_email"]) ) ? $user["emergency_contact_email"] : "";
                            $new_user->emergency_contact_relationship = (isset($user["emergency_contact_relationship"]) and ! empty($user["emergency_contact_relationship"]) ) ? $user["emergency_contact_relationship"] : "";
                            $new_user->save();
                            $user_paid_service = \App\UserPaidService::where("user_id", $new_user->id)->first();
                            if (!isset($user_paid_service->id)) {
                                $user_paid_service = new \App\UserPaidService();
                            }
                            $user_paid_service->user_id = $new_user->id;
                            $user_paid_service->houseaddress = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_user->raddress18;
                            $user_paid_service->houselatbox = (isset($lat) and ! empty($lat) ) ? $lat : "";
                            $user_paid_service->houselonbox = (isset($long) and ! empty($long) ) ? $long : "";
                            $user_paid_service->save();
                            echo "New User has been added = " . $new_user->id . "<br />";
                            $listing_array["user"] = $new_user;
                            //if ($new_user->id) {

                            if (isset($user["exptitle"]) && !empty($user["exptitle"])) {
                                $check_property = \App\Property::where("exptitle", $user["exptitle"])->first();
                                if (isset($check_property->id) && $check_property->id > 0) {
                                    $check_property->user_id = $new_user->id;
                                    $check_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : $check_property->exptitle;
                                    $check_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : $check_property->description;
                                    $check_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : $check_property->optional_description;
                                    $check_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : $check_property->houserules;
                                    $check_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : $check_property->work_schedule;
                                    $check_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : $check_property->work_hours;
                                    $check_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : $check_property->link_to_extra_docs_necessary;
                                    $check_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : $check_property->hr_represent_name;
                                    $check_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : $check_property->hr_represent_surname;
                                    $check_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : $check_property->hr_represent_email;
                                    $check_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : $check_property->hr_represent_phone;
                                    $check_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : $check_property->pro_lat;
                                    $check_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : $check_property->pro_lon;
                                    $check_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : $check_property->property_type;
                                    $check_property->human_resources = (isset($human_resource)) ? $human_resource : $check_property->human_resources;
                                    $check_property->status = "2";
                                    $check_property->property_status = "paid";
                                    $check_property->legal_address = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_property->legal_address;
                                    $check_property->save();
                                    echo "Property has been update = " . $check_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $check_property;
                                } else {
                                    $new_property = new \App\Property();
                                    $new_property->user_id = $new_user->id;
                                    $new_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : "";
                                    $new_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : "";
                                    $new_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : "";
                                    $new_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : "";
                                    $new_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : "";
                                    $new_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : "";
                                    $new_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : "";
                                    $new_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : "";
                                    $new_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : "";
                                    $new_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : "";
                                    $new_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : "";
                                    $new_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : "";
                                    $new_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : "";
                                    $new_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : "";
                                    $new_property->human_resources = (isset($human_resource)) ? $human_resource : "";
                                    $new_property->status = "2";
                                    $new_property->property_status = "paid";
                                    $new_property->legal_address = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : "";
                                    $new_property->save();
                                    echo "New Property has been added = " . $new_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $new_property;
                                }
                            }
                            //}
                        }
                        $user_array[] = $listing_array;
                    }
                }
            }
        }
        //echo "<pre>";
        //print_r($user_array);
        //echo "</pre>";
        //return $user_array;
        exit;
        //last user id = 1945
        //last property id = 107;
    }

    public function newschoolHousesExcel() {
        //Employee with regular contract
        $result = \Excel::load('excel/school_houses_new.xls', function($reader) {
                    
                })->toArray();

//        echo "<pre>";
//        print_r($result);
//        echo "</pre>";
//        exit;
//        return $result;
        $user_array = array();
        $listing_array = array();
        if (sizeof($result) > 0) {
            $count = 0;
            foreach ($result as $res) {
                foreach ($res as $user) {
                    $count++;
                    if ($count > 71) {
                        break;
                    }
                    //return $user;
                    $user_id = 0;
                    $user_email = "";
                    if (isset($user["email"]) && !empty($user["email"])) {
                        $user_email = $user["email"];
                    } elseif (isset($user["orgemail"]) && !empty($user["orgemail"])) {
                        $user_email = $user["orgemail"];
                    }
                    if (!empty($user_email)) {
                        $check_user = \App\User::where("email", $user_email)->first();
                        $legal_address = isset($user["orgaddress"]) ? $user["orgaddress"] : "";
                        $lat = "";
                        $long = "";
                        if (!empty($legal_address)) {
                            $address_result = $this->getLatLong($legal_address);
                            if (isset($address_result->results[0]->geometry->location->lat)) {
                                $lat = $address_result->results[0]->geometry->location->lat;
                            }
                            if (isset($address_result->results[0]->geometry->location->lng)) {
                                $long = $address_result->results[0]->geometry->location->lng;
                            }
                        }
                        $country_id = 0;
                        if (isset($user["country_id"])) {
                            $country = \App\Country::where("name", $user["country_id"])->first();
                            $country_id = isset($country->id) ? $country->id : "0";
                        }
                        $legal_nature = '';
                        if (isset($user["legal_nature"])) {
                            if ($user["legal_nature"] == "Private") {
                                $legal_nature = 0;
                            } elseif ($user["legal_nature"] == "Public") {
                                $legal_nature = 1;
                            }
                        }
                        $hr_id = 0;
                        if (isset($user["orghr"])) {
                            $hr_detail = \App\RoomType::where("name", $user["orghr"])->first();
                            $hr_id = isset($hr_detail->id) ? $hr_detail->id : "0";
                        }
                        $tutor_relations_id = 0;
                        if (isset($user["tutorrelation"])) {
                            $tutor_relations = \App\Tutor::where("name", $user["tutorrelation"])->first();
                            $tutor_relations_id = isset($tutor_relations->id) ? $tutor_relations->id : "0";
                        }
                        $property_type = 0;
                        if (isset($user["workcategory"])) {
                            $type = \App\PropertyType::where("name", $user["workcategory"])->first();
                            $property_type = isset($type->id) ? $type->id : "0";
                        }
                        $human_resource = 1;
                        if (isset($user["checkhumanid"]) && $user["checkhumanid"] == "y") {
                            $human_resource = 0; //so unchecked is 1, therefore user has not specified different HR IF 1, else 0 means there is different HR. 
                        }
                        if (isset($check_user->id) && $check_user->id > 0) {
                            $user_id = $check_user->id;
//                            $check_user->name = (isset($user["name"]) and ! empty($user["name"]) ) ? $user["name"] : $check_user->name;
//                            $check_user->lastname = (isset($user["lastname"]) and ! empty($user["lastname"]) ) ? $user["lastname"] : $check_user->lastname;
//                            $check_user->gender = (isset($user["gender"]) and ! empty($user["gender"]) ) ? $user["gender"] : $check_user->gender;
//                            $check_user->dob = (isset($check_user->dob) and ! empty($check_user->dob) ) ? $check_user->dob : "1/1/1001";
//                            $check_user->basicpob = (isset($user["basicpob"]) and ! empty($user["basicpob"]) ) ? $user["basicpob"] : $check_user->basicpob;
//                            $check_user->country_id = (isset($check_user->country_id) and ! empty($check_user->country_id) ) ? $check_user->country_id : $country_id;
//                            $check_user->phonenumber = (isset($user["phonenumber"]) and ! empty($user["phonenumber"]) ) ? $user["phonenumber"] : $check_user->phonenumber;
//                            $check_user->NIN18 = (isset($user["nin18"]) and ! empty($user["nin18"]) ) ? $user["nin18"] : $check_user->nin18;
//                            $check_user->organisation_tax_registration_number = (isset($user["organisation_tax_registration_number"]) and ! empty($user["organisation_tax_registration_number"]) ) ? $user["organisation_tax_registration_number"] : $check_user->organisation_tax_registration_number;
//                            $check_user->orgname = (isset($user["orgname"]) and ! empty($user["orgname"]) ) ? $user["orgname"] : $check_user->orgname;
//                            $check_user->aboutorg = (isset($user["aboutorg"]) and ! empty($user["aboutorg"]) ) ? $user["aboutorg"] : $check_user->aboutorg;
//                            $check_user->legal_nature = (isset($legal_nature) and ! empty($legal_nature) ) ? $legal_nature : $check_user->legal_nature;
//                            $check_user->orghr = (isset($hr_id) and ( $hr_id > 0)) ? $hr_id : $check_user->orghr;
//                            $check_user->orgemail = (isset($user["orgemail"]) and ! empty($user["orgemail"]) ) ? $user["orgemail"] : $check_user->orgemail;
//                            $check_user->orgphone = (isset($user["orgphone"]) and ! empty($user["orgphone"]) ) ? $user["orgphone"] : $check_user->orgphone;
                            // this si the orgaddress from excel upload to be stored in raddress18, 
                            // so this same address will store in houseaddress in user_paid_service table

                            $check_user->raddress18 = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_user->raddress18;
//                            $check_user->tutorrelation = (isset($tutor_relations_id) and ( $tutor_relations_id > 0)) ? $tutor_relations_id : $check_user->tutorrelation;
//                            $check_user->why_fit = (isset($user["why_fit"]) and ! empty($user["why_fit"]) ) ? $user["why_fit"] : $check_user->why_fit;
//                            $check_user->emergency_contact_name = (isset($user["emergency_contact_name"]) and ! empty($user["emergency_contact_name"]) ) ? $user["emergency_contact_name"] : $check_user->emergency_contact_name;
//                            $check_user->emergency_contact_phone = (isset($user["emergency_contact_phone"]) and ! empty($user["emergency_contact_phone"]) ) ? $user["emergency_contact_phone"] : $check_user->emergency_contact_phone;
//                            $check_user->emergency_contact_email = (isset($user["emergency_contact_email"]) and ! empty($user["emergency_contact_email"]) ) ? $user["emergency_contact_email"] : $check_user->emergency_contact_email;
//                            $check_user->emergency_contact_relationship = (isset($user["emergency_contact_relationship"]) and ! empty($user["emergency_contact_relationship"]) ) ? $user["emergency_contact_relationship"] : $check_user->emergency_contact_relationship;
                            $check_user->save();
                            echo "User has been update = " . $check_user->id . "<br />";
                            $listing_array["user"] = $check_user;
                            $user_paid_service = \App\UserPaidService::where("user_id", $user_id)->first();
                            if (!isset($user_paid_service->id)) {
                                $user_paid_service = new \App\UserPaidService();
                            }
                            $user_paid_service->user_id = $user_id;
                            $user_paid_service->houseaddress = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_user->raddress18;
                            $user_paid_service->houselatbox = (isset($lat) and ! empty($lat) ) ? $lat : "";
                            $user_paid_service->houselonbox = (isset($long) and ! empty($long) ) ? $long : "";
                            $user_paid_service->save();
                            //Listing variables
                            if (isset($user["exptitle"]) && !empty($user["exptitle"])) {
                                $check_property = \App\Property::where("exptitle", $user["exptitle"])->first();
                                if (isset($check_property->id) && $check_property->id > 0) {
                                    $check_property->user_id = $check_user->id;
//                                    $check_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : $check_property->exptitle;
//                                    $check_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : $check_property->description;
//                                    $check_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : $check_property->optional_description;
//                                    $check_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : $check_property->houserules;
//                                    $check_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : $check_property->work_schedule;
//                                    $check_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : $check_property->work_hours;
//                                    $check_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : $check_property->link_to_extra_docs_necessary;
//                                    $check_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : $check_property->hr_represent_name;
//                                    $check_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : $check_property->hr_represent_surname;
//                                    $check_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : $check_property->hr_represent_email;
//                                    $check_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : $check_property->hr_represent_phone;
//                                    $check_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : $check_property->pro_lat;
//                                    $check_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : $check_property->pro_lon;
//                                    $check_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : $check_property->property_type;
//                                    $check_property->human_resources = (isset($human_resource)) ? $human_resource : $check_property->human_resources;
//                                    $check_property->status = "2";
//                                    $check_property->property_status = "paid";
//                                    $check_property->legal_address = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_property->legal_address;
//                                    $check_property->save();
                                    echo "Property has been update = " . $check_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $check_property;
                                } else {
                                    $new_property = new \App\Property();
                                    $new_property->user_id = (isset($user_id) and ! empty($user_id) ) ? $user_id : "";
                                    $new_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : "";
                                    $new_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : "";
                                    $new_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : "";
                                    $new_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : "";
                                    $new_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : "";
                                    $new_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : "";
                                    $new_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : "";
                                    $new_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : "";
                                    $new_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : "";
                                    $new_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : "";
                                    $new_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : "";
                                    $new_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : "";
                                    $new_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : "";
                                    $new_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : "";
                                    $new_property->human_resources = (isset($human_resource)) ? $human_resource : "";
                                    $new_property->status = "2";
                                    $new_property->property_status = "paid";
                                    $new_property->legal_address = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : "";
                                    $new_property->save();
                                    echo "New Property has been added = " . $new_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $new_property;
                                }
                            }
                        } else {
                            //echo bcrypt('f34f4f'); exit;
                            $new_user = new \App\User();
                            $new_user->email = $user_email;
                            $new_user->password = bcrypt('f34f4f');
                            $new_user->name = (isset($user["name"]) and ! empty($user["name"]) ) ? $user["name"] : "";
                            $new_user->lastname = (isset($user["lastname"]) and ! empty($user["lastname"]) ) ? $user["lastname"] : "";
                            $new_user->gender = (isset($user["gender"]) and ! empty($user["gender"]) ) ? $user["gender"] : "";
                            $new_user->dob = "1/1/1001";
                            $new_user->basicpob = (isset($user["basicpob"]) and ! empty($user["basicpob"]) ) ? $user["basicpob"] : "";
                            $new_user->country_id = $country_id;
                            $new_user->phonenumber = (isset($user["phonenumber"]) and ! empty($user["phonenumber"]) ) ? $user["phonenumber"] : "";
                            $new_user->NIN18 = (isset($user["nin18"]) and ! empty($user["nin18"]) ) ? $user["nin18"] : "";
                            $new_user->organisation_tax_registration_number = (isset($user["organisation_tax_registration_number"]) and ! empty($user["organisation_tax_registration_number"]) ) ? $user["organisation_tax_registration_number"] : "";
                            $new_user->orgname = (isset($user["orgname"]) and ! empty($user["orgname"]) ) ? $user["orgname"] : "";
                            $new_user->aboutorg = (isset($user["aboutorg"]) and ! empty($user["aboutorg"]) ) ? $user["aboutorg"] : "";
                            $new_user->legal_nature = (isset($legal_nature) and ! empty($legal_nature) ) ? $legal_nature : "";
                            $new_user->orghr = $hr_id;
                            $new_user->orgemail = (isset($user["orgemail"]) and ! empty($user["orgemail"]) ) ? $user["orgemail"] : "";
                            $new_user->orgphone = (isset($user["orgphone"]) and ! empty($user["orgphone"]) ) ? $user["orgphone"] : "";
                            $new_user->raddress18 = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : "";
                            $new_user->tutorrelation = (isset($tutor_relations_id) and ( $tutor_relations_id > 0)) ? $tutor_relations_id : "";
                            $new_user->why_fit = (isset($user["why_fit"]) and ! empty($user["why_fit"]) ) ? $user["why_fit"] : "";
                            $new_user->emergency_contact_name = (isset($user["emergency_contact_name"]) and ! empty($user["emergency_contact_name"]) ) ? $user["emergency_contact_name"] : "";
                            $new_user->emergency_contact_phone = (isset($user["emergency_contact_phone"]) and ! empty($user["emergency_contact_phone"]) ) ? $user["emergency_contact_phone"] : "";
                            $new_user->emergency_contact_email = (isset($user["emergency_contact_email"]) and ! empty($user["emergency_contact_email"]) ) ? $user["emergency_contact_email"] : "";
                            $new_user->emergency_contact_relationship = (isset($user["emergency_contact_relationship"]) and ! empty($user["emergency_contact_relationship"]) ) ? $user["emergency_contact_relationship"] : "";
                            $new_user->save();
                            $user_paid_service = \App\UserPaidService::where("user_id", $new_user->id)->first();
                            if (!isset($user_paid_service->id)) {
                                $user_paid_service = new \App\UserPaidService();
                            }
                            $user_paid_service->user_id = $new_user->id;
                            $user_paid_service->houseaddress = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_user->raddress18;
                            $user_paid_service->houselatbox = (isset($lat) and ! empty($lat) ) ? $lat : "";
                            $user_paid_service->houselonbox = (isset($long) and ! empty($long) ) ? $long : "";
                            $user_paid_service->save();
                            echo "New User has been added = " . $new_user->id . "<br />";
                            $listing_array["user"] = $new_user;
                            //if ($new_user->id) {

                            if (isset($user["exptitle"]) && !empty($user["exptitle"])) {
                                $check_property = \App\Property::where("exptitle", $user["exptitle"])->first();
                                if (isset($check_property->id) && $check_property->id > 0) {
                                    $check_property->user_id = $new_user->id;
                                    $check_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : $check_property->exptitle;
                                    $check_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : $check_property->description;
                                    $check_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : $check_property->optional_description;
                                    $check_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : $check_property->houserules;
                                    $check_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : $check_property->work_schedule;
                                    $check_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : $check_property->work_hours;
                                    $check_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : $check_property->link_to_extra_docs_necessary;
                                    $check_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : $check_property->hr_represent_name;
                                    $check_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : $check_property->hr_represent_surname;
                                    $check_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : $check_property->hr_represent_email;
                                    $check_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : $check_property->hr_represent_phone;
                                    $check_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : $check_property->pro_lat;
                                    $check_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : $check_property->pro_lon;
                                    $check_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : $check_property->property_type;
                                    $check_property->human_resources = (isset($human_resource)) ? $human_resource : $check_property->human_resources;
                                    $check_property->status = "2";
                                    $check_property->property_status = "paid";
                                    $check_property->legal_address = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : $check_property->legal_address;
                                    $check_property->save();
                                    echo "Property has been update = " . $check_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $check_property;
                                } else {
                                    $new_property = new \App\Property();
                                    $new_property->user_id = $new_user->id;
                                    $new_property->exptitle = (isset($user["exptitle"]) and ! empty($user["exptitle"]) ) ? $user["exptitle"] : "";
                                    $new_property->description = (isset($user["description"]) and ! empty($user["description"]) ) ? $user["description"] : "";
                                    $new_property->optional_description = (isset($user["optional_description"]) and ! empty($user["optional_description"]) ) ? $user["optional_description"] : "";
                                    $new_property->houserules = (isset($user["houserules"]) and ! empty($user["houserules"]) ) ? $user["houserules"] : "";
                                    $new_property->work_schedule = (isset($user["work_schedule"]) and ! empty($user["work_schedule"]) ) ? $user["work_schedule"] : "";
                                    $new_property->work_hours = (isset($user["work_hours"]) and ! empty($user["work_hours"]) ) ? $user["work_hours"] : "";
                                    $new_property->link_to_extra_docs_necessary = (isset($user["link_to_extra_docs_necessary"]) and ! empty($user["link_to_extra_docs_necessary"]) ) ? $user["link_to_extra_docs_necessary"] : "";
                                    $new_property->hr_represent_name = (isset($user["hrname1"]) and ! empty($user["hrname1"]) ) ? $user["hrname1"] : "";
                                    $new_property->hr_represent_surname = (isset($user["hrsurname1"]) and ! empty($user["hrsurname1"]) ) ? $user["hrsurname1"] : "";
                                    $new_property->hr_represent_email = (isset($user["hremail1"]) and ! empty($user["hremail1"]) ) ? $user["hremail1"] : "";
                                    $new_property->hr_represent_phone = (isset($user["hrtel1"]) and ! empty($user["hrtel1"]) ) ? $user["hrtel1"] : "";
                                    $new_property->pro_lat = (isset($lat) and ! empty($lat) ) ? $lat : "";
                                    $new_property->pro_lon = (isset($long) and ! empty($long) ) ? $long : "";
                                    $new_property->property_type = (isset($property_type) and ( $property_type > 0)) ? $property_type : "";
                                    $new_property->human_resources = (isset($human_resource)) ? $human_resource : "";
                                    $new_property->status = "2";
                                    $new_property->property_status = "paid";
                                    $new_property->legal_address = (isset($user["orgaddress"]) and ! empty($user["orgaddress"]) ) ? $user["orgaddress"] : "";
                                    $new_property->save();
                                    echo "New Property has been added = " . $new_property->id . "<br /><br />";
                                    $listing_array["user_listing"] = $new_property;
                                }
                            }
                            //}
                        }
                        $user_array[] = $listing_array;
                    }
                }
            }
        }
        //echo "<pre>";
        //print_r($user_array);
        //echo "</pre>";
        //return $user_array;
        exit;
        //last user id = 1945
        //last property id = 107;
    }

    public function testExcelOld() {
        //Employee with regular contract
        $result = \Excel::load('excel/new_users.xls', function($reader) {
                    
                })->toArray();

        return $result;
        $user_array = array();
        $listing_array = array();
        if (sizeof($result) > 0) {
            foreach ($result as $user) {
                $user_id = 0;
                $user_email = "";
                if (isset($user["email"]) && !empty($user["email"])) {
                    $user_email = $user["email"];
                } elseif (isset($user["orgemail"]) && !empty($user["orgemail"])) {
                    $user_email = $user["orgemail"];
                }
                if (!empty($user_email)) {
                    $check_user = \App\User::where("email", $user_email)->first();
                    if (isset($check_user->id) && $check_user->id > 0) {
                        $user_id = $check_user->id;
                    }

                    $legal_address = isset($user["legal_address"]) ? $user["legal_address"] : "";
                    $address_result = $this->getLatLong($legal_address);
                    $lat = "";
                    $long = "";
                    if (isset($address_result->results[0]->geometry->location->lat)) {
                        $lat = $address_result->results[0]->geometry->location->lat;
                    }
                    if (isset($address_result->results[0]->geometry->location->lng)) {
                        $long = $address_result->results[0]->geometry->location->lng;
                    }

                    //echo "address is = " . $legal_address . "<br />";
                    //echo "lat is = " . $lat . "<br />";
                    //echo "long is = " . $long . "<br /><br />";
                    //continue;

                    if ($user_id == 0) {
                        $country_id = 0;
                        if (isset($user["country_of_citizenship"])) {
                            $country = \App\Country::where("name", $user["country_of_citizenship"])->first();
                            $country_id = isset($country->id) ? $country->id : "0";
                        }
                        $legal_nature = '';
                        if (isset($user["legal_nature"])) {
                            if ($user["legal_nature"] == "Private") {
                                $legal_nature = 0;
                            } elseif ($user["legal_nature"] == "Public") {
                                $legal_nature = 1;
                            }
                        }
                        $hr_id = 0;
                        if (isset($user["human_resources"])) {
                            $hr_detail = \App\RoomType::where("name", $user["human_resources"])->first();
                            $hr_id = isset($hr_detail->id) ? $hr_detail->id : "0";
                        }
                        $tutor_relations_id = 0;
                        if (isset($user["relationship_in_the_company"])) {
                            $tutor_relations = \App\Tutor::where("name", $user["relationship_in_the_company"])->first();
                            $tutor_relations_id = isset($tutor_relations->id) ? $tutor_relations->id : "0";
                        }

                        if (!empty($user_email)) {
                            $data = array(
                                "name" => isset($user["user18_work_tutors_name"]) ? $user["user18_work_tutors_name"] : "",
                                "lastname" => isset($user["work_tutors_user_18_surname"]) ? $user["work_tutors_user_18_surname"] : "",
                                "gender" => isset($user["gender_18"]) ? $user["gender_18"] : "",
                                "dob" => "1/1/1001",
                                "basicpob" => isset($user["work_tutors_place_of_birth"]) ? $user["work_tutors_place_of_birth"] : "",
                                "country_id" => $country_id,
                                "email" => $user_email,
                                "password" => bcrypt('f34f4f'),
                                "phonenumber" => isset($user["work_tutors_contact"]) ? $user["work_tutors_contact"] : "",
                                "NIN18" => isset($user["work_tutors_tax_number"]) ? $user["work_tutors_tax_number"] : "",
                                "orgname" => isset($user["organisation_name"]) ? $user["organisation_name"] : "",
                                "aboutorg" => isset($user["description"]) ? $user["description"] : "",
                                "legal_nature" => $legal_nature,
                                "orghr" => $hr_id,
                                "orgemail" => isset($user["org_email"]) ? $user["org_email"] : "",
                                "orgphone" => isset($user["org_phone"]) ? $user["org_phone"] : "",
                                "raddress18" => isset($user["legal_address"]) ? $user["legal_address"] : "",
                                "tutorrelation" => $tutor_relations_id,
                                "why_fit" => isset($user["work_tutors_answer_to_why_are_you_fit_for_being_a_work_tutor"]) ? $user["work_tutors_answer_to_why_are_you_fit_for_being_a_work_tutor"] : "",
                            );

                            $listing_data = array(
                                "exptitle" => isset($user["titolo_posizione"]) ? $user["titolo_posizione"] : "",
                                "description" => isset($user["duties"]) ? $user["duties"] : "",
                                "optional_description" => isset($user["duties_optional_extra_discription"]) ? $user["duties_optional_extra_discription"] : "",
                                "houserules" => isset($user["rules"]) ? $user["rules"] : "",
                                "work_hours" => isset($user["working_hours_maximum_in_a_week"]) ? $user["working_hours_maximum_in_a_week"] : "",
                                "work_schedule" => isset($user["hourly_articulation_of_working_day"]) ? $user["hourly_articulation_of_working_day"] : "",
                                "hr_represent_name" => isset($user["human_resources_name"]) ? $user["human_resources_name"] : "",
                                "hr_represent_surname" => isset($user["human_resources_surname"]) ? $user["human_resources_surname"] : "",
                                "hr_represent_email" => isset($user["human_resources_email"]) ? $user["human_resources_email"] : "",
                                "hr_represent_phone" => isset($user["human_resources_tel"]) ? $user["human_resources_tel"] : "",
                                "status" => "1",
                                "property_status" => "unpaidflash",
                                "legal_address" => isset($user["legal_address"]) ? $user["legal_address"] : "",
                                "pro_lat" => $lat,
                                "pro_lon" => $long,
                            );
                            $data["user_listing"] = $listing_data;
                            $user_array[] = $data;
                        }
                    } else {
                        $listing_data = array(
                            "exptitle" => isset($user["titolo_posizione"]) ? $user["titolo_posizione"] : "",
                            "description" => isset($user["duties"]) ? $user["duties"] : "",
                            "optional_description" => isset($user["duties_optional_extra_discription"]) ? $user["duties_optional_extra_discription"] : "",
                            "houserules" => isset($user["rules"]) ? $user["rules"] : "",
                            "work_hours" => isset($user["working_hours_maximum_in_a_week"]) ? $user["working_hours_maximum_in_a_week"] : "",
                            "work_schedule" => isset($user["hourly_articulation_of_working_day"]) ? $user["hourly_articulation_of_working_day"] : "",
                            "hr_represent_name" => isset($user["human_resources_name"]) ? $user["human_resources_name"] : "",
                            "hr_represent_surname" => isset($user["human_resources_surname"]) ? $user["human_resources_surname"] : "",
                            "hr_represent_email" => isset($user["human_resources_email"]) ? $user["human_resources_email"] : "",
                            "hr_represent_phone" => isset($user["human_resources_tel"]) ? $user["human_resources_tel"] : "",
                            "status" => "1",
                            "property_status" => "unpaidflash",
                            "legal_address" => isset($user["legal_address"]) ? $user["legal_address"] : "",
                            "pro_lat" => $lat,
                            "pro_lon" => $long,
                        );
                        $data["user_listing"] = $listing_data;
                        $user_array[] = $data;
                    }
                }
            }
        }
        //return $user_array;
        if (sizeof($user_array) > 0) {
            foreach ($user_array as $info) {
                if (isset($info["email"])) {
                    $user_id = 0;
                    $check_user = \App\User::where("email", $info["email"])->first();
                    if (isset($check_user->id) && $check_user->id > 0) {
                        $user_id = $check_user->id;
                    }
                    if ($user_id > 0) {
                        if (sizeof($info["user_listing"]) > 0) {
                            $listing = new \App\Property();
                            $listing->user_id = $user->id;
                            $listing->exptitle = isset($info["user_listing"]["exptitle"]) ? $info["user_listing"]["exptitle"] : "";
                            $listing->description = isset($info["user_listing"]["description"]) ? $info["user_listing"]["description"] : "";
                            $listing->optional_description = isset($info["user_listing"]["optional_description"]) ? $info["user_listing"]["optional_description"] : "";
                            $listing->houserules = isset($info["user_listing"]["houserules"]) ? $info["user_listing"]["houserules"] : "";
                            $listing->work_hours = isset($info["user_listing"]["work_hours"]) ? $info["user_listing"]["work_hours"] : "";
                            $listing->work_schedule = isset($info["user_listing"]["work_schedule"]) ? $info["user_listing"]["work_schedule"] : "";
                            $listing->hr_represent_name = isset($info["user_listing"]["hr_represent_name"]) ? $info["user_listing"]["hr_represent_name"] : "";
                            $listing->hr_represent_surname = isset($info["user_listing"]["hr_represent_surname"]) ? $info["user_listing"]["hr_represent_surname"] : "";
                            $listing->hr_represent_email = isset($info["user_listing"]["hr_represent_email"]) ? $info["user_listing"]["hr_represent_email"] : "";
                            $listing->hr_represent_phone = isset($info["user_listing"]["hr_represent_phone"]) ? $info["user_listing"]["hr_represent_phone"] : "";
                            $listing->status = "1";
                            $listing->property_status = "unpaidflash";
                            $listing->legal_address = isset($info["user_listing"]["legal_address"]) ? $info["user_listing"]["legal_address"] : "";
                            $listing->pro_lat = isset($info["user_listing"]["pro_lat"]) ? $info["user_listing"]["pro_lat"] : "";
                            $listing->pro_lon = isset($info["user_listing"]["pro_lon"]) ? $info["user_listing"]["pro_lon"] : "";
                            $listing->save();
                            echo "Listing created id = " . $listing->id . "<br /><br />";
                        }
                    } else {
                        $user = new \App\User();
                        $user->name = isset($info["name"]) ? $info["name"] : "";
                        $user->lastname = isset($info["lastname"]) ? $info["lastname"] : "";
                        $user->gender = isset($info["gender"]) ? $info["gender"] : "";
                        $user->dob = isset($info["dob"]) ? $info["dob"] : "";
                        $user->basicpob = isset($info["work_tutors_place_of_birth"]) ? $info["work_tutors_place_of_birth"] : "";
                        $user->country_id = isset($info["country_id"]) ? $info["country_id"] : "";
                        $user->email = isset($info["email"]) ? $info["email"] : "";
                        $user->password = isset($info["password"]) ? $info["password"] : "";
                        $user->phonenumber = isset($info["phonenumber"]) ? $info["phonenumber"] : "";
                        $user->NIN18 = isset($info["NIN18"]) ? $info["NIN18"] : "";
                        $user->orgname = isset($info["orgname"]) ? $info["orgname"] : "";
                        $user->aboutorg = isset($info["aboutorg"]) ? $info["aboutorg"] : "";
                        $user->legal_nature = isset($info["legal_nature"]) ? $info["legal_nature"] : "";
                        $user->orghr = isset($info["orghr"]) ? $info["orghr"] : "";
                        $user->orgemail = isset($info["orgemail"]) ? $info["orgemail"] : "";
                        $user->orgphone = isset($info["orgphone"]) ? $info["orgphone"] : "";
                        $user->raddress18 = isset($info["raddress18"]) ? $info["raddress18"] : "";
                        $user->tutorrelation = isset($info["tutorrelation"]) ? $info["tutorrelation"] : "";
                        $user->why_fit = isset($info["why_fit"]) ? $info["why_fit"] : "";
                        $user->save();
                        echo "User created id = " . $user->id . "<br />";
                        if (isset($user->id)) {
                            if (sizeof($info["user_listing"]) > 0) {
                                $listing = new \App\Property();
                                $listing->user_id = $user->id;
                                $listing->exptitle = isset($info["user_listing"]["exptitle"]) ? $info["user_listing"]["exptitle"] : "";
                                $listing->description = isset($info["user_listing"]["description"]) ? $info["user_listing"]["description"] : "";
                                $listing->optional_description = isset($info["user_listing"]["optional_description"]) ? $info["user_listing"]["optional_description"] : "";
                                $listing->houserules = isset($info["user_listing"]["houserules"]) ? $info["user_listing"]["houserules"] : "";
                                $listing->work_hours = isset($info["user_listing"]["work_hours"]) ? $info["user_listing"]["work_hours"] : "";
                                $listing->work_schedule = isset($info["user_listing"]["work_schedule"]) ? $info["user_listing"]["work_schedule"] : "";
                                $listing->hr_represent_name = isset($info["user_listing"]["hr_represent_name"]) ? $info["user_listing"]["hr_represent_name"] : "";
                                $listing->hr_represent_surname = isset($info["user_listing"]["hr_represent_surname"]) ? $info["user_listing"]["hr_represent_surname"] : "";
                                $listing->hr_represent_email = isset($info["user_listing"]["hr_represent_email"]) ? $info["user_listing"]["hr_represent_email"] : "";
                                $listing->hr_represent_phone = isset($info["user_listing"]["hr_represent_phone"]) ? $info["user_listing"]["hr_represent_phone"] : "";
                                $listing->status = "1";
                                $listing->property_status = "unpaidflash";
                                $listing->legal_address = isset($info["user_listing"]["legal_address"]) ? $info["user_listing"]["legal_address"] : "";
                                $listing->pro_lat = isset($info["user_listing"]["pro_lat"]) ? $info["user_listing"]["pro_lat"] : "";
                                $listing->pro_lon = isset($info["user_listing"]["pro_lon"]) ? $info["user_listing"]["pro_lon"] : "";
                                $listing->save();
                                echo "Listing created id = " . $listing->id . "<br /><br />";
                            }
                        }
                    }
                }
            }
        }
        exit;
        //return $listing_array;
        return $user_array;
        //user last id = 1879
        //property last id = 70
    }

    public function saveLatlong() {
        $properties = \App\Property::where("pro_lat", "0.000000")->where("pro_lon", "0.000000")->get();
        if (sizeof($properties) > 0) {
            foreach ($properties as $prop) {
                $legal_address = isset($prop->legal_address) ? $prop->legal_address : "";
                $address_result = $this->getLatLong($legal_address);

                $lat = "";
                $long = "";
                if (isset($address_result->results[0]->geometry->location->lat)) {
                    $lat = $address_result->results[0]->geometry->location->lat;
                }
                if (isset($address_result->results[0]->geometry->location->lng)) {
                    $long = $address_result->results[0]->geometry->location->lng;
                }
                $update_prop = \App\Property::find($prop->id);
                $update_prop->pro_lat = $lat;
                $update_prop->pro_lon = $long;
                $update_prop->save();
                echo "Listing created id = " . $update_prop->id . "<br />";
                echo "address is = " . $update_prop->legal_address . "<br />";
                echo "lat is = " . $lat . "<br />";
                echo "long is = " . $long . "<br /><br />";
            }
        }
        //15 Broad St, Nottingham NG1 3AJ, Regno Unito OR 15, Carlton St, Nottingham NG1 1NL 
        //AIzaSyB11l5QoR0jEa8h50ZLoVf-P_1kLU4sCgQ
    }

    public function getAllusersListing() {
        $users_info = \App\User::with("user_properties")->select("id", "name", "email", "orgname")->get();
        return $users_info;
    }

}
