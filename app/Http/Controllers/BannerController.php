<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\BannerRepository;
use App\Repositories\CodeRepository;
use Carbon\Carbon;


class BannerController extends Controller
{
    private $bredCrum = "Banner Images";
    
    protected $bannerRepository;

    protected $codeRepository;

    public function __construct(BannerRepository $bannerRepository, CodeRepository $codeRepository)
    {
        $this->bannerRepository = $bannerRepository;
        $this->codeRepository = $codeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->bannerRepository->getAll();
        return View('admin.banners.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        $codeArr = $this->codeRepository->getByCodeType(true, 'bannertype');
        return View('admin.banners.create', compact('bredCrum', 'codeArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('banner_img' => $request->file('banner_img'));
        $validator = Validator::make($postData, $this->bannerRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/banner/create')->withErrors($validator);
        } else {
            $file = $request->file('banner_img');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp. '.' .$file->getClientOriginalExtension();
            //$image->filePath = $img_name;
            $file->move(public_path().'/images/banners', $img_name);
            $postData = array('banner_img' => $img_name,'status' => $request->get('status'),'banner_type' => $request->get('banner_type'), 'title' => $request->get('title'), 'text' => $request->get('text'));
            $Obj = $this->bannerRepository->create($postData);
            flash('Banner Image Added Successfully', 'success');
            return \Redirect('admin/banner');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->bannerRepository->getById($id);
        $codeArr = $this->codeRepository->getByCodeType(true, 'bannertype');
        return View('admin.banners.edit', compact('bredCrum', 'editObj', 'codeArr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = array('status' => $request->get('status'), 'banner_type' => $request->get('banner_type'), 'title' => $request->get('title'), 'text' => $request->get('text'));
        if ($request->hasFile('banner_img')) {
            $file = $request->file('banner_img');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp. '.' .$file->getClientOriginalExtension();
            //$image->filePath = $img_name;
            $file->move(public_path().'/images/banners', $img_name);
            $postData = array_merge($postData, array('banner_img' => $img_name));
        }
            $Obj = $this->bannerRepository->update($postData, $id);
            flash('Banner Image Updated Successfully', 'success');
            return \Redirect('admin/banner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
