<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\PropertyRepository;
use App\Repositories\RoomtypeRepository;
use App\Repositories\CodeRepository;
use App\MedMessage;
use App\School;
use App\Role;
use App\Tutor;
use App\RentalsEnquiry;
use App\Services\Mailer\MailNotificationsForUser;
use Carbon\Carbon;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class UsersController extends Controller {

    protected $userRepository;
    protected $propertyRepository;
    protected $roomTypeRepository;
    private $mailNotificationsForUser;
    private $codeRepository;

    public function __construct(UserRepository $userRepository, PropertyRepository $propertyRepository, RoomtypeRepository $roomTypeRepository, MailNotificationsForUser $mailNotificationsForUser, CodeRepository $codeRepository) {
        $this->userRepository = $userRepository;
        $this->propertyRepository = $propertyRepository;
        $this->roomTypeRepository = $roomTypeRepository;
        $this->mailNotificationsForUser = $mailNotificationsForUser;
        $this->codeRepository = $codeRepository;
    }

    public function sendcontactmessage(Request $request) {
        $dataArr = array(
            'productId' => $request->get('proid'),
            'bookingNo' => $request->get('bno'),
            'enqid' => $request->get('listingid'),
            'senderId' => $request->get('senderid'),
            'receiverId' => $request->get('receiverid'),
            'subject' => '',
            'message' => $request->get('messages')
        );
        MedMessage::create($dataArr);
        $receiverObj = $this->userRepository->getById($request->get('receiverid'));
        $proObj = $this->propertyRepository->getById($request->get('proid'));
        $enquiryObj = RentalsEnquiry::where('id', $request->get('listingid'))->first();

        $this->mailNotificationsForUser->sendemailmessage($receiverObj, $proObj, $enquiryObj);
        return "success";
    }

    public function throwError() {
        flash('Trying to View Others Conversation is restricted', 'success');
        return \Redirect('user/messages');
    }

    public function conversation($enqId) {
        $enquiryObj = RentalsEnquiry::where('id', $enqId)->first();
        if (\Gate::denies('viewconversation', $enquiryObj)) {
            return $this->throwError();
        }
        MedMessage::changeReadStatus(\Auth::user()->id);
        $conversation = MedMessage::getConversations($enqId);

        return view('conversation', compact('conversation', 'enqId', 'enquiryObj'));
    }

    public function manage() {
        $bredCrum = "User Management";
        // $userObj = $this->userRepository->getAll();
        // $userObj = \App\User::with('properties')->get();
        //$userObj = \App\User::with(['properties','paid_service','school','userWallet'])->where("email","=","abbasjami47@gmail.com")->get();
        $userObj = \App\User::with(['properties','paid_service','school','userWallet'])->get();
        // if($_SERVER['REMOTE_ADDR'] == '93.33.44.160'){
          //  echo "<pre>";
          //  print_r($userObj->);
          //   echo "</pre>";
          //   die;
        //     exit;
        // }
        return View('admin.user.manage', compact('bredCrum', 'userObj'));
    }

    public function showMessages() {
        //MedMessage::changeReadStatus(\Auth::user()->id);

        $Obj = RentalsEnquiry::with('property')->where('renter_id', \Auth::user()->id)->orWhere('user_id', \Auth::user()->id)->orderBy('dateAdded', 'desc')->get();
        //$Obj = MedMessage::getReceivedMessages(\Auth::user()->id);
		//echo '<pre>';print_r($Obj);die;  
        return View('message', compact('messageObj', 'Obj'));
    }

    public function getMessageType(Request $request) {
        $type = $request->get('msgtype');
        if ($type == "inbox") {
            $Obj = MedMessage::getReceivedMessages(\Auth::user()->id);
        } elseif ($type == "sent") {
            $Obj = MedMessage::getSentMessages(\Auth::user()->id);
        } elseif ($type == "admin") {
            $Obj = MedMessage::getAdminMessages(\Auth::user()->id);
        }
        $view = View('messageajax', compact('Obj', 'type'));
        return $view;
    }

    public function userexport($roleId) {

        if ($roleId == 4) {
            $filename = "org" . date('Y-m-d H:i:s');
        } else if ($roleId == 5) {
            $filename = "parent" . date('Y-m-d H:i:s');
        } else if ($roleId == 6) {
            $filename = "studentover18" . date('Y-m-d H:i:s');
        }




        \Excel::create($filename, function($excel) use($roleId) {
            $sheetName = "user";
            $excel->sheet($sheetName, function($sheet) use($roleId) {
                $userObj = Role::find($roleId)->users()->get();
                $sheet->loadView('admin.export.user')->with('userObj', $userObj)->with('roleId', $roleId);
            });
        })->download('xls');
    }

    public function changeStatus(Request $request) {
        $updateId = $request->get('id');
        $status = $request->get('status');
        $updateData = array('status' => $status);
        $this->userRepository->update($updateId, $updateData);
        $this->propertyRepository->updateStatusByUserId($updateId, $updateData);
        return "success";
    }

    public function editProfile() {
        $userObject = $this->userRepository->getLoginUser();
        //return $userObject->paid_service;
        $roleId = 0;
        if ($userObject->hasRole('Organization')) {
            $roleId = 4;
        } elseif ($userObject->hasRole('Parent')) {
            $roleId = 5;
        } elseif ($userObject->hasRole('Student')) {
            $roleId = 6;
        }
        return View('user.editprofile', compact('userObject', 'roleId'));
    }

    public function editVerification() {
        $current_page = "user_verification";
        $userObject = $this->userRepository->getLoginUser();
        $user_verification = \App\UserVerification::where("user_id", \Auth::user()->id)->first();
        return View('user.editverification', compact('userObject', 'current_page', 'user_verification'));
    }

    public function editExtraInfo() {
        $current_page = "extra_info";
        $userObject = $this->userRepository->getLoginUser();
        $languages = \App\Language::where("status", "1")->get();
        $user_extrainfo = \App\UserExtraInfo::where("user_id", \Auth::user()->id)->first();
        return View('user.editextrainfo', compact('userObject', 'current_page', 'languages', 'user_extrainfo'));
    }

    public function studentGuest() {
        $current_page = "student_guest";
        $userObject = $this->userRepository->getLoginUser();
        $schools = School::where('status', 1)->get();
        $groups = \App\SecondarySchoolGroups::where(["is_delete" => "0"])->get();
        return View('user.studentguest', compact('userObject', 'schools', 'groups', 'current_page'));
    }



    public function editParent() {
        $current_page = "parent_under_18";
        $userObject = $this->userRepository->getLoginUser();
        $schools = School::where('status', 1)->get();
        $groups = \App\SecondarySchoolGroups::where(["is_delete" => "0"])->get();
        return View('user.editparent', compact('userObject', 'schools', 'groups', 'current_page'));
    }

     public function editPreenrolment() {
        $current_page = "pre_enrolment";
        $userObject = $this->userRepository->getLoginUser();
        $schools = School::where('status', 1)->get();
        $groups = \App\SecondarySchoolGroups::where(["is_delete" => "0"])->get();
        return View('user.editpreenrolment', compact('userObject', 'schools', 'groups', 'current_page'));
    }
    public function editPreEnrolmentOver18() {

        $current_page = "pre_enrolment";
        $userObject = $this->userRepository->getLoginUser();
        $schools = School::where('status', 1)->get();
        $groups = \App\SecondarySchoolGroups::where(["is_delete" => "0"])->get();
        return View('user.editpreenrolmentOver18', compact('userObject', 'schools', 'groups', 'current_page'));
    }

    public function workExperience() {
        $current_page = "work_experience";
        $userObject = $this->userRepository->getLoginUser();
        $schools = School::where('status', 1)->get();
        $groups = \App\SecondarySchoolGroups::where(["is_delete" => "0"])->get();
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        $user_legal = \App\UserLegalRepresentative::where("user_id", \Auth::user()->id)->first();
        return View('user.workexperience', compact('userObject', 'schools', 'groups', 'current_page', 'roomTypes', 'tutorRelation', 'user_legal'));
    }

    public function paidServices() {
        $current_page = "paid_services";
        $userObject = $this->userRepository->getLoginUser();
        $schools = School::where('status', 1)->get();
        $groups = \App\SecondarySchoolGroups::where(["is_delete" => "0"])->get();
        $hostPreference = $this->codeRepository->getByCodeType(false, 'HostPreference');
        $teachingType = $this->codeRepository->getByCodeType(false, 'TeachingType');
        $petsType = $this->codeRepository->getByCodeType(false, 'Pets');
        $dietTypes = $this->codeRepository->getByCodeType(false, 'Diet');
        $activityTypes = \App\ActivityType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $dietBoardTypes = $this->codeRepository->getByCodeType(false, 'DietBoard');
        //echo '<pre>'; print_r($teachingType);die(); 

        $homeType = $this->codeRepository->getByCodeType(false, 'HomeType');
        $propertyRegion = $this->codeRepository->getByCodeType(false, 'PropertyRegion');
        $houseLocalAmenities = $this->codeRepository->getByCodeType(false, 'HouseLocalAmenities');
        $facilities = $this->codeRepository->getByCodeType(false, 'Facilities');
        $tableGuest = $this->codeRepository->getByCodeType(false, 'TableGuest');
        $familySituation = $this->codeRepository->getByCodeType(false, 'FamilySituation');
        $pets = $this->codeRepository->getByCodeType(false, 'Pets');
        $urbanLocation = $this->codeRepository->getByCodeType(false, 'UrbanLocation');
        $userObject = $this->userRepository->getLoginUser();
        $selectedHostPrefer = isset($userObject->paid_service->host_preference) ? $userObject->paid_service->host_preference : "";
        $selectedTeachingSkills = isset($userObject->paid_service->teaching_skills) ? $userObject->paid_service->teaching_skills : "";
        //echo '<pre>'; print_r($userObject->paid_service->host_preference);die();
        $selectedAmenities = isset($userObject->paid_service->local_amenity) ? $userObject->paid_service->local_amenity : "";
        $selectedFacilities = isset($userObject->paid_service->facilities) ? $userObject->paid_service->facilities : "";
        $selectedRoomFacilities = isset($userObject->paid_service->room_facility) ? $userObject->paid_service->room_facility : "";
        $selectedPetTypes = isset($userObject->paid_service->pet_types) ? $userObject->paid_service->pet_types : "";
        $selectedDietTypes = isset($userObject->paid_service->school_house_diet) ? $userObject->paid_service->school_house_diet : "";
        $selectedActivityTypes = isset($userObject->paid_service->school_house_activity) ? $userObject->paid_service->school_house_activity : "";
        $hostPreferArray = array();
        $teachingSkillArray = array();
        $petTypeArray = array();
        $dietTypeArray = array();
        $dietBoardTypeArray = array(); //get the selected values from DB for Pets
        $teachingTypeArray = array(); //get the selected vaues from DB for TeachingType
        $roomFacilityArray = array();
        $amenityArray = array();
        $facilitiesArray = array();
        $activityTypeArray = array();

        if ($this->checkCommaExists($selectedActivityTypes)) {
            $activityTypeArray = explode(",", $selectedActivityTypes);
        } else {
            $activityTypeArray = array($selectedActivityTypes);
        }

        if ($this->checkCommaExists($selectedPetTypes)) {
            $petTypeArray = explode(",", $selectedPetTypes);
        } else {
            $petTypeArray = array($selectedPetTypes);
        }
        if ($this->checkCommaExists($selectedDietTypes)) {
            $dietTypeArray = explode(",", $selectedDietTypes);
        } else {
            $dietTypeArray = array($selectedDietTypes);
        }
        if ($this->checkCommaExists($selectedHostPrefer)) {
            $hostPreferArray = explode(",", $selectedHostPrefer);
        } else {
            $hostPreferArray = array($selectedHostPrefer);
        }
        if ($this->checkCommaExists($selectedTeachingSkills)) {
            $teachingSkillArray = explode(",", $selectedTeachingSkills);
        } else {
            $teachingSkillArray = array($selectedTeachingSkills);
        }
        if ($this->checkCommaExists($selectedRoomFacilities)) {
            $roomFacilityArray = explode(",", $selectedRoomFacilities);
        } else {
            $roomFacilityArray = array($selectedRoomFacilities);
        }

        if ($this->checkCommaExists($selectedAmenities)) {
            $amenityArray = explode(",", $selectedAmenities);
        } else {
            $amenityArray = array($selectedAmenities);
        }

        if ($this->checkCommaExists($selectedFacilities)) {
            $facilitiesArray = explode(",", $selectedFacilities);
        } else {
            $facilitiesArray = array($selectedFacilities);
        }
        //return $selectedFacilities;
        //echo $userObject->id; exit;
        $propertyimages = \App\Schoolhouseimage::where('user_id', $userObject->id)->get();
        //return $propertyimages;
        $familyimages = \App\Schoolfamily::where('user_id', $userObject->id)->get();
        $real_room_type = \App\RealRoomTypes::where("is_active", "1")->where("is_delete", "0")->get();
        $room_facilities = \App\RoomFacility::where("is_active", "1")->where("is_delete", "0")->get();

        $class_types = \App\ClassType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $selectedClassTypes = isset($userObject->paid_service->school_house_classes) ? $userObject->paid_service->school_house_classes : "";
        $classTypeArray = array();

        if ($this->checkCommaExists($selectedClassTypes)) {
            $classTypeArray = explode(",", $selectedClassTypes);
        } else {
            $classTypeArray = array($selectedClassTypes);
        }
        $transfer_types = \App\TransferType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $selectedTransferTypes = isset($userObject->paid_service->school_house_transfers) ? $userObject->paid_service->school_house_transfers : "";
        $transferTypeArray = array();

        if ($this->checkCommaExists($selectedTransferTypes)) {
            $transferTypeArray = explode(",", $selectedTransferTypes);
        } else {
            $transferTypeArray = array($selectedTransferTypes);
        }
        $tour_types = \App\TourType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $selectedTourTypes = isset($userObject->paid_service->school_house_tours) ? $userObject->paid_service->school_house_tours : "";
        $tourTypeArray = array();

        if ($this->checkCommaExists($selectedTourTypes)) {
            $tourTypeArray = explode(",", $selectedTourTypes);
        } else {
            $tourTypeArray = array($selectedTourTypes);
        }
        $wow_types = \App\WowType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $selectedWowTypes = isset($userObject->paid_service->school_house_wows) ? $userObject->paid_service->school_house_wows : "";
        $wowTypeArray = array();

        if ($this->checkCommaExists($selectedWowTypes)) {
            $wowTypeArray = explode(",", $selectedWowTypes);
        } else {
            $wowTypeArray = array($selectedWowTypes);
        }
        return View('user.paidservices', compact('userObject', 'schools', 'groups', 'activityTypeArray', 'activityTypes', 'classTypeArray', 'class_types', 'transferTypeArray', 'transfer_types', 'tourTypeArray', 'tour_types', 'wowTypeArray', 'wow_types', 'teachingSkillArray', 'real_room_type', 'room_facilities', 'roomFacilityArray', 'current_page', 'teachingType', 'teachingTypeArray', 'dietBoardTypes', 'dietBoardTypeArray', 'dietTypes', 'dietTypeArray', 'petsType', 'petTypeArray', 'hostPreference', 'propertyRegion', 'houseLocalAmenities', 'facilities', 'tableGuest', 'familySituation', 'pets', 'urbanLocation', 'homeType', 'hostPreferArray', 'amenityArray', 'facilitiesArray', 'propertyimages', 'familyimages'));
    }

    public function checkCommaExists($str) {
        if (strpos($str, ',') !== FALSE) {
            return true;
        } else {
            return false;
        }
    }

    public function evaluate() {
        return View('user.evoluation');
    }

    public function update_profile(Request $request) {
        $formData = $request->all();
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        // echo "Form Data : <pre>";
        //     print_r($_REQUEST);
        //     print_r($formData);
        // echo "</pre>";
        // // exit;
        // }
        //return $formData;
        //for over 18 DOB
        $new_dob = $request->get('byear') . "-" . $request->get('bmonth') . "-" . $request->get('bday');
        //echo "original dob = " . $new_dob . "<br />";
        $user_dob = date("Y-m-d", strtotime($new_dob));
        //echo "User dob = " . $user_dob . "<br />";
        $over18_date = date("Y-m-d", strtotime('-18 years'));
        //echo "current date - 18 years = " . $over18_date . "<br />";
        if ($user_dob > $over18_date) {
            //echo "yes inside if<br />";
            return redirect()->back()->with('error', "You must be over 18 to use this site, your parents must sign you up if you are underage")->withInput();
            exit;
        }
        //exit;
        //for over 16 DOB
//        $over16_dob = $request->get('byear16') . "-" . $request->get('bmonth16') . "-" . $request->get('bday16');
//        //echo "original dob = " . $new_dob . "<br />";
//        $dobover16_converted = date("Y-m-d", strtotime($over16_dob));
//        //echo "converted dob = " . $dob_converted . "<br />";
//        $check_date_over16 = date("Y-m-d", strtotime('-16 years'));
//        //echo "current date - 18 years = " . $check_date . "<br />";
//        if ($dobover16_converted < $check_date_over16) {
//            return redirect()->back()->with('error', "You must be over 16 to take part, check you have put the right date of birth or come back when you turn 16")->withInput();
//            exit;
//        }

        $edituser = \App\User::find(\Auth::user()->id);
        $userObject = $this->userRepository->getLoginUser();
        //return $edituser;
        // basic info
        $check = 0;
        $roleId = 0;

        if($formData['current_page'] == 'profile_info_image'){
            // $file = 
            $user_id = $edituser->id;
            $file = $request->file('property_image_paid')[0];
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp . '-' . $file->getClientOriginalName();
            //$image->filePath = $img_name;
            $file->move(public_path() . "/images/schoolhouse/$user_id", $img_name);
            $postData = array('img_name' => $img_name, 'user_id' => $user_id);
            \App\Schoolhouseimage::create($postData); 

            $res  = '<div class="col-md-3 listimgdiv">';
            $res .= '<img style="height:100px" src="/images/schoolhouse/'.$user_id.'/'.$img_name.'">
            <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$obj->id" onclick="remove_paid_image(this, \''.$img_name.'\',\'paidService\')"></i> </div>';
            // echo "Image uploaded successfully.";
            echo $res;
            exit;
        }
        else if($formData['current_page'] == 'family_info_image'){
                $user_id = $edituser->id;
                $file = $request->file('family_photo')[0];
                //getting timestamp
            //if (!empty($file)) {
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $img_name = $timestamp . '-' . $file->getClientOriginalName();
                //$image->filePath = $img_name;
                $file->move(public_path() . "/images/family/$user_id", $img_name);
                $postData = array('img_name' => $img_name, 'user_id' => $user_id);
                \App\Schoolfamily::create($postData);

                $res  = '<div class="col-md-3 listimgdiv">';
            $res .= '<img style="height:100px" src="/images/family/'.$user_id.'/'.$img_name.'">
            <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$obj->id" onclick="remove_paid_image(this, \''.$img_name.'\',\'paidService_family_photo\')"></i> </div>';
            // echo "Image uploaded successfully.";
            echo $res;
            exit;
            //}
        }
        else if($formData['current_page'] == 'user_editverifiation'){
                $user_id = $edituser->id;
            //     $file = $request->file('personal_id_picture_front')[0];
            //     //getting timestamp
            // //if (!empty($file)) {
            //     $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            //     $img_name = $timestamp . '-' . $file->getClientOriginalName();
            //     //$image->filePath = $img_name;
            //     $file->move(public_path() . "/images/family/$user_id", $img_name);
            //     $postData = array('img_name' => $img_name, 'user_id' => $user_id);
            //     \App\Schoolfamily::create($postData);
             //Customer::where('id', $u)->update(array());   
                $user_verification = \App\UserVerification::where("user_id", \Auth::user()->id)->first();

                if($formData['type'] == 'personal_id_picture_front'){if(isset($formData['action']) && $formData['action'] == 'remove'){
                        $img_front_name = '';
                        $res  = '';
                    }else{  

                        $pic_front_file = $request->file('personal_id_picture_front');
                        //getting timestamp
                        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                        $img_front_name = "pic_front-" . $timestamp . '-' . $pic_front_file->getClientOriginalName();
                        //$image->filePath = $img_name;
                        $pic_front_file->move(public_path() . "/images/extra_info/", $img_front_name);
                        // $user_verification->personal_id_picture_front = $img_front_name;
                        $res  = '<div class="col-md-3 listimgdiv">';
                    $res .= '<img style="width:50%" src="/images/extra_info/'.$img_front_name.'">
                    <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$obj->id" onclick="remove_trust_image(this, \''.$img_front_name.'\',\'personal_id_picture_front\')"></i> </div>';
                    }

                    \App\UserVerification::where('user_id', \Auth::user()->id)->update(array('personal_id_picture_front'=> $img_front_name));

                }else if($formData['type'] == 'personal_id_picture_back'){
                    if(isset($formData['action']) && $formData['action'] == 'remove'){
                        $img_back_name = '';
                        $res  = '';
                    }else{
                        $pic_back_file = $request->file('personal_id_picture_back');
                        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                        $img_back_name = "pic_back-" . $timestamp . '-' . $pic_back_file->getClientOriginalName();
                        //$image->filePath = $img_name;
                        $pic_back_file->move(public_path() . "/images/extra_info/", $img_back_name);


                         $res  = '<div class="col-md-3 listimgdiv">';
                $res .= '<img style="width:50%" src="/images/extra_info/'.$img_back_name.'">
                <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$obj->id" onclick="remove_trust_image(this, \''.$img_back_name.'\',\'personal_id_picture_back\')"></i> </div>';

                    }
                    //getting timestamp
                    \App\UserVerification::where('user_id', \Auth::user()->id)->update(array('personal_id_picture_back'=> $img_back_name));

                   
                }
                
                else if($formData['type'] == 'criminal_check_image'){
                    if(isset($formData['action']) && $formData['action'] == 'remove'){
                        $img_criminal_name = '';
                        $res  = '';
                    }else{
                        $pic_criminal_file = $request->file('criminal_check_image');
                        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                        
                        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                        $img_criminal_name = "pic_criminal-" . $timestamp . '-' . $pic_criminal_file->getClientOriginalName();
                        //$image->filePath = $img_name;
                        $pic_criminal_file->move(public_path() . "/images/extra_info/", $img_criminal_name);
                        $user_verification->criminal_check_image = $img_criminal_name;

                         $res  = '<div class="col-md-3 listimgdiv">';
                $res .= '<img style="width:50%" src="/images/extra_info/'.$img_criminal_name.'">
                <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$obj->id" onclick="remove_trust_image(this, \''.$img_criminal_name.'\',\'criminal_check_image\')"></i> </div>';

                    }
                    //getting timestamp
                    \App\UserVerification::where('user_id', \Auth::user()->id)->update(array('criminal_check_image'=> $img_criminal_name));

                   
                }
                
            // echo "Image uploaded successfully.";
            echo $res;
            exit;
        }

        $redirect_page = "edit";
        if ($formData["current_page"] == "student_guest") {
            $roleId = 6;
            $redirect_page = "studentGuest";
            if ($userObject->hasRole('Student')) {
                $check = 1;
            }
        } else if ($formData["current_page"] == "parent_under_18") {
            $roleId = 5;
            $redirect_page = "parent";
            if ($userObject->hasRole('Parent')) {
                $check = 1;
            }
        } else if ($formData["current_page"] == "work_experience") {
            $roleId = 4;
            $redirect_page = "workExperience";
            if ($userObject->hasRole('Organization')) {
                $check = 1;
            }
        } else if ($formData["current_page"] == "paid_services") {
            $roleId = 7;
            $redirect_page = "paidServices";
            if ($userObject->hasRole('PaidServices')) {
                $check = 1;
            }
        } else if ($formData["current_page"] == "extra_info") {
            $redirect_page = "extraInfo";
        } else if ($formData["current_page"] == "user_verification") {
            $redirect_page = "verification";
        }
        //echo $check."  --  ".$roleId; exit;
        if ($check != 1 && $roleId != 0) {
            //$roleId = $request->get('role');
            $roles = array($roleId);
            $userObject->roles()->attach($roles);
            $userObject->save();
        }
        //echo $userObject->hasRole('Student'); exit;
        $edituser->emergency_contact_name = isset($formData["emergency_contact_name"]) ? $formData["emergency_contact_name"] : "";
        $edituser->emergency_contact_phone = isset($formData["emergency_contact_phone"]) ? $formData["emergency_contact_phone"] : "";
        $edituser->emergency_contact_email = isset($formData["emergency_contact_email"]) ? $formData["emergency_contact_email"] : "";
        $edituser->emergency_contact_relationship = isset($formData["emergency_contact_relationship"]) ? $formData["emergency_contact_relationship"] : "";
        if (isset($formData["current_page"]) && $formData["current_page"] == "basic_info") {
            $dob = $request->get('bday') . "/" . $request->get('bmonth') . "/" . $request->get('byear');
            $edituser->name = isset($formData["name"]) ? $formData["name"] : "";
            $edituser->lastname = isset($formData["lastname"]) ? $formData["lastname"] : "";
            $edituser->dob = $dob;
            $edituser->gender = isset($formData["gender"]) ? $formData["gender"] : "";
            $edituser->basicpob = isset($formData["basicpob"]) ? $formData["basicpob"] : "";
            $edituser->country_id = isset($formData["country_id"]) ? $formData["country_id"] : "";
            $edituser->phonenumber = isset($formData["phonenumber"]) ? $formData["phonenumber"] : "";
            $edituser->NIN18 = isset($formData["NIN18"]) ? $formData["NIN18"] : "";
        }
        // updating extra info
        else if (isset($formData["current_page"]) && $formData["current_page"] == "extra_info") {
            //return $request->all();
            $edituser->NIN18 = isset($formData["NIN18"]) ? $formData["NIN18"] : "";
            $user_extrainfo = \App\UserExtraInfo::where("user_id", \Auth::user()->id)->first();
            if (!isset($user_extrainfo->id)) {
                $user_extrainfo = new \App\UserExtraInfo();
                $user_extrainfo->user_id = \Auth::user()->id;
            }
            $user_extrainfo->second_citizenship = $request->get("second_citizenship");
            $user_extrainfo->native_language_speaker = $request->get("native_language_speaker");
            $user_extrainfo->second_language_speaker = $request->get("second_language_speaker");
            $user_extrainfo->third_language_speaker = $request->get("third_language_speaker");
            $user_extrainfo->description_more_languages = $request->get("description_more_languages");
            $user_extrainfo->member_assisting_you_name = $request->get("member_assisting_you_name");
            $user_extrainfo->member_assisting_you_surname = $request->get("member_assisting_you_surname");
            $user_extrainfo->member_assisting_you_email = $request->get("member_assisting_you_email");
            $user_extrainfo->save();
        }
        // user verification
        else if (isset($formData["current_page"]) && $formData["current_page"] == "user_verification") {
            //return $request->all();
            $user_verification = \App\UserVerification::where("user_id", \Auth::user()->id)->first();
            if (!isset($user_verification->id)) {
                $user_verification = new \App\UserVerification();
                $user_verification->user_id = \Auth::user()->id;
            }
            $user_verification->personal_id_kind = $request->get("personal_id_kind");
            $user_verification->personal_id_expiry_date = $request->get("personal_id_expiry_date");
            // if (!empty($request->file("personal_id_picture_front"))) {
            //     $pic_front_file = $request->file('personal_id_picture_front');
            //     //getting timestamp
            //     $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            //     $img_front_name = "pic_front-" . $timestamp . '-' . $pic_front_file->getClientOriginalName();
            //     //$image->filePath = $img_name;
            //     $pic_front_file->move(public_path() . "/images/extra_info/", $img_front_name);
            //     $user_verification->personal_id_picture_front = $img_front_name;
            // }
            // if (!empty($request->file("personal_id_picture_back"))) {
            //     $pic_back_file = $request->file('personal_id_picture_back');
            //     //getting timestamp
            //     $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            //     $img_back_name = "pic_back-" . $timestamp . '-' . $pic_back_file->getClientOriginalName();
            //     //$image->filePath = $img_name;
            //     $pic_back_file->move(public_path() . "/images/extra_info/", $img_back_name);
            //     $user_verification->personal_id_picture_back = $img_back_name;
            // }
            $user_verification->criminal_check_done = $request->get("criminal_check_done");
            $user_verification->criminal_check_last_done_year = $request->get("criminal_check_last_done_year");
            // if (!empty($request->file("criminal_check_image"))) {
            //     $pic_criminal_file = $request->file('criminal_check_image');
            //     //getting timestamp
            //     $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            //     $img_criminal_name = "pic_criminal-" . $timestamp . '-' . $pic_criminal_file->getClientOriginalName();
            //     //$image->filePath = $img_name;
            //     $pic_criminal_file->move(public_path() . "/images/extra_info/", $img_criminal_name);
            //     $user_verification->criminal_check_image = $img_criminal_name;
            // }
            $user_verification->is_active = 0;
            $user_verification->save();
        }
        // Student Guest (over 18)
        else if (isset($formData["current_page"]) && $formData["current_page"] == "student_guest") {

            $edituser->about_yourself = isset($formData["about_yourself"]) ? $formData["about_yourself"] : "";
            $edituser->medical_learning = isset($formData["medical_learning"]) ? $formData["medical_learning"] : "";
            $edituser->host_family_notes = isset($formData["family_notes18"]) ? $formData["family_notes18"] : "";
            $edituser->work_tutor_notes = isset($formData["tutor_notes18"]) ? $formData["tutor_notes18"] : "";
            $edituser->academic_qualifications = isset($formData["academic_qualifications"]) ? $formData["academic_qualifications"] : "";
            $edituser->date_of_attainment = isset($formData["date_of_attainment"]) ? $formData["date_of_attainment"] : "";
            $edituser->raddress18 = isset($formData["over18raddress"]) ? $formData["over18raddress"] : "";
            $edituser->street_numberr18 = isset($formData["street_numbersr18"]) ? $formData["street_numbersr18"] : "";
            $edituser->router18 = isset($formData["routesr18"]) ? $formData["routesr18"] : "";
            $edituser->localityr18 = isset($formData["localitysr18"]) ? $formData["localitysr18"] : "";
            $edituser->administrative_area_level_1r18 = isset($formData["administrative_area_level_1sr18"]) ? $formData["administrative_area_level_1sr18"] : "";
            $edituser->postal_coder18 = isset($formData["postal_codesr18"]) ? $formData["postal_codesr18"] : "";
            $edituser->countryr18 = isset($formData["countrysr18"]) ? $formData["countrysr18"] : "";
            if ($request->get('s_res_dom_same') == 'on') {
                $s_res_dom_same = 0;
                $edituser->daddress18 = isset($formData["s18domicileaddress"]) ? $formData["s18domicileaddress"] : "";
                $edituser->street_numberd18 = isset($formData["street_numbersd18"]) ? $formData["street_numbersd18"] : "";
                $edituser->routed18 = isset($formData["routesd18"]) ? $formData["routesd18"] : "";
                $edituser->localityd18 = isset($formData["localitysd18"]) ? $formData["localitysd18"] : "";
                $edituser->administrative_area_level_1d18 = isset($formData["administrative_area_level_1sd18"]) ? $formData["administrative_area_level_1sd18"] : "";
                $edituser->countryd18 = isset($formData["countrysd18"]) ? $formData["countrysd18"] : "";
                $edituser->postal_coded18 = isset($formData["postal_codesd18"]) ? $formData["postal_codesd18"] : "";
                $edituser->s_res_dom_same = $s_res_dom_same = 0;

                $edituser->daddress16 = isset($formData["s18domicileaddress"]) ? $formData["s18domicileaddress"] : "";
                $edituser->street_numberd16 = isset($formData["street_numbersd18"]) ? $formData["street_numbersd18"] : "";
                $edituser->routed16 = isset($formData["routesd18"]) ? $formData["routesd18"] : "";
                $edituser->localityd16 = isset($formData["localitysd18"]) ? $formData["localitysd18"] : "";
                $edituser->administrative_area_level_1d16 = isset($formData["administrative_area_level_1sd18"]) ? $formData["administrative_area_level_1sd18"] : "";
                $edituser->countryd16 = isset($formData["countrysd18"]) ? $formData["countrysd18"] : "";
                $edituser->postal_coded16 = isset($formData["postal_codesd18"]) ? $formData["postal_codesd18"] : "";
                $edituser->s16_res_dom_same = $s_res_dom_same;
            } else {
                $edituser->s_res_dom_same = 1;
                $edituser->s16_res_dom_same = 1;
            }
            $edituser->school_name = isset($formData["school_name"]) ? $formData["school_name"] : "";
            $edituser->class_letter = isset($formData["class_letter"]) ? $formData["class_letter"] : "";
            $edituser->class_number = isset($formData["class_number"]) ? $formData["class_number"] : "";
            $edituser->student_number = isset($formData["student_number18"]) ? $formData["student_number18"] : "";
            $edituser->tutor_name = isset($formData["tutor_name"]) ? $formData["tutor_name"] : "";
            $edituser->tutor_pob = isset($formData["tutor_pob"]) ? $formData["tutor_pob"] : "";
            $edituser->tutor_surname = isset($formData["tutor_surname"]) ? $formData["tutor_surname"] : "";
            $edituser->tutor_pan = isset($formData["tutor_pan"]) ? $formData["tutor_pan"] : "";
            $edituser->tutor_email = isset($formData["tutor_email"]) ? $formData["tutor_email"] : "";
            $edituser->tutor_phone = isset($formData["tutor_phone"]) ? $formData["tutor_phone"] : "";
            $edituser->tutor_emergency_phone = isset($formData["tutor_emergency_phone"]) ? $formData["tutor_emergency_phone"] : "";
            $edituser->school_tutor_name = isset($formData["school_tutor_name"]) ? $formData["school_tutor_name"] : "";
            $edituser->school_tutor_email = isset($formData["school_tutor_email"]) ? $formData["school_tutor_email"] : "";
            $edituser->school_tutor_number = isset($formData["school_tutor_number"]) ? $formData["school_tutor_number"] : "";
            $edituser->secondary_group_id = isset($formData["secondary_group_id"]) ? $formData["secondary_group_id"] : "";
        }
        // Parent of Under 18 address and info
        else if (isset($formData["current_page"]) && $formData["current_page"] == "pre_enrolment") {
            $edituser->sname16 = isset($formData["sname16"]) ? $formData["sname16"] : "";
            $edituser->ssurname16 = isset($formData["ssurname16"]) ? $formData["ssurname16"] : "";
            $edituser->semail16 = isset($formData["semail16"]) ? $formData["semail16"] : "";
            $edituser->sphone16 = isset($formData["sphone16"]) ? $formData["sphone16"] : "";
            $edituser->school_name = isset($formData["school_name16"]) ? $formData["school_name16"] : "";
            $edituser->class_letter = isset($formData["class_letter16"]) ? $formData["class_letter16"] : "";
            $edituser->student_number = isset($formData["student_number16"]) ? $formData["student_number16"] : "";
            $edituser->class_number = isset($formData["class_number16"]) ? $formData["class_number16"] : "";
            $edituser->secondary_group_id = isset($formData["secondary_group_parent_id"]) ? $formData["secondary_group_parent_id"] : "";
        }
        // Parent of Over 18 address and info
        else if (isset($formData["current_page"]) && $formData["current_page"] == "pre_enrolment") {
            // $edituser->sname16 = isset($formData["sname16"]) ? $formData["sname16"] : "";
            // $edituser->ssurname16 = isset($formData["ssurname16"]) ? $formData["ssurname16"] : "";
            // $edituser->semail16 = isset($formData["semail16"]) ? $formData["semail16"] : "";
            // $edituser->sphone16 = isset($formData["sphone16"]) ? $formData["sphone16"] : "";
            $edituser->school_name = isset($formData["school_name16"]) ? $formData["school_name16"] : "";
            $edituser->class_letter = isset($formData["class_letter16"]) ? $formData["class_letter16"] : "";
            $edituser->class_number = isset($formData["class_number16"]) ? $formData["class_number16"] : "";
            // $edituser->student_number = isset($formData["student_number16"]) ? $formData["student_number16"] : "";
            $edituser->secondary_group_id = isset($formData["secondary_group_parent_id"]) ? $formData["secondary_group_parent_id"] : "";
        }
        // Parent of Under 18 address and info        
        else if (isset($formData["current_page"]) && $formData["current_page"] == "parent_under_18") {
            $edituser->student_relationship = isset($formData["student_relationship"]) ? $formData["student_relationship"] : "";
            $allowcheck = 0;
            if ($request->get('check_allow') == 'on') {
                $allowcheck = 1;
            }
            $edituser->check_allow = $allowcheck; // here for me to unerstand, thanks. I insist, what is problem_ okay sure Why are you delesometingH_ow that is un-useable just completed that before from below line
            // as If you deleist e howcan I see wrong code_
            // i said i have copied that code I see now
            // as the problem is this check is removed but i dont know how and who have remove this storing checkbox as last time when we add on localhost
            // It worked on esp but not on info user when I checked after receiving complaint. perhaps not refreshed page.
            // after refreshing it didn-t work on either, so change must be very recent, like yesterday yes but we didnt use this function but let me upload and check
            $edituser->check_minors_allow = !empty($request->get('check_minors_allow')) ? $request->get("check_minors_allow") : 0;
            $edituser->check_same_nationality_allow = !empty($request->get('check_same_nationality_allow')) ? $request->get("check_same_nationality_allow") : 0;
            $edituser->raddress18 = isset($formData["parentaddress"]) ? $formData["parentaddress"] : "";
            $edituser->street_numberr18 = isset($formData["street_numberp"]) ? $formData["street_numberp"] : "";
            $edituser->router18 = isset($formData["routep"]) ? $formData["routep"] : "";
            $edituser->localityr18 = isset($formData["localityp"]) ? $formData["localityp"] : "";
            $edituser->administrative_area_level_1r18 = isset($formData["administrative_area_level_1p"]) ? $formData["administrative_area_level_1p"] : "";
            $edituser->countryr18 = isset($formData["countryp"]) ? $formData["countryp"] : "";
            $edituser->postal_coder18 = isset($formData["postal_codep"]) ? $formData["postal_codep"] : "";
            /* Domicile Address for Parent of under 18 student */
            if ($request->get('p_res_dom_same') == 'on') {
                $s_res_dom_same = 0;
                $edituser->daddress18 = isset($formData["pdomicileaddress"]) ? $formData["pdomicileaddress"] : "";
                $edituser->street_numberd18 = isset($formData["street_numberdp"]) ? $formData["street_numberdp"] : "";
                $edituser->routed18 = isset($formData["routedp"]) ? $formData["routedp"] : "";
                $edituser->localityd18 = isset($formData["localitydp"]) ? $formData["localitydp"] : "";
                $edituser->administrative_area_level_1d18 = isset($formData["administrative_area_level_1dp"]) ? $formData["administrative_area_level_1dp"] : "";
                $edituser->countryd18 = isset($formData["countrydp"]) ? $formData["countrydp"] : "";
                $edituser->postal_coded18 = isset($formData["postal_codedp"]) ? $formData["postal_codedp"] : "";
                $edituser->s_res_dom_same = $s_res_dom_same;
            } else {
                $edituser->s_res_dom_same = 1;
            }
            $edituser->sname16 = isset($formData["sname16"]) ? $formData["sname16"] : "";
            $edituser->ssurname16 = isset($formData["ssurname16"]) ? $formData["ssurname16"] : "";
            $edituser->semail16 = isset($formData["semail16"]) ? $formData["semail16"] : "";
            $edituser->sphone16 = isset($formData["sphone16"]) ? $formData["sphone16"] : "";
            $edituser->gender16 = isset($formData["gender16"]) ? $formData["gender16"] : "";
            $dob16 = $request->get('bday16') . "/" . $request->get('bmonth16') . "/" . $request->get('byear16');
            $edituser->dob16 = $dob16;
            $edituser->pob16 = isset($formData["pob16"]) ? $formData["pob16"] : "";
            $edituser->NIN16 = isset($formData["NIN16"]) ? $formData["NIN16"] : "";
            $edituser->host_family_notes = isset($formData["host_family_notes"]) ? $formData["host_family_notes"] : "";
            $edituser->work_tutor_notes = isset($formData["work_tutor_notes"]) ? $formData["work_tutor_notes"] : "";
            $edituser->about_yourself = isset($formData["profile-about16"]) ? $formData["profile-about16"] : "";
            $edituser->medical_learning = isset($formData["medical_learning16"]) ? $formData["medical_learning16"] : "";
            $edituser->academic_qualifications = isset($formData["academic_qualifications16"]) ? $formData["academic_qualifications16"] : "";
            $edituser->date_of_attainment = isset($formData["date_of_attainment16"]) ? $formData["date_of_attainment16"] : "";
            $edituser->raddress16 = $request->get('sraddress16');
            $edituser->street_numberr16 = $request->get('street_numbersr16');
            $edituser->router16 = isset($formData["routesr16"]) ? $formData["routesr16"] : "";
            $edituser->localityr16 = isset($formData["localitysr16"]) ? $formData["localitysr16"] : "";
            $edituser->administrative_area_level_1r16 = isset($formData["administrative_area_level_1sr16"]) ? $formData["administrative_area_level_1sr16"] : "";
            $edituser->countryr16 = isset($formData["countrysr16"]) ? $formData["countrysr16"] : "";
            $edituser->postal_coder16 = isset($formData["postal_codesr16"]) ? $formData["postal_codesr16"] : "";
            /* Student16 Domicilary Address */
            if ($request->get('s16_res_dom_same') == 'on') {
                $s16_res_dom_same = 0;
                $edituser->daddress16 = isset($formData["daddress16"]) ? $formData["daddress16"] : "";
                $edituser->street_numberd16 = isset($formData["street_numberd16"]) ? $formData["street_numberd16"] : "";
                $edituser->routed16 = isset($formData["routed16"]) ? $formData["routed16"] : "";
                $edituser->localityd16 = isset($formData["localityd16"]) ? $formData["localityd16"] : "";
                $edituser->administrative_area_level_1d16 = isset($formData["administrative_area_level_1d16"]) ? $formData["administrative_area_level_1d16"] : "";
                $edituser->countryd16 = isset($formData["countryd16"]) ? $formData["countryd16"] : "";
                $edituser->postal_coded16 = isset($formData["postal_coded16"]) ? $formData["postal_coded16"] : "";
                $edituser->s16_res_dom_same = $s16_res_dom_same;
            } else {
                $edituser->s16_res_dom_same = 1;
            }


            $edituser->school_name = isset($formData["school_name16"]) ? $formData["school_name16"] : "";
            $edituser->class_letter = isset($formData["class_letter16"]) ? $formData["class_letter16"] : "";
            $edituser->student_number = isset($formData["student_number16"]) ? $formData["student_number16"] : "";
            $edituser->class_number = isset($formData["class_number16"]) ? $formData["class_number16"] : "";
            // school tutor
            $edituser->tutor_name = isset($formData["tutor_name16"]) ? $formData["tutor_name16"] : "";
            $edituser->tutor_pob = isset($formData["tutor_pob16"]) ? $formData["tutor_pob16"] : "";
            $edituser->tutor_surname = isset($formData["tutor_surname16"]) ? $formData["tutor_surname16"] : "";
            $edituser->tutor_pan = isset($formData["tutor_pan16"]) ? $formData["tutor_pan16"] : "";
            $edituser->tutor_email = isset($formData["tutor_email16"]) ? $formData["tutor_email16"] : "";
            $edituser->tutor_phone = isset($formData["tutor_phone16"]) ? $formData["tutor_phone16"] : "";
            $edituser->tutor_emergency_phone = isset($formData["tutor_emergency_phone16"]) ? $formData["tutor_emergency_phone16"] : "";
            // Accompanying you
            $edituser->school_tutor_name = isset($formData["school_tutor_name16"]) ? $formData["school_tutor_name16"] : "";
            $edituser->school_tutor_email = isset($formData["school_tutor_email16"]) ? $formData["school_tutor_email16"] : "";
            $edituser->school_tutor_number = isset($formData["school_tutor_number16"]) ? $formData["school_tutor_number16"] : "";
            $edituser->secondary_group_id = isset($formData["secondary_group_parent_id"]) ? $formData["secondary_group_parent_id"] : "";
        }
        // Work Experience
        else if (isset($formData["current_page"]) && $formData["current_page"] == "work_experience") {
            $edituser->orgname = isset($formData["orgname"]) ? $formData["orgname"] : "";
            $edituser->aboutorg = isset($formData["aboutorg"]) ? $formData["aboutorg"] : "";
            $edituser->legal_nature = isset($formData["legal_nature"]) ? $formData["legal_nature"] : "";
            $edituser->org_lat = isset($formData["orglatbox"]) ? $formData["orglatbox"] : "";
            $edituser->org_lan = isset($formData["orglonbox"]) ? $formData["orglonbox"] : "";
            $edituser->orghr = isset($formData["orghr"]) ? $formData["orghr"] : "";
            $edituser->orgemail = isset($formData["orgemail"]) ? $formData["orgemail"] : "";
            $edituser->orgphone = isset($formData["orgphone"]) ? $formData["orgphone"] : "";
            $edituser->raddress18 = isset($formData["orgaddress"]) ? $formData["orgaddress"] : "";
            $edituser->street_numberr18 = isset($formData["street_numberorg"]) ? $formData["street_numberorg"] : "";
            $edituser->router18 = isset($formData["routeorg"]) ? $formData["routeorg"] : "";
            $edituser->localityr18 = isset($formData["localityorg"]) ? $formData["localityorg"] : "";
            $edituser->administrative_area_level_1r18 = isset($formData["administrative_area_level_1org"]) ? $formData["administrative_area_level_1org"] : "";
            $edituser->countryr18 = isset($formData["countryorg"]) ? $formData["countryorg"] : "";
            $edituser->postal_coder18 = isset($formData["postal_codeorg"]) ? $formData["postal_codeorg"] : "";
            $edituser->why_fit = isset($formData["why_fit"]) ? $formData["why_fit"] : "";
            $edituser->tutorrelation = isset($formData["tutorrelation"]) ? $formData["tutorrelation"] : "";
            $edituser->organisation_tax_registration_number = isset($formData["organisation_tax_registration_number"]) ? $formData["organisation_tax_registration_number"] : "";
            $edituser->link_to_extra_docs_necessary = isset($formData["link_to_extra_docs_necessary"]) ? $formData["link_to_extra_docs_necessary"] : "";
            if (!empty($request->get("is_legal_representative")) && $request->get("is_legal_representative") == 1) {
                $user_legal = \App\UserLegalRepresentative::where("user_id", \Auth::user()->id)->first();
                if (!isset($user_legal->id)) {
                    $user_legal = new \App\UserLegalRepresentative();
                    $user_legal->user_id = \Auth::user()->id;
                }
                $user_legal->is_legal_representative = 1;
                $user_legal->legal_representative_name = $request->get("legal_representative_name");
                $user_legal->legal_representative_surname = $request->get("legal_representative_surname");
                $user_legal->legal_representative_dob = $request->get("legal_representative_dob");
                $user_legal->legal_representative_birth_place = $request->get("legal_representative_birth_place");
                $user_legal->save();
            } else {
                $user_legal = \App\UserLegalRepresentative::where("user_id", \Auth::user()->id)->first();
                if (!isset($user_legal->id)) {
                    $user_legal = new \App\UserLegalRepresentative();
                    $user_legal->user_id = \Auth::user()->id;
                }
                $user_legal->is_legal_representative = 0;
                $user_legal->legal_representative_name = "";
                $user_legal->legal_representative_surname = "";
                $user_legal->legal_representative_dob = "";
                $user_legal->legal_representative_birth_place = "";
                $user_legal->save();
            }
        }
        //paid services
        else if (isset($formData["current_page"]) && $formData["current_page"] == "paid_services") {
            $paid_status = $this->save_paid_services($request, $edituser->id);
        }
        // else if(){

        // }

        if ($edituser->save()) {
            //$user = $this->userRepository->updateUser($postData);
            // role 4
            if (isset($formData["current_page"]) && $formData["current_page"] == "work_experience") {
                $datalatlon = array(
                    'legal_nature' => $request->get('legal_nature'),
                    'room_type' => $request->get('orghr'),
                    'title' => $request->get('orgname'),
                    'represent_name' => $request->get('name'),
                    'represent_surname' => $request->get('lastname'),
                    'represent_born' => $request->get('basicpob'),
                    'represent_email' => \Auth::user()->email,
                    'represent_phone' => $request->get('phonenumber'),
                    'represent_tax_no' => $request->get('NIN18'),
                    'work_tutor_fit' => $request->get('why_fit'),
                    'relation_with_company' => $request->get('tutorrelation'),
                    'pro_lat' => $request->get('orglatbox'),
                    'pro_lon' => $request->get('orglonbox')
                );

                $this->propertyRepository->updateaddressinproperty(\Auth::user()->id, $datalatlon);
            }
            if (isset($paid_status) && $paid_status == false) {
                flash('The email of the person you indicated as Member who Referred or is Assisting you is not present in our system, please check the email you have written is correct, if it is, please contact us. Profile has been updates successfully, thanks!', 'success');
//                flash('This person is not a Member, please check the email you have written is correct, if it is, please contact us.', 'referral_error');
                return \Redirect::to('user/' . $redirect_page)->with("referral_error", "The email of the person you indicated as Member who Referred or is Assisting you is not present in our system, please check the email you have written is correct, if it is, please contact us.");
            } else {
                flash('Profile Updated Successfully', 'success');
                return \Redirect::to('user/' . $redirect_page);
            }
        } else {
            flash('Some problem occured while saving', 'error');
            return \Redirect::to('user/' . $redirect_page);
        }
    }

    public function save_paid_services($request, $user_id) {
        if (!empty($request)) {
            $paid_status = true;
            if (!empty($request->get("paid_services_id")) && $request->get("paid_services_id") > 0) {
                $paid_service = \App\UserPaidService::where('id', $request->get("paid_services_id"))->first();
            } else {
                $paid_service = new \App\UserPaidService();
            }
            // echo "<pre>";
            // print_r($_REQUEST);
            // print_r($request);
            // echo "</pre>";
            // exit;

            //paid services basic info / Now tab @School House@ 
            if ($request->get("paid_service_tab") == "basics") {
                $paid_service->user_id = $user_id;
                $paid_service->house_name = $request->get('house_name');
                $paid_service->house_slogan = $request->get('house_slogan');
                $paid_service->house_desc = $request->get('house_desc');
                $paid_service->house_host_desc = $request->get('house_host_desc');
                $paid_service->houseaddress = $request->get('houseaddress');
                $paid_service->host_experience = $request->get('host_experience');
                $hostPreference = $request->get('host_preference');
                $hostPreferenceText = "";
                if (is_array($hostPreference)) {
                    foreach ($hostPreference as $value) {
                        $hostPreferenceText .= $value . ",";
                    }
                }
                if ($this->checkCommaExists($hostPreferenceText)) {
                    $hostPreferenceText = $this->trimLastCharacter($hostPreferenceText);
                }
                $paid_service->host_preference = $hostPreferenceText;
                $teaching_skills = $request->get('teaching_skills');
                $teaching_skillsText = "";
                if (is_array($teaching_skills)) {
                    foreach ($teaching_skills as $skill) {
                        $teaching_skillsText .= $skill . ",";
                    }
                }
                if ($this->checkCommaExists($teaching_skillsText)) {
                    $teaching_skillsText = $this->trimLastCharacter($teaching_skillsText);
                }
                $paid_service->teaching_skills = $teaching_skillsText;
                $paid_service->street_numberh = $request->get('street_numberh');
                $paid_service->routeh = $request->get('routeh');
                $paid_service->localityh = $request->get('localityh');
                $paid_service->administrative_area_level_1h = $request->get('administrative_area_level_1h');
                $paid_service->postal_codeh = $request->get('postal_codeh');
                $paid_service->countryh = $request->get('countryh');
                $paid_service->houselatbox = $request->get('houselatbox');
                $paid_service->houselonbox = $request->get('houselonbox');

                if ($request->get('house_property_differ') == 'on') {
                    $house_property_differ = 1;
                    $paid_service->house_property_differ = $house_property_differ;
                    $paid_service->propertyaddress = $request->get('propertyaddress');
                    $paid_service->street_numberp = $request->get('street_numberp');
                    $paid_service->routep = $request->get('routep');
                    $paid_service->localityp = $request->get('localityp');
                    $paid_service->administrative_area_level_1p = $request->get('administrative_area_level_1p');
                    $paid_service->postal_codep = $request->get('postal_codep');
                    $paid_service->countryp = $request->get('countryp');
                    $paid_service->proplatbox = $request->get('proplatbox');
                    $paid_service->proplonbox = $request->get('proplonbox');
                } else {
                    $paid_service->house_property_differ = "0";
                }

                $user_referral = \App\UserReferral::where("user_id", $user_id)->first();
                if (isset($user_referral->email)) {
                    $check_referral_email = !empty($user_referral->email) ? $user_referral->email : "empty";
                }
                if (!isset($user_referral->id)) {
                    $user_referral = new \App\UserReferral();
                }
                $user_referral->user_id = $user_id;
                $user_referral->name = $request->get("referral_name");
                $user_referral->surname = $request->get("referral_surname");
                $user_referral->email = $request->get("referral_email");
                $user_referral->referral_referr_me = $request->get("referral_referr_me");
                $user_referral->is_assisting_me = $request->get("is_assisting_me");
                $check_user = \App\User::where("email", $request->get("referral_email"))->first();
                if (isset($check_user->id)) {
                    $user_referral->user_is_exist = "1";
                    $user_referral->referral_user_id = $check_user->id;
                } else {
                    // the code for error message after saving the 
                    $paid_status = false;
                    $user_referral->user_is_exist = "0";
                    $user_referral->referral_user_id = "0";
                }
                $user_referral->save();
                if (isset($paid_status) && $paid_status == false) {
                    if (!empty($user_referral->email)) {
                        if (isset($user_referral->is_send_wrong_referral_email) && $user_referral->is_send_wrong_referral_email == 0) {
                            if (!empty($check_referral_email) && $check_referral_email != $request->get("referral_email")) {
                                $user = \App\User::where("id", $user_id)->first();
                                $this->mailNotificationsForUser->sendReferralWrongEmailNotification($user_referral, $user);
                                $user_referral->is_send_wrong_referral_email = 1;
                                $user_referral->save();
                            }
                        } else if (!empty($check_referral_email) && $check_referral_email != $request->get("referral_email")) {
                            $user = \App\User::where("id", $user_id)->first();
                            $this->mailNotificationsForUser->sendReferralWrongEmailNotification($user_referral, $user);
                            $user_referral->is_send_wrong_referral_email = 1;
                            $user_referral->save();
                        }
                    } else {
                        if (isset($user_referral->is_send_no_referral_email) && $user_referral->is_send_no_referral_email == 0) {
                            $user = \App\User::where("id", $user_id)->first();
                            $this->mailNotificationsForUser->sendReferralNoEmailNotification($user_referral, $user);
                            $user_referral->is_send_no_referral_email = 1;
                            $user_referral->save();
                        }
                    }
                }
            }
            //paid serivces extra info
            else if ($request->get("paid_service_tab") == "extra") {
                $localAmenities = $request->get('local_amenity');
                $localAmenitiesText = "";
                if (is_array($localAmenities)) {
                    foreach ($localAmenities as $value) {
                        $localAmenitiesText .= $value . ",";
                    }
                }
                if ($this->checkCommaExists($localAmenitiesText)) {
                    $localAmenitiesText = $this->trimLastCharacter($localAmenitiesText);
                }

                $paid_service->airport_way = $request->get('airport_way');
                $paid_service->home_way = $request->get('home_way');
                $paid_service->mobile_use = $request->get('mobile_use');
                $paid_service->amenity_desc = $request->get('amenity_desc');
                $paid_service->house_rules = $request->get('house_rules');

                $paid_service->other_house_rules = $request->get('other_house_rules');
                $paid_service->home_type = $request->get('home_type');
                $paid_service->urban_location = $request->get('urban_location');
                $paid_service->property_region = $request->get('property_region');

                $paid_service->local_amenity = $localAmenitiesText;

                $paid_service->pdf_document = $request->get('pdf_document');

                // add bank info
                $check_bank_detail = \App\UserBankDetail::where("user_id", $user_id)->first();
                if (isset($check_bank_detail->id) && $check_bank_detail->id > 0) {
                    $bank_detail = $check_bank_detail;
                } else {
                    $bank_detail = new \App\UserBankDetail();
                }
                $bank_detail->user_id = $user_id;
                $bank_detail->account_holder_name = $request->get("account_holder_name");
                $bank_detail->bank_name = $request->get("bank_name");
                $bank_detail->iban = $request->get("iban");
                $bank_detail->swift_code = $request->get("swift_code");
                $bank_detail->sortCode = $request->get("sortCode1") . "-" . $request->get("sortCode2") . "-" . $request->get("sortCode3");
                $bank_detail->accountNumber = $request->get("accountNumber");
                $bank_detail->receiverType = "Private";
                $bank_detail->amountCurrency = "GBP";
                $bank_detail->sourceCurrency = "GBP";
                $bank_detail->targetCurrency = "GBP";
                $bank_detail->save();
            }
            //paid services property Info
            else if ($request->get("paid_service_tab") == "property_info") {
                // $file = $request->file('property_image_paid');
                // if (!empty($file)) {
                //     // echo "Request :<pre>";
                //     //     print_r($file);
                //     //     print_r($_FILES);
                //     //     print_r($_REQUEST);
                //     // echo "</pre>";
                //     // exit;
                //     //getting timestamp
                //     $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                //     $img_name = $timestamp . '-' . $file->getClientOriginalName();
                //     //$image->filePath = $img_name;
                //     $file->move(public_path() . "/images/schoolhouse/$user_id", $img_name);
                //     $postData = array('img_name' => $img_name, 'user_id' => $user_id);
                //     \App\Schoolhouseimage::create($postData);
                // }

                $paid_service->home_type = $request->get('home_type');
                $paid_service->property_desc = $request->get('property_desc');

                $facilities = $request->get('facilities');
                $facilitiesText = "";
                if (is_array($facilities)) {
                    foreach ($facilities as $value) {
                        $facilitiesText .= $value . ",";
                    }
                }
                if ($this->checkCommaExists($facilitiesText)) {
                    $facilitiesText = $this->trimLastCharacter($facilitiesText);
                }
                $paid_service->facilities = $facilitiesText;
                $paid_service->facility_desc = $request->get('facility_desc');
                $paid_service->smoking = $request->get('smoking');
                $paid_service->time_limits = $request->get('time_limits');
                $paid_service->key_given = $request->get('key_given');
                $paid_service->guest_table = $request->get('guest_table');
                $paid_service->property_slogan = $request->get('property_slogan');
                $paid_service->room_no = $request->get('room_no');
                $paid_service->guest_room_no = $request->get('guest_room_no');
                $paid_service->toilet_no = $request->get('toilet_no');
                $paid_service->shower_no = $request->get('shower_no');
                $paid_service->bath_no = $request->get('bath_no');

                // adding multiple guest rooms
                $room_name = $request->get("room_name");
//                echo "<pre>";
//                print_r($request->get('room_facility'));
//                echo "</pre>";
//                exit;
                if (is_array($room_name) && isset($room_name[0]) && !empty($room_name[0])) {
                    \App\UserPaidGuestRoom::where("user_id", $user_id)->delete();
                    foreach ($room_name as $r_key => $r_name) {
                        if (!empty($r_name)) {
                            $guest_room = new \App\UserPaidGuestRoom();
                            $guest_room->user_id = $user_id;
                            $guest_room->room_name = $r_name;
                            $guest_room->room_type_id = $request->get("room_type_id")[$r_key];
                            $guest_room->room_description = $request->get("room_description")[$r_key];
                            $room_facilities = $request->get('room_facility')[$r_key];
                            $roomFacilitiesText = "";
                            if (is_array($room_facilities)) {
                                foreach ($room_facilities as $facility) {
                                    $roomFacilitiesText .= $facility . ",";
                                }
                            }
                            if ($this->checkCommaExists($roomFacilitiesText)) {
                                $roomFacilitiesText = $this->trimLastCharacter($roomFacilitiesText);
                            }
                            $guest_room->room_facility = $roomFacilitiesText;
                            $guest_room->room_guest_toilet = $request->get("room_guest_toilet")[$r_key];
                            $guest_room->room_guest_shower = $request->get("room_guest_shower")[$r_key];
                             
                            $guest_room->save();
                        }
                    }
                } else {
                    \App\UserPaidGuestRoom::where("user_id", $user_id)->delete();
                }
            }
            //paid service property images
            else if ($request->get("paid_service_tab") == "property_image") {
                //$userObject = $this->userRepository->getLoginUser();
            }
            //paid services family photos
            else if ($request->get("paid_service_tab") == "family_image") {
                $file = $request->file('family_photo');
                //getting timestamp
                // if (!empty($file)) {
                //     $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                //     $img_name = $timestamp . '-' . $file->getClientOriginalName();
                //     //$image->filePath = $img_name;
                //     $file->move(public_path() . "/images/family/$user_id", $img_name);
                //     $postData = array('img_name' => $img_name, 'user_id' => $user_id);
                //     \App\Schoolfamily::create($postData);
                // }
                $paid_service->family_name = $request->get('family_name');
                $paid_service->is_language = $request->get('is_language');
                $paid_service->member_desc = $request->get('member_desc');
                $paid_service->family_situation = $request->get('family_situation');
                $pet_types = $request->get('pet_types');
                $petTypesText = "";
                if (is_array($pet_types)) {
                    foreach ($pet_types as $value) {
                        $petTypesText .= $value . ",";
                    }
                }
                if ($this->checkCommaExists($petTypesText)) {
                    $petTypesText = $this->trimLastCharacter($petTypesText);
                }
                $paid_service->pet_types = $petTypesText; // add new field
                $paid_service->pet_desc = $request->get('pet_desc'); // add new field
                // adding family member relations
                $relation_name = $request->get("family_member_name");
                if (is_array($relation_name) && isset($relation_name[0]) && !empty($relation_name[0])) {
                    \App\UserPaidRelation::where("user_id", $user_id)->delete();
                    foreach ($relation_name as $r_key => $r_name) {
                        if (!empty($r_name)) {
                            $relation = new \App\UserPaidRelation();
                            $relation->user_id = $user_id;
                            $relation->name = $r_name;
                            $relation->birth_year = $request->get("family_member_birth_year")[$r_key];
                            $relation->relationship = $request->get("family_member_relation")[$r_key];
                            $relation->save();
                        }
                    }
                } else {
                    \App\UserPaidRelation::where("user_id", $user_id)->delete();
                }
            }
            //paid service included services
            else if ($request->get("paid_service_tab") == "includedServices") {
                $paid_service->service_included = $request->get('service_included');
                $paid_service->service_desc = $request->get('service_desc');
                // Activities
                $school_house_activity = $request->get('school_house_activity');
                $schoolHouseActivityText = "";
                if (is_array($school_house_activity)) {
                    foreach ($school_house_activity as $value) {
                        $schoolHouseActivityText .= $value . ",";
                    }
                }
                if ($this->checkCommaExists($schoolHouseActivityText)) {
                    $schoolHouseActivityText = $this->trimLastCharacter($schoolHouseActivityText);
                }
                $paid_service->school_house_activity = $schoolHouseActivityText; // add new field
                $paid_service->activity_desc = $request->get('activity_desc'); // add new field
                // Classes
                $school_house_classes = $request->get('school_house_classes');
                $schoolHouseClassText = "";
                if (is_array($school_house_classes)) {
                    foreach ($school_house_classes as $value) {
                        $schoolHouseClassText .= $value . ",";
                    }
                }
                if ($this->checkCommaExists($schoolHouseClassText)) {
                    $schoolHouseClassText = $this->trimLastCharacter($schoolHouseClassText);
                }
                $paid_service->school_house_classes = $schoolHouseClassText; // add new field
                $paid_service->class_desc = $request->get('class_desc'); // add new field
                // diet
                $school_house_diet = $request->get('school_house_diet');
                $schoolHouseDietText = "";
                if (is_array($school_house_diet)) {
                    foreach ($school_house_diet as $value) {
                        $schoolHouseDietText .= $value . ",";
                    }
                }
                if ($this->checkCommaExists($schoolHouseDietText)) {
                    $schoolHouseDietText = $this->trimLastCharacter($schoolHouseDietText);
                }
                $paid_service->school_house_diet = $schoolHouseDietText;
                $paid_service->diet_desc = $request->get('diet_desc');
                $paid_service->school_house_diet_board = $request->get('school_house_diet_board');
                // Transfers
                $school_house_transfers = $request->get('school_house_transfers');
                $schoolHouseTransferText = "";
                if (is_array($school_house_transfers)) {
                    foreach ($school_house_transfers as $value) {
                        $schoolHouseTransferText .= $value . ",";
                    }
                }
                if ($this->checkCommaExists($schoolHouseTransferText)) {
                    $schoolHouseTransferText = $this->trimLastCharacter($schoolHouseTransferText);
                }
                $paid_service->school_house_transfers = $schoolHouseTransferText;
                $paid_service->transfer_desc = $request->get('transfer_desc');
                // Tour
                $school_house_tours = $request->get('school_house_tours');
                $schoolHouseTourText = "";
                if (is_array($school_house_tours)) {
                    foreach ($school_house_tours as $value) {
                        $schoolHouseTourText .= $value . ",";
                    }
                }
                if ($this->checkCommaExists($schoolHouseTourText)) {
                    $schoolHouseTourText = $this->trimLastCharacter($schoolHouseTourText);
                }
                $paid_service->school_house_tours = $schoolHouseTourText;
                $paid_service->tour_desc = $request->get('tour_desc');
                // WOW
                $school_house_wows = $request->get('school_house_wows');
                $schoolHouseWowText = "";
                if (is_array($school_house_wows)) {
                    foreach ($school_house_wows as $value) {
                        $schoolHouseWowText .= $value . ",";
                    }
                }
                if ($this->checkCommaExists($schoolHouseWowText)) {
                    $schoolHouseWowText = $this->trimLastCharacter($schoolHouseWowText);
                }
                $paid_service->school_house_wows = $schoolHouseWowText;
                $paid_service->wow_desc = $request->get('wow_desc');
            }
            else if ($request->get("paid_service_tab") == "property_info_image"){
                $file = $request->file('property_image_paid');
                    echo "Request :<pre>";
                        print_r($file);
                        print_r($_FILES);
                        print_r($_REQUEST);
                    echo "</pre>";
                    exit;
                if (!empty($file)) {
                    //getting timestamp
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $img_name = $timestamp . '-' . $file->getClientOriginalName();
                    //$image->filePath = $img_name;
                    $file->move(public_path() . "/images/schoolhouse/$user_id", $img_name);
                    $postData = array('img_name' => $img_name, 'user_id' => $user_id);
                    \App\Schoolhouseimage::create($postData);
                }
            }
            $paid_service->save();
            return $paid_status;
            //exit;
        }
    }

    public function trimLastCharacter($str) {
        return rtrim($str, ",");
    }

    public function profile_submit(Request $request) {
        /* Creating Roles */
        $userObject = $this->userRepository->getLoginUser();
        $check = 0;
        /* $userwithroles = $this->userRepository->getUserRoles(\Auth::user()->id);
          $rolesarray = array();
          foreach($userwithroles->roles as $rs)  {
          array_push($rolesarray, $rs->id);
          } */
        if ($userObject->hasRole('Student') && $request->get('role') == 6) {
            $check = 1;
        }
        if ($userObject->hasRole('Organization') && $request->get('role') == 4) {
            $check = 1;
        }
        if ($userObject->hasRole('Parent') && $request->get('role') == 5) {
            $check = 1;
        }
        if ($check != 1 && $request->get('role') != 0) {
            $roleId = $request->get('role');
            $roles = array($roleId);
            $userObject->roles()->attach($roles);
            $userObject->save();
        }
        $roleDependsFormData = array();
        $dob = $request->get('bday') . "/" . $request->get('bmonth') . "/" . $request->get('byear');
        $basicinfo = array('name' => $request->get('name'),
            'lastname' => $request->get('lastname'),
            'dob' => $dob,
            'gender' => $request->get('gender'),
            'basicpob' => $request->get('basicpob'),
            'country_id' => $request->get('country_id'),
            'phonenumber' => $request->get('phonenumber'),
            'NIN18' => $request->get('NIN18'),
            'emergency_contact_name' => $request->get('emergency_contact_name'),
            'emergency_contact_phone' => $request->get('emergency_contact_phone'),
            'emergency_contact_email' => $request->get('emergency_contact_email'),
            'emergency_contact_relationship' => $request->get('emergency_contact_relationship')
        );
        if ($request->get('role') == 4) {
            $roleDependsFormData = array('orgname' => $request->get('orgname'),
                'aboutorg' => $request->get('aboutorg'),
                'legal_nature' => $request->get('legal_nature'),
                'orghr' => $request->get('orghr'),
                'orgemail' => $request->get('orgemail'),
                'orgphone' => $request->get('orgphone'),
                'raddress18' => $request->get('orgaddress'),
                'street_numberr18' => $request->get('street_numberorg'),
                'router18' => $request->get('routeorg'),
                'localityr18' => $request->get('localityorg'),
                'administrative_area_level_1r18' => $request->get('administrative_area_level_1org'),
                'countryr18' => $request->get('countryorg'),
                'postal_coder18' => $request->get('postal_codeorg'),
                'why_fit' => $request->get('why_fit'),
                'tutorrelation' => $request->get('tutorrelation'),
                'org_lat' => $request->get('orglatbox'),
                'org_lan' => $request->get('orglonbox'),
            );
            $datalatlon = array(
                'legal_nature' => $request->get('legal_nature'),
                'room_type' => $request->get('orghr'),
                'title' => $request->get('orgname'),
                'represent_name' => $request->get('name'),
                'represent_surname' => $request->get('lastname'),
                'represent_born' => $request->get('basicpob'),
                'represent_email' => \Auth::user()->email,
                'represent_phone' => $request->get('phonenumber'),
                'represent_tax_no' => $request->get('NIN18'),
                'work_tutor_fit' => $request->get('why_fit'),
                'relation_with_company' => $request->get('tutorrelation'),
                'pro_lat' => $request->get('orglatbox'),
                'pro_lon' => $request->get('orglonbox')
            );

            $this->propertyRepository->updateaddressinproperty(\Auth::user()->id, $datalatlon);
        }
        if ($request->get('role') == 6) {

            $student18Data = array(
                'sname16' => $request->get('name'),
                'ssurname16' => $request->get('lastname'),
                'semail16' => \Auth::user()->email,
                'sphone16' => $request->get('sphone16'),
                'gender16' => $request->get('gender16'),
                'dob16' => $dob,
                'pob16' => $request->get('basicpob'),
                'NIN16' => $request->get('NIN18'),
                'about_yourself' => $request->get('about_yourself'),
                'school_name' => $request->get('school_name'),
                'class_letter' => $request->get('class_letter'),
                'class_number' => $request->get('class_number'),
                'student_number' => $request->get('student_number'),
                'medical_learning' => $request->get('medical_learning'),
                'host_family_notes' => $request->get('family_notes18'),
                'work_tutor_notes' => $request->get('tutor_notes18'),
                'academic_qualifications' => $request->get('academic_qualifications'),
                'date_of_attainment' => $request->get('date_of_attainment'),
                'school_tutor_name' => $request->get('school_tutor_name'),
                'school_tutor_email' => $request->get('school_tutor_email'),
                'school_tutor_number' => $request->get('school_tutor_number'),
                'raddress18' => $request->get('over18raddress'),
                'street_numberr18' => $request->get('street_numbersr18'),
                'router18' => $request->get('routesr18'),
                'localityr18' => $request->get('localitysr18'),
                'administrative_area_level_1r18' => $request->get('administrative_area_level_1sr18'),
                'countryr18' => $request->get('countrysr18'),
                'postal_coder18' => $request->get('postal_codesr18'),
                'raddress16' => $request->get('over18raddress'),
                'street_numberr16' => $request->get('street_numbersr18'),
                'router16' => $request->get('routesr18'),
                'localityr16' => $request->get('localitysr18'),
                'administrative_area_level_1r16' => $request->get('administrative_area_level_1sr18'),
                'countryr16' => $request->get('countrysr18'),
                'postal_coder16' => $request->get('postal_codesr18'),
                'tutor_name' => $request->get('tutor_name'),
                'tutor_pob' => $request->get('tutor_pob'),
                'tutor_surname' => $request->get('tutor_surname'),
                'tutor_pan' => $request->get('tutor_pan'),
                'tutor_email' => $request->get('tutor_email'),
                'tutor_phone' => $request->get('tutor_phone'),
                'tutor_emergency_phone' => $request->get('tutor_emergency_phone'),
            );

            /* Student18 Domicilary Address */
            if ($request->get('s_res_dom_same') == 'on') {
                $s_res_dom_same = 0;
                $sdomicileaddress = array('daddress18' => $request->get('s18domicileaddress'),
                    'street_numberd18' => $request->get('street_numbersd18'),
                    'routed18' => $request->get('routesd18'),
                    'localityd18' => $request->get('localitysd18'),
                    'administrative_area_level_1d18' => $request->get('administrative_area_level_1sd18'),
                    'countryd18' => $request->get('countrysd18'),
                    'postal_coded18' => $request->get('postal_codesd18'),
                    's_res_dom_same' => $s_res_dom_same,
                    'daddress16' => $request->get('s18domicileaddress'),
                    'street_numberd16' => $request->get('street_numbersd18'),
                    'routed16' => $request->get('routesd18'),
                    'localityd16' => $request->get('localitysd18'),
                    'administrative_area_level_1d16' => $request->get('administrative_area_level_1sd18'),
                    'countryd16' => $request->get('countrysd18'),
                    'postal_coded16' => $request->get('postal_codesd18'),
                    's16_res_dom_same' => $s_res_dom_same
                );
            } else {
                $sdomicileaddress = array('s_res_dom_same' => 1, 's16_res_dom_same' => 1);
            }

            $roleDependsFormData = array_merge($student18Data, $sdomicileaddress);
        }
        if ($request->get('role') == 5) {
            //echo "yes here in role 5 = check is =>".$request->get('check_allow'); exit;
            $dob16 = $request->get('bday16') . "/" . $request->get('bmonth16') . "/" . $request->get('byear16');
            $allowcheck = 0;
            if ($request->get('check_allow') == 'on') {
                $allowcheck = 1;
            }
            $student16Data = array('sname16' => $request->get('sname16'),
                'ssurname16' => $request->get('ssurname16'),
                'semail16' => $request->get('semail16'),
                'sphone16' => $request->get('sphone16'),
                'gender16' => $request->get('gender16'),
                'dob16' => $dob16,
                'pob16' => $request->get('pob16'),
                'school_name' => $request->get('school_name16'),
                'class_letter' => $request->get('class_letter16'),
                'class_number' => $request->get('class_number16'),
                'student_number' => $request->get('student_number16'),
                'medical_learning' => $request->get('medical_learning16'),
                'academic_qualifications' => $request->get('academic_qualifications16'),
                'date_of_attainment' => $request->get('date_of_attainment16'),
                'school_tutor_name' => $request->get('school_tutor_name16'),
                'school_tutor_email' => $request->get('school_tutor_email16'),
                'school_tutor_number' => $request->get('school_tutor_number16'),
                'NIN16' => $request->get('NIN16'),
                'about_yourself' => $request->get('profile-about16'),
                'raddress16' => $request->get('sraddress16'),
                'street_numberr16' => $request->get('street_numbersr16'),
                'router16' => $request->get('routesr16'),
                'localityr16' => $request->get('localitysr16'),
                'administrative_area_level_1r16' => $request->get('administrative_area_level_1sr16'),
                'countryr16' => $request->get('countrysr16'),
                'postal_coder16' => $request->get('postal_codesr16'),
                'tutor_name' => $request->get('tutor_name16'),
                'tutor_pob' => $request->get('tutor_pob16'),
                'tutor_surname' => $request->get('tutor_surname16'),
                'tutor_pan' => $request->get('tutor_pan16'),
                'tutor_email' => $request->get('tutor_email16'),
                'tutor_phone' => $request->get('tutor_phone16'),
                'tutor_emergency_phone' => $request->get('tutor_emergency_phone16'),
            );
            /* Student16 Domicilary Address */
            if ($request->get('s16_res_dom_same') == 'on') {
                $s16_res_dom_same = 0;
                $sdomicileaddress = array('daddress16' => $request->get('daddress16'),
                    'street_numberd16' => $request->get('street_numberd16'),
                    'routed16' => $request->get('routed16'),
                    'localityd16' => $request->get('localityd16'),
                    'administrative_area_level_1d16' => $request->get('administrative_area_level_1d16'),
                    'countryd16' => $request->get('countryd16'),
                    'postal_coded16' => $request->get('postal_coded16'),
                    's16_res_dom_same' => $s16_res_dom_same
                );
            } else {
                $sdomicileaddress = array('s16_res_dom_same' => 1);
            }


            $studentFinalData = array_merge($student16Data, $sdomicileaddress);



            $parentData = array(
                'student_relationship' => $request->get('student_relationship'),
                'host_family_notes' => $request->get('host_family_notes'),
                'work_tutor_notes' => $request->get('work_tutor_notes'),
                'check_allow' => $allowcheck,
                'raddress18' => $request->get('parentaddress'),
                'street_numberr18' => $request->get('street_numberp'),
                'router18' => $request->get('routep'),
                'localityr18' => $request->get('localityp'),
                'administrative_area_level_1r18' => $request->get('administrative_area_level_1p'),
                'countryr18' => $request->get('countryp'),
                'postal_coder18' => $request->get('postal_codep'),
            );

            /* Domicile Address */
            if ($request->get('p_res_dom_same') == 'on') {
                $s_res_dom_same = 0;
                $pdomicileaddress = array('daddress18' => $request->get('pdomicileaddress'),
                    'street_numberd18' => $request->get('street_numberdp'),
                    'routed18' => $request->get('routedp'),
                    'localityd18' => $request->get('localitydp'),
                    'administrative_area_level_1d18' => $request->get('administrative_area_level_1dp'),
                    'countryd18' => $request->get('countrydp'),
                    'postal_coded18' => $request->get('postal_codedp'),
                    's_res_dom_same' => $s_res_dom_same
                );
            } else {
                $pdomicileaddress = array('s_res_dom_same' => 1);
            }

            $parentfinalarray = array_merge($parentData, $pdomicileaddress);

            $roleDependsFormData = array_merge($studentFinalData, $parentfinalarray);
        }
        $postData = array_merge($basicinfo, $roleDependsFormData);
        $user = $this->userRepository->updateUser($postData);
        flash('Profile Updated Successfully', 'success');
        return \Redirect::to('user/edit');




        /* $dob = $request->get('bday')."/".$request->get('bmonth')."/".$request->get('byear');
          $dob16 = $request->get('bday16')."/".$request->get('bmonth16')."/".$request->get('byear16');
          $checkallow =1;
          if ($request->get('check_allow') == "off"){
          $checkallow =0;
          }
          $postData = array('name' => $request->get('name'),
          'lastname' => $request->get('lastname'),
          'dob' => $dob,
          'gender' => $request->get('gender'),
          'paypalid' => $request->get('paypalid'),
          'about_yourself' => $request->get('about_yourself'),
          'emergency_contact_name' => $request->get('emergency_contact_name'),
          'emergency_contact_phone' => $request->get('emergency_contact_phone'),
          'emergency_contact_email' => $request->get('emergency_contact_email'),
          'emergency_contact_relationship' => $request->get('emergency_contact_relationship'),
          'country_id' => $request->get('country_id'),
          'phonenumber' => $request->get('phonenumber'),
          'student_relationship' => $request->get('student_relationship'),
          'NIN18' => $request->get('NIN18'),
          'daddress18' => $request->get('daddress18'),
          'street_numberd18' => $request->get('street_numberd18'),
          'routed18' => $request->get('routed18'),
          'administrative_area_level_1d18' => $request->get('administrative_area_level_1d18'),
          'localityd18' => $request->get('localityd18'),
          'postal_coded18' => $request->get('postal_coded18'),
          'countryd18' => $request->get('countryd18'),
          'daddress16' => $request->get('daddress16'),
          'street_numberd16' => $request->get('street_numberd16'),
          'routed16' => $request->get('routed16'),
          'administrative_area_level_1d16' => $request->get('administrative_area_level_1d16'),
          'localityd16' => $request->get('localityd16'),
          'postal_coded16' => $request->get('postal_coded16'),
          'countryd16' => $request->get('countryd16'),
          'raddress16' => $request->get('raddress16'),
          'street_numberr16' => $request->get('street_numberr16'),
          'router16' => $request->get('router16'),
          'administrative_area_level_1r16' => $request->get('administrative_area_level_1r16'),
          'postal_coder16' => $request->get('postal_coder16'),
          'countryr16' => $request->get('countryr16'),
          'localityr16' => $request->get('localityr16'),
          'dob16' => $dob16,
          'pob16' => $request->get('pob16'),
          'medical_learning' => $request->get('medical_learning'),
          'host_family_notes' => $request->get('host_family_notes'),
          'work_tutor_notes' => $request->get('work_tutor_notes'),
          'NIN16' => $request->get('NIN16'),
          'sname16' => $request->get('sname16'),
          'ssurname16' => $request->get('ssurname16'),
          'sphone16' => $request->get('sphone16'),
          'semail16' => $request->get('semail16'),
          'school_name' => $request->get('school_name'),
          'class_letter' => $request->get('class_letter'),
          'class_number' => $request->get('class_number'),
          'student_number' => $request->get('student_number'),
          'academic_qualifications' => $request->get('academic_qualifications'),
          'date_of_attainment' => $request->get('date_of_attainment'),
          'school_tutor_name' => $request->get('school_tutor_name'),
          'school_tutor_email' => $request->get('school_tutor_email'),
          'school_tutor_number' => $request->get('school_tutor_number'),
          'check_allow' => $checkallow,
          );
          $validator = Validator::make($postData, $this->userRepository->editValidationRules(\Auth::user()->id));
          if ($validator->fails()) {
          return \Redirect('user/edit')->withErrors($validator);
          }
          $user = $this->userRepository->updateUser($postData);
          flash('Profile Updated Successfully', 'success');
          return \Redirect::to('user/edit'); */
    }

    public function image_submit(Request $request) {
        if ($request->hasFile('profile_img_name')) {
            $file = $request->file('profile_img_name');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp . '.' . $file->getClientOriginalExtension();
            //$image->filePath = $img_name;
            $file->move(public_path() . '/images/profile', $img_name);
            $postData = array('profile_img_name' => $img_name);
            $user = $this->userRepository->updateUser($postData);
            flash('Profile Image Updated Successfully', 'success');
            return \Redirect::to('user/dashboard');
        }
    }

    public function changepassword() {
        return View('user.account_password');
    }
    
    public function wallet() {
        $user_wallet = \App\UserWallet::where("user_id", \Auth::user()->id)->first();
        return View('user.wallet', compact('user_wallet'));
    }

    public function resetpassword(Request $request) {
        $rules = array(
            'password' => 'required',
            'password_confirm' => 'required|same:password');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect('user/changepassword')->withErrors($validator);
        } else {
            $formData = array('password' => \Hash::make($request->get('password')));
            $this->userRepository->updateUser($formData);
            flash('Password Updated Successfully', 'success');
            return \Redirect('user/changepassword');
        }
    }

    public function showDashboard() {
        return View('user.dashboard');
    }

    public function viewProfileInAdmin($id) {
        $bredCrum = "User Management";
        $userobject = $this->userRepository->getById($id);
        return View('admin.user.viewprofile', compact('bredCrum', 'userobject'));
    }

    public function viewProfile($id = null) {
        if (!\Auth::check() && is_null($id)) {
            flash('Not Valid URL', 'success');
            return \Redirect('signin');
        }
        if (\Auth::check() && is_null($id)) {
            $userobj = $this->userRepository->getById(\Auth::user()->id);
            return View('user.viewprofile', compact('userobj'));
        }
        $userobj = $this->userRepository->getById($id);
        return View('user.viewprofile', compact('userobj'));
    }

    public function viewTrust() {
        return View('user.trustverify');
    }

    public function verifications() {
        $users_verifications = \App\UserVerification::get();

        $bredCrum = "User Verifications";
        return View('admin.user.verifications', compact('bredCrum', 'users_verifications'));
    }

    public function verifiy_approval_06_02_2019($verification_id = 0) {
        if ($verification_id > 0) {
            $user_verification = \App\UserVerification::where("id", $verification_id)->first();
            if ($user_verification->is_active == 0) {
                $user_verification->is_active = 1;
                $user_verification->save();
                return redirect()->to("admin/user/verifications")->with("message", $user_verification->user->name . " User Verification has been approved");
            } else {
                $user_verification->is_active = 0;
                $user_verification->save();
                return redirect()->to("admin/user/verifications")->with("error", $user_verification->user->name . " User Verification has not been approved");
            }
        } else {
            return redirect()->to("admin/user/verifications")->with("error", "There has been some mistake. Please try again later!");
        }
    }
    public function verifiy_approval($type='byId',$verification_id = 0) {
        // echo "<pre>";
        // echo "Type : ".$type."<br> Veri Id :".$verification_id;
        //     print_r($_REQUEST);
        // echo "</pre>";
        // exit;
        if ($verification_id > 0) {
            $user_verification = \App\UserVerification::where("id", $verification_id)->first();
        //     echo "<pre>";
        // // echo "Type : ".$type."<br> Veri Id :".$verification_id;
        //     print_r($user_verification);
        // echo "</pre>";
        // exit;
            if($type == 'byId'){
                if ($user_verification->is_active == 0) {
                    $user_verification->is_active = 1;
                    $user_verification->save();
                    return redirect()->to("admin/user/verifications")->with("message", $user_verification->user->name . " User Verification has been approved");
                } else {
                    $user_verification->is_active = 0;
                    $user_verification->save();
                    return redirect()->to("admin/user/verifications")->with("error", $user_verification->user->name . " User Verification has not been approved");
                }
            }else if($type=='bycriminal'){
                if ($user_verification->is_criminal_verified == 'no' || $user_verification->is_criminal_verified == '') {
                    $user_verification->is_criminal_verified = 'yes';
                    $user_verification->save();
                    return redirect()->to("admin/user/verifications")->with("message", $user_verification->user->name . " User Verification has been approved");
                } else {
                    $user_verification->is_criminal_verified = 'no';
                    $user_verification->save();
                    return redirect()->to("admin/user/verifications")->with("error", $user_verification->user->name . " User Verification has not been approved");
                }
            }else if($type=='byflash'){
                if ($user_verification->is_verfied_by_flash == 'no' || $user_verification->is_verfied_by_flash == '') {
                    $user_verification->is_verfied_by_flash = 'yes';
                    $user_verification->save();
                    return redirect()->to("admin/user/verifications")->with("message", $user_verification->user->name . " User Verification has been approved");
                } else {
                    $user_verification->is_verfied_by_flash = 'no';
                    $user_verification->save();
                    return redirect()->to("admin/user/verifications")->with("error", $user_verification->user->name . " User Verification has not been approved");
                }
            }
        } else {
            return redirect()->to("admin/user/verifications")->with("error", "There has been some mistake. Please try again later!");
        }
    }
public function buy_nights(Request $request){
            $userId = \Auth::user()->id;
            /*require_once(base_path('vendor/stripe/stripe-php/init.php'));
            // require_once(base_path('vendor/stripe/stripe-php/lib/Stripe.php'));
            // require_once(base_path('vendor/stripe/stripe-php/lib/Customer.php'));
            // require_once(base_path('vendor/stripe/stripe-php/lib/Charge.php'));
            // Stripe::setApiKey('sk_test_ilTWgJTUxFnwikQRryS0OCiT00FZUPmlSW');
            
// use Stripe\Stripe;
// use Stripe\Customer;
// use Stripe\Charge
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            // \Stripe\Stripe
         $customer = \Stripe\Customer::create(array(
                'email' => $request->stripeEmail,
                'source'  => $request->stripeToken
            ));
            $charge = \Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount'   => '17650',
                'currency' => env('STRIPE_CURRENCY'),//'usd'
            ));        

            echo "<pre>";
                print_r($customer);
                echo "<hr> <br> Below is Charge <br> <hr>";
                print_r($charge);
            echo "</pre>";
            exit;*/
            // echo "<pre>";
            //     print_r($_SERVER);
            // echo "</pre>";
            // exit;
//             $userInfo = \App\User::find($userId);//\App\Users::where("id",$userId);

//             $userNameInfo = (isset($userInfo->name) && $userInfo->name != '' ? $userInfo->name." " : '').(isset($userInfo->surname) && $userInfo->surname != '' ? $userInfo->surname  : '');
//             $userNameInfo1 = (isset($userInfo->sname16) && $userInfo->sname16 != '' ? $userInfo->sname16." " : '').(isset($userInfo->ssurname16) && $userInfo->ssurname16 != '' ? $userInfo->ssurname16  : '');
//             $userTaxNo = (isset($userInfo->NIN18) && $userInfo->NIN18 != '' ? $userInfo->NIN18 : '');
//             $userTaxNo1 = (isset($userInfo->NIN16) && $userInfo->NIN16 != '' ? $userInfo->NIN16 : '');
// // if (is_not_null($rentalObj->user->student_relationship)) { }
//             // $userNameInfo1 = 
//              $transid = time();//$charge->id; like this uncomment the code and will work
//              $nights = 7;
//             $title = $nights." nights";
//             $streetaddress = '';//"Address here";
//             $Night = $nights." Night";
//             $checkindate = '';//date("Y-m-d");
//             $checkoutdate = '';//date("Y-m-d"); in what order is this_ no order whaagreement_
//             $TotalwithoutService = $nights;
//             $servicefee = 0;
//             $TotalAmt = ($TotalwithoutService + $servicefee);



//             $user_email = 'esperienzainglese@gmail.com';
//             $data = array('user' => $user_email, 'userNameInfo' => $userNameInfo,'userTaxNo'=>$userTaxNo, 'userNameInfo1' => $userNameInfo1,'userTaxNo1'=>$userTaxNo1,'transid'=>$transid,'title'=>$title,'Night'=>$Night,'TotalwithoutService'  =>  $TotalwithoutService,'servicefee'=>$servicefee,'TotalAmt'   =>$TotalAmt);
//         \Mail::send('emails.receipt', $data, function($message) use($user_email) {
//             $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
//             $message->to($user_email, "Payment Receipt")
//                     ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
//                     ->subject('Payment Receipt');
//         });
//         if (\Mail::failures()) {
//             echo  "<bR> email sending fails";
//             exit;
//         }
//         echo "<br> email send";
// 
            // exit;
        // $tripsObj = RentalsEnquiry::where('user_id', $userId)->where('cancelled', 'No')->orderBy('dateAdded', 'desc')->get();
        // echo "Request to Buy Nights :".$request->buy_nights."<br>";
        // echo "Request to pay for Nights :".$request->nights."<br>";
        if($request->buy_nights != '' && ($request->nights != '' || $request->nights != 0)){
        // echo "<br> <pre>";
        //     print_r($_REQUEST);
        // echo "</pre>";

            /*payment gateway api start here */
            /*payment gateway api end here */
                                    // $paymentId = rand()+time();
                                    // $purchasedNights = $request->nights;
                                    // $amount = $request->nights;
                                    // $send_to_unpaid = 1;
                                    // $book_unpaid = 1;
                                    // $send_to_schoolhouse = 1;
                                    // $book_paid = 1;
                                    // $paid_status = 'Pending';
                                    // $created_at = date("Y-m-d H:i:s");
            $paymentArray = array(
                                    'payment_id'            => rand()+time(),
                                    // 'purchasedNights = $request->nights,
                                    'amount'                =>  $request->nights,
                                    'send_to_unpaid'        =>  3,
                                    'book_unpaid'           =>  1,
                                    'send_to_schoolhouse'   =>  0,
                                    'book_paid'             =>  $request->nights,
                                    'paid_status'           =>  'Pending',
                                    'created_at'            =>  date("Y-m-d H:i:s"),
                                );
            // book_paid
            $userWallet = \App\UserWallet::where("user_id", $userId)->first();
            // echo "User's book_paid wallet :".$userWallet->book_paid;
            $updatedSendToUnpaidCredits = ($userWallet->send_to_unpaid + 3);
            $updatedBookUnpaidCredits = ($userWallet->book_unpaid + 1);
            $updatedBookPaidCredits = ($userWallet->book_paid + $request->nights);

            $updateCredits = array( 
                                    "send_to_unpaid"    =>  $updatedSendToUnpaidCredits,
                                    "book_unpaid"       =>  $updatedBookUnpaidCredits,
                                    "book_paid"         =>  $updatedBookPaidCredits,
                                    );
            // echo "<br> Updated Credits :".
            $userWalletUpdate = \App\UserWallet::where("user_id", $userId)->update($updateCredits);
            // echo "<br> Update Wallet User :".var_dump($userWalletUpdate);
            // echo "<br> <pre>";
            //    print_r($userWallet);
            // echo "</pre>";
            // echo "<pre>";
            //     print_r($paymentArray);
            // echo "</pre>";
            // exit;

            // DB("")::create()
            $lastInsertedId = \DB::table('user_wallets_credits_log')->insertGetId($paymentArray);
            // echo "Last Id :".$lastInsertedId."<br>";
            sleep(5);
            $paymentDataArray = array(
                                    'paid_status'   =>  'Paid',
                                    'updated_at'    =>  date('Y-m-d H:i:s'),
                                 );

             $lastUpdatedID = \DB::table('user_wallets_credits_log')->where('id',$lastInsertedId)->update($paymentDataArray);

             $userWallet = \App\UserWallet::where("user_id", $userId)->first();

             flash('Your payment has been completed successfully.', 'success');
             // echo "<pre>";
             //    print_r($userWallet);
             // echo "</pre>";
             // exit;
        }
        

        //return $tripsObj;

        return View('buy_nights', compact(''));
        
    }
public function checkout(Request $request){

        // Stripe::setApiKey('sk_test_ilTWgJTUxFnwikQRryS0OCiT00FZUPmlSW');
        //  $customer = Customer::create(array(
        //         'email' => $request->stripeEmail,
        //         'source'  => $request->stripeToken
        //     ));
        //     $charge = Charge::create(array(
        //         'customer' => $customer->id,
        //         'amount'   => '0140',
        //         'currency' => 'usd'
        //     ));        

        //     echo "<pre>";
        //         print_r($customer);
        //         echo "<hr> <br> Below is Charge <br> <hr>";
        //         print_r($charge);
        //     echo "</pre>";
        //     exit;
    // echo "<pre>";
    //     print_r($_REQUEST);
    // echo "</pre>";
    // exit;
        if($request->stripeToken != '' && ($request->nights != '' || $request->nights != 0)){
            
            $nights = $_REQUEST['nights'];
            $amount = $_REQUEST['amount'];
            $payableAmount = $_REQUEST['amount'];

        
            /*payment gateway api start here */
            /*payment gateway api end here */
            // $paymentId = rand()+time();
            // $purchasedNights = $request->nights;
            // $amount = $request->nights;
            // $send_to_unpaid = 1;
            // $book_unpaid = 1;
            // $send_to_schoolhouse = 1;
            // $book_paid = 1;
            // $paid_status = 'Pending';
            // $created_at = date("Y-m-d H:i:s");
            $paymentArray = array(
                                    //'payment_id'            => rand()+time(),
                                    //'payment_id'            => rand()+time(),
                                    // 'purchasedNights = $request->nights,

                                    'amount'                =>  $nights,
                                    'send_to_unpaid'        =>  3,
                                    'book_unpaid'           =>  1,
                                    'send_to_schoolhouse'   =>  0,
                                    'book_paid'             =>  $nights,
                                    'paid_status'           =>  'Pending',
                                    'created_at'            =>  date("Y-m-d H:i:s"),
                                );
            $userId = \Auth::user()->id;
            // book_paid
            $userWallet = \App\UserWallet::where("user_id", $userId)->first();
            // echo "User's book_paid wallet :".$userWallet->book_paid;
            $updatedSendToUnpaidCredits = (isset($userWallet->send_to_unpaid) ? ($userWallet->send_to_unpaid + 3) : 0);
            $updatedBookUnpaidCredits = (isset($userWallet->book_unpaid) ? ($userWallet->book_unpaid + 1) : 0);
            $updatedBookPaidCredits = (isset($userWallet->book_paid) ? ($userWallet->book_paid + $request->nights) : 0);

            $updateCredits = array( 
                                    "send_to_unpaid"    =>  $updatedSendToUnpaidCredits,
                                    "book_unpaid"       =>  $updatedBookUnpaidCredits,
                                    "book_paid"         =>  $updatedBookPaidCredits,
                                    );
            $userWalletUpdate = \App\UserWallet::where("user_id", $userId)->update($updateCredits);
            
            $lastInsertedId = \DB::table('user_wallets_credits_log')->insertGetId($paymentArray);
            // echo "Last Inerted Id :".$lastInsertedId."<br>";
        try {
             require_once(base_path('vendor/stripe/stripe-php/init.php'));

                // Stripe::setApiKey('sk_test_ilTWgJTUxFnwikQRryS0OCiT00FZUPmlSW');

                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

                $customer = \Stripe\Customer::create(array(
                    'email' => $request->stripeEmail,
                    'source'  => $request->stripeToken
                ));

                $charge = \Stripe\Charge::create(array(
                    'customer' => $customer->id,
                    'amount'   => $payableAmount,
                    'currency' => env('STRIPE_CURRENCY')
                ));

                $paymentDataArray = array(
                                        'payment_customer_id'   =>  $customer->id,
                                        'payment_id'   =>  $charge->id,
                                        'paid_status'   =>  'Paid',
                                        'updated_at'    =>  date('Y-m-d H:i:s'),
                                     );

                 $lastUpdatedID = \DB::table('user_wallets_credits_log')->where('id',$lastInsertedId)->update($paymentDataArray);

                 $userWallet = \App\UserWallet::where("user_id", $userId)->first();

                 /* Email Template Content Start Here */
                $userInfo = \App\User::find($userId);//\App\Users::where("id",$userId);
               /* $userNameInfo = (isset($userInfo->sname16) && $userInfo->sname16 != '' ? $userInfo->sname16." " : '').(isset($userInfo->ssurname16) && $userInfo->ssurname16 != '' ? $userInfo->ssurname16  : '');
                $userTaxNo = (isset($userInfo->NIN18) && $userInfo->NIN18 != '' ? $userInfo->NIN18 : '');
                $transid = $charge->id;
                $title = "Bought ".$nights." nights";
                $streetaddress = '';//"Address here";
                $Night = $nights." Night";
                $checkindate = '';//date("Y-m-d");
                $checkoutdate = '';//date("Y-m-d");
                $TotalwithoutService = $nights;
                $servicefee = 0;
                $TotalAmt = ($TotalwithoutService + $servicefee);*/

            $userInfo = \App\User::find($userId);//\App\Users::where("id",$userId);

            $userNameInfo = (isset($userInfo->name) && $userInfo->name != '' ? $userInfo->name." " : '').(isset($userInfo->surname) && $userInfo->surname != '' ? $userInfo->surname  : '');
            $userNameInfo1 = (isset($userInfo->sname16) && $userInfo->sname16 != '' ? $userInfo->sname16." " : '').(isset($userInfo->ssurname16) && $userInfo->ssurname16 != '' ? $userInfo->ssurname16  : '');
            $userTaxNo = (isset($userInfo->NIN18) && $userInfo->NIN18 != '' ? $userInfo->NIN18 : '');
            $userTaxNo1 = (isset($userInfo->NIN16) && $userInfo->NIN16 != '' ? $userInfo->NIN16 : '');
// if (is_not_null($rentalObj->user->student_relationship)) { }
            // $userNameInfo1 = 
             $transid = $charge->id; //like this uncomment the code and will work
             // $nights = 7;
            $title = $nights." nights";
            $streetaddress = '';//"Address here";
            $Night = $nights." Night";
            $checkindate = '';//date("Y-m-d");
            $checkoutdate = '';//date("Y-m-d"); in what order is this_ no order whaagreement_
            $TotalwithoutService = ($payableAmount/100);
            $servicefee = 0;
            $TotalAmt = ($TotalwithoutService + $servicefee);
            $user_email = $userInfo->email;
            $user_email = 'esperienzainglese@gmail.com';
            // $user_email = 'esperienzainglese@gmail.com';
            $data = array('user' => $user_email, 'userNameInfo' => $userNameInfo,'userTaxNo'=>$userTaxNo, 'userNameInfo1' => $userNameInfo1,'userTaxNo1'=>$userTaxNo1,'transid'=>$transid,'title'=>$title,'Night'=>$Night,'TotalwithoutService'  =>  $TotalwithoutService,'servicefee'=>$servicefee,'TotalAmt'   =>$TotalAmt,'billerId'=>$lastInsertedId);
            // echo "<pre>";
            //     print_r($data);
            // echo "</pre>";
        \Mail::send('emails.receipt', $data, function($message) use($user_email) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user_email, "Payment Receipt")
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Payment Receipt');
                    // ->getSwiftMessage()
                    // ->getHeaders()
                    // ->addTextHeader('MIME-Verson', '1.0')
                    // ->addTextHeader('Content-type', 'text/html')
                    // ->addTextHeader('Charset', 'iso-88591-1');
        });
        \Mail::send('emails.receipt', $data, function($message) use($user_email) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user_email, "Payment Receipt")
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Payment Receipt');
        }); 
        if (\Mail::failures()) {
            echo  "<bR> email sending fails for Payment Receipt.";
            // exit;
        }

        $userData = \App\User::with('school')->where('id',$userId)->get();
        //$user_email = '';
        $voucher = '';
        $tutorName = (isset($userData[0]->tutor_name) && $userData[0]->tutor_name != '' ? $userData[0]->tutor_name : '');
        $tutorSurName = (isset($userData[0]->tutor_surname) && $userData[0]->tutor_surname != '' ? $userData[0]->tutor_surname : '');
        $tutorPob = (isset($userData[0]->tutor_pob) && $userData[0]->tutor_pob != '' ? $userData[0]->tutor_pob : '');
        $tutorPan = (isset($userData[0]->tutor_pan) && $userData[0]->tutor_pan != '' ? $userData[0]->tutor_pan : '');
        $tutorEmail = (isset($userData[0]->tutor_email) && $userData[0]->tutor_email != '' ? $userData[0]->tutor_email : '');
        $tutorPhone = (isset($userData[0]->tutor_phone) && $userData[0]->tutor_phone != '' ? $userData[0]->tutor_phone : '');
        $schoolEmail = (isset($userData[0]->school->school_email) && $userData[0]->school->school_email != '' ? $userData[0]->school->school_email : '');
        $phoneNumber = (isset($userData[0]->phonenumber) && $userData[0]->phonenumber != '' ? $userData[0]->phonenumber : '');


        $data = array('user' => $user_email, 'voucher' => $voucher,'tutor_name'=>$tutorName,'tutor_surname'=>$tutorSurName,'tutor_pob'=>$tutorPob,'tutor_pan'=>$tutorPan,'tutor_email'=>$tutorEmail,'tutor_email'=>$tutorEmail,'tutor_phone'=>$tutorPhone,'school_email' => $schoolEmail,'phonenumber'=>$phoneNumber);

        \Mail::send('emails.SendPaymentEmailPreEnrolledToCompleteProfile', $data, function($message) use($user_email) {

        $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
        $message->to($user_email, "User")
                ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                ->subject('work-related-learning.com - Istruzioni completamento iscrizione al portale ed invio domande di lavoro - Alternanza scuola lavoro in contesto estero');
        });
        // var_dump(\Mail::failures());
        if (\Mail::failures()) {
        return "email sending fails for Complete Profile.";
        }
        echo "<br> email send";
                 /* Email Template Content End Here */

                 flash('Your payment has been completed successfully.', 'success');

                 return \Redirect::to('user/buynights');
            }catch (\Exception $ex) {
                return $ex->getMessage();
            }
        }else{
        //     echo "Fall in Else part<pre>";
        // print_r($_REQUEST);
        // echo "</pre>";
        // exit;
        }

        
        exit;
    }

public function live_buy_nights(Request $request){
            $userId = \Auth::user()->id;
            /*require_once(base_path('vendor/stripe/stripe-php/init.php'));
            // require_once(base_path('vendor/stripe/stripe-php/lib/Stripe.php'));
            // require_once(base_path('vendor/stripe/stripe-php/lib/Customer.php'));
            // require_once(base_path('vendor/stripe/stripe-php/lib/Charge.php'));
            // Stripe::setApiKey('sk_test_ilTWgJTUxFnwikQRryS0OCiT00FZUPmlSW');
            
// use Stripe\Stripe;
// use Stripe\Customer;
// use Stripe\Charge
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            // \Stripe\Stripe
         $customer = \Stripe\Customer::create(array(
                'email' => $request->stripeEmail,
                'source'  => $request->stripeToken
            ));
            $charge = \Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount'   => '17650',
                'currency' => env('STRIPE_CURRENCY'),//'usd'
            ));        

            echo "<pre>";
                print_r($customer);
                echo "<hr> <br> Below is Charge <br> <hr>";
                print_r($charge);
            echo "</pre>";
            exit;*/
            // echo "<pre>";
            //     print_r($_SERVER);
            // echo "</pre>";
            // exit;
//             $userInfo = \App\User::find($userId);//\App\Users::where("id",$userId);

//             $userNameInfo = (isset($userInfo->name) && $userInfo->name != '' ? $userInfo->name." " : '').(isset($userInfo->surname) && $userInfo->surname != '' ? $userInfo->surname  : '');
//             $userNameInfo1 = (isset($userInfo->sname16) && $userInfo->sname16 != '' ? $userInfo->sname16." " : '').(isset($userInfo->ssurname16) && $userInfo->ssurname16 != '' ? $userInfo->ssurname16  : '');
//             $userTaxNo = (isset($userInfo->NIN18) && $userInfo->NIN18 != '' ? $userInfo->NIN18 : '');
//             $userTaxNo1 = (isset($userInfo->NIN16) && $userInfo->NIN16 != '' ? $userInfo->NIN16 : '');
// // if (is_not_null($rentalObj->user->student_relationship)) { }
//             // $userNameInfo1 = 
//              $transid = time();//$charge->id; like this uncomment the code and will work
//              $nights = 7;
//             $title = $nights." nights";
//             $streetaddress = '';//"Address here";
//             $Night = $nights." Night";
//             $checkindate = '';//date("Y-m-d");
//             $checkoutdate = '';//date("Y-m-d"); in what order is this_ no order whaagreement_
//             $TotalwithoutService = $nights;
//             $servicefee = 0;
//             $TotalAmt = ($TotalwithoutService + $servicefee);



//             $user_email = 'esperienzainglese@gmail.com';
//             $data = array('user' => $user_email, 'userNameInfo' => $userNameInfo,'userTaxNo'=>$userTaxNo, 'userNameInfo1' => $userNameInfo1,'userTaxNo1'=>$userTaxNo1,'transid'=>$transid,'title'=>$title,'Night'=>$Night,'TotalwithoutService'  =>  $TotalwithoutService,'servicefee'=>$servicefee,'TotalAmt'   =>$TotalAmt);
//         \Mail::send('emails.receipt', $data, function($message) use($user_email) {
//             $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
//             $message->to($user_email, "Payment Receipt")
//                     ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
//                     ->subject('Payment Receipt');
//         });
//         if (\Mail::failures()) {
//             echo  "<bR> email sending fails";
//             exit;
//         }
//         echo "<br> email send";
// 
            // exit;
        // $tripsObj = RentalsEnquiry::where('user_id', $userId)->where('cancelled', 'No')->orderBy('dateAdded', 'desc')->get();
        // echo "Request to Buy Nights :".$request->buy_nights."<br>";
        // echo "Request to pay for Nights :".$request->nights."<br>";

        //return $tripsObj;

        return View('buy_nights_euro', compact(''));
        
    }
public function live_checkout(Request $request){

        // Stripe::setApiKey('sk_test_ilTWgJTUxFnwikQRryS0OCiT00FZUPmlSW');
        //  $customer = Customer::create(array(
        //         'email' => $request->stripeEmail,
        //         'source'  => $request->stripeToken
        //     ));
        //     $charge = Charge::create(array(
        //         'customer' => $customer->id,
        //         'amount'   => '0140',
        //         'currency' => 'usd'
        //     ));        

        //     echo "<pre>";
        //         print_r($customer);
        //         echo "<hr> <br> Below is Charge <br> <hr>";
        //         print_r($charge);
        //     echo "</pre>";
        //     exit;
    // echo "<pre>";
    //     print_r($_REQUEST);
    // echo "</pre>";
    // exit;
        if($request->stripeToken != '' && ($request->nights != '' || $request->nights != 0)){
            
            $nights = $_REQUEST['nights'];
            $amount = $_REQUEST['amount'];
            $payableAmount = $_REQUEST['amount'];

        
            /*payment gateway api start here */
            /*payment gateway api end here */
            // $paymentId = rand()+time();
            // $purchasedNights = $request->nights;
            // $amount = $request->nights;
            // $send_to_unpaid = 1;
            // $book_unpaid = 1;
            // $send_to_schoolhouse = 1;
            // $book_paid = 1;
            // $paid_status = 'Pending';
            // $created_at = date("Y-m-d H:i:s");
            $paymentArray = array(
                                    //'payment_id'            => rand()+time(),
                                    //'payment_id'            => rand()+time(),
                                    // 'purchasedNights = $request->nights,

                                    'amount'                =>  $nights,
                                    'send_to_unpaid'        =>  3,
                                    'book_unpaid'           =>  1,
                                    'send_to_schoolhouse'   =>  0,
                                    'book_paid'             =>  $nights,
                                    'paid_status'           =>  'Pending',
                                    'created_at'            =>  date("Y-m-d H:i:s"),
                                );
            $userId = \Auth::user()->id;
            // book_paid
            $userWallet = \App\UserWallet::where("user_id", $userId)->first();
            // echo "User's book_paid wallet :".$userWallet->book_paid;
            $updatedSendToUnpaidCredits = (isset($userWallet->send_to_unpaid) ? ($userWallet->send_to_unpaid + 3) : 0);
            $updatedBookUnpaidCredits = (isset($userWallet->book_unpaid) ? ($userWallet->book_unpaid + 1) : 0);
            $updatedBookPaidCredits = (isset($userWallet->book_paid) ? ($userWallet->book_paid + $request->nights) : 0);

            $updateCredits = array( 
                                    "send_to_unpaid"    =>  $updatedSendToUnpaidCredits,
                                    "book_unpaid"       =>  $updatedBookUnpaidCredits,
                                    "book_paid"         =>  $updatedBookPaidCredits,
                                    );
            $userWalletUpdate = \App\UserWallet::where("user_id", $userId)->update($updateCredits);
            
            $lastInsertedId = \DB::table('user_wallets_credits_log')->insertGetId($paymentArray);
            // echo "Last Inerted Id :".$lastInsertedId."<br>";
        try {
             require_once(base_path('vendor/stripe/stripe-php/init.php'));

                // Stripe::setApiKey('sk_test_ilTWgJTUxFnwikQRryS0OCiT00FZUPmlSW');

                // \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                \Stripe\Stripe::setApiKey('sk_live_zLqjmWLBmEmJh39QUIiB40oV007HpmCitx');

                $customer = \Stripe\Customer::create(array(
                    'email' => $request->stripeEmail,
                    'source'  => $request->stripeToken
                ));

                $charge = \Stripe\Charge::create(array(
                    'customer' => $customer->id,
                    'amount'   => $payableAmount,
                    'currency' => env('STRIPE_CURRENCY')
                ));

                $paymentDataArray = array(
                                        'payment_customer_id'   =>  $customer->id,
                                        'payment_id'   =>  $charge->id,
                                        'paid_status'   =>  'Paid',
                                        'updated_at'    =>  date('Y-m-d H:i:s'),
                                     );

                 $lastUpdatedID = \DB::table('user_wallets_credits_log')->where('id',$lastInsertedId)->update($paymentDataArray);

                 $userWallet = \App\UserWallet::where("user_id", $userId)->first();

                 /* Email Template Content Start Here */
                $userInfo = \App\User::find($userId);//\App\Users::where("id",$userId);
               /* $userNameInfo = (isset($userInfo->sname16) && $userInfo->sname16 != '' ? $userInfo->sname16." " : '').(isset($userInfo->ssurname16) && $userInfo->ssurname16 != '' ? $userInfo->ssurname16  : '');
                $userTaxNo = (isset($userInfo->NIN18) && $userInfo->NIN18 != '' ? $userInfo->NIN18 : '');
                $transid = $charge->id;
                $title = "Bought ".$nights." nights";
                $streetaddress = '';//"Address here";
                $Night = $nights." Night";
                $checkindate = '';//date("Y-m-d");
                $checkoutdate = '';//date("Y-m-d");
                $TotalwithoutService = $nights;
                $servicefee = 0;
                $TotalAmt = ($TotalwithoutService + $servicefee);*/

            $userInfo = \App\User::find($userId);//\App\Users::where("id",$userId);

            $userNameInfo = (isset($userInfo->name) && $userInfo->name != '' ? $userInfo->name." " : '').(isset($userInfo->surname) && $userInfo->surname != '' ? $userInfo->surname  : '');
            $userNameInfo1 = (isset($userInfo->sname16) && $userInfo->sname16 != '' ? $userInfo->sname16." " : '').(isset($userInfo->ssurname16) && $userInfo->ssurname16 != '' ? $userInfo->ssurname16  : '');
            $userTaxNo = (isset($userInfo->NIN18) && $userInfo->NIN18 != '' ? $userInfo->NIN18 : '');
            $userTaxNo1 = (isset($userInfo->NIN16) && $userInfo->NIN16 != '' ? $userInfo->NIN16 : '');
// if (is_not_null($rentalObj->user->student_relationship)) { }
            // $userNameInfo1 = 
             $transid = $charge->id; //like this uncomment the code and will work
             // $nights = 7;
            $title = $nights." nights";
            $streetaddress = '';//"Address here";
            $Night = $nights." Night";
            $checkindate = '';//date("Y-m-d");
            $checkoutdate = '';//date("Y-m-d"); in what order is this_ no order whaagreement_
            $TotalwithoutService = ($payableAmount/100);
            $servicefee = 0;
            $TotalAmt = ($TotalwithoutService + $servicefee);
            $user_email = $userInfo->email;
            $user_email = 'esperienzainglese@gmail.com';
            // $user_email = 'esperienzainglese@gmail.com';
            $data = array('user' => $user_email, 'userNameInfo' => $userNameInfo,'userTaxNo'=>$userTaxNo, 'userNameInfo1' => $userNameInfo1,'userTaxNo1'=>$userTaxNo1,'transid'=>$transid,'title'=>$title,'Night'=>$Night,'TotalwithoutService'  =>  $TotalwithoutService,'servicefee'=>$servicefee,'TotalAmt'   =>$TotalAmt,'billerId'=>$lastInsertedId);
            // echo "<pre>";
            //     print_r($data);
            // echo "</pre>";
        \Mail::send('emails.receipt', $data, function($message) use($user_email) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user_email, "Payment Receipt")
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Payment Receipt');
                    // ->getSwiftMessage()
                    // ->getHeaders()
                    // ->addTextHeader('MIME-Verson', '1.0')
                    // ->addTextHeader('Content-type', 'text/html')
                    // ->addTextHeader('Charset', 'iso-88591-1');
        });
        \Mail::send('emails.receipt', $data, function($message) use($user_email) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user_email, "Payment Receipt")
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Payment Receipt');
        }); 
        if (\Mail::failures()) {
            echo  "<bR> email sending fails for Payment Receipt.";
            // exit;
        }

        $userData = \App\User::with('school')->where('id',$userId)->get();
        //$user_email = '';
        $voucher = '';
        $tutorName = (isset($userData[0]->tutor_name) && $userData[0]->tutor_name != '' ? $userData[0]->tutor_name : '');
        $tutorSurName = (isset($userData[0]->tutor_surname) && $userData[0]->tutor_surname != '' ? $userData[0]->tutor_surname : '');
        $tutorPob = (isset($userData[0]->tutor_pob) && $userData[0]->tutor_pob != '' ? $userData[0]->tutor_pob : '');
        $tutorPan = (isset($userData[0]->tutor_pan) && $userData[0]->tutor_pan != '' ? $userData[0]->tutor_pan : '');
        $tutorEmail = (isset($userData[0]->tutor_email) && $userData[0]->tutor_email != '' ? $userData[0]->tutor_email : '');
        $tutorPhone = (isset($userData[0]->tutor_phone) && $userData[0]->tutor_phone != '' ? $userData[0]->tutor_phone : '');
        $schoolEmail = (isset($userData[0]->school->school_email) && $userData[0]->school->school_email != '' ? $userData[0]->school->school_email : '');
        $phoneNumber = (isset($userData[0]->phonenumber) && $userData[0]->phonenumber != '' ? $userData[0]->phonenumber : '');


        $data = array('user' => $user_email, 'voucher' => $voucher,'tutor_name'=>$tutorName,'tutor_surname'=>$tutorSurName,'tutor_pob'=>$tutorPob,'tutor_pan'=>$tutorPan,'tutor_email'=>$tutorEmail,'tutor_email'=>$tutorEmail,'tutor_phone'=>$tutorPhone,'school_email' => $schoolEmail,'phonenumber'=>$phoneNumber);

        \Mail::send('emails.SendPaymentEmailPreEnrolledToCompleteProfile', $data, function($message) use($user_email) {

        $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
        $message->to($user_email, "User")
                ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                ->subject('work-related-learning.com - Istruzioni completamento iscrizione al portale ed invio domande di lavoro - Alternanza scuola lavoro in contesto estero');
        });
        // var_dump(\Mail::failures());
        if (\Mail::failures()) {
        return "email sending fails for Complete Profile.";
        }
        echo "<br> email send";
                 /* Email Template Content End Here */

                 flash('Your payment has been completed successfully.', 'success');

                 return \Redirect::to('user/buynights');
            }catch (\Exception $ex) {
                return $ex->getMessage();
            }
        }else{
        //     echo "Fall in Else part<pre>";
        // print_r($_REQUEST);
        // echo "</pre>";
        // exit;
        }

        
        exit;
    }

    public function PromptSendUnpaid(){
       
        $allusers = \App\User::with(['userWallet'])->whereNotIn("users.secondary_group_id",[0,1])->get();
       
        $count = 0;
        $al_cunt = 0;
        $case_1_html = '';
        $case_2_html = ''; 
        foreach($allusers as $user){
            if($user->status != 1){
                continue;
            }
            $secondary_group_id = $user->secondary_group_id;
            $group_info         = \App\SecondarySchoolGroups::where('id',$secondary_group_id)->get();

            $today_date = strtotime(date("m/d/Y"));
            $group_date_out = strtotime($group_info[0]->group_date_out);
            $group_name = $group_info[0]->group_name;

           
            $parent_email = $user->email; 
            $student_email = $user->semail16;
            if($group_date_out >= $today_date){
                // we have skipped older groups and only checking for new groups whose dates are in future or today 
                // Now we are checking user wallet credits for send_to_upaid
                $get_credits_sendtounpaid_all = \App\UserCredit::where("user_id", $user->id)->where("send_to_unpaid", ">", "0")->get();
              
                $total_send_to_unpaid_sent = 0;
                $wallet_sendtounpaid = 0;
                $credit_sendtounpaid = 0;

                foreach ($get_credits_sendtounpaid_all as $key => $send_to_unpaid) {
                    $total_send_to_unpaid_sent += $send_to_unpaid->send_to_unpaid;
                }
                
                // This variable is used to check if they already used some credits total_send_to_unpaid_sent / so they have credits
                // This variable for all credits they were having wallet_sendtounpaid
                $get_credits_sendtounpaid = $total_send_to_unpaid_sent;

                $wallet_sendtounpaid = (isset($user->userWallet[0])?$user->userWallet[0]['send_to_unpaid']:0);
                //here checiking remaining credits
                $credit_sendtounpaid = $wallet_sendtounpaid - $get_credits_sendtounpaid;
                $credit_sendtounpaid = ($credit_sendtounpaid > 0 ? $credit_sendtounpaid : 0);

                // If available then it means this $credit_sendtounpaid must be greater than zero if no credit then in 
                              
                $user_unpaid_request_sent = RentalsEnquiry::getuserUnpaidRequestsForPromptEmailNotOld($user->id,date('Y-m-d'));

                if($user_unpaid_request_sent == 0 && $credit_sendtounpaid > 0){
                    $case_1_html .= " Username : ".$parent_email." User sent request are ".$user_unpaid_request_sent." NOT OLD Requests<br>user total credits allowed are:".$wallet_sendtounpaid."</br> user credits used till now are ".$total_send_to_unpaid_sent."<br> user remaining credits are:".$credit_sendtounpaid."</br>";
                    // so user did not sent requests yet we will prompt email here
                    //echo "This user still not sent any request user_id=".$user->id."<br> and Parent Email is :".$parent_email."<br> and Student Email is :".$student_email."<br>"; 
                   /* $parent_email = 'abbasjami47@gmail.com';
                    $student_email = 'jami42you@gmail.com';
                    $group_name = 'JAMI';
                    $user->name = 'Abbas USING CRON JOB FOR TEST';*/
                   // $this->mailNotificationsForUser->PromptSendUnpaid($parent_email,$student_email,$group_name,$user->name);
                    
                }


          

                // we can send another prompt email here and add credits to that user right? so this elase refers to a user who has registered but fun out of credits & never had credits_ (NO never had credits but now they have only zero credits to apply if you want they did not used any credits yet then we check this too ) okay, first cron was for students who had not sent request yet *despite having credtis, that is being accredited, clear_yes  we need to prompt , and add credits, to usrs who have sent requests but that after a week have not found a job, so we credit plus 3 sendToUnapid and email them to try again OK, that-s why hope my logic correct, that is> 1 members of a gorup 2 sent requests but not accepted *not old requests( 3 add cretia dn send email, correct_ yes let me thnk about this 
                //checking user request thet sent and passed 7 days of time and not been accepted ANYWHERE unpaid or unpaidflash  yet 
               /* $already_sent_emails = array("greta.giassi@gmail.com","eleniafinazzi@gmail.com","maurob1970@virgilio.it","pcolenghi@gmail.com","stefanomorandipaioli@gmail.com","Jennynomadi@gmail.com","minnibenigni@libero.it","gnespoli74@gmail.com","geom.belottisergio@libero.it","cucciolo200@hotmail.it","fboffelli@icloud.com","viscardi.ivan@alice.it","DANIELLARABAIOLI@gmail.com","sabina.pastori@libero.it","giuseppinacattaneo@virgilio.it","Mariacremaschi65@gmail.com","giuseppinacattaneo@virgilio.it","federica.rossetti@ymail.com","laura.rocco@libero.it","bprimiceri@gmail.com","emilguerri.simona@capirola.com");*/
                $today_date = date('Y-m-d');
                $user_unpaid_accepted = RentalsEnquiry::getuserUnpaidRequestsForPromptEmailLastSevenDays($user->id,$today_date);
                /*
                if(in_array($user->email, $already_sent_emails)){
                    $al_cunt++; 
                    $users_to_we_needs_to_send_already .= $al_cunt." ==> Parent Email is :".$parent_email."<br>";    

                    continue;
                } */

                if($user_unpaid_accepted["accepted"] == 0 && $user_unpaid_accepted["not_accepted"] > 0){    
                    $count++;
                    $user_requests_seven_days_passed = RentalsEnquiry::getUserRequestSevenDaysPassed($user->id,$today_date);
                     $case_2_html .= "<strong>Username : ".$parent_email." User sent request that are not accepted yet and 7 days passed to the request sent  date are : ".$user_unpaid_accepted["not_accepted"]."</strong><br>";
                    foreach ($user_requests_seven_days_passed as $user_request) {
                        $Bookingno = $user_request->Bookingno;
                        $requested_date = $user_request->dateAdded;
                        $checkout_date = $user_request->checkout;
                        $case_2_html .= "Booking No.".$Bookingno." and Request sent date ".$requested_date." ==== BOOKING CHECKOUT DATE IS :".$checkout_date."<br>";
                        $case_2_html .= "1 Credit will be added <br>";
                       /* $parent_email = 'abbasjami47@gmail.com';
                        $student_email = 'jami42you@gmail.com';
                        $group_name = 'JAMI';
                        $user->name = 'Abbas Jami';*/
                       // $this->mailNotificationsForUser->PromptSendUnpaidLastSevenDaysNotApproved($parent_email,$student_email,$group_name,$user->name,$Bookingno);
                       // die;
                       // $user_update_credits = array("send_to_unpaid"=>$wallet_sendtounpaid + 1,"creadit_type"=>'');

                       //  $result =  \App\UserWallet::where("user_id", $user->id)->update($user_update_credits);
                    }
                    
                  
                    // so user did not sent requests yet we will prompt email here
                   // echo "This user send request in last seven days and yet not approved  user_id=".$user->id."<br> and 
                     // $users_to_we_needs_to_send .= $count." ==> Parent Email is :".$parent_email."<br>";    

                    /*$parent_email = 'abbasjami47@gmail.com';
                    $student_email = 'jami42you@gmail.com';
                    $group_name = 'JAMI';
                    $user->name = 'Abbas Jami';*/

                   // $this->mailNotificationsForUser->PromptSendUnpaidLastSevenDaysNotApproved($parent_email,$student_email,$group_name,$user->name);
                   // die; 
                    // Now will add 3 credits to send_to_unpaid ok
                   // $user_update_credits = array("send_to_unpaid"=>$wallet_sendtounpaid + 3,"creadit_type"=>'');

                   // $result =  \App\UserWallet::where("user_id", $user->id)->update($user_update_credits);
                    //var_dump($result);die;
                }

            }
        }
         echo "<h3>Case 1 cron job users list who did  not sent any request yet even they had credits to send_to_unpaid  .</h3> <br>";
        echo "<div style='background-color:green'>".$case_1_html."</div>";
         echo "<h3>Case 2 cron job users list who sent requests and did not responded yet and 7 days passed to their requests sents.</h3> <br>";
         echo "<div style='background-color:yellow'".$case_2_html."</div>";
    }
   
    // Cron job for calculating distance time fro paid to unpaid listings 
    public function updateDistanceTime() {
        $innerCount =0;
        $mainCount = 0;
        $filePath = dirname(dirname(dirname(dirname(__FILE__))))."/distance_cronlog/livetet_chron_log_file_".date('YmdH_i_s').".txt";

       // echo "FilePath :".$filePath."<br>";//exit;
       $countCalls = 0;
        $all_paid_property = \App\Property::where("property_status", "paid")->where('property_type',0)->where('property.status','!=',2)
                ->join("users", "users.id", "=", "property.user_id")
                ->join("user_paid_services", "user_paid_services.user_id", "=", "property.user_id")
                ->select("property.*","user_paid_services.houseaddress","user_paid_services.house_property_differ","user_paid_services.propertyaddress")
                ->orderBy('property.id','ASC')            
                ->get(); 
            
        echo "<br> Count Paid property :".count($all_paid_property)." Paid Property :";
               // echo "Paid Property : <pre>";
                 //    print_r($all_paid_property[0]);
              //echo "</pre>";
            // die;
        if (sizeof($all_paid_property) > 0) {
            $countLimits = 0;
            foreach ($all_paid_property as $paid_property) {
                echo "Calling For Paid Property ID:".$paid_property->id."<br>";
                $propId = $paid_property->id;
                 
                // to get the houseaddress from user_paid_services table for normal address, not from property table
                $address = isset($paid_property->user->paid_service->houseaddress) ? $paid_property->user->paid_service->houseaddress : '';
                // this is where from frontend we store school house legal address, 
                // check if school house property address is not same as legal address here above
                if (isset($paid_property->user->paid_service->house_property_differ) && $paid_property->user->paid_service->house_property_differ == "1") {
                    $address = isset($paid_property->user->paid_service->propertyaddress) ? $paid_property->user->paid_service->propertyaddress : '';
                }
                if ($paid_property->addr_work_org_same == 0) {
                    $address = $paid_property->workplace_address; // it is one not 0, workplace_address is our property, listing, address. Case paid, if checkbox checked, then use this address:workplace_address
                }

                $address = \DB::getPdo()->quote($address);
                // to get unpaid details
                // to get paid details
                
                if (!empty($address) && $address != "''") {
                    $all_unpaid_property = \App\Property::whereRaw("property_status ='unpaid' or property_status ='unpaidflash'")->get(); //offset(1)->limit(25)->limit 10 not here_ this is for unpaid. 
                   //echo "total unpaid:".count($all_unpaid_property);die; I was looking for else case, end of line raddress18 should be used if none of the above ok wait which is var legal_address i guess
                    if (sizeof($all_unpaid_property) > 0) {
                       
                        foreach ($all_unpaid_property as $unpaid_property) {
                            // echo "\n Inner Count :".$innerCount." \t Main Count :".$mainCount;


                            $unpaidPropId = $unpaid_property->id;
                            $already_found = \App\DistanceHomeWork::where(["paidPropId" => $propId, "unpaidPropId" => $unpaidPropId])->get()->count();
                            if($already_found > 0){
                                continue;
                            }

                            $unpaidAddress = isset($unpaid_property->user->raddress18) ? $unpaid_property->user->raddress18 : ''; //this is where we store address for unpaid 
                            if ($unpaid_property->addr_work_org_same == 0) {
                                $unpaidAddress = $unpaid_property->workplace_address; // case unpaid, if checkbox checked, then use this address:workplace_address
                            }
                            /* ****************** */
                            $time = 0;
                            $add1 = $address;
                            $add1 = str_replace(' ', '+', $add1);
                            $add1 = str_replace('#', '', $add1);
                            $add2 = $unpaidAddress;
                            $add2 = str_replace(' ', '+', $add2);
                            $add2 = str_replace('#', '', $add2);
                            
                            if($add2 == 'Nanda+Nagar,+Indore,+Madhya+Pradesh,+India' or $add2 == '6+Main+Street,+Dundrum,+Dublin+14,+Ireland' or $add2 == '12+Lambourne+Wood,+Brenanstown,+Cabinteely,+County+Dublin,+Ireland'){
                                continue;
                            }

                            if ($add1 != '' && $add2 != '' && $add1 != "''" && $add2 != "''") {
                               //$gmapApi = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyC646z-W9bGxhDtgjILxonsK2KI5TU-z68&origins=$add1&destinations=$add2&mode=transit";
                               //TEst KEy By JAMi
                               $gmapApi = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyBc4i62UqRy4l1UvEXJ9oHVAiwI3KiP2nA&origins=$add1&destinations=$add2&mode=transit";
                               //$today = date('D');
                               // $today = 'Mon';
                              // if($today == 'Mon'){ AIzaSyBc4i62UqRy4l1UvEXJ9oHVAiwI3KiP2nA AIzaSyBc4i62UqRy4l1UvEXJ9oHVAiwI3KiP2nA smae one ok new key for you please limit to 1000 calls per cron check if storing and cotninue storing until filled up, agree?yes but wait first we have to test with two calls fi, one always works, ok
                                $gmapApi .= '&arrivalTime=0900';
                              // }
                               $gmapApi .= '&day_sensor=false';

                              $countCalls++;
                           
                               $json = file_get_contents($gmapApi);
                               $details = json_decode($json, TRUE);
                               $distance_how_work_log = new \App\DistanceHomeWorkRequestResponseLog();
                               $distance_how_work_log->request_url = $gmapApi;
                               $distance_how_work_log->created_at = date("Y-m-d H:i:s");
                               $distance_how_work_log->ip = $_SERVER['REMOTE_ADDR'];
                               $distance_how_work_log->response_data = $json;
                               $distance_how_work_log->updated_at = date("Y-m-d H:i:s");
                               $distance_how_work_log->save();
                               
                               if ($details['status'] == 'OK' && isset($details['rows'][0]['elements'][0]['duration']['value'])) {
                                    $time = $details['rows'][0]['elements'][0]['duration']['value'];// 

                                    $time = floor($time / 60);
                                    //this will run 75555/60 and remove the decimals yes floor function do this (r) it.
                                } else {
                                    $time = "N/A";
                                    echo "From '<strong>".$add1."</strong> To '<strong>".$add2."</strong>' distanve time is ".$time." Minutes</br> ";
                                    echo "no API Returns \n";
                                   // continue; 
                                }
                               
                                /*echo "this is testing for one record <pre>";
                                echo "<br> Time : ".$time."<br>";
                                    print_r($details);
                                    print_r($json);
                                echo "</pre>";
                                 $fp = fopen($filePath,"a");
                                 chmod($filePath,0777);
                                $write  = "\n =========================== Start Here on ".date("Y-m-d H:i:s")." =========================== \n";
                                $write .= "\n \n Reqest Url : ".$gmapApi." \n \n";
                                $write .= "\n\n address = $address , time = $time added \n\n";
                                $write .= "\n =========================== End Here on ".date("Y-m-d H:i:s")." =========================== \n";

                                fwrite($fp, $write);
                                fclose($fp);
                                    // exit;*/
                              
                                echo "From '<strong>".$add1."</strong> To '<strong>".$add2."</strong>' distanve time is ".$time." Minutes</br> ";
                                  //  print_r($add1);
                                  //  echo "Add2 :";
                                  //  print_r($add2);
                               // echo "</pre>";
                                echo "<br> paid prop ID :".$propId."<br> Unpaid Id :".$unpaidPropId."<br>";
                               // echo "<pre>";
                                //print_r($details);
                                //echo "</pre><br>";

                            }else {
                                echo "Paid Address".$add1."<br>";
                                echo "Unpaid Address:".$add2."<br>";
                                echo "<br> FALL in ELSE Part <pre>";
                              
                               // echo "</pre><br> ";
                                continue;
                            }
                            
                            if($countCalls > 1000){
                                break;
                            }
                           
                            $distance_how_work = \App\DistanceHomeWork::where(["paidPropId" => $propId, "unpaidPropId" => $unpaidPropId])->first();
                            if (!isset($distance_how_work->id)) {
                                $distance_how_work = new \App\DistanceHomeWork();
                            }
                            $distance_how_work->paidAddress = $address;
                            $distance_how_work->paidPropId = $propId;
                            $distance_how_work->unpaidAddress = $unpaidAddress;
                            $distance_how_work->unpaidPropId = $unpaidPropId;
                            $distance_how_work->time = $time;
                            $distance_how_work->save();

                           /* $fp = fopen($filePath,"a");
                            chmod($filePath,0777);
                            $write  = "\n =========================== Start Here on ".date("Y-m-d H:i:s")." =========================== \n";
                            $write .= "\n \n Reqest Url : ".$gmapApi." \n \n";
                            $write .= "\n\n address = $address , time = $time added \n\n";
                            $write .= "\n =========================== End Here on ".date("Y-m-d H:i:s")." =========================== \n";

                            fwrite($fp, $write);
                            fclose($fp);*/
                            // echo "address = $address , time = $time added \n\n";
                            // exit; 
                        } 
                       
                    }
                }
                if($countCalls > 1000){
                    break;
                }
            }
        }
       // $this->info("All the listing addresses are stored successfully");
    }

    public function imageResizer(){
        $all_images = \App\PropertyImages::get(); 
        $properties_images = array();
                foreach ($all_images as $property_images) {
                    $image_path = public_path().'/images/rentals/'.$property_images->property_id.'/'.$property_images->img_name;    
                    if(!\File::exists($image_path)){
                        continue;
                    }
                    $thumbnail_path = public_path() . '/images/rentals/' . $property_images->property_id . "/thumbnail/" . $property_images->img_name;
                    $thumbnail_path_check = public_path() . '/images/rentals/' . $property_images->property_id . "/thumbnail";
                    if (!\File::exists($thumbnail_path_check)) {
                        \File::makeDirectory($thumbnail_path_check, $mode = 0777, true, true);
                    }
                    $canvas = \Image::canvas(370,250);
                    $img = \Image::make($image_path)->resize(370,250,function($constraint){
                        $constraint->aspectRatio();
                    });
                    $canvas->insert($img,'center');
                    $canvas->save($thumbnail_path);
                    echo "image orignal path:".$image_path."<br>";
                    echo "thumbnail path:".$thumbnail_path."<br>";

                }
             
        /* $img = \Image::make(public_path() . '/images/rentals/' . $request->get('listingid') . "/" . $img_name)->resize(370, 250);
                $thumbnail_path = public_path() . '/images/rentals/' . $request->get('listingid') . "/thumbnail/" . $img_name;
                $thumbnail_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/thumbnail";
                if (!\File::exists($thumbnail_path_check)) {
                    \File::makeDirectory($thumbnail_path_check, $mode = 0777, true, true);
                }
               $img->save($thumbnail_path);*/
    }
	
	public function bedImagesToRoom(){
		di("That was used for moving all existing beds images to their room images.please do no uncomment this because already done.");
		 $bed_properties = \App\Property::where("property_status", "paid")->where('property_type',26)->get(); 
		echo "Total beds in site are :".count($bed_properties);
		foreach($bed_properties as $bed){
			
			$user_first_property = \App\Property::where("property_status", "paid")->where('property_type',0)->where('user_id',$bed->user_id)->first(); 
			echo "<br>===user id :".$bed->user_id ."===<br>";
			echo "user first_property_id :".$user_first_property->id ."<br>";
			
			$room_id = $bed->bed_guest_room_id;
			if($room_id != NULL and $room_id != 0){
                			
				$guest_room_exists = \App\UserPaidGuestRoom::where('id',$room_id)->get();
				$bed_images = \App\PropertyImages::where("property_id",$bed->id)->get(); 
				if(count($bed_images) > 0  and count($guest_room_exists) > 0){
					echo "Room id ".$room_id." found has ".count($bed_images)." Images <br>";	
					foreach ($bed_images as $bed_image) {
						
						// Create guest room image from bed image
						$imgObj = new \App\GuestRoomImages;
						$imgObj->img_name = $bed_image->img_name;
						$imgObj->img_src = "";
						$imgObj->status = 1;
						$imgObj->created_at = date("Y-m-d H:i:s");
						$imgObj->updated_at = date("Y-m-d H:i:s");
						$imgObj->room_id =$room_id;
						$imgObj->save();
						
						$bed_image_path = public_path().'/images/rentals/'.$bed_image->property_id.'/'.$bed_image->img_name;    
						$room_image_path = public_path().'/images/rentals/'.$user_first_property->id.'/rooms/'.$bed_image->img_name;    
						$room_image_path_check = public_path().'/images/rentals/'.$user_first_property->id.'/rooms/';    
						if(!\File::exists($bed_image_path)){
							continue;
						}
						if (!\File::exists($room_image_path_check)) {
							\File::makeDirectory($room_image_path_check, $mode = 0777, true, true);
						}
						// copy file from property images folder to rooms seperate folder
						if(!copy($bed_image_path,$room_image_path)){
							echo "failed to copy $bed_image_path <br>";
						}else{
							echo "copied $bed_image_path into $room_image_path <br>";
						}
						
						$bed_thumbnail_path = public_path() . '/images/rentals/' . $bed_image->property_id . "/thumbnail/" . $bed_image->img_name;
						$room_thumbnail_path = public_path() . '/images/rentals/' . $user_first_property->id . "/rooms/thumbnail/" . $bed_image->img_name;
						$bed_thumbnail_path_check = public_path() . '/images/rentals/' . $bed_image->property_id . "/thumbnail";
						$room_thumbnail_path_check = public_path() . '/images/rentals/' . $user_first_property->id . "/rooms/thumbnail";
						if (!\File::exists($bed_thumbnail_path_check)) {
							\File::makeDirectory($bed_thumbnail_path_check, $mode = 0777, true, true);
							// Resize Existing bed images to thumbnail
							$canvas = \Image::canvas(370,250);
							$img = \Image::make($bed_image_path)->resize(370,250,function($constraint){
								$constraint->aspectRatio();
							});
							$canvas->insert($img,'center');
							$canvas->save($bed_thumbnail_path);
							
						}
						if (!\File::exists($room_thumbnail_path_check)) {
							\File::makeDirectory($room_thumbnail_path_check, $mode = 0777, true, true);
						}
						
						// copy file from property images folder to rooms seperate folder
						if(!copy($bed_thumbnail_path,$room_thumbnail_path)){
							echo "failed to copy $room_thumbnail_path";
						}else{
							echo "copied $bed_thumbnail_path into $room_thumbnail_path\n";
						}
						//die;
					}
				}
			}
		}
            
	}
}
