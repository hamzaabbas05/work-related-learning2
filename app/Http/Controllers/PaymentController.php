<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PaymentRepository;

class PaymentController extends Controller
{
    private $bredCrum = "Payment Management";
    
    protected $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }
    
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $paymentObj = $this->paymentRepository->getAll();
        $status = "all";
        return View('admin.payment.payment', compact('paymentObj', 'bredCrum', 'status'));
    }

    public function getPaymentListByStatus($status)
    {
        $bredCrum = $this->bredCrum;
        $paymentObj = $this->paymentRepository->getByStatus($status);
        return View('admin.payment.payment', compact('paymentObj', 'bredCrum', 'status'));
    }


    public function export($status)
    {
        $filename = "payment".date('Y-m-d H:i:s');
        \Excel::create($filename, function($excel) use($status){
            $sheetName = "payment";
            $excel->sheet($sheetName, function($sheet) use($status) {
                $paymentObj = $this->paymentRepository->getByStatus($status);
                $sheet->loadView('admin.export.payment')->with('paymentObj', $paymentObj);
            });
        })->download('xls');
    }

    public function getTransactions($status)
    {
        if ($status == "completed") {
            $status = "Paid";
        } elseif ($status == "pending") {
            $status = "Pending";
        }
        $paymentObj = $this->paymentRepository->getTransactionsForUser(\Auth::user()->id, $status);
        return View('transactions', compact('paymentObj', 'status'));
    }
}
