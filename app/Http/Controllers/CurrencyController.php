<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CurrencyRepository;

class CurrencyController extends Controller
{
    private $bredCrum = "Currency";
    
    protected $currencyRepository;

    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->currencyRepository->getAll();
        return View('admin.currency.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        return View('admin.currency.create', compact('bredCrum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('currency_name' => $request->get('currency_name'),'currency_code' => $request->get('currency_code'),'currency_symbol' => $request->get('currency_symbol'),'currency_rate' => $request->get('currency_rate'),
                        'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->currencyRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/currency/create')->withErrors($validator);
        } else {
            $propertyTypeObj = $this->currencyRepository->create($postData);
            flash('Currency Added Successfully', 'success');
            return \Redirect('admin/currency');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->currencyRepository->getById($id);
        return View('admin.currency.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = array('currency_name' => $request->get('currency_name'),'currency_code' => $request->get('currency_code'),'currency_symbol' => $request->get('currency_symbol'),'currency_rate' => $request->get('currency_rate'),
                        'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->currencyRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/currency/'.$id.'/edit')->withErrors($validator);
        } else {
            $Obj = $this->currencyRepository->update($postData, $id);
            flash('Currency Updated Successfully', 'success');
            return \Redirect('admin/currency');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
