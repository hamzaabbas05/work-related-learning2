<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SecondarySchoolGroups;
use App\Http\Requests\SecondaryGroupRequest;

class SecondarySchoolGroupsController extends Controller {

    private $bredCrum = "Secondary School Groups";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $bredCrum = $this->bredCrum;
        $groups = SecondarySchoolGroups::where(["is_delete" => "0"])->get();
        return View('admin.school_groups.index', compact('bredCrum', 'groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $bredCrum = $this->bredCrum;
        $school_group = new SecondarySchoolGroups();
        return View('admin.school_groups.create', compact('bredCrum', 'school_group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SecondarySchoolGroups $school_group) {
        
        $school_group = new SecondarySchoolGroups();
        $school_group->group_name = $request->get("group_name");
        $school_group->group_date_in = $request->get("group_date_in");
        $school_group->group_date_out = $request->get("group_date_out");
        $school_group->pick_up_where = $request->get("pick_up_where");
        $school_group->drop_off_where = $request->get("drop_off_where");
        $school_group->pick_up_time = $request->get("pick_up_time");
        $school_group->drop_off_time = $request->get("drop_off_time");
        $school_group->link_to_route_to_meeting_point = $request->get("link_to_route_to_meeting_point");
        $school_group->link_to_route_from_meeting_point = $request->get("link_to_route_from_meeting_point");
        $school_group->catching_the_what_bus_to = $request->get("catching_the_what_bus_to");
        $school_group->catching_the_what_time_bus_to = $request->get("catching_the_what_time_bus_to");
        $school_group->catching_the_what_bus_back = $request->get("catching_the_what_bus_back");
        $school_group->catching_the_what_time_bus_back = $request->get("catching_the_what_time_bus_back");
        $school_group->flight_to = $request->get("flight_to");
        $school_group->flight_back = $request->get("flight_back");
        if ($school_group->save()) {
            flash('Secondary School Group Added Successfully', 'success');
            return \Redirect('admin/schoolGroups');
        } else {
            flash('There has been some problem while adding Secondary School Group', 'error');
            return \Redirect('admin/schoolGroups/create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $school_group = SecondarySchoolGroups::find($id);
        $bredCrum = $this->bredCrum;
        return View('admin.school_groups.edit', compact('bredCrum', 'school_group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $school_group = SecondarySchoolGroups::find($id);
        $school_group->group_name = $request->get("group_name");
        $school_group->group_date_in = $request->get("group_date_in");
        $school_group->group_date_out = $request->get("group_date_out");
        $school_group->pick_up_where = $request->get("pick_up_where");
        $school_group->drop_off_where = $request->get("drop_off_where");
        $school_group->pick_up_time = $request->get("pick_up_time");
        $school_group->drop_off_time = $request->get("drop_off_time");
        $school_group->link_to_route_to_meeting_point = $request->get("link_to_route_to_meeting_point");
        $school_group->link_to_route_from_meeting_point = $request->get("link_to_route_from_meeting_point");
        $school_group->catching_the_what_bus_to = $request->get("catching_the_what_bus_to");
        $school_group->catching_the_what_time_bus_to = $request->get("catching_the_what_time_bus_to");
        $school_group->catching_the_what_bus_back = $request->get("catching_the_what_bus_back");
        $school_group->catching_the_what_time_bus_back = $request->get("catching_the_what_time_bus_back");
        $school_group->flight_to = $request->get("flight_to");
        $school_group->flight_back = $request->get("flight_back");
        if ($school_group->save()) {
            flash('Secondary School Group Added Successfully', 'success');
            return \Redirect('admin/schoolGroups');
        } else {
            flash('There has been some problem while saving Secondary School Group', 'error');
            return \Redirect('admin/schoolGroups/' . $school_group->id . "/edit");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function studentToGroup() {
        $bredCrum = "Students To Groups";
        $groups = SecondarySchoolGroups::where(["is_delete" => "0"])->get();
        $users = \App\User::get();
        return View('admin.school_groups.studentToGroup', compact('bredCrum', 'groups', 'users'));
    }

    public function updateStudentToGroups(Request $request) {
        $this->validate($request, [
            'user_id' => 'required',
            'secondary_group_id' => 'required',
        ]);
        $user_id = $request->get("user_id");
        $username = "";
        if ($user_id > 0) {
            $user = \App\User::find($user_id);
            if (isset($user->id)) {
                $user->secondary_group_id = $request->get("secondary_group_id");
                $user->save();
                $username = $user->name . " " . $user->lastname . " (" . $user->email . ")";
            }
        }
        return redirect()->back()->with("message", "Group has been added to User : " . $username);
    }

}
