<?php 

 $this->data['CalendarBooking'] = $this->product_model->get_all_details(CALENDARBOOKING,array('PropId'=>$this->data['productDetails']->row()->id));
  if($this->data['CalendarBooking']->num_rows() > 0)
  { 
   foreach($this->data['CalendarBooking']->result()  as $CRow){
    $DisableCalDate .='"'.$CRow->the_date.'",';
   }
   $this->data['forbiddenCheckIn']='['.$DisableCalDate.']';
  }
  else
  {
   $this->data['forbiddenCheckIn']='[]';
   $this->data['forbiddenCheckOut']='[]';
  }
  $all_dates = array();
  $selected_dates = array();
  foreach($this->data['CalendarBooking']->result()  as $date)
  { 
   $all_dates[] = trim($date->the_date);
   $date1 = new DateTime(trim($date->the_date));
   $date2 = new DateTime($prev);
   $diff = $date2->diff($date1)->format("%a");
   if($diff == '1')
   { 
    $selected_dates[] = trim($date->the_date);
   } 
   $prev = trim($date->the_date);
   $DisableCalDate = '';
   foreach($all_dates as $CRow)
   {
    $DisableCalDate .= '"'.$CRow.'",';
   } 
   $this->data['forbiddenCheckIn']='['.$DisableCalDate.']';
   $DisableCalDate = '';
   foreach($selected_dates as $CRow)
   {
    $DisableCalDate .= '"'.$CRow.'",';
   } 
   $this->data['forbiddenCheckOut']='['.$DisableCalDate.']';
  }














$aLong = $lon+0.05;
		$bLong = $lon-0.05;
		$aLat = $lat+0.05;
		$bLat = $lat-0.05;
		$whereNew = 'where (pa.lat < '.$aLat.' AND pa.lat > '.$bLat.' AND pa.lang < '.$aLong.' AND pa.lang >'.$bLong.') AND '.$condition;

bookings
$this->data['CalendarBooking'] = $this->product_model->get_all_details(CALENDARBOOKING,array('PropId'=>$this->data['productDetails']->row()->id));
		if($this->data['CalendarBooking']->num_rows() > 0)
		{	
			foreach($this->data['CalendarBooking']->result()  as $CRow){
				$DisableCalDate .='"'.$CRow->the_date.'",';
			}
			$this->data['forbiddenCheckIn']='['.$DisableCalDate.']';
		}
		else
		{
			$this->data['forbiddenCheckIn']='[]';
			$this->data['forbiddenCheckOut']='[]';
		}
		$all_dates = array();
		$selected_dates = array();
		foreach($this->data['CalendarBooking']->result()  as $date)
		{	
			$all_dates[] = trim($date->the_date);
			$date1 = new DateTime(trim($date->the_date));
			$date2 = new DateTime($prev);
			$diff = $date2->diff($date1)->format("%a");
			if($diff == '1')
			{	
				$selected_dates[] = trim($date->the_date);
			}	
			$prev = trim($date->the_date);
			$DisableCalDate = '';
			foreach($all_dates as $CRow)
			{
				$DisableCalDate .= '"'.$CRow.'",';
			}	
			$this->data['forbiddenCheckIn']='['.$DisableCalDate.']';
			$DisableCalDate = '';
			foreach($selected_dates as $CRow)
			{
				$DisableCalDate .= '"'.$CRow.'",';
			}	
			$this->data['forbiddenCheckOut']='['.$DisableCalDate.']';
		}	



<script type="text/javascript">
jQuery(document).ready( function () {
	initialize();
});

var array1 = <?php echo $forbiddenCheckIn; ?>;   
var array2 = <?php echo $forbiddenCheckOut; ?>;   
</script>

<?php if($product->calendar_checked == 'onetime') { ?>
<script type="text/javascript">
$(function() {
	var oneTimeMinY = '<?php echo date('Y', strtotime($product->datefrom));?>';
	var oneTimeMinM = '<?php echo date('m', strtotime($product->datefrom));?>';
	var oneTimeMinD = '<?php echo date('d', strtotime($product->datefrom));?>';
	var oneTimeMaxY = '<?php echo date('Y', strtotime($product->dateto));?>';
	var oneTimeMaxM = '<?php echo date('m', strtotime($product->dateto));?>';
	var oneTimeMaxD = '<?php echo date('d', strtotime($product->dateto));?>';
	var oneTimeMin = new Date(oneTimeMinY, oneTimeMinM -1, oneTimeMinD);
	
	var oneTimeMax = new Date(oneTimeMaxY, oneTimeMaxM, oneTimeMaxD);
	$( "#datefrom" ).datepicker({
		changeMonth: true,
		minDate: new Date(oneTimeMinY, oneTimeMinM -1, oneTimeMinD),
        maxDate: new Date(oneTimeMaxY, oneTimeMaxM -1, oneTimeMaxD),
		beforeShowDay: function(date){
			var string = $.datepicker.formatDate('yy-mm-dd', date);
			var check = array1.indexOf(string) == -1;
			if(typeof(check)!='undefined')
			{
				if(check)
				{
					return [true, '', ''];
				}
				else
				{
					return [false, "bookedDate", "check"];
				}
			}
		},
		onClose: function( selectedDate ) {
			$( "#expiredate" ).datepicker( "option", "minDate", selectedDate );
			$( "#expiredate" ).val('');
		}
	});
	
	$( "#expiredate" ).datepicker({
		changeMonth: true,
		numberOfMonths: 1,
		minDate: new Date(oneTimeMinY, oneTimeMinM -1, oneTimeMinD),
        maxDate: new Date(oneTimeMaxY, oneTimeMaxM -1, oneTimeMaxD),
		beforeShowDay: function(date){
			var string = $.datepicker.formatDate('yy-mm-dd', date);
			var check = array2.indexOf(string) == -1;
			if(typeof(check)!='undefined')
			{
				if(check)
				{
					return [true, '', ''];
				}
				else
				{			
					return [false, "bookedDate", "check"];
				}
			}
		},
		onClose: function( selectedDate ) {
			$( "#datefrom" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	
	$( "#datefromContact" ).datepicker({
		changeMonth: true,
		numberOfMonths: 1,
		minDate: new Date(oneTimeMinY, oneTimeMinM -1, oneTimeMinD),
        maxDate: new Date(oneTimeMaxY, oneTimeMaxM -1, oneTimeMaxD),
		beforeShowDay: function(date){
			var string = $.datepicker.formatDate('yy-mm-dd', date);
			var check = array1.indexOf(string) == -1;
			if(typeof(check)!='undefined')
			{
				if(check)
				{
					return [true, '', ''];
				}
				else
				{
					return [false, "bookedDate", "check"];
				}
			}
		},
		onClose: function( selectedDate ) {
			$( "#expiredateContact" ).datepicker( "option", "minDate", selectedDate );
			$( "#expiredateContact" ).val('');
		}
	});
	
	$( "#expiredateContact" ).datepicker({
		changeMonth: true,
		numberOfMonths: 1,
		minDate: new Date(oneTimeMinY, oneTimeMinM -1, oneTimeMinD),
        maxDate: new Date(oneTimeMaxY, oneTimeMaxM -1, oneTimeMaxD),
		beforeShowDay: function(date){
			var string = $.datepicker.formatDate('yy-mm-dd', date);
			var check = array2.indexOf(string) == -1;
			if(typeof(check)!='undefined')
			{
				if(check)
				{
					return [true, '', ''];
				}
				else
				{			
					return [false, "bookedDate", "check"];
				}
			}
		},
		onClose: function( selectedDate ) {
			$( "#datefromContact" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<?php } else { ?>
<script type="text/javascript">
$(function() {
	$( "#datefrom" ).datepicker({
		changeMonth: true,
		numberOfMonths: 1,
		minDate:0,
		beforeShowDay: function(date){
			var string = $.datepicker.formatDate('yy-mm-dd', date);
			var check = array1.indexOf(string) == -1;
			if(typeof(check)!='undefined')
			{
				if(check)
				{
					return [true, '', ''];
				}
				else
				{
					return [false, "bookedDate", "check"];
				}
			}
		},
		onClose: function( selectedDate ) {
			$( "#expiredate" ).datepicker( "option", "minDate", selectedDate );
			$( "#expiredate" ).val('');
		}
	});
	
	$( "#expiredate" ).datepicker({
		changeMonth: true,
		numberOfMonths: 1,
		minDate:0,
		beforeShowDay: function(date){
			var string = $.datepicker.formatDate('yy-mm-dd', date);
			var check = array2.indexOf(string) == -1;
			if(typeof(check)!='undefined')
			{
				if(check)
				{
					return [true, '', ''];
				}
				else
				{			
					return [false, "bookedDate", "check"];
				}
			}
		},
		onClose: function( selectedDate ) {
			$( "#datefrom" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	
	$( "#datefromContact" ).datepicker({
		changeMonth: true,
		numberOfMonths: 1,
		minDate:0,
		beforeShowDay: function(date){
			var string = $.datepicker.formatDate('yy-mm-dd', date);
			var check = array1.indexOf(string) == -1;
			if(typeof(check)!='undefined')
			{
				if(check)
				{
					return [true, '', ''];
				}
				else
				{
					return [false, "bookedDate", "check"];
				}
			}
		},
		onClose: function( selectedDate ) {
			$( "#expiredateContact" ).datepicker( "option", "minDate", selectedDate );
			$( "#expiredateContact" ).val('');
		}
	});
	
	$( "#expiredateContact" ).datepicker({
		changeMonth: true,
		numberOfMonths: 1,
		minDate:0,
		beforeShowDay: function(date){
			var string = $.datepicker.formatDate('yy-mm-dd', date);
			var check = array2.indexOf(string) == -1;
			if(typeof(check)!='undefined')
			{
				if(check)
				{
					return [true, '', ''];
				}
				else
				{			
					return [false, "bookedDate", "check"];
				}
			}
		},
		onClose: function( selectedDate ) {
			$( "#datefromContact" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<?php } ?>


function DateBetween(){

	var date = $("#datefrom").datepicker('getDate');

    var start = date.setDate(date.getDate()),

	end = $("#expiredate").datepicker("getDate"),

        currentDate = new Date(start),

        

        between = [];

    while (currentDate < end) {

        between.push(new Date(currentDate));

        currentDate.setDate(currentDate.getDate() + 1);

    }

   days = between.length;
   if(days == 0)
   return false;

    $('#results').val(between.join(','));

    var price=$("#Global_Price").val();

  

    $.ajax({

        type: 'POST',   

        url:baseURL+'site/product/ajaxdateCalculate',

        data:{'dateval':between.join(','),'pid':$("#prd_id").val(),'price':price},

        success:function(response){
			//alert(response);
			$('#price_div').html(response);
			return false;
        }

    });

 

}


function ajaxdateCalculate(){
		$id=$this->input->post('pid');
		$Price = $this->input->post('price');
		$CalendarDateArr = explode(',',$this->input->post('dateval'));
		
		foreach($CalendarDateArr as $CalendarDateRow){
			$CalendarTimeDateArr = explode(' GMT',$CalendarDateRow);
			$sadfsd=trim($CalendarTimeDateArr[0]);
			$startDate = strtotime($sadfsd);    
			$result[] =  date("Y-m-d",$startDate);
		}
		$DateCalCul=0;
		$this->data['ScheduleDatePrice'] = $this->product_model->get_all_details(SCHEDULE,array('id'=>$id));
		if($this->data['ScheduleDatePrice']->row()->data !=''){
			$dateArr=json_decode($this->data['ScheduleDatePrice']->row()->data);
			$finaldateArr=(array)$dateArr;
			foreach($result as $Rows){
				if (array_key_exists($Rows, $finaldateArr)) {
					$DateCalCul= $DateCalCul+$finaldateArr[$Rows]->price;
				}else{
					$DateCalCul= $DateCalCul+$Price;
				}
			}
		}else{
			$DateCalCul = (count($result) * $Price);
			
		}
		//echo $DateCalCul; die;
		$service_tax_query='SELECT * FROM '.COMMISSION.' WHERE seo_tag="guest-booking" AND status="Active"';
		$service_tax=$this->product_model->ExecuteQuery($service_tax_query);
		if($service_tax->num_rows() == 0)
		{
			$this->data['taxValue'] = '0.00';
			$this->data['taxString'] = 'No Tax';
		}
		else 
		{
			$this->data['commissionType'] = $service_tax->row()->promotion_type;
			$this->data['commissionValue'] = $service_tax->row()->commission_percentage;
			if($service_tax->row()->promotion_type=='flat')
			{
					$currencyCode     = $this->db->where('id',$id)->get(PRODUCT)->row()->currency;
					$currInto_result = $this->db->where('currency_type',$currencyCode)->get(CURRENCY)->row();
		
					$rate = $service_tax->row()->commission_percentage * $currInto_result->currency_rate;
				$this->data['taxValue'] = $rate;
				$this->data['taxString'] = $rate;
			}
			else
			{
				$finalTax = ( $service_tax->row()->commission_percentage * $DateCalCul)/100;
				$this->data['taxValue'] = $finalTax;
				$this->data['taxString'] = $finalTax;
			}
		}
		
		$this->data['total_nights'] = count($result);
		$this->data['product_id'] = $id;
		$this->data['subTotal'] = $DateCalCul;
		$this->data['total_value'] = CurrencyValue($id,$DateCalCul);
		$this->data['net_total_value'] = stripslashes($DateCalCul) + $this->data['taxValue'];
		$this->data['net_total_string'] = CurrencyValue($id,$this->data['net_total_value']);

		$this->data['requestType'] = 'booking_request'; 
		$this->load->view('site/rentals/price_value',$this->data);
		
	}



function myfunction(val) {



var famt =$('#bookingsubtot').html();

//alert(famt);

var service_tax =$('#servicetax').html();

//alert(service_tax+famt);

famt = famt.replace ( /[^\d.]/g, '' );

//famt=parseInt(famt);



service_tax = service_tax.replace ( /[^\d.]/g, '' );

service_tax=parseInt(service_tax);



//service_tax=parseInt(service_tax);



if('<?php echo $service_tax->num_rows()?>'==0)

{

total =(val*famt);





$('#bookingtot').html(total);



}

else if('<?php echo $service_tax->row()->promotion_type; ?>'=='flat')

{

total =(val*famt);

var gtotal = (val*famt);

total =(val*famt)+parseInt(service_tax);

var stax = total-gtotal;

//$('#bookingtot').html(total);

//$('#stax').html(stax);

}

else{

total =(val*famt);

var gtotal = (val*famt);

total =total+(total*parseInt(service_tax)/100);

total=Math.round(total);

var stax = total-gtotal;

//$('#bookingtot').html(total);

//$('#stax').html(stax);

}

}

</script>