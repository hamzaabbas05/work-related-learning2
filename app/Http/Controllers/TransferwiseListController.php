<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TransferwiseListController extends Controller {

    private $bredCrum = "Transferwise.com Excel Sheet";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $bredCrum = $this->bredCrum;
        $rentalPaidJobs = \App\RentalsEnquiry::join('property', 'property.id', '=', 'fc_rentalsenquiry.prd_id')
                        ->where(["fc_rentalsenquiry.approval" => "Accept", "property.property_status" => "paid"])
                        ->select("fc_rentalsenquiry.id as rental_id", "property.id as property_id", "fc_rentalsenquiry.renter_id", "fc_rentalsenquiry.user_id AS r_user_id", "property.user_id AS p_user_id", "fc_rentalsenquiry.Bookingno")->get();
        //return $rentalPaidJobs;
        return view("admin.transferwise.view", ['rentalPaidJobs' => $rentalPaidJobs, 'bredCrum' => $bredCrum]);
    }

    public function exportTransferwiseSheet() {
        $bredCrum = $this->bredCrum;
//        $rentalPaidJobs = \App\RentalsEnquiry::join('property', 'property.id', '=', 'fc_rentalsenquiry.prd_id')
//                        ->where(["fc_rentalsenquiry.approval" => "Accept", "property.property_status" => "paid"])
//                        ->select("fc_rentalsenquiry.id as rental_id", "property.id as property_id", "fc_rentalsenquiry.renter_id", "fc_rentalsenquiry.user_id AS r_user_id", "property.user_id AS p_user_id", "fc_rentalsenquiry.Bookingno")->get();
        //return $rentalPaidJobs;

        $filename = "Transferwise_sheet_" . date('Y-m-d H:i:s');
        \Excel::create($filename, function($excel) {
            $sheetName = "Transferwise_sheet";
            $excel->sheet($sheetName, function($sheet) {
                $rentalPaidJobs = \App\RentalsEnquiry::join('property', 'property.id', '=', 'fc_rentalsenquiry.prd_id')
                                ->where(["fc_rentalsenquiry.approval" => "Accept", "property.property_status" => "paid"])
                                ->select("fc_rentalsenquiry.id as rental_id", "property.id as property_id", "fc_rentalsenquiry.renter_id", "fc_rentalsenquiry.user_id AS r_user_id", "property.user_id AS p_user_id", "fc_rentalsenquiry.Bookingno")->get();
                $sheet->loadView('admin.export.transferwise')->with('rentalPaidJobs', $rentalPaidJobs);
            });
        })->download('xls');
    }

}
