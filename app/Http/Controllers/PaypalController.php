<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Paypal;
use App\RentalsEnquiry;
use App\Payment;
use Session;
use App\Booking;
use App\CommissionTrack;
use App\Commission;
use App\Schedule;
use App\Services\Constants\AppConstants;

class PaypalController extends Controller
{

    private $_apiContext;

    public function __construct()
    {
        $this->_apiContext = Paypal::ApiContext(
            \config('services.paypal.client_id'),
            \config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 60,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));

    }

    public function getCheckout(Request $request)
    {    
        $payer = Paypal::Payer();
        $payer->setPaymentMethod('paypal');
        $enquiryid = $request->get('enquiryId');
        $enquiryObj = RentalsEnquiry::where('id', $enquiryid)->first();
        $amount = PayPal:: Amount();
        $amount->setCurrency('EUR');
        $amount->setTotal($enquiryObj->totalAmt);
        $now = date("Y-m-d H:i:s");
        $taxObj = Commission::getById(AppConstants::TAX_COMMISSION, true);
        $tax = 0;
        if ($taxObj->count() > 0) {
            $tax = Commission::calculateCommission($taxObj, $enquiryObj->totalAmt);
        }
        $dealCodeNumber = mt_rand();
        $paymentArr=array(
            'product_id'=>$enquiryObj->prd_id,
            'price'=>$enquiryObj->totalAmt,
            'indtotal'=>$enquiryObj->property->price_night,
            'tax'=>$tax,
            'sumtotal'=>$enquiryObj->totalAmt,
            'user_id'=>\Auth::user()->id,
            'sell_id'=>$enquiryObj->property->user_id,
            'created' => $now,
            'dealCodeNumber' => $dealCodeNumber,
            'status' => 'Pending',
            'shipping_status' => 'Pending',
            'total'  => $enquiryObj->totalAmt,
            'EnquiryId'=>$enquiryid,
            'inserttime' => date("Y-m-d H:i:s"));
        

        $paymentExists = Payment::where('EnquiryId', $enquiryid)->where('status', '!=', 'Paid')->exists();
        if ($paymentExists) {
            $paymentUpdate = Payment::where('EnquiryId', $enquiryid)->update($paymentArr);
            $paymentObj = Payment::where('EnquiryId', $enquiryid)->first(); 

        } else {
           $paymentObj = Payment::create($paymentArr);
        }
        Session::put('PaymentId',$paymentObj->id);
        Session::put('dealCodeNumber',$dealCodeNumber);
        $transaction = Paypal::Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription('Book Room for Vacations');
        $redirectUrls = Paypal:: RedirectUrls();
        $redirectUrls->setReturnUrl(route('getDone'));
        $redirectUrls->setCancelUrl(route('getCancel'));
        
        $payment = Paypal::Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));
        $response = $payment->create($this->_apiContext);
        $redirectUrl = $response->links[1]->href;
        return redirect()->to($redirectUrl);
    }
    
    public function getDonecheck()
    {
        $DealCodeNo = 535809363;
        $paymentObj = Payment::where('dealCodeNumber', $DealCodeNo)->first();
        $enquiryObj = RentalsEnquiry::where('id', $paymentObj->EnquiryId)->first();
        $dataArr1 = array('booking_status'=>'Booked');
        $paymentArr = array('status' => 'Paid','shipping_status' => 'Processed', 'paypal_transaction_id' => 'PAY-6BU815396D448034ALATSJPA', 'payer_email' => 'test@gmail.com','payment_type' => 'Paypal');
            Payment::where('dealCodeNumber', $DealCodeNo)->where('user_id', 3)->update($paymentArr);
        RentalsEnquiry::where('id', $enquiryObj->id)->update($dataArr1);
            $propertyId = $enquiryObj->prd_id;
            $arrival = $enquiryObj->checkin;
            $depature = $enquiryObj->checkout;
            $dates = $this->getDatesFromRange($arrival, $depature);
            $this->saveBookedDates($dates, $propertyId);
            $this->updateScheduleCalendar($propertyId, $enquiryObj->property->price_night);
            $this->updateCommissionTracking($enquiryObj);
    }


    public function getDone(Request $request)
    {
        $id = $request->get('paymentId');
        $token = $request->get('token');
        $payer_id = $request->get('PayerID');


        // Get the payment ID before session clear
        $payment_id = Session::get('paypal_payment_id');
        $dealCodeNumber=Session::get('dealCodeNumber');
        Session::forget('dealCodeNumber');
        Session::forget('paypal_payment_id');

        if (empty($payer_id) || empty($token)) {
            /*$result = "cancel";
            $paymentObj = Payment::where('dealCodeNumber', $DealCodeNo)->first();
            $enquiryObj = RentalsEnquiry::where('id', $paymentObj->EnquiryId)->first();
            return View('paymentResult', compact('enquiryObj', 'result'));*/
            return  \Redirect('payment/cancel');

        }
        $payment = Paypal::getById($id, $this->_apiContext);
        $paymentExecution = Paypal::PaymentExecution();
        $paymentExecution->setPayerId($payer_id);
        $result = $payment->execute($paymentExecution, $this->_apiContext);
        //print_r($executePayment);
        if ($result->getState() == 'approved') {
            \DB::beginTransaction();
        try {
            $transId=$result->id;
            $PayerEmail=$result->payer->payer_info->email;
            $UserNo = \Auth::user()->id;
            $DealCodeNo = $dealCodeNumber; 
            $paymentObj = Payment::where('dealCodeNumber', $DealCodeNo)->first();
            $enquiryObj = RentalsEnquiry::where('id', $paymentObj->EnquiryId)->first();
            $dataArr1 = array('booking_status' => 'Booked');
            // Update payment Table
            $paymentArr = array('status' => 'Paid','shipping_status' => 'Processed', 'paypal_transaction_id' => $transId, 'payer_email' => $PayerEmail,'payment_type' => 'Paypal');
            Payment::where('dealCodeNumber', $DealCodeNo)->where('user_id', $UserNo)->update($paymentArr);
              
            $updated = RentalsEnquiry::where('id', $enquiryObj->id)->update($dataArr1);
            $propertyId = $enquiryObj->prd_id;
            $arrival = $enquiryObj->checkin;
            $depature = $enquiryObj->checkout;
            $dates = $this->getDatesFromRange($arrival, $depature);
            $this->updateCommissionTracking($enquiryObj);
            $this->saveBookedDates($dates, $propertyId);
            $this->updateScheduleCalendar($propertyId, $enquiryObj->property->price_night);
            \DB::commit();
            $enquiryObj = RentalsEnquiry::where('id', $paymentObj->EnquiryId)->first();
            $result = "success";
            return View('paymentresult', compact('enquiryObj', 'result'));
            } catch (\Exception $e) {
                    \DB::rollback();
                    print_r($e->getMessage());
            }
        } else 
        {
             $result = "pending";
            return View('paymentresult', compact('enquiryObj', 'result'));
        }
    }

    public function getDatesFromRange($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }

     public function saveBookedDates($dates, $propertyId)
    {
        $i=1;
        $dateMinus1= count($dates)-1;
        foreach($dates as $date){
                if($i <= $dateMinus1){
                    $bookingObj = Booking::where('PropId', $propertyId)->where('id_state', 1)->where('id_item', 1)->where('the_date', $date)->get();
                    if ($bookingObj->count() > 0) {
                        
                    } else{
                        $dataArr = array('PropId' => $propertyId,
                                         'id_state' => 1,
                                         'id_item' => 1,
                                         'the_date' => $date
                                        );
                        Booking::create($dataArr);
                    }
                }
                $i++;
            }
    }

    public function updateScheduleCalendar($propertyId, $price)
    {
        $DateArr = Booking::where('PropId', $propertyId)->get();
        $dateDispalyRowCount=0;
        $dateArrVAl="";
        if($DateArr->count() > 0){
            $dateArrVAl .='{';
            foreach($DateArr as $dateDispalyRow){
                if($dateDispalyRowCount==0){
                    $dateArrVAl .='"'.$dateDispalyRow->the_date.'":{"available":"1","bind":0,"info":"","notes":"","price":"'.$price.'","promo":"","status":"booked"}';
                }else{
                    $dateArrVAl .=',"'.$dateDispalyRow->the_date.'":{"available":"1","bind":0,"info":"","notes":"","price":"'.$price.'","promo":"","status":"booked"}';
                }
                $dateDispalyRowCount=$dateDispalyRowCount+1;
            }
            $dateArrVAl .='}';
            $scheduleObj = Schedule::where('id', $propertyId)->exists();
            $inputArr4 = array('id' =>$propertyId,'data' => trim($dateArrVAl));
            if ($scheduleObj) {
                Schedule::where('id', $propertyId)->update($inputArr4);
            } else {
                Schedule::create($inputArr4);
            }  
        }  
    }

    public function updateCommissionTracking($enquiryObj)
    {

        $guest_fee = $enquiryObj->serviceFee;
        $netAmount = $enquiryObj->totalAmt-$guest_fee;
        $hostCommissionObj = Commission::getById(AppConstants::HOST_COMMISSION, true);
        $host_fee = 0;
        if ($hostCommissionObj->count() > 0) {
            $host_fee = Commission::calculateCommission($hostCommissionObj, $netAmount);
        }
        $hostemail = $enquiryObj->property->user->email;
        $payable_amount = $netAmount - $host_fee;
        $data1 = array('host_email'=>$hostemail,
            'booking_no'=>$enquiryObj->Bookingno,
            'total_amount'=>$enquiryObj->totalAmt,
            'guest_fee'=>$guest_fee,
            'host_fee'=>$host_fee,
            'payable_amount'=>$payable_amount
            );
        $commision = CommissionTrack::where('booking_no', $enquiryObj->Bookingno)->exists();
        if ($commision) {
            CommissionTrack::where('booking_no', $enquiryObj->Bookingno)->update($data1);
        } else 
        {
            CommissionTrack::create($data1);
        }
    }

    public function getCancel()
    {
        return  \Redirect('payment/cancel');
    }

    public function showCancel()
    {
        return View('paymentcancel');
    }
}