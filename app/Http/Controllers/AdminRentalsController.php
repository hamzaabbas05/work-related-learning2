<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PropertytypeRepository;
use App\Repositories\RoomtypeRepository;
use App\Repositories\PropertyRepository;
use App\Repositories\AmenityRepository;
use App\Repositories\SpecialAttributesRepository;
use App\Services\Constants\AppConstants;
use App\Repositories\CancellationRepository;
use App\PropertyImages;
use Carbon\Carbon;
use App\Review;
use App\User;
use App\Property;
use App\RentalsEnquiry;
use App\Payment;
use App\Tutor;
use App\Booking;

class AdminRentalsController extends Controller {

    private $bredCrum = "Property";
    protected $propertyTypeRepository;
    protected $roomTypeRepository;
    protected $propertyRepository;
    protected $amenityRepository;
    protected $specialAttributesRepository;
    protected $cancellationRepository;

    public function __construct(PropertytypeRepository $propertyTypeRepository, RoomtypeRepository $roomTypeRepository, PropertyRepository $propertyRepository, AmenityRepository $amenityRepository, SpecialAttributesRepository $specialAttributesRepository, CancellationRepository $cancellationRepository) {
        $this->propertyTypeRepository = $propertyTypeRepository;
        $this->roomTypeRepository = $roomTypeRepository;
        $this->propertyRepository = $propertyRepository;
        $this->amenityRepository = $amenityRepository;
        $this->specialAttributesRepository = $specialAttributesRepository;
        $this->cancellationRepository = $cancellationRepository;
    }

    public function dashboard() {
        $bredCrum = "Admin Dashboard";
        $usersCount = User::count();
        $propertyCount = Property::count();
        $activeusers = User::where('status', 1)->count();
        $deactiveusers = User::where('status', 2)->count();
        $activepro = Property::where('status', 1)->count();
        $deactivepro = Property::where('status', 2)->count();
        $reservation = RentalsEnquiry::count();
        $preservation = RentalsEnquiry::where('approval', 'Pending')->count();
        $areservation = RentalsEnquiry::where('approval', 'Accept')->count();
        $dreservation = RentalsEnquiry::where('approval', 'Decline')->count();
        $ttransactions = Payment::count();
        $ctransactions = Payment::where('status', 'Paid')->count();
        $ptransactions = Payment::where('status', 'Pending')->count();
        return View('admin.dashboard', compact('bredCrum', 'usersCount', 'propertyCount', 'activeusers', 'deactiveusers', 'activepro', 'deactivepro', 'reservation', 'preservation', 'areservation', 'dreservation', 'ttransactions', 'ptransactions', 'ctransactions'));
    }

    public function propertyexport() {
        $filename = "property" . date('Y-m-d H:i:s');
        \Excel::create($filename, function($excel) {
            $sheetName = "property";
            $excel->sheet($sheetName, function($sheet) {
                $reserveObj = $this->propertyRepository->getAll();
                $sheet->loadView('admin.export.property')->with('paymentObj', $reserveObj);
            });
        })->download('xls');
    }

    public function manage() {
        $bredCrum = $this->bredCrum;
        //$currentObj = $this->propertyRepository->getAll();
        $currentObj = Property::where("property_status", "!=", "paid")->orderBy('status', 'ASC')->orderBy('status', 'DESC')->get();
        //return $currentObj;
        return View('admin.property.manage', compact('bredCrum', 'currentObj'));
    }
    
    public function manageDistanceHomeWork(){
        $bredCrum = $this->bredCrum;
        //$currentObj = $this->propertyRepository->getAll();
        $currentObj = Property::where("property_status", "!=", "paid")->get();
        //return $currentObj;
        return View('admin.property.manageDistanceHomeWork', compact('bredCrum', 'currentObj'));
    }

    public function managePaid() {
        $bredCrum = $this->bredCrum;
        //$currentObj = $this->propertyRepository->getAll();
        $currentObj = Property::where("property_status", "paid")->orderBy('status', 'ASC')->orderBy('status', 'DESC')->get();
        //return $currentObj;
        return View('admin.property.managePaid', compact('bredCrum', 'currentObj'));
    }

    public function create() {
        $bredCrum = $this->bredCrum;
        $propertyTypes = $this->propertyTypeRepository->getAll(true);
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        return View('admin.property.create', compact('bredCrum', 'propertyTypes', 'roomTypes', 'tutorRelation'));
    }

    public function saveRentalFirstStep(Request $request) {
        $data = array('user_id' => \Auth::user()->id,
            'property_type' => $request->get('wcategory'),
            'exptitle' => $request->get('workexp'),
            'description' => $request->get('desc'),
            'work_schedule' => $request->get('schedule'),
            'work_hours' => $request->get('hour')
        );
        $obj = $this->propertyRepository->create($data);
        flash('Property Added Successfully.Please Add More information', 'success');
        return "success," . $obj->id;
    }

    public function showBasic($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        $propertyTypes = $this->propertyTypeRepository->getAll(true);
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        return View('admin.property.spacebasics', compact('propertyObj', 'propertyTypes', 'roomTypes', 'tutorRelation'));
    }

    public function showBasicPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        $propertyTypes = $this->propertyTypeRepository->getAll(true);
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        return View('admin.property.spacebasicsPaid', compact('propertyObj', 'propertyTypes', 'roomTypes', 'tutorRelation'));
    }

    public function showDescription($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        return View('admin.property.spacedesc', compact('propertyObj'));
    }

    public function showDescriptionPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        return View('admin.property.spacedescPaid', compact('propertyObj'));
    }

    public function showAmenity($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        $commonAmenities = $this->amenityRepository->getByType(AppConstants::COMMON_AMENITIES);
        $additionalAmenities = $this->amenityRepository->getByType(AppConstants::ADDITIONAL_AMENITIES);
        $specialFeatures = $this->amenityRepository->getByType(AppConstants::SPECIAL_FEATURES);
        $safetyCheck = $this->amenityRepository->getByType(AppConstants::SAFETY_CHECKLISTS);
        $property_amenity = $propertyObj->amenities()->lists('id')->toArray();
        return View('admin.property.spaceamenities', compact('propertyObj', 'commonAmenities', 'additionalAmenities', 'specialFeatures', 'safetyCheck', 'property_amenity'));
    }

    public function showPhotos($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        return View('admin.property.spacephotos', compact('propertyObj'));
    }

    public function showPhotosPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        return View('admin.property.spacephotosPaid', compact('propertyObj'));
    }

    public function showLocation($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        return View('admin.property.spaceaddress', compact('propertyObj'));
    }

    public function showLocationPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        return View('admin.property.spaceaddressPaid', compact('propertyObj'));
    }

    public function showPricing($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        $pricing = \App\PropertyPricing::where("property_id", $propertyId)->first();
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        return View('admin.property.spaceprice', compact('propertyObj', 'pricing'));
    }

    public function showBooking($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        $cancellation = $this->cancellationRepository->getArray();
        return View('admin.property.spacebookings', compact('propertyObj', 'cancellation'));
    }

    public function showBookingPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        $cancellation = $this->cancellationRepository->getArray();
        return View('admin.property.spacebookingsPaid', compact('propertyObj', 'cancellation'));
    }

    public function showCalendar($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        $calendarObj = Booking::bookingForProperty($propertyId);
        list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
        return View('admin.property.spacecalendar', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut'));
    }

    public function showCalendarPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (!$propertyObj) {
            flash('Property Doesnot exists', 'success');
            return \Redirect('admin/property/manage');
        }
        $calendarObj = Booking::bookingForProperty($propertyId);
        list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
        return View('admin.property.spacecalendarPaid', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut'));
    }

    public function prepareRestrictedDays($calendarObj) {
        $forbiddenCheckIn = '[]';
        $forbiddenCheckOut = '[]';
        $DisableCalDate = "";
        $prev = "";
        $all_dates = array();
        $selected_dates = array();
        if ($calendarObj->count() > 0) {
            foreach ($calendarObj as $CRow) {
                $DisableCalDate .= '"' . $CRow->the_date . '",';
            }
            $forbiddenCheckIn = '[' . $DisableCalDate . ']';
        }
        foreach ($calendarObj as $date) {
            $all_dates[] = trim($date->the_date);
            $date1 = new \DateTime(trim($date->the_date));
            $date2 = new \DateTime($prev);
            $diff = $date2->diff($date1)->format("%a");
            if ($diff == '1') {
                $selected_dates[] = trim($date->the_date);
            }
            $prev = trim($date->the_date);
            $DisableCalDate = '';
            foreach ($all_dates as $CRow) {
                $DisableCalDate .= '"' . $CRow . '",';
            }
            $forbiddenCheckIn = '[' . $DisableCalDate . ']';
            $DisableCalDate = '';
            foreach ($selected_dates as $CRow) {
                $DisableCalDate .= '"' . $CRow . '",';
            }
            $forbiddenCheckOut = '[' . $DisableCalDate . ']';
        }

        return array($forbiddenCheckIn, $forbiddenCheckOut);
    }

    public function changeStatus(Request $request) {
        $propertyId = $request->get('id');
        $status = $request->get('status');
        $updateData = array('status' => $status);
        if ($status == 3) {
            $updateData = array('premium' => 1); //YOU WERE HERE 27th APR
        }
        if ($status == 4) {
            $updateData = array('premium' => 0);
        }

        $this->propertyRepository->update($updateData, $propertyId);
        return "success";
    }

    public function showReview() {
        $bredCrum = "Review Management";
        $reviews = Review::orderBy('dateAdded', 'desc')->get();
        return View('admin.review.manage', compact('reviews', 'bredCrum'));
    }

    public function reviewstatus(Request $request) {

        $reviewId = $request->get('id');
        $status = $request->get('status');
        $updateData = array('status' => $status);
        Review::where('id', $reviewId)->update($updateData);
        return "success";
    }

}
