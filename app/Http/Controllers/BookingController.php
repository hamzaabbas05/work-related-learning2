<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Mailer\MailNotificationsForUser;
use Carbon\Carbon;
use App\Booking;
use App\RentalsEnquiry;
use App\MedMessage;
use App\Schedule;
use Mail;
use App\User;
use Dompdf\Dompdf as Dompdf;
use PDF;
use App\Repositories\CmsRepository;

class BookingController extends Controller {

    private $bredCrum = "Booking Management";
    private $mailNotificationsForUser;
    protected $cmsRepository;

    public function __construct(MailNotificationsForUser $mailNotificationsForUser, CmsRepository $cmsRepository) {
        $this->mailNotificationsForUser = $mailNotificationsForUser;
        $this->cmsRepository = $cmsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = 'all') {
        $bredCrum = $this->bredCrum;
        $users_info = '';        
        // ini_set('memory_limit', '-1');
        if ($status == "accept") {
              $users_info = \App\RentalsEnquiry::with('user_detail')->where("approval", "Accept")->orderBy('dateAdded', 'DESC')->paginate('700');
           
        } else if ($status == "pending") {
            //return $status;
            $users_info = \App\RentalsEnquiry::with('user_detail')->where("approval", "Pending")->orderBy('dateAdded', 'DESC')->paginate('700');
            //return $users_info;
        } else if ($status == "all") {
            $users_info = \App\RentalsEnquiry::with('user_detail')->orderBy('dateAdded', 'DESC')->paginate('700');
        }
        
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        //     echo "STatus :".$status."<br>";
        //     echo "<pre>";
        //         print_r($bredCrum);
        //         // print_r($users_info);
        //     echo "</pre>";
        //     // exit;
        // }

        
        return View('admin.manage_booking.index', compact('bredCrum', 'users_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancelBooking($id) {
        $bredCrum = "Cancel booking";
        if ($id) {
            $booking = \App\RentalsEnquiry::where("id", $id)->first();
            $users = \App\User::get();
            $jobs = \App\Property::where(['status' => 1])->where('property_status', '!=', 'paid')->get(); //1 stands for unpaid and unpaidflash - 2 stands for paid - 0 stands for deleted or inactive

            return view('admin/manage_booking/cancel_booking', compact('bredCrum', 'booking', 'users', 'jobs'));
        } else {
            return redirect()->back()->with("error", "There is no selected job");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cancelBookingForm(Request $request) {
        //return $request->all();
        $id = $request->get("booking_id");
        $rentalObj = \App\RentalsEnquiry::where("id", $id)->first();
        if (isset($rentalObj->id)) {
            if ($request->get("cancel_type") == "cancel_booking") {
                //if date_in and date_out are same than only cancel booking 
                $rentalObj->cancelled = "Yes";
                $rentalObj->save();
                //sending cancellation email
                $this->mailNotificationsForUser->sendBookingCancelEmail($rentalObj);
            } else if ($request->get("cancel_type") == "cancel_save_booking") {
                //if date_in and date_out are not same than cancel current booking and add new booking with new dates
                $rentalObj->cancelled = "Yes";
                $rentalObj->save();

                if ($rentalObj->approval == "Accept") {
                    $approval = "Accept";
                } else {
                    $approval = "Pending";
                }

                $post_checkIn = date('Y-m-d', strtotime($request->get('startdate')));
                $post_checkOut = date('Y-m-d', strtotime($request->get('enddate')));
                $rental_checkIn = date('Y-m-d', strtotime($rentalObj->checkin));
                $rental_checkOut = date('Y-m-d', strtotime($rentalObj->checkout));
                if (($rental_checkIn == $post_checkIn) && ($rental_checkOut == $post_checkOut)) {
                    // send email that only This booking has been cancelled
                    $this->mailNotificationsForUser->sendBookingCancelEmail($rentalObj);
                    return redirect()->to('admin/bookedJobs/' . strtolower($approval))->with("message", $rentalObj->Bookingno . " Booking has been cancelled successfully");
                } else {
                    // checking if the current booking is pending or accepted:

                    $request->request->add(['booking_status' => $approval]);
                    if (isset($rentalObj->property->property_status)) {
                        if ($rentalObj->property->property_status == "unpaid" || $rentalObj->property->property_status == "unpaidflash") {
                            $getResult = $this->storeUnpaidPendingBooking($request, $rentalObj);
                            $this->mailNotificationsForUser->sendBookingCancelEmail($rentalObj);
                            return redirect()->to('admin/bookedJobs/' . strtolower($approval))->with("message", $getResult);
                        } else if ($rentalObj->property->property_status == "paid") {
                            if ($approval == "Accept") {
                                $getResult = $this->storePaidAcceptBooking($request, $rentalObj);
                                $this->mailNotificationsForUser->sendBookingCancelEmail($rentalObj);
                                return redirect()->to('admin/bookedJobs/' . strtolower($approval))->with("message", $getResult);
                            } else {
                                $getResult = $this->storePaidPendingBooking($request, $rentalObj);
                                $this->mailNotificationsForUser->sendBookingCancelEmail($rentalObj);
                                return redirect()->to('admin/bookedJobs/' . strtolower($approval))->with("message", $getResult);
                            }
                        } else {
                            return redirect()->back()->with("error", "There is no property type is found as unpaid or paid");
                        }
                    } else {
                        return redirect()->back()->with("error", "There is no property type is found");
                    }
                }
            } else {
                return redirect()->back()->with("error", "There is no selected Cancel Type");
            }
        } else {
            return redirect()->back()->with("error", "There is no selected Booking");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeUnpaidPendingBooking($request, $rentalObj) {
        $userId = $rentalObj->user_id;
        $userEnquiries = \App\RentalsEnquiry::where('user_id', $userId)->get();
        $checkIn = date('Y-m-d H:i:s', strtotime($request->get('startdate')));
        $checkOut = date('Y-m-d H:i:s', strtotime($request->get('enddate')));
        $allowBooking = 0;
        $countEnquiry = 0;
        $settingsObj = \App\PropertySettings::propertySettings();
        $allowedEnquiryCount = $settingsObj->booking_request;
        $allowedEnquiryCount = $allowedEnquiryCount + 20;
        //echo $allowedEnquiryCount."<br />";
        if ($userEnquiries) {
            $countEnquiry = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
        }
        //echo $countEnquiry;
        //exit;
        if ($countEnquiry < $allowedEnquiryCount) {
            $allowBooking = 1;
        }
        if ($allowBooking == 0) {
            return redirect()->back()->with("error", "your limit is exceeded");
        } else {
            $productId = $rentalObj->prd_id;
            $propertyObj = \App\Property::where("id", $productId)->first();

            $renterId = $propertyObj->user->id;
            $now = strtotime($checkIn); // or your date as well
            $your_date = strtotime($checkOut);
            $datediff = $your_date - $now;

            $noOfNyts = floor($datediff / (60 * 60 * 24));
            $msg = "";
            $worktutoremail = $propertyObj->represent_email;
            $organizationemail = $propertyObj->user->orgemail;
            $bookingno = "";
            $hr_email = $propertyObj->hr_represent_email;
            $check_hr = $propertyObj->human_resources;
            if (($check_hr == 0) && !empty($hr_email)) {
                //echo "yes inside hr"; exit;
                $hr_details = \App\User::where('email', $hr_email)->first();
                if (isset($hr_details->id) && !empty($hr_details->id)) {
                    $hr_dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => $userId,
                        'renter_id' => $renterId,
                        'is_hr' => '1',
                        'prd_id' => $productId,
                        'is_admin_booked' => "1"
                    );


                    $hr_booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $hr_dataArr = array_merge($hr_dataArr, $hr_booking_status);
                    $hr_enquiryObj = \App\RentalsEnquiry::create($hr_dataArr);
                    $hr_insertid = $hr_enquiryObj->id;

                    if ($hr_enquiryObj->Bookingno == '' || $hr_enquiryObj->Bookingno == NULL) {
                        $val = 10 * $hr_insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $hr_newdata = array(
                            'Bookingno' => $bookingno
                        );
                        \App\RentalsEnquiry::where('id', $hr_insertid)->update($hr_newdata);
                    }
                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $hr_insertid,
                        'senderId' => $userId,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    \App\MedMessage::create($dataArr);
                    //$this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $hr_enquiryObj->prd_id, \Auth::user()->id, $hr_insertid);
                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    if ($availableCheck == 0) {
                        $this->mailNotificationsForUser->adminsendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                    } else {
                        $this->mailNotificationsForUser->adminsendBookingNotification($propertyObj, $hr_enquiryObj);
                    }
                } else {
                    //echo "yes inside else case"; exit;
                    $dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => $userId,
                        'renter_id' => $renterId,
                        'prd_id' => $productId,
                        'is_admin_booked' => "1"
                    );


                    $booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $dataArr = array_merge($dataArr, $booking_status);
                    $enquiryObj = \App\RentalsEnquiry::create($dataArr);
                    $insertid = $enquiryObj->id;

                    if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                        $val = 10 * $insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $newdata = array(
                            'Bookingno' => $bookingno
                        );
                        \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
                    }

                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $insertid,
                        'senderId' => $userId,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    \App\MedMessage::create($dataArr);
                    //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                    //checking if user have its first choice within selected dates or not
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    //echo $availableCheck;
                    if ($availableCheck == 0) {
                        //echo "yes its first choice and send email<br />"; exit;
                        if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                            $this->mailNotificationsForUser->adminsendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                        } else {
                            if (isset($enquiryObj->renter_id)) {
                                $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();
                                if (isset($renter_user->email) && !empty($renter_user->email)) {
                                    $this->mailNotificationsForUser->adminsendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                                }
                            }
                        }
                    } else {
                        //echo "sending email to esp as notification for sencond or 3rd choice<br />";
                        $this->mailNotificationsForUser->adminsendBookingNotification($propertyObj, $enquiryObj);
                    }
                }
            } else {
                //echo "yes inside else case"; exit;
                $dataArr = array(
                    'checkin' => $checkIn,
                    'checkout' => $checkOut,
                    'numofdates' => $noOfNyts,
                    'caltophone' => '',
                    'enquiry_timezone' => '',
                    'user_id' => $userId,
                    'renter_id' => $renterId,
                    'prd_id' => $productId,
                    'is_admin_booked' => "1"
                );


                $booking_status = array(
                    'booking_status' => 'Pending'
                );

                $dataArr = array_merge($dataArr, $booking_status);
                $enquiryObj = \App\RentalsEnquiry::create($dataArr);
                $insertid = $enquiryObj->id;

                if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                    $val = 10 * $insertid + 8;
                    $val = 1500000 + $val;
                    $bookingno = "WRL" . $val;
                    $newdata = array(
                        'Bookingno' => $bookingno
                    );
                    \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
                }

                $dataArr = array(
                    'productId' => $productId,
                    'bookingNo' => $bookingno,
                    'enqid' => $insertid,
                    'senderId' => $userId,
                    'receiverId' => $renterId,
                    'subject' => 'Booking Request : ' . $bookingno,
                    'message' => ""
                );
                \App\MedMessage::create($dataArr);
                //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                //checking if user have its first choice within selected dates or not
                //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
                $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                //echo $availableCheck;
                if ($availableCheck == 0) {
                    //echo "yes its first choice and send email<br />"; exit;
                    if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                        $this->mailNotificationsForUser->adminsendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                    } else {
                        if (isset($enquiryObj->renter_id)) {
                            $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();
                            if (isset($renter_user->email) && !empty($renter_user->email)) {
                                $this->mailNotificationsForUser->adminsendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                            }
                        }
                    }
                } else {
                    //echo "sending email to esp as notification for sencond or 3rd choice<br />";
                    $this->mailNotificationsForUser->adminsendBookingNotification($propertyObj, $enquiryObj);
                }
            }
            return "Your Booking has been added successfully!";
            //return redirect()->back()->with("message", "Your Booking has been added successfully!");
        }
    }

    public function storePaidPendingBooking($request, $rentalObj) {
        $userId = $rentalObj->user_id;
        $userEnquiries = \App\RentalsEnquiry::where('user_id', $userId)->get();
        $checkIn = date('Y-m-d H:i:s', strtotime($request->get('startdate')));
        $checkOut = date('Y-m-d H:i:s', strtotime($request->get('enddate')));
        $allowBooking = 0;
        $countEnquiry = 0;
        $settingsObj = \App\PropertySettings::propertySettings();
        $allowedEnquiryCount = $settingsObj->booking_request;
        $allowedEnquiryCount = $allowedEnquiryCount + 20;
        //echo $allowedEnquiryCount."<br />";
        if ($userEnquiries) {
            $countEnquiry = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
        }
        //echo $countEnquiry;
        //exit;
        if ($countEnquiry < $allowedEnquiryCount) {
            $allowBooking = 1;
        }
        if ($allowBooking == 0) {
            return redirect()->back()->with("error", "your limit is exceeded");
        } else {
            $productId = $rentalObj->prd_id;
            $propertyObj = \App\Property::where("id", $productId)->first();

            $renterId = $propertyObj->user->id;
            $now = strtotime($checkIn); // or your date as well
            $your_date = strtotime($checkOut);
            $datediff = $your_date - $now;

            $noOfNyts = floor($datediff / (60 * 60 * 24));
            $msg = "";
            $worktutoremail = $propertyObj->represent_email;
            $organizationemail = $propertyObj->user->orgemail;
            $bookingno = "";
            $hr_email = $propertyObj->hr_represent_email;
            $check_hr = $propertyObj->human_resources;
            if (($check_hr == 0) && !empty($hr_email)) {
                //echo "yes inside hr"; exit;
                $hr_details = \App\User::where('email', $hr_email)->first();
                if (isset($hr_details->id) && !empty($hr_details->id)) {
                    $hr_dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => $userId,
                        'renter_id' => $renterId,
                        'is_hr' => '1',
                        'prd_id' => $productId,
                        'is_admin_booked' => "1"
                    );


                    $hr_booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $hr_dataArr = array_merge($hr_dataArr, $hr_booking_status);
                    $hr_enquiryObj = \App\RentalsEnquiry::create($hr_dataArr);
                    $hr_insertid = $hr_enquiryObj->id;

                    if ($hr_enquiryObj->Bookingno == '' || $hr_enquiryObj->Bookingno == NULL) {
                        $val = 10 * $hr_insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $hr_newdata = array(
                            'Bookingno' => $bookingno
                        );
                        \App\RentalsEnquiry::where('id', $hr_insertid)->update($hr_newdata);
                    }
                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $hr_insertid,
                        'senderId' => $userId,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    \App\MedMessage::create($dataArr);
                    //$this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $hr_enquiryObj->prd_id, \Auth::user()->id, $hr_insertid);
                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    if ($availableCheck == 0) {
                        $this->mailNotificationsForUser->adminPaidsendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                    } else {
                        $userData =  \App\User::where('id',$userId)->first();
                        $userEmail = $userData->email;
                        $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $hr_enquiryObj,$userEmail);
                    }
                } else {
                    //echo "yes inside else case"; exit;
                    $dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => $userId,
                        'renter_id' => $renterId,
                        'prd_id' => $productId,
                        'is_admin_booked' => "1"
                    );


                    $booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $dataArr = array_merge($dataArr, $booking_status);
                    $enquiryObj = \App\RentalsEnquiry::create($dataArr);
                    $insertid = $enquiryObj->id;

                    if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                        $val = 10 * $insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $newdata = array(
                            'Bookingno' => $bookingno
                        );
                        \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
                    }

                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $insertid,
                        'senderId' => $userId,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    \App\MedMessage::create($dataArr);
                    //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                    //checking if user have its first choice within selected dates or not
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    if ($availableCheck == 0) {
                        //echo "yes its first choice and send email ";
                        if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                            //echo "work tutor email ";
                            $this->mailNotificationsForUser->adminPaidsendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                        } else {
                            //echo "else work tutor ";
                            if (isset($enquiryObj->renter_id)) {
                                //echo "renter id ".$enquiryObj->renter_id;
                                $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();
                                if (isset($renter_user->email) && !empty($renter_user->email)) {
                                    $this->mailNotificationsForUser->adminPaidsendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                                }
                            }
                        }
                    } else {
                        //echo "sending email to esp as notification for sencond or 3rd choice"; 
                        $userData =  \App\User::where('id',$userId)->first();
                        $userEmail = $userData->email;
                        $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj,$userEmail);
                    }
                    //exit;
                }
            } else {
                //echo "yes inside else case"; exit;
                $dataArr = array(
                    'checkin' => $checkIn,
                    'checkout' => $checkOut,
                    'numofdates' => $noOfNyts,
                    'caltophone' => '',
                    'enquiry_timezone' => '',
                    'user_id' => $userId,
                    'renter_id' => $renterId,
                    'prd_id' => $productId,
                    'is_admin_booked' => "1"
                );


                $booking_status = array(
                    'booking_status' => 'Pending'
                );

                $dataArr = array_merge($dataArr, $booking_status);
                $enquiryObj = \App\RentalsEnquiry::create($dataArr);
                $insertid = $enquiryObj->id;

                if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                    $val = 10 * $insertid + 8;
                    $val = 1500000 + $val;
                    $bookingno = "WRL" . $val;
                    $newdata = array(
                        'Bookingno' => $bookingno
                    );
                    \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
                }

                $dataArr = array(
                    'productId' => $productId,
                    'bookingNo' => $bookingno,
                    'enqid' => $insertid,
                    'senderId' => $userId,
                    'receiverId' => $renterId,
                    'subject' => 'Booking Request : ' . $bookingno,
                    'message' => ""
                );
                \App\MedMessage::create($dataArr);
                //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                //checking if user have its first choice within selected dates or not
                //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
                $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                if ($availableCheck == 0) {
                    //echo "yes its first choice and send email ";
                    if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                        //echo "work tutor email ";
                        $this->mailNotificationsForUser->adminPaidsendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                    } else {
                        //echo "else work tutor ";
                        if (isset($enquiryObj->renter_id)) {
                            //echo "renter id ".$enquiryObj->renter_id;
                            $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();
                            if (isset($renter_user->email) && !empty($renter_user->email)) {
                                $this->mailNotificationsForUser->adminPaidsendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                            }
                        }
                    }
                } else {
                    $userData =  \App\User::where('id',$userId)->first();
                    $userEmail = $userData->email;
                    //echo "sending email to esp as notification for sencond or 3rd choice"; 
                    $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj,$userEmail);
                }
                //exit;
            }
            return "Your Booking has been added successfully!";
            //return redirect()->back()->with("message", "Your Booking has been added successfully!");
        }
    }

    public function storePaidAcceptBooking($request, $rentalObj) {
        $userId = $rentalObj->user_id;
        $userEnquiries = \App\RentalsEnquiry::where('user_id', $userId)->get();
        $checkIn = date('Y-m-d H:i:s', strtotime($request->get('startdate')));
        $checkOut = date('Y-m-d H:i:s', strtotime($request->get('enddate')));
        $allowBooking = 0;
        $countEnquiry = 0;
        $settingsObj = \App\PropertySettings::propertySettings();
        $allowedEnquiryCount = $settingsObj->booking_request;
        $allowedEnquiryCount = $allowedEnquiryCount + 1;
        $enquiryid = 0;
        //echo $allowedEnquiryCount."<br />";
        if ($userEnquiries) {
            $countEnquiry = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
        }
        //echo $countEnquiry;
        //exit;
        if ($countEnquiry < $allowedEnquiryCount) {
            $allowBooking = 1;
        }

        $productId = $rentalObj->prd_id;
        $propertyObj = \App\Property::where("id", $productId)->first();
        $renterId = $propertyObj->user->id;
        $now = strtotime($checkIn); // or your date as well
        $your_date = strtotime($checkOut);
        $datediff = $your_date - $now;

        $noOfNyts = floor($datediff / (60 * 60 * 24));
        $msg = "";
        $worktutoremail = $propertyObj->represent_email;
        $organizationemail = $propertyObj->user->orgemail;
        $bookingno = "";
        $hr_email = $propertyObj->hr_represent_email;

        // job booking process starts from here
        if (!empty($hr_email)) {
            //$this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj);
            $hr_details = \App\User::where('email', $hr_email)->first();
            if (isset($hr_details->id) && !empty($hr_details->id)) {
                $hr_dataArr = array(
                    'checkin' => $checkIn,
                    'checkout' => $checkOut,
                    'numofdates' => $noOfNyts,
                    'caltophone' => '',
                    'enquiry_timezone' => '',
                    'user_id' => $userId,
                    'renter_id' => $renterId,
                    'is_hr' => '1',
                    'prd_id' => $productId,
                    'is_admin_booked' => "1"
                );


                $hr_booking_status = array(
                    'booking_status' => 'Pending'
                );

                $hr_dataArr = array_merge($hr_dataArr, $hr_booking_status);
                $hr_enquiryObj = \App\RentalsEnquiry::create($hr_dataArr);
                $hr_insertid = $hr_enquiryObj->id;
                $enquiryid = $hr_enquiryObj->id;

                if ($hr_enquiryObj->Bookingno == '' || $hr_enquiryObj->Bookingno == NULL) {
                    $val = 10 * $hr_insertid + 8;
                    $val = 1500000 + $val;
                    $bookingno = "WRL" . $val;
                    $hr_newdata = array(
                        'Bookingno' => $bookingno
                    );
                    \App\RentalsEnquiry::where('id', $hr_insertid)->update($hr_newdata);
                }
                $dataArr = array(
                    'productId' => $productId,
                    'bookingNo' => $bookingno,
                    'enqid' => $hr_insertid,
                    'senderId' => $userId,
                    'receiverId' => $hr_details->id,
                    'subject' => 'Booking Request : ' . $bookingno,
                    'message' => ""
                );
                \App\MedMessage::create($dataArr);
            } else {
                //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj);
                $dataArr = array(
                    'checkin' => $checkIn,
                    'checkout' => $checkOut,
                    'numofdates' => $noOfNyts,
                    'caltophone' => '',
                    'enquiry_timezone' => '',
                    'user_id' => $userId,
                    'renter_id' => $renterId,
                    'prd_id' => $productId,
                    'is_admin_booked' => "1"
                );


                $booking_status = array(
                    'booking_status' => 'Pending'
                );

                $dataArr = array_merge($dataArr, $booking_status);
                $enquiryObj = \App\RentalsEnquiry::create($dataArr);
                $insertid = $enquiryObj->id;
                $enquiryid = $enquiryObj->id;

                if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                    $val = 10 * $insertid + 8;
                    $val = 1500000 + $val;
                    $bookingno = "WRL" . $val;
                    $newdata = array(
                        'Bookingno' => $bookingno
                    );
                    \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
                }

                $dataArr = array(
                    'productId' => $productId,
                    'bookingNo' => $bookingno,
                    'enqid' => $insertid,
                    'senderId' => $userId,
                    'receiverId' => $renterId,
                    'subject' => 'Booking Request : ' . $bookingno,
                    'message' => ""
                );
                \App\MedMessage::create($dataArr);
            }
        } else {
            //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj);
            $dataArr = array(
                'checkin' => $checkIn,
                'checkout' => $checkOut,
                'numofdates' => $noOfNyts,
                'caltophone' => '',
                'enquiry_timezone' => '',
                'user_id' => $userId,
                'renter_id' => $renterId,
                'prd_id' => $productId,
                'is_admin_booked' => "1"
            );


            $booking_status = array(
                'booking_status' => 'Pending'
            );

            $dataArr = array_merge($dataArr, $booking_status);
            $enquiryObj = \App\RentalsEnquiry::create($dataArr);
            $insertid = $enquiryObj->id;
            $enquiryid = $enquiryObj->id;

            if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                $val = 10 * $insertid + 8;
                $val = 1500000 + $val;
                $bookingno = "WRL" . $val;
                $newdata = array(
                    'Bookingno' => $bookingno
                );
                \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
            }

            $dataArr = array(
                'productId' => $productId,
                'bookingNo' => $bookingno,
                'enqid' => $insertid,
                'senderId' => $userId,
                'receiverId' => $renterId,
                'subject' => 'Booking Request : ' . $bookingno,
                'message' => ""
            );
            \App\MedMessage::create($dataArr);
        }

        // accepting the booking while adding
        if (isset($enquiryid) && $enquiryid > 0) {
            $status = "Accept";
            $rentalObj = \App\RentalsEnquiry::find($enquiryid);
            //checking the same day booking count
            $newDateStart = date("Y-m-d", strtotime($rentalObj->checkin));
            $newDateEnd = date("Y-m-d", strtotime($rentalObj->checkout));
            $availableCheck = \App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $rentalObj->prd_id);
            $same_day_count = count($availableCheck);

            \App\RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
            $propertyId = $rentalObj->prd_id;
            $arrival = $rentalObj->checkin;
            $depature = $rentalObj->checkout;
            //get number of days between checkin and checkout dates
            $dates = $this->getDatesFromRange($arrival, $depature);
            //save booked dates using job id
            $this->saveBookedDates($dates, $propertyId);
            // update scheduler for booked job
            $this->updateScheduleCalendar($propertyId, 0);
            // adding new internal messages
            $dataArr = array(
                'productId' => $rentalObj->prd_id,
                'bookingNo' => $rentalObj->Bookingno,
                'enqid' => $enquiryid,
                'senderId' => $userId,
                'receiverId' => $rentalObj->user_id,
                'subject' => 'Booking Request : ' . $status,
                'message' => "You Request Approved"
            );
            \App\MedMessage::create($dataArr);

            /* Send Learning Agreement */
            $cmsObj = $this->cmsRepository->getById(9);
            $stipulate = $this->cmsRepository->getById(12);
            $article1 = $this->cmsRepository->getById(13);
            $article2 = $this->cmsRepository->getById(14);
            $article3 = $this->cmsRepository->getById(15);
            $article4 = $this->cmsRepository->getById(16);
            $article5 = $this->cmsRepository->getById(17);
            $article6 = $this->cmsRepository->getById(18);
            $article7 = $this->cmsRepository->getById(19);
            $educationalobjectives = $this->cmsRepository->getById(20);
            $article9 = $this->cmsRepository->getById(21);
            $article10 = $this->cmsRepository->getById(22);

            $rentalObj = \App\RentalsEnquiry::find($enquiryid);

            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $file_name = $timestamp . '-' . $rentalObj->Bookingno . ".pdf";
            if ($propertyObj->property_status != "paid") {
                $pdf = PDF::loadView('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'))->save(public_path() . "/pdf/agreement/" . $file_name);
            }
//return $file_name ;
            //$tutorEmail = $rentalObj->user->school->tutor_email;
            $tutorEmail = "";
            if (isset($rentalObj->property->work_org_same) && $rentalObj->property->work_org_same == "0") {
                //echo "yes here if the work tutor is NOT you <br />";
                if (isset($rentalObj->prd_id) && !empty($rentalObj->prd_id)) {
                    $tutor_detail = \App\Property::find($rentalObj->prd_id);
                    if (isset($tutor_detail->represent_email) && !empty($tutor_detail->represent_email)) {
                        $tutorEmail = $tutor_detail->represent_email;
                    }
                }
            } else {
                //echo "yes here if the work tutor is you <br />";
                $tutorEmail = isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : "";
            }


            $parentEmail = null;
            if (is_null($rentalObj->user->student_relationship)) {
                $studentEmail = $rentalObj->user->email;
            } else {
                $parentEmail = $rentalObj->user->email;
                $studentEmail = $rentalObj->user->semail16;
            }

            // get HR details
            $hr_enquiry = RentalsEnquiry::where(["id" => $enquiryid, 'is_hr' => '1'])->first();
            $hr_email = "";
            $hr_email_detail = "";
            if (isset($rentalObj->property->human_resources) && $rentalObj->property->human_resources == "0") {
                $hr_email = isset($rentalObj->property->hr_represent_email) ? $rentalObj->property->hr_represent_email : "";
                if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
                    $hr_user = User::find($hr_enquiry->renter_id);
                    if (isset($hr->prd_id) && !empty($hr->prd_id)) {
                        $hr_email_detail = \App\Property::find($hr->prd_id);
                    }
                }
            } else {
                $hr_email = isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : "";
            }
//            $hr_email_detail = ""; human_resources
//            if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
//                $hr_user = User::find($hr_enquiry->renter_id);
//                $hr_email = isset($hr_user->email) ? $hr_user->email : '';
//                if (isset($hr->prd_id) && !empty($hr->prd_id)) {
//                    $hr_email_detail = Property::find($hr->prd_id);
//                }
//            }
//            echo "work tutor email = " . $tutorEmail . "<br />";
//            echo "HR email = " . $hr_email . "<br />";
//            echo "Parent email = " . $parentEmail . "<br />";
//            echo "Student email = " . $studentEmail . "<br />";
//            echo "is HR = " . $rentalObj->is_hr . "<br />";
//            exit;
            //echo $propertyObj->property_status;
            //exit;
            if ($propertyObj->property_status == "paid") {
                $this->mailNotificationsForUser->sendemailPaidToStudentAboutSchoolHouse($parentEmail, $studentEmail, $rentalObj);
                $school_house_email = isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : "";
                //echo $school_house_email; exit;
                $this->mailNotificationsForUser->sendemailPaidToSchoolHouseAboutStudent($school_house_email, $rentalObj, $file_name);
            } else {
                $this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $file_name, $rentalObj, $hr_email, $hr_email_detail);
            }
            // if there is an HR for booking send email and send internal message
            if ($rentalObj->is_hr == "1") {
                $propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
                $worktutoremail = $propertyObj->represent_email;
                if (!empty($worktutoremail)) {
                    //$worktutoremail = $rentalObj->user->email;
                    $organizationemail = $propertyObj->user->orgemail;
                    //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj);
                }
                //echo $worktutoremail; exit;
                //exit;
                $checkIn = $rentalObj->checkin;
                $checkOut = $rentalObj->checkout;
                $noOfNyts = $rentalObj->numofdates;
                $renterId = $propertyObj->user->id;
                $dataArr = array(
                    'checkin' => $checkIn,
                    'checkout' => $checkOut,
                    'numofdates' => $noOfNyts,
                    'caltophone' => '',
                    'enquiry_timezone' => '',
                    'user_id' => $rentalObj->user_id,
                    'renter_id' => $renterId,
                    'prd_id' => $rentalObj->prd_id
                );


                $booking_status = array(
                    'booking_status' => 'Pending'
                );

                $dataArr = array_merge($dataArr, $booking_status);
                $enquiryObj = RentalsEnquiry::create($dataArr);
                $insertid = $enquiryObj->id;

                if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                    $val = 10 * $insertid + 8;
                    $val = 1500000 + $val;
                    $bookingno = "WRL" . $val;
                    $newdata = array(
                        'Bookingno' => $bookingno
                    );
                    RentalsEnquiry::where('id', $insertid)->update($newdata);
                }

                $dataArr = array(
                    'productId' => $rentalObj->prd_id,
                    'bookingNo' => $bookingno,
                    'enqid' => $insertid,
                    'senderId' => $rentalObj->user_id,
                    'receiverId' => $renterId,
                    'subject' => 'Booking Request : ' . $bookingno,
                    'message' => ""
                );
                MedMessage::create($dataArr);
            }
            return "Your Booking has been added and Accepted successfully!";
            //return redirect()->back()->with("message", "Your Booking has been added and Accepted successfully!");
        } else {
            return "There has been some error while adding booking!";
            //return redirect()->back()->with("error", "There has been some error while adding booking!");
        }
    }

    public function getEnquiryDates($enquiresObj, $checkIn, $checkOut) {
        $enquiryDates = array();
        $count = 0;
        foreach ($enquiresObj as $enq) {
            $dates = $this->getDatesFromRange($enq->checkin, $enq->checkout);
            $i = 1;
            $dateMinus1 = count($dates);
            foreach ($dates as $date) {
                if ($i <= $dateMinus1) {
                    array_push($enquiryDates, $date);
                }
                $i++;
            }
            if (in_array($checkIn, $enquiryDates) || in_array($checkOut, $enquiryDates)) {
                $count++;
            }
        }
        return $count;
    }

    public function getDatesFromRange($start, $end) {
        $dates = array($start);
        while (end($dates) < $end) {
            $dates[] = date('Y-m-d', strtotime(end($dates) . ' +1 day'));
        }
        return $dates;
    }

    public function saveBookedDates($dates, $propertyId) {
        $i = 1;
        $dateMinus1 = count($dates);
        foreach ($dates as $date) {
            if ($i <= $dateMinus1) {
                $bookingObj = \App\Booking::where('PropId', $propertyId)->where('id_state', 1)->where('id_item', 1)->where('the_date', $date)->get();
                if ($bookingObj->count() > 0) {
                    
                } else {
                    $dataArr = array('PropId' => $propertyId,
                        'id_state' => 1,
                        'id_item' => 1,
                        'the_date' => $date
                    );
                    \App\Booking::create($dataArr);
                }
            }
            $i++;
        }
    }

    public function updateScheduleCalendar($propertyId, $price) {
        $DateArr = \App\Booking::where('PropId', $propertyId)->get();
        $dateDispalyRowCount = 0;
        $dateArrVAl = "";
        if ($DateArr->count() > 0) {
            $dateArrVAl .= '{';
            foreach ($DateArr as $dateDispalyRow) {
                if ($dateDispalyRowCount == 0) {
                    $dateArrVAl .= '"' . $dateDispalyRow->the_date . '":{"available":"1","bind":0,"info":"","notes":"","price":"' . $price . '","promo":"","status":"booked"}';
                } else {
                    $dateArrVAl .= ',"' . $dateDispalyRow->the_date . '":{"available":"1","bind":0,"info":"","notes":"","price":"' . $price . '","promo":"","status":"booked"}';
                }
                $dateDispalyRowCount = $dateDispalyRowCount + 1;
            }
            $dateArrVAl .= '}';
            $scheduleObj = \App\Schedule::where('id', $propertyId)->exists();
            $inputArr4 = array('id' => $propertyId, 'data' => trim($dateArrVAl));
            if ($scheduleObj) {
                \App\Schedule::where('id', $propertyId)->update($inputArr4);
            } else {
                \App\Schedule::create($inputArr4);
            }
        }
    }

}
