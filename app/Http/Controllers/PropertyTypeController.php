<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PropertytypeRepository;

class PropertyTypeController extends Controller
{
    private $bredCrum = "Property_Type";
    
    protected $propertyTypeRepository;

    public function __construct(PropertytypeRepository $propertyTypeRepository)
    {
        $this->propertyTypeRepository = $propertyTypeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->propertyTypeRepository->getAll();
        return View('admin.propertytype.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        return View('admin.propertytype.create', compact('bredCrum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('name' => $request->get('name'),
                        'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->propertyTypeRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/propertytype/create')->withErrors($validator);
        } else {
            $propertyTypeObj = $this->propertyTypeRepository->create($postData);
            flash('Property Type Added Successfully', 'success');
            return \Redirect('admin/propertytype');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->propertyTypeRepository->getById($id);
        return View('admin.propertytype.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = array('name' => $request->get('name'),
                        'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->propertyTypeRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/propertytype/'.$id.'/edit')->withErrors($validator);
        } else {
            $propertyTypeObj = $this->propertyTypeRepository->update($postData, $id);   
            flash('Property Type Updated Successfully', 'success');
            return \Redirect('admin/propertytype');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
