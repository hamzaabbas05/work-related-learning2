<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Constants\AppConstants;
use App\Repositories\BannerRepository;
use App\Repositories\HomeDestinationRepository;
use App\Services\Mailer\MailNotificationsForUser;
use App\Property;
use Auth;
use DistanceMatrix;
use CiroVargas\GoogleDistanceMatrix\GoogleDistanceMatrix;
use Illuminate\Console\Scheduling\Schedule;

class HomeController extends Controller {

    protected $bannerRepository;
    protected $homeDestinationRepository;
    private $mailNotificationsForUser;

    public function __construct(BannerRepository $bannerRepository, HomeDestinationRepository $homeDestinationRepository, MailNotificationsForUser $mailNotificationsForUser) {
        $this->bannerRepository = $bannerRepository;
        $this->homeDestinationRepository = $homeDestinationRepository;
        $this->mailNotificationsForUser = $mailNotificationsForUser;
        // var_dump(\App\Artisan::call("config:cache"));
    }

    public function index() {
       
        $homeBanner = $this->bannerRepository->getByType(AppConstants::HOME_BANNER);
        $homeSlider = $this->bannerRepository->getByType(AppConstants::HOME_SLIDER);
        $homeCenterBanner = $this->bannerRepository->getByType(AppConstants::HOME_CENTER_SLIDER);
        $homeDestination = $this->homeDestinationRepository->getAll(true);
        $languages = \App\Language::where("status", "1")->get();
        $propobj = Property::where('premium', 1)->get();
        return View('index', compact('homeBanner', 'homeSlider', 'homeCenterBanner', 'homeDestination', 'propobj','languages'));
    }

    public function sendTestMail() {
        $this->mailNotificationsForUser->testMail();
    }

    public function signIn(Request $request) {
		
        $previous_url = \URL::previous();
		
		
		if(Auth::check()){
			if($previous_url == 'http://work-related-learning.com/signin'){
				return \Redirect('/');
			}else{
				return \Redirect($previous_url);
			}
			
		} 
		
        return View('login', compact('previous_url'));
    }

    public function signUp() {
        $languages = \App\Language::where("status", "1")->get();

        return View('register',compact('languages'));
    }

    public function contactus() {
        
    }

// this function is used for all confirmation emails
    public function jobEmailConfirm($code) {
        if (!$code) {
            flash('Invalid Confirmation Code', 'success');
            return \Redirect('home');
        } else {
            $check_token = \App\EmailSent::where("token", $code)->first();
            if (isset($check_token->id)) {
                $check_token->is_confirm = "1";
                $check_token->token = "";
                $check_token->save();
                flash('Your Confirmation has been received', 'success'); //Student confirming receipt of email with Work placement,  paid service and all

                return \Redirect('home');
            } else {
                flash('Invalid Confirmation Code', 'success');
                return \Redirect('home');
            }
        }
    }

    public function testTime() {

        $filePath = dirname(dirname(dirname(dirname(__FILE__))))."/chron_log_file_".date('YmdH_i_s').".txt";
        echo "FILE PATH:".$filePath;die;
        // exit;
        echo "Date :".date("Y-m-d H:i:s");  
        exit;
        $datetime = date("Y-m-d H:i:s");
        $check_time = (int) date("H");
        echo $datetime . "<br />";
        echo $check_time . "<br />";
        if ($check_time > 9 && (int) date("i") > 0) { //Is this your code?
            $datetime = date("Y-m-d 09:00:00", strtotime("+1 day"));
        } else {
            $datetime = date("Y-m-d 09:00:00");
        }
        echo $datetime . "<br />";

        // var_dump(exec('/usr/bin/php /home2/workrel1/public_html/artisan command:addPropertyAddress >> /dev/null 2>&1'));
        echo "Cron Set:";
        var_dump(exec('/home2/workrel1/public_html/artisan php artisan list'));
exit;
        $fp = fopen($filePath,"a");
        $write  = "\n =========================== Start Here on ".date("Y-m-d H:i:s")." =========================== \n";
    $write .= "\n \n Reqest Url : \n \n";
    // $write .= "\n\n address = $address , time = $time added \n\n";
    $write .= "\n =========================== End Here on ".date("Y-m-d H:i:s")." =========================== \n";

fwrite($fp, $write);
fclose($fp);

exit;
        $arrival_time = strtotime($datetime);
        //echo $arrival_time; exit;
        // $google_map_api = "AIzaSyAz0HqeAgq5xNt9DSUSMPIVlmLK_uuAoDc";
        $google_map_api = "AIzaSyA0BQbSzrX8W4conO6nmF9-LzpH4R-y1jU";
        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );
//        $detail_url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=$google_map_api&origins=$add1&destinations=$add2&mode=transit&arrival_time=$arrival_time&sensor=false";
//        echo $detail_url . "<br />";
//        $json = file_get_contents($detail_url, false, stream_context_create($arrContextOptions));
//        $details = json_decode($json, TRUE);
//        echo "<pre>";
//        print_r($details);
//        echo "</pre>";
//        exit;




        $all_paid_property = \App\Property::where("property_status", "paid")
                ->where("property.property_type", "0")
//                ->join("users", "users.id", "=", "property.user_id")
//                ->leftjoin("user_paid_services", "property.user_id", "=", "user_paid_services.user_id")
                ->offset(1)
                ->limit(2)// 100 for test_
                ->orderBy("property.id", "ASC")
                ->get();
        //$google_key = "AIzaSyC646z-W9bGxhDtgjILxonsK2KI5TU-z68";
//        return $all_paid_property;
        if (sizeof($all_paid_property) > 0) {
            foreach ($all_paid_property as $paid_property) {
                $propId = $paid_property->id;
                if ($paid_property->addr_work_org_same == 0) {
                    $address = $paid_property->workplace_address; // workplace_address is our property, listing, address. Case paid, if checkbox checked, then use this address:workplace_address
                }
                // this is where from frontend we store school house legal address, 
                // check if school house property address is not same
                else if (isset($paid_property->user->paid_service->house_property_differ) && !empty($paid_property->user->paid_service->propertyaddress) && $paid_property->user->paid_service->house_property_differ == "1") {
                    $address = isset($paid_property->user->paid_service->propertyaddress) ? $paid_property->user->paid_service->propertyaddress : '';
                }
                // to get the houseaddress from user_paid_services table for normal address, not from property table
                else if (isset($paid_property->user->paid_service->house_property_differ) && !empty($paid_property->user->paid_service->houseaddress) && $paid_property->user->paid_service->house_property_differ == "0") {
                    $address = isset($paid_property->user->paid_service->houseaddress) ? $paid_property->user->paid_service->houseaddress : '';
                } else if (isset($paid_property->user->paid_service->house_property_differ) && !empty($paid_property->user->paid_service->houseaddress) && $paid_property->user->paid_service->house_property_differ == "0") {
                    $address = isset($paid_property->user->paid_service->houseaddress) ? $paid_property->user->paid_service->houseaddress : '';
                }
                // else get the user address 
                else {
                    $address = isset($paid_property->user->raddress18) ? $paid_property->user->raddress18 : "";
                }

                $address = \DB::getPdo()->quote($address);
//                echo "property id is = " . $paid_property->id . " and address is = " . $address . "<br />";
//                continue;
                // to get unpaid details
                // to get paid details
                if (!empty($address) && $address != "") {
                    $all_unpaid_property = \App\Property::whereRaw("property_status ='unpaid' or property_status ='unpaidflash'")
                            ->offset(1)
                            ->limit(2)// 1000 daily limit, lower values for tests
                            ->orderBy("id", "ASC")
                            ->get();
//                    return $all_unpaid_property;
//                    exit;
                    if (sizeof($all_unpaid_property) > 0) {
                        foreach ($all_unpaid_property as $unpaid_property) {
                            $unpaidPropId = $unpaid_property->id;
                            $unpaidAddress = isset($unpaid_property->user->raddress18) ? $unpaid_property->user->raddress18 : ''; //this is where we store address for unpaid 
                            if ($unpaid_property->addr_work_org_same == 0) {
                                $unpaidAddress = $unpaid_property->workplace_address; // case unpaid, if checkbox checked, then use this address:workplace_address
                            }
                            /*                             * ****************** */
                            $time = 0;
                            $add1 = $address;
                            $add1 = str_replace(' ', '+', $add1);
                            $add1 = str_replace('#', '', $add1);
                            $add2 = $unpaidAddress;
                            $add2 = str_replace(' ', '+', $add2);
                            $add2 = str_replace('#', '', $add2);
                            if ($add1 != '' && $add2 != '') {
                                //$add1 = '';
                                $detail_url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=$google_map_api&origins=$add1&destinations=$add2&mode=transit&arrival_time=$arrival_time&sensor=false";
                                echo $detail_url . "<br />";
                                $json = file_get_contents($detail_url, false, stream_context_create($arrContextOptions));
                                $details = json_decode($json, TRUE);

                                //echo $details;die; we had tried with different mode> transport, I don-t know why back at driving mode let me check it
                                //there is no api key in this request, no as i have copied that from Jitendra file
                                // let me check how i can add arrival time.
                                //$json = file_get_contents($details);
                                //$details = json_decode($json, TRUE);
//                                return $details;
                                if ($details['status'] == 'OK') {
                                    if (isset($details['rows'][0]['elements'][0]['duration']['value'])) {
                                        $time = $details['rows'][0]['elements'][0]['duration']['value'];
                                        $time = floor($time / 60);
                                    }
                                } else {
                                    echo "no API Returns <br />";
                                }
                            }
                            $distance_how_work = \App\DistanceHomeWork::where(["paidPropId" => $propId, "unpaidPropId" => $unpaidPropId])->first();
                            if (!isset($distance_how_work->id)) {
                                $distance_how_work = new \App\DistanceHomeWork();
                            }
                            $distance_how_work->paidAddress = $address;
                            $distance_how_work->paidPropId = $propId;
                            $distance_how_work->unpaidAddress = $unpaidAddress;
                            $distance_how_work->unpaidPropId = $unpaidPropId;
                            $distance_how_work->time = $time;
                            $distance_how_work->save();
                            echo $distance_how_work->id . " : address = $address , time = $time added <br /><br />";
                        }
                    }
                }
            }
        }
    }

    public function addTimeDistance($offset) {
        $distance_rows = \App\DistanceHomeWork::where("time", "0")
                ->where("paidAddress", "!=", "''")
                ->offset($offset)
                ->limit(100)
                ->get();
//        return $distance_rows;
        if (sizeof($distance_rows) > 0) {
            foreach ($distance_rows as $row) {
                if (!empty($row->paidAddress) && $row->paidAddress != "''") {
                    $time = 0;
                    $add1 = $row->paidAddress;
                    $add1 = str_replace(' ', '+', $add1);
                    $add1 = str_replace('#', '', $add1);
                    $add2 = $row->unpaidAddress;
                    $add2 = str_replace(' ', '+', $add2);
                    $add2 = str_replace('#', '', $add2);
                    if ($add1 != '' && $add2 != '') {
                        //$add1 = '';
                        $details = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$add1&destinations=$add2&mode=driving&sensor=false";
                        //echo $details;die; we had tried with different mode> transport, I don-t know why back at driving mode let me check it
                        //there is no api key in this request, no as i have copied that from Jitendra file
                        // let me check how i can add arrival time.
                        $json = file_get_contents($details);
                        $details = json_decode($json, TRUE);
//                                return $details;
                        if ($details['status'] == 'OK') {
                            if (isset($details['rows'][0]['elements'][0]['duration']['value'])) {
                                $time = $details['rows'][0]['elements'][0]['duration']['value'];
                                $time = floor($time / 60);
                            }
                        } else {
                            echo "no API Returns <br />";
                        }
                    }
                    $distance_how_work = \App\DistanceHomeWork::where(["id" => $row->id])->first();

                    $distance_how_work->time = $time;
                    $distance_how_work->save();
                    echo $distance_how_work->id . " : address = $row->paidAddress , time = $time added <br /><br />";
                }
            }
        }
    }

}
