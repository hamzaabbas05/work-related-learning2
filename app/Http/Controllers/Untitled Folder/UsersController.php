<?php

class UsersController extends BaseController 
{
	public function getReviews(){
		 
			$ReviewDetails=Userproperty::get_productreview_aboutyou(Session::get('userid'));
			$uId=Session::get('userid');
		return View::make('profilereviews')->with('menu',5)->with('submenu',4)->with('ReviewDetails',$ReviewDetails)->with('uId',$uId);	 
	}
	public function getReview1(){
	 
	 
			$ReviewDetails=Userproperty::get_productreview_byyou(Session::get('userid'));
			$uId=Session::get('userid');
		
			return View::make('profilereviewsyou')->with('menu',5)->with('submenu',4)->with('ReviewDetails',$ReviewDetails)->with('uId',$uId);	 
	 	 
	}
	
	public function user_profile($id) {
	
		$userid = $id;
		
		$condition = array('id'=>$userid);
		$user_Details =Commonmodel::get_all_details('users',$condition);
		$ReviewDetails=Userproperty::get_productreview_aboutyou(Session::get('userid'));
		 
		$rentalDetail=Userproperty::get_dashboard_list ( $userid,1);
		
	 
	 
	   return View::make('showuserprofile')->with('user_Details',$user_Details)->with('ReviewDetails',$ReviewDetails)->with('rentalDetail',$rentalDetail);
		
	}
	public function getInvoice($bookid) {
	$id = $bookid;
	$Invoicetmp = Commonmodel::get_all_details('fc_rentalsenquiry',array('Bookingno'=>$id));
	
	
	 
	$eId = $Invoicetmp[0]->id;

    $transactionid = Commonmodel::get_all_details('fc_payment',array('EnquiryId'=>$eId));
	
	 
	$transid = $Invoicetmp[0]->Bookingno;
	
	$productvalue = Commonmodel::get_all_details('air_property',array('id'=>$Invoicetmp[0]->prd_id));
	
   
	$product_id = $Invoicetmp[0]->prd_id;
	$checkindate =date('d-M-Y',strtotime($Invoicetmp[0]->checkin));
    $checkoutdate =date('d-M-Y',strtotime($Invoicetmp[0]->checkout));
	
	$TotalAmt_temp = ($Invoicetmp[0]->totalAmt) - ($Invoicetmp[0]->serviceFee);
 $TotalwithoutService =$TotalAmt_temp ;
	 
     
     $service = Commonmodel::get_all_details('air_commission',array('id'=>1, 'status'=>1));
    // echo '<pre>'; print_r($service->row()->commission_percentage);
      	$servicefee = $Invoicetmp[0]->serviceFee;
      	$TotalAmt =  $Invoicetmp[0]->totalAmt  ;
		$gtotalAmt =  $productvalue[0]->price_night ; 

	 
	 
	 $Night = ($Invoicetmp[0]->numofdates == 1)?"Night":"Nights";
	 $Guest = ($Invoicetmp[0]->NoofGuest==1)?"Guest":"Guests";
	 //echo "<pre>"; print_r($productaddress->result()); die;
	 $houserule = ($productvalue[0]->house_rules!='')?$productvalue[0]->house_rules:'None';
	 
	// $couponcode ='[Coupon code Used]'.$transactionid->row()->couponCode;
	 
	
$messageview = '
<html>
<head>
  <title>HolidaySiles booking</title>
</head>
	<body style="margin:0px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#4BBEFF" data-bgcolor="body-bg-dark" data-module="1" class="ui-sortable-handle currentTable">  
	<tbody><tr>
	 <td>
	 <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="devicewidth" style="background-color:#ffffff;" data-bgcolor="light-gray-bg"> 
	 <tbody><tr>
	 <td height="30" bgcolor="#4BBEFF" >&nbsp;</td> 
	 </tr>  
	 <tr>  
	<td align="center">          
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">              
	 <tbody><tr style="padding: 10px 10px 0px 10px; float: left">          
			   
	 <td align="center" valign="top">
					<table width="650" border="0" cellpadding="5" cellspacing="1" >
							<tbody style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;">
							<tr>
							  
							  <th width="70" bgcolor="#4BBEFF"style="color:#fff; font-size:15px;">Receipt</th>
							  <th width="75" ></th>
							  <th width="75"></th>
							  <th width="75"></th>
							  <th align="right" width="75" style="color:#f3402e; text-align:right"><a onClick="window.print()" TARGET="_blank" style="cursor: pointer; cursor: hand;text-decoration:underline;">Print Page</a></th>
							</tr>
							
						 
							
				</tbody></table>
				</td>       
	 </tr>          
	 </tr>
	 <tr><td align="left" style="color:#4c4c4c;font-size:13px;font-family:Open Sans, Arial, Helvetica, sans-serif;margin:10px; padding: 10px">Booking No : '.$transid.'</td></tr>
	 <tr><td align="left" style="color:#4c4c4c;font-size:13px;font-family:Open Sans, Arial, Helvetica, sans-serif;margin:10px; padding: 10px">Property Name : '.$productvalue[0]->title.'</td></tr>
	 <tr><td align="left" style="color:#4c4c4c;font-size:13px;font-family:Open Sans, Arial, Helvetica, sans-serif;margin:10px; padding: 10px">Address : '.$productvalue[0]->streetaddress.'</td></tr>
	 <tr>
	 <td style="border-top:1px solid #808080" bgcolor="#fff">&nbsp;</td>       
	 </tr>        
	 <tr>         
	 <td>
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">              
	 <tbody><tr style="padding: 10px; float: left">          
			   
	 <td align="center" valign="top">
					<table width="650" border="0" cellpadding="5" cellspacing="1" >
							<tbody style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;" ><tr>
							  <th width="75" bgcolor="#EFEFEF">Check In</th>
							 <th width="5"></th>
							  <th width="75" bgcolor="#EFEFEF">Check Out</th>
							  <th width="75" ></th>
							  <th width="75" bgcolor="#EFEFEF">'.$Night.'</th>
							  <th width="75" bgcolor="#EFEFEF">'.$Guest.'</th>
							 
							</tr>
							<tr align="center">
								<td >'.$checkindate.'</td>
								<td ></td>
								<td >'.$checkoutdate.'</td>
								<td ></td>
								<td >'.$Invoicetmp[0]->numofdates.'</td>
								<td >'.$Invoicetmp[0]->NoofGuest.'</td>

							  </tr>
						
							
						 
							
				</tbody></table>
				</td>       
	 </tr>          
	 </tbody>
	 </table>  
	 </td>
	 </tr>      
	 
	<tr style="pointer-events:none;">
	 <td align="center" valign="top" style="color:#000; font-weight: 700; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;" data-size="body-text" data-min="10" data-max="25" data-color="footer-text">
<img id="map-image" border="0" alt="'.$productvalue[0]->streetaddress.'" src="https://maps.googleapis.com/maps/api/staticmap?center='.$productvalue[0]->streetaddress.'&zoom=13&size=600x300&maptype=roadmap&sensor=false&format=png&visual_refresh=true&markers=size:mid%7Ccolor:red%7C'. $productvalue[0]->streetaddress.'">	
	 </td> 
	</tr> 
		   
	 <tr>      
	 <td>&nbsp;</td>      
	 </tr>       
	      
	 <tr>   
	  <tr>         
 <td align="center" >          
 <table width="100%" border="0" cellspacing="1" cellpadding="0" align="center" style="padding:0px 10px;">              
 <tbody><tr>          
            
 <td align="left" width="300px" valign="top" style="color:#4f595b; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px; line-height:20px;" data-size="body-text" data-min="10" data-max="25" data-color="footer-text">
<h4 style="float: left; width:100%;">Cancellation Policy  -    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </h4>For More details of the cancellation policy, please refer <a href="'.url('').'/pages/cancellation-policy" target="_blank">cancellation policy</a>.
 <td>
 <td align="left" width="300px"valign="top" style="color:#4f595b; text-align:justify; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;line-height:20px;" data-size="body-text" data-min="10" data-max="25" data-color="footer-text"> 
 <h4 style="float: left; width:100%;">House Rules</h4>
 '.$houserule.'</td> 
 </tr>      


<tr>          
      
 <td align="left" width="300px" valign="top" style="color:#4f595b; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px; line-height:20px;" data-size="body-text" data-min="10" data-max="25" data-color="footer-text">
<h4 style="float: left; width:100%; margin: 10px 0px;">Billing</h4>
<table style="width:100%; font-size:13px;">
  <tr>
    <td style="border-bottom: 1px solid #bbb;">Booked for '.$Invoicetmp[0]->numofdates.'  &nbsp;'.$Night.'</td>
    <td style="border-bottom: 1px solid #bbb;"></td>		
    <td style="border-bottom: 1px solid #bbb; padding: 5px 0px;">'."€" .' '.$TotalwithoutService.'</td>
  </tr>
  <tr>
<td style="border-bottom: 1px solid #bbb;">Service Fee</td>
    <td style="border-bottom: 1px solid #bbb;"></td>		
    <td style="border-bottom: 1px solid #bbb; padding: 5px 0px;">'."€" .' '.$servicefee.'</td>
  </tr>
  <tr>
<td style="border-bottom: 1px solid #bbb;  padding: 10px 0px;">Total</td>
    <td style="border-bottom: 1px solid #bbb;padding: 10px 0px;"></td>		
    <td style="border-bottom: 1px solid #bbb;padding: 10px 0px;">'."€" .' '.$TotalAmt.'</td>
	
	
	
  </tr>
  
 
  
  
 
</table>

<td>
 </tr> 
 
 </tbody>
 </table>      
 </td>        
 </tr> 
	 </tr>
	        
	 <tr>      
	 <td>&nbsp;</td>     
	 </tr>       
	 <tr>    
	 <td align="center" valign="middle" style="color:#444444; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;"><a href="javascript:void(0);" style="color:#0094aa; text-decoration:none;" data-size="body-text" data-min="10" data-max="25" data-link-color="plain-url-color" data-link-size="plain-url-text">(Remember: Not responding to this booking will result in your listing being ranked lower.)</a></td>       
	 </tr>        
	 <tr>        
	 <td>&nbsp;</td>   
	 </tr>              
	 <tr>               
	 <td align="center" valign="middle" style="color:#444444; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px; padding:0 20px;" data-size="body-text" data-min="10" data-max="25" data-color="body-text">If you need help or have any questions, please visit <a href="mailto:holidaysiles@gmail.com"   style="color:#0094aa;" data-link-color="plain-url-color"> holidaysiles@gmail.com </a></td>     
	 </tr>       
	 <tr>       
	 <td height="50">&nbsp;</td>      
	 </tr>         
	 <tr>       
	 <td height="30" bgcolor="#fff">&nbsp;</td>     
	 </tr>      
	 <tr>         
	 <td align="center" bgcolor="#fff">          
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="padding:0px 10px;">              
	 <tbody>
	 
	 </tbody>
	 </table>      
	 </td>        
	 </tr>         
	 <tr>
	 <td height="30" bgcolor="#4BBEFF" >&nbsp;</td> 
	 </tr> 
	 </tbody></table> 
	 </td>      </tr>  
	 </tbody></table>
	 </body>	
</html>
';
 
	
	//$this->load->view ( 'site/user/invoice', $this->data );
	
	return View::make('invoice')->with('msgview',$messageview);
	
	}



	public function AddToWishList() {
		$Rental_id = $_POST[ 'pid' ];
		$notes =  $_POST['add-notes'];
		$user_id = Session::get('userid');
		$note_id =  $_POST[ 'nid' ];
		$wishlist_cat =  $_POST[ 'wishlist_cat'];
		
		// $wishlist_catStr=implode(',',$wishlist_cat);
		if ($Rental_id != '') {
			Userproperty::update_wishlist ($Rental_id, $wishlist_cat );
			
			if ($note_id != '') {
				Userproperty::update_notes ( array (
						'notes' => $notes 
				), array (
						'id' => $note_id 
				) );
			} else {
				Userproperty::update_notes ( array (
						'product_id' => $Rental_id,
						'user_id' => $user_id,
						'notes' => $notes 
				));
			}
			
		 Flash::success('Wish list added successfully');
		 
		 
		 $pr=DB::table('air_property')->where('id',$Rental_id)->get();
		 return Redirect::to("room_detail/".$Rental_id."/".$pr[0]->title );
		 
		}
		//echo '<script>window.history.go(-1);</script>';
	}
	
	 public  function rentalwishlistcategoryAdd() {
		$wishuser_id = Session::get('userid');
		$wishcatname = $_GET['list_name'];
		$val =1;
		$list_id ="";
		
		if($val=='0')
		{
		$whocansee = 'Everyone';
		}
		else {
			$whocansee = 'Only me';
		}
	//	$rental_id = $this->input->post ( 'rental_id' );
		if($list_id!='')
		{
			
			$this->data ['WishListCat'] =  Commonmodel::get_all_details ('fc_lists', array (
					'user_id' => $wishuser_id,
					'name' => $wishcatname,
					'id !=' => $list_id
			) );
			if ($WishListCat) {
					$res ['result'] = '1';
			}
			else{
			
			 DB::table('fc_lists')->where('id',$list_id)->update(array('user_id' => $wishuser_id,
					'name' => ucfirst ( $wishcatname ),
					'whocansee' =>  $whocansee));
			
			
			$res ['result'] = '5';
			}
		}
		
		else{
			
		
		$WishListCat = Commonmodel::get_all_details ( 'fc_lists', array (
				'user_id' => $wishuser_id,
				'name' => $wishcatname 
		) );
		if ($WishListCat) {
			$res ['result'] = '1';
		} else {
			$res ['result'] = '0';
		   DB::table('fc_lists')->insert(array('user_id' => $wishuser_id,
					'name' => ucfirst ( $wishcatname ),
					'whocansee' =>  $whocansee));
			$data=DB::getPdo()->lastInsertId();		
					
					
			
			$res ['wlist'] = '<li><label><input type="checkbox" checked="checked" value="' . $data . '" name="wishlist_cat[]" id="wish_' . $data . '" />' . $wishcatname . '</label></li>';
		}	
			 
		}
		echo json_encode ( $res );
	}
	
	
public function AddWishListForm($id) {
		if(Session::has('userid'))
		{
		$Rental_id =$id;
		$productList = Userproperty::get_product_details_wishlist($Rental_id);
		//echo $this->db->last_query($this->data ['productList']);die;
		$WishListCat  = Userproperty::get_list_details_wishlist (Session::get('userid'));
		
		$notesAdded = Userproperty::get_notes_added ( $Rental_id,Session::get('userid') );
		
		return View::make('popupwishlist')->with('productList',$productList)->with('WishListCat',$WishListCat)->with('notesAdded',$notesAdded) ;
		
		} 
	}

	public function send_message()
	{
		$sender_id = $_POST['sender_id'];
		$receiver_id = $_POST['receiver_id'];
		$booking_id = $_POST['booking_id'];
		$product_id = $_POST['product_id'];
		$subject = $_POST['subject'];
		$message = $_POST['message'];
		$statusQry = Commonmodel::get_all_details ('fc_med_message', array ('bookingNo' => $booking_id));
		$status = $statusQry[0]->status;
		$dataArr = array(
			'productId' => $product_id ,
			'senderId' => $sender_id ,
			'receiverId' => $receiver_id ,
			'bookingNo' => $booking_id ,
			'subject' => $subject ,
			'message' => $message,
			'status' => $status
		);
	 
		DB::table('fc_med_message')->insert($dataArr);
		
		echo 'Success';
	}
	public function confirm_booking() {
	
		$sender_id = $_POST [ 'sender_id' ];
		$receiver_id = $_POST [ 'receiver_id'];
		$booking_id = $_POST [ 'booking_id'];
		$product_id = $_POST [ 'product_id'];
		$subject = $_POST [ 'subject' ];
		$message = $_POST [ 'message'];
		

		$status = $_POST [ 'status'];
		$dataArr = array(
			'productId' => $product_id ,
			'senderId' => $sender_id ,
			'receiverId' => $receiver_id ,
			'bookingNo' => $booking_id ,
			'subject' => $subject ,
			'message' => $message,
			'point' => '1',
			'status' => $status
		);
		
		 	
		DB::table('fc_med_message')->insert($dataArr);
		DB::table('fc_med_message')->where('bookingNo',$booking_id)->update(array('status' => $status));
		$newdata = array('approval' => $status);
		$condition = array('Bookingno' => $booking_id);
		DB::table('fc_rentalsenquiry')->where('bookingNo',$booking_id)->update(array('approval' => $status));
		
		$bookingDetails = Commonmodel::get_all_details('fc_rentalsenquiry', $condition);
		
		 
		$enqId = $bookingDetails[0]->id;
		
		
		$hostdetails=User::where('id', '=',$bookingDetails[0]->renter_id)->first();
		
		
		$guestdetails=User::where('id', '=',$bookingDetails[0]->user_id)->first();
		
		
		$propertydata= DB::select(DB::raw("SELECT p.*,t.name as typename,r.name as roomname,c.name as cancellationname,u.username as hostname,u.email,u.profileimg as hostimg,l.name as destination from air_property p left join air_propertytype t on p.property_type=t.id left join air_roomtype  r on p.room_type=r.id left join air_cancellation c on p.cancellation_policy=c.id left join users u on p.userid=u.id left join air_location l on p.location=l.id where p.id=".$bookingDetails[0]->prd_id));
		
		
		 
		 
		
		if($status = 'Accept')
		{
		
		$data = array('productId' => $bookingDetails[0]->prd_id, 'bookingNo' => $bookingDetails[0]->Bookingno, 'senderId' => $bookingDetails[0]->user_id, 'receiverId' => $bookingDetails[0]->renter_id, 'subject' => 'Booking Request : '.$bookingDetails[0]->Bookingno, 'message' => $message,'title'=>$propertydata[0]->title,'description'=>$propertydata[0]->description, 'destination'=>$propertydata[0]->destination, 'address'=>$propertydata[0]->address,'hostname'=>$hostdetails->username,'hostemail'=>$hostdetails->email,'guestname'=>$guestdetails->username,'guestemail'=>$guestdetails->email,'status'=>1);
			  Mail::send('emails.testmail4', $data, function($message)use($guestdetails)  {
			 $message->to($guestdetails->email,$guestdetails->username)
	         ->subject('Holidaysiles Booking Request Accepted By Host ');});
			 
			 Mail::send('emails.testmail4', $data, function($message)  {
			 $message->to('hostaccept@holidaysiles.com','Admin')
	         ->subject('Host Acceptance');
				});
		
		
			//$this->guideapproval($enqId);
			//$this->guideapprovalbyhost($enqId);
		}
		else if($status = 'Decline')
		{
		$data = array('productId' => $bookingDetails[0]->prd_id, 'bookingNo' => $bookingDetails[0]->Bookingno, 'senderId' => $bookingDetails[0]->user_id, 'receiverId' => $bookingDetails[0]->renter_id, 'subject' => 'Booking Request : '.$bookingDetails[0]->Bookingno, 'message' => $message,'title'=>$propertydata[0]->title,'description'=>$propertydata[0]->description, 'destination'=>$propertydata[0]->destination, 'address'=>$propertydata[0]->address,'hostname'=>$hostdetails->username,'hostemail'=>$hostdetails->email,'guestname'=>$guestdetails->username,'guestemail'=>$guestdetails->email,'status'=>0);
		
			  Mail::send('emails.testmail3', $data, function($message)use($guestdetails)  {
			 $message->to($guestdetails->email,$guestdetails->username)
	         ->subject('Holidaysiles Booking Request Declined By Host ');});
			 
			 Mail::send('emails.testmail4', $data, function($message)  {
			 $message->to('hostaccept@holidaysiles.com','Admin')
	         ->subject('Guest Request Declined by host');
				});
		}
		echo 'Success';
	}





		public function new_conversation($id)
	    { 
			$bookingNo =Request::segment(2);
			$bookingNo= $bookingNo;
			$userId=Session::get('userid');
		if(Session::has('userid') && $bookingNo != '')
		{
			$conversationDetails =Commonmodel::get_all_details ('fc_med_message', array ('bookingNo' => $bookingNo ),array(array('field'=>'id', 'type'=>'desc')));
			 
			$product_details = Commonmodel::get_all_details('air_property',array('id'=>$conversationDetails[0]->productId));
			 
			 //print_r($product_details);exit;
			if($product_details[0]->userid == $userId)
			{
		 
			//$this->db->update(MED_MESSAGE, array('msg_read' => 'Yes','host_msgread_status'=>'Yes')); 
			
			DB::table('fc_med_message')->where('bookingNo',$bookingNo)->update(array('msg_read' => 'Yes','host_msgread_status'=>'Yes'));
			
			
			}else
			{
			   DB::table('fc_med_message')->where('bookingNo',$bookingNo)->update(array('msg_read' => 'Yes','user_msgread_status'=>'Yes'));
			}
			$bookingDetails =Commonmodel::get_booking_details($bookingNo);
		 
			//echo '<pre>';print_r($this->data['bookingDetails']->result_array());die;
			$temp[] = $conversationDetails[0]->senderId;
			$temp[] = $conversationDetails[0]->receiverId;
			 $productId=$conversationDetails[0]->productId;
			
			if(!in_array(Session::get('userid'), $temp))redirect();
			if(Session::get('userid') == $temp[0])
			{
				$sender_id = $temp[0];
				$receiver_id = $temp[1];
			}
			else 
			{
				$sender_id = $temp[1];
				$receiver_id = $temp[0];
			}
			
			
			


			$senderDetails =Commonmodel::get_all_details ('users', array ('id' => $sender_id));
			
			$receiverDetails= Commonmodel::get_all_details ('users', array ('id' => $receiver_id));
			
			$verifiedDetails =Commonmodel::get_all_details ('fc_requirements', array ('user_id' => $receiver_id));
			
		 $productDetails=Commonmodel::get_all_details ( 'air_property', array ('id' => $productId));

			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    		$ip = $_SERVER['HTTP_CLIENT_IP'];
		    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		    } else {
    		$ip = $_SERVER['REMOTE_ADDR'];
		   }
           $sender = Commonmodel::get_users_details("where id=$sender_id");
           $receiver = Commonmodel::get_users_details("where id=$receiver_id");
		
		 $DisableCalDate="";
		 $forbiddenCheckIn="";
		 $forbiddenCheckOut="";
		 $prev="";
		$CalendarBooking =Commonmodel::get_all_details('bookings',array('PropId'=>$productDetails[0]->id));
		if($CalendarBooking)
		{	
			foreach($CalendarBooking as $CRow){
				$DisableCalDate .='"'.$CRow->the_date.'",';
			}
		}
		else
		{
			$forbiddenCheckIn='[]';
			$forbiddenCheckOut='[]';
		}
		$all_dates = array();
		$selected_dates = array();
		foreach($CalendarBooking as $date)
		{	
			$all_dates[] = trim($date->the_date);
			$date1 = new DateTime(trim($date->the_date));
			$date2 = new DateTime($prev);
			$diff = $date2->diff($date1)->format("%a");
			if($diff == '1')
			{	
				$selected_dates[] = trim($date->the_date);
			}	
			$prev = trim($date->the_date);
			$DisableCalDate = '';
			foreach($all_dates as $CRow)
			{
				$DisableCalDate .= '"'.$CRow.'",';
			}	
			$forbiddenCheckIn='['.$DisableCalDate.']';
			$DisableCalDate = '';
			foreach($selected_dates as $CRow)
			{
				$DisableCalDate .= '"'.$CRow.'",';
			}	
			$forbiddenCheckOut='['.$DisableCalDate.']';
		}	
		
		$unread_messages_count = Commonmodel::get_unread_messages_count(Session::get('userid'));
		
	 return View::make('host_conversation')->with('receiverDetails',$receiverDetails)->with('bookingDetails',$bookingDetails)->with('conversationDetails',$conversationDetails)->with('sender_id',$sender_id)->with('receiver_id',$receiver_id)->with('bookingNo',$bookingNo)->with('productId',$productId)->with('senderDetails',$senderDetails)->with('menu',15)->with('userId',$userId);
			
		}
		else
		{
			return Redirect::to('/');
		}
	}
			public function getDashboard() //insert data
			{
			return View::make('dashboard')->with('menu',1);
			}
			public function getYourtrips() //insert data
			{
			$user_id=Session::get('userid');
			$bookedRental=Commonmodel::booked_rental_trip(Session::get('userid'));
			return View::make('upcomingtrips')->with('menu',4)->with('bookedRental',$bookedRental)->with('user_id',$user_id);
			}
			/*Profile Menu */
			public function getEdit() //insert data
			{
			return View::make('editprofile')->with('menu',5)->with('submenu',1);
			}
			public function getUploadphoto() //insert data
			{
			return View::make('uploadphoto')->with('menu',5)->with('submenu',2);
			}
		 public function getVerifymail()
		{
		 $userDetails= Commonmodel::get_all_details ('users', array (
					'id' =>Session::get('userid')
			) );	

		$uid = $userDetails[0]->id;
		$username = $userDetails[0]->username;
		$email = $userDetails[0]->email;
		
	 	
		$randStr=substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 7);
		
		$condition = array (
				'user_id' => $uid 
		);
		$dataArr = array (
				'verify_code' => $randStr 
		);
		$user_id_exist=Commonmodel::get_all_details('fc_requirements',array('user_id'=>$uid));
		
		$user_id_exist1=Commonmodel::get_all_details('users',array('id'=>$uid));
		 
		if($user_id_exist)
		{
		//echo "<script>alert('inside')</script>"; die;
		$dataArr1 = array (
					'user_id' => $uid,
					'id_verified'=>'no',
					'verify_code' => $randStr
					);
					
		$condition1 = array();
		$excludeArr= array();
	 	
		DB::table('fc_requirements')->where('user_id',$uid)->update($dataArr1);
		 $dataArr2 = array (
					
					'is_verified'=>0,
					'verify_code' => $randStr
					);
		DB::table('users')->where('id',$uid)->update($dataArr2);
		 
		}
		else
		{
		$dataArr1 = array (
					'user_id' => $uid,
					'id_verified'=>'no',
					'verify_code' => $randStr
					);
			DB::table('fc_requirements')->insert($dataArr1);		
					
		 $dataArr2 = array (
					'is_verified'=>0,
					'verify_code' => $randStr
					);
		DB::table('users')->where('id',$uid)->update($dataArr2);
		 
		}

	 $data=array('confirmation_code'=>$randStr,'username'=>$username);
Mail::send('emails.verify', $data, function($message) use($userDetails){
	$message->to($userDetails[0]->email,$userDetails[0]->username)
	->subject('Verify your ID -HolidaySiles');
	}); 
 
 
 Flash::success('Verification ID Email sent to your email address.Please Verify by clicking the Email.');
 return Redirect::to('profile/trustandverify');
				}
			public function getTrustandverify() //insert data
			{
			
	$userDetail= Commonmodel::get_all_details ('users', array (
					'id' =>Session::get('userid')
			) );		
			 return View::make('userverify')->with('menu',5)->with('submenu',3)->with('UserDetail',$userDetail);
			}
			public function getReviewsaboutyou() //insert data
			{
			return View::make('reviewsaboutyou')->with('menu',5)->with('submenu',4);
			}
			public function getReviewsbyyou() //insert data
			{
			return View::make('reviewsbyyou')->with('menu',5)->with('submenu',4);
			}

			/* Profile Menu end */		
			/*Inbox */	
			public function getInbox() //insert data
			{
			
		$med_messages= Commonmodel::get_med_messages(Session::get('userid'));
	    return View::make('inbox')->with('med_messages',$med_messages)->with('menu',2);
			}
			public function getSentbox() //insert data
			{
			return View::make('sentbox')->with('menu',2);
			}
			/* Inbox end */		
			/* Listings */
				public function updateresreq() {
 
				if (Input::get( 'verify_id' ))
				$verify_id = 'yes';
			else
				$verify_id = 'no';
			
		  
		 
			
			DB::table('fc_requirements')->where('user_id',Session::get('userid'))->delete();
			
			$data = array (
					'user_id' => Input::get( 'user_id' ),
					'id_verified' => $verify_id,
					 
			);
			 DB::table('fc_requirements')->insert($data);
			
		 Flash::success('Reservation requirements updated successfully');
		 return Redirect::to('profile/reservationreq');
		 
	}
			public function getReservationreq() //insert data
			{
			$userDetail= Commonmodel::get_all_details ('users', array (
					'id' =>Session::get('userid')
			) );
			$RequestDetail =Commonmodel::get_all_details ('fc_requirements', array (
					'user_id' => Session::get('userid')
			) );
			
			 return View::make('verifyreq')->with('userDetail',$userDetail)->with('RequestDetail',$RequestDetail)->with('menu',3)->with('submenu',3);
		 
			}
			public function getReservation() //insert data
			{
			
			$bookedRental =Userproperty::booked_rental (Session::get('userid'));
			return View::make('reservation')->with('menu',3)->with('submenu',2)->with('bookedRental',$bookedRental);
			}
			/* ListingsEnd*/		
			/* Account Menu */ 
			public function getPaymethod() //insert data
			{
			return View::make('accountpay')->with('menu',6)->with('submenu',2);
			}
			/* Account menu Ended */		
			public function getView() //insert data
			{
			return View::make('viewprofile');
			}
			public function postUpdateprofile()
			{
			$id=User::find(Input::get('profileid'));
			if($id)
			{
			$input=array('username'=>Input::get('firstname'),
			'lastname'=>Input::get('lastname'),
			'gender'=>Input::get('gender'),
			'phonenumber'=>Input::get('phone_number'),
			'email1'=>Input::get('email2'),
			'livelocation'=>Input::get('s_city'),
			'myself'=>Input::get('description'),
			'ename'=>Input::get('emergency_name'),
			'eemail'=>Input::get('emergency_phone'),
			'ephone'=>Input::get('emergency_email'),
			'erelation'=>Input::get('emergency_relationship'),
			);
			$id->update($input);
			Flash::success('Profile Details Updated Successfully');
			return Redirect::to('profile/edit');
			} 
			}
			public function postPhotoup()
			{
			$id=User::find(Input::get('profileid'));
			$banner = Input::file('upload-file');
			$filename = str_random(8).$banner->getClientOriginalName();
			$input=array('profileimg'=>$filename);
			$destinationPath = './assets/profileimg/';
			$uploadSuccess = Input::file('upload-file')->move($destinationPath,$filename);
			$id->update($input);

			Flash::success('Profile Image Updated Successfully');
			return Redirect::to('profile/uploadphoto');	
			}
			public function getWishlists()
			{
		

		$WishListCat = DB::table('fc_lists')->where('user_id',Session::get('userid'))->get();
			$totalcount=DB::table('fc_lists')->where('user_id',Session::get('userid'))->count();
			
			
			/*$wishlistcount=Wishlist::get_wishlistcount($wishlistdetails);
			$wishlistinfo=Wishlist::get_wishlistinfo($wishlistdetails);
			
			*/
			
			return View::make('wishlist')->with('totalcount',$totalcount)->with('WishListCat',$WishListCat);	
			
			
			
			}
			public function addwishlist()
			{
			$postdata=Input::all();
			$name=Input::get('list_name');
			$status=Input::get('whocansee');
			$id=Session::get('userid');
			$rules = array(
			'list_name'=>'required|unique:fc_lists,name,'.$id,
			);
			$validator = Validator::make($postdata, $rules);
			if ($validator->fails()) 
			{
			return 1;
			}
			else
			{
			
				if($status=='0')
		{
		$whocansee = 'Everyone';
		}
		else {
			$whocansee = 'Only me';
		}
			
		//	$input=array('userid'=>Session::get('userid'),'name'=>$name,'status'=>$status,);
			//$obj=new Wishlist;
			//$obj->create($input);
			
			   DB::table('fc_lists')->insert(array('user_id' => Session::get('userid'),
					'name' => ucfirst ($name ),
					'whocansee' =>  $whocansee));
			return 0;
			}

			}
			
			public function getLogout()
{ 
	Session::forget('userid');
	Session::forget('username');
	Session::forget('reg');
	Session::flush();
	Flash::success("Logged out Successfully");
	return Redirect::to('home');
	
} 
 	/* Property Related Functions */




		  
	 
}