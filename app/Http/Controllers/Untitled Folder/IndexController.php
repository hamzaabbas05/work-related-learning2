<?php
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\Order;

class IndexController extends BaseController
{
 
    private $_api_context;
	//public $trans_id;

    public function __construct()
    {
    
        // setup PayPal api context
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
	
	public function postPayment12()
{
     
    $payer = new Payer();
    $payer->setPaymentMethod('paypal');

    $item_1 = new Item();
    $item_1->setName('Item 1') // item name
        ->setCurrency('USD')
        ->setQuantity(2)
        ->setPrice('15'); // unit price

    $item_2 = new Item();
    $item_2->setName('Item 2')
        ->setCurrency('USD')
        ->setQuantity(4)
        ->setPrice('7');

    $item_3 = new Item();
    $item_3->setName('Item 3')
        ->setCurrency('USD')
        ->setQuantity(1)
        ->setPrice('20');

    // add item to list
    $item_list = new ItemList();
    $item_list->setItems(array($item_1, $item_2, $item_3));

    $amount = new Amount();
    $amount->setCurrency('USD')
        ->setTotal(78);

    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setItemList($item_list)
        ->setDescription('Your transaction description');

    $redirect_urls = new RedirectUrls();
    $redirect_urls->setReturnUrl(URL::route('payment.status'))
        ->setCancelUrl(URL::route('payment.status'));

    $payment = new Payment();
    $payment->setIntent('Sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirect_urls)
        ->setTransactions(array($transaction));

    try {
        $payment->create($this->_api_context);
    } catch (\PayPal\Exception\PPConnectionException $ex) {
        if (\Config::get('app.debug')) {
            echo "Exception: " . $ex->getMessage() . PHP_EOL;
            $err_data = json_decode($ex->getData(), true);
            exit;
        } else {
            die('Some error occur, sorry for inconvenient');
        }
    }

    foreach($payment->getLinks() as $link) {
        if($link->getRel() == 'approval_url') {
            $redirect_url = $link->getHref();
            break;
        }
    }

    // add payment ID to session
    Session::put('paypal_payment_id', $payment->getId());

    if(isset($redirect_url)) {
        // redirect to paypal
        return Redirect::away($redirect_url);
    }

    return Redirect::route('original.route')
        ->with('error', 'Unknown error occurred');
}

     
public function PaymentProcess()
{ 		

	$payer = new Payer();
    $payer->setPaymentMethod('paypal');

		$product_id = Input::get('product_id');
		$tax =Input::get('tax');
		$enquiryid = Input::get('enquiryid'); 
		$product =Commonmodel::get_all_details('air_property',array('id' => $product_id));
		$seller = Commonmodel::get_all_details('users',array('id' => $product[0]->userid));
		$dealcode =0;
		$totalAmount = Input::get('price');
		//User ID
		$loginUserId = Session::get('userid');
		//DealCodeNumber
		$lastFeatureInsertId =Session::get('randomNo') ;
		//echo $lastFeatureInsertId;die;
		$quantity = 1;

		
		if(Session::get('randomNo')!= '') {
			$delete = 'delete from fc_payment where dealCodeNumber = "'.Session::get('randomNo').'" and user_id = "'.$loginUserId.'" and status != "Paid"  ';
		 
		   DB::select(DB::raw("$delete"));
		
		 
			$dealCodeNumber = Session::get('randomNo');
		} else {
			$dealCodeNumber = mt_rand();
		}
		
		$insertIds = array();
		$now = date("Y-m-d H:i:s");
		$paymentArr=array(
			'product_id'=>$product_id,
			'price'=>$totalAmount,
			'indtotal'=>$product[0]->price_night,
			'tax'=>$tax,
			'sumtotal'=>$totalAmount,
			'user_id'=>$loginUserId,
			'sell_id'=>$product[0]->userid,
			'created' => $now,
			'dealCodeNumber' => $dealCodeNumber,
			'status' => 'Pending',
			'shipping_status' => 'Pending',
			'total'  => $totalAmount,
			'EnquiryId'=>$enquiryid,
			'inserttime' => date("Y-m-d H:i:s"));
		
		 DB::table('fc_payment')->insert($paymentArr);
		$insertIds[]= DB::getPdo()->lastInsertId();
		 
		$paymtdata = array(
			'randomNo' => $dealCodeNumber,
			'randomIds' => $insertIds
		);
		$lastFeatureInsertId = $dealCodeNumber;		
	 
		Session::put('paymtdata',$paymtdata);
		 Session::put('dealCodeNumber',$dealCodeNumber);
	
	
	
    $item_1 = new Item();
    $item_1->setName('Room Booking') // item name
        ->setCurrency('EUR')
        ->setQuantity(1)
        ->setPrice($totalAmount); // unit price

 $item_list = new ItemList();
    $item_list->setItems(array($item_1));

    $amount = new Amount();
    $amount->setCurrency('EUR')
        ->setTotal($totalAmount);

    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setItemList($item_list)
        ->setDescription('Your transaction description');

$redirect_urls = new RedirectUrls();
    $redirect_urls->setReturnUrl(URL::route('payment.status'))
        ->setCancelUrl(URL::route('payment.status'));

    $payment = new Payment();
    $payment->setIntent('Sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirect_urls)
        ->setTransactions(array($transaction));

    try {
        $payment->create($this->_api_context);
    } catch (\PayPal\Exception\PPConnectionException $ex) {
        if (\Config::get('app.debug')) {
            echo "Exception: " . $ex->getMessage() . PHP_EOL;
            $err_data = json_decode($ex->getData(), true);
            exit;
        } else {
            die('Some error occur, sorry for inconvenient');
        }
    }

    foreach($payment->getLinks() as $link) {
        if($link->getRel() == 'approval_url') {
            $redirect_url = $link->getHref();
            break;
        }
    }

    // add payment ID to session
    Session::put('paypal_payment_id', $payment->getId());

    if(isset($redirect_url)) {
        // redirect to paypal
        return Redirect::away($redirect_url);
    }

    return Redirect::route('original.route')
        ->with('error', 'Unknown error occurred');
 
   
}

 
	public function getDatesFromRange($start, $end){
    	$dates = array($start);
    	while(end($dates) < $end){
        	$dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
    	}
		return $dates;
	}	
 public function getPaymentStatus()
{
    // Get the payment ID before session clear
    $payment_id = Session::get('paypal_payment_id');
	$dealCodeNumber=Session::get('dealCodeNumber');
	Session::forget('dealCodeNumber');

  // clear the session payment ID
    Session::forget('paypal_payment_id');

 
		$temppayid=Input::get('PayerID');		
		$temptoken=Input::get('token');
		
		
    if (empty($temppayid) || empty($temptoken)) {
	  return Redirect::route('original.route')
          ->with('error', 'Payment failed');
    }

    $payment = Payment::get($payment_id, $this->_api_context);

    // PaymentExecution object includes information necessary 
    // to execute a PayPal account payment. 
    // The payer_id is added to the request query parameters
    // when the user is redirected from paypal back to your site
    $execution = new PaymentExecution();
    $execution->setPayerId(Input::get('PayerID'));
	//Execute the payment
    $result = $payment->execute($execution, $this->_api_context);

	//echo '<pre>';print_r($result);echo '</pre>';
	//exit; // DEBUG RESULT, remove it later*/
	
 if ($result->getState() == 'approved') {
 // payment made
 
 //echo Session::get('newses');exit;
 
	$transId=$result->id;
	$Pray_Email=$result->payer->payer_info->email;
	$UserNo = Session::get('userid');
	$DealCodeNo = $dealCodeNumber; 
	
	
	$PaymentSuccessCheck = Commonmodel::get_all_details('fc_payment',array('user_id' => Session::get('userid'), 'dealCodeNumber' => $DealCodeNo,'status'=>'Paid'));
			
	$EnquiryUpdate = Commonmodel::get_all_details('fc_payment',array('dealCodeNumber'=>$DealCodeNo));
	 $eprd_id = $EnquiryUpdate[0]->product_id;
	$eInq_id = $EnquiryUpdate[0]->EnquiryId;
	
	$price=$EnquiryUpdate[0]->indtotal;
	
	
	/******** coupon   *********/
		 
		  
		// $coupon = Session::get('coupon_strip');
		 
		// print_r($coupon);exit;
         $coupon = explode('-', Session::get('cvalid'));
         if($coupon['0'] != ''){
			$data = array('is_coupon_used' => 'Yes',
		               'discount_type' => $coupon['4'],
					   'coupon_code' => $coupon['0'],
					   'discount' => $coupon['2'],
					   'dval' => $coupon['1'],
					   'total_amt' => $coupon['3']
					  );

		 Session::forget('cvalid'); 
		   
		 DB::table('fc_payment')->where('dealCodeNumber',$DealCodeNo)->update($data);
		 
		 $data = array('totalAmt' => $coupon['2']);
		 
		 
		 
		 
		  DB::table('fc_rentalsenquiry')->where('id',$eInq_id)->update($data);
		
		 
		 
		 $data = array('code' => trim($coupon['0']));
		 
		 
		 $couponi = Commonmodel::get_all_details('air_coupon',$data);
		 
		  
		}
		
		
		/******** coupon end  *************/
	
	 $invoicedata = Commonmodel::get_all_details('fc_rentalsenquiry',array('id'=>$eInq_id));		
	
	
	$condition1 = array('user_id'=>$UserNo,'prd_id'=>$eprd_id,'id'=>$eInq_id);
	$dataArr1 = array('booking_status'=>'Booked');
	
	DB::table('fc_rentalsenquiry')->where($condition1)->update($dataArr1);
	
	$Confirmation=Userproperty::PaymentSuccess(Session::get('userid'),$DealCodeNo,$transId,$Pray_Email);
   $SelBookingQty =Commonmodel::get_all_details('fc_rentalsenquiry',array( 'id' => $eInq_id));	 		
	$productId = $SelBookingQty[0]->prd_id;
		$arrival = $SelBookingQty[0]->checkin;
		$depature = $SelBookingQty[0]->checkout;
		$dates = $this->getDatesFromRange($arrival, $depature);
		$i=1;
		$dateMinus1= count($dates)-1; 
		
		foreach($dates as $date){
			if($i <= $dateMinus1){
				$BookingArr=Commonmodel::get_all_details('bookings',array('PropId' => $productId,'id_state' => 1,'id_item' => 1,'the_date' => $date));
				if($BookingArr){
				
				}else{
					$dataArr = array('PropId' => $productId,
									 'id_state' => 1,
									 'id_item' => 1,
									 'the_date' => $date
									);
			 DB::table('bookings')->insert($dataArr);		
					
					
				}
		   }
		   $i++;
		}
		
		//SCHEDULE calendar
		$DateArr=Commonmodel::get_all_details('bookings',array('PropId'=>$productId));
		$dateDispalyRowCount=0;
		$dateArrVAl="";
		if($DateArr){
			$dateArrVAl .='{';
			foreach($DateArr as $dateDispalyRow){
									
				if($dateDispalyRowCount==0){
					$dateArrVAl .='"'.$dateDispalyRow->the_date.'":{"available":"1","bind":0,"info":"","notes":"","price":"'.$price.'","promo":"","status":"booked"}';
				}else{
					$dateArrVAl .=',"'.$dateDispalyRow->the_date.'":{"available":"1","bind":0,"info":"","notes":"","price":"'.$price.'","promo":"","status":"booked"}';
				}
				$dateDispalyRowCount=$dateDispalyRowCount+1;
			}
			$dateArrVAl .='}';
		}
									
		$inputArr4=array();
		$inputArr4 = array('id' =>$productId,'data' => trim($dateArrVAl));
		DB::table('schedule')->where('id',$productId)->update($inputArr4);							
		 
		
		
									
		//End SCHEDULE calendar
		
		
		$condition = array('status' => 1, 'id'=>'3');
		$service_tax_host=Commonmodel::get_all_details('air_commission', $condition);
		
		if($service_tax_host[0]->type==1)
		{
			$host_tax_type="Percentage";
		}
		else if($service_tax_host[0]->type==2)
		{
			$host_tax_type="Flat";
		}
		$host_tax_value = $service_tax_host[0]->price;
		
		
		 
		$condition = array('status' => 1, 'id'=>'1');
		$service_tax_guest=Commonmodel::get_all_details('air_commission', $condition);
		
		if($service_tax_guest[0]->type==1)
		{
			$guest_tax_type="Percentage";
		}
		else if($service_tax_guest[0]->type==2)
		{
			$guest_tax_type="Flat";
		}
		$guest_tax_value = $service_tax_guest[0]->price;
		
	 	$orderDetails =Commonmodel::get_all_details('fc_rentalsenquiry',array( 'id' => $eInq_id));
		
		//echo $this->db->last_query();die;
		$userDetails = Commonmodel::get_all_details('users',array('id' => $orderDetails[0]->renter_id));
		$guest_fee = $orderDetails[0]->serviceFee;
		$netAmount = $orderDetails[0]->totalAmt-$orderDetails[0]->serviceFee;
		if($host_tax_type== 'Flat')
		{
			$host_fee = $host_tax_value;
		}
		else 
		{
			$host_fee = ($netAmount * $host_tax_value)/100;
		}
		$payable_amount = $netAmount - $host_fee;
		$data1 = array('host_email'=>$userDetails[0]->email,
			'booking_no'=>$orderDetails[0]->Bookingno,
			'total_amount'=>$orderDetails[0]->totalAmt,
			'guest_fee'=>$guest_fee,
			'host_fee'=>$host_fee,
			'payable_amount'=>$payable_amount
			);
		$chkQry = Commonmodel::get_all_details('fc_commission_tracking',array( 'booking_no' => 
		 $orderDetails[0]->Bookingno));
		 
		if($chkQry)
		{	  //$this->product_model->simple_insert(COMMISSION_TRACKING, $data1);
		DB::table('fc_commission_tracking')->where('booking_no',$orderDetails[0]->Bookingno)->update($data1); 	
		}
		else
		{
		DB::table('fc_commission_tracking')->insert($data1);
			
		}

	 							
		//Siva}
						
		$Confirmation = 'Success';
		$productId = $productId;
		//$this->load->view('site/order/order.php',$this->data);
		
		//return View::make('orderresult');
	//echo "success".$invoicedata[0]->Bookingno;	exit;
		
		 
		//return View::make('order')->with('Confirmation',$Confirmation)->with('invoicedata',$invoicedata);
		
		return Redirect::to('order_response')->with('Confirmation',$Confirmation)->with('bno',$invoicedata[0]->Bookingno)->with('amt',$invoicedata[0]->totalAmt);
		
		 
    }
	
		//return View::make('order')->with('Confirmation',"Failure");
		
		return Redirect::to('order_response')->with('Confirmation',"Failure");
			
	
   /* return Redirect::route('original.route')
        ->with('error', 'Payment failed');*/
}
}