<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CancellationRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\CodeRepository;

class CancellationController extends Controller
{
    private $bredCrum = "Cancellation Policy";
    
    protected $cancellationRepository;

    protected $currencyRepository;

     protected $codeRepository;

    public function __construct(
        CancellationRepository $cancellationRepository,
        CurrencyRepository $currencyRepository,
        CodeRepository $codeRepository
    ) {
        $this->cancellationRepository = $cancellationRepository;
        $this->currencyRepository = $currencyRepository;
        $this->codeRepository = $codeRepository;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->cancellationRepository->getAll();
        $currencies = $this->currencyRepository->getCurrency(true);
        return View('admin.cancellationpolicy.index', compact('bredCrum', 'currentObj', 'currencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        $codeArr = $this->codeRepository->getByCodeType(true, 'cancellation');
        $currencies = $this->currencyRepository->getCurrency(true);
        return View('admin.cancellationpolicy.create', compact('bredCrum', 'codeArr', 'currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('currency_id' => $request->get('currency_id'),'cancellation_type' => $request->get('cancellation_type'),'cancellation_charge' => $request->get('cancellation_charge'),'name' => $request->get('name'),'desc' => $request->get('desc'),'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->cancellationRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/cancellation/create')->withErrors($validator);
        } else {
            $Obj = $this->cancellationRepository->create($postData);
            flash('cancellation Policy Added Successfully', 'success');
            return \Redirect('admin/cancellation');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->cancellationRepository->getById($id);
        $codeArr = $this->codeRepository->getByCodeType(true, 'cancellation');
        $currencies = $this->currencyRepository->getCurrency(true);
        return View('admin.cancellationpolicy.edit', compact('bredCrum', 'editObj', 'currencies', 'codeArr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = array('currency_id' => $request->get('currency_id'),'cancellation_type' => $request->get('cancellation_type'),'cancellation_charge' => $request->get('cancellation_charge'),'name' => $request->get('name'),'desc' => $request->get('desc'),'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->cancellationRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/cancellation/'.$id.'/edit')->withErrors($validator);
        } else {
            $Obj = $this->cancellationRepository->update($postData, $id);
            flash('cancellation Policy Updated Successfully', 'success');
            return \Redirect('admin/cancellation');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
