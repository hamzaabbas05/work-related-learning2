<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\Http\Controllers\Controller;
use App\Repositories\LanguageRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\SettingsRepository;
use App\Repositories\PropertySettingsRepository;
use Carbon\Carbon;

class SettingsController extends Controller
{
    protected $languageRepository;

    protected $currencyRepository;

    protected $settingsRepository;

    protected $propertySettingsRepository;

    public function __construct(
        LanguageRepository $languageRepository,
        CurrencyRepository $currencyRepository,
        SettingsRepository $settingsRepository,
        PropertySettingsRepository $propertySettingsRepository
    ) {

        $this->languageRepository = $languageRepository;
        $this->currencyRepository = $currencyRepository;
        $this->settingsRepository = $settingsRepository;
        $this->propertySettingsRepository = $propertySettingsRepository;
    }

    public function showGeneral()
    {
        $bredCrum = 'Site Settings';
        $languages = $this->languageRepository->getLanguage(true);
        $currencies = $this->currencyRepository->getCurrency(true);
        return view('admin.settings.general', compact('bredCrum', 'languages', 'currencies'));
        //return view('admin.manage', compact('bredCrum', 'languages', 'currencies'));
    }

    public function storeGeneral(Request $request)
    {

        $formData = array('site_title' => $request->get('site_title'),
                        'site_slogan' => $request->get('site_slogan'),
                        'site_meta_title' => $request->get('site_meta_title'),
                        'site_meta_keywords' => $request->get('site_meta_keywords'),
                        'site_meta_description' => $request->get('site_meta_description'),
                        'language_id' => $request->get('language_id'),
                        'currency_id' => $request->get('currency_id')
                        );
        $validator = Validator::make($formData, $this->settingsRepository->generalValidationRule);
        if ($validator->fails()) {
             return \Redirect('admin/settings/general')->withErrors($validator);
        } else {
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp. '-' .$file->getClientOriginalName();
            //$image->filePath = $img_name;
            $file->move(public_path().'/images/logo', $img_name);
            $formData = array_merge($formData, array('logo' => $img_name));
        }
        if ($request->hasFile('favicon')) {
            $file = $request->file('favicon');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp. '-' .$file->getClientOriginalName();
            //$image->filePath = $img_name;
            $file->move(public_path().'/images/favicon', $img_name);
            $formData = array_merge($formData, array('favicon' => $img_name));
        }
        $affectedRows = $this->settingsRepository->update($formData);
        flash('General Settings updated Successfully', 'success');
        return \Redirect('admin/settings/general');
        }
    }

    public function showEmail()
    {
        $bredCrum = 'Email Settings';
        return view('admin.settings.email', compact('bredCrum'));
    }

    public function storeEmail(Request $request)
    {
        $formData = array('superadmin_email' => $request->get('superadmin_email'),
                        'support_email' => $request->get('support_email'),
                        'noreply_email' => $request->get('noreply_email'),
                        'contact_email' => $request->get('contact_email'),
                        'contact_skype' => $request->get('contact_skype'),
                        'open_timings' => $request->get('open_timings'),
                        'contact_phone' => $request->get('contact_phone'),
                        'contact_address' =>  $request->get('contact_address'),
                    );
        $validator = Validator::make($formData, $this->settingsRepository->emailValidationRule);
        if ($validator->fails()) {
             return \Redirect('admin/settings/email')->withErrors($validator);
        } else {
            $affectedRows = $this->settingsRepository->update($formData);
            flash('Email and Contact Settings updated Successfully', 'success');
            return \Redirect('admin/settings/email');
        }
    }

    public function showSocialMedia()
    {
        $bredCrum = 'SocialMedia';
        return view('admin.settings.socialmedia', compact('bredCrum'));
    }

    public function storeSocialmedia(Request $request)
    {
        $formData = array('facebook_url' => $request->get('facebook_url'),
                        'googleplus_url' => $request->get('googleplus_url'),
                        'twitter_url' => $request->get('twitter_url'),
                    );
        $validator = Validator::make($formData, $this->settingsRepository->socialMediaValidationRule);
        if ($validator->fails()) {
             return \Redirect('admin/settings/socialmedia')->withErrors($validator);
        } else {
        $affectedRows = $this->settingsRepository->update($formData);
        flash('SocialMedia Settings updated Successfully', 'success');
        return \Redirect('admin/settings/socialmedia');
        }
    }

    public function showSocialApp()
    {
        $bredCrum = 'SocialApplication';
        return view('admin.settings.app', compact('bredCrum'));
    }

    public function storeApp(Request $request)
    {
        $formData = array('fb_app_id' => $request->get('fb_app_id'),
                        'fb_secret_key' => $request->get('fb_secret_key'),
                        'gplus_app_id' => $request->get('gplus_app_id'),
                        'gplus_secret_key' => $request->get('gplus_secret_key'),
                    );
        $validator = Validator::make($formData, $this->settingsRepository->appValidationRule);
        if ($validator->fails()) {
             return \Redirect('admin/settings/app')->withErrors($validator);
        } else {
        $affectedRows = $this->settingsRepository->update($formData);
        flash('App Settings updated Successfully', 'success');
        return \Redirect('admin/settings/app');
        }
    }

    public function showPayment()
    {
        $bredCrum = 'Payment';
        return view('admin.settings.payment', compact('bredCrum'));
    }

    public function storePayment(Request $request)
    {
        $formData = array('paypal_email' => $request->get('paypal_email'),
                        'paypal_client_id' => $request->get('paypal_client_id'),
                        'paypal_secret_key' => $request->get('paypal_secret_key'),
                    );
        $validator = Validator::make($formData, $this->settingsRepository->paypalValidationRule);
        if ($validator->fails()) {
             return \Redirect('admin/settings/payment')->withErrors($validator);
        } else {
        $affectedRows = $this->settingsRepository->update($formData);
        flash('Payment Settings updated Successfully', 'success');
        return \Redirect('admin/settings/payment');
        }
    }

    public function showNoImage()
    {
        $bredCrum = 'NoImage';
        return view('admin.settings.noimage', compact('bredCrum'));
    }

    public function storeNoImage(Request $request)
    {
        $formData = array();
        if ($request->hasFile('no_image_profile')) {
            $file = $request->file('no_image_profile');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp. '-' .$file->getClientOriginalName();
            //$image->filePath = $img_name;
            $file->move(public_path().'/images/noimage', $img_name);
            $formData = array_merge($formData, array('no_image_profile' => $img_name));
        }
        if ($request->hasFile('no_image_property')) {
            $file = $request->file('no_image_property');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp. '-' .$file->getClientOriginalName();
            //$image->filePath = $img_name;
            $file->move(public_path().'/images/noimage', $img_name);
            $formData = array_merge($formData, array('no_image_property' => $img_name));
        }
        $affectedRows = $this->settingsRepository->update($formData);
        flash('NoImage Settings updated Successfully', 'success');
        return \Redirect('admin/settings/noimage');
    }

    public function showPropertyAttributes()
    {
        $bredCrum = 'General Property Attributes';
        $obj = $this->propertySettingsRepository->getById(1);
        return view('admin.settings.property_settings', compact('bredCrum', 'obj'));
    }

    public function storePropertyAttributes(Request $request)
    {
        $formData = array('booking_request' => $request->get('booking_request'),
                   );
        $affectedRows = $this->propertySettingsRepository->update($formData, 1);
        flash('General Property Attributes updated Successfully', 'success');
        return \Redirect('admin/settings/propertyattributes');
        /*$validator = Validator::make($formData, $this->settingsRepository->attributeValidationRule);
        if ($validator->fails()) {
             return \Redirect('admin/settings/propertyattributes')->withErrors($validator);
        } else {
        $affectedRows = $this->propertySettingsRepository->update($formData, 1);
        flash('General Property Attributes updated Successfully', 'success');
        return \Redirect('admin/settings/propertyattributes');
        }*/
    }
}
