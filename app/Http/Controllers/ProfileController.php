<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Validator;

class ProfileController extends Controller
{
     /**
    * Instances of Admin Repository
    */
    protected $userRepository;

    /**
    * Access all methods and objects in Repository
    */

    public function __construct(UserRepository $userRepository)
    {

        $this->userRepository = $userRepository;
    }

    public function showProfile()
    {
        $userObject = $this->userRepository->getLoginUser();
        $bredCrum = 'Profile';
        return View('admin.profile.profile', compact('userObject', 'bredCrum'));
    }

    public function showPassword(Request $request)
    {
        $bredCrum = 'Change Password';
        $userObject = $this->userRepository->getLoginUser();
        return View('admin.profile.changepassword', compact('userObject', 'bredCrum'));
    }

    public function changePassword(Request $request)
    {
        $rules = array(
                'password'         => 'required',
                'password_confirm' => 'required|same:password');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect('admin/profile/password')->withErrors($validator);
        } else {
            $formData = array('password' => \Hash::make($request->get('password')));
            $this->userRepository->updateUser($formData);
            flash('Password Updated Successfully', 'success');
            return \Redirect('admin/profile/password');
        }
    }

    public function store(Request $request)
    {
        $formData = $request->all();
        $postData = array('name' => $request->get('name'), 'email' => $request->get('email'));
        $rules = [
        'name' => 'required', // alpha - only contain letters
        'email' => 'required|email|unique:users,email,'.$this->userRepository->getCurrentUserLoginId(), // unique:tablename - checking given table form unique email
        ];
        $validator = \Validator::make($formData, $rules);
        if ($validator->fails()) {
            return \Redirect('admin/profile')->withInput()->withErrors($validator);
        }
        if ($request->hasFile('profile_img_name')) {
            $file = $request->file('profile_img_name');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp. '-' .$file->getClientOriginalName();
            //$image->filePath = $img_name;
            $file->move(public_path().'/images/profile', $img_name);
            $postData = array_merge($postData, array('profile_img_name' => $img_name));
        }
        $affectedRows = $this->userRepository->updateUser($postData);
        flash('Profile Updated Successfully', 'success');
        return \Redirect('admin/profile');
    }
}

