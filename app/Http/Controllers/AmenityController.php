<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\AmenityRepository;
use App\Repositories\CodeRepository;
use Carbon\Carbon;

class AmenityController extends Controller
{
    private $bredCrum = "Amenity";
    
    protected $amenityRepository;

    protected $codeRepository;

    public function __construct(AmenityRepository $amenityRepository, CodeRepository $codeRepository)
    {
        $this->amenityRepository = $amenityRepository;
        $this->codeRepository = $codeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->amenityRepository->getSelectedTypes([1,2]);
        return View('admin.amenity.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        $codeArr = $this->codeRepository->getByCodeType(true, 'amenities');
        return View('admin.amenity.create', compact('bredCrum', 'codeArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('name' => $request->get('name'),
                        'type' => $request->get('type'),
                        'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->amenityRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/amenities/create')->withErrors($validator);
        } else {
            if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp. '-' .$file->getClientOriginalName();
            //$image->filePath = $img_name;
            $file->move(public_path().'/images/amenity', $img_name);
            $postData = array_merge($postData, array('icon' => $img_name));
            }
            $Obj = $this->amenityRepository->create($postData);
            flash('Amenity Added Successfully', 'success');
            return \Redirect('admin/amenities');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->amenityRepository->getById($id);
        $codeArr = $this->codeRepository->getByCodeType(true, 'amenities');
        return View('admin.amenity.edit', compact('bredCrum', 'editObj', 'codeArr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = array('name' => $request->get('name'),
                        'type' => $request->get('type'),
                        'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->amenityRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/amenities/'.$id.'/edit')->withErrors($validator);
        } else {
            if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $timestamp. '-' .$file->getClientOriginalName();
            //$image->filePath = $img_name;
            $file->move(public_path().'/images/amenity', $img_name);
            $postData = array_merge($postData, array('icon' => $img_name));
            }
            $Obj = $this->amenityRepository->update($postData, $id);
            flash('Amenity Updated Successfully', 'success');
            return \Redirect('admin/amenities');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
