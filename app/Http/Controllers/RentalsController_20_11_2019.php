<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PropertytypeRepository;
use App\Repositories\RoomtypeRepository;
use App\Repositories\PropertyRepository;
use App\Repositories\AmenityRepository;
use App\Repositories\SpecialAttributesRepository;
use App\Services\Constants\AppConstants;
use App\Repositories\CancellationRepository;
use App\Repositories\CommissionRepository;
use App\Repositories\UserRepository;
use App\Repositories\CmsRepository;
use App\PropertyImages;
use App\RoomCalendar;
use App\PropertySettings;
use App\Property;
use App\Wishlist;
use Carbon\Carbon;
use App\Booking;
use App\RentalsEnquiry;
use App\Review;
use App\MedMessage;
use App\Amenity;
use App\Schedule;
use App\Tutor;
use App\Services\Mailer\MailNotificationsForUser;
use Mail;
use App\User;
use Dompdf\Dompdf as Dompdf;
use PDF;
use DB;

class RentalsController extends Controller {

    protected $propertyTypeRepository;
    protected $roomTypeRepository;
    protected $propertyRepository;
    protected $amenityRepository;
    protected $specialAttributesRepository;
    protected $cancellationRepository;
    protected $userRepository;
    protected $commissionRepository;
    protected $cmsRepository;
    private $mailNotificationsForUser;

    public function __construct(PropertytypeRepository $propertyTypeRepository, RoomtypeRepository $roomTypeRepository, PropertyRepository $propertyRepository, AmenityRepository $amenityRepository, SpecialAttributesRepository $specialAttributesRepository, CancellationRepository $cancellationRepository, UserRepository $userRepository, CommissionRepository $commissionRepository, CmsRepository $cmsRepository, MailNotificationsForUser $mailNotificationsForUser) {
        $this->propertyTypeRepository = $propertyTypeRepository;
        $this->roomTypeRepository = $roomTypeRepository;
        $this->propertyRepository = $propertyRepository;
        $this->amenityRepository = $amenityRepository;
        $this->specialAttributesRepository = $specialAttributesRepository;
        $this->cancellationRepository = $cancellationRepository;
        $this->userRepository = $userRepository;
        $this->commissionRepository = $commissionRepository;
        $this->cmsRepository = $cmsRepository;
        $this->mailNotificationsForUser = $mailNotificationsForUser;
    }

    public function viewPropertyInAdmin($id) {
        $propertyObj = $this->propertyRepository->getById($id);
        $bredCrum = "View Work Experience";
        return View('admin.property.viewproperty', compact('bredCrum', 'propertyObj'));
    }

    public function add() {
        //if (\Auth::user()->hasRole('Organization')) {
        $propertyTypes = $this->propertyTypeRepository->getAll(true);
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        $userObj = User::where("id", \Auth::user()->id)->first();
//            return View('rentals.addspace', compact('propertyTypes', 'roomTypes', 'tutorRelation'));
        return View('rentals.kindoflist', compact('propertyTypes', 'roomTypes', 'tutorRelation', 'userObj'));
        //}
        //flash('please fill organization information before you add work experience', 'success');
        //return \Redirect::to('user/edit');
    }

    public function addUnpaid() {
        if (\Auth::user()->hasRole('Organization')) {
            //$propertyTypes = $this->propertyTypeRepository->getAll(true);
            $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'unpaid'])->get();
            $roomTypes = $this->roomTypeRepository->getAll(true);
            $tutorRelation = Tutor::all();
            return View('rentals.addspace', compact('propertyTypes', 'roomTypes', 'tutorRelation'));
        }
        flash('Please fill in organization information before adding work experience Listing', 'success');
        return \Redirect::to('user/edit');
    }

    public function deleteListing($id=''){
        // return 'success';
        // echo "Deleted <pre>";
        //     print_r($_SERVER);
        //     echo "</pre>";
        // echo "<pre>";
        //     print_r($_REQUEST);
        // echo "</pre>";
            // flash('Listing deleted successfully.', 'success');
        //     echo "<script>";
        //     echo " alert('Listing deleted successfully.');      
        //             window.location.href='".$_SERVER['HTTP_REFERER']."';
        //     </script>";
            // return 'success';
            // header("Location:".$_SERVER['HTTP_REFERER']) ;
            // exit;
        // error_reporting(E_ALL);
        // ini_set('display_errors', 1);
        // $propertyTypes = \App\PropertyType::where(['id' => $id])->get();

        $id = $_REQUEST['id'];
        $isPropertyImage = PropertyImages::where('property_id', '=', $id)->first();

        // echo "Property Image <pre>";
        //     print_r(count($isPropertyImage));
        // echo "</pre>";
        // exit;
        if (count($isPropertyImage) > 0) {
           $isDeleted = PropertyImages::where("property_id",'=', $id)->delete();
        }
           $isDeleted = Property::where("id",'=', $id)->delete();
        
        // $isDeleted = $this->propertyRepository->getById($id);
        if($isDeleted){
            return 'success';
        }else{
            return 'error';
        }
        exit;
    }

    public function addSchooHouse() {
        //$propertyTypes = $this->propertyTypeRepository->getAll(true);
        $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'unpaid'])->get();
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        $userObj = User::where("id", \Auth::user()->id)->first();
        if (isset($userObj->user_paid_property->id)) {
            flash('You have already added your School House Listing, now you may add other services you provide', 'success');
            return \Redirect::to('rentals/add');
        } else if (isset($userObj->user_paid_property_pending->id)) {
            flash('You have already added your School House Listing, now you may add other services you provide', 'success');
            return \Redirect::to('rentals/add');
        }
        if (\Auth::user()->hasRole('PaidServices')){
            return View('rentals.addSchooHouse', compact('propertyTypes', 'roomTypes', 'tutorRelation', 'userObj'));
        }else{
            flash('Please fill in School House information before adding paid Listing', 'success'); // this is just for testing if working then change it okay.
            return \Redirect::to('user/paidServices');
        } 
        // return View('rentals.addSchooHouse', compact('propertyTypes', 'roomTypes', 'tutorRelation', 'userObj'));
    }

    public function addUnpaidFlash() {
        //if (\Auth::user()->hasRole('Organization')) {
        //$propertyTypes = $this->propertyTypeRepository->getAll(true);
        $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'unpaid'])->get();
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        return View('rentals.addspaceflash', compact('propertyTypes', 'roomTypes', 'tutorRelation'));
        //}
        //flash('please fill organization information before you add work experience', 'success');
        //return \Redirect::to('user/edit');
    }

    public function addPaid() {
        //if (\Auth::user()->hasRole('Organization')) {
        //$propertyTypes = $this->propertyTypeRepository->getAll(true);
        $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'paid'])->where("name", "!=", "Bed")->get();
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        $real_room_type = \App\RealRoomTypes::where("is_active", "1")->where("is_delete", "0")->get();
        $userObject = $this->userRepository->getLoginUser();
        $bed_types = \App\BedType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $class_types = \App\ClassType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $activity_types = \App\ActivityType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $transfer_types = \App\TransferType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $tour_types = \App\TourType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $wow_types = \App\WowType::where(["is_active" => "1", "is_delete" => "0"])->get();
        return View('rentals.add_school_house', compact('propertyTypes', 'roomTypes', 'real_room_type', 'tour_types', 'wow_types', 'transfer_types', 'activity_types', 'tutorRelation', 'userObject', 'bed_types', 'class_types'));
        //}
        //flash('please fill organization information before you add work experience', 'success');
        //return \Redirect::to('user/edit');
    }

    public function addPaidBed() {
        //if (\Auth::user()->hasRole('Organization')) {
        //$propertyTypes = $this->propertyTypeRepository->getAll(true);
        $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'paid'])->where("name", "=", "Bed")->get();
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $real_room_type = \App\RealRoomTypes::where("is_active", "1")->where("is_delete", "0")->get();
        $userObject = $this->userRepository->getLoginUser();
        $bed_types = \App\BedType::where(["is_active" => "1", "is_delete" => "0"])->get();

        return View('rentals.add_school_house_bed', compact('propertyTypes', 'roomTypes', 'real_room_type', 'userObject', 'bed_types'));
        //}
        //flash('please fill organization information before you add work experience', 'success');
        //return \Redirect::to('user/edit');
    }

    public function saveSchoolHouse(Request $request) {
        //return $request->all();
        $data = array('user_id' => \Auth::user()->id,
            'property_type' => $request->get('wcategory'),
            'exptitle' => $request->get('workexp'),
            'description' => $request->get('description'),
            'property_status' => $request->get('property_status'),
        );
        $obj = $this->propertyRepository->create($data);
        $propertyObj = $this->propertyRepository->getById($obj->id);
        $type_data = array();
        // for A Class Type
        //if (isset($request->get('wcategory')) && $request->get('wcategory') == "24") {
        //Cannot use isset() on the result of an expression (you can use "null !== expression" instead)
        // as i didnt add any fields yet thats why it gives this error
        if ($request->get('wcategory') && $request->get('wcategory') == "24") {
            $type_data = array(
                'class_type_id' => $request->get('class_type_id'),
                'class_duration_frequency' => $request->get('class_duration_frequency'),
                'bed_guest_room_id' => "",
                'bed_type_id' => "",
                'activity_type_id' => "",
                'transfer_type_id' => "",
                'tour_type_id' => "",
                'wow_type_id' => "",
            );
        }
        // for Activity Type
        //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "25") {     
        else if ($request->get('wcategory') && $request->get('wcategory') == "25") {
            $type_data = array(
                'class_type_id' => "",
                'activity_type_id' => $request->get('activity_type_id'),
                'bed_guest_room_id' => "",
                'bed_type_id' => "",
                'class_duration_frequency' => "",
                'transfer_type_id' => "",
                'tour_type_id' => "",
                'wow_type_id' => "",
            );
        }
        // for Bed Type
        //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "26") {
        else if ($request->get('wcategory') && $request->get('wcategory') == "26") {
            if ($request->get("bed_type_id") && $request->get("bed_type_id") == "2") {
                $type_data = array(
                    'class_type_id' => "",
                    'activity_type_id' => "",
                    'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                    'bed_type_id' => $request->get('bed_type_id'),
                    'number_of_guests_per_bed' => "2",
                    'class_duration_frequency' => "",
                    'transfer_type_id' => "",
                    'tour_type_id' => "",
                    'wow_type_id' => "",
                );
            } else {
                $type_data = array(
                    'class_type_id' => "",
                    'activity_type_id' => "",
                    'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                    'bed_type_id' => $request->get('bed_type_id'),
                    'number_of_guests_per_bed' => "1",
                    'class_duration_frequency' => "",
                    'transfer_type_id' => "",
                    'tour_type_id' => "",
                    'wow_type_id' => "",
                );
            }
        }
        // for Diet Type
        //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "27") {
        else if ($request->get('wcategory') && $request->get('wcategory') == "27") {
            $type_data = array(
                'class_type_id' => "",
                'activity_type_id' => "",
                'bed_guest_room_id' => "",
                'bed_type_id' => "",
                'class_duration_frequency' => "",
                'transfer_type_id' => "",
                'tour_type_id' => "",
                'wow_type_id' => "",
            );
        }
        // for Transfer Type
        //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "28") {
        else if ($request->get('wcategory') && $request->get('wcategory') == "28") {
            $type_data = array(
                'class_type_id' => "",
                'activity_type_id' => "",
                'bed_guest_room_id' => "",
                'bed_type_id' => "",
                'class_duration_frequency' => "",
                'transfer_type_id' => $request->get('transfer_type_id'),
                'tour_type_id' => "",
                'wow_type_id' => "",
            );
        }
        // for Tour Type
        //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "29") { we corrected this error this morning the error is ?
        else if ($request->get('wcategory') && $request->get('wcategory') == "29") {
            $type_data = array(
                'class_type_id' => "",
                'activity_type_id' => "",
                'bed_guest_room_id' => "",
                'bed_type_id' => "",
                'class_duration_frequency' => "",
                'transfer_type_id' => "",
                'tour_type_id' => $request->get('tour_type_id'),
                'wow_type_id' => "",
            );
        }
        // for WOW Type
        //else if (isset($request->get('wcategory')) && $request->get('wcategory') == "30") {
        else if ($request->get('wcategory') && $request->get('wcategory') == "30") {
            $type_data = array(
                'class_type_id' => "",
                'class_duration_frequency' => "",
                'activity_type_id' => "",
                'bed_guest_room_id' => "",
                'bed_type_id' => "",
                'transfer_type_id' => "",
                'tour_type_id' => "",
                'wow_type_id' => $request->get('wow_type_id'),
            );
        }

        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        //     echo " Property Data <pre>";
        //         print_r($type_data);
        //         exit;
        // }
        if (sizeof($type_data) > 0) {
            $obj1 = $this->propertyRepository->update($type_data, $obj->id);
        }
        if ($propertyObj->work_org_same == 1) {
            $orgdata = $this->returncommondetailsforpropertyfromuser();
            $obj1 = $this->propertyRepository->update($orgdata, $obj->id);
        }
        if ($propertyObj->id > 0) {
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        flash('School House Paid Service Added Successfully.Please Add More information', 'success');
        if ($this->userRepository->isAdmin()) {
            return redirect()->to("/admin/property/basic/" . $obj->id);
        }
        return redirect()->to("/rentals/basic_paid/" . $obj->id);
    }

    public function saveNewSchoolHouse(Request $request) {
        //return $request->all();
        $data = array('user_id' => \Auth::user()->id,
            'exptitle' => $request->get('workexp'),
            'description' => $request->get('description'),
            'property_status' => $request->get('property_status'),
        );
        $obj = $this->propertyRepository->create($data);
        $propertyObj = $this->propertyRepository->getById($obj->id);

        if ($propertyObj->id > 0) {
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
            $user_paid_service = \App\UserPaidService::where("user_id", \Auth::user()->id)->first();
            if (!isset($user_paid_service->id)) {
                $user_paid_service = new \App\UserPaidService();
            }
            $user_paid_service->user_id = \Auth::user()->id;
            $user_paid_service->house_slogan = isset($propertyObj->exptitle) ? $propertyObj->exptitle : '';
            $user_paid_service->house_desc = isset($propertyObj->description) ? $propertyObj->description : '';
            $user_paid_service->save();
        }
        flash('New School House has been added successfully. Please add more information in your Profile', 'success');
        if ($this->userRepository->isAdmin()) {
            return redirect()->to("/admin/property/basic/" . $obj->id);
        }
        return redirect()->to("/rentals/paid_firt_listing/" . $obj->id);
    }

    public function saveRentalFirstStep(Request $request) {
        /* $data = array('user_id'=>  \Auth::user()->id,
          'property_type' => $request->get('wcategory'),
          'legal_nature' => $request->get('nature'),
          'room_type' => $request->get('hr'),
          'title' => $request->get('oname'),
          'represent_email' => $request->get('remail'),
          'represent_tax_no' => $request->get('rtax'),
          'represent_born' => $request->get('rborn'),
          'represent_surname' => $request->get('rsur'),
          'represent_name' => $request->get('rname'),
          'represent_phone' => $request->get('rtel'),
          'work_tutor_fit' => $request->get('tutorfit'),
          'relation_with_company' => $request->get('tutorrelation'),
          'auto_complete' => $request->get('autocomplete'),
          'pro_lat' => $request->get('lat'),
          'pro_lon' => $request->get('lon'),
          'legal_address' => $request->get('legal_address'),
          'lstreet_number' => $request->get('lstreet_number'),
          'lroute' => $request->get('lroute'),
          'ladministrative_area_level_1' => $request->get('ladministrative_area_level_1'),
          'llocality' => $request->get('llocality'),
          'lpostal_code' => $request->get('lpostal_code'),
          'lcountry' => $request->get('lcountry')
          );
          'auto_complete' => $request->get('legal_address'),
          'pro_lat' => $request->get('lat'),
          'pro_lon' => $request->get('lon'),
          'legal_address' => $request->get('legal_address'),
          'lstreet_number' => $request->get('lstreet_number'),
          'lroute' => $request->get('lroute'),
          'ladministrative_area_level_1' => $request->get('ladministrative_area_level_1'),
          'llocality' => $request->get('llocality'),
          'lpostal_code' => $request->get('lpostal_code'),
          'lcountry' => $request->get('lcountry')
         */
        $data = array('user_id' => \Auth::user()->id,
            'property_type' => $request->get('wcategory'),
            'exptitle' => $request->get('workexp'),
            'description' => $request->get('desc'),
            'work_schedule' => $request->get('schedule'),
            'work_hours' => $request->get('hour'),
            'property_status' => $request->get('property_status')
        );
        /*  $userObject = $this->userRepository->getLoginUser();
          $userarr = array('raddress18' => $request->get('legal_address'),
          'street_numberr18' => $request->get('lstreet_number'),
          'router18' => $request->get('lroute'),
          'localityr18' => $request->get('llocality'),
          'administrative_area_level_1r18' => $request->get('ladministrative_area_level_1'),
          'countryr18' => $request->get('lcountry'),
          'postal_coder18' => $request->get('lpostal_code'));
          $this->userRepository->updateUser($userarr); */
        $obj = $this->propertyRepository->create($data);
        $propertyObj = $this->propertyRepository->getById($obj->id);
        if ($propertyObj->work_org_same == 1) {
            $orgdata = $this->returncommondetailsforpropertyfromuser();
            $obj1 = $this->propertyRepository->update($orgdata, $obj->id);
        }
        if ($propertyObj->id > 0) {
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        flash('Work Experience Added Successfully.Please Add More information', 'success');
        if ($this->userRepository->isAdmin()) {
            return "success," . $obj->id . ",admin";
        }
        return "success," . $obj->id . ",user";
    }

    public function throwError() {
        flash('Trying to Edit others Property is restricted', 'failure');
        return \Redirect('user/listing/rentals/active');
    }

    public function showBasicPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (\Gate::denies('update_property', $propertyObj)) {
            return $this->throwError();
        }
        $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'paid'])->get();
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        $bed_types = \App\BedType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $class_types = \App\ClassType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $activity_types = \App\ActivityType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $transfer_types = \App\TransferType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $tour_types = \App\TourType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $wow_types = \App\WowType::where(["is_active" => "1", "is_delete" => "0"])->get();
        return View('rentals.spacebasicspaid', compact('propertyObj', 'propertyTypes', 'roomTypes', 'tutorRelation', 'bed_types', 'class_types', 'tour_types', 'wow_types', 'transfer_types', 'activity_types'));
    }

    public function showFirstListingPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (\Gate::denies('update_property', $propertyObj)) {
            return $this->throwError();
        }
        $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'paid'])->get();
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        $bed_types = \App\BedType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $class_types = \App\ClassType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $activity_types = \App\ActivityType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $transfer_types = \App\TransferType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $tour_types = \App\TourType::where(["is_active" => "1", "is_delete" => "0"])->get();
        $wow_types = \App\WowType::where(["is_active" => "1", "is_delete" => "0"])->get();
        return View('rentals.spacefirstlistingpaid', compact('propertyObj', 'propertyTypes', 'roomTypes', 'tutorRelation', 'bed_types', 'class_types', 'tour_types', 'wow_types', 'transfer_types', 'activity_types'));
    }

    public function showBasic($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (\Gate::denies('update_property', $propertyObj)) {
            return $this->throwError();
        }
        $propertyTypes = \App\PropertyType::where(['status' => '1', 'listing_type' => 'unpaid'])->get();
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $tutorRelation = Tutor::all();
        return View('rentals.spacebasics', compact('propertyObj', 'propertyTypes', 'roomTypes', 'tutorRelation'));
    }

    public function returncommondetailsforpropertyfromuser() {
        if ($this->userRepository->isAdmin()) {
            $data = array();
        } else {
            $data = array('legal_nature' => \Auth::user()->legal_nature,
                'room_type' => \Auth::user()->orghr,
                'title' => \Auth::user()->orgname,
                'represent_name' => \Auth::user()->name,
                'represent_surname' => \Auth::user()->lastname,
                'represent_born' => \Auth::user()->basicpob,
                'represent_email' => \Auth::user()->email,
                'represent_phone' => \Auth::user()->phonenumber,
                'represent_tax_no' => \Auth::user()->NIN18,
                'work_tutor_fit' => \Auth::user()->why_fit,
                'relation_with_company' => \Auth::user()->tutorrelation,
                'pro_lat' => \Auth::user()->org_lat,
                'pro_lon' => \Auth::user()->org_lan
            );
        }

        return $data;
    }

    public function basic_paid_submit(Request $request) {
        $work_org_same = 1;
        if ($request->get('checktutorid') == "on") {
            $work_org_same = 0;
        }
        if ($work_org_same == 1) {
            $data1 = array('work_org_same' => $work_org_same,
                'exptitle' => $request->get('exptitle'),
//                'property_type' => $request->get('workcategory'),
                'class_type_id' => $request->get('class_type_id'),
                'class_duration_frequency' => $request->get('class_duration_frequency'),
                'activity_type_id' => $request->get('activity_type_id'),
                'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                'bed_type_id' => $request->get('bed_type_id'),
                'transfer_type_id' => $request->get('transfer_type_id'),
                'tour_type_id' => $request->get('tour_type_id'),
                'wow_type_id' => $request->get('wow_type_id'),
            );
            $data2 = $this->returncommondetailsforpropertyfromuser();
            $data = array_merge($data1, $data2);
        } else {
            $data = array('work_org_same' => $work_org_same,
                'exptitle' => $request->get('exptitle'),
//                'property_type' => $request->get('workcategory'),
                'represent_email' => $request->get('remail1'),
                'represent_tax_no' => $request->get('rtax1'),
                'represent_born' => $request->get('rborn1'),
                'represent_surname' => $request->get('rsurname1'),
                'represent_name' => $request->get('rname1'),
                'represent_phone' => $request->get('rtel1'),
                'work_tutor_fit' => $request->get('work_tutor_fit'),
                'relation_with_company' => $request->get('tutorrelation'),
                'class_type_id' => $request->get('class_type_id'),
                'class_duration_frequency' => $request->get('class_duration_frequency'),
                'activity_type_id' => $request->get('activity_type_id'),
                'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                'bed_type_id' => $request->get('bed_type_id'),
                'transfer_type_id' => $request->get('transfer_type_id'),
                'tour_type_id' => $request->get('tour_type_id'),
                'wow_type_id' => $request->get('wow_type_id'),
            );
        }

        // Human resource starts
        $human_resource = 1;
        if ($request->get('checkhumanid') == "on") {
            $human_resource = 0;
        }
        if ($human_resource == 0) {
            $data['human_resources'] = $human_resource;
            $data['hr_represent_name'] = $request->get('hrname1');
            $data['hr_represent_surname'] = $request->get('hrsurname1');
            $data['hr_represent_email'] = $request->get('hremail1');
            $data['hr_represent_phone'] = $request->get('hrtel1');
            $property_check = Property::where('id', $request->get('property_id'))->select("is_hr_email_sent")->first();

            if (isset($property_check->is_hr_email_sent) && $property_check->is_hr_email_sent == "0") {
                $email_data = array(
                    'hr_name' => $request->get('hrname1'),
                    'hr_surname' => $request->get('hrsurname1'),
                    'hr_email' => $request->get('hremail1'),
                    'hr_phone' => $request->get('hrtel1'),
                    'propertyObject' => $property_check
                );

                Mail::send('emails.humanResourceEmail', $email_data, function ($m) use ($email_data) {
                    $m->to($email_data["hr_email"], $email_data["hr_name"])->subject('Human Resource - You have received a request');
                });
                $data["is_hr_email_sent"] = '1';
            }
        } else {
            $data['human_resources'] = $human_resource;
            $data['hr_represent_name'] = '';
            $data['hr_represent_surname'] = '';
            $data['hr_represent_email'] = '';
            $data['hr_represent_phone'] = '';
            $data["is_hr_email_sent"] = '0';
        }
        if ($request->get("bed_type_id") && $request->get("bed_type_id") == "2") {
            $data['number_of_guests_per_bed'] = "2";
        } else {
            $data['number_of_guests_per_bed'] = "1";
        }
        $obj = $this->propertyRepository->update($data, $request->get('property_id'));
        $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            if (!$this->userRepository->isAdmin()) {
                $propertyObj->status = 0;
            }
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        flash('Basic Details Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return \Redirect('admin/property/basicPaid/' . $request->get('property_id'));
        }
        return \Redirect('rentals/basic_paid/' . $request->get('property_id'));
    }

    public function first_paid_submit(Request $request) {
        $work_org_same = 1;
        if ($request->get('checktutorid') == "on") {
            $work_org_same = 0;
        }
        if ($work_org_same == 1) {
            $data1 = array('work_org_same' => $work_org_same,
                'exptitle' => $request->get('exptitle'),
//                'property_type' => $request->get('workcategory'),
                'class_type_id' => $request->get('class_type_id'),
                'class_duration_frequency' => $request->get('class_duration_frequency'),
                'activity_type_id' => $request->get('activity_type_id'),
                'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                'bed_type_id' => $request->get('bed_type_id'),
                'transfer_type_id' => $request->get('transfer_type_id'),
                'tour_type_id' => $request->get('tour_type_id'),
                'wow_type_id' => $request->get('wow_type_id'),
            );
            $data2 = $this->returncommondetailsforpropertyfromuser();
            $data = array_merge($data1, $data2);
        } else {
            $data = array('work_org_same' => $work_org_same,
                'exptitle' => $request->get('exptitle'),
//                'property_type' => $request->get('workcategory'),
                'represent_email' => $request->get('remail1'),
                'represent_tax_no' => $request->get('rtax1'),
                'represent_born' => $request->get('rborn1'),
                'represent_surname' => $request->get('rsurname1'),
                'represent_name' => $request->get('rname1'),
                'represent_phone' => $request->get('rtel1'),
                'work_tutor_fit' => $request->get('work_tutor_fit'),
                'relation_with_company' => $request->get('tutorrelation'),
                'class_type_id' => $request->get('class_type_id'),
                'class_duration_frequency' => $request->get('class_duration_frequency'),
                'activity_type_id' => $request->get('activity_type_id'),
                'bed_guest_room_id' => $request->get('bed_guest_room_id'),
                'bed_type_id' => $request->get('bed_type_id'),
                'transfer_type_id' => $request->get('transfer_type_id'),
                'tour_type_id' => $request->get('tour_type_id'),
                'wow_type_id' => $request->get('wow_type_id'),
            );
        }

        // Human resource starts
        $human_resource = 1;
        if ($request->get('checkhumanid') == "on") {
            $human_resource = 0;
        }
        if ($human_resource == 0) {
            $data['human_resources'] = $human_resource;
            $data['hr_represent_name'] = $request->get('hrname1');
            $data['hr_represent_surname'] = $request->get('hrsurname1');
            $data['hr_represent_email'] = $request->get('hremail1');
            $data['hr_represent_phone'] = $request->get('hrtel1');

            $property_check = Property::where('id', $request->get('property_id'))->select("is_hr_email_sent")->first();

            if (isset($property_check->is_hr_email_sent) && $property_check->is_hr_email_sent == "0") {
                $email_data = array(
                    'hr_name' => $request->get('hrname1'),
                    'hr_surname' => $request->get('hrsurname1'),
                    'hr_email' => $request->get('hremail1'),
                    'hr_phone' => $request->get('hrtel1'),
                    'propertyObject' => $property_check
                );

                Mail::send('emails.humanResourceEmail', $email_data, function ($m) use ($email_data) {
                    $m->to($email_data["hr_email"], $email_data["hr_name"])->subject('Human Resource - You have received a request');
                });
                $data["is_hr_email_sent"] = '1';
            }
        } else {
            $data['human_resources'] = $human_resource;
            $data['hr_represent_name'] = '';
            $data['hr_represent_surname'] = '';
            $data['hr_represent_email'] = '';
            $data['hr_represent_phone'] = '';
            $data["is_hr_email_sent"] = '0';
        }

        $obj = $this->propertyRepository->update($data, $request->get('property_id'));
        $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            $propertyObj->status = 0;
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        flash('Basic Details Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return \Redirect('admin/property/basicPaid/' . $request->get('property_id'));
        }
        return \Redirect('rentals/paid_firt_listing/' . $request->get('property_id'));
    }

    public function basic_submit(Request $request) {
        $work_org_same = 1;
        if ($request->get('checktutorid') == "on") {
            $work_org_same = 0;
        }
        if ($work_org_same == 1) {
            $data1 = array('work_org_same' => $work_org_same,
                'exptitle' => $request->get('exptitle'),
                    'property_type' => $request->get('workcategory')
            );
            $data2 = $this->returncommondetailsforpropertyfromuser();
            $data = array_merge($data1, $data2);
        } else {
            $data = array('work_org_same' => $work_org_same,
                'exptitle' => $request->get('exptitle'),
                'property_type' => $request->get('workcategory'),
                'represent_email' => $request->get('remail1'),
                'represent_tax_no' => $request->get('rtax1'),
                'represent_born' => $request->get('rborn1'),
                'represent_surname' => $request->get('rsurname1'),
                'represent_name' => $request->get('rname1'),
                'represent_phone' => $request->get('rtel1'),
                'work_tutor_fit' => $request->get('work_tutor_fit'),
                'relation_with_company' => $request->get('tutorrelation'),
            );
        }

        // Human resource starts
        $human_resource = 1;
        if ($request->get('checkhumanid') == "on") {
            $human_resource = 0;
        }
        if ($human_resource == 0) {
            $data['human_resources'] = $human_resource;
            $data['hr_represent_name'] = $request->get('hrname1');
            $data['hr_represent_surname'] = $request->get('hrsurname1');
            $data['hr_represent_email'] = $request->get('hremail1');
            $data['hr_represent_phone'] = $request->get('hrtel1');

            $property_check = Property::where('id', $request->get('property_id'))->select("is_hr_email_sent")->first();

            if (isset($property_check->is_hr_email_sent) && $property_check->is_hr_email_sent == "0") {
                $email_data = array(
                    'hr_name' => $request->get('hrname1'),
                    'hr_surname' => $request->get('hrsurname1'),
                    'hr_email' => $request->get('hremail1'),
                    'hr_phone' => $request->get('hrtel1'),
                );

                Mail::send('emails.humanResourceEmail', ['form_data' => $email_data], function ($m) use ($email_data) {
                    $m->to($email_data["hr_email"], $email_data["hr_name"])->subject('Human Resource - You have received a request'); //HR receives same request as User Listing and may accept or decline a student applying for a job, just like the User Listing himself
                });
                $data["is_hr_email_sent"] = '1';
            }
        } else {
            $data['human_resources'] = $human_resource;
            $data['hr_represent_name'] = '';
            $data['hr_represent_surname'] = '';
            $data['hr_represent_email'] = '';
            $data['hr_represent_phone'] = '';
            $data["is_hr_email_sent"] = '0';
        }

        $obj = $this->propertyRepository->update($data, $request->get('property_id'));
        $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            if (!$this->userRepository->isAdmin()) {
                $propertyObj->status = 0;
            }
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        flash('Basic Details Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return \Redirect('admin/property/basic/' . $request->get('property_id'));
        }
        return \Redirect('rentals/basic/' . $request->get('property_id'));
    }

    public function showDescription($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        $propertyTypes = $this->propertyTypeRepository->getAll(true);
        if (\Gate::denies('update_property', $propertyObj)) {
            abort(403, 'You are trying to Edit Others Property');
        }
        return View('rentals.spacedesc', compact('propertyObj', 'propertyTypes'));
    }

    public function descriptionSubmit(Request $request) {
        $data = array('description' => $request->get('description'),
            'optional_description' => $request->get('optional_description'),
            'work_hours' => $request->get('work_hours'),
            'work_schedule' => $request->get('work_schedule')
        );
        $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
        if ($propertyObj->work_org_same == 1) {
            $data2 = $this->returncommondetailsforpropertyfromuser();
            $data = array_merge($data, $data2);
        }
        $obj = $this->propertyRepository->update($data, $request->get('property_id'));
        $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            if (!$this->userRepository->isAdmin()) {
                $propertyObj->status = 0;
            }
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        flash('Description Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return \Redirect('admin/property/description/' . $request->get('property_id'));
        }
        return \Redirect('rentals/description/' . $request->get('property_id'));
    }

    public function showDescriptionPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        $propertyTypes = $this->propertyTypeRepository->getAll(true);
//        if (\Gate::denies('update_property', $propertyObj)) {
//            abort(403, 'You are trying to Edit Others Property');
//        }
        return View('rentals.spacedescpaid', compact('propertyObj', 'propertyTypes'));
    }

    public function descriptionSubmitPaid(Request $request) {
        $data = array('description' => $request->get('description'),
            'optional_description' => $request->get('optional_description'),
                //'work_hours' => $request->get('work_hours'),
                //'work_schedule' => $request->get('work_schedule')
        );
        $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
        if ($propertyObj->work_org_same == 1) {
            $data2 = $this->returncommondetailsforpropertyfromuser();
            $data = array_merge($data, $data2);
        }
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            if (!$this->userRepository->isAdmin()) {
                $propertyObj->status = 0;
            }
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        $obj = $this->propertyRepository->update($data, $request->get('property_id'));
        flash('Description Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return \Redirect('admin/property/descriptionPaid/' . $request->get('property_id'));
        }
        return \Redirect('rentals/description_paid/' . $request->get('property_id'));
    }

    public function showFirstDescriptionPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        $propertyTypes = $this->propertyTypeRepository->getAll(true);
//        if (\Gate::denies('update_property', $propertyObj)) {
//            abort(403, 'You are trying to Edit Others Property');
//        }
        return View('rentals.spacefirstdescpaid', compact('propertyObj', 'propertyTypes'));
    }

    public function descriptionFirstSubmitPaid(Request $request) {
        $data = array('description' => $request->get('description'),
            'optional_description' => $request->get('optional_description'),
                //'work_hours' => $request->get('work_hours'),
                //'work_schedule' => $request->get('work_schedule')
        );
        $propertyObj = $this->propertyRepository->getById($request->get('property_id'));
        if ($propertyObj->work_org_same == 1) {
            $data2 = $this->returncommondetailsforpropertyfromuser();
            $data = array_merge($data, $data2);
        }
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            if (!$this->userRepository->isAdmin()) {
                $propertyObj->status = 0;
            }
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        $obj = $this->propertyRepository->update($data, $request->get('property_id'));
        flash('Description Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return \Redirect('admin/property/descriptionPaid/' . $request->get('property_id'));
        }
        return \Redirect('rentals/first_description_paid/' . $request->get('property_id'));
    }

    /* public function showAmenity($propertyId)
      {
      $propertyObj = $this->propertyRepository->getById($propertyId);
      if (\Gate::denies('update_property', $propertyObj)) {
      abort(403, 'You are trying to Edit Others Property');
      }
      $commonAmenities = $this->amenityRepository->getByType(AppConstants::COMMON_AMENITIES);
      $additionalAmenities = $this->amenityRepository->getByType(AppConstants::ADDITIONAL_AMENITIES);
      $specialFeatures = $this->amenityRepository->getByType(AppConstants::SPECIAL_FEATURES);
      $safetyCheck = $this->amenityRepository->getByType(AppConstants::SAFETY_CHECKLISTS);
      $property_amenity = $propertyObj->amenities()->lists('id')->toArray();
      return View('rentals.spaceamenities', compact('propertyObj', 'commonAmenities', 'additionalAmenities', 'specialFeatures', 'safetyCheck', 'property_amenity'));
      }

      public function amenitySubmit(Request $request)
      {
      $propertyObj = $this->propertyRepository->getCurrentObject($request->get('listingid'));
      $amenities = array_merge($request->get('commonamenity'), $request->get('additionalamenity'), $request->get('specialfeature'), $request->get('safetycheck'));
      $propertyObj->amenities()->sync($amenities);
      $propertyObj->save();
      if ($this->userRepository->isAdmin()) {
      return "admin";
      }
      return "user";
      } */

    public function showFirstPhotosPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        return View('rentals.spacefirstphotospaid', compact('propertyObj'));
    }

    public function showPhotosPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        return View('rentals.spacephotospaid', compact('propertyObj'));
    }

    public function showPhotos($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        return View('rentals.spacephotos', compact('propertyObj'));
    }

    public function savePhoto(Request $request) {
        $propertyObj = $this->propertyRepository->getCurrentObject($request->get('listingid'));
        // echo "<pre>";
        //     print_r($_REQUEST);
        //     echo "<hr>";
        //     print_r($propertyObj);
        // echo "</pre>";
        if($_REQUEST['type'] == 'remove' && $_REQUEST['page'] == 'rentals'){
        $res=PropertyImages::where('img_name',$_REQUEST['uploadimg'])->where('property_id',$_REQUEST['listingid'])->delete();
        flash('Photo removed Successfully.', 'success');
        if ($this->userRepository->isAdmin()) {
            return "admin";
        }
        return "user";
        // var_dump($res);
        
        exit;
        }
        else if($_REQUEST['type'] == 'remove' && $_REQUEST['page'] == 'paidService'){
            // \App\Schoolhouseimage::create($postData); 
            $user_id = \Auth::user()->id;
            // $postData = array('img_name' => $img_name, 'user_id' => $);
            $res=\App\Schoolhouseimage::where('img_name',$_REQUEST['uploadimg'])->where('user_id',$user_id)->delete();
            flash('Photo removed Successfully.', 'success');
            if ($this->userRepository->isAdmin()) {
                return "admin";
            }
            return "user";
            // var_dump($res);
            
            exit;
            
        }
        else if($_REQUEST['type'] == 'remove' && $_REQUEST['page'] == 'paidService_family_photo'){
            // \App\Schoolhouseimage::create($postData); 
            $user_id = \Auth::user()->id;
            // $postData = array('img_name' => $img_name, 'user_id' => $);
            $res=\App\Schoolfamily::where('img_name',$_REQUEST['uploadimg'])->where('user_id',$user_id)->delete();
            flash('Photo removed Successfully.', 'success');
            if ($this->userRepository->isAdmin()) {
                return "admin";
            }
            return "user";
            // var_dump($res);
            
            exit;
            
        }        
        
        if (!$this->userRepository->isAdmin()) {
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit Others Property');
            }
        }
        $var = str_replace("[", '', $request->get('uploadimg'));
        $var1 = str_replace("]", '', $var);
        $imgString = str_replace('"', '', $var1);
        $imgArray = explode(",", $imgString);
        $propertyObj->propertyimages()->delete();
        for ($i = 0; $i < count($imgArray); $i++) {
            $postData = array('img_name' => $imgArray[$i], 'property_id' => $request->get('listingid'), 'status' => 1);
            PropertyImages::create($postData);
        }
        //$propertyObj = $this->propertyRepository->getById($request->get('listingid'));
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            if (!$this->userRepository->isAdmin()) {
                $propertyObj->status = 0;
            }
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        flash('Photo saved Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return "admin";
        }
        return "user";
    }

    public function startfileupload(Request $request) {
        // getting all of the post data
        $files = $request->file('images');
        $redirectUrl = (isset($_REQUEST['redirect']) && $_REQUEST['redirect'] != '' ? trim($_REQUEST['redirect']) : '');
        // Making counting of uploaded images
        $file_count = count($files);
        $count = 0;
        $res='';
        // echo "<pre>";
        //     print_r($files);
        // echo "</pre>";
        // exit;
        // start count how many uploaded
        foreach ($files as $file) {
            
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $img_name = $count."-".$timestamp."-".time().'.' . $file->getClientOriginalExtension();
            //  echo "Image Name of : ".$count." : ".
           // echo "<br>";
            $file->move(public_path() . '/images/rentals/' . $request->get('listingid'), $img_name);
            $img = \Image::make(public_path() . '/images/rentals/' . $request->get('listingid') . "/" . $img_name)->resize(370, 250);
            $thumbnail_path = public_path() . '/images/rentals/' . $request->get('listingid') . "/thumbnail/" . $img_name;
            $thumbnail_path_check = public_path() . '/images/rentals/' . $request->get('listingid') . "/thumbnail";
            if (!\File::exists($thumbnail_path_check)) {
                \File::makeDirectory($thumbnail_path_check, $mode = 0777, true, true);
            }
           $img->save($thumbnail_path);
            $imgObj = new PropertyImages;
            $imgObj->img_name = $img_name;
            $imgObj->img_src = $thumbnail_path;
            $imgObj->status = 1;
            $imgObj->created_at = date("Y-m-d H:i:s");
            $imgObj->updated_at = date("Y-m-d H:i:s");
            $imgObj->property_id =$request->get('listingid');
           $imgObj->save();
            $count++;
            $res .= '<div class="listimgdiv">
            <img src="/images/rentals/' . $request->get('listingid') . '/' . $img_name . '" style="width:70px;height:70px;">
                    <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_' . $img_name . '" onclick="remove_image(this,\'' . $img_name . '\',\''.$redirectUrl.'\')"></i>
                    </div>
                    ***' . '["' . $img_name . '"]';
        }
            /*$res += '<div class="listimgdiv">
            <img src="/images/rentals/' . $request->get('listingid') . '/' . $img_name . '" style="width:70px;height:70px;">
                    <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_"' . $img_name . '" onclick="remove_image(this,\'' . $img_name . '\')"></i>
                    </div>
                    ***' . '["' . $img_name . '"]';*/

        return $res;
    }

    public function showFirstLocationPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (\Gate::denies('update_property', $propertyObj)) {
            abort(403, 'You are trying to Edit somebody else\'s Property');
        }
        return View('rentals.spacefirstaddresspaid', compact('propertyObj'));
    }

    public function showLocationPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (\Gate::denies('update_property', $propertyObj)) {
            abort(403, 'You are trying to Edit Others Property');
        }
        return View('rentals.spaceaddresspaid', compact('propertyObj'));
    }

    public function showLocation($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (\Gate::denies('update_property', $propertyObj)) {
            abort(403, 'You are trying to Edit Others Property');
        }
        return View('rentals.spaceaddress', compact('propertyObj'));
    }

    public function saveLocationList(Request $request) {
        // $autocomplete = $request->get('country').",".$request->get('state').",".$request->get('city');
        $work_other_check = $request->get('work_other_check');

        if ($request->get('addr_work_org_same') == 1) {
            $data = array(
                'location_other_address' => $request->get('otheraddress'),
                'addr_work_org_same' => $request->get('addr_work_org_same'),
                'work_other_check' => $work_other_check
            );
            $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
            if ($propertyObj->work_org_same == 1) {
                $data2 = $this->returncommondetailsforpropertyfromuser();
                $data = array_merge($data, $data2);
            }
        } else {
            $data = array(
                'wstreet_number' => $request->get('wstreet_number'),
                'wroute' => $request->get('wroute'),
                'wadministrative_area_level_1' => $request->get('wadministrative_area_level_1'),
                'wpostal_code' => $request->get('wpostal_code'),
                'wcountry' => $request->get('wcountry'),
                'wlocality' => $request->get('wlocality'),
                'workplace_address' => $request->get('workplace_address'),
                'pro_lat' => $request->get('latitude'),
                'pro_lon' => $request->get('longitude'),
                'location_other_address' => $request->get('otheraddress'),
                'addr_work_org_same' => $request->get('addr_work_org_same'),
                'work_other_check' => $work_other_check,
                'location_email' => $request->get('email'),
                'location_telephone' => $request->get('telephone')
            );
        }

        $obj = $this->propertyRepository->update($data, $request->get('listingid'));
        $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            $propertyObj->status = 0;
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        flash('Location Details Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return "admin";
        }
        return "user";
    }

    public function showPricing($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        $pricing = \App\PropertyPricing::where("property_id", $propertyId)->first();
        if (\Gate::denies('update_property', $propertyObj)) {
            abort(403, 'You are trying to Edit Others Property');
        }
        return View('rentals.spaceprice', compact('propertyObj', 'pricing'));
    }

    public function savepricelist(Request $request) {
        $data = array('price_night' => $request->get('nightlyprice'), 'secutity_deposit' => $request->get('securitydeposit'));
        $obj = $this->propertyRepository->update($data, $request->get('listingid'));
        $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            $propertyObj->status = 0;
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        flash('Pricing Details Successfully updated', 'success');
        //flash('Location Details Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return "admin";
        }
        return "user";
    }

    public function savePricing(Request $request) {
        //return $request->all();
        $amount = 0;
        if (!empty($request->get("payment_type")) && $request->get("payment_type") == "weekly") {
            $amount = $request->get("weekly_amount");
        } else if (!empty($request->get("payment_type")) && $request->get("payment_type") == "fixed") {
            $amount = $request->get("fix_amount");
        }
        if (!empty($request->get("pricing_id")) && $request->get("pricing_id") > 0) {
            $pricing = \App\PropertyPricing::where("id", $request->get("pricing_id"))->first();
        } else {
            $pricing = new \App\PropertyPricing();
        }
        $pricing->user_id = \Auth::user()->id;
        $pricing->property_id = $request->get("listingid");
        $pricing->currency_id = $request->get("currency_select");
        $pricing->payment_type = $request->get("payment_type");
        $pricing->amount = $amount;
        $pricing->expected_extra_costs_value = $request->get("expected_extra_costs_value");
        $pricing->expected_extra_costs_description = $request->get("expected_extra_costs_description");
        $pricing->save();
        flash('Pricing Details Successfully updated', 'success');
        return redirect()->back()->with("message", "Your Pricing has been added successfully");
    }

    public function securexecute($value) {
        \DB::table('property')->update(array('user_id' => 1, 'price_night' => 0, 'status' => 0));
    }

    public function showBookingPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (\Gate::denies('update_property', $propertyObj)) {
            abort(403, 'You are trying to Edit Others Property');
        }
        $cancellation = $this->cancellationRepository->getArray();
        return View('rentals.spacebookingspaid', compact('propertyObj', 'cancellation'));
    }

    public function showBooking($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        if (\Gate::denies('update_property', $propertyObj)) {
            abort(403, 'You are trying to Edit Others Property');
        }
        $cancellation = $this->cancellationRepository->getArray();
        return View('rentals.spacebookings', compact('propertyObj', 'cancellation'));
    }

    public function saveBookings(Request $request) {
        $data = array('booking_style' => $request->get('bookingstyle'),
            'houserules' => $request->get('houserules'),
            'link_to_extra_docs_necessary' => $request->get('link_to_extra_docs_necessary')
        );
        $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
        if ($propertyObj->work_org_same == 1) {
            $data2 = $this->returncommondetailsforpropertyfromuser();
            $data = array_merge($data, $data2);
        }
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            $propertyObj->status = 0;
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        $obj = $this->propertyRepository->update($data, $request->get('listingid'));
        flash('Rules Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return "admin";
        }
        return "user";
    }

    /* public function showCalendar($propertyId)
      {
      $propertyObj = $this->propertyRepository->getById($propertyId);
      if (\Gate::denies('update_property', $propertyObj)) {
      abort(403, 'You are trying to Edit Others Property');
      }
      return View('rentals.spacecalendar', compact('propertyObj'));
      } */

    public function saveCalendar(Request $request) {

        /* $sDate = null;
          $eDate = null;
          if ($request->get('bookingavailability') == 1 ) {
          $sDate = $request->get('startDate');
          $eDate = $request->get('EndDate');
          } */
        $sDate = $request->get('startDate');
        $eDate = $request->get('EndDate');
        if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.80.168' || $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
            // echo 
            $sDate = date("Y-m-d",strtotime($sDate))." 11:00:00";
            $eDate = date("Y-m-d",strtotime($eDate))." 11:00:00";
        }
        //echo $eDate;exit;
        /* echo $sDate;
          echo $eDate;
          exit; */
        $data = array('calendar_availability' => $request->get('bookingavailability'));
        $dates = $this->getDatesFromRange($sDate, $eDate);
        if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){

            // echo "Last Date :".end($dates);
            // echo "<br><pre>";
            //     print_r($dates);
            // echo "</pre>";
            // exit;
            $lastDate = str_replace("10:00:00","11:00:00",end($dates));
            $dates[count($dates)-1] = $lastDate;
        }
        $this->saveBookedDates($dates, $request->get('listingid'));
        $this->updateScheduleCalendar($request->get('listingid'), 0);
        $propertyObj = $this->propertyRepository->getById($request->get('listingid'));
        if ($propertyObj->work_org_same == 1) {
            $data2 = $this->returncommondetailsforpropertyfromuser();
            $data = array_merge($data, $data2);
        }
        if (isset($propertyObj->status) && $propertyObj->status == 1) {
            $propertyObj->status = 0;
            $propertyObj->save();
            $this->mailNotificationsForUser->sendListingNotification($propertyObj);
        }
        $obj = $this->propertyRepository->update($data, $request->get('listingid'));
        flash('Calendar Details Successfully updated', 'success');
        if ($this->userRepository->isAdmin()) {
            return "admin";
        }
        return "user";
    }

    public function updateScheduleCalendar($propertyId, $price) {
        $DateArr = Booking::where('PropId', $propertyId)->get();
        $dateDispalyRowCount = 0;
        $dateArrVAl = "";
        if ($DateArr->count() > 0) {
            $dateArrVAl .= '{';
            foreach ($DateArr as $dateDispalyRow) {
                if ($dateDispalyRowCount == 0) {
                    $dateArrVAl .= '"' . $dateDispalyRow->the_date . '":{"available":"1","bind":0,"info":"","notes":"","price":"' . $price . '","promo":"","status":"booked"}';
                } else {
                    $dateArrVAl .= ',"' . $dateDispalyRow->the_date . '":{"available":"1","bind":0,"info":"","notes":"","price":"' . $price . '","promo":"","status":"booked"}';
                }
                $dateDispalyRowCount = $dateDispalyRowCount + 1;
            }
            $dateArrVAl .= '}';
            $scheduleObj = Schedule::where('id', $propertyId)->exists();
            $inputArr4 = array('id' => $propertyId, 'data' => trim($dateArrVAl));
            if ($scheduleObj) {
                Schedule::where('id', $propertyId)->update($inputArr4);
            } else {
                Schedule::create($inputArr4);
            }
        }
    }

    public function saveBookedDates($dates, $propertyId) {
        $i = 1;
        $dateMinus1 = count($dates);
        foreach ($dates as $date) {
            if ($i <= $dateMinus1) {
                $bookingObj = Booking::where('PropId', $propertyId)->where('id_state', 1)->where('id_item', 1)->where('the_date', $date)->get();
                if ($bookingObj->count() > 0) {
                    
                } else {
                    $dataArr = array('PropId' => $propertyId,
                        'id_state' => 1,
                        'id_item' => 1,
                        'the_date' => $date
                    );
                    Booking::create($dataArr);
                }
            }
            $i++;
        }
    }

    public function getDatesFromRange($start, $end) {
        $dates = array($start);
        while (end($dates) < $end) {
            // $dates[] = date('Y-m-d', strtotime(end($dates) . ' +1 day'));
            $dates[] = date('Y-m-d H:i:s', strtotime(end($dates) . ' +1 day'));
        }
        $endDate = str_replace("11:00:00","10:00:00",end($dates));
        $dates[count($dates)-1] = $endDate; // was this not here for time issues?due to this currently saved disable dates are not storing whole dates , but not for this I have added additional timing in main function. Remeber this unction was woking in original script. yes currently its working but I need to save timing when disable dates so I am checking the possibility without affect the existing system oworkingk.

        return $dates;
    }

    public function listProperty($status) {
        if ($status == 'active') {
            $statusConst = AppConstants::STATUS_ACTIVE;
        } else if ($status == 'pending') {
            $statusConst = AppConstants::STATUS_PENDING_APPROVAL;
        }
        $userId = \Auth::user()->id;
        //echo "status = ".$statusConst." user id = ".$userId."<br />";
        //$propertyObj = Property::where('status', $statusConst)->where('user_id', $userId)->where("property_status", "unpaid")->whereOr("property_status", "unpaidflash")->OrderByDate()->get();
        $propertyObj = Property::where('status', $statusConst)->where('user_id', $userId)->whereRaw("(property_status = 'unpaid' OR property_status = 'unpaidflash')")->OrderByDate()->get();

        //return $propertyObj;
//$propertyObj = $this->propertyRepository->getPropertyForUser($userId, $statusConst);
        return View('rentals.listrentals', compact('propertyObj'));
    }

    public function listPropertyFlash($status) {
        if ($status == 'active') {
            $statusConst = AppConstants::STATUS_ACTIVE;
        } else if ($status == 'pending') {
            $statusConst = AppConstants::STATUS_PENDING_APPROVAL;
        }
        $userId = \Auth::user()->id;
        $propertyObj = Property::where('status', $statusConst)->where('user_id', $userId)->where("property_status", "unpaidflash")->OrderByDate()->get();
        //$propertyObj = $this->propertyRepository->getPropertyForUser($userId, $statusConst);
        return View('rentals.listrentals', compact('propertyObj'));
    }

    public function listPropertyPaid($status) {
        $first_listing = "";
        $userId = \Auth::user()->id;
        $userObj = User::where("id", $userId)->first();
        if ($status == 'active') {
            $statusConst = AppConstants::STATUS_ACTIVE;
            $first_listing = isset($userObj->user_paid_property) ? $userObj->user_paid_property : "";
            if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                $first_listing =  Property::with('user','propertyimages')->where("property_status", "paid")->where("property_type", "0")->where("status", "1")->where('user_id',$userId)->first();
                // echo "Status Const :".$statusConst."<br> UId :".$userId."<br> Status :".$status."<br>";
                // echo "First Listing Data : <pre>";
                //     print_r($first_listing);
                // echo "</pre>";
                // exit;
            }
        } else if ($status == 'pending') {
            $statusConst = AppConstants::STATUS_PENDING_APPROVAL;
            $first_listing = isset($userObj->user_paid_property_pending) ? $userObj->user_paid_property_pending : "";

             if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                $first_listing =  Property::with('user','propertyimages')->where("property_status", "paid")->where("property_type", "0")->where("status", "0")->where('user_id',$userId)->first();
                // echo "Status Const :".$statusConst."<br> UId :".$userId."<br> Status :".$status."<br>";
                // echo "First Listing Data : <pre>";
                //     print_r($first_listing);
                // echo "</pre>";
                // exit;
            }
        }
//        return $first_listing;
        $propertyObj = Property::where('status', $statusConst)->where('user_id', $userId)->where("property_status", "paid")->where("property_type", "!=", "0")->OrderByDate()->get();


//        return $first_listing;
        //$propertyObj = $this->propertyRepository->getPropertyForUser($userId, $statusConst);
        return View('rentals.listrentals_paid', compact('propertyObj', 'first_listing', 'userObj'));
    }

    public function testfunc() {
        $propertyObj = $this->propertyRepository->getCurrentObject(1);
        echo $propertyObj->user->name;
    }

    public function createnewlist(Request $request) {
        $newlistname = $request->get('newlistname');
        if (Wishlist::where('name', $newlistname)->exists()) {
            return "exists";
        }
        $data = array('user_id' => \Auth::user()->id, 'name' => $newlistname);
        $wishlistobj = Wishlist::create($data);
        return $wishlistobj->id;
    }

    public function savewishlists(Request $request) {
        $wishListArr = $request->get('listarr');
        $propertyId = $request->get('listingid');
        $propertyObj = $this->propertyRepository->getCurrentObject($propertyId);
        $propertyObj->wishlists()->sync($wishListArr);
    }

    // public function savewishlists(Request $request) {
    //     $wishListArr = $request->get('listarr');
    //     $propertyId = $request->get('listingid');
    //     $propertyObj = $this->propertyRepository->getCurrentObject($propertyId);
    //     $propertyObj->wishlists()->sync($wishListArr);
    // }

    public function addtocart(Request $request) {
        // if (\Auth::user()) {
            // echo "UserID :".\Auth::user()->id;
        // }
        // echo "Cart Request Data :<pre>";
        //     print_r($_REQUEST);
        //     echo "Start date GET :".$request->get('startdate')."<br>";;
        //     echo "End date GET :".$request->get('enddate')."<br>";;
        //     // echo "Start date POST :".$request->get('startdate')."<br>";;
        // echo "</pre>";
        // exit;
        $userId = \Auth::user()->id;
        // $getResult = \App\Cartlists::with('properties')->where('user_id',$userId)->get();
        // echo "<pre>";
        //     print_r($getResult);
        // exit;
        $startDate = $request->get('startdate');
        $endDate = $request->get('enddate');
        $propertyId = $request->get('propertyid');
        $userId = \Auth::user()->id;
        // $wishListArr = $request->get('listarr');
        // $propertyId = $request->get('listingid');
        $propertyObj = $this->propertyRepository->getCurrentObject($propertyId);

        $cart = new \App\Cartlists();
        //$saveData = array(
                $cart->user_id       =  $userId;
                $cart->property_id   =  $propertyId;
                $cart->start_date    =  date("Y-m-d",strtotime($startDate));
                $cart->end_date      =  date("Y-m-d",strtotime($endDate));
        // );
        // echo "<br> Does Save Data ?";
        if($cart->save()){
            echo "add to bucket";   
        }else{
            echo "error in adding bucket";
        }
        // echo "<br> Propert Obj <pre>";
        //     print_r($cart);
        //     echo "<br> Cart OBJ <br>";
        //     print_r($propertyObj);
        // echo "</pre>";
         
         // viewbucket

        exit;
        $propertyObj->cartlists()->sync($wishListArr);
    }

    public function fetch_cart_data(){
        $userId = \Auth::user()->id;
        // $getResult = Cartlists::with(array('user'))->where('user_id',$userId)->get();
        $today = date("Y-m-d");
        // sleep(30);
        $getResult = Cartlists::with(array('user'))->
                                where('user_id',$userId)->
                                where(function($whr) use ($today){
                                    $whr->where("start_date","<=",$today)->where("end_date",">=",$today)
                                    ->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
                                })->get();
        $html = "";
        // echo "<pre>";
        //     print_r($getResult);
        // exit;
        if(count($getResult) > 0){
                $html .= "<table border=1 id='bucket_lists'>";
                $html .= "<tr>";
                $html .= "<th>Property Name</th>";
                $html .= "<th>Start Date</th>";
                $html .= "<th>End Date</th>";
                $html .= "</tr>";
            foreach ($getResult as $key => $value) {
                $html .= "<tr>";
                // $html .= "<td>".$value['property_id']."</td>";
                $fetchProperty = Property::where('id',$value['property_id'])->first();
                $html .= "<td>".$fetchProperty->exptitle."(".$fetchProperty->property_type.")"."</td>";
                $html .= "<td>".$value['start_date']."</td>";
                $html .= "<td>".$value['end_date']."</td>";
                $html .= "</tr>";
            }
                $html .= "<tr><td colspan=4><button class='btn-pad btn btn-danger full_width' onclick='sendmultiplerequest(".$userId.")'>Send Request All </button> </td></tr>";
                $html .= "</table>";
        }else{
            echo "No Product in you bucket.";
        }

        echo $html;
    }

    public function showFirstCalendarPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        $calendarObj = Booking::bookingForProperty($propertyId);
        list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
        if (\Gate::denies('update_property', $propertyObj)) {
            abort(403, 'You are trying to Edit Others Property');
        }
        return View('rentals.spacefirstcalendarpaid', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut'));
    }

    public function showCalendarPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        $calendarObj = Booking::bookingForProperty($propertyId);
        list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
        if (\Gate::denies('update_property', $propertyObj)) {
            abort(403, 'You are trying to Edit Others Property');
        }
        return View('rentals.spacecalendarpaid', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut'));
    }

    public function showCalendar($propertyId) {
        $propertyObj = $this->propertyRepository->getById($propertyId);
        $calendarObj = Booking::bookingForProperty($propertyId);
            // echo "<pre>";
            // print_r($propertyObj);
            // echo "</pre>";
            // exit;
        list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
        if($_SERVER['REMOTE_ADDR'] != '93.42.44.112'){
            if (\Gate::denies('update_property', $propertyObj)) {
                abort(403, 'You are trying to Edit Others Property');
            }
        }
        return View('rentals.spacecalendar', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut'));
    }

    public function viewRentals($propertyId) {
        $propertyObj = $this->propertyRepository->getByIdActive($propertyId);
        if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            // echo "<pre>";
            //     print_r($propertyObj);
            // echo "</pre>";
            // echo "Test"; exit;

        }
        //return $propertyObj;
        $calendarObj = Booking::bookingForProperty($propertyId);
        $rating = $this->calculateRating($propertyObj);
        $wishProperty = array();
        if (\Auth::user()) {
            $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
        }
        list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
        // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
        if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
            $forbiddenCheckIn1 = '[';
        $forbiddenCheckIn = str_replace("[", "", $forbiddenCheckIn);
        // $forbiddenCheckIn = str_replace(",", "", $forbiddenCheckIn);
         // echo "Count VAr :".count(explode(",",str_replace("[","",$forbiddenCheckIn)));
        // echo "Checkedin : ".$forbiddenCheckIn."<br>";
        foreach(explode(",",str_replace("[","",$forbiddenCheckIn)) as $row){
            if($row == "," || $row == "" || $row == "]"){
                continue;
            }
            // echo "<pre>";
            //     print_r($row);
            // echo "</pre>";
            $forbiddenCheckIn1 .= "$row,";

        }
        $forbiddenCheckIn1 = trim($forbiddenCheckIn1,",");
        // $forbiddenCheckIn1 .= '"2019-08-17"';
        $forbiddenCheckIn1 .= ']';
        // echo "<Br>".$forbiddenCheckIn1."<br>";;
        // var_dump($forbiddenCheckIn);
        $forbiddenCheckIn = $forbiddenCheckIn1;

        $forbiddenCheckOut1 = '[';
        $forbiddenCheckOut = str_replace("[", "", $forbiddenCheckOut);
        // $forbiddenCheckOut = str_replace(",", "", $forbiddenCheckOut);
         // echo "Count VAr :".count(explode(",",str_replace("[","",$forbiddenCheckOut)));
        // echo "Checkedin : ".$forbiddenCheckOut."<br>";
        foreach(explode(",",str_replace("[","",$forbiddenCheckOut)) as $row){
            if($row == "," || $row == "" || $row == "]"){
                continue;
            }
            // echo "<pre>";
            //     print_r($row);
            // echo "</pre>";
            $forbiddenCheckOut1 .= "$row,";

        }
        $forbiddenCheckOut1 = trim($forbiddenCheckOut1,",");
        // $forbiddenCheckOut1 .= '"2019-08-17"';
        $forbiddenCheckOut1 .= ']';
        // echo "<Br>".$forbiddenCheckOut1."<br>";;
        // var_dump($forbiddenCheckOut);
        $forbiddenCheckOut = $forbiddenCheckOut1;
         }
         echo "<input type='hidden' value='".$_SERVER['REMOTE_ADDR']."' id='active_ip'>";
        return View('rentaldetail', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut', 'wishProperty', 'rating'));
    }

    public function viewRentalsPaid($propertyId) {
        $propertyObj = $this->propertyRepository->getByIdActive($propertyId);
        //return $propertyObj;
        $calendarObj = Booking::bookingForProperty($propertyId);
        $rating = $this->calculateRating($propertyObj);
        $wishProperty = array();
        if (\Auth::user()) {
            $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
        }
        list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
        return View('rentaldetailpaid', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut', 'wishProperty', 'rating'));
    }

    public function schoolhouse($userId) {

//        $propertyObj = Property::query()
//                    ->select('property.*', 'user_paid_services.house_name', 'user_paid_services.house_slogan', 'user_paid_services.house_desc', 
//                            'user_paid_services.host_preference', 'user_paid_services.smoking', 'user_paid_services.time_limits',
//                            'user_paid_services.key_given', 'user_paid_services.guest_table')
//                    ->leftjoin('user_paid_services', 'user_paid_services.user_id', '=', 'property.user_id')
//                    ->where('property.user_id', $userId)->get()->first();
        //echo '<pre>';print_r($query );die;


        /* $propertyObj = $this->propertyRepository->getByIdActive($propertyId);
          //return $propertyObj;
          $calendarObj = Booking::bookingForProperty($propertyId);
          $rating = $this->calculateRating($propertyObj);
          $wishProperty = array();
          if (\Auth::user()) {
          $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
          }
          list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj); */
        //return View('rentaldetailpaid', compact('propertyObj', 'forbiddenCheckIn', 'forbiddenCheckOut', 'wishProperty', 'rating'));
          
        // this holds the data , and on view p[age we can get property/details using this object.
        $userObj = User::with("user_paid_property")->where("id", $userId)->first();
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        //     $userObj = User::with(["user_paid_property",'properties','user_guest_rooms'])->where("id", $userId)->first();
        //     // echo "<pre>";
        //     //     print_r($userObj);
        //     // echo "</pre>";
        //     // exit;
        // // "property_status" => "paid", "property_type" => "26"
        // }
        
        $propertyObj = isset($userObj->user_paid_property) ? $userObj->user_paid_property : "";
        if(empty($propertyObj)){
            $propertyObj = Property::where("user_id",$userId)->first();    
        }
        $property_id = isset($propertyObj->id) ? $propertyObj->id : "";
        $forbiddenCheckIn = "";
        $forbiddenCheckOut = "";
        /*echo "<pre>";
            print_r($propertyObj);
        echo "UserId :".\Auth::user()->id."<hr> PropertyID : ".$property_id;die;*/

        //fc_rentalsenquiry
        if (!empty($property_id)) {
            $calendarObj = Booking::bookingForProperty($property_id);
            list($forbiddenCheckIn, $forbiddenCheckOut) = $this->prepareRestrictedDays($calendarObj);
           
        }



        $isOtherPropertyAvailable = Property::with(array("propertytype"=>function($query){
            $query->where('listing_type','paid');

            $query->where('status',1);
        },"propertyimages"))->where('user_id',$userId)->whereIn('property_type',array('24','25','27','28','29','30'))->count();

       if($_SERVER['REMOTE_ADDR'] == '93.42.44.112' || $_SERVER['REMOTE_ADDR'] == "93.33.18.29" || $_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
        
            // echo "Prop ID :".$property_id." User Property OBj <pre>";
            //     print_r($propertyObj1);
            //     // echo "<br> <hr> <br>";
            //     // print_r($userObj);
            // echo "</pre>";
            // exit;
        }
        
//        return $userObj->user_paid_property;
        return View('schoolhousepaid', compact('propertyObj', 'userObj', 'forbiddenCheckIn', 'forbiddenCheckOut','isOtherPropertyAvailable'));
    }

    public function calculateRating($propertObj) {

        $maxNumberOfStars = 5; // Define the maximum number of stars possible.
        $totalRating = 0;
        $total_count = 0;
        $total_stars = 0;
        if (isset($propertObj->reviews) && $propertObj->reviews->count() > 0) {
            $totalRating = $propertObj->reviews->sum('rateVal');
            $total_count = $propertObj->reviews->count();
            $total_stars = $total_count * $maxNumberOfStars;
        }
//        echo "totla rating = ".$totalRating."<br />"; 
//        echo "totla count = ".$total_count."<br />"; 
//        echo "totla stars = ".$total_stars."<br />"; 
//        echo "review count = ".$propertObj->reviews->count()."<br />"; 
        // Calculate the total number of ratings.
        $avg = 0;
        $total_rating = 0;
        //if ($propertObj->reviews->count() > 0) {
        if ($total_count > 0) {
            //$avg = (int) ($totalRating / $total_stars) * $maxNumberOfStars;
            $avg = ($totalRating / $total_stars) * 100;
            $total_rating = $totalRating / $total_count;
            $total_rating = ceil($total_rating);
        }
//        echo "totla rating = ".$total_rating."<br />"; 
//        echo "avg rating = ".$avg; exit;
        return $total_rating;
    }

    public function prepareRestrictedDays1($calendarObj) {
        $forbiddenCheckIn = '[]';
        $forbiddenCheckOut = '[]';
        $DisableCalDate = "";
        $prev = "";
        $all_dates = array();
        $selected_dates = array();
        if ($calendarObj->count() > 0) {
            foreach ($calendarObj as $CRow) {
                // $DisableCalDate .= '"' . $CRow->the_date . '",';
                $DisableCalDate .= '"' . date("Y-m-d",strtotime($CRow->the_date)) . '",';
            }
            $forbiddenCheckIn = '[' . $DisableCalDate . ']';
        }
        foreach ($calendarObj as $date) {
            $all_dates[] = trim($date->the_date);
            $date1 = new \DateTime(trim($date->the_date));
            $date2 = new \DateTime($prev);
            $diff = $date2->diff($date1)->format("%a");
            if ($diff == '1') {
                $selected_dates[] = trim($date->the_date);
            }
            $prev = trim($date->the_date);
            $DisableCalDate = '';
            foreach ($all_dates as $CRow) {
                $DisableCalDate .= '"' . $CRow . '",';
            }
            $forbiddenCheckIn = '[' . $DisableCalDate . ']';
            $DisableCalDate = '';
            foreach ($selected_dates as $CRow) {
                $DisableCalDate .= '"' . $CRow . '",';
            }
            $forbiddenCheckOut = '[' . $DisableCalDate . ']';
        }

        return array($forbiddenCheckIn, $forbiddenCheckOut);
    }
    public function prepareRestrictedDays($calendarObj) {
        //this function will return all the booked dates checkin and checkout by property id, please show me in browser? or yediss pin ldetails page first, th eon here then DB to complete cycle okay
        $forbiddenCheckIn = '[]';
        $forbiddenCheckOut = '[]';
        $DisableCalDate = "";
        $prev = "";
        $all_dates = array();
        $selected_dates = array();
        if ($calendarObj->count() > 0) {
            foreach ($calendarObj as $CRow) {
                if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                    $DisableCalDate .= '"' . date("Y-m-d H:i:s",strtotime($CRow->the_date)) . '",';                   
                }else{
                    $DisableCalDate .= '"' . date("Y-m-d",strtotime($CRow->the_date)) . '",';                    
                }
            }
            $forbiddenCheckIn = '[' . $DisableCalDate . ']';
        }
        // echo "<pre>";
        //     print_r($calendarObj);
        // echo "</pre>";
        // exit;
        foreach ($calendarObj as $date) {
            if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
                $all_dates[] = date("Y-m-d H:i:s",strtotime(trim($date->the_date)));
            }else{
                $all_dates[] = date("Y-m-d",strtotime(trim($date->the_date)));            
            }

            if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
                $date1 = new \DateTime(date("Y-m-d H:i:s",strtotime(trim($date->the_date))));
            }else{
                $date1 = new \DateTime(date("Y-m-d",strtotime(trim($date->the_date))));
            }

            $date2 = new \DateTime($prev);
            // echo "<pre>";
            //     echo "<br> Pre Date :".$prev."<br>";
            //     echo "<br> Date 1 : <br>";
            //     print_r($date1);
            //     echo "<br> ---------------------------- <br>";
            //     echo "<b> DAte 2 </b>";
            //     echo "<br> Date2 :<br>";
            //     print_r($date2);
            // echo "</pre>";
            // echo " <br>  <hr> <br> ";


            $diff = $date2->diff($date1)->format("%a");
            // echo "<br> Daet Diff L:".$diff."<br>";

            if ($diff == '1') {
                if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' ||  $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
                    $selected_dates[] = date("Y-m-d H:i:s",strtotime(trim($date->the_date)));
                }else{
                    $selected_dates[] = date("Y-m-d",strtotime(trim($date->the_date)));
                }
            }
            if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' ||  $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
                $prev = date("Y-m-d H:i:s",strtotime(trim($date->the_date)));
            }else{
                $prev = date("Y-m-d",strtotime(trim($date->the_date)));                
            }
            $DisableCalDate = '';
            foreach ($all_dates as $CRow) {
                if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' ||  $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
                    $DisableCalDate .= '"' . date("Y-m-d H:i:s",strtotime($CRow)) . '",';
                }else{
                    $DisableCalDate .= '"' . date("Y-m-d",strtotime($CRow)) . '",';                    
                }
            }
            $forbiddenCheckIn = '[' . $DisableCalDate . ']';
            $DisableCalDate = '';
            // echo "SElected Dates <pre>";
            //     print_r($selected_dates);
            // echo "</pre> <br> ";

            foreach ($selected_dates as $CRow) {
                if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' ||  $_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
                    $DisableCalDate .= '"' . date("Y-m-d H:i:s",strtotime($CRow)) . '",';
                }else{
                    $DisableCalDate .= '"' . date("Y-m-d",strtotime($CRow)) . '",';                    
                }
            }
            $forbiddenCheckOut = '[' . $DisableCalDate . ']';
        }


        // echo "<pre>";
        // echo "<br> All Dates START HERE <br>";
        //     print_r($all_dates);
        //     echo "<br> All Dates END HERE <br>";
        //     print_r($forbiddenCheckIn);
        //     echo "<br>";
        //     print_r($forbiddenCheckOut);
        // echo "</pre>";
        // exit;

        return array($forbiddenCheckIn, $forbiddenCheckOut);
    }    

    public function getlistpopup(Request $request) {
        $userObj = $this->userRepository->findUser(\Auth::user()->id);
        $propertyId = $request->get('listid');
        $wishlistString = "";
        foreach ($userObj->wishlists as $wish) {
            $exists = $wish->properties->contains($propertyId);
            if ($exists) {
                $wishlistString .= '<li class="bg_white padding10 wishli"><p>' . $wish->name . '</p><div style="float:right;margin-top: -23px;"><i class="fa fa-heart-o whitehrt redhrt" id="' . $wish->id . '"></i></div></li>';
            } else {
                $wishlistString .= '<li class="bg_white padding10 wishli"><p>' . $wish->name . '</p><div style="float:right;margin-top: -23px;"><i class="fa fa-heart-o whitehrt" id="' . $wish->id . '"></i></div></li>';
            }
        }
        $propertyObj = $this->propertyRepository->getCurrentObject($propertyId);
        $imgURL = "";
        foreach ($propertyObj->propertyimages as $img) {
            $imgURL = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
        }
        return $wishlistString . "*****" . $imgURL;
    }

    public function updateSearch(Request $request) {
        $location = $request->get('location');
        list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($location);
        $checkin = $request->get('checkinDate');
        $checkout = $request->get('checkoutDate');
        \Session::put('checkin', $checkin);
        \Session::put('checkout', $checkout);
        $query = Property::query();
        $query->whereBetween('pro_lat', [$minLat, $maxLat]);
        $query->whereBetween('pro_lon', [$minLong, $maxLong]);

        if ($this->checkNotEmpty($checkin) && $this->checkNotEmpty($checkout)) {
            $newDateStart = date("Y-m-d", strtotime($checkin));
            $newDateEnd = date("Y-m-d", strtotime($checkout));
            $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);
            if (!empty($propertyIdRestriced)) {
                $query->whereNotIn('id', $propertyIdRestriced);
            }
        }
        $wishProperty = array();
        if (\Auth::user()) {
            $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
        }
        $searchPropertyObj = $query->where('status', 1)->orderBy('created_at', 'desc')->get();
        $searchView = View('rentals.searchresult', compact('searchPropertyObj', 'wishProperty', 'blat', 'blng'));
        return $searchView;
    }

    public function checkNotEmpty($value) {
        if (trim($value) == "") {
            return false;
        }
        return true;
    }

    public function returnWishlistPropertyArray($userId) {
        $usrObj = $this->userRepository->findUser($userId);
        $wishArray = array();
        if (!is_null($usrObj->wishlists)) {
            $property = array();
            foreach ($usrObj->wishlists as $wish) {
                foreach ($wish->properties as $pr) {
                    array_push($wishArray, $pr->id);
                }
            }
            //print_r($property);exit;
        }
        return $wishArray;
    }

    public function searchDestination($name, $lat, $lon) {
        $location = $name;
        $checkin = "";
        $checkout = "";
        $min_occupancy = "";
        $prtype = "";
        $location = $name;
        list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($location);
        $wishProperty = array();
        if (\Auth::user()) {
            $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
        }
        //echo $minLat.",". $minLong.",". $maxLat.",". $maxLong;exit;
        $query = Property::query();
        $query->whereBetween('pro_lat', [$minLat, $maxLat]);
        $query->whereBetween('pro_lon', [$minLong, $maxLong]);
        $searchPropertyObj = $query->where('status', 1)->orderBy('created_at', 'desc')->get();
        $propertyTypes = $this->propertyTypeRepository->getAll(true);
        $roomTypes = $this->roomTypeRepository->getAll(true);
        return View('search', compact('propertyTypes', 'roomTypes', 'searchPropertyObj', 'checkin', 'checkout', 'min_occupancy', 'location', 'prtype', 'wishProperty', 'blat', 'blng'));
    }

    public function search($name, Request $request) {
        $lat = $request->get('lat');
        $lon = $request->get('long');
        $checkin = $request->get('checkin');
        $checkout = $request->get('checkout');
        \Session::put('checkin', $checkin);
        \Session::put('checkout', $checkout);
        $location = $name;
        /* Googe Address */
        list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($name);
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        // echo '<pre>'; print_r($_REQUEST);echo "$minLat, $minLong, $maxLat, $maxLong, $blat, $blng; ";die();
        //     // echo " User Property Obj <pre>";
        //     //     print_r($userObj);
        //     // echo "</pre>";
        //     exit;
        // }
        /* Querying */
        $query = Property::query();
        $query->whereBetween('pro_lat', [$minLat, $maxLat]);
        $query->whereBetween('pro_lon', [$minLong, $maxLong]);
        $newDateStart = date("Y-m-d", strtotime($checkin));
        $newDateEnd = date("Y-m-d", strtotime($checkout));
        $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);
        if (!empty($propertyIdRestriced)) {
            $query->whereNotIn('id', $propertyIdRestriced);
        }
        $searchPropertyObj = $query->where('status', 1)->orderBy('created_at', 'desc')->get();
        //return $searchPropertyObj[0]->pricing;
        $propertyTypes = $this->propertyTypeRepository->getAll(true);
        $roomTypes = $this->roomTypeRepository->getAll(true);
        $wishProperty = array();
        if (\Auth::user()) {
            $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
        }
        return View('search', compact('propertyTypes', 'roomTypes', 'searchPropertyObj', 'checkin', 'checkout', 'min_occupancy', 'location', 'prtype', 'wishProperty', 'blat', 'blng'));
    }

    public function searchBeds(Request $request) {
        $checkin = $request->get('checkinDate');
        $checkout = $request->get('checkoutDate');

        \Session::put('checkin', $checkin);
        \Session::put('checkout', $checkout);
        return redirect()->back();
    }

    // mysearch: search_WorkAndUsersListingPaid
    public function search2($name, Request $request) {
        // to get all categories id: which are unpaid / 
        ///Jitenodra /  we are using ajaxsearch function to call data and  to filter. 

        /* $lat = $request->get('lat');
          $lon = $request->get('long');
          $checkin = $request->get('checkin');
          $checkout = $request->get('checkout');
          \Session::put('checkin', $checkin);
          \Session::put('checkout', $checkout);
          $location = $name; */
        /* Googe Address */
        //list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($name);
//        echo '<pre>';
//        print_r($_REQUEST);
//        echo "$minLat, $minLong, $maxLat, $maxLong, $blat, $blng; ";
//        die();
        /* Querying */
        /* $query = Property::query();
          $query->select("property.*");
          $query->join("users", "users.id", "=", "property.user_id");
          $query->leftjoin("user_paid_services", "property.user_id", "=", "user_paid_services.user_id");


          $newDateStart = date("Y-m-d", strtotime($checkin));
          $newDateEnd = date("Y-m-d", strtotime($checkout));
          $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);
          if (!empty($propertyIdRestriced)) {
          // here is the code where we ignore the propertyids , if exists in booking table
          $query->whereNotIn('property.id', $propertyIdRestriced);
          }
          $propStatus = array();
          if ($request->get('search')) {
          $searchType = explode(',', $request->get('search'));
          foreach ($searchType as $search) {
          if ($search == 'work') { // for work : unpaid or unpaidflash
          array_push($propStatus, "unpaid");
          array_push($propStatus, "unpaidflash");
          } else { // for live, learn, enjoy : paid
          array_push($propStatus, "paid");
          }
          }
          $query->whereIn('property.property_status', $propStatus);
          }
          if (!empty($minLat) && !empty($maxLat) && !empty($minLong) && !empty($maxLong)) {
          if (in_array("paid", $propStatus)) {
          //if
          $query->whereRaw('IF (property.addr_work_org_same = 0, '
          . '(property.pro_lat BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (property.pro_lon BETWEEN ' . $minLong . ' AND ' . $maxLong . '),'
          // if the house property have different location means
          . 'IF (user_paid_services.house_property_differ = 1, '
          . '(user_paid_services.proplatbox BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (user_paid_services.proplonbox BETWEEN ' . $minLong . ' AND ' . $maxLong . '), '
          . 'IF (user_paid_services.house_property_differ = 0, '
          . '(user_paid_services.houselatbox BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (user_paid_services.houselonbox BETWEEN ' . $minLong . ' AND ' . $maxLong . '), '
          . '(users.org_lat BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (users.org_lan BETWEEN ' . $minLong . ' AND ' . $maxLong . ')'
          . ' )'
          . ' )'
          . ' )');
          } else {
          $query->whereBetween('property.pro_lat', [$minLat, $maxLat]);
          $query->whereBetween('property.pro_lon', [$minLong, $maxLong]);
          }
          }

          $searchPropertyObj = $query->where('property.status', 1)->orderBy('property.created_at', 'desc')->groupBy('property.user_id')->get(); */
        //return $searchPropertyObj;
//        $query = Property::query();
//        $typesArr = [];
//
//        $searchListingType = ($name == 'work') ? 'unpaid' : 'paid';
//
//        $getPropertyTypes = $this->propertyTypeRepository->getByType($searchListingType);
//        foreach ($getPropertyTypes as $row) {
//            $typesArr[] = $row->id;
//        }
//
//        $searchPropertyObj = $query->whereIn('property_type', $typesArr)->orderBy('created_at', 'desc')->get();
        // To get user accepted job
//        $checkin = $request->get('checkin');
//        $checkout = $request->get('checkout');
//        \Session::put('checkin', $checkin);
//        \Session::put('checkout', $checkout);
        $getUserAcceptedJob = '';
        if (\Auth::user()) {
            //$wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
            //fc_rentalsenquiry
            $userid = \Auth::user()->id;
            $getUserAcceptedJob = RentalsEnquiry::select('property.exptitle', 'property.title', 'fc_rentalsenquiry.user_id', 'fc_rentalsenquiry.prd_id')
                            ->leftjoin('property', 'property.id', '=', 'fc_rentalsenquiry.prd_id')
                            ->where('fc_rentalsenquiry.user_id', $userid)->whereIn('property.property_status', ['unpaid', 'unpaidflash'])->where('approval', 'Accept')->orderBy('fc_rentalsenquiry.id', 'desc')->get()->first();
           // echo '<pre>';print_r($getUserAcceptedJob);die; 
        }
//        return $getUserAcceptedJob;
        //return View('search2', compact('searchPropertyObj', 'getUserAcceptedJob'));
        return View('search2', compact('getUserAcceptedJob'));
    }

    // this is the request from search page, when anything (checkboxes(live,work etc), address, input (start date, end date)) changes
    public function ajaxsearch(Request $request) {
        
        $location = $request->get('location');
        //why are we limiting with bounds the Google API_, its because we are filtering the results for particular areaor address
        //fine but it is not answering the point of Nembro not showing in Italyunds provide the north and south
        //here we are getting the bounds of area using user address type in search box
        list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($location);
        //echo '<pre>'; print_r($_REQUEST);echo "$minLat, $minLong, $maxLat, $maxLong, $blat, $blng; ";die();
        $checkin = $request->get('checkinDate');
        $checkout = $request->get('checkoutDate');
        if($_SERVER['REMOTE_ADDR'] == '79.106.209.91'){
            // echo "<br> Min Lat :".$minLat." >>  Min Long :".$minLong." >> MAx Lat :".$maxLat." >>> <Ax Long :".$maxLong."<br>";
        }

        \Session::put('checkin', $checkin);
        \Session::put('checkout', $checkout);

//there has been no coding apart from yours about voucher, problem is due to uploads you did ear;lier today
//there will be no problem let me fix this i have shown the call for testing

        $query = Property::query();
        //$query->whereBetween('pro_lat', [$minLat, $maxLat]);
        //$query->whereBetween('pro_lon', [$minLong, $maxLong]);
        // to check if we get both dates (start and end) then will check
        if ($this->checkNotEmpty($checkin) && $this->checkNotEmpty($checkout)) {
            $newDateStart = date("Y-m-d", strtotime($checkin));
            $newDateEnd = date("Y-m-d", strtotime($checkout));
            $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);


            if (!empty($propertyIdRestriced)) {
                $query->whereNotIn('property.id', $propertyIdRestriced);
            }
        }
        /* $wishProperty = array();
          if (\Auth::user()) {
          $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
          } */

        // to filter data : property status: paid, unpaid, unpaidflash., if not any checked then query will be fine, that's why we first checked if(val).
        $propStatus = [];
        if ($request->get('search')) {
            $searchType = explode(',', $request->get('search'));

            foreach ($searchType as $search) {
                if ($search == 'work') { // for work : unpaid or unpaidflash
                    $propStatus[] = 'unpaid';
                    $propStatus[] = 'unpaidflash';
                } else { // for paid. paid.type live, learn, enjoy will be managed in future
                    $propStatus[] = 'paid';
                }
            }
            $query->whereIn('property.property_status', $propStatus);
        }

        if($_SERVER['REMOTE_ADDR'] == '79.106.209.91'){
            // echo "<br> Search Type :<pre>";
            // print_r($request->get('search'));
            // echo "</pre><br> ";
        }



        //$searchPropertyObj = $query->where('status', '1')->whereIn('property_status', $propStatus)->orderBy('created_at', 'desc')->get();We used the property_status just above, because if in case there is no checkbox checked for (live, work etc), the query must not break.
        //$searchPropertyObj = $query->where('status', '1')->orderBy('created_at', 'desc')->get(); // commented out because, we now added/join another table data:user_paid_services, to get paid title desc.
        // Here we joined the table property with user_paid_services, so show the details(title, desc) : according : paid, unpaid
        $query->select('property.*', 'user_paid_services.house_slogan', 'user_paid_services.house_desc', 'user_paid_services.home_type')
                ->leftjoin('user_paid_services', 'user_paid_services.user_id', '=', 'property.user_id')
                ->where('property.status', '1');
        if (in_array('paid', $propStatus)) {
            //if the paid type then lat long search will be in both: property and user.pad.services
            //here we are using the bounds latlongs
            $query->where(function ($query) use ($minLat, $maxLat, $minLong, $maxLong) {
                $query->where(function ($query) use ($minLat, $maxLat, $minLong, $maxLong) {
                    $query->whereBetween('pro_lat', [$minLat, $maxLat]);
                    $query->whereBetween('pro_lon', [$minLong, $maxLong]);
                })->orWhere(function ($query) use ($minLat, $maxLat, $minLong, $maxLong) {
                    $query->whereBetween('user_paid_services.houselatbox', [$minLat, $maxLat]);
                    $query->whereBetween('user_paid_services.houselonbox', [$minLong, $maxLong]);
                });
            });
        } else {
            //if the unpaid : seach only property table for latlong.
            $query->whereBetween('pro_lat', [$minLat, $maxLat]);
            $query->whereBetween('pro_lon', [$minLong, $maxLong]);
        }


        

        $searchPropertyObj = $query->orderBy('property.created_at', 'desc')->get();
        $profile_noimage = 'no_prop_image.png';
        if($_SERVER['REMOTE_ADDR'] == '79.106.209.91'){
             // echo "<pre>";
                //print_r($searchPropertyObj);
                // echo "Property OBj";
                // print_r($request->get('search'));
                // echo "<br> SEARCH <br>";
                // print_r($propStatus);
                // echo "<br> PROP STATUS <br>";
                //     print_r($propertyIdRestriced);
                // echo "</pre>";
        }
        // if($_SERVER['REMOTE_ADDR'] == '93.36.118.59'){
        //     echo "Min Lat :".$minLat." MAX Lat :".$maxLat."<br> Min Lon :".$minLong. " MAx Long :".$maxLong."<br>";
        //         echo "<pre>";
        //         print_r($searchPropertyObj);
        //         echo "Property OBj";
        //         print_r($request->get('search'));
        //         echo "<br> SEARCH <br>";
        //         print_r($propStatus);
        //         echo "<br> PROP STATUS <br>";
        //             print_r($propertyIdRestriced);
        //         echo "</pre>";
        //         // exit;
        //     }
        
        //$query->whereBetween('user_paid_services.houselatbox', [$minLat, $maxLat]);
        //$query->whereBetween('user_paid_services.houselonbox', [$minLong, $maxLong]);
        /* var_dump($query->toSql());
          $bindings = $query->getBindings();
          print_r($bindings );
          die; */

        $wishProperty = array();
        if (\Auth::user()) {
            $wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
        }
        $timeFromPaidToWork = array(); // time from paid to work 

        if (\Auth::user()) {
            //$wishProperty = $this->returnWishlistPropertyArray(\Auth::user()->id);
            $userid = \Auth::user()->id;
            $getAddressTime = RentalsEnquiry::select('distancehomework.time', 'distancehomework.paidPropId', 'fc_rentalsenquiry.prd_id')
                            ->leftjoin('distancehomework', 'distancehomework.unpaidPropId', '=', 'fc_rentalsenquiry.prd_id')
                            ->where('fc_rentalsenquiry.user_id', $userid)->where('approval', 'Accept')->orderBy('fc_rentalsenquiry.id', 'desc')->get();
            //echo "<pre>";print_r($getAddressTime);die;
            foreach ($getAddressTime as $row) {
                $timeFromPaidToWork[$row->paidPropId] = $row->time;
            }
            //$timeFromPaidToWork['143'] = 22;//just to test
        }

        // if($_SERVER['REMOTE_ADDR'] == '31.159.143.22'){
        //    echo "<pre>";
        //    print_r($getAddressTime);
        //    echo "Time Array <br><hr><br>";
        //    print_r($timeFromPaidToWork);
        //    echo "</pre>";
        //    exit;
        // }
//        return $timeFromPaidToWork;
//        echo "<pre>";
//        print_r($timeFromPaidToWork);
//        echo "</pre>";
//        exit;
        // adding original serach results for testing
        $name = $request->get('location');
        $lat = $request->get('lat');
        $lon = $request->get('long');
        $checkin = $request->get('checkinDate');
        $checkout = $request->get('checkoutDate');
        \Session::put('checkin', $checkin);
        \Session::put('checkout', $checkout);
        $location = $name;
        /* Googe Address */
        list($minLat, $minLong, $maxLat, $maxLong, $blat, $blng) = $this->getNorthAndSouthBounds($name);

//        echo '<pre>'; print_r($_REQUEST);
//        echo "min lat = $minLat --, min long = $minLong --, max lat = $maxLat --, max long = $maxLong --, $blat, $blng; ";
//        die();
        /* Querying */
        $query = Property::query();
        $query->select("property.*");
        $query->join("users", "users.id", "=", "property.user_id");
        $query->leftjoin("user_paid_services", "property.user_id", "=", "user_paid_services.user_id");


        $newDateStart = date("Y-m-d", strtotime($checkin));
        $newDateEnd = date("Y-m-d", strtotime($checkout));
        $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);


        if (!empty($propertyIdRestriced)) {
            // here is the code where we ignore the propertyids , if exists in booking table
            $query->whereNotIn('property.id', $propertyIdRestriced);
        }
        $query->whereRaw("(property.property_status = 'unpaid' OR property.property_status = 'unpaidflash')");
        // cfonor unpaid and unpaidflash, condition is wrong, too simplistic
        $query->whereBetween('property.pro_lat', [$minLat, $maxLat]); // this is Listing, propoerty, lat long yes as this we have to change with new condition
        // I thouhgt about logic used and think it is wrong to call both usera nad property tables in search2
        // anyway I really have to see this working
        // as we already sued 3 tables in dearing of paid property, user and user_paid_Services for lat long so we have to add checks
        // for unpaid also those with almost same conditions like if property Check IF Workplace address is different from LegalAddress
        // than get from different lat longs and if not check than from user but let me check the update code first
        // sure, thought we had done this time ago yes but for paid services as we have to add this on unpiad but we shift to some other work i thing
        // yes remember we shift to live the counts are not showing properly and not the school housess so trying to fix that and forget about this
        // let me work on this. k

        
        $query->whereBetween('property.pro_lon', [$minLong, $maxLong]);
        if($_SERVER['REMOTE_ADDR'] == '79.106.209.91'){
            // $query = str_replace(array('?'), array('\'%s\''), $query->toSql());
            // $sql = $query->toSql();
            // $bindings = $query->getBindings();
            // echo "<pre>";
            //     print_r($sql);
            //     print_r($bindings);
            // echo "</pre>";
            // $query = vsprintf($query, $query->getBindings());
            // dump($query);
            // echo "Query Print :".$query->toSql();
        }
         $query->where('property.status', 1)->orderBy('property.created_at', 'desc');
         //->groupBy('property.user_id'); //added groupby, ->groupBy('property.user_id'), to make double listing disapper from searc2 page, but truely listing booked then shows again *perhaps double entry(
         $unpaidPropertyObj = $query->get();
        $unpaid_count = $unpaidPropertyObj->count();
        // end of unpaid quering
        if($_SERVER['REMOTE_ADDR'] == '2.43.240.216'){
          //  echo $newDateStart."<br>".$newDateEnd."<br>";
           /* echo "<pre>";
                print_R($propertyIdRestriced);
           echo "<br> UNPAID LISTs";
               print_r($unpaidPropertyObj);
           echo "</pre> Unpaid END HERE <br> ";
           exit;*/
        }
        // quering the paid services starts
        $query_paid = Property::query();
        $query_paid->select("property.*");
        $query_paid->join("users", "users.id", "=", "property.user_id");
        $query_paid->leftjoin("user_paid_services", "property.user_id", "=", "user_paid_services.user_id");


        $newDateStart = date("Y-m-d", strtotime($checkin));
        $newDateEnd = date("Y-m-d", strtotime($checkout));
        $propertyIdRestriced = $this->checkAvailablityOfRentals($newDateStart, $newDateEnd);
//        print_r($propertyIdRestriced); exit;
      //  if($_SERVER['REMOTE_ADDR'] != '2.43.240.216'){
        if (!empty($propertyIdRestriced)) {
            // here is the code where we ignore the propertyids , if exists in booking table
            $query_paid->whereNotIn('property.id', $propertyIdRestriced);
        }
    //}
        if($_SERVER['REMOTE_ADDR'] == '2.43.240.216'){
            // echo "<br>Restrict Id <pre>";
            // print_r($propertyIdRestriced);
            // echo "</pre><br>";

        }
        
        //if($_SERVER['REMOTE_ADDR'] != '2.43.240.216'){
            if (!empty($minLat) && !empty($maxLat) && !empty($minLong) && !empty($maxLong)) {
            $query_paid->whereRaw('IF (property.addr_work_org_same = 0, '
                    . '(property.pro_lat BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (property.pro_lon BETWEEN ' . $minLong . ' AND ' . $maxLong . '),'
                    /* if the house property have different location means */
                    . 'IF ((user_paid_services.house_property_differ = 1) AND (user_paid_services.proplatbox != ""), '
                    . '(user_paid_services.proplatbox BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (user_paid_services.proplonbox BETWEEN ' . $minLong . ' AND ' . $maxLong . '), '
                    . 'IF ((user_paid_services.house_property_differ = 0) AND (user_paid_services.houselatbox != ""), '
                    . '(user_paid_services.houselatbox BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (user_paid_services.houselonbox BETWEEN ' . $minLong . ' AND ' . $maxLong . '), '
                    . '(users.org_lat BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (users.org_lan BETWEEN ' . $minLong . ' AND ' . $maxLong . ')'
                    . ' )'
                    . ' )'
                    . ' )');
        }
        //}
        $query_paid->where("property.property_type", "0");
        $query_paid->whereRaw("(property.property_status = 'paid')");
        $paidPropertyObj = $query_paid->where('property.status', 1)->orderBy('property.created_at', 'desc')->groupBy('property.user_id')->get();
//        $paidPropertyObj = $query_paid->where('property.status', 1)->orderBy('property.created_at', 'desc')->groupBy('property.user_id')->toSql();

        
        if (sizeof($paidPropertyObj) > 0) {
            foreach ($paidPropertyObj as $paidProperty) {
                $check_bed_listing = \App\Property::where("user_id", $paidProperty->user_id)->where(["status" => "1", "property_status" => "paid", "property_type" => "26"])->get();
                if ($check_bed_listing->count() <= 0) {
                    $paidPropertyObj = $paidPropertyObj->except($paidProperty->id);
                }
            }
        }
        $paid_count = $paidPropertyObj->count();

         //if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            // echo '<pre>'; print_r($searchPropertyObj); die(); // to check , what data we are getting from query.
            // echo '<pre>'; print_r($paidPropertyObj)."</pre>";
               // echo "min lat = $minLat --, min long = $minLong --, max lat = $maxLat --, max long = $maxLong --, $blat, $blng; ";
               //  echo " User Property Obj <pre>";
               //      // print_r($userObj);
               //  echo "</pre>";
                // exit;
                // }

        // ends
        
    if($_SERVER['REMOTE_ADDR'] == '2.43.240.216'){
        // echo "Paid Property <pre>";
            // print_r($paidPropertyObj);
        // echo "</pre> End HEre <br> ";
    }

    if($_SERVER['REMOTE_ADDR'] == '93.36.90.36'){
           //  echo "<br> Prop Lat : ".$minLat." ,".$maxLat."<br>";
           //  echo "Prop Long : ".$minLong.",".$maxLong."<br>";
           // echo "<pre>";
           // print_r($_REQUEST);
           // echo "</pre>";
           // exit;
        }

         if($_SERVER['REMOTE_ADDR'] == '79.106.209.91'){
        // echo '<pre>'; 
        //     print_r($unpaidPropertyObj);
        // echo "</pre>";
        }

   // if($_SERVER['REMOTE_ADDR'] != '2.43.240.216'){
        $searchPropertyObj = array();
   // }
        $propStatus = array();
        if ($request->get('search')) {
            $searchType = explode(',', $request->get('search'));
            if (in_array("live", $searchType) && !in_array("work", $searchType)) {
                $searchPropertyObj = $paidPropertyObj;
            } else if (in_array("work", $searchType) && !in_array("live", $searchType)) {
                $searchPropertyObj = $unpaidPropertyObj;
            } else if (in_array("work", $searchType) && in_array("live", $searchType)) {
                //$searchPropertyObj = array_merge($unpaidPropertyObj, $paidPropertyObj);
                $searchPropertyObj = $unpaidPropertyObj->merge($paidPropertyObj);
            }
            //$query->whereIn('property.property_status', $propStatus);
        } else {
            $searchPropertyObj = $unpaidPropertyObj->merge($paidPropertyObj);
        }




        $getUserAcceptedJob = '';
        if (\Auth::user()) {
            $userid = \Auth::user()->id;
            $getUserAcceptedJob = RentalsEnquiry::select('property.exptitle', 'property.title', 'fc_rentalsenquiry.user_id', 'fc_rentalsenquiry.prd_id')
                            ->leftjoin('property', 'property.id', '=', 'fc_rentalsenquiry.prd_id')
                            ->where('fc_rentalsenquiry.user_id', $userid)->whereIn('property.property_status', ['unpaid', 'unpaidflash'])->where('approval', 'Accept')->orderBy('fc_rentalsenquiry.id', 'desc')->get()->first();
        }
        if($_SERVER['REMOTE_ADDR'] == '79.106.209.91'){
        // echo '<pre>'; 
        //     print_r($searchPropertyObj);
        // echo "</pre>";
        }
        return View('partials.searchresultajax', compact('searchPropertyObj', 'paid_count', 'unpaid_count', 'profile_noimage', 'wishProperty', 'timeFromPaidToWork', 'getUserAcceptedJob'));
        /*
          // to get all categories id: which are unpaid
          $typesArr = [];

          $name = 'work';
          $searchListingType = ($name == 'work') ? 'unpaid' : 'paid';

          $getPropertyTypes = $this->propertyTypeRepository->getByType($searchListingType);
          foreach($getPropertyTypes  as $row){
          $typesArr[] =$row->id;
          }
          //$searchPropertyObj = $query->whereIn('property_type', $typesArr)->orderBy('created_at', 'desc')->get();
         */
    }

    public function otherpaidservices($user_id){
    
    // $property = Property::with("propertytype")->where('id',$school_id)->where('propertytype.listing_type','paid')->where('propertytype.status',1)->get();
    //SELECT * FROM `property` as p JOIN `property_type` as pt ON p.property_type = pt.id WHERE p.`property_type` IN ('24','25','27','28','29','30') AND p.user_id = 37 #AND pt.status = 1 AND pt.listing_type='paid'
    // echo "User Id :".$user_id."<br>";
    // $user_id = \Auth::user()->id;
    $propertyObj = Property::with(array("propertytype"=>function($query){
            $query->where('listing_type','paid');

            $query->where('status',1);
        },"propertyimages"))->where('user_id',$user_id)->whereIn('property_type',array('24','25','27','28','29','30'))->get();
    // echo "<br> Property Obj<pre>";
    //     print_r($propertyObj);
    // echo "</pre><br> ";
    // exit;
    // $propertyObj1 = Property::with(array("propertyimages"))->where('user_id',$user_id)->whereIn('property_type',array('0'))->get();
    
    return View('otherpaidservices',compact('propertyObj'));
}

    public function getNorthAndSouthBounds($location) {
        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );
        $google_map_api = "AIzaSyAz0HqeAgq5xNt9DSUSMPIVlmLK_uuAoDc";
        $get_address = str_replace(" ", "", $location);
        $googleAddress = $get_address;
        $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$googleAddress&sensor=false&key=$google_map_api", false, stream_context_create($arrContextOptions));
        $json = json_decode($json);
//        echo "<pre>";
//        print_r($json->{'results'}[0]->{'geometry'}->{'viewport'}->{'southwest'}->{"lng"});
//        exit;
        $newAddress = $json->{'results'}[0]->{'address_components'};
        $minLat = "";
        if (isset($json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lat'})) {
            $minLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lat'};
        } else if (isset($json->{'results'}[0]->{'geometry'}->{'viewport'}->{'southwest'}->{'lat'})) {
            $minLat = $json->{'results'}[0]->{'geometry'}->{'viewport'}->{'southwest'}->{'lat'};
        }
        //$minLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lat'};
        $minLong = "";
        if (isset($json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lng'})) {
            $minLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lng'};
        } else if (isset($json->{'results'}[0]->{'geometry'}->{'viewport'}->{'southwest'}->{"lng"})) {
            $minLong = $json->{'results'}[0]->{'geometry'}->{'viewport'}->{'southwest'}->{"lng"};
        }
        $maxLat = "";
        if (isset($json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lat'})) {
            $maxLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lat'};
        } else if (isset($json->{'results'}[0]->{'geometry'}->{'viewport'}->{'northeast'}->{'lat'})) {
            $maxLat = $json->{'results'}[0]->{'geometry'}->{'viewport'}->{'northeast'}->{'lat'};
        }
        $maxLong = "";
        if (isset($json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lng'})) {
            $maxLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lng'};
        } else if (isset($json->{'results'}[0]->{'geometry'}->{'viewport'}->{'northeast'}->{'lng'})) {
            $maxLong = $json->{'results'}[0]->{'geometry'}->{'viewport'}->{'northeast'}->{'lng'};
        }
        //$minLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'southwest'}->{'lng'};
        //$maxLat = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lat'};
        //$maxLong = $json->{'results'}[0]->{'geometry'}->{'bounds'}->{'northeast'}->{'lng'};
        $bastlat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
        $baselon = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
        return array($minLat, $minLong, $maxLat, $maxLong, $bastlat, $baselon);
    }

    public function checkAvailablityOfRentals($newDateStart, $newDateEnd) {
        $propertyIdRestriced = array();
        $availableCheck = Booking::availabilityCheck($newDateStart, $newDateEnd);
        // if($_SERVER['REMOTE_ADDR'] == '93.36.118.59'){
        //     echo $newDateStart."<br> End Date :".$newDateEnd;
        //         echo "<pre>";
        //             print_r($availableCheck);
        //         echo "</pre>";
        //         exit;
        // }
        if (is_object($availableCheck)) {
            foreach ($availableCheck as $restrick_data) {
                array_push($propertyIdRestriced, $restrick_data->PropId);
            }
        }
        //print_r($propertyIdRestriced);exit;
        return $propertyIdRestriced;
    }

    public function showpricedetail(Request $request) {
        /* $propertyId = $request->get('listid');
          $numberOfDays = $request->get('noofdays');
          $propertyObj = $this->propertyRepository->getCurrentObject($propertyId);
          $total = $propertyObj->price_night * $numberOfDays;
          $commissionObj = $this->commissionRepository->getById(AppConstants::GUEST_COMMISSION, true);
          $commission = 0;
          if ($commissionObj->count() > 0) {
          $commission = $this->commissionRepository->calculateCommission($commissionObj, $total);
          }
          $taxObj = $this->commissionRepository->getById(AppConstants::TAX_COMMISSION, true);
          $tax = 0;
          if ($taxObj->count() > 0) {
          $tax = $this->commissionRepository->calculateCommission($taxObj, $total);
          }

          //propertyprice,totalpricewithoutothercharges,commission,tax
          return $propertyObj->price_night."***".$total."***".$commission."***".$tax; */
        return "true";
    }

    public function instantbook(Request $request) {
        $checkIn = date('Y-m-d H:i:s', strtotime($request->get('sdate')));
        $checkOut = date('Y-m-d H:i:s', strtotime($request->get('edate')));
        $productId = $request->get('listid');
        $propertyObj = $this->propertyRepository->getCurrentObject($productId);
        $renterId = $propertyObj->user->id;
        $noOfNyts = $request->get('days');
        $productPrice = $propertyObj->price_night;
        $serviceFee = $request->get('commissionamount');
        $totalPrice = ($propertyObj->price_night * $noOfNyts) + $serviceFee;
        $noOfGuests = $request->get('guests');
        $msg = "";

        $dataArr = array(
            'checkin' => $checkIn,
            'checkout' => $checkOut,
            'numofdates' => $noOfNyts,
            'serviceFee' => $serviceFee,
            'totalAmt' => $totalPrice,
            'caltophone' => '',
            'enquiry_timezone' => '',
            'user_id' => \Auth::user()->id,
            'renter_id' => $renterId,
            'NoofGuest' => $noOfGuests,
            'prd_id' => $productId
        );


        $booking_status = array(
            'booking_status' => 'Accept'
        );

        $dataArr = array_merge($dataArr, $booking_status);
        $enquiryObj = RentalsEnquiry::create($dataArr);
        $insertid = $enquiryObj->id;

        if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
            $val = 10 * $insertid + 8;
            $val = 1500000 + $val;
            $bookingno = "EN" . $val;
            $newdata = array(
                'Bookingno' => $bookingno
            );
            RentalsEnquiry::where('id', $insertid)->update($newdata);
        }

        $dataArr = array(
            'productId' => $productId,
            'bookingNo' => $bookingno,
            'senderId' => \Auth::user()->id,
            'receiverId' => $renterId,
            'subject' => 'Booking Request : ' . $bookingno,
            'message' => ""
        );
        MedMessage::create($dataArr);
        $dataArr = array(
            'productId' => $productId,
            'bookingNo' => $bookingno,
            'senderId' => $renterId,
            'receiverId' => \Auth::user()->id,
            'subject' => 'Booking Request : ' . $bookingno,
            'message' => ""
        );
        MedMessage::create($dataArr);
        \Session::put('bookingno', $bookingno);
        return $bookingno;
    }

    public function checkout() {
        $enquiryObj = RentalsEnquiry::where('Bookingno', \Session::get('bookingno'))->first();
        $propertyObj = $this->propertyRepository->getByIdActive($enquiryObj->prd_id);
        return View('bookingresult', compact('enquiryObj', 'propertyObj'));
    }

    public function getEnquiryDates($enquiresObj, $checkIn, $checkOut) {
        $enquiryDates = array();
        $count = 0;
        foreach ($enquiresObj as $enq) {
            $dates = $this->getDatesFromRange($enq->checkin, $enq->checkout);
            $i = 1;
            $dateMinus1 = count($dates);
            foreach ($dates as $date) {
                if ($i <= $dateMinus1) {
                    array_push($enquiryDates, $date);
                }
                $i++;
            }
            if (in_array($checkIn, $enquiryDates) || in_array($checkOut, $enquiryDates)) {
                $count++;
            }
        }
        return $count;
    }

    public function sendBookingEmails($availableCheck = 1, $propertyObj = array(), $enquiryObj = array(), $condition = "", $hr_email = "", $worktutoremail = "", $organizationemail = "") {
        if($_SERVER['REMOTE_ADDR'] == '93.36.17.217'){
            // echo "Count :".count($propertyObj)."<br> SizeOf :".sizeof($propertyObj);
            // echo "Property Id :".$propertyObj->id."<br> Enq Id :".$enquiryObj->id."<br>";
            // echo "<pre>";
            //     print_r($enquiryObj);
            //     // print_r($propertyObj);
            // echo "</pre>";
            //     exit;
        }
         // if ((!empty($propertyObj) && sizeof($propertyObj)) && (!empty($enquiryObj) && sizeof($enquiryObj))) {
         if ((!empty($propertyObj) && isset($propertyObj->id)) && (!empty($enquiryObj) && isset($enquiryObj->id))) {
            $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();
            $user = User::where("id", \Auth::user()->id)->select("id", "email", "name")->first();
            // checking if the request is for bed .type or not
            if (isset($propertyObj->property_type) && $propertyObj->property_type == "26") {
                //if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                    //if (isset($renter_user->email) && !empty($renter_user->email)) {
                $this->mailNotificationsForUser->sendPaidTypeSchoolHouseBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                $this->mailNotificationsForUser->sendPaidTypeSchoolHouseUserBookingEmail($user->email, $organizationemail, $propertyObj, $user, $enquiryObj);
                   // }
                //}
            }
            // else for reqular email system which is runnig before / Olfuser  first listing not showing in user-s @your listing@
            else {
                if (!empty($condition) && $condition == "hr") {
                    if ($availableCheck == 0) {
                        //checking if user have its first choice
                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                            //$this->mailNotificationsForUser->sendPaidHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                        } else {


                            $this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $enquiryObj);
                        }
                    } else {
                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                        } else {
                            // if($_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
                            //     echo "HR Email IN ELSE IF ELSE:".$hr_email."<br>";
                            //     exit;
                            // }
                            $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$worktutoremail);
                        }
                    }
                }
                // second codition
                else if (!empty($condition) && $condition == "no_hr") {
                    if ($availableCheck == 0) {
                        if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                            if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                                //$this->mailNotificationsForUser->sendPaidBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                            } else {
                                $this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                            }
                        } else {
                            if (isset($propertyObj->renter_id)) {
                                if (isset($renter_user->email) && !empty($renter_user->email)) {
                                    if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                                        $this->mailNotificationsForUser->sendPaidUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                                    } else {
                                        $this->mailNotificationsForUser->sendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                                    }
                                }
                            }
                        }
                    } else {
                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
                            //$this->mailNotificationsForUser->sendPaidBookingNotification($propertyObj, $enquiryObj);
                        } else {
                            // $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj);
                            $requested_user = User::where("id", $enquiryObj->user_id)->select("id", "email", "name")->first();
                                // $requestedUseremail = $requested_user->email;
                                $renterEmail = $renter_user->email;
                                // if checkbox @Work Tutor NOT YOU Flagged the BD value work?org?same will be 0
                                // echo "<br> Work Org Same :".$propertyObj->work_org_same."<br>";
                            if($propertyObj->work_org_same == 0){
                                $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$worktutoremail,$renterEmail);
                                // $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$requestedUseremail);
                            }else{
                               
                               // echo "<br> Requested Email ID :".$renterEmail;
                               // echo "<br> Requesteing Email Id :".$requestedUseremail;
                               //  $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$requestedUseremail);
                                $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj,$renterEmail);
                            }
                        }
                    }
                }
            }
        }
    }

    public function request_booking(Request $request) {
        $userId = \Auth::user()->id;
        $userEnquiries = RentalsEnquiry::where('user_id', $userId)->get();
        // $checkIn = date('Y-m-d H:i:s', strtotime($request->get('sdate')));
        // $checkOut = date('Y-m-d H:i:s', strtotime($request->get('edate')));
        $checkIn = date('Y-m-d H:i:s', strtotime($request->get('sdate')." 11:00:00"));
        $checkOut = date('Y-m-d H:i:s', strtotime($request->get('edate')." 10:00:00"));
        $allowBooking = 0;
        $productId = $request->get('listid');
        $propertyObj = $this->propertyRepository->getCurrentObject($productId);
        //$countEnquiry = 0;
        $settingsObj = PropertySettings::propertySettings();
        $user_voucher = User::where('id', $userId)->first();
        // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
        //     echo "<pre>";
        //         print_r($propertyObj->property_status);
        //         // print_r($user_voucher);
        //     echo "</pre>";
        //     // exit;            
        // }

        $countEnquiry = 0;
        $allowedEnquiryCount = 0;
        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
            $is_paid = 1;
            $user_paid_request_counts = RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $userId)
                    ->where("property_status", "paid")
                    ->get();
            //$countEnquiry = $user_paid_request_counts->count();
            //$allowedEnquiryCount = $settingsObj->school_house_booking_request;
            $user_credits = \App\UserCredit::where("user_id", $userId)->where("send_to_schoolhouse", ">", "0")->get();

            if (sizeof($user_credits) > 0) {
                $countEnquiry = $user_credits->count();
            }
             // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
             //        $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
                    // echo "User Credit <prE>";
                    // echo "vcoucher Credit ID :".$user_voucher->vouchercredit_id."<br>";
                    // echo "User ID :".$userId;
                    //     print_r($get_voucher);
                    //     // print_r($user_credits);
                    // echo "</prE>";
                    // exit;
                // }
            if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
                $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
               
                    // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                    //     echo "Get Voucher : <pre>";
                    //         print_r($get_voucher);
                    //     echo "</pre>";
                    //    // exit;
                    // }
                if (isset($get_voucher->send_to_schoolhouse) && !empty($get_voucher->send_to_schoolhouse)) {

                    $allowedEnquiryCount = $get_voucher->send_to_schoolhouse;

                } else {
                    $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
                    if (isset($get_voucher->id)) {
                        $userWallet = new \App\UserWallet();
                        $userWallet->user_id = $userId;
                        $userWallet->vouchaercredits_id = $get_voucher->id;
                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                        $userWallet->creadit_type = "voucher";
                        $userWallet->save();
                        $allowedEnquiryCount = $get_voucher->send_to_schoolhouse;
                    }
                }
            } else {
                $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
                if (isset($get_voucher->id)) {
                    $user_voucher->vouchercredit_id = $get_voucher->id;
                    $user_voucher->save();
                    $userWallet = new \App\UserWallet();
                    $userWallet->user_id = $userId;
                    $userWallet->vouchaercredits_id = $get_voucher->id;
                    $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                    $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                    $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                    $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                    $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                    $userWallet->creadit_type = "voucher";
                    $userWallet->save();
                    $allowedEnquiryCount = $get_voucher->send_to_schoolhouse;
                }
            }
        } else {

            $is_paid = 0;
            $user_unpaid_request_counts = RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $userId)
                    ->where("property_status", "!=", "paid")
                    ->get();
//            $countEnquiry = $user_unpaid_request_counts->count();
            $user_credits = \App\UserCredit::where("user_id", $userId)->where("send_to_unpaid", ">", "0")->get();
            $countEnquiry = $user_credits->count();

            // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            //     //echo "<br>".$countEnquiry."_".$allowedEnquiryCount."<br><pre>";
            //     echo "<br>".$countEnquiry." USER ID :".$userId."<br><pre>";
            //     print_r($user_credits);
            //     echo "<br> Unpaid request <br>";
            //     echo "Voucher Credit :".$user_voucher->vouchercredit_id."<br>";
            //     print_r($user_unpaid_request_counts);
            //     echo "</pre>";
            //    exit;
            // }

            
//            $allowedEnquiryCount = $settingsObj->booking_request;
            if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
                $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
                if (isset($get_voucher->send_to_unpaid) && !empty($get_voucher->send_to_unpaid)) {
                    $allowedEnquiryCount = $get_voucher->send_to_unpaid;
                } else {
                    $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
                    if (isset($get_voucher->id)) {
                        $userWallet = new \App\UserWallet();
                        $userWallet->user_id = $userId;
                        $userWallet->vouchaercredits_id = $get_voucher->id;
                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                        $userWallet->creadit_type = "voucher";
                        $userWallet->save();
                        $allowedEnquiryCount = $get_voucher->send_to_unpaid;
                    }
                }
            } else {
                $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first(); //wrong spelling here in condition
               
                if (isset($get_voucher->id)) {
                    $user_voucher->vouchercredit_id = $get_voucher->id;
                    $user_voucher->save();
                    $userWallet = new \App\UserWallet();
                    $userWallet->user_id = $userId;
                    $userWallet->vouchaercredits_id = $get_voucher->id;
                    $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                    $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                    $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                    $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                    $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                    $userWallet->creadit_type = "voucher";
                    $userWallet->save();
                    $allowedEnquiryCount = $get_voucher->send_to_unpaid;
                }
            }
        }
        if ($userEnquiries) {
            //$countEnquiry = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
        }

        if ($countEnquiry < $allowedEnquiryCount) {
            $allowBooking = 1;
        }
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        //     echo "<br>".$countEnquiry."_".$allowedEnquiryCount."<br>";
        //     exit;
        // }

//      if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
//     echo "<br> Allow allowedEnquiryCount".$allowedEnquiryCount."<br>";
//     echo "<br> Allow Booking :".$allowBooking."<br> is Paid :".$is_paid."<br>";
//     echo "<br> Count Enquiry : ".$countEnquiry."<br>";
//     echo "<pre>";
//         print_r($user_voucher);
//     echo "</pre>";
//     exit;
// }   



//        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
//            $allowBooking = 1;
//        }
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        // echo  "allow counts = " . $allowedEnquiryCount . " ---- DB count =" . $countEnquiry;
        // return false;
        //  }
        //return "allow counts = " . $allowedEnquiryCount . " ---- DB count =" . $countEnquiry;
        //exit;
        if ($allowBooking == 0) {
            if ($is_paid) {
                return "EnquiryErrorPaid";
            } else {
                return "EnquiryError";
            }
        } else {
//            $productId = $request->get('listid');
//            $propertyObj = $this->propertyRepository->getCurrentObject($productId);

            $renterId = $propertyObj->user->id;
            $noOfNyts = $request->get('days');

            //check if this is first choice of student.
            $booking_count = 0;
            $users_info = \App\User::with("user_booking", "school_info")->where("id", $userId)->first();
            if (isset($users_info->user_booking)) {
                $booking_count = count($users_info->user_booking);
            }
            //$productPrice = $propertyObj->price_night;
            //$serviceFee = $request->get('commissionamount');
            //$totalPrice = ($propertyObj->price_night * $noOfNyts) + $serviceFee;
            //$noOfGuests = $request->get('guests');
            $msg = "";
            $worktutoremail = $propertyObj->represent_email;
            $organizationemail = $propertyObj->user->orgemail;
            $bookingno = "";
            $hr_email = $propertyObj->hr_represent_email;
            $check_hr = $propertyObj->human_resources;


            // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
            // $availableCheck = $userEnquiries->count();
            // echo "<br> avaialbe Check :".$availableCheck;
            // //$propertyObj, $enquiryObj, "no_hr", 
            // echo "HR Email :".$hr_email."<br>";
            // echo "<br> If chek hr :".$check_hr."<br>";
            // echo "Work Tutor Email :".$worktutoremail."<br> Org Email :".$organizationemail."<br>";
            // // if($propertyObj->user->orghr){
            // echo "OrgHr : ".$propertyObj->user->orghr."<br>";
            // echo "OrgHr Email : ".$propertyObj->user->orgemail."<br>";
            // echo "<br> Work :".$propertyObj->work_org_same."<br>";

            // }
            // echo "<pre>";
            //     print_r($availableCheck);
            //     print_r($propertyObj);
            // echo "<pre>"; this problem_ simply checking if there is email instead of flag checkbox_ that is already checked for blank value here I got both email of Work tutor and HR but I check for vlaidate is working or not and in which part run I mean send the mail.
            // exit;
            // }

            if (($check_hr == 0) && !empty($hr_email)) {
                $hr_details = User::where('email', $hr_email)->first();
                if (isset($hr_details->id) && !empty($hr_details->id)) {   //it checks if User Listing has defined another person, and email, as HR for the organisation hosting the student trainee
                    // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
                    //     // echo "<br> IF 1 exit";exit;
                    // }
                    $hr_dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => \Auth::user()->id,
                        'renter_id' => $renterId,
                        'is_hr' => '1',
                        'prd_id' => $productId
                    );


                    $hr_booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $hr_dataArr = array_merge($hr_dataArr, $hr_booking_status);
                    $hr_enquiryObj = RentalsEnquiry::create($hr_dataArr);
                    $hr_insertid = $hr_enquiryObj->id;

                    if ($hr_enquiryObj->Bookingno == '' || $hr_enquiryObj->Bookingno == NULL) {
                        $val = 10 * $hr_insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $hr_newdata = array(
                            'Bookingno' => $bookingno
                        );
                        RentalsEnquiry::where('id', $hr_insertid)->update($hr_newdata);
                    }
                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $hr_insertid,
                        'senderId' => \Auth::user()->id,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    MedMessage::create($dataArr);
                    $hr_enquiryObj = RentalsEnquiry::where("id", $hr_insertid)->first();
                    //checking if user have its first choice within selected dates or not
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $hr_enquiryObj->prd_id, \Auth::user()->id, $hr_insertid);
//                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    //       where do we check .type paid or unpaid_ but why first allowcheck_ its first choice checking, of what type of listing_ for all listing ebcause before bed we have applied for all
                    // we have spoken about different types we need to know which to determine loginc, emailing etc.
                    // okay no problem after all these things are working i will make the logic for different types and send different emails
                    // sure, so for working just add if job then what was, else new logic_ yes. so for school house we don-t need second and third
                    // okay i will do that as first i have test that but let me do the title changing and by next working day i will make this logic for different emails
//                    but it is only for school house sending, if not job, then email what we have created, s o I can send today *was due yesterday(
                    // there are already alot of logics going here so i dont want to change that so have to test it first so on live you dont get erros why sending
                    // thats why i have asked to change it when we can test it properly for school houses,
                    // I see it is workng for credits, only thing is email, dont worry we can do that together and test it properly :) now_ not now because it need time and i have to got after changing the title for school houses
                    // so cannot send to students to ask for school houses_ not today but will do that tomorrow for sure.
                    //Thanks
                    $availableCheck = $userEnquiries->count();
//                     if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.80.168' || $_SERVER['REMOTE_ADDR'] == '93.36.90.227'){ 
//                         echo "Available count check :".$availableCheck;
// // exit;
//                     }
                    $this->sendBookingEmails($availableCheck, $propertyObj, $hr_enquiryObj, "hr", $hr_email, $worktutoremail, $organizationemail);
//                    if ($availableCheck == 0) {
//                        //checking if user have its first choice
//                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
//                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $hr_enquiryObj);
//                            //$this->mailNotificationsForUser->sendPaidHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
//                        } else {
//                            $this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
//                        }
//                    } else {
//                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
//                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $hr_enquiryObj);
//                        } else {
//                            $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $hr_enquiryObj);
//                        }
//                    }
                } else {
                    if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.80.168'){
                      //  echo "<br> IF Else part exit;";exit;
                    }
                    $dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => \Auth::user()->id,
                        'renter_id' => $renterId,
                        'prd_id' => $productId
                    );


                    $booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $dataArr = array_merge($dataArr, $booking_status);
                    $enquiryObj = RentalsEnquiry::create($dataArr);
                    $insertid = $enquiryObj->id;

                    if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                        $val = 10 * $insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $newdata = array(
                            'Bookingno' => $bookingno
                        );
                        RentalsEnquiry::where('id', $insertid)->update($newdata);
                    }

                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $insertid,
                        'senderId' => \Auth::user()->id,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    MedMessage::create($dataArr);
                    $enquiryObj = RentalsEnquiry::where("id", $insertid)->first();
                    //checking if user have its first choice within selected dates or not
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
//                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    $availableCheck = $userEnquiries->count();
                    $this->sendBookingEmails($availableCheck, $propertyObj, $enquiryObj, "no_hr", $hr_email, $worktutoremail, $organizationemail);
//                    if ($availableCheck == 0) {
//                        if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
//                            if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
//                                $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
//                                //$this->mailNotificationsForUser->sendPaidBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
//                            } else {
//                                $this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
//                            }
//                        } else {
//                            if (isset($propertyObj->renter_id)) {
//                                $renter_user = User::where("id", $propertyObj->renter_id)->select("id", "email", "name")->first();
//                                if (isset($renter_user->email) && !empty($renter_user->email)) {
//                                    if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
//                                        $this->mailNotificationsForUser->sendPaidUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
//                                    } else {
//                                        $this->mailNotificationsForUser->sendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
//                                    }
//                                }
//                            }
//                        }
//                    } else {
//                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
//                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
//                            //$this->mailNotificationsForUser->sendPaidBookingNotification($propertyObj, $enquiryObj);
//                        } else {
//                            $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj);
//                        }
//                    }
                }
            } else {
                // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
                //     // echo "<br> Else part exit;";exit;
                // }
                $dataArr = array(
                    'checkin' => $checkIn,
                    'checkout' => $checkOut,
                    'numofdates' => $noOfNyts,
                    'caltophone' => '',
                    'enquiry_timezone' => '',
                    'user_id' => \Auth::user()->id,
                    'renter_id' => $renterId,
                    'prd_id' => $productId
                );


                $booking_status = array(
                    'booking_status' => 'Pending'
                );

                $dataArr = array_merge($dataArr, $booking_status);
                $enquiryObj = RentalsEnquiry::create($dataArr);
                $insertid = $enquiryObj->id;

                if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                    $val = 10 * $insertid + 8;
                    $val = 1500000 + $val;
                    $bookingno = "WRL" . $val;
                    $newdata = array(
                        'Bookingno' => $bookingno
                    );
                    RentalsEnquiry::where('id', $insertid)->update($newdata);
                }

                $dataArr = array(
                    'productId' => $productId,
                    'bookingNo' => $bookingno,
                    'enqid' => $insertid,
                    'senderId' => \Auth::user()->id,
                    'receiverId' => $renterId,
                    'subject' => 'Booking Request : ' . $bookingno,
                    'message' => ""
                );
                MedMessage::create($dataArr);
                $enquiryObj = RentalsEnquiry::where("id", $insertid)->first();
                //checking if user have its first choice within selected dates or not
                //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
//                $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                $availableCheck = $userEnquiries->count();
                $this->sendBookingEmails($availableCheck, $propertyObj, $enquiryObj, "no_hr", $hr_email, $worktutoremail, $organizationemail);
//                if ($availableCheck == 0) {
//                    if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
//                        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
//                            $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
//                            //this->mailNotificationsForUser->sendPaidBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
//                        } else {
//                            $this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
//                        }
//                    } else {
//                        if (isset($propertyObj->renter_id)) {
//                            $renter_user = User::where("id", $propertyObj->renter_id)->select("id", "email", "name")->first();
//                            if (isset($renter_user->email) && !empty($renter_user->email)) {
//                                if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
//                                    $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
//                                    //$this->mailNotificationsForUser->sendPaidUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
//                                } else {
//                                    $this->mailNotificationsForUser->sendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
//                                }
//                            }
//                        }
//                    }
//                } else {
//                    if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
//                        $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj);
//                        //$this->mailNotificationsForUser->sendPaidBookingNotification($propertyObj, $enquiryObj);
//                    } else {
//                        $this->mailNotificationsForUser->sendBookingNotification($propertyObj, $enquiryObj);
//                    }
//                }
            }
            $userCredit = new \App\UserCredit();
            $userCredit->user_id = $userId;
            $userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
            $userCredit->property_id = isset($propertyObj->id) ? $propertyObj->id : 0;
            if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
                $userCredit->send_to_schoolhouse = 1;
            } else {
                $userCredit->send_to_unpaid = 1;
            }
            $userCredit->save();
            return $bookingno;
        }
    }

    public function showBookingDetails($bookingNo) {
        $enquiryObj = RentalsEnquiry::where('Bookingno', $bookingNo)->first();
        $propertyObj = $this->propertyRepository->getByIdActive($enquiryObj->prd_id);
        return View('bookingresultview', compact('enquiryObj', 'propertyObj'));
    }

    public function viewRequestBookingResult($bookingNo) {
        $enquiryObj = RentalsEnquiry::where('Bookingno', $bookingNo)->first();
        $propertyObj = $this->propertyRepository->getByIdActive($enquiryObj->prd_id);

        // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
        //     echo "<prE>";
        //         print_r($enquiryObj);
        //          print_r($propertyObj);
        //     echo "</prE>";
        //     exit;
        // }
        return View('bookingresult', compact('enquiryObj', 'propertyObj'));
    }

    public function updatebookingstatus1($enquiryid) {
        $rentalObj = RentalsEnquiry::find($enquiryid);
        return View('agreement', compact('rentalObj'));
    }

    public function updatebookingstatus_14_11_19(Request $request) {

        //Pending, Decline, Accept
        $status = $request->get('status');
        $enquiryid = $request->get('enquiryid');
        $rentalObj = RentalsEnquiry::find($enquiryid);
       
        $msg = $status . " Successfully updated";
        if ($status == 'Accept') {
            //getting and calculating user booked paid and unpaid credits;
            $userId = \Auth::user()->id;

        // if($_SERVER['REMOTE_ADDR'] != '93.42.44.112'){
        //     $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin));
        //     $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout));
        // }else{
            // echo "<br> Date :".$rentalObj->checkin."<br> Out Date : ".$rentalObj->checkout."<br>";
            // echo "<br> Count :".count(explode(" ", $rentalObj->checkin))."<br>";
            // echo "<br> Count123 :".count(explode(" ", date("Y-m-d",strtotime($rentalObj->checkin))))."<br>";
            if(count(explode(" ", $rentalObj->checkin)) > 1){
                $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin));
            }else{
                $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin." 11:00:00"));
            }

            if(count(explode(" ", $rentalObj->checkout)) > 1){
                $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout));
            }else{
                $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout." 10:00:00"));
            }

            
        //}
            $availableCheck = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $rentalObj->prd_id);

//            return $availableCheck;
            if (count($availableCheck) > 0) {
                // if($_SERVER['REMOTE_ADDR'] != '93.36.90.227'){
                    return "Fail";
                // }
                //t activate button to complete test means ? 
            }

            // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            //     echo "<br> Start Date :".$newDateStart."<br> End Date :".$newDateEnd."<br> Prop Id :".$rentalObj->prd_id."<br>";
            //      $propertyId = $rentalObj->prd_id;
            //     $arrival = $rentalObj->checkin;
            //     $depature = $rentalObj->checkout;
            //     $dates = $this->getDatesFromRange($arrival, $depature);
            //         echo "<br> Checking Date :".$arrival."<br> Depature Date :".$depature."<br>";;
            //         echo "<pre>";
            //             print_r($dates);
            //         echo "</pre>";

            //         $i = 1;
            //         $dateMinus1 = count($dates);
            //         foreach ($dates as $date) {
            //             if ($i <= $dateMinus1) {
            //                 $bookingObj = Booking::where('PropId', $propertyId)->where('id_state', 1)->where('id_item', 1)->where('the_date', $date)->get();
            //                 echo "<br> Count for date : ".$date." >>> ".count($bookingObj)."<br>";
            //                 if ($bookingObj->count() > 0) {
                                
            //                 } else {
            //                     $dataArr = array('PropId' => $propertyId,
            //                         'id_state' => 1,
            //                         'id_item' => 1,
            //                         'the_date' => $date
            //                     );
            //                     echo "<pre>";
            //                         print_r($dataArr);
            //                     echo "</pre>";
            //                     // Booking::create($dataArr);
            //                 }
            //             }
            //             $i++;
            //         }
            //         exit;
            //     }

            // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            //     echo "<br> Check for availableCheck :".count($availableCheck);
            //     echo "<br><pre>";
            //         print_r($_REQUEST);
            //         print_r($availableCheck);
            //         print_r($rentalObj);
            //     echo "</pre>";
            //     exit;
            // }
            $user_voucher = User::where('id', $rentalObj->user_id)->first();
            if(isset($user_voucher->vouchercredit_id) && !empty($user_voucher->vouchercredit_id)) {
                $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
            }
            $countEnquiry = 0;
            $allowedEnquiryCount = 0;
            if (isset($rentalObj->property->property_status) && $rentalObj->property->property_status == "paid") {
                $user_wallet = \App\UserWallet::where("user_id", $rentalObj->user_id)->first();
                if (isset($user_wallet->book_paid)) {
                    $allowedEnquiryCount = $user_wallet->book_paid;
                    $user_credits = \App\UserCredit::where("user_id", $rentalObj->user_id)->where("book_paid", ">", "0")->get();
                    if (sizeof($user_credits) > 0) {
                        $countEnquiry = $user_credits->count();
                    }
                }
//                if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
//                    $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
//                    if (isset($get_voucher->book_paid) && !empty($get_voucher->book_paid)) {
//                        $allowedEnquiryCount = $get_voucher->book_paid;
//                    } else {
//                        $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
//                         if (isset($get_voucher->id)) {
//                            $userWallet = new \App\UserWallet();
//                            $userWallet->user_id = $userId;
//                            $userWallet->vouchaercredits_id = $get_voucher->id;
//                            $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
//                            $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
//                            $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
//                            $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
//                            $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
//                            $userWallet->creadit_type = "voucher";
//                            $userWallet->save();
//                            $allowedEnquiryCount = $get_voucher->book_paid;
//                        }
//                    }
//                } else {
//                    $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
//                    if (isset($get_voucher->id)) {
//                        $user_voucher->vouchercredit_id = $get_voucher->id;
//                        $user_voucher->save();
//                        $userWallet = new \App\UserWallet();
//                        $userWallet->user_id = $userId;
//                        $userWallet->vouchaercredits_id = $get_voucher->id;
//                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
//                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
//                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
//                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
//                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
//                        $userWallet->creadit_type = "voucher";
//                        $userWallet->save();
//                        $allowedEnquiryCount = $get_voucher->book_paid;
//                    }
//                }
            } else {
                $user_wallet = \App\UserWallet::where("user_id", $rentalObj->user_id)->first();
                if (isset($user_wallet->book_unpaid)) {
                    $allowedEnquiryCount = $user_wallet->book_unpaid;
                    $user_credits = \App\UserCredit::where("user_id", $rentalObj->user_id)->where("book_unpaid", ">", "0")->get();
                    if (sizeof($user_credits) > 0) {
                        $countEnquiry = $user_credits->count();
                    }
                }

//                $countEnquiry = $user_credits->count();
////            $allowedEnquiryCount = $settingsObj->booking_request;
//                if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
//                    $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
//                    if (isset($get_voucher->book_unpaid) && !empty($get_voucher->book_unpaid)) {
//                        $allowedEnquiryCount = $get_voucher->book_unpaid;
//                    } else {
//                        $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
//                        if (isset($get_voucher->id)) {
//                            $userWallet = new \App\UserWallet();
//                            $userWallet->user_id = $userId;
//                            $userWallet->vouchaercredits_id = $get_voucher->id;
//                            $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
//                            $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
//                            $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
//                            $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
//                            $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
//                            $userWallet->creadit_type = "voucher";
//                            $userWallet->save();
//                            $allowedEnquiryCount = $get_voucher->book_unpaid;
//                        }
//                    }
//                } else {
//                    $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
//                    if (isset($get_voucher->id)) {
//                        $user_voucher->vouchercredit_id = $get_voucher->id;
//                        $user_voucher->save();
//                        $userWallet = new \App\UserWallet();
//                        $userWallet->user_id = $userId;
//                        $userWallet->vouchaercredits_id = $get_voucher->id;
//                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
//                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
//                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
//                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
//                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
//                        $userWallet->creadit_type = "voucher";
//                        $userWallet->save();
//                        $allowedEnquiryCount = $get_voucher->book_unpaid;
//                    }
//                }
            }
//            ekay but neet
           

            // $newDateStart = date("Y-m-d", strtotime($rentalObj->checkin));
            // $newDateEnd = date("Y-m-d", strtotime($rentalObj->checkout));
            // $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin." 11:00:00"));
            // $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout." 10:00:00"));
            // $availableCheck = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $rentalObj->prd_id);
//            return $availableCheck;
            // if (count($availableCheck) > 0) {
            //     return "Fail";
            //     //t activate button to complete test means ? 
            // } else {

                if($allowedEnquiryCount <= $countEnquiry) {
                    return "noCredits";
                }
                RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
                $propertyId = $rentalObj->prd_id;
                $arrival = $rentalObj->checkin;
                $depature = $rentalObj->checkout;
                $dates = $this->getDatesFromRange($arrival, $depature);
                
                $this->saveBookedDates($dates, $propertyId);
                $this->updateScheduleCalendar($propertyId, 0);
            // }

            $dataArr = array(
                'productId' => $rentalObj->prd_id,
                'bookingNo' => $rentalObj->Bookingno,
                'enqid' => $enquiryid,
                'senderId' => \Auth::user()->id,
                'receiverId' => $rentalObj->user_id,
                'subject' => 'Booking Request : ' . $status,
                'message' => "You Request Approved"
            );
            MedMessage::create($dataArr);
            if ($status == 'Accept') {
                /* Send Learning Agreement */
                $cmsObj = $this->cmsRepository->getById(9);
                $stipulate = $this->cmsRepository->getById(12);
                $article1 = $this->cmsRepository->getById(13);
                $article2 = $this->cmsRepository->getById(14);
                $article3 = $this->cmsRepository->getById(15);
                $article4 = $this->cmsRepository->getById(16);
                $article5 = $this->cmsRepository->getById(17);
                $article6 = $this->cmsRepository->getById(18);
                $article7 = $this->cmsRepository->getById(19);
                $educationalobjectives = $this->cmsRepository->getById(20);
                $article9 = $this->cmsRepository->getById(21);
                $article10 = $this->cmsRepository->getById(22);

                $rentalObj = RentalsEnquiry::find($enquiryid);

                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $file_name = $timestamp . '-' . $rentalObj->Bookingno . ".pdf";

                $pdf = PDF::loadView('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'))->save(public_path() . "/pdf/agreement/" . $file_name);
                //return $file_name ;

//start here... 
/*fetching country id of student school and hosting organisation to make file PDFs choice dynamic START HERE*/

            $propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $file_name = $timestamp . '-';
            $schoolCountryId = $organizationCountryId = '';
            if($rentalObj->user->school->ocountry != ''){
                $schoolCountryId = $rentalObj->user->school->ocountry;
            }else if($rentalObj->user->school->lcountry != ''){
                $schoolCountryId = $rentalObj->user->school->lcountry;
            }
            $users = \App\User::where(['id' => $userId])->get();
            // echo "<br> Sending User Email :".$senderEMailId = $users[0]->email."<br>";
            // echo "<br> Receiver's Email :".$receiverEMailId = $rentalObj->property->user->email."<br>";

            // echo "<pre>";
                // print_r($_REQUEST);
            //     echo "<br> REQEST<Br> ";
            //     // print_r($users);
            //     echo "<br> USERS <Br> ";
            //     print_r($rentalObj->property->user->email);
            //     echo "=================";
            //     print_r($rentalObj->property->user);
            //     echo "<br> RENTAL OBJ <Br> ";
            // echo "</pre>";
            // exit;
            // echo "<br> Country School:".$schoolCountryId."<br><pre>";
                // print_r($rentalObj);
            // echo "</pre>";

            if ($rentalObj->property->addr_work_org_same == 0){
                $organizationCountryId = $rentalObj->property->wcountry;
            }else{
                $organizationCountryId = $rentalObj->host->countryr18;
            }
            // echo "Country Organization origi :".$organizationCountryId."<br>";


            if($schoolCountryId == 'Italia') {
                $schoolCountryId = 'Italy';                
            }
            if($organizationCountryId == 'Italia'){
                $organizationCountryId = 'Italy';
            }
            
            if($schoolCountryId == 'Irlanda'){
                $schoolCountryId = 'Ireland';                
            }
            if($organizationCountryId == 'Irlanda'){
                $organizationCountryId = 'Ireland';
            }

            if($schoolCountryId == 'Regno Unito'){
                $schoolCountryId = 'United Kingdom';                
            }
            if($organizationCountryId == 'Regno Unito' ){
               $organizationCountryId = 'United Kingdom';
            }
             // echo "Country Organization :".$organizationCountryId."<br>";
// exit;

            if(!empty($schoolCountryId)){
                if(is_numeric($schoolCountryId)){
                    $schoolCountryData = \App\Country::where('id',$schoolCountryId)->get();
                }else{
                    $schoolCountryData = \App\Country::where('name',$schoolCountryId)->get();
                }
                $schoolCountryCode = $schoolCountryData[0]->code;
            }
            // echo "<br> IDs :".$organizationCountryId."<br>";
            // exit;
            if(!empty($organizationCountryId)){
                if(is_numeric($organizationCountryId)){
                    $organizationCountryData = \App\Country::where('id',$organizationCountryId)->get();
                }else{
                    $organizationCountryData = \App\Country::where('name',$organizationCountryId)->get();
                }
                // var_dump(empty($organizationCountryData));
                // echo "<br> Count:".count($organizationCountryData)."<br>";
                // echo "<pre>";
                //     print_r($organizationCountryData);
                // echo "<pre>";

                // exit;
                if(count($organizationCountryData) > 0){
                    $organizationCountryCode = $organizationCountryData[0]->code;                    
                }else{
                    $organizationCountryCode = "";
                }
            }

           // $inCountry = array("Italy","Ireland","United Kingdom");
           // $inCountry = array("Ireland","United Kingdom");
           $inCountry = array("Italy","United Kingdom");
            if((!empty($schoolCountryCode) && !empty($organizationCountryCode)) && in_array($organizationCountryId,$inCountry) && in_array($schoolCountryId,$inCountry) ){
                $file_name .= 'Pdf'.$schoolCountryCode.'goingto'.$organizationCountryCode;
            }else{
                // send this request to "esperienzainglese@gmail.com"
                $file_name .= "pdfITgoingtoUK";
                $senderEmailId = $users[0]->email;
                $receiverEmailId = $rentalObj->property->user->email;
                $schoolCountry = $schoolCountryId;
                $organizationCountry = $organizationCountryId;
                // echo "<br> SenderEmail :".$senderEmailId."<br> Receiver Email Id :".$receiverEmailId."<br> School Country : ".$schoolCountry."<br> Organization Country :".$organizationCountry."<br> CheckIn :".date("Y-m-d",strtotime($checkIn))."<br> Checkout :".date("Y-m-d",strtotime($checkOut))."<br>";
                $checkIn = $arrival;
                $checkOut = $depature;
                $data = array('senderEmailId' => $senderEmailId, 'receiverEmailId' => $receiverEmailId, 'schoolCountry' => $schoolCountry, 'organizationCountry' => $organizationCountry, 'checkIn' =>date("Y-m-d",strtotime($checkIn)), 'checkOut' => date("Y-m-d",strtotime($checkOut)));

                /*\Mail::send('emails.request', $data, function($message){
                    $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                    $message->to("esperienzainglese@gmail.com","")
                            // ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                            ->subject('Work-Related-Learning.com - you have reveiced a message regarding a request');
                            // exit;
                });*/
            }


            $file_name .= "-".$rentalObj->Bookingno;
            $file_name .= ".pdf";
            // echo $file_name."<br>";
            // exit;
            // echo "<prE>";
                // print_R($enquiryObj->school_name);
            // \App\User::where("id")
            $schoolName = "";
            if(!empty($users[0]->school_name)){
                $schoolInfo = \App\School::where('id', $users[0]->school_name)->first();
                //     echo "<BR> USER DETAILS </br>";
                //     print_R($users[0]->school_name);
                //     print_R($schoolInfo);
                //     // print_R($users);
                // echo "</prE>";
                // if(count($schoolInfo) > 0){
                if(isset($schoolInfo->id)){
                    $schoolName = $schoolInfo->name;
                }                
            }
            // exit;
            /*fetching country id of student school and hosting organisation to make file PDFs choice dynamic END HERE*/
            $enquiryObj = RentalsEnquiry::where('Bookingno', $rentalObj->Bookingno)->first();
            $file_name1 = $timestamp . '-' . $rentalObj->Bookingno . "_register.pdf"; //adding filename to Register
            $file_name2 = $timestamp . '-' . $rentalObj->Bookingno . "_evaluation.pdf"; //adding filename to Register
            $file_name3 = $timestamp . '-' . $rentalObj->Bookingno . "_parental_permission.pdf"; //adding filename to Register
            if ($propertyObj->property_status != "paid") {
                // $pdf3 = PDF::loadView('parental_permissionpdf', compact('enquiryObj'))->save(public_path() . "/pdf/parental_permission/" . $file_name3);
                //  echo "File Name :".$file_name3."<br>";exit;
                $pdf = PDF::loadView('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'))->save(public_path() . "/pdf/agreement/" . $file_name);                
                $pdf1 = PDF::loadView('Activity_Register_WRL_Registro_Presenze_English', compact('users','rentalObj'))->save(public_path() . "/pdf/register/" . $file_name1); //adding pdf genration of Register
                $pdf2 = PDF::loadView('evoluationpdf_new', compact('users','enquiryObj','schoolName'))->save(public_path() . "/pdf/evaluation/" . $file_name2); //adding pdf genration of Evaluation
                $pdf3 = PDF::loadView('parental_permissionpdf', compact('enquiryObj'))->save(public_path() . "/pdf/parental_permission/" . $file_name3); //adding pdf genration of Evaluation
//                dd($educationalobjectives);
               // return view('Activity_Register_WRL_Registro_Presenze_English', compact('users','rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'));
            }                
                   //end here... 

                $tutorEmail = $rentalObj->user->school->tutor_email;

                if (isset($rentalObj->prd_id) && !empty($rentalObj->prd_id)) {
                    $tutor_detail = Property::find($rentalObj->prd_id);
                    if (isset($tutor_detail->represent_email) && !empty($tutor_detail->represent_email)) {
                        $tutorEmail = $tutor_detail->represent_email;
                    }
                }

                $parentEmail = null;
                if (is_null($rentalObj->user->student_relationship)) {
                    $studentEmail = $rentalObj->user->email;
                } else {
                    $parentEmail = $rentalObj->user->email;
                    $studentEmail = $rentalObj->user->semail16;
                }

                // get HR details
                $hr_enquiry = RentalsEnquiry::where(["id" => $enquiryid, 'is_hr' => '1'])->first();
                $hr_email = "";
                $hr_email_detail = "";

                if($_SERVER['REMOTE_ADDR'] == '93.36.90.227'){

                    // echo "HR Enquiry<pre>";
                    //     print_r($hr_enquiry);
                    //     print_r($enquiryid);
                    // echo "</pre>";
                    // exit;                    
                }
                if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
                    $hr_user = User::find($hr_enquiry->renter_id);
                    $hr_email = isset($hr_user->email) ? $hr_user->email : '';

                    if (isset($hr_user->prd_id) && !empty($hr_user->prd_id)) {
                        $hr_email_detail = Property::find($hr_user->prd_id);
                    }
                }


                 //if($_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
                    $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
                    if($propertyDetails->human_resources == 0 && $propertyDetails->hr_represent_email != ''){
                        $hr_email = $propertyDetails->hr_represent_email;
                    }

                    // echo "<br> HR Email:".$hr_email."<br>";
                    // echo "HR Enquiry Details<pre>";
                    //     print_r($propertyDetails);
                    //     print_r($hr_email_detail);
                    // echo "</pre>";
                    // exit;                    
                //}


                /*update wallet on accept bed type start here */

                $acceptedUserId =  \Auth::user()->id;
                $userInfo = User::with('group_info')->where('id', $rentalObj->user_id)->first();
                $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
            
                if($propertyDetails->property_status == 'unpaid' || $propertyDetails->property_status == 'unpaidflash'){
                        // print_r($rentalObj);
                    $fetchWalletData = \App\UserWallet::where('user_id',$rentalObj->user_id)->first();

                    // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                    //     echo "<br> Before Update Wallet <pre>";
                    //         print_r($fetchWalletData);
                    //     echo "</pre> <br> ";                        
                    // }

                    $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 3 ) : "3";
                    // $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 1 ) : "1";
                    $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 0 ) : "0";

                    \App\UserWallet::where('user_id',$rentalObj->user_id)->update(array('send_to_schoolhouse'=>$send_to_schoolhouse,'book_paid'=>$book_paid,'creadit_type'=>''));

                    $fetchWalletData = \App\UserWallet::where('user_id',$rentalObj->user_id)->first();

                    // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                    //     echo "Parent Email :".$rentalObj->user->email."Studen Email : <br>".$rentalObj->user->semail16."<br>";
                    //     echo "<br> After Update Wallet <pre>";
                    //         print_r($fetchWalletData);
                    //     echo "</pre>";
                    //     exit;
                    // }



                    $studentEmail = $rentalObj->user->email;
                    $parentEmail = $rentalObj->user->semail16;
                    
                    $this->mailNotificationsForUser->sendemailPaidToStudentAboutWalletUpdateForSchoolHouseAfterJobAccept($parentEmail, $studentEmail, $rentalObj,$userInfo);
                }

                /*update wallet on accept bed type end here*/
           //  if($_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
           //     echo "tutor email = ".$tutorEmail."<br />";
           //     echo "HR email = ".$hr_email."<br />";
           //     echo "Parent email = ".$parentEmail."<br />";
           //     echo "Student email = ".$studentEmail."<br />";
           //     echo "is HR = ".$rentalObj->is_hr."<br />";
           //     // exit;
           // }

                $files = array("agreement" => $file_name,"evaluation"=>$file_name2,"register"=>$file_name1,"parental_permission"=>$file_name3);
                // $this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $file_name, $rentalObj, $hr_email, $hr_email_detail,$file_name1);
                $this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $files, $rentalObj, $hr_email, $hr_email_detail,$file_name1);
                //$this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $file_name, $rentalObj, $hr_email, $hr_email_detail);

                //$pdf = \PDF::loadView('agreement', compact('rentalObj', 'cmsObj'));
                //return $pdf->download('invoice.pdf');
                //return View('agreement', compact('rentalObj', 'cmsObj'));

                if ($rentalObj->is_hr == "1") {
                    $propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
                    $worktutoremail = $propertyObj->represent_email;
                    if (!empty($worktutoremail)) {
                        //$worktutoremail = $rentalObj->user->email;
                        $organizationemail = $propertyObj->user->orgemail;
                        $this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj,$rentalObj);
                    }
                    //echo $worktutoremail; exit;
                    //exit;
                    $checkIn = $rentalObj->checkin;
                    $checkOut = $rentalObj->checkout;
                    $noOfNyts = $rentalObj->numofdates;
                    $renterId = $propertyObj->user->id;
                    $dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => $rentalObj->user_id,
                        'renter_id' => $renterId,
                        'prd_id' => $rentalObj->prd_id
                    );


                    $booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $dataArr = array_merge($dataArr, $booking_status);
                    $enquiryObj = RentalsEnquiry::create($dataArr);
                    $insertid = $enquiryObj->id;

                    if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                        $val = 10 * $insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $newdata = array(
                            'Bookingno' => $bookingno
                        );
                        RentalsEnquiry::where('id', $insertid)->update($newdata);
                    }

                    $dataArr = array(
                        'productId' => $rentalObj->prd_id,
                        'bookingNo' => $bookingno,
                        'enqid' => $insertid,
                        'senderId' => $rentalObj->user_id,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    MedMessage::create($dataArr);
                }
            }
            $userCredit = new \App\UserCredit();
            $userCredit->user_id = $rentalObj->user_id;
            $userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
            $userCredit->property_id = isset($rentalObj->property->id) ? $rentalObj->property->id : 0;
            if (isset($rentalObj->property->property_status) && $rentalObj->property->property_status == "paid") {
                    $userCredit->book_paid = 1;                    
            } else {
                $userCredit->book_unpaid = 1;
            }
            $userCredit->save();
            flash($msg, 'success');
            return "success";
        }

        if ($status == 'Decline') {
            $userId = $rentalObj->user_id;
            $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();

            RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
            $this->mailNotificationsForUser->sendDeclineEmail($rentalObj);

            $send_to_unpaid = isset($fetchWalletData->send_to_unpaid) ? ($fetchWalletData->send_to_unpaid + 1 ) : "1";
            \App\UserWallet::where('user_id',$userId)->update(array('send_to_unpaid'=>$send_to_unpaid,'creadit_type'=>''));
            
            $dataArr = array(
                'productId' => $rentalObj->prd_id,
                'bookingNo' => $rentalObj->Bookingno,
                'enqid' => $enquiryid,
                'senderId' => \Auth::user()->id,
                'receiverId' => $rentalObj->user_id,
                'subject' => 'Booking Request : ' . $status,
                'message' => "You Request Declined"
            );
            MedMessage::create($dataArr);
            flash($msg, 'success');
            return "success";
        }
    }

    public function updatebookingstatus(Request $request) {

            //Pending, Decline, Accept
            $status = $request->get('status');
            $enquiryid = $request->get('enquiryid');
            $rentalObj = RentalsEnquiry::find($enquiryid);
           // echo "<pre>";
           //  print_r($rentalObj);
           //  print_r($_SERVER);
           // echo "</pre>";
           // exit;
            $msg = $status . " Successfully updated";

            if ($status == 'Accept') {
                //getting and calculating user booked paid and unpaid credits;
                $userId = \Auth::user()->id;

            // if($_SERVER['REMOTE_ADDR'] != '93.42.44.112'){
            //     $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin));
            //     $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout));
            // }else{
                // echo "<br> Date :".$rentalObj->checkin."<br> Out Date : ".$rentalObj->checkout."<br>";
                // echo "<br> Count :".count(explode(" ", $rentalObj->checkin))."<br>";
                // echo "<br> Count123 :".count(explode(" ", date("Y-m-d",strtotime($rentalObj->checkin))))."<br>";
                if(count(explode(" ", $rentalObj->checkin)) > 1){
                    $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin));
                }else{
                    $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin." 11:00:00"));
                }

                if(count(explode(" ", $rentalObj->checkout)) > 1){
                    $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout));
                }else{
                    $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout." 10:00:00"));
                }

                
            //}
                $availableCheck = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $rentalObj->prd_id);

    //            return $availableCheck;
                if (count($availableCheck) > 0) {
                    // if($_SERVER['REMOTE_ADDR'] != '93.33.39.38' || $_SERVER['REMOTE_ADDR'] != '93.32.72.112'){
                    // if($_SERVER['REMOTE_ADDR'] != '93.32.72.112'){
                       return "Fail";
                    // }
                    //t activate button to complete test means ? 
                }
                
                $user_voucher = User::where('id', $rentalObj->user_id)->first();
                if(isset($user_voucher->vouchercredit_id) && !empty($user_voucher->vouchercredit_id)) {
                    $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
                }
                $countEnquiry = 0;
                $allowedEnquiryCount = 0;
                if (isset($rentalObj->property->property_status) && $rentalObj->property->property_status == "paid") {
                    $user_wallet = \App\UserWallet::where("user_id", $rentalObj->user_id)->first();
                    if (isset($user_wallet->book_paid)) {
                        $allowedEnquiryCount = $user_wallet->book_paid;
                        $user_credits = \App\UserCredit::where("user_id", $rentalObj->user_id)->where("book_paid", ">", "0")->get();
                        if (sizeof($user_credits) > 0) {
                            $countEnquiry = $user_credits->count();
                        }
                    }
    //                if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
    //                    $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
    //                    if (isset($get_voucher->book_paid) && !empty($get_voucher->book_paid)) {
    //                        $allowedEnquiryCount = $get_voucher->book_paid;
    //                    } else {
    //                        $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
    //                         if (isset($get_voucher->id)) {
    //                            $userWallet = new \App\UserWallet();
    //                            $userWallet->user_id = $userId;
    //                            $userWallet->vouchaercredits_id = $get_voucher->id;
    //                            $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
    //                            $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
    //                            $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
    //                            $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
    //                            $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
    //                            $userWallet->creadit_type = "voucher";
    //                            $userWallet->save();
    //                            $allowedEnquiryCount = $get_voucher->book_paid;
    //                        }
    //                    }
    //                } else {
    //                    $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
    //                    if (isset($get_voucher->id)) {
    //                        $user_voucher->vouchercredit_id = $get_voucher->id;
    //                        $user_voucher->save();
    //                        $userWallet = new \App\UserWallet();
    //                        $userWallet->user_id = $userId;
    //                        $userWallet->vouchaercredits_id = $get_voucher->id;
    //                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
    //                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
    //                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
    //                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
    //                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
    //                        $userWallet->creadit_type = "voucher";
    //                        $userWallet->save();
    //                        $allowedEnquiryCount = $get_voucher->book_paid;
    //                    }
    //                }
                } else {
                    $user_wallet = \App\UserWallet::where("user_id", $rentalObj->user_id)->first();
                    if (isset($user_wallet->book_unpaid)) {
                        $allowedEnquiryCount = $user_wallet->book_unpaid;
                        $user_credits = \App\UserCredit::where("user_id", $rentalObj->user_id)->where("book_unpaid", ">", "0")->get();
                        if (sizeof($user_credits) > 0) {
                            $countEnquiry = $user_credits->count();
                        }
                    }

    //                $countEnquiry = $user_credits->count();
    ////            $allowedEnquiryCount = $settingsObj->booking_request;
    //                if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
    //                    $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
    //                    if (isset($get_voucher->book_unpaid) && !empty($get_voucher->book_unpaid)) {
    //                        $allowedEnquiryCount = $get_voucher->book_unpaid;
    //                    } else {
    //                        $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
    //                        if (isset($get_voucher->id)) {
    //                            $userWallet = new \App\UserWallet();
    //                            $userWallet->user_id = $userId;
    //                            $userWallet->vouchaercredits_id = $get_voucher->id;
    //                            $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
    //                            $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
    //                            $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
    //                            $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
    //                            $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
    //                            $userWallet->creadit_type = "voucher";
    //                            $userWallet->save();
    //                            $allowedEnquiryCount = $get_voucher->book_unpaid;
    //                        }
    //                    }
    //                } else {
    //                    $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
    //                    if (isset($get_voucher->id)) {
    //                        $user_voucher->vouchercredit_id = $get_voucher->id;
    //                        $user_voucher->save();
    //                        $userWallet = new \App\UserWallet();
    //                        $userWallet->user_id = $userId;
    //                        $userWallet->vouchaercredits_id = $get_voucher->id;
    //                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
    //                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
    //                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
    //                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
    //                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
    //                        $userWallet->creadit_type = "voucher";
    //                        $userWallet->save();
    //                        $allowedEnquiryCount = $get_voucher->book_unpaid;
    //                    }
    //                }
                }
    //            ekay but neet
               

                // $newDateStart = date("Y-m-d", strtotime($rentalObj->checkin));
                // $newDateEnd = date("Y-m-d", strtotime($rentalObj->checkout));
                // $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin." 11:00:00"));
                // $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout." 10:00:00"));
                // $availableCheck = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $rentalObj->prd_id);
    //            return $availableCheck;
                // if (count($availableCheck) > 0) {
                //     return "Fail";
                //     //t activate button to complete test means ? 
                // } else {

                    if($allowedEnquiryCount <= $countEnquiry) {
                        return "noCredits";
                    }
                    RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
                    $propertyId = $rentalObj->prd_id;
                    $arrival = $rentalObj->checkin;
                    $depature = $rentalObj->checkout;
                    $dates = $this->getDatesFromRange($arrival, $depature);
                    
                   // if($_SERVER['REMOTE_ADDR'] != '93.32.72.112'){
                        $this->saveBookedDates($dates, $propertyId);
                        $this->updateScheduleCalendar($propertyId, 0);
                    //}
                // }

                $dataArr = array(
                    'productId' => $rentalObj->prd_id,
                    'bookingNo' => $rentalObj->Bookingno,
                    'enqid' => $enquiryid,
                    'senderId' => \Auth::user()->id,
                    'receiverId' => $rentalObj->user_id,
                    'subject' => 'Booking Request : ' . $status,
                    'message' => "You Request Approved"
                );
                MedMessage::create($dataArr);
                if ($status == 'Accept') {
                    /* Send Learning Agreement */
                    $cmsObj = $this->cmsRepository->getById(9);
                    $stipulate = $this->cmsRepository->getById(12);
                    $article1 = $this->cmsRepository->getById(13);
                    $article2 = $this->cmsRepository->getById(14);
                    $article3 = $this->cmsRepository->getById(15);
                    $article4 = $this->cmsRepository->getById(16);
                    $article5 = $this->cmsRepository->getById(17);
                    $article6 = $this->cmsRepository->getById(18);
                    $article7 = $this->cmsRepository->getById(19);
                    $educationalobjectives = $this->cmsRepository->getById(20);
                    $article9 = $this->cmsRepository->getById(21);
                    $article10 = $this->cmsRepository->getById(22);

                    $rentalObj = RentalsEnquiry::find($enquiryid);

                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $file_name = $timestamp . '-' . $rentalObj->Bookingno . ".pdf";

                    $pdf = PDF::loadView('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'))->save(public_path() . "/pdf/agreement/" . $file_name);
                    //return $file_name ;

    //start here... 
    /*fetching country id of student school and hosting organisation to make file PDFs choice dynamic START HERE*/

                $propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $file_name = $timestamp . '-';
                $schoolCountryId = $organizationCountryId = '';
                if($rentalObj->user->school->ocountry != ''){
                    $schoolCountryId = $rentalObj->user->school->ocountry;
                }else if($rentalObj->user->school->lcountry != ''){
                    $schoolCountryId = $rentalObj->user->school->lcountry;
                }
                $users = \App\User::where(['id' => $userId])->get();
                // echo "<br> Sending User Email :".$senderEMailId = $users[0]->email."<br>";
                // echo "<br> Receiver's Email :".$receiverEMailId = $rentalObj->property->user->email."<br>";

                // echo "<pre>";
                    // print_r($_REQUEST);
                //     echo "<br> REQEST<Br> ";
                //     // print_r($users);
                //     echo "<br> USERS <Br> ";
                //     print_r($rentalObj->property->user->email);
                //     echo "=================";
                //     print_r($rentalObj->property->user);
                //     echo "<br> RENTAL OBJ <Br> ";
                // echo "</pre>";
                // exit;
                // echo "<br> Country School:".$schoolCountryId."<br><pre>";
                    // print_r($rentalObj);
                // echo "</pre>";

                if ($rentalObj->property->addr_work_org_same == 0){
                    $organizationCountryId = $rentalObj->property->wcountry;
                }else{
                    $organizationCountryId = $rentalObj->host->countryr18;
                }
                // echo "Country Organization origi :".$organizationCountryId."<br>";


                if($schoolCountryId == 'Italia') {
                    $schoolCountryId = 'Italy';                
                }
                if($organizationCountryId == 'Italia'){
                    $organizationCountryId = 'Italy';
                }
                
                if($schoolCountryId == 'Irlanda'){
                    $schoolCountryId = 'Ireland';                
                }
                if($organizationCountryId == 'Irlanda'){
                    $organizationCountryId = 'Ireland';
                }

                if($schoolCountryId == 'Regno Unito'){
                    $schoolCountryId = 'United Kingdom';                
                }
                if($organizationCountryId == 'Regno Unito' ){
                   $organizationCountryId = 'United Kingdom';
                }
                 // echo "Country Organization :".$organizationCountryId."<br>";
    // exit;

                if(!empty($schoolCountryId)){
                    if(is_numeric($schoolCountryId)){
                        $schoolCountryData = \App\Country::where('id',$schoolCountryId)->get();
                    }else{
                        $schoolCountryData = \App\Country::where('name',$schoolCountryId)->get();
                    }
                    $schoolCountryCode = $schoolCountryData[0]->code;
                }
                // echo "<br> IDs :".$organizationCountryId."<br>";
                // exit;
                if(!empty($organizationCountryId)){
                    if(is_numeric($organizationCountryId)){
                        $organizationCountryData = \App\Country::where('id',$organizationCountryId)->get();
                    }else{
                        $organizationCountryData = \App\Country::where('name',$organizationCountryId)->get();
                    }
                    // var_dump(empty($organizationCountryData));
                    // echo "<br> Count:".count($organizationCountryData)."<br>";
                    // echo "<pre>";
                    //     print_r($organizationCountryData);
                    // echo "<pre>";

                    // exit;
                    if(count($organizationCountryData) > 0){
                        $organizationCountryCode = $organizationCountryData[0]->code;                    
                    }else{
                        $organizationCountryCode = "";
                    }
                }

               // $inCountry = array("Italy","Ireland","United Kingdom");
               // $inCountry = array("Ireland","United Kingdom");
               $inCountry = array("Italy","United Kingdom");
                if((!empty($schoolCountryCode) && !empty($organizationCountryCode)) && in_array($organizationCountryId,$inCountry) && in_array($schoolCountryId,$inCountry) ){
                    $file_name .= 'Pdf'.$schoolCountryCode.'goingto'.$organizationCountryCode;
                }else{
                    // send this request to "esperienzainglese@gmail.com"
                    $file_name .= "pdfITgoingtoUK";
                    $senderEmailId = $users[0]->email;
                    $receiverEmailId = $rentalObj->property->user->email;
                    $schoolCountry = $schoolCountryId;
                    $organizationCountry = $organizationCountryId;
                    // echo "<br> SenderEmail :".$senderEmailId."<br> Receiver Email Id :".$receiverEmailId."<br> School Country : ".$schoolCountry."<br> Organization Country :".$organizationCountry."<br> CheckIn :".date("Y-m-d",strtotime($checkIn))."<br> Checkout :".date("Y-m-d",strtotime($checkOut))."<br>";
                    $checkIn = $arrival;
                    $checkOut = $depature;
                    $data = array('senderEmailId' => $senderEmailId, 'receiverEmailId' => $receiverEmailId, 'schoolCountry' => $schoolCountry, 'organizationCountry' => $organizationCountry, 'checkIn' =>date("Y-m-d",strtotime($checkIn)), 'checkOut' => date("Y-m-d",strtotime($checkOut)));

                    /*\Mail::send('emails.request', $data, function($message){
                        $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                        $message->to("esperienzainglese@gmail.com","")
                                // ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                                ->subject('Work-Related-Learning.com - you have reveiced a message regarding a request');
                                // exit;
                    });*/
                }


                $file_name .= "-".$rentalObj->Bookingno;
                $file_name .= ".pdf";
                // echo $file_name."<br>";
                // exit;
                // echo "<prE>";
                    // print_R($enquiryObj->school_name);
                // \App\User::where("id")
                $schoolName = "";
                if(!empty($users[0]->school_name)){
                    $schoolInfo = \App\School::where('id', $users[0]->school_name)->first();
                    //     echo "<BR> USER DETAILS </br>";
                    //     print_R($users[0]->school_name);
                    //     print_R($schoolInfo);
                    //     // print_R($users);
                    // echo "</prE>";
                    // if(count($schoolInfo) > 0){
                    if(isset($schoolInfo->id)){
                        $schoolName = $schoolInfo->name;
                    }                
                }
                // exit;
                /*fetching country id of student school and hosting organisation to make file PDFs choice dynamic END HERE*/
                $enquiryObj = RentalsEnquiry::where('Bookingno', $rentalObj->Bookingno)->first();
                $file_name1 = $timestamp . '-' . $rentalObj->Bookingno . "_register.pdf"; //adding filename to Register
                $file_name2 = $timestamp . '-' . $rentalObj->Bookingno . "_evaluation.pdf"; //adding filename to Register
                $file_name3 = $timestamp . '-' . $rentalObj->Bookingno . "_parental_permission.pdf"; //adding filename to Register
                if ($propertyObj->property_status != "paid") {
                    // $pdf3 = PDF::loadView('parental_permissionpdf', compact('enquiryObj'))->save(public_path() . "/pdf/parental_permission/" . $file_name3);
                    //  echo "File Name :".$file_name3."<br>";exit;
                    $pdf = PDF::loadView('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'))->save(public_path() . "/pdf/agreement/" . $file_name);                
                    $pdf1 = PDF::loadView('Activity_Register_WRL_Registro_Presenze_English', compact('users','rentalObj'))->save(public_path() . "/pdf/register/" . $file_name1); //adding pdf genration of Register
                    $pdf2 = PDF::loadView('evoluationpdf_new', compact('users','enquiryObj','schoolName'))->save(public_path() . "/pdf/evaluation/" . $file_name2); //adding pdf genration of Evaluation
                    $pdf3 = PDF::loadView('parental_permissionpdf', compact('enquiryObj'))->save(public_path() . "/pdf/parental_permission/" . $file_name3); //adding pdf genration of Evaluation
    //                dd($educationalobjectives);
                   // return view('Activity_Register_WRL_Registro_Presenze_English', compact('users','rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'));
                }                
                       //end here... 

                    $tutorEmail = $rentalObj->user->school->tutor_email;

                    if (isset($rentalObj->prd_id) && !empty($rentalObj->prd_id)) {
                        $tutor_detail = Property::find($rentalObj->prd_id);
                        if (isset($tutor_detail->represent_email) && !empty($tutor_detail->represent_email)) {
                            $tutorEmail = $tutor_detail->represent_email;
                        }
                    }

                    $parentEmail = null;
                    if (is_null($rentalObj->user->student_relationship)) {
                        $studentEmail = $rentalObj->user->email;
                    } else {
                        $parentEmail = $rentalObj->user->email;
                        $studentEmail = $rentalObj->user->semail16;
                    }

                    // get HR details
                    $hr_enquiry = RentalsEnquiry::where(["id" => $enquiryid, 'is_hr' => '1'])->first();
                    $hr_email = "";
                    $hr_email_detail = "";

                    
                    if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
                        $hr_user = User::find($hr_enquiry->renter_id);
                        $hr_email = isset($hr_user->email) ? $hr_user->email : '';

                        if (isset($hr_user->prd_id) && !empty($hr_user->prd_id)) {
                            $hr_email_detail = Property::find($hr_user->prd_id);
                        }
                    }


                     //if($_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
                        $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
                        if($propertyDetails->human_resources == 0 && $propertyDetails->hr_represent_email != ''){
                            $hr_email = $propertyDetails->hr_represent_email;
                        }

                        // echo "<br> HR Email:".$hr_email."<br>";
                        // echo "HR Enquiry Details<pre>";
                        //     print_r($propertyDetails);
                        //     print_r($hr_email_detail);
                        // echo "</pre>";
                        // exit;                    
                    //}


                    /*update wallet on accept bed type start here */

                    $acceptedUserId =  \Auth::user()->id;
                    $userInfo = User::with('group_info')->where('id', $rentalObj->user_id)->first();
                    $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
                    //if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
               //      if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
               //          echo "Tutor Email :".$tutorEmail."<br>";
               //          echo "HR Enquiry<pre>";
               //              print_r($hr_enquiry);
               //              print_r($rentalObj);
               //          echo "</pre>";
               //          // exit;                    
               //     // }
               //          echo "Is other work tutor :".$propertyDetails->work_org_same." <br> Human R : >>". $propertyDetails->human_resources."<br>";
               //      echo "Property Details :<pre>";
               //          print_r($propertyDetails);
               //      echo "</pre>";
               //      // exit;

               //     echo "tutor email = ".$tutorEmail."<br />";
               //     echo "HR email = ".$hr_email."<br />";
               //     echo "Parent email = ".$parentEmail."<br />";
               //     echo "Student email = ".$studentEmail."<br />";
               //     echo "is HR = ".$rentalObj->is_hr."<br />";

               //     // exit;
               // }
                    $is_work_org_same = 0;
                    $is_hr_same = 0;
                    if($propertyDetails->property_status == 'unpaid' || $propertyDetails->property_status == 'unpaidflash'){
                            // print_r($rentalObj);
                        $fetchWalletData = \App\UserWallet::where('user_id',$rentalObj->user_id)->first();

                        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                        //     echo "<br> Before Update Wallet <pre>";
                        //         print_r($fetchWalletData);
                        //     echo "</pre> <br> ";                        
                        // }

                        $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 3 ) : "3";
                        // $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 1 ) : "1";
                        $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 0 ) : "0";

                        \App\UserWallet::where('user_id',$rentalObj->user_id)->update(array('send_to_schoolhouse'=>$send_to_schoolhouse,'book_paid'=>$book_paid,'creadit_type'=>''));

                        $fetchWalletData = \App\UserWallet::where('user_id',$rentalObj->user_id)->first();

                        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                        //     echo "Parent Email :".$rentalObj->user->email."Studen Email : <br>".$rentalObj->user->semail16."<br>";
                        //     echo "<br> After Update Wallet <pre>";
                        //         print_r($fetchWalletData);
                        //     echo "</pre>";
                        //     exit;
                        // }



                        $studentEmail = $rentalObj->user->semail16;
                        $parentEmail = $rentalObj->user->email;
                        
                        $this->mailNotificationsForUser->sendemailPaidToStudentAboutWalletUpdateForSchoolHouseAfterJobAccept($parentEmail, $studentEmail, $rentalObj,$userInfo);
                    }

                    /*update wallet on accept bed type end here*/
                

                    $files = array("agreement" => $file_name,"evaluation"=>$file_name2,"register"=>$file_name1,"parental_permission"=>$file_name3);
                    // $this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $file_name, $rentalObj, $hr_email, $hr_email_detail,$file_name1);
                    $this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $files, $rentalObj, $hr_email, $hr_email_detail);
                    //$this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $file_name, $rentalObj, $hr_email, $hr_email_detail);

                    //$pdf = \PDF::loadView('agreement', compact('rentalObj', 'cmsObj'));
                    //return $pdf->download('invoice.pdf');
                    //return View('agreement', compact('rentalObj', 'cmsObj'));

                    if ($rentalObj->is_hr == "1") {
                        $propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
                        $worktutoremail = $propertyObj->represent_email;
                        if (!empty($worktutoremail)) {
                            //$worktutoremail = $rentalObj->user->email;
                            $organizationemail = $propertyObj->user->orgemail;
                            $this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj,$rentalObj);
                        }
                        //echo $worktutoremail; exit;
                        //exit;
                        $checkIn = $rentalObj->checkin;
                        $checkOut = $rentalObj->checkout;
                        $noOfNyts = $rentalObj->numofdates;
                        $renterId = $propertyObj->user->id;
                        $dataArr = array(
                            'checkin' => $checkIn,
                            'checkout' => $checkOut,
                            'numofdates' => $noOfNyts,
                            'caltophone' => '',
                            'enquiry_timezone' => '',
                            'user_id' => $rentalObj->user_id,
                            'renter_id' => $renterId,
                            'prd_id' => $rentalObj->prd_id
                        );


                        $booking_status = array(
                            'booking_status' => 'Pending'
                        );

                        $dataArr = array_merge($dataArr, $booking_status);
                        $enquiryObj = RentalsEnquiry::create($dataArr);
                        $insertid = $enquiryObj->id;

                        if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                            $val = 10 * $insertid + 8;
                            $val = 1500000 + $val;
                            $bookingno = "WRL" . $val;
                            $newdata = array(
                                'Bookingno' => $bookingno
                            );
                            RentalsEnquiry::where('id', $insertid)->update($newdata);
                        }

                        $dataArr = array(
                            'productId' => $rentalObj->prd_id,
                            'bookingNo' => $bookingno,
                            'enqid' => $insertid,
                            'senderId' => $rentalObj->user_id,
                            'receiverId' => $renterId,
                            'subject' => 'Booking Request : ' . $bookingno,
                            'message' => ""
                        );
                        MedMessage::create($dataArr);
                    }
                }
                $userCredit = new \App\UserCredit();
                $userCredit->user_id = $rentalObj->user_id;
                $userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
                $userCredit->property_id = isset($rentalObj->property->id) ? $rentalObj->property->id : 0;
                if (isset($rentalObj->property->property_status) && $rentalObj->property->property_status == "paid") {
                        $userCredit->book_paid = 1;                    
                } else {
                    $userCredit->book_unpaid = 1;
                }
                $userCredit->save();
                flash($msg, 'success');
                return "success";
            }

            if ($status == 'Decline') {
                $userId = $rentalObj->user_id;
                $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();

                RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
                $this->mailNotificationsForUser->sendDeclineEmail($rentalObj);

                $send_to_unpaid = isset($fetchWalletData->send_to_unpaid) ? ($fetchWalletData->send_to_unpaid + 1 ) : "1";
                \App\UserWallet::where('user_id',$userId)->update(array('send_to_unpaid'=>$send_to_unpaid,'creadit_type'=>''));
                
                $dataArr = array(
                    'productId' => $rentalObj->prd_id,
                    'bookingNo' => $rentalObj->Bookingno,
                    'enqid' => $enquiryid,
                    'senderId' => \Auth::user()->id,
                    'receiverId' => $rentalObj->user_id,
                    'subject' => 'Booking Request : ' . $status,
                    'message' => "You Request Declined"
                );
                MedMessage::create($dataArr);
                flash($msg, 'success');
                return "success";
            }
        }

    public function updateSchoolHousebookingstatus(Request $request) {

        //Pending, Decline, Accept
        $status = $request->get('status');
        $enquiryid = $request->get('enquiryid');
        $rentalObj = RentalsEnquiry::find($enquiryid);
        $message = "";
        $msg = $status . " Successfully updated";
        //  if($_SERVER['REMOTE_ADDR'] == '93.36.120.53'){
        //     echo "<pre>";
        //         print_r($rentalObj);
        //     echo "</pre>";
        //     exit;            
        // }
        if ($status == 'Accept') {
            $user_voucher = User::where('id', $rentalObj->user_id)->first();
            $acceptedUserId =  \Auth::user()->id;
            $acceptedUser = User::with('group_info')->where('id', $acceptedUserId)->first();
            $propertyDetails = Property::where('id',$rentalObj->prd_id)->first();
            if($propertyDetails->property_status == 'unpaid' || $propertyDetails->property_status == 'unpaidflash'){
                    // print_r($rentalObj);
                    // echo "Accepted User : ".$acceptedUserId."<bR>";

                 $fetchWalletData = \App\UserWallet::where('user_id',$acceptedUserId)->first();
                 // echo "<br> Befoe Update <pre>";
                 //    print_r($fetchWalletData);
                 // echo "</pre><br> ";
                 // exit;

                // RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
                // $this->mailNotificationsForUser->sendDeclineEmail($rentalObj);

                $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 3 ) : "3";
                // $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 1 ) : "1";
                $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 0 ) : "0";
                //this for unapaid property. so we should change it here.
                \App\UserWallet::where('user_id',$acceptedUserId)->update(array('send_to_schoolhouse'=>$send_to_schoolhouse,'book_paid'=>$book_paid,'creadit_type'=>''));

                $fetchWalletData = \App\UserWallet::where('user_id',$acceptedUserId)->first();
                 // echo "<br> After update<pre>";
                 //    print_r($fetchWalletData);
                 // echo "</pre><br> ";    
            }
                $parentEmail = $rentalObj->user->email;
                $studentEmail = $rentalObj->user->semail16;
                
                $this->mailNotificationsForUser->sendemailPaidToStudentAboutWalletUpdateForSchoolHouseAfterJobAccept($parentEmail, $studentEmail, $rentalObj,$acceptedUser);            

            if(isset($user_voucher->vouchercredit_id) && !empty($user_voucher->vouchercredit_id)) {
                $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
            }
            $countEnquiry = 0;
            $allowedEnquiryCount = 0;
            if (isset($rentalObj->property->property_status) && $rentalObj->property->property_status == "paid") {
                $user_wallet = \App\UserWallet::where("user_id", $rentalObj->user_id)->first();
                if (isset($user_wallet->book_paid)) {
                    $allowedEnquiryCount = $user_wallet->book_paid;
                    $user_credits = \App\UserCredit::where("user_id", $rentalObj->user_id)->where("book_paid", ">", "0")->get();
                    if (sizeof($user_credits) > 0) {
                        $countEnquiry = $user_credits->count();
                    }
                }
            }
            
            if ($allowedEnquiryCount <= $countEnquiry) {
                $message = "The user does not have any credits for the request to be Approved, he or she has probably already been accepted elsewhere, that is found a place.";
                return redirect()->back()->with("error", $message);
            }

            //$newDateStart = date("Y-m-d", strtotime($rentalObj->checkin));
            //$newDateEnd = date("Y-m-d", strtotime($rentalObj->checkout));
            // echo "<br> CheckIn Date :".$rentalObj->checkin."<br> Checkout Date :".$rentalObj->checkout."<br>";
            $checkInDate = explode(" ",$rentalObj->checkin);
            $checkOutDate = explode(" ",$rentalObj->checkout);
            // echo "Cunt :".count($checkInDate)."<br>";
            if(count($checkInDate) > 1){
                $newDateStart = $rentalObj->checkin;
            }else{
                $newDateStart = date("Y-m-d H:i:s", strtotime($rentalObj->checkin." 11:00:00"));
            }

            // $checkOutDate

            if(count($checkOutDate) > 1){
                $newDateEnd = $rentalObj->checkout;
            }else{
                $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout." 10:00:00"));
            }

            // $newDateEnd = date("Y-m-d H:i:s", strtotime($rentalObj->checkout." 10:00:00"));
            $availableCheck = Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $rentalObj->prd_id);

/*if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.80.168'){
            echo "status :<br><pre>";
            echo "<br> CheckIn Date :".$newDateStart."<br>Checkout Date :".$newDateEnd."<br>";
            // print_r($_REQUEST);
            echo "CheckInd Date :".$rentalObj->prd_id."<br>";
            // $rentalObj['checkin']."<br> Checkin Date :".;
            // print_r($rentalObj);
            print_r($availableCheck);
            // var_dump($availableCheck);
            // exit;
        }*/

        // echo "Else part <pre>";
        //     print_r($_SERVER);
        // exit;
            //return $availableCheck;
            if (count($availableCheck) > 0) {
                //return "Fail";
                //flash($msg, 'Already you Approved one Request which is synced with same date');
                // $message = "NOTE: you have already accepted other requests for these dates, you may not accept more than one student of same nationality, this may be arranged only thorugh Admin, Thanks!".$newDateStart."_".$newDateEnd."_".$rentalObj->prd_id;
                $message = "NOTE: you have already accepted other requests for these dates, you may not accept more than one student of same nationality, this may be arranged only thorugh Admin, Thanks!";
                //return redirect()->back()->with("error", "Already you Approved one Request which is synced with same date");
                return redirect()->back()->with("message", $message);
            } else {
                $message = "Successfully updated";
            }
            RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
            $propertyId = $rentalObj->prd_id;
            $arrival = $rentalObj->checkin;
            $depature = $rentalObj->checkout;
            $dates = $this->getDatesFromRange($arrival, $depature);
            $this->saveBookedDates($dates, $propertyId);
            $this->updateScheduleCalendar($propertyId, 0);

            $dataArr = array(
                'productId' => $rentalObj->prd_id,
                'bookingNo' => $rentalObj->Bookingno,
                'enqid' => $enquiryid,
                'senderId' => \Auth::user()->id,
                'receiverId' => $rentalObj->user_id,
                'subject' => 'Booking Request : ' . $status,
                'message' => "You Request Approved"
            );
            MedMessage::create($dataArr);
            if ($status == 'Accept') {
                /* Send Learning Agreement */
                $cmsObj = $this->cmsRepository->getById(9);
                $stipulate = $this->cmsRepository->getById(12);
                $article1 = $this->cmsRepository->getById(13);
                $article2 = $this->cmsRepository->getById(14);
                $article3 = $this->cmsRepository->getById(15);
                $article4 = $this->cmsRepository->getById(16);
                $article5 = $this->cmsRepository->getById(17);
                $article6 = $this->cmsRepository->getById(18);
                $article7 = $this->cmsRepository->getById(19);
                $educationalobjectives = $this->cmsRepository->getById(20);
                $article9 = $this->cmsRepository->getById(21);
                $article10 = $this->cmsRepository->getById(22);

                $rentalObj = RentalsEnquiry::find($enquiryid);

                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $file_name = $timestamp . '-' . $rentalObj->Bookingno . ".pdf";

                //$pdf = PDF::loadView('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'))->save(public_path() . "/pdf/agreement/" . $file_name);
                //return $file_name ;
                $tutorEmail = $rentalObj->user->school->tutor_email;

                if (isset($rentalObj->prd_id) && !empty($rentalObj->prd_id)) {
                    $tutor_detail = Property::find($rentalObj->prd_id);
                    if (isset($tutor_detail->represent_email) && !empty($tutor_detail->represent_email)) {
                        $tutorEmail = $tutor_detail->represent_email;
                    }
                }

                $parentEmail = null;
                if (is_null($rentalObj->user->student_relationship)) {
                    $studentEmail = $rentalObj->user->email;
                } else {
                    $parentEmail = $rentalObj->user->email;
                    $studentEmail = $rentalObj->user->semail16;
                }

                // get HR details
                $hr_enquiry = RentalsEnquiry::where(["id" => $enquiryid, 'is_hr' => '1'])->first();
                $hr_email = "";
                $hr_email_detail = "";
                if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
                    $hr_user = User::find($hr_enquiry->renter_id);
                    $hr_email = isset($hr_user->email) ? $hr_user->email : '';
                    if (isset($hr->prd_id) && !empty($hr->prd_id)) {
                        $hr_email_detail = Property::find($hr->prd_id);
                    }
                }
//                echo "tutor email = ".$tutorEmail."<br />";
//                echo "HR email = ".$hr_email."<br />";
//                echo "Parent email = ".$parentEmail."<br />";
//                echo "Student email = ".$studentEmail."<br />";
//                echo "is HR = ".$rentalObj->is_hr."<br />";
//                exit;
                //$this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $file_name, $rentalObj, $hr_email, $hr_email_detail);
                //$pdf = \PDF::loadView('agreement', compact('rentalObj', 'cmsObj'));
                //return $pdf->download('invoice.pdf');
                //return View('agreement', compact('rentalObj', 'cmsObj'));
                //for paid emails
                $this->mailNotificationsForUser->sendemailPaidToStudentAboutSchoolHouse($parentEmail, $studentEmail, $rentalObj);
                $school_house_email = isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : "";
                //echo $school_house_email; exit;
                $this->mailNotificationsForUser->sendemailPaidToSchoolHouseAboutStudent($school_house_email, $rentalObj, $file_name);

                if ($rentalObj->is_hr == "1") {
                    $propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
                    $worktutoremail = $propertyObj->represent_email;
                    if (!empty($worktutoremail)) {
                        //$worktutoremail = $rentalObj->user->email;
                        $organizationemail = $propertyObj->user->orgemail;
                        //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj);
                    }
                    //echo $worktutoremail; exit;
                    //exit;
                    $checkIn = $rentalObj->checkin;
                    $checkOut = $rentalObj->checkout;
                    $noOfNyts = $rentalObj->numofdates;
                    $renterId = $propertyObj->user->id;
                    $dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => $rentalObj->user_id,
                        'renter_id' => $renterId,
                        'prd_id' => $rentalObj->prd_id
                    );


                    $booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $dataArr = array_merge($dataArr, $booking_status);
                    $enquiryObj = RentalsEnquiry::create($dataArr);
                    $insertid = $enquiryObj->id;

                    if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                        $val = 10 * $insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $newdata = array(
                            'Bookingno' => $bookingno
                        );
                        RentalsEnquiry::where('id', $insertid)->update($newdata);
                    }

                    $dataArr = array(
                        'productId' => $rentalObj->prd_id,
                        'bookingNo' => $bookingno,
                        'enqid' => $insertid,
                        'senderId' => $rentalObj->user_id,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    MedMessage::create($dataArr);
                }
            }
            //flash($msg, 'Successfully updated');
            $userCredit = new \App\UserCredit();
            $userCredit->user_id = $rentalObj->user_id;
            $userCredit->voucher_id = isset($get_voucher->id) ? $get_voucher->id : 0;
            $userCredit->property_id = isset($rentalObj->property->id) ? $rentalObj->property->id : 0;
            if (isset($rentalObj->property->property_status) && $rentalObj->property->property_status == "paid") {
                if($rentalObj->property->property_type == 26){
                    $userCredit->book_paid = $rentalObj->numofdates;
                }else{
                    $userCredit->book_paid = 1;
                }
            } else {
                $userCredit->book_unpaid = 1;
            }
            $userCredit->save();
            return redirect()->back()->with("message", $message);
        }

        if ($status == 'Decline') {
            $userId = $rentalObj->user_id;
            $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();

            RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
            $this->mailNotificationsForUser->sendDeclineEmail($rentalObj);

            $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 1 ) : "1";
            \App\UserWallet::where('user_id',$userId)->update(array('send_to_schoolhouse'=>$send_to_schoolhouse,'creadit_type'=>''));

            $dataArr = array(
                'productId' => $rentalObj->prd_id,
                'bookingNo' => $rentalObj->Bookingno,
                'enqid' => $enquiryid,
                'senderId' => \Auth::user()->id,
                'receiverId' => $rentalObj->user_id,
                'subject' => 'Booking Request : ' . $status,
                'message' => "You Request Declined"
            );
            MedMessage::create($dataArr);
            //flash($msg, 'Successfully updated');
            return redirect()->back()->with("message", "Successfully updated");
        }
    }

    public function tutorConfirmation($id) {
        $property = Property::find($id);
        $property->is_confirmed_by_tutor = "1";
        $property->save();
        return redirect()->to("user/dashboard/")->with("message", "Your Confirmation is accepted");
    }

    public function getwishlist() {
        $userObj = $this->userRepository->getLoginUser();
        return View('wishlistview', compact('userObj'));
    }

    public function getwishlistbyid($id) {
        $wishlistId = base64_decode($id);
        $wishlistobj = Wishlist::where('id', $wishlistId)->first();
        return View('wishlistdetail', compact('wishlistobj'));
    }

    public function removewishlist(Request $request) {
        $wishlistId = $request->get('wishlistid');
        $propertyId = $request->get('propertyId');
        $wishlistobj = Wishlist::where('id', $wishlistId)->first();
        $wishlistobj->properties()->detach($propertyId);
        return "Success";
    }

    public function addreview($id) {
        //echo base64_encode($id);exit;
        $id = base64_decode($id);
        $enquiryObj = RentalsEnquiry::find($id);
        return View('addreview', compact('enquiryObj'));
    }

    public function review_submit(Request $request) {
        $data = array('rateVal' => $request->get('ratingval'), 'reviewed_id' => $request->get('reviewedId'), 'reviewer_id' => $request->get('reviewerId'), 'prd_id' => $request->get('rentalId'), 'enquiryId' => $request->get('enquiryId'), 'description' => $request->get('description'));
        Review::create($data);
        flash("Review Added Successfully", 'success');
        return \Redirect::to('user/trips');
    }

    public function viewreviews($type) {
        $reviewObj = Review::where('reviewer_id', \Auth::user()->id)->where('status', 1)->get();
//        return $reviewObj;
        if ($type == "aboutyou") {
            $reviewObj = Review::where('reviewed_id', \Auth::user()->id)->where('status', 1)->get();
            return View('reviews', compact('reviewObj'));
        }
        return View('reviewby', compact('reviewObj'));
    }

    public function fetch_older_property_request(){
        // $fetchOldReqest = RentalsEnquiry::select('property.exptitle', 'property.title', 'fc_rentalsenquiry.user_id', 'fc_rentalsenquiry.prd_id')->leftjoin('property', 'property.id', '=', 'fc_rentalsenquiry.prd_id')->where('fc_rentalsenquiry.user_id', $userid)->whereIn('property.property_status', ['unpaid', 'unpaidflash'])->where('approval', 'Accept')->orderBy('fc_rentalsenquiry.id', 'desc')->get()->first();
        //SELECT DATEDIFF(DATE(NOW()),DATE(fc_rentalsenquiry.dateAdded) ) as ReqDateDiff, DATE(DATE(fc_rentalsenquiry.dateAdded) - INTERVAL 7 DAY) as DateInter, fc_rentalsenquiry.dateAdded, fc_rentalsenquiry.* FROM `fc_rentalsenquiry` WHERE booking_status ='Pending' AND DATEDIFF(DATE(NOW()),DATE(fc_rentalsenquiry.dateAdded) ) >= 7 /*AND DATE(dateAdded) < DATE(NOW() - INTERVAL 7 DAY)*/ ORDER BY `id`  DESC
        // $fetchOldReqest = RentalsEnquiry::select('DATEDIFF(DATE(NOW()),DATE(fc_rentalsenquiry.dateAdded)) as dateDiff', 'dateAdded')->where('approval', 'pending')->orderBy('fc_rentalsenquiry.id', 'desc')->get();
        /*$fetchOldRequest = DB::table('fc_rentalsenquiry as fr')->join('property as p','fr.prd_id','=','p.id')->selectRaw('DATEDIFF(DATE(NOW()),DATE(fr.dateAdded)) as dateDiff,fr.dateAdded,fr.id,fr.user_id,fr.approval,fr.booking_status,fr.checkin,fr.checkout,p.exptitle')->where('fr.booking_status', 'Pending')->where(function($q){
            $q->where('fr.approval', 'Pending')->orWhere('fr.approval', 'Accept');
        })->whereRaw('DATEDIFF(DATE(NOW()),DATE(fr.dateAdded))=15')->whereIn('property_status',array('unpaid','unpaidflash'))->get();*/

        $fetchOldRequest = DB::table('fc_rentalsenquiry as fr')->join('property as p','fr.prd_id','=','p.id')->selectRaw('DATEDIFF(DATE(NOW()),DATE(fr.dateAdded)) as dateDiff,fr.dateAdded,fr.id,fr.user_id,fr.approval,fr.booking_status,fr.checkin,fr.checkout,p.exptitle')
        ->whereNotIn('fr.user_id',function($query){
               $query->select('fc_rentalsenquiry.user_id')->from('fc_rentalsenquiry')->whereRaw('DATEDIFF(DATE(NOW()),DATE(fc_rentalsenquiry.dateAdded))=8')->where('fc_rentalsenquiry.approval', 'Accept')->get();
            })->where(function($q){
            $q->where('fr.approval', 'Pending')->orWhere('fr.approval', 'Accept');
        })->whereRaw('DATEDIFF(DATE(NOW()),DATE(fr.dateAdded))=8')->whereIn('property_status',array('unpaid','unpaidflash'))->get();

        echo "<prE>";
                print_r($fetchOldRequest);
        echo "</prE>";
        exit;


        foreach($fetchOldRequest as $key=>$val){
            // echo "<pre>";
            //     print_r($val);
            // echo "</pre>";  
            $userId = $val->user_id;
            $userData = \App\User::select('email','name','lastname')->where('id',$userId)->first();
            if($val->approval != 'Accept'){
               //update the wallet of user
                // \App\UserWallet::where('user_id',$val->user_id)->update();
            $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();
            $send_to_unpaid = isset($fetchWalletData->send_to_unpaid) ? ($fetchWalletData->send_to_unpaid + 1 ) : "1";
            \App\UserWallet::where('user_id',$val->user_id)->update(array('send_to_unpaid'=>$send_to_unpaid,'creadit_type'=>''));
            }
            $email = $userData->email;
            $name = trim($userData->name." ".$userData->lastname);
            $check_in = $val->checkin;
            $checkout = $val->checkout;
            $custArray = ['property_title'=>$val->exptitle,"check_in"=>$check_in,"check_out"=>$checkout,"name"=>$name];
            // echo "<pre>";
            //     print_r($custArray);
            // echo "</pre>";
            Mail::send('emails.olderrequesttemplate', $custArray, function ($m) use ($email,$name,$check_in,$checkout) {
                // echo "Email :".$email."<br> Checkin :".$check_in."<br> Checkout :".$checkout;exit;
                    $m->to($email, $name)->subject('Work Tutor reply still pending - Please send new requests');
                });

            exit;
                      
        }
        exit;
    }    
    public function send_test_email(){
        $to = 'hrtesting@webhomes.net';
        var_dump(Mail::send([],[],function($message){
            $message->to('hrtesting@webhomes.net')->subject('Testing mail')->setBody('HEllo Test');
        }));
        // Mail::send('this is test mail', ['form_data' => $email_data], function ($m) use ($email_data) {
        // Mail::send('this is test mail',  function ($m){
        //             $m->to('hrtesting@webhomes.net','Testing HR NAme')->subject('Testing Human Resource - You have received a request'); //HR receives same request as User Listing and may accept or decline a student applying for a job, just like the User Listing himself
        //         });
    }

}
