<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\RentalsEnquiry;
use App\Repositories\PropertyRepository;

class TripController extends Controller {

    protected $propertyRepository;

    public function __construct(PropertyRepository $propertyRepository) {
        $this->propertyRepository = $propertyRepository;
    }

    public function showTrips() {
        
        $userId = \Auth::user()->id;
        $tripsObj = RentalsEnquiry::with('property')->where('user_id', $userId)->where('cancelled', 'No')->orderBy('dateAdded', 'desc')->get();
       
        return View('trips', compact('tripsObj'));
    }

    public function showReservations($status = 'all') {
        $userId = \Auth::user()->id;
        if ($status == "all") {
            $tripsObj = RentalsEnquiry::where('renter_id', $userId)->where('cancelled', 'No')->orderBy('dateAdded', 'desc')->get();
        }
        if ($status == "unapprove") {
            $tripsObj = RentalsEnquiry::where('renter_id', $userId)->where('approval', 'Pending')->where('cancelled', 'No')->orderBy('dateAdded', 'desc')->get();
        }
        if ($status == "cancelled") {
            $tripsObj = RentalsEnquiry::where('renter_id', $userId)->where('approval', 'Pending')->orderBy('dateAdded', 'desc')->get();
        }
        return View('reservation', compact('tripsObj'));
    }

    public function schoolHouses($status = 'all') {
       
        $userId = \Auth::user()->id;
        if ($status == "all") {
            $tripsObj = RentalsEnquiry::where('renter_id', $userId)->where('cancelled', 'No')->orderBy('dateAdded', 'desc')->get();
        }
        if ($status == "unapprove") {
            $tripsObj = RentalsEnquiry::where('renter_id', $userId)->where('approval', 'Pending')->where('cancelled', 'No')->orderBy('dateAdded', 'desc')->get();
        }
        if ($status == "cancelled") {
            $tripsObj = RentalsEnquiry::where('renter_id', $userId)->where('approval', 'Pending')->orderBy('dateAdded', 'desc')->get();
        }
        return View('school_house_reservation', compact('tripsObj'));
    }

}
