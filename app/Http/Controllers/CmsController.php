<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CmsRepository;
use App\Services\Constants\AppConstants;
use App\RentalsEnquiry;

class CmsController extends Controller
{
    private $bredCrum = "CMS Page";
    
    protected $cmsRepository;

    public function __construct(CmsRepository $cmsRepository)
    {
        $this->cmsRepository = $cmsRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->cmsRepository->getAll();
        return View('admin.cms.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        return View('admin.cms.create', compact('bredCrum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('page_name' => $request->get('page_name'),
                        'page_desc' => $request->get('page_desc'),
                        'status' => $request->get('status')
                    );
        $validator = Validator::make($postData, $this->cmsRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/cms/create')->withErrors($validator);
        } else {
            $Obj = $this->cmsRepository->create($postData);
            flash('Page Added Successfully', 'success');
            return \Redirect('admin/cms');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->cmsRepository->getById($id);
        return View('admin.cms.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $postData = array('page_name' => $request->get('page_name'),
                        'page_desc' => $request->get('page_desc'),
                        'status' => $request->get('status')
                    );
        $validator = Validator::make($postData, $this->cmsRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/propertytype/'.$id.'/edit')->withErrors($validator);
        } else {
            $Obj = $this->cmsRepository->update($postData, $id);
            flash('Page Updated Successfully', 'success');
            return \Redirect('admin/cms');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function agree($enquiryid)
    {
        $cmsObj = $this->cmsRepository->getById(9);
        $stipulate = $this->cmsRepository->getById(12);
        $article1 = $this->cmsRepository->getById(13);
        $article2 = $this->cmsRepository->getById(14);
        $article3 = $this->cmsRepository->getById(15);
        $article4 = $this->cmsRepository->getById(16);
        $article5 = $this->cmsRepository->getById(17);
        $article6 = $this->cmsRepository->getById(18);
        $article7 = $this->cmsRepository->getById(19);
        $educationalobjectives = $this->cmsRepository->getById(20);
        $article9 = $this->cmsRepository->getById(21);
        $article10 = $this->cmsRepository->getById(22);


        $rentalObj = RentalsEnquiry::find($enquiryid);
        return View('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2','article3','article4','article5','article6','article7','article9','article10','stipulate','educationalobjectives'));
        //$pdf = \PDF::loadView('agreement', compact('rentalObj', 'cmsObj'));
        //return $pdf->download('invoice.pdf');
        //return View('agreement', compact('rentalObj', 'cmsObj'));
    }
    
    public function showPage($pageName)
    {
        if ($pageName == 'terms') {
            $cmsObj = $this->cmsRepository->getById(AppConstants::TERMS_AND_CONDITIONS);
        } else if ($pageName == 'privacypolicy') {
            $cmsObj = $this->cmsRepository->getById(AppConstants::PRIVACY_POLICY);
        } else if ($pageName == 'howitworks') {
            $cmsObj = $this->cmsRepository->getById(AppConstants::HOW_WORKS);
        } else if ($pageName == 'howdoistart') {
            $cmsObj = $this->cmsRepository->getById(AppConstants::WHY_HOST);
        } else if ($pageName == 'trustandsafety') {
            $cmsObj = $this->cmsRepository->getById(AppConstants::TRUST_SAFETY);
        }
        else if ($pageName == 'presentation') {
            $cmsObj = $this->cmsRepository->getById(7);
        } 
        else if ($pageName == 'responsibility') {
            $cmsObj = $this->cmsRepository->getById(8);
        } 
        else if ($pageName == 'Netiquette') {
            $cmsObj = $this->cmsRepository->getById(10);
        }
        else if ($pageName == 'LearningAgreement_Italy') {
            $cmsObj = $this->cmsRepository->getById(11);
        }
        
        else {
            flash('No Page Found', 'failure');
            \Redirect('/');
        }
        return View('staticpage', compact('cmsObj'));
    }

    public function aboutus()
    {
        $cmsObj = $this->cmsRepository->getById(AppConstants::ABOUT_US);
        return View('staticpage', compact('cmsObj'));
    }

}
