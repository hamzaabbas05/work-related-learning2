<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\LanguageController;
use App\Repositories\LanguageRepository;

class LanguageController extends Controller
{
    private $bredCrum = "Language";
    
    protected $languageRepository;

    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->languageRepository->getAll();
        return View('admin.language.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        return View('admin.language.create', compact('bredCrum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('language_name' => $request->get('language_name'),
                        'language_code' => $request->get('language_code'),
                        'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->languageRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/language/create')->withErrors($validator);
        } else {
            $Obj = $this->languageRepository->create($postData);
            flash('Language Added Successfully', 'success');
            return \Redirect('admin/language');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->languageRepository->getById($id);
        return View('admin.language.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = array('language_name' => $request->get('language_name'),
                        'language_code' => $request->get('language_code'),
                        'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->languageRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/language/'.$id.'/edit')->withErrors($validator);
        } else {
            $Obj = $this->languageRepository->update($postData, $id);   
            flash('Language Updated Successfully', 'success');
            return \Redirect('admin/language');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
