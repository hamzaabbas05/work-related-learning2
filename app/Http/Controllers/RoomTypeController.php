<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\RoomtypeRepository;

class RoomTypeController extends Controller
{
    private $bredCrum = "RoomType";
    
    protected $roomTypeRepository;

    public function __construct(RoomtypeRepository $roomTypeRepository)
    {
        $this->roomTypeRepository = $roomTypeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->roomTypeRepository->getAll();
        return View('admin.roomtype.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        return View('admin.roomtype.create', compact('bredCrum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('name' => $request->get('name'),
                        'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->roomTypeRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/roomtype/create')->withErrors($validator);
        } else {
            $roomTypeObj = $this->roomTypeRepository->create($postData);
            flash('Room Type Added Successfully', 'success');
            return \Redirect('admin/roomtype');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->roomTypeRepository->getById($id);
        return View('admin.roomtype.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = array('name' => $request->get('name'),
                        'status' => $request->get('status'),
                    );
        $validator = Validator::make($postData, $this->roomTypeRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/roomtype/'.$id.'/edit')->withErrors($validator);
        } else {
            $roomTypeObj = $this->roomTypeRepository->update($postData, $id);   
            flash('Room Type Updated Successfully', 'success');
            return \Redirect('admin/roomtype');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
