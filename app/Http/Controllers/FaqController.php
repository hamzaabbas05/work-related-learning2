<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\FaqRepository;

class FaqController extends Controller
{
    private $bredCrum = "FAQ";
    
    protected $faqRepository;

    public function __construct(FaqRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->faqRepository->getAll();
        return View('admin.faq.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        return View('admin.faq.create', compact('bredCrum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('question' => $request->get('question'),'answer' => $request->get('answer'),'status' => $request->get('status'),'ftype' => $request->get('ftype')
                    );
        $validator = Validator::make($postData, $this->faqRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/faq/create')->withErrors($validator);
        } else {
            $propertyTypeObj = $this->faqRepository->create($postData);
            flash('FAQ Added Successfully', 'success');
            return \Redirect('admin/faq');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->faqRepository->getById($id);
        return View('admin.faq.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = array('question' => $request->get('question'),'answer' => $request->get('answer'),'status' => $request->get('status'),'ftype' => $request->get('ftype')
                    );
        $validator = Validator::make($postData, $this->faqRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/faq/'.$id.'/edit')->withErrors($validator);
        } else {
            $obj = $this->faqRepository->update($postData, $id);
            flash('Faq Updated Successfully', 'success');
            return \Redirect('admin/faq');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showFaq($type)
    {
        $ftype = 2;
        if ($type == 'work') {
            $ftype =1;
        }if ($type == 'schoolhouses') {
            $ftype =3;
        }
        $faqObject = $this->faqRepository->getByType($ftype);
        return View('faq', compact('faqObject'));
    }
}
