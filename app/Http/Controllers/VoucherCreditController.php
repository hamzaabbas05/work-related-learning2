<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\VoucherCredit;

class VoucherCreditController extends Controller {

    private $bredCrum = "Voucher Credits";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $bredCrum = $this->bredCrum;
        $currentObj = VoucherCredit::get();
        return View('admin.vouchercredits.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $bredCrum = $this->bredCrum;
        $users = \App\User::with('school')->get();
        
        //name and surname, emails of both parent and student and school name
        return View('admin.vouchercredits.create', compact('bredCrum', 'users'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createforpreenroll() {
        $bredCrum = $this->bredCrum;
        $users = \App\User::get();
        return View('admin.vouchercredits.createforpreenroll', compact('bredCrum', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $messages = [
            'voucher_code.required' => "Voucher Code cannot be empty",
            'voucher_code.max' => "Voucher Code cannot be greater than 255 characters",
            'voucher_code.unique' => "Voucher Code already exists",
            'amount.required' => "Amount cannot be empty",
            'send_to_unpaid.required' => "Send to Unpaid cannot be empty",
            'book_unpaid.required' => "Book Unpaid cannot be empty",
            'send_to_schoolhouse.required' => "Send to Schoolhouse cannot be empty",
            'book_paid.required' => "Book Paid cannot be empty",
           // 'email.required' => "Book Paid cannot be empty",
        ];

        $this->validate($request, [
            'voucher_code' => 'required|max:255|unique:voucher_credits',
            'amount' => 'required',
            'send_to_unpaid' => 'required',
            'book_unpaid' => 'required',
            'send_to_schoolhouse' => 'required',
            'book_paid' => 'required',
            // 'email' => 'required|unique:users',
                ], $messages);



        // checking if user have already voucher wallet credits
//        $check_wallet = \App\UserWallet::where("user_id", $user_id)->first();
//        if(isset($check_wallet->id)) {
//            return redirect()->back()->withInput()->with("error", "User already have a wallet credits");
//        }

        $voucher = new VoucherCredit();
        $voucher->voucher_code = $request->get("voucher_code");
        $voucher->amount = $request->get("amount");
        $voucher->send_to_unpaid = $request->get("send_to_unpaid");
        $voucher->book_unpaid = $request->get("book_unpaid");
        $voucher->send_to_schoolhouse = $request->get("send_to_schoolhouse");
        $voucher->book_paid = $request->get("book_paid");
        $voucher->save();
        if (isset($voucher->id) && $voucher->id > 0) {
                $user_email = $request->get("email");
                if(!empty($user_email)){
                    $email_message = \App\Services\Mailer\MailNotificationsForUser::sendAdminUserVoucherCode($user_email, $voucher);
                }
//            echo $email_message; exit;
//            $user_id = $request->get("user_id");
//            if (!empty($user_id) && $user_id > 0) {
//                $user = \App\User::where("id", $user_id)->first();
//                if (isset($user->id) && $user->id > 0) {
//                    $user->vouchercredit_id = $voucher->id;
//                    $user->save();
//                    if (!isset($check_wallet->id)) {
//                        $userWallet = new \App\UserWallet();
//                        $userWallet->user_id = $user_id;
//                        $userWallet->vouchaercredits_id = $voucher->id;
//                        $userWallet->amount = isset($voucher->amount) ? $voucher->amount : "0";
//                        $userWallet->send_to_unpaid = isset($voucher->send_to_unpaid) ? $voucher->send_to_unpaid : "0";
//                        $userWallet->book_unpaid = isset($voucher->book_unpaid) ? $voucher->book_unpaid : "0";
//                        $userWallet->send_to_schoolhouse = isset($voucher->send_to_schoolhouse) ? $voucher->send_to_schoolhouse : "0";
//                        $userWallet->book_paid = isset($voucher->book_paid) ? $voucher->book_paid : "0";
//                        $userWallet->creadit_type = "voucher";
//                        $userWallet->save();
//                        
//                    }
//                }
//            }
        }
        flash('Voucher Credits has been added Successfully', 'success');
        return redirect()->to('admin/voucherCredit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storepreenrolled(Request $request) {
        $messages = [
            'voucher_code.required' => "Voucher Code cannot be empty",
            'voucher_code.max' => "Voucher Code cannot be greater than 255 characters",
            'voucher_code.unique' => "Voucher Code already exists",
            'amount.required' => "Amount cannot be empty",
            'send_to_unpaid.required' => "Send to Unpaid cannot be empty",
            'book_unpaid.required' => "Book Unpaid cannot be empty",
            'send_to_schoolhouse.required' => "Send to Schoolhouse cannot be empty",
            'book_paid.required' => "Book Paid cannot be empty",
            'email.required' => "Book Paid cannot be empty",
        ];

        $this->validate($request, [
            'voucher_code' => 'required|max:255|unique:voucher_credits',
            'amount' => 'required',
            'send_to_unpaid' => 'required',
            'book_unpaid' => 'required',
            'send_to_schoolhouse' => 'required',
            'book_paid' => 'required',
            // 'email' => 'required|unique:users',
            'email' => 'required',
                ], $messages);
        // if($_SERVER['REMOTE_ADDR'] == '2.43.166.247'){
        //     echo "<pre>";
        //         print_r($_REQUEST);
        //     echo "</pre>";
        //     // exit;
        // }

        // checking if user have already voucher wallet credits
//        $check_wallet = \App\UserWallet::where("user_id", $user_id)->first();
//        if(isset($check_wallet->id)) {
//            return redirect()->back()->withInput()->with("error", "User already have a wallet credits");
//        }


        $voucher = new VoucherCredit();
        $voucher->voucher_code = $request->get("voucher_code");
        $voucher->amount = $request->get("amount");
        $voucher->send_to_unpaid = $request->get("send_to_unpaid");
        $voucher->book_unpaid = $request->get("book_unpaid");
        $voucher->send_to_schoolhouse = $request->get("send_to_schoolhouse");
        $voucher->book_paid = $request->get("book_paid");
        // if($_SERVER['REMOTE_ADDR'] != '93.42.44.112'){
            $voucher->save();
        // }else{
        //     $voucher->id = 5211;            
        // }
         //echo "<pre>.";   print_r($voucher);die;
        if (isset($voucher->id) && $voucher->id > 0) {
            $user_email = $request->get("email");
            $users = \App\User::with('school')->where('email',$user_email)->get();
           $email_message = \App\Services\Mailer\MailNotificationsForUser::sendAdminUserVoucherCodetoPreEnrolled($user_email, $users, $voucher);
           // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
           //   echo "Is Email Message:".$email_message; exit;
           // }
           $user_id = $users[0]->id;//$request->get("user_id");
           if (!empty($user_id) && $user_id > 0) {
               $user = \App\User::where("id", $user_id)->first();
               if (isset($user->id) && $user->id > 0) {
                   $user->vouchercredit_id = $voucher->id;
                   $user->save();
                   if (!isset($check_wallet->id)) {

                       //$userWallet = new \App\UserWallet();
                       $userWallet = \App\UserWallet::where("user_id",$user_id)->first();
                        // echo "<pre>";
                        //     print_r($userWallet);  
                        // echo "</pre>";
                        // exit;
                       // if(count($userWallet) > 0){ // if already exist then update the wallet count
                       // if(sizeof($userWallet) > 0){ // if already exist then update the wallet count
                       if(isset($userWallet->id)){ // if already exist then update the wallet count
                        // echo "Fall in IF ";
                           $userWallet->amount = (isset($userWallet->amount) ? ($userWallet->amount + $voucher->amount) : "0");
                           //isset($voucher->amount) ? $voucher->amount : "0";
                           $userWallet->send_to_unpaid = (isset($userWallet->send_to_unpaid) ? ($userWallet->send_to_unpaid + $voucher->send_to_unpaid) : "0");
                           // isset($voucher->send_to_unpaid) ? $voucher->send_to_unpaid : "0";
                           $userWallet->book_unpaid =  (isset($userWallet->book_unpaid) ? ($userWallet->book_unpaid + $voucher->book_unpaid) : "0");
                           //isset($voucher->book_unpaid) ? $voucher->book_unpaid : "0";
                           $userWallet->send_to_schoolhouse = (isset($userWallet->send_to_schoolhouse) ? ($userWallet->send_to_schoolhouse + $voucher->send_to_schoolhouse) : "0"); 
                           //isset($voucher->send_to_schoolhouse) ? $voucher->send_to_schoolhouse : "0";
                           $userWallet->book_paid =  (isset($userWallet->book_paid) ? ($userWallet->book_paid + $voucher->book_paid) : "0"); 
                            $userWallet->user_id = $user_id;
                            $userWallet->vouchaercredits_id = $voucher->id;
                            $userWallet->creadit_type = "voucher";  

                            $userWallet->update();
                           //isset($voucher->book_paid) ? $voucher->book_paid : "0";
                       }else{ // user not exist so save the fresh data.
                       // echo "Fall in Else ";
                           $userWallet = new \App\UserWallet();
                           $userWallet->user_id = $user_id;
                           $userWallet->vouchaercredits_id = $voucher->id;
                           $userWallet->amount = isset($voucher->amount) ? $voucher->amount : "0";
                           $userWallet->send_to_unpaid = isset($voucher->send_to_unpaid) ? $voucher->send_to_unpaid : "0";
                           $userWallet->book_unpaid = isset($voucher->book_unpaid) ? $voucher->book_unpaid : "0";
                           $userWallet->send_to_schoolhouse = isset($voucher->send_to_schoolhouse) ? $voucher->send_to_schoolhouse : "0";
                           $userWallet->book_paid = isset($voucher->book_paid) ? $voucher->book_paid : "0";
                           $userWallet->creadit_type = "voucher";
                           $userWallet->save();
                        
                       }
                   }
               }
           }
        } 
        flash('Voucher Credits has been added Successfully', 'success');
        return redirect()->to('admin/voucherCredit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $bredCrum = $this->bredCrum;
        $editObj = VoucherCredit::find($id);
        return View('admin.vouchercredits.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $messages = [
            'voucher_code.required' => "Voucher Code cannot be empty",
            'voucher_code.max' => "Voucher Code cannot be greater than 255 characters",
            'voucher_code.unique' => "Voucher Code already exists",
            'amount.required' => "Amount cannot be empty",
            'send_to_unpaid.required' => "Send to Unpaid cannot be empty",
            'book_unpaid.required' => "Book Unpaid cannot be empty",
            'send_to_schoolhouse.required' => "Send to Schoolhouse cannot be empty",
            'book_paid.required' => "Book Paid cannot be empty",
        ];

        $this->validate($request, [
            'voucher_code' => 'required|max:255|unique:vouchers',
            'amount' => 'required',
            'send_to_unpaid' => 'required',
            'book_unpaid' => 'required',
            'send_to_schoolhouse' => 'required',
            'book_paid' => 'required',
                ], $messages);

        $voucher = VoucherCredit::find($id);
        $voucher->voucher_code = $request->get("voucher_code");
        $voucher->amount = $request->get("amount");
        $voucher->send_to_unpaid = $request->get("send_to_unpaid");
        $voucher->book_unpaid = $request->get("book_unpaid");
        $voucher->send_to_schoolhouse = $request->get("send_to_schoolhouse");
        $voucher->book_paid = $request->get("book_paid");
        $voucher->save();
        flash('Voucher Credits has been updated Successfully', 'success');
        return redirect()->to('admin/voucherCredit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function assignUser() {
        $bredCrum = "Assign Voucher To User";
        $users = \App\User::get();
        return View('admin.vouchercredits.assignUser', compact('bredCrum', 'users'));
    }

    public function addVoucher(Request $request) {
        
        $messages = [
            'voucher_code.required' => "Voucher Code cannot be empty",
            'voucher_code.max' => "Voucher Code cannot be greater than 255 characters",
            'voucher_code.unique' => "Voucher Code already exists",
            'amount.required' => "Amount cannot be empty",
            'send_to_unpaid.required' => "Send to Unpaid cannot be empty",
            'book_unpaid.required' => "Book Unpaid cannot be empty",
            'send_to_schoolhouse.required' => "Send to Schoolhouse cannot be empty",
            'book_paid.required' => "Book Paid cannot be empty",
            'user_id.required' => "Please Select User",
        ];

        $this->validate($request, [
            'voucher_code' => 'required|max:255|unique:voucher_credits',
            'amount' => 'required',
            'send_to_unpaid' => 'required',
            'book_unpaid' => 'required',
            'send_to_schoolhouse' => 'required',
            'book_paid' => 'required',
            'user_id' => 'required',
                ], $messages);

        $voucher = new VoucherCredit();
        $voucher->voucher_code = $request->get("voucher_code");
        $voucher->amount = $request->get("amount");
        $voucher->send_to_unpaid = $request->get("send_to_unpaid");
        $voucher->book_unpaid = $request->get("book_unpaid");
        $voucher->send_to_schoolhouse = $request->get("send_to_schoolhouse");
        $voucher->book_paid = $request->get("book_paid");
        $voucher->save();
                // $voucher->id = 1001;
        if (isset($voucher->id) && $voucher->id > 0) {
            $user_id = $request->get("user_id");
            $user = \App\User::where("id", $user_id)->first();
            if (isset($user->id)) {
                if (isset($user->id) && $user->id > 0) {
                    $user->vouchercredit_id = $voucher->id;
                    $user->save();
                    // checking if user have already voucher wallet credits
                    $check_wallet = \App\UserWallet::where("user_id", $user_id)->first();

                    if (!empty($check_wallet->id) && $check_wallet->id != '') {
                        // echo "In If";
                        $userWallet = \App\UserWallet::where("user_id", $user_id)->first();
                        $userWallet->vouchaercredits_id = $voucher->id;
                        $userWallet->amount = isset($voucher->amount) ? $voucher->amount + $check_wallet->amount : $check_wallet->amount;
                        $userWallet->send_to_unpaid = isset($voucher->send_to_unpaid) ? $voucher->send_to_unpaid + $check_wallet->send_to_unpaid : $check_wallet->send_to_unpaid;
                        $userWallet->book_unpaid = isset($voucher->book_unpaid) ? $voucher->book_unpaid + $check_wallet->book_unpaid : $check_wallet->book_unpaid;
                        $userWallet->send_to_schoolhouse = isset($voucher->send_to_schoolhouse) ? $voucher->send_to_schoolhouse + $check_wallet->send_to_schoolhouse : $check_wallet->send_to_schoolhouse;
                        $userWallet->book_paid = isset($voucher->book_paid) ? $voucher->book_paid + $check_wallet->book_paid : $check_wallet->book_paid;
                    } else {
                        // echo "In Else";
                        $userWallet = new \App\UserWallet();
                        $userWallet->user_id = $user_id;
                        $userWallet->vouchaercredits_id = $voucher->id;
                        $userWallet->amount = isset($voucher->amount) ? $voucher->amount : "0";
                        $userWallet->send_to_unpaid = isset($voucher->send_to_unpaid) ? $voucher->send_to_unpaid : "0";
                        $userWallet->book_unpaid = isset($voucher->book_unpaid) ? $voucher->book_unpaid : "0";
                        $userWallet->send_to_schoolhouse = isset($voucher->send_to_schoolhouse) ? $voucher->send_to_schoolhouse : "0";
                        $userWallet->book_paid = isset($voucher->book_paid) ? $voucher->book_paid : "0";
                    }
                    $userWallet->creadit_type = "voucher";
                    $userWallet->save();

                    $email_message = \App\Services\Mailer\MailNotificationsForUser::sendAdminAddUserVoucherCode($user, $voucher);
                }
            }
        }
        flash('Voucher Credits has been added Successfully', 'success');
        return redirect()->to('admin/voucherCredit');
    }

}
