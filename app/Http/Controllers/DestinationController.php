<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\DestinationRepository;
use Carbon\Carbon;

class DestinationController extends Controller
{
    private $bredCrum = "Destination";
    
    protected $destinationRepository;

    public function __construct(DestinationRepository $destinationRepository)
    {
        $this->destinationRepository = $destinationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bredCrum = $this->bredCrum;
        $currentObj = $this->destinationRepository->getAll();
        return View('admin.destination.index', compact('bredCrum', 'currentObj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bredCrum = $this->bredCrum;
        return View('admin.destination.create', compact('bredCrum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = array('name' => $request->get('name'),
                        'display_name' => $request->get('display_name'),
                        'lattitude' => $request->get('lattitude'),
                        'longitude' => $request->get('longitude'),
                        'status' => $request->get('status')
                    );
        $validator = Validator::make($postData, $this->destinationRepository->validationRule);
        if ($validator->fails()) {
             return \Redirect('admin/destination/create')->withErrors($validator);
        } else {
            $Obj = $this->destinationRepository->create($postData);
            flash('Destination Added Successfully', 'success');
            return \Redirect('admin/destination');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bredCrum = $this->bredCrum;
        $editObj = $this->destinationRepository->getById($id);
        return View('admin.destination.edit', compact('bredCrum', 'editObj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = array('name' => $request->get('name'),
                        'display_name' => $request->get('display_name'),
                        'lattitude' => $request->get('lattitude'),
                        'longitude' => $request->get('longitude'),
                        'status' => $request->get('status')
                    );
        $validator = Validator::make($postData, $this->destinationRepository->editValidationRules($id));
        if ($validator->fails()) {
             return \Redirect('admin/destination/'.$id.'/edit')->withErrors($validator);
        } else {
            $Obj = $this->destinationRepository->update($postData, $id);
            flash('Destination Updated Successfully', 'success');
            return \Redirect('admin/destination');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
