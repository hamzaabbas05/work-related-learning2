<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PropertytypeRepository;
use App\Repositories\RoomtypeRepository;
use App\Repositories\PropertyRepository;
use App\Repositories\AmenityRepository;
use App\Repositories\SpecialAttributesRepository;
use App\Services\Constants\AppConstants;
use App\Repositories\CancellationRepository;
use App\Repositories\CommissionRepository;
use App\Repositories\UserRepository;
use App\Repositories\CmsRepository;
use App\PropertyImages;
use App\RoomCalendar;
use App\PropertySettings;
use App\Property;
use App\Wishlist;
use Carbon\Carbon;
use App\Booking;
use App\RentalsEnquiry;
use App\Review;
use App\MedMessage;
use App\Amenity;
use App\Schedule;
use App\Tutor;
use App\Services\Mailer\MailNotificationsForUser;
use Mail;
use App\User;
use Dompdf\Dompdf as Dompdf;
use PDF;

class BookingGeneratorController extends Controller {

    private $bredCrum = "Booking Generator";
    protected $propertyTypeRepository;
    protected $roomTypeRepository;
    protected $propertyRepository;
    protected $amenityRepository;
    protected $specialAttributesRepository;
    protected $cancellationRepository;
    protected $userRepository;
    protected $commissionRepository;
    protected $cmsRepository;
    private $mailNotificationsForUser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(PropertytypeRepository $propertyTypeRepository, RoomtypeRepository $roomTypeRepository, PropertyRepository $propertyRepository, AmenityRepository $amenityRepository, SpecialAttributesRepository $specialAttributesRepository, CancellationRepository $cancellationRepository, UserRepository $userRepository, CommissionRepository $commissionRepository, CmsRepository $cmsRepository, MailNotificationsForUser $mailNotificationsForUser) {
        $this->propertyTypeRepository = $propertyTypeRepository;
        $this->roomTypeRepository = $roomTypeRepository;
        $this->propertyRepository = $propertyRepository;
        $this->amenityRepository = $amenityRepository;
        $this->specialAttributesRepository = $specialAttributesRepository;
        $this->cancellationRepository = $cancellationRepository;
        $this->userRepository = $userRepository;
        $this->commissionRepository = $commissionRepository;
        $this->cmsRepository = $cmsRepository;
        $this->mailNotificationsForUser = $mailNotificationsForUser;
    }

    public function index() {
        $bredCrum = $this->bredCrum;
        $users = \App\User::get();
        $jobs = \App\Property::where(['status' => 1])->where('property_status', '!=', 'paid')->get(); //1 stands for unpaid and unpaidflash - 2 stands for paid - 0 stands for deleted or inactive
        return View('admin.booking_generator.booking', compact('bredCrum', 'users', 'jobs'));
    }

    // unpaid or unpaidflash pending booking from admin
    public function create(Request $request) {
        //return $request->all();
        $userId = $request->get("user_id");
        $userEnquiries = \App\RentalsEnquiry::where('user_id', $userId)->get();
        $checkIn = date('Y-m-d 11:00:00', strtotime($request->get('startdate')));
        $checkOut = date('Y-m-d 10:00:00', strtotime($request->get('enddate')));
        $allowBooking = 0;
        $countEnquiry = 0;
        $settingsObj = \App\PropertySettings::propertySettings();
        $allowedEnquiryCount = $settingsObj->booking_request;
        $allowedEnquiryCount = $allowedEnquiryCount + 100;
        //echo $allowedEnquiryCount."<br />";
        if ($userEnquiries) {
            $countEnquiry = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
        }
        //echo $countEnquiry;
        //exit;
        if ($countEnquiry < $allowedEnquiryCount) {
            $allowBooking = 1;
        }
        if ($allowBooking == 0) {
            return redirect()->back()->with("error", "your limit is exceeded");
        } else {
            $productId = $request->get('job_id');
            $propertyObj = $this->propertyRepository->getCurrentObject($productId);

            $renterId = $propertyObj->user->id;
            $now = strtotime($checkIn); // or your date as well
            $your_date = strtotime($checkOut);
            $datediff = $your_date - $now;

            $noOfNyts = floor($datediff / (60 * 60 * 24));
            $msg = "";
            $worktutoremail = $propertyObj->represent_email;
            $organizationemail = $propertyObj->user->orgemail;
            $bookingno = "";
            $hr_email = $propertyObj->hr_represent_email;
            $check_hr = $propertyObj->human_resources;
            if (($check_hr == 0) && !empty($hr_email)) {
                //echo "yes inside hr"; exit;
                $hr_details = \App\User::where('email', $hr_email)->first();
                if (isset($hr_details->id) && !empty($hr_details->id)) {
                    $hr_dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => $userId,
                        'renter_id' => $renterId,
                        'is_hr' => '1',
                        'prd_id' => $productId,
                        'is_admin_booked' => "1"
                    );


                    $hr_booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $hr_dataArr = array_merge($hr_dataArr, $hr_booking_status);
                    $hr_enquiryObj = \App\RentalsEnquiry::create($hr_dataArr);
                    $hr_insertid = $hr_enquiryObj->id;

                    if ($hr_enquiryObj->Bookingno == '' || $hr_enquiryObj->Bookingno == NULL) {
                        $val = 10 * $hr_insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $hr_newdata = array(
                            'Bookingno' => $bookingno
                        );
                        \App\RentalsEnquiry::where('id', $hr_insertid)->update($hr_newdata);
                    }
                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $hr_insertid,
                        'senderId' => $userId,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    \App\MedMessage::create($dataArr);
                    //$this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $hr_enquiryObj->prd_id, \Auth::user()->id, $hr_insertid);
                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    if ($availableCheck == 0) {
                        $this->mailNotificationsForUser->adminsendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                    } else {
                        $this->mailNotificationsForUser->adminsendBookingNotification($propertyObj, $hr_enquiryObj);
                    }
                } else {
                    //echo "yes inside else case"; exit;
                    $dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => $userId,
                        'renter_id' => $renterId,
                        'prd_id' => $productId,
                        'is_admin_booked' => "1"
                    );


                    $booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $dataArr = array_merge($dataArr, $booking_status);
                    $enquiryObj = \App\RentalsEnquiry::create($dataArr);
                    $insertid = $enquiryObj->id;

                    if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                        $val = 10 * $insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $newdata = array(
                            'Bookingno' => $bookingno
                        );
                        \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
                    }

                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $insertid,
                        'senderId' => $userId,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    \App\MedMessage::create($dataArr);
                    //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                    //checking if user have its first choice within selected dates or not
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    //echo $availableCheck;
                    if ($availableCheck == 0) {
                        //echo "yes its first choice and send email<br />"; exit;
                        if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                            $this->mailNotificationsForUser->adminsendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                        } else {
                            if (isset($enquiryObj->renter_id)) {
                                $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();
                                if (isset($renter_user->email) && !empty($renter_user->email)) {
                                    $this->mailNotificationsForUser->adminsendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                                }
                            }
                        }
                    } else {
                        //echo "sending email to esp as notification for sencond or 3rd choice<br />";
                        $this->mailNotificationsForUser->adminsendBookingNotification($propertyObj, $enquiryObj);
                    }
                }
            } else {
                //echo "yes inside else case"; exit;
                $dataArr = array(
                    'checkin' => $checkIn,
                    'checkout' => $checkOut,
                    'numofdates' => $noOfNyts,
                    'caltophone' => '',
                    'enquiry_timezone' => '',
                    'user_id' => $userId,
                    'renter_id' => $renterId,
                    'prd_id' => $productId,
                    'is_admin_booked' => "1"
                );


                $booking_status = array(
                    'booking_status' => 'Pending'
                );

                $dataArr = array_merge($dataArr, $booking_status);
                $enquiryObj = \App\RentalsEnquiry::create($dataArr);
                $insertid = $enquiryObj->id;

                if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                    $val = 10 * $insertid + 8;
                    $val = 1500000 + $val;
                    $bookingno = "WRL" . $val;
                    $newdata = array(
                        'Bookingno' => $bookingno
                    );
                    \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
                }

                $dataArr = array(
                    'productId' => $productId,
                    'bookingNo' => $bookingno,
                    'enqid' => $insertid,
                    'senderId' => $userId,
                    'receiverId' => $renterId,
                    'subject' => 'Booking Request : ' . $bookingno,
                    'message' => ""
                );
                \App\MedMessage::create($dataArr);
                //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                //checking if user have its first choice within selected dates or not
                //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
                $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                //echo $availableCheck;
                if ($availableCheck == 0) {
                    //echo "yes its first choice and send email<br />"; exit;
                    if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                        $this->mailNotificationsForUser->adminsendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                    } else {
                        if (isset($enquiryObj->renter_id)) {
                            $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();
                            if (isset($renter_user->email) && !empty($renter_user->email)) {
                                $this->mailNotificationsForUser->adminsendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                            }
                        }
                    }
                } else {
                    //echo "sending email to esp as notification for sencond or 3rd choice<br />";
                    $this->mailNotificationsForUser->adminsendBookingNotification($propertyObj, $enquiryObj);
                }
            }
            return redirect()->back()->with("message", "Your Booking has been added successfully!");
        }
    }

    public function paidgenerator() {
        $bredCrum = "Paid Booking Generator - Pending";
        $users = \App\User::get();
        $jobs = \App\Property::where('status', "!=", 0)->get(); //1 stands for unpaid and unpaidflash - 2 stands for paid - 0 stands for deleted or inactive
        return View('admin.booking_generator.paidbooking', compact('bredCrum', 'users', 'jobs'));
    }

    // paid pending booking from admin
    public function createpaid(Request $request) {
        //return $request->all();
        $userId = $request->get("user_id");
        $userEnquiries = \App\RentalsEnquiry::where('user_id', $userId)->get();
        $checkIn = date('Y-m-d 11:00:00', strtotime($request->get('startdate')));
        $checkOut = date('Y-m-d 10:00:00', strtotime($request->get('enddate')));
        $allowBooking = 0;
        $countEnquiry = 0;
        $settingsObj = \App\PropertySettings::propertySettings();
        $allowedEnquiryCount = $settingsObj->booking_request;
        $allowedEnquiryCount = $allowedEnquiryCount + 100;

        //echo $allowedEnquiryCount."<br />";
        if ($userEnquiries) {
            $countEnquiry = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
        }
        //echo $countEnquiry;
        //exit;
        if ($countEnquiry < $allowedEnquiryCount) {
            $allowBooking = 1;
        }
        if ($allowBooking == 0) {
            return redirect()->back()->with("error", "your limit is exceeded");
        } else {
            $productId = $request->get('job_id');
            $availableCheck = \App\Booking::availabilityCheckForProperty($checkIn, $checkOut, $productId);
            if(count($availableCheck) > 0){
                return redirect()->back()->with("error", "Sorry this user not available these dates.");
            }
            $propertyObj = $this->propertyRepository->getCurrentObject($productId);

            $renterId = $propertyObj->user->id;
            $now = strtotime($checkIn); // or your date as well
            $your_date = strtotime($checkOut);
            $datediff = $your_date - $now;

            $noOfNyts = floor($datediff / (60 * 60 * 24));
            $msg = "";
            $worktutoremail = $propertyObj->represent_email;
            $organizationemail = $propertyObj->user->orgemail;
            $bookingno = "";
            $hr_email = $propertyObj->hr_represent_email;
            $check_hr = $propertyObj->human_resources;
            if (($check_hr == 0) && !empty($hr_email)) {
                //echo "yes inside hr"; exit;
                $hr_details = \App\User::where('email', $hr_email)->first();
                if (isset($hr_details->id) && !empty($hr_details->id)) {
                    $hr_dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => $userId,
                        'renter_id' => $renterId,
                        'is_hr' => '1',
                        'prd_id' => $productId,
                        'is_admin_booked' => "1"
                    );


                    $hr_booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $hr_dataArr = array_merge($hr_dataArr, $hr_booking_status);
                    $hr_enquiryObj = \App\RentalsEnquiry::create($hr_dataArr);
                    $hr_insertid = $hr_enquiryObj->id;

                    if ($hr_enquiryObj->Bookingno == '' || $hr_enquiryObj->Bookingno == NULL) {
                        $val = 10 * $hr_insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $hr_newdata = array(
                            'Bookingno' => $bookingno
                        );
                        \App\RentalsEnquiry::where('id', $hr_insertid)->update($hr_newdata);
                    }
                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $hr_insertid,
                        'senderId' => $userId,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    \App\MedMessage::create($dataArr);
                    //$this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $hr_enquiryObj->prd_id, \Auth::user()->id, $hr_insertid);
                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    if ($availableCheck == 0) {
                        $this->mailNotificationsForUser->adminPaidsendHRBookingEmail($hr_email, $organizationemail, $propertyObj, $hr_enquiryObj);
                    } else {
                        $userData =  \App\User::where('id',$userId)->first();
                        $userEmail = $userData->email;
                        $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $hr_enquiryObj,$userEmail);
                    }
                } 
                else {
                    //echo "yes inside else case"; exit;
                    $dataArr = array(
                        'checkin' => $checkIn,
                        'checkout' => $checkOut,
                        'numofdates' => $noOfNyts,
                        'caltophone' => '',
                        'enquiry_timezone' => '',
                        'user_id' => $userId,
                        'renter_id' => $renterId,
                        'prd_id' => $productId,
                        'is_admin_booked' => "1"
                    );


                    $booking_status = array(
                        'booking_status' => 'Pending'
                    );

                    $dataArr = array_merge($dataArr, $booking_status);
                    $enquiryObj = \App\RentalsEnquiry::create($dataArr);
                    $insertid = $enquiryObj->id;

                    if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                        $val = 10 * $insertid + 8;
                        $val = 1500000 + $val;
                        $bookingno = "WRL" . $val;
                        $newdata = array(
                            'Bookingno' => $bookingno
                        );
                        \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
                    }

                    $dataArr = array(
                        'productId' => $productId,
                        'bookingNo' => $bookingno,
                        'enqid' => $insertid,
                        'senderId' => $userId,
                        'receiverId' => $renterId,
                        'subject' => 'Booking Request : ' . $bookingno,
                        'message' => ""
                    );
                    \App\MedMessage::create($dataArr);
                    //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                    //checking if user have its first choice within selected dates or not
                    //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
                    $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                    if ($availableCheck == 0) {
                        //echo "yes its first choice and send email ";
                        if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                            //echo "work tutor email ";
                            $this->mailNotificationsForUser->adminPaidsendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                        } else {
                            //echo "else work tutor ";
                            if (isset($enquiryObj->renter_id)) {
                                //echo "renter id ".$enquiryObj->renter_id;
                                $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();
                                if (isset($renter_user->email) && !empty($renter_user->email)) {
                                    $this->mailNotificationsForUser->adminPaidsendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                                }
                            }
                        }
                    } else {
                        $userData =  \App\User::where('id',$userId)->first();
                        $userEmail = $userData->email;
                        //echo "sending email to esp as notification for sencond or 3rd choice"; 
                        $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj,$userEmail);
                    }
                    //exit;
                }
            } 
            else {
                //echo "yes inside else case"; exit;
                $dataArr = array(
                    'checkin' => $checkIn,
                    'checkout' => $checkOut,
                    'numofdates' => $noOfNyts,
                    'caltophone' => '',
                    'enquiry_timezone' => '',
                    'user_id' => $userId,
                    'renter_id' => $renterId,
                    'prd_id' => $productId,
                    'is_admin_booked' => "1"
                );


                $booking_status = array(
                    'booking_status' => 'Pending'
                );

                $dataArr = array_merge($dataArr, $booking_status);
                $enquiryObj = \App\RentalsEnquiry::create($dataArr);
                $insertid = $enquiryObj->id;

                if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                    $val = 10 * $insertid + 8;
                    $val = 1500000 + $val;
                    $bookingno = "WRL" . $val;
                    $newdata = array(
                        'Bookingno' => $bookingno
                    );
                    \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
                }

                $dataArr = array(
                    'productId' => $productId,
                    'bookingNo' => $bookingno,
                    'enqid' => $insertid,
                    'senderId' => $userId,
                    'receiverId' => $renterId,
                    'subject' => 'Booking Request : ' . $bookingno,
                    'message' => ""
                );
                \App\MedMessage::create($dataArr);
                //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                //checking if user have its first choice within selected dates or not
                //$availableCheck = RentalsEnquiry::availabilityCheckForProperty($checkIn, $checkOut, $enquiryObj->prd_id, \Auth::user()->id, $insertid);
                $availableCheck = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
                if ($availableCheck == 0) {
                    //echo "yes its first choice and send email ";
                    if (($propertyObj->work_org_same == 1) && !empty($worktutoremail)) {
                        //echo "work tutor email ";
                        $this->mailNotificationsForUser->adminPaidsendBookingEmail($worktutoremail, $organizationemail, $propertyObj, $enquiryObj);
                    } else {
                        //echo "else work tutor ";
                        if (isset($enquiryObj->renter_id)) {
                            //echo "renter id ".$enquiryObj->renter_id;
                            $renter_user = User::where("id", $enquiryObj->renter_id)->select("id", "email", "name")->first();
                            if (isset($renter_user->email) && !empty($renter_user->email)) {
                                $this->mailNotificationsForUser->adminPaidsendUserListingBookingEmail($renter_user->email, $organizationemail, $propertyObj, $renter_user, $enquiryObj);
                            }
                        }
                    }
                } else {
                    $userData =  \App\User::where('id',$userId)->first();
                    $userEmail = $userData->email;
                    //echo "sending email to esp as notification for sencond or 3rd choice"; 
                    $this->mailNotificationsForUser->adminPaidsendBookingNotification($propertyObj, $enquiryObj,$userEmail);
                }
                //exit;
            }
            return redirect()->back()->with("message", "Your Booking has been added successfully!");
        }
    }

    public function accept() {
        // $bredCrum = "Booking Status Accepted";
        $bredCrum = "Booking Status Accepted / which does not send School House credits";
        $users = \App\User::get();
        $jobs = \App\Property::where('status', "!=", 0)->get();
        return View('admin.booking_generator.booking_status_accepted', compact('bredCrum', 'users', 'jobs'));
    }

    public function gmap($rental_id, $property_id, $user_rental_id) {
        $rentalObj = \App\RentalsEnquiry::find($rental_id);
        $user_rental = \App\RentalsEnquiry::find($user_rental_id);
        $property = \App\Property::find($property_id);
        return view("gmap", ['rentalObj' => $rentalObj, "property" => $property, "user_rental" => $user_rental]);
    }

    public function check_for_credit(){

        // echo "<pre>";
        //     print_r($_REQUEST);
        // echo "</pre>";
        // exit;
        $isBalanceAvailabe = (isset($_REQUEST['balance_check']) && $_REQUEST['balance_check'] != '' ? $_REQUEST['balance_check'] : '');
        $productId = (isset($_REQUEST['pid']) && $_REQUEST['pid'] != '' ? $_REQUEST['pid'] : '');
        $userId = (isset($_REQUEST['uid']) && $_REQUEST['uid'] != '' ? $_REQUEST['uid'] : '');

        if(!empty($isBalanceAvailabe)){
            $propertyObj = $this->propertyRepository->getCurrentObject($productId);
            $user_wallet = \App\UserWallet::where("user_id", $userId)->first();

            if( $propertyObj->property_status == 'paid'){
                $get_credits_bookpaid = \App\UserCredit::where("user_id", \Auth::user()->id)->where("book_paid", ">", "0")->count();
                $wallet_bookpaid = isset($user_wallet->book_paid)?$user_wallet->book_paid:0;
                $credit_bookpaid = $wallet_bookpaid - $get_credits_bookpaid;
                if($credit_bookpaid <= 0){
                    echo "noCredits";exit;                         
                }else{
                    echo 'success';
                }
            }else if($propertyObj->property_status == 'unpaid' || $propertyObj->property_status == 'unpaidflash'){
                $get_credits_bookunpaid = \App\UserCredit::where("user_id", \Auth::user()->id)->where("book_unpaid", ">", "0")->count();
                $wallet_bookunpaid = isset($user_wallet->book_unpaid)?$user_wallet->book_unpaid:0;
                $credit_bookunpaid = $wallet_bookunpaid - $get_credits_bookunpaid;
                if($credit_bookunpaid <= 0){
                    echo "noCredits";exit;                    
                }else{
                    echo 'success';
                }
            }
        }else{
         echo "success";
        }
        exit;
    }
    public function check_for_credit_old(){
        // echo "<pre>";
        //     print_r($_REQUEST);
        // echo "</pre>";

        $userId = $_REQUEST['user_id'];
         $property_id = $_REQUEST['pr_id'];
        $propertyObj = \App\Property::find($property_id);
        // echo "Property <pre>";
        //     print_r($propertyObj);
        // echo "</pre>";
        // exit;
        if(!empty($propertyObj) && $propertyObj->property_status != 'paid'){
            $user_wallet = \App\UserWallet::where("user_id", $userId)->first();
            // $get_credits_sendtounpaid = \App\UserCredit::where("user_id",$userId)->where("send_to_unpaid", ">", "0")->count();

            // $wallet_sendtounpaid = isset($user_wallet->send_to_unpaid)?$user_wallet->send_to_unpaid:0;
            // // echo "Send to paid :".$wallet_sendtounpaid."\n credit :".$get_credits_sendtounpaid."\n";
            // $credit_sendtounpaid = $wallet_sendtounpaid - $get_credits_sendtounpaid;
            // $credit_sendtounpaid = ($credit_sendtounpaid > 0 ? $credit_sendtounpaid : 0);


            $get_credits_bookunpaid = \App\UserCredit::where("user_id", $userId)->where("book_unpaid", ">", "0")->count();
            $wallet_bookunpaid = isset($user_wallet->book_unpaid)?$user_wallet->book_unpaid:0;
            $credit_bookunpaid = $wallet_bookunpaid - $get_credits_bookunpaid;
            $credit_bookunpaid = ($credit_bookunpaid > 0 ? $credit_bookunpaid : 0);
            // echo "Current Wallet :".$get_credits_sendtounpaid."\n Wakket : ".$credit_sendtounpaid;
            // exit;
            if($credit_bookunpaid > 0){
                echo 'allow';exit;
            }else{
                echo 'disallow';exit;
            }            
        }else{
            echo 'paidjob';exit;
        }
    }

    // paid accept booking from admin
    public function acceptcreate(Request $request) {
        //return $request->all();
        $userId = $request->get("user_id");
        $productId = $request->get('job_id');
        // if($_SERVER['REMOTE_ADDR']){ non eed for this we are only ones to access this page okay

        // }
        $userEnquiries = \App\RentalsEnquiry::where('user_id', $userId)->get();
        // echo "<pre>";
        //     echo "<br> User :".$userId."<br> Product Id :".$productId."<br>";
        //     print_r($userEnquiries);
        //     exit;
         
        
        $checkIn = date('Y-m-d 11:00:00', strtotime($request->get('startdate')));
        $checkOut = date('Y-m-d 10:00:00', strtotime($request->get('enddate')));
        $allowBooking = 0;
        $countEnquiry = 0;
        $settingsObj = \App\PropertySettings::propertySettings();
        $allowedEnquiryCount = $settingsObj->booking_request;
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){

        /*Condition for wallet check start here */
        // $currnetSendToUnpaid = 
        $user_wallet = \App\UserWallet::where("user_id", $userId)->first();
        $propertyObj = \App\Property::find($productId);
        // echo "<br> User Id :".$userId."<br>";;
        $get_credits_bookunpaid = \App\UserCredit::where("user_id", $userId)->where("book_unpaid", ">", "0")->count();
        // echo "<br> count Credits :".$get_credits_bookunpaid."<br>";
        // $get_credits_bookunpaid = \App\UserCredit::where("user_id", $userId)->where("book_unpaid", ">", "0")->count();
         $wallet_bookunpaid = isset($user_wallet->book_unpaid)?$user_wallet->book_unpaid:0;
        $credit_bookunpaid = $wallet_bookunpaid - $get_credits_bookunpaid;
        $credit_bookunpaid = ($credit_bookunpaid > 0 ? $credit_bookunpaid : 0);
        $user_voucher = User::where('id', $userId)->first();
        $is_paid = 0;
        $user_credits = \App\UserCredit::where("user_id", $userId)->where("send_to_unpaid", ">", "0")->get();
        // echo "<br> Counter Enquiry :".
        $countEnquiry = $user_credits->count();
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        //     echo "<br> Wallet Book Unp :".$wallet_bookunpaid."<br> Credits Bookunpaid :".$get_credits_bookunpaid."<br> Credits :".$credit_bookunpaid."<br>";
        //     $get_credits_bookunpaid1 = \App\UserCredit::where("user_id", $userId)->where("book_unpaid", ">", "0")->get();
        //     echo "Property Obj <pre>";
            
        //     print_r($get_credits_bookunpaid1);
        //     echo "<br> End here Property <br>";
        //     print_r($propertyObj);
        //     print_r($user_voucher);
        //     echo "</pre>";
        // }
        // echo "Current Wallet :".$user_wallet->send_to_unpaid."<br> BookUnpaid :".$credit_bookunpaid."<br>";
        //bertolini
        if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid") {
            $is_paid = 1;
            $user_paid_request_counts = RentalsEnquiry::select("fc_rentalsenquiry.*")
                    ->join("property", "property.id", "=", "fc_rentalsenquiry.prd_id")
                    ->where("fc_rentalsenquiry.user_id", $userId)
                    ->where("property_status", "paid")
                    ->get();
            //$countEnquiry = $user_paid_request_counts->count();
            //$allowedEnquiryCount = $settingsObj->school_house_booking_request;
            $user_credits = \App\UserCredit::where("user_id", $userId)->where("send_to_schoolhouse", ">", "0")->count();

            // if (sizeof($user_credits) > 0) {
                // $countEnquiry = $user_credits->count();
                $countEnquiry = $user_credits;
            // }
             // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
             //        $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
                    // echo "User Credit <prE>";
                    // echo "vcoucher Credit ID :".$user_voucher->vouchercredit_id."<br>";
                    // echo "User ID :".$userId;
                    //     print_r($get_voucher);
                    //     // print_r($user_credits);
                    // echo "</prE>";
                    // exit;
                // }
            if (isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
                $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
               
                    // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                    //     echo "Get Voucher : <pre>";
                    //         print_r($get_voucher);
                    //     echo "</pre>";
                    //    // exit;
                    // }
                if (isset($get_voucher->send_to_schoolhouse) && !empty($get_voucher->send_to_schoolhouse)) {

                    $allowedEnquiryCount = $get_voucher->send_to_schoolhouse;

                } else {
                    $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
                    if (isset($get_voucher->id)) {
                        $userWallet = new \App\UserWallet();
                        $userWallet->user_id = $userId;
                        $userWallet->vouchaercredits_id = $get_voucher->id;
                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                        $userWallet->creadit_type = "voucher";
                        $userWallet->save();
                        $allowedEnquiryCount = $get_voucher->send_to_schoolhouse;
                    }
                }
            } else {
                $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
                if (isset($get_voucher->id)) {
                    $user_voucher->vouchercredit_id = $get_voucher->id;
                    $user_voucher->save();
                    $userWallet = new \App\UserWallet();
                    $userWallet->user_id = $userId;
                    $userWallet->vouchaercredits_id = $get_voucher->id;
                    $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                    $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                    $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                    $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                    $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                    $userWallet->creadit_type = "voucher";
                    $userWallet->save();
                    $allowedEnquiryCount = $get_voucher->send_to_schoolhouse;
                }
            }
        }else{
            if($credit_bookunpaid >= 1 && $propertyObj->property_status != 'paid'){

                $book_unpaid = ($user_wallet->book_unpaid - 1);
                $user_wallet->book_unpaid = $book_unpaid;
                $user_wallet->save();
            }else{

                /*start here of 25-07-2019*/
            if(isset($user_voucher->vouchercredit_id) && $user_voucher->vouchercredit_id > 0) {
                    $get_voucher = \App\UserWallet::where("vouchaercredits_id", $user_voucher->vouchercredit_id)->first();
                    // echo "Get Voucher :<prE>";
                    //     print_r($get_voucher);
                    // echo "</prE>";
                    if (isset($get_voucher->send_to_unpaid) && !empty($get_voucher->send_to_unpaid)) {
                        $allowedEnquiryCount = $get_voucher->send_to_unpaid;
                        // echo "<br> Allowd Enquiry Count :".$allowedEnquiryCount."<br>";
                    }else {
                        $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first();
                        if (isset($get_voucher->id)) {
                            $userWallet = new \App\UserWallet();
                            $userWallet->user_id = $userId;
                            $userWallet->vouchaercredits_id = $get_voucher->id;
                            $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                            $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                            $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                            $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                            $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                            $userWallet->creadit_type = "voucher";
                            $userWallet->save();
                            $allowedEnquiryCount = $get_voucher->send_to_unpaid;
                        }
                    }
                    // echo "Allow Enquiring :".$allowedEnquiryCount."<br>"; why all this stuff_  this needs to check where the flow is going on because error is shwoing is only for unpaid not for paid property thats why.

                } else {
                    $get_voucher = \App\VoucherCredit::where("voucher_code", "Default")->first(); //wrong spelling here in condition
                   
                    if (isset($get_voucher->id)) {
                        $user_voucher->vouchercredit_id = $get_voucher->id;
                        $user_voucher->save();
                        $userWallet = new \App\UserWallet();
                        $userWallet->user_id = $userId;
                        $userWallet->vouchaercredits_id = $get_voucher->id;
                        $userWallet->amount = isset($get_voucher->amount) ? $get_voucher->amount : "0";
                        $userWallet->send_to_unpaid = isset($get_voucher->send_to_unpaid) ? $get_voucher->send_to_unpaid : "0";
                        $userWallet->book_unpaid = isset($get_voucher->book_unpaid) ? $get_voucher->book_unpaid : "0";
                        $userWallet->send_to_schoolhouse = isset($get_voucher->send_to_schoolhouse) ? $get_voucher->send_to_schoolhouse : "0";
                        $userWallet->book_paid = isset($get_voucher->book_paid) ? $get_voucher->book_paid : "0";
                        $userWallet->creadit_type = "voucher";
                        $userWallet->save();
                        $allowedEnquiryCount = $get_voucher->send_to_unpaid;
                    }
                }
                /*end here of 25-07-2019*/
            }
        }
                // echo "count Enquiry : ".$countEnquiry."<br> Allow Enquiry Count :".$allowedEnquiryCount."<br>";
                if ($countEnquiry < $allowedEnquiryCount) {// what is allowedEnquiryCount_ this variable get total credits in his wallets and check with his/her already sent request and check if credits is more than his/her wallets i from users so it check. understood?  Is Paid and here above to_unpaid ? So countEnquiry is 4 yes . I s this not obviuosly wrong_ this is from DB not a final count because script is alreaddy checking for his sent request count and then minus from counter


                   $allowBooking = 1;
                }
                // echo "<br> Allow booking :".$allowBooking."<br> Is Paid :".$is_paid."<br>";
    // exit; whose allow booking_ this will check for total credits user WHATONE (selected user *WHICH ONE( espe...@gmail.com wy not articolo75gmail.com_ h 's credits ) ha
                if(\Auth::user()->id != 1){
                    if ($allowBooking == 0) {
                        if ($is_paid) {
                            /*if(\Auth::user()->id == 1){
                                return "EnquiryError";
                                return redirect()->back()->with("error", "You are not allowed to send any more requests.."); 
                            }else{*/
                                return "EnquiryErrorPaid";                            
                            //}

                        } else {
                            // return "EnquiryError";
                            // echo "Current Login Id :".\Auth::user()->id."<pre>";

                            // exit;
                            if(\Auth::user()->id != 1){
                                return redirect()->back()->with("error", "You are not allowed to send any more requests.");
                            }
                        }
                    }
                }

        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        //     echo "End here";
        //     // exit;
        // }
        // echo "<br> Send to Unpaid :".$send_to_unpaid."<br>";

        /*Condition for wallet check end here */
            // echo "Allow Enq :<br><pre>";
            // print_r($user_wallet);
            // echo "</pre><br> END HERE </br>";;
            // exit;
        // }
        $allowedEnquiryCount = $allowedEnquiryCount + 100;
        $enquiryid = 0;
        //echo $allowedEnquiryCount."<br />";
        if ($userEnquiries) {
            $countEnquiry = $this->getEnquiryDates($userEnquiries, $checkIn, $checkOut);
        }
        //echo $countEnquiry;
        //exit;
        if ($countEnquiry < $allowedEnquiryCount) {
            $allowBooking = 1;
        }
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            // echo "Allow Booking :".$allowBooking."<br>count Enquire :".$countEnquiry."<br> Allow Enq Count :".$allowedEnquiryCount."<pre>";
            // // print_r($count);
            // echo "</pre>";
            // exit;

            /*update wallet on accept bed type start here */

               /* $acceptedUserId =  $userId;//\Auth::user()->id;
                $userInfo = User::with('group_info')->where('id', $userId)->first();
                $propertyDetails = Property::where('id',$productId)->first();
            
                if($propertyDetails->property_status == 'unpaid' || $propertyDetails->property_status == 'unpaidflash'){
                        // print_r($rentalObj);
                    $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();

                    if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                        echo "<br> Before Update Wallet <pre>";
                            print_r($fetchWalletData);
                        echo "</pre> <br> ";                        
                    }

                    $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 3 ) : "3";
                    $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 1 ) : "1";
                    $updateWalletData = array('send_to_schoolhouse'=>$send_to_schoolhouse,'book_paid'=>$book_paid,'creadit_type'=>'');
                    echo "Update Wallet Array Data <pre>";
                    print_r($updateWalletData);
                    echo "</pre>";
                    echo "<br> User ID :".$userId."<br>";
                    // exit;
                    \App\UserWallet::where('user_id',$userId)->update($updateWalletData);

                    $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();

                    if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                        // echo "Parent Email :".$rentalObj->user->email."Studen Email : <br>".$rentalObj->user->semail16."<br>";
                        echo "<br> After Update Wallet <pre>";
                            print_r($fetchWalletData);
                        echo "</pre>";
                        // exit;
                    }



                    $parentEmail = $propertyObj->user->email;
                    $studentEmail = $propertyObj->user->semail16;
                    
                    $this->mailNotificationsForUser->sendemailPaidToStudentAboutWalletUpdateForSchoolHouseAfterJobAccept($parentEmail, $studentEmail, $propertyObj,$userInfo);
                }*/

                /*update wallet on accept bed type end here*/
                // exit;
        // }

        
        $propertyObj = $this->propertyRepository->getCurrentObject($productId);
        $renterId = $propertyObj->user->id;
        $now = strtotime($checkIn); // or your date as well
        $your_date = strtotime($checkOut);
        $datediff = $your_date - $now;

        $noOfNyts = floor($datediff / (60 * 60 * 24));
        $msg = "";
        $worktutoremail = $propertyObj->represent_email;
        $organizationemail = $propertyObj->user->orgemail;
        $bookingno = "";
        $hr_email = $propertyObj->hr_represent_email;

        // job booking process starts from here
        if (!empty($hr_email)) {
            //$this->mailNotificationsForUser->sendHRBookingEmail($hr_email, $organizationemail, $propertyObj);
            $hr_details = \App\User::where('email', $hr_email)->first();
            if (isset($hr_details->id) && !empty($hr_details->id)) {
                $hr_dataArr = array(
                    'checkin' => $checkIn,
                    'checkout' => $checkOut,
                    'numofdates' => $noOfNyts,
                    'caltophone' => '',
                    'enquiry_timezone' => '',
                    'user_id' => $userId,
                    'renter_id' => $renterId,
                    'is_hr' => '1',
                    'prd_id' => $productId,
                    'is_admin_booked' => "1"
                );


                $hr_booking_status = array(
                    'booking_status' => 'Pending'
                );

                $hr_dataArr = array_merge($hr_dataArr, $hr_booking_status);
                // rental created if hr
                $hr_enquiryObj = \App\RentalsEnquiry::create($hr_dataArr);
                $hr_insertid = $hr_enquiryObj->id;
                $enquiryid = $hr_enquiryObj->id;

                if ($hr_enquiryObj->Bookingno == '' || $hr_enquiryObj->Bookingno == NULL) {
                    $val = 10 * $hr_insertid + 8;
                    $val = 1500000 + $val;
                    $bookingno = "WRL" . $val;
                    $hr_newdata = array(
                        'Bookingno' => $bookingno
                    );
                    \App\RentalsEnquiry::where('id', $hr_insertid)->update($hr_newdata);
                }
                $dataArr = array(
                    'productId' => $productId,
                    'bookingNo' => $bookingno,
                    'enqid' => $hr_insertid,
                    'senderId' => $userId,
                    'receiverId' => $hr_details->id,
                    'subject' => 'Booking Request : ' . $bookingno,
                    'message' => ""
                );
                \App\MedMessage::create($dataArr);
            } else {
                //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj);
                $dataArr = array(
                    'checkin' => $checkIn,
                    'checkout' => $checkOut,
                    'numofdates' => $noOfNyts,
                    'caltophone' => '',
                    'enquiry_timezone' => '',
                    'user_id' => $userId,
                    'renter_id' => $renterId,
                    'prd_id' => $productId,
                    'is_admin_booked' => "1"
                );


                $booking_status = array(
                    'booking_status' => 'Pending'
                );

                $dataArr = array_merge($dataArr, $booking_status);
                // rental created else case / what case if hr email or hr is not added in users table. So same case under almost but inside coditions
                $enquiryObj = \App\RentalsEnquiry::create($dataArr);
                $insertid = $enquiryObj->id;
                $enquiryid = $enquiryObj->id;

                if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                    $val = 10 * $insertid + 8;
                    $val = 1500000 + $val;
                    $bookingno = "WRL" . $val;
                    $newdata = array(
                        'Bookingno' => $bookingno
                    );
                    \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
                }

                $dataArr = array(
                    'productId' => $productId,
                    'bookingNo' => $bookingno,
                    'enqid' => $insertid,
                    'senderId' => $userId,
                    'receiverId' => $renterId,
                    'subject' => 'Booking Request : ' . $bookingno,
                    'message' => ""
                );
                \App\MedMessage::create($dataArr);
            }
        } else {
            //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj);
            $dataArr = array(
                'checkin' => $checkIn,
                'checkout' => $checkOut,
                'numofdates' => $noOfNyts,
                'caltophone' => '',
                'enquiry_timezone' => '',
                'user_id' => $userId,
                'renter_id' => $renterId,
                'prd_id' => $productId,
                'is_admin_booked' => "1"
            );


            $booking_status = array(
                'booking_status' => 'Pending'
            );

            $dataArr = array_merge($dataArr, $booking_status);
            // rental created if no hr *ESLE case aforementioned was no HR
            $enquiryObj = \App\RentalsEnquiry::create($dataArr);
            $insertid = $enquiryObj->id;
            $enquiryid = $enquiryObj->id;

            if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
                $val = 10 * $insertid + 8;
                $val = 1500000 + $val;
                $bookingno = "WRL" . $val;
                $newdata = array(
                    'Bookingno' => $bookingno
                );
                \App\RentalsEnquiry::where('id', $insertid)->update($newdata);
            }

            $dataArr = array(
                'productId' => $productId,
                'bookingNo' => $bookingno,
                'enqid' => $insertid,
                'senderId' => $userId,
                'receiverId' => $renterId,
                'subject' => 'Booking Request : ' . $bookingno,
                'message' => ""
            );
            \App\MedMessage::create($dataArr);
        }

        // accepting the booking while adding
        if (isset($enquiryid) && $enquiryid > 0) {
            $status = "Accept";
            $rentalObj = \App\RentalsEnquiry::find($enquiryid);
            //checking the same day booking count
            $newDateStart = date("Y-m-d", strtotime($rentalObj->checkin));
            $newDateEnd = date("Y-m-d", strtotime($rentalObj->checkout));
            $availableCheck = \App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $rentalObj->prd_id);
            $same_day_count = count($availableCheck);

            RentalsEnquiry::where('id', $enquiryid)->update(array('approval' => $status));
            $propertyId = $rentalObj->prd_id;
            $arrival = $rentalObj->checkin;
            $depature = $rentalObj->checkout;
            //get number of days between checkin and checkout dates
            $dates = $this->getDatesFromRange($arrival, $depature);
            //save booked dates using job id
            $this->saveBookedDates($dates, $propertyId);
            // update scheduler for booked job
            $this->updateScheduleCalendar($propertyId, 0);
            // adding new internal messages
            $dataArr = array(
                'productId' => $rentalObj->prd_id,
                'bookingNo' => $rentalObj->Bookingno,
                'enqid' => $enquiryid,
                'senderId' => $userId,
                'receiverId' => $rentalObj->user_id,
                'subject' => 'Booking Request : ' . $status,
                'message' => "You Request Approved"
            );
            MedMessage::create($dataArr);

            /* Send Learning Agreement */
            $cmsObj = $this->cmsRepository->getById(9);
            $stipulate = $this->cmsRepository->getById(12);
            $article1 = $this->cmsRepository->getById(13);
            $article2 = $this->cmsRepository->getById(14);
            $article3 = $this->cmsRepository->getById(15);
            $article4 = $this->cmsRepository->getById(16);
            $article5 = $this->cmsRepository->getById(17);
            $article6 = $this->cmsRepository->getById(18);
            $article7 = $this->cmsRepository->getById(19);
            $educationalobjectives = $this->cmsRepository->getById(20);
            $article9 = $this->cmsRepository->getById(21);
            $article10 = $this->cmsRepository->getById(22);

            $rentalObj = RentalsEnquiry::find($enquiryid);
            $enquiryObj = RentalsEnquiry::with('user')->get()->find($enquiryid);
            
            $users = \App\User::select('class_number', 'class_letter')->where(['id' => $userId])->get();
//        dd($users);
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            /*fetching country id of student school and hosting organisation to make file PDFs choice dynamic START HERE*/


            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $file_name = $timestamp . '-';
            $schoolCountryId = $organizationCountryId = '';
            // if($rentalObj->user->school->ocountry != ''){ // is this isssue_ I have put the condition if no value then pass it to next condition using isset() so this will bypass the conditioon (recently added) so issue not here_ after put the condition isset() I am testing but you explamin me the issue so not tested . okay hope clear now also searching for the school tbut not found in the dropdown , whatschool_
            if(isset($rentalObj->user->school->ocountry) && $rentalObj->user->school->ocountry != ''){ // is this isssue_ I have put the condition if no value then pass it to next condition using isset() so this will bypass the conditioon (recently added) so issue not here_ after put the condition isset() I am testing but you explamin me the issue so not tested . okay hope clear now also searching for the school tbut not found in the dropdown , whatschool_
                $schoolCountryId = $rentalObj->user->school->ocountry;
            }else if(isset($rentalObj->user->school->lcountry) && $rentalObj->user->school->lcountry != ''){
            // }else if( $rentalObj->user->school->lcountry != ''){
                $schoolCountryId = $rentalObj->user->school->lcountry;
            }
            $users = \App\User::where(['id' => $userId])->get();
            // echo "<br> Sending User Email :".$senderEMailId = $users[0]->email."<br>";
            // echo "<br> Receiver's Email :".$receiverEMailId = $rentalObj->property->user->email."<br>";

            // echo "<pre>";
                // print_r($_REQUEST);
            //     echo "<br> REQEST<Br> ";
            //     // print_r($users);
            //     echo "<br> USERS <Br> ";
            //     print_r($rentalObj->property->user->email);
            //     echo "=================";
            //     print_r($rentalObj->property->user);
            //     echo "<br> RENTAL OBJ <Br> ";
            // echo "</pre>";
            // exit;
            // echo "<br> Country School:".$schoolCountryId."<br><pre>";
                // print_r($rentalObj);
            // echo "</pre>";

            if ($rentalObj->property->addr_work_org_same == 0){
                $organizationCountryId = $rentalObj->property->wcountry;
            }else{
                $organizationCountryId = $rentalObj->host->countryr18;
            }
            // echo "Country Organization origi :".$organizationCountryId."<br>";


            if($schoolCountryId == 'Italia') {
                $schoolCountryId = 'Italy';                
            }
            if($organizationCountryId == 'Italia'){
                $organizationCountryId = 'Italy';
            }
            
            if($schoolCountryId == 'Irlanda'){
                $schoolCountryId = 'Ireland';                
            }
            if($organizationCountryId == 'Irlanda'){
                $organizationCountryId = 'Ireland';
            }

            if($schoolCountryId == 'Regno Unito'){
                $schoolCountryId = 'United Kingdom';                
            }
            if($organizationCountryId == 'Regno Unito' ){
               $organizationCountryId = 'United Kingdom';
            }
             // echo "Country Organization :".$organizationCountryId."<br>";
// exit;

            if(!empty($schoolCountryId)){
                if(is_numeric($schoolCountryId)){
                    $schoolCountryData = \App\Country::where('id',$schoolCountryId)->get();
                }else{
                    $schoolCountryData = \App\Country::where('name',$schoolCountryId)->get();
                }
                $schoolCountryCode = $schoolCountryData[0]->code;
            }
            // if($_SERVER['REMOTE_ADDR'] == "2.43.166.247"){
            //     echo "<br> IDs :".$organizationCountryId."<br>";
            //     exit;
            // }
            if(!empty($organizationCountryId)){
                if(is_numeric($organizationCountryId)){
                    $organizationCountryData = \App\Country::where('id',$organizationCountryId)->get();
                }else{
                    $organizationCountryData = \App\Country::where('name',$organizationCountryId)->get();
                }

                // if($_SERVER['REMOTE_ADDR'] == "2.43.166.247"){
                //     echo "<br> IDs :".$organizationCountryId."<br>";
                //     echo "Count :".count($organizationCountryData);
                //     exit;
                // }
                // echo "<pre>";
                //     print_r($organizationCountryData);
                // echo "<pre>";
                // exit;

                // $organizationCountryCode = $organizationCountryData[0]->code;
                if(count($organizationCountryData) > 0){
                    $organizationCountryCode = $organizationCountryData[0]->code;                    
                }else{
                    $organizationCountryCode = "";
                }
            }

           // $inCountry = array("Italy","Ireland","United Kingdom");
           // $inCountry = array("Ireland","United Kingdom");
           $inCountry = array("Italy","United Kingdom");
            if((!empty($schoolCountryCode) && !empty($organizationCountryCode)) && in_array($organizationCountryId,$inCountry) && in_array($schoolCountryId,$inCountry) ){
                $file_name .= 'Pdf'.$schoolCountryCode.'goingto'.$organizationCountryCode;
            }else{
                // send this request to "esperienzainglese@gmail.com"
                $file_name .= "pdfITgoingtoUK";
                $senderEmailId = $users[0]->email;
                $receiverEmailId = $rentalObj->property->user->email;
                $schoolCountry = $schoolCountryId;
                $organizationCountry = $organizationCountryId;
                // echo "<br> SenderEmail :".$senderEmailId."<br> Receiver Email Id :".$receiverEmailId."<br> School Country : ".$schoolCountry."<br> Organization Country :".$organizationCountry."<br> CheckIn :".date("Y-m-d",strtotime($checkIn))."<br> Checkout :".date("Y-m-d",strtotime($checkOut))."<br>";

                $data = array('senderEmailId' => $senderEmailId, 'receiverEmailId' => $receiverEmailId, 'schoolCountry' => $schoolCountry, 'organizationCountry' => $organizationCountry, 'checkIn' =>date("Y-m-d",strtotime($checkIn)), 'checkOut' => date("Y-m-d",strtotime($checkOut)));

                \Mail::send('emails.request', $data, function($message){
                    $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                    $message->to("esperienzainglese@gmail.com","")
                            // ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                            ->subject('Work-Related-Learning.com - you have reveiced a message regarding a request');
                            // exit;
                });
            }

            $file_name .= "-".$rentalObj->Bookingno;
            $file_name .= ".pdf";
            // echo $file_name."<br>";
            // exit;
            // echo "<prE>";
                // print_R($enquiryObj->school_name);
            // \App\User::where("id")
            $schoolName = "";
            if(!empty($users[0]->school_name)){
                $schoolInfo = \App\School::where('id', $users[0]->school_name)->first();
            // echo "<prE>";
            //         echo "<BR> USER DETAILS </br>";
            //         // echo "Count :".count($schoolInfo)."<br>";
            //         // print_R($users[0]->school_name);
            //         echo "School ID :".$schoolInfo->id;
            //         print_R($schoolInfo);
            //         // print_R($users);
            //     echo "</prE>";
                // if(!empty($schoolInfo) && count($schoolInfo) > 0){
                if(!empty($schoolInfo) && isset($schoolInfo->id)){
                    $schoolName = $schoolInfo->name;
                }                
            }
            // exit;
            /*fetching country id of student school and hosting organisation to make file PDFs choice dynamic END HERE*/

            // $file_name = $timestamp . '-' . $rentalObj->Bookingno . ".pdf";
            $file_name1 = $timestamp . '-' . $rentalObj->Bookingno . "register.pdf"; //adding filename to Register
            $file_name2 = $timestamp . '-' . $rentalObj->Bookingno . "evaluation.pdf";
            $file_name3 = $timestamp . '-' . $rentalObj->Bookingno . "parental_permission.pdf"; //adding filename to Register
            $pdf = PDF::loadView('agreement', compact('rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'))->save(public_path() . "/pdf/agreement/" . $file_name);                
            // if($_SERVER['REMOTE_ADDR'] == '93.33.28.150' || $_SERVER['REMOTE_ADDR'] = '93.32.72.112'){
            //     // phpinfo();
            //     echo "<pre>";
            //     echo "<br> PDF Var <br>";
            //         print_r($pdf);
            //         // print_r($rentalObj);
            //         // echo "<br> CSM OBJ <br>";
            //         // print_r($cmsObj);
            //         // echo "<br> Article1 OBJ <br>";
            //         // print_r($article1);
            //         //  echo "<br> Article2 OBJ <br>";
            //         // print_r($article2);
            //         // echo "<br> Article3 OBJ <br>";
            //         // print_r($article3);
            //         // echo "<br> Article4 OBJ <br>";
            //         // print_r($article4);
            //         // echo "<br> Article5 OBJ <br>";
            //         // print_r($article5);
            //         // echo "<br> Article6 OBJ <br>";
            //         // print_r($article6);
            //         // echo "<br> Article7 OBJ <br>";
            //         // print_r($article7);
            //         // echo "<br> Article9 OBJ <br>";
            //         // print_r($article9);
            //         // echo "<br> stipulate OBJ <br>";
            //         // print_r($stipulate);
            //         // echo "<br> educationalobjectives OBJ <br>";
            //         // print_r($educationalobjectives);
            //         // print_r($cmsObj);
            //     echo "</pre>";
            //     exit;

            // }
            if ($propertyObj->property_status != "paid") {

            // dd($pdf);
            // dd($users);
                $pdf1 = PDF::loadView('Activity_Register_WRL_Registro_Presenze_English', compact('users','rentalObj'))->save(public_path() . "/pdf/register/" . $file_name1); //adding pdf genration of Register

                $pdf2 = PDF::loadView('evoluationpdf_new', compact('users','enquiryObj','schoolName'))->save(public_path() . "/pdf/evaluation/" . $file_name2);

                $pdf3 = PDF::loadView('parental_permissionpdf', compact('enquiryObj'))->save(public_path() . "/pdf/parental_permission/" . $file_name3);
                // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                //     echo "<br>".$file_name2."<br>";                    
                //     exit;
                // }
               // dd($educationalobjectives);
               // return view('Activity_Register_WRL_Registro_Presenze_English', compact('users','rentalObj', 'cmsObj', 'article1', 'article2', 'article3', 'article4', 'article5', 'article6', 'article7', 'article9', 'article10', 'stipulate', 'educationalobjectives'));
            }
            // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            //     echo "<pre>";
            //     echo $file_name2;
            //     exit;
            // }
//return $file_name ;
            //$tutorEmail = $rentalObj->user->school->tutor_email;
            
            $tutorEmail = "";
            if (isset($rentalObj->property->work_org_same) && $rentalObj->property->work_org_same == "0") {
                //echo "yes here if the work tutor is NOT you <br />";
                if (isset($rentalObj->prd_id) && !empty($rentalObj->prd_id)) {
                    $tutor_detail = Property::find($rentalObj->prd_id);
                    if (isset($tutor_detail->represent_email) && !empty($tutor_detail->represent_email)) {
                        $tutorEmail = $tutor_detail->represent_email;
                    }
                }
            } else {
                //echo "yes here if the work tutor is you <br />";
                $tutorEmail = isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : "";
            }


            $parentEmail = null;
            if (is_null($rentalObj->user->student_relationship)) {
                $studentEmail = $rentalObj->user->email;
            } else {
                $parentEmail = $rentalObj->user->email;
                $studentEmail = $rentalObj->user->semail16;
            }

            // get HR details
            $hr_enquiry = RentalsEnquiry::where(["id" => $enquiryid, 'is_hr' => '1'])->first();
            $hr_email = "";
            $hr_email_detail = "";
            if (isset($rentalObj->property->human_resources) && $rentalObj->property->human_resources == "0") {
                $hr_email = isset($rentalObj->property->hr_represent_email) ? $rentalObj->property->hr_represent_email : "";
                if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
                    $hr_user = User::find($hr_enquiry->renter_id);
                    if (isset($hr->prd_id) && !empty($hr->prd_id)) {
                        $hr_email_detail = Property::find($hr->prd_id);
                    }
                }
            } else {
                $hr_email = isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : "";
            }
//            $hr_email_detail = ""; human_resources
//            if (isset($hr_enquiry->renter_id) && !empty($hr_enquiry->renter_id)) {
//                $hr_user = User::find($hr_enquiry->renter_id);
//                $hr_email = isset($hr_user->email) ? $hr_user->email : '';
//                if (isset($hr->prd_id) && !empty($hr->prd_id)) {
//                    $hr_email_detail = Property::find($hr->prd_id);
//                }
//            }
//            echo "work tutor email = " . $tutorEmail . "<br />";
//            echo "HR email = " . $hr_email . "<br />";
//            echo "Parent email = " . $parentEmail . "<br />";
//            echo "Student email = " . $studentEmail . "<br />";
//            echo "is HR = " . $rentalObj->is_hr . "<br />";
//            exit;
            //echo $propertyObj->property_status;
            //exit;
            if ($propertyObj->property_status == "paid") {
                $this->mailNotificationsForUser->sendemailPaidToStudentAboutSchoolHouse($parentEmail, $studentEmail, $rentalObj);
                $school_house_email = isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : "";
                //echo $school_house_email; exit;
                $this->mailNotificationsForUser->sendemailPaidToSchoolHouseAboutStudent($school_house_email, $rentalObj, $file_name);
            } else {

                /*update wallet on accept bed type start here */

               /* $acceptedUserId =  $userId;//\Auth::user()->id;
                $userInfo = User::with('group_info')->where('id', $userId)->first();
                $propertyDetails = Property::where('id',$productId)->first();
            
                if($propertyDetails->property_status == 'unpaid' || $propertyDetails->property_status == 'unpaidflash'){
                        // print_r($rentalObj);
                    $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();

                    // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                    //     echo "<br> Before Update Wallet <pre>";
                    //         print_r($fetchWalletData);
                    //     echo "</pre> <br> ";                        
                    // }

                    $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 3 ) : "3";
                    $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 1 ) : "1";

                    \App\UserWallet::where('user_id',$userId)->update(array('send_to_schoolhouse'=>$send_to_schoolhouse,'book_paid'=>$book_paid,'creadit_type'=>''));

                    $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();

                    if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                        echo "Parent Email :".$rentalObj->user->email."Studen Email : <br>".$rentalObj->user->semail16."<br>";
                        echo "<br> After Update Wallet <pre>";
                            print_r($fetchWalletData);
                        echo "</pre>";
                        exit;
                    }



                    $parentEmail = $rentalObj->user->email;
                    $studentEmail = $rentalObj->user->semail16;
                    
                    $this->mailNotificationsForUser->sendemailPaidToStudentAboutWalletUpdateForSchoolHouseAfterJobAccept($parentEmail, $studentEmail, $rentalObj,$userInfo);
                }*/

                /*update wallet on accept bed type end here*/
                /*update wallet on accept bed type start here */

                $acceptedUserId =  $userId;//\Auth::user()->id;
                $userInfo = User::with('group_info')->where('id', $userId)->first();
                $propertyDetails = Property::where('id',$productId)->first();
            
                if($propertyDetails->property_status == 'unpaid' || $propertyDetails->property_status == 'unpaidflash'){
                        // print_r($rentalObj);
                    $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();

                    // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                    //     echo "<br> Before Update Wallet <pre>";
                    //         print_r($fetchWalletData);
                    //     echo "</pre> <br> ";                        
                    // }
                    if(\Auth::user()->id != 1){
                        
                        $send_to_schoolhouse = isset($fetchWalletData->send_to_schoolhouse) ? ($fetchWalletData->send_to_schoolhouse + 3 ) : "3";
                        $book_paid = isset($fetchWalletData->book_paid) ? ($fetchWalletData->book_paid + 1 ) : "1";
                        $updateWalletData = array('send_to_schoolhouse'=>$send_to_schoolhouse,'book_paid'=>$book_paid,'creadit_type'=>'');
                        // echo "Update Wallet Array Data <pre>";
                        // print_r($updateWalletData);
                        // echo "</pre>";
                        // echo "<br> User ID :".$userId."<br>";
                        // exit;
                        \App\UserWallet::where('user_id',$userId)->update($updateWalletData);

                        $fetchWalletData = \App\UserWallet::where('user_id',$userId)->first();

                        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                        //     // echo "Parent Email :".$rentalObj->user->email."Studen Email : <br>".$rentalObj->user->semail16."<br>";
                        //     echo "<br> After Update Wallet <pre>";
                        //         print_r($fetchWalletData);
                        //     echo "</pre>";
                        //     // exit;
                        // }

                        $parentEmail = $rentalObj->user->email;
                        $studentEmail = $rentalObj->user->semail16;
                        
                        $this->mailNotificationsForUser->sendemailPaidToStudentAboutWalletUpdateForSchoolHouseAfterJobAccept($parentEmail, $studentEmail, $rentalObj,$userInfo);
                    }
                }

                /*update wallet on accept bed type end here*/


                // $this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $file_name, $rentalObj, $hr_email, $hr_email_detail,$file_name1);
                // $files = array("agreement" => $file_name,"evaluation"=>$file_name2);
                $files = array("agreement" => $file_name,"evaluation"=>$file_name2,"register"=>$file_name1,"parental_permission"=>$file_name3);
                // if($_SERVER['REMOTE_ADDR'] != '93.36.75.10'){
                    $this->mailNotificationsForUser->sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $files, $rentalObj, $hr_email, $hr_email_detail,$file_name1);                    
                // }
                
            }
            // if there is an HR for booking send email and send internal message
//            if ($rentalObj->is_hr == "1") {
//                $propertyObj = $this->propertyRepository->getCurrentObject($rentalObj->prd_id);
//                $worktutoremail = $propertyObj->represent_email;
//                if (!empty($worktutoremail)) {
//                    //$worktutoremail = $rentalObj->user->email;
//                    $organizationemail = $propertyObj->user->orgemail;
//                    //$this->mailNotificationsForUser->sendBookingEmail($worktutoremail, $organizationemail, $propertyObj);
//                }
//                //echo $worktutoremail; exit;
//                //exit;
//                $checkIn = $rentalObj->checkin;
//                $checkOut = $rentalObj->checkout;
//                $noOfNyts = $rentalObj->numofdates;
//                $renterId = $propertyObj->user->id;
//                $dataArr = array(
//                    'checkin' => $checkIn,
//                    'checkout' => $checkOut,
//                    'numofdates' => $noOfNyts,
//                    'caltophone' => '',
//                    'enquiry_timezone' => '',
//                    'user_id' => $rentalObj->user_id,
//                    'renter_id' => $renterId,
//                    'prd_id' => $rentalObj->prd_id,
//                    'is_admin_booked' => "1"
//                );
//
//
//                $booking_status = array(
//                    'booking_status' => 'Pending'
//                );
//
//                $dataArr = array_merge($dataArr, $booking_status);
//                //$enquiryObj = RentalsEnquiry::create($dataArr);
//                $enquiryObj = RentalsEnquiry::where('id', $rentalObj->id)->update($dataArr);
//                $insertid = $rentalObj->ids;
//
//                if ($enquiryObj->Bookingno == '' || $enquiryObj->Bookingno == NULL) {
//                    $val = 10 * $insertid + 8;
//                    $val = 1500000 + $val;
//                    $bookingno = "WRL" . $val;
//                    $newdata = array(
//                        'Bookingno' => $bookingno
//                    );
//                    RentalsEnquiry::where('id', $insertid)->update($newdata);
//                }
//
//                $dataArr = array(
//                    'productId' => $rentalObj->prd_id,
//                    'bookingNo' => $bookingno,
//                    'enqid' => $insertid,
//                    'senderId' => $rentalObj->user_id,
//                    'receiverId' => $renterId,
//                    'subject' => 'Booking Request : ' . $bookingno,
//                    'message' => ""
//                );
//                MedMessage::create($dataArr);
//            }
            // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            //     echo "End here";
            //     exit;
            // }else{
                return redirect()->back()->with("message", "Your Booking has been added and Accepted successfully!");
                
            // }
        } else {
            return redirect()->back()->with("error", "There has been some error while adding booking!");
        }
    }

    public function checkAccBalance(){
        $isBalanceAvailabe = (isset($_REQUEST['balance_check']) && $_REQUEST['balance_check'] != '' ? $_REQUEST['balance_check'] : '');
        $productId = (isset($_REQUEST['pid']) && $_REQUEST['pid'] != '' ? $_REQUEST['pid'] : '');
        $userId = (isset($_REQUEST['uid']) && $_REQUEST['uid'] != '' ? $_REQUEST['uid'] : '');

        if(!empty($isBalanceAvailabe)){
            $propertyObj = $this->propertyRepository->getCurrentObject($productId);
            $user_wallet = \App\UserWallet::where("user_id", $userId)->first();

            if( $propertyObj->property_status == 'paid'){
                $get_credits_bookpaid = \App\UserCredit::where("user_id", \Auth::user()->id)->where("book_paid", ">", "0")->count();
                $wallet_bookpaid = isset($user_wallet->book_paid)?$user_wallet->book_paid:0;
                $credit_bookpaid = $wallet_bookpaid - $get_credits_bookpaid;
                if($credit_bookpaid <= 0){
                    echo "noCredits";exit;                         
                }else{
                    echo 'success';
                }
            }else if($propertyObj->property_status == 'unpaid'){
                $get_credits_bookunpaid = \App\UserCredit::where("user_id", \Auth::user()->id)->where("book_unpaid", ">", "0")->count();
                $wallet_bookunpaid = isset($user_wallet->book_unpaid)?$user_wallet->book_unpaid:0;
                $credit_bookunpaid = $wallet_bookunpaid - $get_credits_bookunpaid;
                if($credit_bookunpaid <= 0){
                    echo "noCredits";exit;                    
                }else{
                    echo 'success';
                }
            }
        }else{
         echo "success";
        }
        exit;
    }


    public function saveBookedDates($dates, $propertyId) {
        $i = 1;
        $dateMinus1 = count($dates);
        foreach ($dates as $date) {
            if ($i <= $dateMinus1) {
                $bookingObj = Booking::where('PropId', $propertyId)->where('id_state', 1)->where('id_item', 1)->where('the_date', $date)->get();
                if ($bookingObj->count() > 0) {
                    
                } else {
                    $dataArr = array('PropId' => $propertyId,
                        'id_state' => 1,
                        'id_item' => 1,
                        'the_date' => $date
                    );
                    Booking::create($dataArr);
                }
            }
            $i++;
        }
    }

    public function updateScheduleCalendar($propertyId, $price) {
        $DateArr = Booking::where('PropId', $propertyId)->get();
        $dateDispalyRowCount = 0;
        $dateArrVAl = "";
        if ($DateArr->count() > 0) {
            $dateArrVAl .= '{';
            foreach ($DateArr as $dateDispalyRow) {
                if ($dateDispalyRowCount == 0) {
                    $dateArrVAl .= '"' . $dateDispalyRow->the_date . '":{"available":"1","bind":0,"info":"","notes":"","price":"' . $price . '","promo":"","status":"booked"}';
                } else {
                    $dateArrVAl .= ',"' . $dateDispalyRow->the_date . '":{"available":"1","bind":0,"info":"","notes":"","price":"' . $price . '","promo":"","status":"booked"}';
                }
                $dateDispalyRowCount = $dateDispalyRowCount + 1;
            }
            $dateArrVAl .= '}';
            $scheduleObj = Schedule::where('id', $propertyId)->exists();
            $inputArr4 = array('id' => $propertyId, 'data' => trim($dateArrVAl));
            if ($scheduleObj) {
                Schedule::where('id', $propertyId)->update($inputArr4);
            } else {
                Schedule::create($inputArr4);
            }
        }
    }

    public function getEnquiryDates($enquiresObj, $checkIn, $checkOut) {
        $enquiryDates = array();
        $count = 0;
        foreach ($enquiresObj as $enq) {
            $dates = $this->getDatesFromRange($enq->checkin, $enq->checkout);
            $i = 1;
            $dateMinus1 = count($dates);
            foreach ($dates as $date) {
                if ($i <= $dateMinus1) {
                    array_push($enquiryDates, $date);
                }
                $i++;
            }
            if (in_array($checkIn, $enquiryDates) || in_array($checkOut, $enquiryDates)) {
                $count++;
            }
        }
        return $count;
    }

    public function getDatesFromRange($start, $end) {
        $dates = array($start);
        while (end($dates) < $end) {
            $dates[] = date('Y-m-d', strtotime(end($dates) . ' +1 day'));
        }
        return $dates;
    }

}
