<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Find and replace image extensions when not "." (but as code was for example: "-")
     * */
    public function getDirectoryImages() {
        echo phpinfo(); exit;
        $files = \File::allFiles(public_path('images'));
        foreach ($files as $file) {
            $org_file_path = (string) $file;
            $file_name = basename($org_file_path);
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (empty($ext)) {
                $get_ext = explode("-", $file_name);
                $ext_count = sizeof($get_ext) - 1;
                $org_ext = $get_ext[$ext_count];
                unset($get_ext[$ext_count]);
                $new_file_name = implode("-", $get_ext) . "." . $org_ext;
                $new_path = explode("\\", $org_file_path);
                unset($new_path[count($new_path) - 1]);
                $new_path = implode("\\", $new_path);
                $new_file_path = $new_path . "\\" . $new_file_name;
                $ext_array = array("jpg", "jpeg", "png", "PNG", "JPG", "JPEG", "gif", "GIF");
                if (in_array($org_ext, $ext_array)) {
                    echo "Org file path = " . $org_file_path . "<br />";
                    echo "New file path = " . $new_file_path . "<br />";
                    echo (string) $file_name . " ------- extension is =" . $ext . "<br />";
                    echo "Org extension = " . $org_ext . "<br />";
                    echo "New File name = " . $new_file_name . "<br /> <br />";
                    rename($org_file_path, $new_file_path);
                }
            }
        }
    }

    public function changeDBImages() {
        //get all property images
        $images = \App\Property::with("propertyimages")->select("id", "title")->get();
        //return $images;
        if (sizeof($images) > 0) {
            foreach ($images as $proimg) {
                if (sizeof($proimg->propertyimages) > 0) {
                    foreach ($proimg->propertyimages as $img) {
                        $img_name = !empty($img->img_name) ? $img->img_name : "";
                        if (!empty($img_name)) {
                            $get_ext = explode("-", $img_name);
                            $ext_count = sizeof($get_ext) - 1;
                            $org_ext = $get_ext[$ext_count];
                            unset($get_ext[$ext_count]);
                            $new_file_name = implode("-", $get_ext) . "." . $org_ext;
                            $ext_array = array("jpg", "jpeg", "png", "PNG", "JPG", "JPEG", "gif", "GIF");
                            if (in_array($org_ext, $ext_array)) {
                                echo "Org file path = " . $img_name . "<br />";
                                echo "New File name = " . $new_file_name . "<br /> <br />";
                                $update_img = \App\PropertyImages::find($img->id);
                                $update_img->img_name = $new_file_name;
                                $update_img->save();
                            }
                        }
                    }
                }
            }
        }
        exit;
    }

    public function index() {
//         $url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyC646z-W9bGxhDtgjILxonsK2KI5TU-z68&origins='Via+Provinciale,+22,+Albino,+Province+of+Bergamo,+Italy'&destinations=Via+Provinciale,+31,+Albino,+BG,+Italia&mode=driving&sensor=false";
// $json = file_get_contents($url);
// $details = json_decode($json, TRUE);
// echo "<pre>";
// print_r($details);
// exit;
         // echo dirname(dirname(dirname(dirname(__FILE__))));
         // exit;
        // var_dump(shell_exec("whereis php"));
        // echo $filePath = dirname(dirname(dirname(dirname(__FILE__))));
        // $fileName = "/chron_log_file_".date('Ymd')."_testController.txt";
        // $finalFilePath = $filePath.$fileName;

        // // echo "<pre>";
        // // print_r($_SERVER);
        // // echo "</pre>";
        // // exit;

        // $fp = fopen($finalFilePath,"a"); 
        //       chmod($finalFilePath,"0755");

        //                       $time = 0;
        //                       $address = '';
        //                     $write  = "\n =========================== Start Here on ".date("Y-m-d H:i:s")." =========================== \n";
        //                     $write .= "\n\n address = $address , time = $time added \n\n";
        //                     $write .= "\n =========================== End Here on ".date("Y-m-d H:i:s")." =========================== \n";

        //                     fwrite($fp, $write);
        //                     fclose($fp);
        // echo "<br>";

// $response = shell_exec("php -q /home2/workrel1/public_html/artisan schedule:run");
$response = shell_exec("php -q /home2/workrel1/public_html/artisan command:addPropertyAddress");
// var_dump($response);
echo "Hi";
        // var_dump(shell_exec("php /home2/workrel1/public_html/artisan schedule:run 1"));
        exit;
        echo phpinfo(); exit;
        return view('auth.register');
        //echo \Session::get('test');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //echo "dgdfg";exit;
        $validator = \Validator::make($request->all(), [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
        ]);
        if ($validator->fails()) {
            return \Redirect::to('auth/register')
                            ->withErrors($validator);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
