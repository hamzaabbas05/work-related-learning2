<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schoolfamily extends Model
{
    protected $table = 'user_family_photo';

    protected $guarded = ['id'];

    public $timestamps = false;
    
    public function user() {
        return $this->belongsTo(User::class);
    }
}
