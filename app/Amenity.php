<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    protected $table = 'amenities';

    protected $guarded = ['id'];

    public function properties()
    {
        return $this->belongsToMany(Property::class, 'amenity_property', 'amenity_id', 'property_id');
    }
}
