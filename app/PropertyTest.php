<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTest extends Model
{
    protected $table = 'property_test';
    protected $guarded = ['id'];
}
