<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{

     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'code';

    protected $primaryKey = 'CodeId';
}