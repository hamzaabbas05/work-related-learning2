<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCredit extends Model {
    
    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function vouchercredits(){
        return $this->belongsTo(VoucherCredit::class, "voucher_id");
    }
}
