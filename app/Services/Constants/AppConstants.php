<?php

namespace App\Services\Constants;
 
class AppConstants
{ 
	const HOME_BANNER = 1;

	const HOME_SLIDER = 2;

	const HOME_CENTER_SLIDER = 3;

	const STATUS_ACTIVE = 1;

	const STATUS_DEACTIVE = 2;

	const STATUS_PENDING_APPROVAL = 0;

	const COMMON_AMENITIES = 1;

	const ADDITIONAL_AMENITIES = 2;

	const SPECIAL_FEATURES = 4;

	const SAFETY_CHECKLISTS = 3;

	const ABOUT_US = 1;

	const TERMS_AND_CONDITIONS = 2;

	const PRIVACY_POLICY = 3;

	const WHY_HOST = 4;

	const TRUST_SAFETY = 6;

 	const HOW_WORKS = 5;

 	const COMMISSION_TYPE_FLAT = 1;

 	const COMMISSION_TYPE_PERCENTAGE = 2;

 	const TAX_COMMISSION = 2;

 	const GUEST_COMMISSION = 1;

 	const HOST_COMMISSION = 3;




	 
 }
