<?php

namespace App\Services\Mailer;

class Mail extends MailerAbstract
{
    public function send($toEmail, $toName, $subject, $viewFile, $dataForView = null, $fromEmail = null, $fromName = null)
    {
        $fromEmailNotBlank = $fromEmail ? $fromEmail : $this->adminMail;
        $fromNameNotBlank = $fromName ? $fromName : $this->adminName;
        $this->mailer->queue($viewFile, ['datas' => $dataForView],  function ($m) use ($toEmail, $toName, $subject, $fromEmailNotBlank, $fromNameNotBlank) {
            $m->from($fromEmailNotBlank, $fromNameNotBlank);
            $m->to($toEmail, $toName)->subject($subject);
        });
    }

    public function sendRawText($toEmail, $toName = '', $subject, $dataRawText = null, $fromEmail = null, $fromName = null)
    {
        $fromEmailNotBlank = $fromEmail ? $fromEmail : $this->adminMail;
        $fromNameNotBlank = $fromName ? $fromName : $this->adminName;

        $this->mailer->raw($dataRawText, function ($message) use ($toEmail, $toName, $subject, $fromEmailNotBlank, $fromNameNotBlank) {
            $message->from($fromEmailNotBlank, $fromNameNotBlank);
            $message->to($toEmail, $toName)->subject($subject);
        });
    }
    public function sendTestMail($toEmail, $dataRawText, $subject = null)
    {
        $this->sendRawText($toEmail, 'Test User', $subject, $dataRawText, 'test@gmail.com', 'Test');
    }
}
