<html>
<head>
<style>
.divbottom 
{
  margin-bottom:10px;
}

.margintop
{
   margin-top:50px;
}
 

</style>
</head>
<body>
<div style="margin-left:60px;">
  <div>
{!! html_entity_decode($cmsObj->page_desc) !!}
  <h2>1) INSTITUTE OF HIGHER EDUCATION - PROMOTER</h2></div>
        <div class="divbottom">Institute (hereinafter referred to as Promoter):{{$rentalObj->user->school->name}}</div>
        <div class="divbottom">Mechano-graphical code:{{$rentalObj->user->school->Mecanographiccode}}</div>
<!-- Pending -- >

<div class="divbottom" ><h3>Registered office/Legal Address</h3></div> 
<div class="divbottom">Address street name:{{$rentalObj->user->school->lroute}}</div>
<div class="divbottom">Address street number:{{$rentalObj->user->school->lstreet_number}}</div>
<div class="divbottom">Town/City:{{$rentalObj->user->school->llocality}}</div>
<div class="divbottom">State/Region:{{$rentalObj->user->school->ladministrative_area_level_1}}</div>
<div  class="divbottom">Provincia:</div>
<div class="divbottom">Postal code:{{$rentalObj->user->school->lpostal_code}}</div>
<div class="divbottom">Country:{{$rentalObj->user->school->lcountry}}</div> 
<div class="divbottom">Telephone:{{$rentalObj->user->school->phonenumber}}</div> 
<div class="divbottom">e-mail:{{$rentalObj->user->school->school_email}}</div> 
<!-- Pending End-->

<div class="divbottom">Headmaster:{{$rentalObj->user->school->HeadMaster}}</div>
<!-- Doubt -->
<div class="divbottom"><p>Represented by</p></div>
<div class="divbottom">Name of School Tutor:{{$rentalObj->user->school->tutor_name}}</div>
<div class="divbottom">Surname of School Tutor:{{$rentalObj->user->school->tutor_surname}}</div>
<div class="divbottom">Place of birth:{{$rentalObj->user->school->tutor_pob}}</div>
<div class="divbottom">Tax Code -</div>
<div class="divbottom">e- mail:{{$rentalObj->user->school->tutor_email}}</div> 
<div class="divbottom">Telephone:{{$rentalObj->user->school->tutor_phone}}</div> 

  <div><h2>2) ORGANISATION - THE HOSTING PARTY</h2></div>
        <div class="divbottom">Organisation Name:{{$rentalObj->host->orgname}}</div>
        <div class="divbottom">Tax Code - Tax identification number:{{$rentalObj->host->NIN18}}</div>
        <!-- Legal Address -->
        <div class="divbottom" ><h3>Registered office/Legal Address</h3></div></tr>
        <div class="divbottom">Address street name:{{$rentalObj->host->router18}}</div>
        <div class="divbottom">Address street number:{{$rentalObj->host->street_numberr18}}</div>
        <div class="divbottom">Town/City:{{$rentalObj->host->localityr18}}</div>
        <div class="divbottom">State/Region:{{$rentalObj->host->administrative_area_level_1r18 }}</div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code:{{$rentalObj->host->postal_coder18}}</div>
        <div class="divbottom">Country:{{$rentalObj->host->countryr18}}</div>
        <!-- End Legal Address -->
<?php if($rentalObj->property->addr_work_org_same == 0) { ?>  
        <!-- Operative Address -->
        <div class="divbottom" ><h3>Operative Address where the work related learning traineeship takes place :</h3></div></tr>
        <div class="divbottom">Address street name:{{$rentalObj->property->wroute}}</div>
        <div class="divbottom">Address street number:{{$rentalObj->property->wstreet_number}}</div>
        <div class="divbottom">Town/City:{{$rentalObj->property->wlocality}}</div>
        <div class="divbottom">State/Region:{{$rentalObj->property->wadministrative_area_level_1 }}</div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code:{{$rentalObj->property->wpostal_code}}</div>
        <div class="divbottom">Country:{{$rentalObj->property->wcountry}}</div>
        <!-- End Legal Address -->

<?php } ?>
        <div class="divbottom">Telephone:{{$rentalObj->host->orgemail}}</div>
        <div class="divbottom">e-mail:{{$rentalObj->host->orgphone}}</div>
        <div class="divbottom"> Further (possible) operating office to conduct the Internship: <input type="radio" <?php if ($rentalObj->property->work_other_check == 1) { ?> checked <?php } ?> name="ss">Yes
        <input type="radio" <?php if ($rentalObj->property->work_other_check == 0) { ?> checked <?php } ?> name="ss">No
        </div>
        <div class="divbottom"> Other operative Address where the work related learning traineeship takes place: </div> 
        <?php $ln = "";
            if ($rentalObj->host->legal_nature == 1) {
$ln = "Public";
            }else if ($rentalObj->host->legal_nature == 0) {
$ln = "Private";
            }
        ?>

        <div class="divbottom"> Legal Nature (private or public): {{$ln}} </div> 
        <div class="divbottom" ><h3>RAMON CODE [CODICE ATECO 2007]: 99.00 Extra-territorial organizations and bodies [99.00 Organizzazioni ed Organi extraterritoriali]</h3></div> 
        <div class="divbottom">Human Resources:{{$rentalObj->user->roomtype->name}}</div> 
        

        <!-- Work Tutor Infomation  -->
        <div class="divbottom" ><h3> Represented by </h3></div> 
        
        <div class="divbottom">Name of Work Tutor: {{$rentalObj->property->represent_name}}</div> 
        <div class="divbottom"> Surname of Work Tutor:{{$rentalObj->property->represent_surname}}</div> 
        <div class="divbottom"> Place of birth: {{$rentalObj->property->represent_born}}</div>
        <div class="divbottom">Tax Code - Tax identification number:{{$rentalObj->property->represent_tax_no}}</div> 
        <div class="divbottom"> e- mail:  {{$rentalObj->property->represent_email}}</div> 
        <div class="divbottom"> Telephone:{{$rentalObj->property->represent_phone}} </div> 

        <div class="divbottom" ><h3> as EXTERNAL TUTOR (WORK TUTOR)</h3></div> 
        <div class="divbottom"> Role/Relationship in the Company: {{$rentalObj->property->tutorrelation->name}}</div> 
        <div class="divbottom"> Work Experience and skills you have (brief description of the experience and professional competencies as proof of the adequacy of your services as work Tutor: {{$rentalObj->property->work_tutor_fit}}</div> 
        <!-- End Work Tutor Infomation  -->

        <div class="divbottom">COMPENSATION:Empty</div> 
        <div class="divbottom">TRAINEESHIPS IN PROGRESS:doubt</div> 
        <div class="divbottom">Number curricular traineeships in progress:Empty</div> 
        <div class="divbottom">Number extracurricular traineeships in progress:Empty</div> 
        <div class="divbottom">Number of trainees assigned to commencement of the traineeship:Empty </div> 
    

    <div><h2>3) STUDENT TRAINE </h2></div>
        <div class="divbottom">Name of Student:{{$rentalObj->user->sname16}}</div>
        <div class="divbottom">Surname of Student:{{$rentalObj->user->ssurname16}}</div>
        <div class="divbottom">Place of birth:{{$rentalObj->user->pob16}}</div>
        <div class="divbottom">Date of Birth:{{$rentalObj->user->dob16}}</div>
        <div class="divbottom">Tax Code - Tax identification number: {{$rentalObj->user->NIN16}}</div>
        <div class="divbottom">e- mail:{{$rentalObj->user->semail16}}</div>
        <div  class="divbottom">Telephone:{{$rentalObj->user->sphone16}} </div>


        <div class="divbottom" ><h3>Residency Address</h3></div> 
        <div class="divbottom">Address street name:{{$rentalObj->user->router16}}</div>
        <div class="divbottom">Address street number:{{$rentalObj->user->street_numberr16}}</div>
        <div class="divbottom">Town/City:{{$rentalObj->user->localityr16}}</div>
        <div class="divbottom">State/Region:{{$rentalObj->user->administrative_area_level_1r16}}</div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code:{{$rentalObj->user->postal_coder16}}</div>
        <div class="divbottom">Country:{{$rentalObj->user->countryr16}}</div>



        <?php if($rentalObj->user->s16_res_dom_same == 0) { ?>    
        <div class="divbottom" ><h3>Domicile Address</h3></div></tr>
        <div class="divbottom">Address street name:{{$rentalObj->user->routerd6}}</div>
        <div class="divbottom">Address street number:{{$rentalObj->user->street_numberd16}}</div>
        <div class="divbottom">Town/City:{{$rentalObj->user->localityd16}}</div>
        <div class="divbottom">State/Region:{{$rentalObj->user->administrative_area_level_1d16}}</div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code:{{$rentalObj->user->postal_coded16}}</div>
        <div class="divbottom">Country:{{$rentalObj->user->countryd16}}</div>
        <?php }?>





        <div class="divbottom">CONDITIONS OF THE TRAINEE AT INITIATING TRAINEESHIP:{{$rentalObj->user->medical_learning}}</div> 
        <div class="divbottom">ACADEMIC QUALIFICATIONS: {{$rentalObj->user->academic_qualifications}}</div> 

    <div><h2>4) PARENT / LEGAL GUARDIAN OF THE UNDERAGE STUDENT TRAINEE</h2></div>
<?php if(is_null($rentalObj->user->student_relationship)) { ?>

    <div class="divbottom">Name: </div>
        <div class="divbottom">Surname: </div>
        <div class="divbottom">Place of birth: </div>
        <div class="divbottom">Date of Birth: </div>
        <div class="divbottom">Tax Code - Tax identification number: </div>
        
        <!-- Parent / Above 18 Address -->
        <div class="divbottom" ><h3>Residency Address</h3></div></tr>
        <div class="divbottom">Address street name: </div>
        <div class="divbottom">Address street number: </div>
        <div class="divbottom">Town/City: </div>
        <div class="divbottom">State/Region: </div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code: </div>
        <div class="divbottom">Country: </div>
  
 
 <div class="divbottom">As: </div>
<p><p>In case the data herein shown is the same for the student trainee and Parent it is because the student is over 18 and therefore we do not need any consent, signature for disclaimer or waivier for an underage trainee.</p>
 
<?php } else { ?>  
        <div class="divbottom">Name:{{$rentalObj->user->name}}</div>
        <div class="divbottom">Surname:{{$rentalObj->user->lastname}}</div>
        <div class="divbottom">Place of birth:{{$rentalObj->user->basicpob}}</div>
        <div class="divbottom">Date of Birth:{{$rentalObj->user->dob}}</div>
        <div class="divbottom">Tax Code - Tax identification number:{{$rentalObj->user->NIN18}}</div>
        
        <!-- Parent / Above 18 Address -->
        <div class="divbottom" ><h3>Residency Address</h3></div></tr>
        <div class="divbottom">Address street name:{{$rentalObj->user->raddress18}}</div>
        <div class="divbottom">Address street number:{{$rentalObj->user->street_numberr18}}</div>
        <div class="divbottom">Town/City:{{$rentalObj->user->localityr18}}</div>
        <div class="divbottom">State/Region:{{$rentalObj->user->administrative_area_level_1r18}}</div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code:{{$rentalObj->user->postal_coder18}}</div>
        <div class="divbottom">Country:{{$rentalObj->user->countryr18}}</div>

<?php if($rentalObj->user->s_res_dom_same == 0) { ?> 
        <!-- Parent / Above 18 Address -->
        <div class="divbottom" ><h3>Domicile Address</h3></div></tr>
        <div class="divbottom">Address street name:{{$rentalObj->user->routed18}}</div>
        <div class="divbottom">Address street number:{{$rentalObj->user->street_numberd18}}</div>
        <div class="divbottom">Town/City:{{$rentalObj->user->localityd18}}</div>
        <div class="divbottom">State/Region:{{$rentalObj->user->administrative_area_level_1d18}}</div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code:{{$rentalObj->user->postal_coded18}}</div>
        <div class="divbottom">Country:{{$rentalObj->user->countryd18}}</div>
<?php } ?>

        <!-- End Parent / Above 18 Address -->
<?php $relationship = "";
if($rentalObj->user->student_relationship == 1) { 
$relationship = "Mother";
    } elseif($rentalObj->user->student_relationship == 2) { 
$relationship = "Father";
    }else if($rentalObj->user->student_relationship == 3) { 
$relationship = "Legal Gaurdian";
    }
?>
 <div class="divbottom">As: {{$relationship}}</div>
<p><p>In case the data herein shown is the same for the student trainee and Parent it is because the student is over 18 and therefore we do not need any consent, signature for disclaimer or waivier for an underage trainee.</p>

<?php } ?>
<div>ARTICLE 8 <br>
Individual training project (traineeship pact)<br>
Curricular Educational Traineeship</div>
        <div class="divbottom">Date of Agreement:{{$rentalObj->dateAdded}}</div>
        <div class="divbottom">Reference number of Agreement:{{$rentalObj->Bookingno}}</div>
        <div class="divbottom"><h3>MANDATORY INSURANCE COVERAGE</h3></div></tr>
        <div class="divbottom">Insurance - with Company:{{$rentalObj->user->school->insurancewithcompany}}</div>
        <div class="divbottom">Insurance - Number of insurance policy:{{$rentalObj->user->school->noofpolicy}}</div>
        <div class="divbottom">Insurance - Number of insurance contract for Third Party Liability:{{$rentalObj->user->school->noofpolicythirdparty}}
        </div>
        <div class="divbottom">Insurance - Legal expenses cover number:{{$rentalObj->user->school->legalexpcovernumber}}
        </div>
        <div class="divbottom"><h3>WORK EXPERIENCE PLACEMENT DURATION, AIMS AND RULES
        </h3></div> 
        <div class="divbottom">Duration in hours at the address of the organization hosting traineeship:</div>
        <div class="divbottom">Start Date:{{$rentalObj->checkin}}
        </div>
        <div class="divbottom">End Date:{{$rentalObj->checkout}} 
        </div>
        <div class="divbottom">Duration in working hours of the experience (eg. 40):{{$rentalObj->property->work_hours}}
        </div>
        <div class="divbottom">Hourly articulation (daily and weekly) - Description - Enter daily and weekly scheduled working hours (eg. from 9am to 12.30pm and from 1pm to 5pm): {{$rentalObj->property->work_schedule}}
        </div>
        <div class="divbottom"><h3>OTHER POSSIBLE LOCATIONS WHERE THE WORK PLACE WILL TAKE PLACE
        </h3></div> 
        <div class="divbottom"> Would students do work at other addresses?: 
       
        <input type="radio" <?php if ($rentalObj->property->work_other_check == 1) { ?> checked <?php } ?> name="ss">Yes
        <input type="radio" <?php if ($rentalObj->property->work_other_check == 0) { ?> checked <?php } ?> name="ss">No
        </div>
        <div class="divbottom">Describe other workplaces and eventually addresses: {{$rentalObj->property->location_other_address}}
        </div>
        <div class="divbottom" ><h3>AREA OF INSERTION</h3></div> 
        <div class="divbottom">Work Category :{{$rentalObj->property->propertytype->name}}
        </div>
        <div class="divbottom" ><h3>ACTIVITIES - PURPOSE OF THE TRAINING 
        </h3></div> 
        <div class="divbottom">Duties : {{$rentalObj->property->description}}
        </div>
        <div class="divbottom">Rules: {{$rentalObj->property->houserules}}
        </div>
         <div class="divbottom">Description: {{$rentalObj->property->exptitle}}
        </div>
        <div class="divbottom">School Tutor extra optional Description :{{$rentalObj->property->exptitle}}</div>

  <div><h3>SIGNATURES</h3></div>
        <div class="divbottom">Reference number of Agreement:</div>
        <div class="divbottom"><h3>Signature and stamp</h3></div>
        <div class="divbottom">The Headmaster of the higher education institution(Signature and stamp)</div>
        <div class="divbottom margintop">Internal School tutor(Signature and stamp)</div>
        <div class="divbottom">Location & Date</div>
        <div class="divbottom margintop">Student doing Traineeship(signature)</div>
        <div class="divbottom margintop">Parent / Legal Guardian of a underage student</div>
        <div class="divbottom">Location & Date: --------------------                      ---------------------</div>
</div>
</body>
</html>