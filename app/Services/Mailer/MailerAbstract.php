<?php

namespace App\Services\Mailer;

use Illuminate\Translation\Translator;
use Illuminate\Config\Repository as Config;
use Illuminate\Mail\Mailer as IlluminateMailer;

 
abstract class MailerAbstract
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Translator
     */
    protected $lan;

    /**
     * @var Illuminate\Mail\Mailer
     */
    protected $mailer;
    /**
     * @var type adminEmail
     */
    protected $adminMail;
    /**
     * @var type adminName
     */
    protected $adminName;

    public function __construct(Translator $lan, IlluminateMailer $mailer, Config $config)
    {
        $this->mailer = $mailer;
        $this->lan = $lan;
        $this->config = $config;
        $this->init();
    }
    public function init()
    {
        $this->adminMail = $this->config->get('mail.admin_email');
        $this->adminName = $this->config->get('mail.admin_name');
    }
}
