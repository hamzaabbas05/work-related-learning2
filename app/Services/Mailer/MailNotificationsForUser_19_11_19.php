<?php

namespace App\Services\Mailer;

use App\User;
use App\RentalsEnquiry;
use Illuminate\Translation\Translator;

class MailNotificationsForUser extends MailerAbstract {

    public function __construct(Translator $lan, Mail $mail) {
        $this->lan = $lan;
        $this->mail = $mail;
    }

    public function sendtestmail($email) {
        $this->mail->sendTestMail($email, "gdfgdf");
    }

    public static function sendAdminUserVoucherCode($user_email, $voucher) {
        $data = array('user' => $user_email, 'voucher' => $voucher);
        \Mail::send('emails.adminSendVoucherCodeEmail', $data, function($message) use($user_email) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user_email, "New Invitation")
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Invio Voucher code ed istruzioni Iscrizione al portale www.work-related-learning.com - Alternanza scuola lavoro in contesto estero');
        });
        if (\Mail::failures()) {
            return "email sending fails";
        }
        return "email send";
    }

    public static function sendAdminUserVoucherCodetoPreEnrolled($user_email, $userData, $voucher) {
        // echo "<pre>";
        //     // print_r($userData[0]->tutor_name);
        //     // print_r($userData);
        //     print_r($voucher);
        // echo "</pre>";

        // exit;
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        //     echo "Tutor Detail<pre>";
        //         print_r($userData[0]->tutor_name);
        //         print_r($userData);
        //         print_r($voucher);
        //     echo "</pre>";

        // }
        $tutorName = (isset($userData[0]->tutor_name) && $userData[0]->tutor_name != '' ? $userData[0]->tutor_name : '');
        $tutorSurName = (isset($userData[0]->tutor_surname) && $userData[0]->tutor_surname != '' ? $userData[0]->tutor_surname : '');
        $tutorPob = (isset($userData[0]->tutor_pob) && $userData[0]->tutor_pob != '' ? $userData[0]->tutor_pob : '');
        $tutorPan = (isset($userData[0]->tutor_pan) && $userData[0]->tutor_pan != '' ? $userData[0]->tutor_pan : '');
        $tutorEmail = (isset($userData[0]->tutor_email) && $userData[0]->tutor_email != '' ? $userData[0]->tutor_email : '');
        $tutorPhone = (isset($userData[0]->tutor_phone) && $userData[0]->tutor_phone != '' ? $userData[0]->tutor_phone : '');
        $schoolEmail = (isset($userData[0]->school->school_email) && $userData[0]->school->school_email != '' ? $userData[0]->school->school_email : '');
        $phoneNumber = (isset($userData[0]->phonenumber) && $userData[0]->phonenumber != '' ? $userData[0]->phonenumber : '');

        $data = array('user' => $user_email, 'voucher' => $voucher,'tutor_name'=>$tutorName,'tutor_surname'=>$tutorSurName,'tutor_pob'=>$tutorPob,'tutor_pan'=>$tutorPan,'tutor_email'=>$tutorEmail,'tutor_email'=>$tutorEmail,'tutor_phone'=>$tutorPhone,'school_email' => $schoolEmail,'phonenumber'=>$phoneNumber);
    //     if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
    //         echo "Data Array <pre>";
    //     print_r($data);
    //     echo "</pre>";
    // // exit;
    //         //exit;            
    //     }

        \Mail::send('emails.adminSendVoucherCodeEmailPreEnrolled', $data, function($message) use($user_email) {
            // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            //     echo "UserEmail :".$user_email;
            //     exit;
            // }
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user_email, "New Invitation")
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('work-related-learning.com - Istruzioni completamento iscrizione al portale ed invio domande di lavoro - Alternanza scuola lavoro in contesto estero');
        });
        // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
        //     echo "End Here";
        // }
        if (\Mail::failures()) {
            return "email sending fails";
        }
        return "email send";
    }    
    
    public function sendemailmessage($receiverObj, $proObj, $enquiryObj) {
        $data = array('bookingno' => $enquiryObj->Bookingno, 'title' => $proObj->exptitle, 'name' => $receiverObj->name, 'enqid' => $enquiryObj->id);
        \Mail::send('emails.message', $data, function($message) use($receiverObj) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($receiverObj->email, $receiverObj->name)
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Work-Related-Learning.com - you have reveiced a message regarding a request');
        });
    }

    public function sendDeclineEmail($enquiryObj) {
        $data = array('bookingno' => $enquiryObj->Bookingno, 'name' => $enquiryObj->user->name);
        \Mail::send('emails.decline', $data, function($message) use($enquiryObj) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($enquiryObj->user->email, $enquiryObj->user->name)
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Work-Related-Learning - Request Declined');
        });
    }

    public function sendBookingCancelEmail($rentalObj) {
        $data = array('rentalObj' => $rentalObj, 'bookingno' => $rentalObj->Bookingno, 'name' => $rentalObj->user->name);
        if (isset($rentalObj->property->property_status)) {
            if ($rentalObj->property->property_status == "unpaid" || $rentalObj->property->property_status == "unpaidflash") {
                // sending email to work tutor
                //checking if the work tutor is not you
                if (isset($rentalObj->property->work_org_same) && $rentalObj->property->work_org_same == "0") {
                    if (isset($rentalObj->property->represent_email) && !empty($rentalObj->property->represent_email)) {
                        $tutorEmail = $rentalObj->property->represent_email;
                        \Mail::send('emails.cancelUnpaidBooking', $data, function($message) use($rentalObj, $tutorEmail) {
                            $name = isset($rentalObj->property->represent_name) ? $rentalObj->property->represent_name : "";
                            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                            $message->to($tutorEmail, $name)
                                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                                    ->subject('Work-Related-Learning - Work Experience Cancelled');
                        });
                    } else {
                        if (isset($rentalObj->property->user->email) && !empty($rentalObj->property->user->email)) {
                            $tutorEmail = $rentalObj->property->user->email;
                            \Mail::send('emails.cancelUnpaidBooking', $data, function($message) use($rentalObj, $tutorEmail) {
                                $name = isset($rentalObj->property->user->name) ? $rentalObj->property->user->name : "";
                                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                                $message->to($tutorEmail, $name)
                                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                                        ->subject('Work-Related-Learning - Work Experience Cancelled');
                            });
                        }
                    }
                } else {
                    if (isset($rentalObj->property->user->email) && !empty($rentalObj->property->user->email)) {
                        $tutorEmail = $rentalObj->property->user->email;
                        \Mail::send('emails.cancelUnpaidBooking', $data, function($message) use($rentalObj, $tutorEmail) {
                            $name = isset($rentalObj->property->user->name) ? $rentalObj->property->user->name : "";
                            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                            $message->to($tutorEmail, $name)
                                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                                    ->subject('Work-Related-Learning - Work Experience Cancelled');
                        });
                    }
                }
                \Mail::send('emails.cancelUnpaidBooking', $data, function($message) use($rentalObj) {
                    $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                    $message->to($rentalObj->user->email, $rentalObj->user->name)
                            ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                            ->subject('Work-Related-Learning - Work Experience Cancelled');
                });
            } else if ($rentalObj->property->property_status == "paid") {
                \Mail::send('emails.cancelPaidBooking', $data, function($message) use($rentalObj) {
                    $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                    $message->to($rentalObj->user->email, $rentalObj->user->name)
                            ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                            ->subject('Work-Related-Learning - Schoo House Cancelled');
                });
            }
        }
    }
// could you also comment as not all names are clear> like> sending email to requested schoolhouse for bed type okay
    public function sendBookingEmail($tutorEmail, $orgEmail, $propertyObject, $rentalObj) {
        if (!empty($tutorEmail)) {
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "worktutor", 'rentalObj' => $rentalObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.booking', $data, function($message) use($tutorEmail, $orgEmail,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($tutorEmail, 'Work Tutor')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        // ->cc("notification@work-related-learning.com", 'Work Tutor')
                        ->subject('Unpaid work experience - A student has applied for this Work Experience - from ' . $check_in . ' to ' . $check_out . ' - It is this student\'s First Choice (they can select three job listings)');
            });
        }
    }
    
    public function sendPaidBookingEmail($tutorEmail, $orgEmail, $propertyObject, $rentalObj) {
        if (!empty($tutorEmail)) {
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "worktutor", 'rentalObj' => $rentalObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.bookingPaid', $data, function($message) use($tutorEmail, $orgEmail,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($tutorEmail, 'Work Tutor')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        // ->cc("notification@work-related-learning.com", 'Work Tutor')
                        ->subject('School House servies - A student has requested for your services - from ' . $check_in . ' to ' . $check_out . ' - It is this student\'s First Choice (they can select three School Houses)');
            });
        }
    }

    public function adminsendBookingEmail($tutorEmail, $orgEmail, $propertyObject, $rentalObj) {
        if (!empty($tutorEmail)) {
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "worktutor", 'rentalObj' => $rentalObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.adminbooking', $data, function($message) use($tutorEmail, $orgEmail,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($tutorEmail, 'Work Tutor')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        // ->subject('Unpaid work experience - A student has applied for this Work Experience - It is this student\'s First Choice (they can select three job listings)');
                        ->subject('Unpaid work experience - A student has applied for this Work Experience - from ' . $check_in . ' to ' . $check_out . '  - It is this student\'s First Choice (they can select three job listings)');
            });
        }
    }

    public function adminPaidsendBookingEmail($tutorEmail, $orgEmail, $propertyObject, $rentalObj) {
        if (!empty($tutorEmail)) {
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "worktutor", 'rentalObj' => $rentalObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.adminPaidbooking', $data, function($message) use($tutorEmail, $orgEmail,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($tutorEmail, 'Work Tutor')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        // ->subject('Unpaid work experience - A student applied for this Work Experience - It is this student\'s First Choice (they can select three job listings)');
                        ->subject('Unpaid work experience - A student applied for this Work Experience - from ' . $check_in . ' to ' . $check_out . '  - It is this student\'s First Choice (they can select three job listings)');
            });
        }
    }

    public function sendUserListingBookingEmail($user_listing_email, $orgEmail, $propertyObject, $renter_user, $rentalObj) {
        if (!empty($user_listing_email)) {
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'renter_user' => $renter_user, "sent_to" => "userlisting", 'rentalObj' => $rentalObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.booking', $data, function($message) use($user_listing_email, $orgEmail,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($user_listing_email, 'User Listing')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        ->cc("notification@work-related-learning.com", 'User Listing')
                        // ->subject('Unpaid work experience - A student applied for this Work Experience - It is this student\'s First Choice (they can select three job listings)');
                        ->subject('Unpaid work experience - A student applied for this Work Experience - from ' . $check_in . ' to ' . $check_out . ' - It is this student\'s First Choice (they can select three job listings)');
            });
        }
    }

    public function adminsendUserListingBookingEmail($user_listing_email, $orgEmail, $propertyObject, $renter_user, $rentalObj) {
        if (!empty($user_listing_email)) {
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'renter_user' => $renter_user, "sent_to" => "userlisting", 'rentalObj' => $rentalObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.adminbooking', $data, function($message) use($user_listing_email, $orgEmail,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($user_listing_email, 'User Listing')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        // ->subject('Unpaid work experience - A student applied for this Work Experience - It is this student\'s First Choice (they can select three job listings)');
                        ->subject('Unpaid work experience - A student applied for this Work Experience - from ' . $check_in . ' to ' . $check_out . ' - It is this student\'s First Choice (they can select three job listings)');
            });
        }
    }

    public function adminPaidsendUserListingBookingEmail($user_listing_email, $orgEmail, $propertyObject, $renter_user, $rentalObj) {
        if (!empty($user_listing_email)) {
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'renter_user' => $renter_user, "sent_to" => "userlisting", 'rentalObj' => $rentalObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.adminPaidbooking', $data, function($message) use($user_listing_email, $orgEmail,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($user_listing_email, 'User Listing')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        // ->subject('Unpaid work experience - A student applied for this Work Experience - It is this student\'s First Choice (they can select three job listings)');
                        ->subject('Unpaid work experience - A student applied for this Work Experience - from ' . $check_in . ' to ' . $check_out . ' - It is this student\'s First Choice (they can select three job listings)');
            });
        }
    }
    public function sendPaidUserListingBookingEmail($user_listing_email, $orgEmail, $propertyObject, $renter_user, $rentalObj) {
        if (!empty($user_listing_email)) {
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'renter_user' => $renter_user, "sent_to" => "userlisting", 'rentalObj' => $rentalObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.bookingPaid', $data, function($message) use($user_listing_email, $orgEmail,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($user_listing_email, 'User Listing')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        // ->subject('School House servies - A student has requested for your services');
                        ->subject('School House servies - A student has requested for your services - from ' . $check_in . ' to ' . $check_out . ' - It is this student\'s First Choice (they can select three School Houses - www.Work-Related-Learning.com)');
            });
        }
    }
    
    public function sendPaidTypeSchoolHouseBookingEmail($user_listing_email, $orgEmail, $propertyObject, $renter_user, $rentalObj) {
        if (!empty($user_listing_email)) {
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'renter_user' => $renter_user, 
                "propertyObject" => $propertyObject, "sent_to" => "userlisting", 'rentalObj' => $rentalObj, 'token' => $token);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
             $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.bedtypeBookingToSchoolHouseEmail', $data, function($message) use($user_listing_email, $orgEmail,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($user_listing_email, 'User Listing')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        // ->subject('School House servies - You have requested for school house services');
                        ->subject('School House servies - A student has requested for your services - from ' . $check_in . ' to ' . $check_out . ' - It is this student\'s First Choice (they can select three School Houses - www.Work-Related-Learning.com)');
            });
        }
    }
    
    public function sendPaidTypeSchoolHouseUserBookingEmail($user_listing_email, $orgEmail, $propertyObject, $renter_user, $rentalObj) {
        if (!empty($user_listing_email)) {
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 
                "propertyObject" => $propertyObject, 'renter_user' => $renter_user, "sent_to" => "userlisting", 
                'rentalObj' => $rentalObj, 'token' => $token);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.bedtypeBookingToUserBrowsingEmail', $data, function($message) use($user_listing_email, $orgEmail,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($user_listing_email, 'User Listing')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        // ->subject('School House servies - A student has requested for your services - It is this student\'s First Choice (they can select three School Houses)');
                        ->subject('School House servies - A student has requested for your services - from ' . $check_in . ' to ' . $check_out . ' - It is this student\'s First Choice (they can select three School Houses - www.Work-Related-Learning.com)');
            });
        }
    }

    public function sendHRBookingEmail($hrEmail, $orgEmail, $propertyObject, $rentalObj) {
        $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "hr", 'rentalObj' => $rentalObj);
        $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
        $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
        \Mail::send('emails.humanResourceEmail', $data, function($message) use($hrEmail, $orgEmail, $check_in, $check_out) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($hrEmail, 'Human Resource')
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    //->cc("notification@work-related-learning.com", 'Human Resource')
                    // ->subject('First Choice - From ' . $check_in . ' to ' . $check_out . ' - A student applied for this unpaid work experience - It is this student\'s First Choice  (they can select three jobs) - www.Work-Related-Learning.com');
                    ->subject('First Choice - from ' . $check_in . ' to ' . $check_out . ' - A student applied for this unpaid work experience - It is this student\'s First Choice  (they can select three experiences) - www.Work-Related-Learning.com');
        });
    }

    public function adminsendHRBookingEmail($hrEmail, $orgEmail, $propertyObject, $rentalObj) {
        $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "hr", 'rentalObj' => $rentalObj);
        $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
        $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
        \Mail::send('emails.adminhumanResourceEmail', $data, function($message) use($hrEmail, $orgEmail, $check_in, $check_out) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($hrEmail, 'Human Resource')
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    // ->subject('First Choice - From ' . $check_in . ' to ' . $check_out . ' - A student applied for this unpaid work experience - It is this student\'s First Choice  (they can select three jobs) - www.Work-Related-Learning.com');
                    ->subject('First Choice - from ' . $check_in . ' to ' . $check_out . ' - A student applied for this unpaid work experience - It is this student\'s First Choice  (they can select three experiences) - www.Work-Related-Learning.com');
        });
    }

    public function adminPaidsendHRBookingEmail($hrEmail, $orgEmail, $propertyObject, $rentalObj) {
        $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "hr", 'rentalObj' => $rentalObj);
        $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
        $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
        \Mail::send('emails.adminPaidhumanResourceEmail', $data, function($message) use($hrEmail, $orgEmail, $check_in, $check_out) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($hrEmail, 'Human Resource')
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    // ->subject('First Choice - From ' . $check_in . ' to ' . $check_out . ' - A student applied for this unpaid work experience - It is this student\'s First Choice  (they can select three jobs) - www.Work-Related-Learning.com');
                    ->subject('First Choice - from ' . $check_in . ' to ' . $check_out . ' - A student applied for this unpaid work experience - It is this student\'s First Choice  (they can select three experiences) - www.Work-Related-Learning.com');
        });
    }
    
    public function sendPaidHRBookingEmail($hrEmail, $orgEmail, $propertyObject, $rentalObj) {
        $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "hr", 'rentalObj' => $rentalObj);
        $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
        $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
        \Mail::send('emails.humanResourceEmailpaid', $data, function($message) use($hrEmail, $orgEmail, $check_in, $check_out) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            // $message->to($hrEmail, 'Human Resource')
            //         ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
            //         ->subject('First Choice - From ' . $check_in . ' to ' . $check_out . ' - A student applied for this paid work experience - It is this student\'s First Choice  (they can select three school houses) - www.Work-Related-Learning.com');
            $message->to($hrEmail, 'User managing Requests')
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('First Choice - from ' . $check_in . ' to ' . $check_out . ' - A student applied for this paid service - It is this student\'s First Choice (they can select three school houses) - www.Work-Related-Learning.com');
        });
    }

    public function sendBookingNotification($propertyObject, $rentalObj,$tutorEmail='',$renterId = '') {
        // $email = "esperienzainglese@gmail.com";
        if(!empty($tutorEmail)){
            $email = $tutorEmail;
            //$email = "madrelinguastaff@gmail.com";
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "notification", 'rentalObj' => $rentalObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";

            \Mail::send('emails.humanResourceEmail', $data, function($message) use($email,$renterId,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($email, 'User offering Unpaid work experience')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning');
                        // ->cc("notification@work-related-learning.com", 'Notification Email');
                        if(!empty($renterId)){
                            $message->cc($renterId, 'work-related-learning');
                        }
                // $message->subject('Unpaid work experience - A student has applied for a 2nd of 3rd choice.');
                $message->subject('A student has applied - from ' . $check_in . ' to ' . $check_out . ' - for this work experience - It is this student\'s second or third choice  (they can select three experiences) - www.Work-Related-Learning.com');
            });
        }
    }
    
    public function sendPaidBookingNotification($propertyObject, $rentalObj,$tutorEmail='') {
        // $email = "esperienzainglese@gmail.com";
        if(!empty($tutorEmail)){
            $email = $tutorEmail; 
            //$email = "madrelinguastaff@gmail.com";
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "notification", 'rentalObj' => $rentalObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
                 $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            \Mail::send('emails.humanResourceEmailpaid', $data, function($message) use($email,$check_in,$check_out) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($email, 'Notification Email')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        ->cc("notification@work-related-learning.com", 'Notification Email')
                        // ->subject('School House services - A student has sent a request for a 2nd of 3rd choice.');
                        ->subject('School House servies - A student has requested for your services - from ' . $check_in . ' to ' . $check_out . ' - It is this student\'s second or third choice (they can select three School Houses) - www.Work-Related-Learning.com');
            });
        }
    }

    public function sendReferralWrongEmailNotification($referral_email_wrong, $user) {
        $email = "esperienzainglese@gmail.com";
        //$email = "madrelinguastaff@gmail.com";
        $data = array('referral_email_wrong' => $referral_email_wrong, "user" => $user);
        \Mail::send('emails.sendReferralWrongEmailNotification', $data, function($message) use($email, $user) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($email, 'Notification Referral Wrong Email')
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->cc($user->email, 'work-related-learning')
                    ->subject('FAO Admin - User Listing has Updated the School House Profile and provided an email which is not in our Database - Please check and contact user');
        });
    }
    
    public function sendReferralNoEmailNotification($referral_email_wrong, $user) {
        $email = "esperienzainglese@gmail.com";
        //$email = "madrelinguastaff@gmail.com";
        $data = array('referral_email_wrong' => $referral_email_wrong, "user" => $user);
        \Mail::send('emails.sendReferralNoEmailNotification', $data, function($message) use($email, $user) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($email, 'Notification No Referral Email was specified')
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->cc($user->email, 'work-related-learning')
                    ->subject('FAO Admin - User Listing has Updated the School House Profile and not provided an email (we need one for our Database - of a Member) - Please check and add.');
        });
    }
    
    public function sendListingNotification($propertyObject) {
        $email = "esperienzainglese@gmail.com";
        //$email = "madrelinguastaff@gmail.com";
        $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "notification");
        \Mail::send('emails.userListingEmail', $data, function($message) use($email) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($email, 'Notification Email')
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('FAO Admin - A User Listing has Created or Updated a listing - Please check and approve or email user');
        });
    }

    public function adminsendBookingNotification($propertyObject, $rentalObj,$userEmail='') {
        // $email = "esperienzainglese@gmail.com";
        if(!empty($userEmail)){
            $email = $userEmail;
        //$email = "basit.ullah.18@gmail.com";
        // echo $email; exit;
        //$email = "madrelinguastaff@gmail.com";
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "notification", 'rentalObj' => $rentalObj);
            try {
                \Mail::send('emails.adminhumanResourceEmail', $data, function($message) use($email) {
                    //$message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                    $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                    $message->to($email, 'Notification Email')
                            ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                            ->subject('Unpaid work experience - A student has applied for a 2nd of 3rd choice.');
                });
            } catch (\Exception $exc) {
                //echo $exc->getTraceAsString();
                //echo "mail is failed try again";
                //exit;
            }
        }
    }

    public function adminPaidsendBookingNotification($propertyObject, $rentalObj,$userEmail='') {
        // $email = "esperienzainglese@gmail.com";
        if(!empty($userEmail)){
            $email = $userEmail;
            $data = array('exptitle' => $propertyObject->exptitle, 'name' => $propertyObject->user->name, 'propertyObject' => $propertyObject, "sent_to" => "notification", 'rentalObj' => $rentalObj);
            \Mail::send('emails.adminPaidhumanResourceEmail', $data, function($message) use($email) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($email, 'Notification Email')
                        ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                        ->subject('A Student Guest has sent a request, please reply');
    //                    ->subject('Unpaid work experience - A student has applied for a 2nd of 3rd choice.');
            });
        }
    }

    public function sendPDF($email, $filename) {
        $data = array();
        \Mail::send('emails.emailpdf', $data, function($message) use($email, $filename) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($email, 'Tutor')
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Student Evaluation Report');
            $message->attach(public_path() . "/pdf/evaluation/$filename");
        });
    }

    public function sendemailPaidToSchoolHouseAboutStudent($school_house_email, $rentalObj, $filename) {
        if (!is_null($school_house_email)) {
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $data = array('property_id' => $rentalObj->property->id, 'rentalObj' => $rentalObj, 'token' => $token);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            $pick_up = isset($rentalObj->user->group_info->pick_up_where) ? $rentalObj->user->group_info->pick_up_where : "";
            $pick_up_at = isset($rentalObj->user->group_info->pick_up_time) ? $rentalObj->user->group_info->pick_up_time : "";
            // <from Grupo info> Pick up at <where> Victoria Embankment at <time>17.20
            $subject = "Group $check_in - $check_out Students, Work Tutors and School Tutor details - Pick up at $pick_up at $pick_up_at";
            \Mail::send('emails.paidToSchoolHouseAboutStudentEmail', $data, function($message) use($school_house_email, $subject, $filename) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($school_house_email, 'work-related-learning user');
                $message->cc("madrelinguastaff@gmail.com", 'work-related-learning user');
                $message->cc('learningagreements@work-related-learning.com', 'work-related-learning Notification')
                        ->subject($subject);
                //$message->attach(public_path() . "/pdf/agreement/$filename");
            });

            $email_sent = new \App\EmailSent();
            $email_sent->user_id = $rentalObj->user_id;
            $email_sent->property_id = $rentalObj->prd_id;
            $email_sent->email = $school_house_email;
            $email_sent->sent_to = "Paid Service school houses Email";
            $email_sent->token = $token;
            // check for failures
            if (\Mail::failures()) {
                $email_sent->is_sent = "Undelivered";
            } else {
                $email_sent->is_sent = "Sent";
            }
            $email_sent->save();
        }
        // sending notification OF WHAT? to learning agreement email
//        $random_token = rand("0", "9999999");
//        $token = base64_encode($random_token);
//        $data = array('property_id' => $rentalObj->property->id, 'rentalObj' => $rentalObj, 'token' => $token);
//        $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
//        $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
//        $pick_up = isset($rentalObj->user->group_info->pick_up_where) ? $rentalObj->user->group_info->pick_up_where : "";
//        $pick_up_at = isset($rentalObj->user->group_info->pick_up_time) ? $rentalObj->user->group_info->pick_up_time : "";
//        // <from Grupo info> Pick up at <where> Victoria Embankment at <time>17.20
//        $subject = "Group $check_in - $check_out Students, Work Tutors and School Tutor details - Pick up at $pick_up at $pick_up_at";
//        \Mail::send('emails.paidToSchoolHouseAboutStudentEmail', $data, function($message) use($school_house_email, $subject, $filename) {
//            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
//            $message->to('learningagreements@work-related-learning.com', 'work-related-learning Notification');
//            $message->cc("notification@work-related-learning.com", 'work-related-learning user')
//                    ->subject($subject);
//            //$message->attach(public_path() . "/pdf/agreement/$filename");
//        });
    }

    public function sendemailPaidToStudentAboutSchoolHouse($parentEmail, $studentEmail, $rentalObj) {
        $random_token = rand("0", "9999999");
        $token = base64_encode($random_token);
        $data = array('property_id' => $rentalObj->property->id, 'rentalObj' => $rentalObj, 'token' => $token);
        $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
        $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
        $subject = "Alternanza in contesto estero: dettagli della School House (Famiglia ospitante), informazioni generali sul viaggio, borse, contanti, mezzi, Wi-Fi, regolamenti compagnia aerea e varie. From $check_in to $check_out";
        \Mail::send('emails.parentofPaidEmail', $data, function($message) use($subject) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to("esperienzainglese@gmail.com", 'work-related-learning user');
            $message->cc("madrelinguastaff@gmail.com", 'work-related-learning user')
                    ->subject($subject);
        });
        if (!is_null($parentEmail)) {
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $data = array('property_id' => $rentalObj->property->id, 'rentalObj' => $rentalObj, 'token' => $token);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            $subject = "Alternanza in contesto estero: dettagli della School House (Famiglia ospitante), informazioni generali sul viaggio, borse, contanti, mezzi, Wi-Fi, regolamenti compagnia aerea e varie. From $check_in to $check_out";
            \Mail::send('emails.parentofPaidEmail', $data, function($message) use($parentEmail, $subject) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($parentEmail, 'work-related-learning user');
                $message->cc("madrelinguastaff@gmail.com", 'work-related-learning user')
                        ->subject($subject);
            });

            $email_sent = new \App\EmailSent();
            $email_sent->user_id = $rentalObj->user_id;
            $email_sent->property_id = $rentalObj->prd_id;
            $email_sent->email = $parentEmail;
            $email_sent->sent_to = "Paid Service Parent Email";
            $email_sent->token = $token;
            // check for failures
            if (\Mail::failures()) {
                $email_sent->is_sent = "Undelivered";
            } else {
                $email_sent->is_sent = "Sent";
            }
            $email_sent->save();
        }
        if (!is_null($studentEmail) && !empty($studentEmail)) {
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $data = array('property_id' => $rentalObj->property->id, 'rentalObj' => $rentalObj, 'token' => $token);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            $subject = "Alternanza in contesto estero: dettagli della School House (Famiglia ospitante), voli, informazioni generali sul viaggio, borse, contanti, mezzi, Wi-Fi, regolamenti compagnia aerea e varie. From $check_in to $check_out";
            \Mail::send('emails.parentofPaidEmail', $data, function($message) use($studentEmail, $subject) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($studentEmail, 'work-related-learning student');
                $message->cc("madrelinguastaff@gmail.com", 'work-related-learning student')
                        ->subject($subject);
            });

            $email_sent = new \App\EmailSent();
            $email_sent->user_id = $rentalObj->user_id;
            $email_sent->property_id = $rentalObj->prd_id;
            $email_sent->email = $studentEmail;
            $email_sent->sent_to = "Paid Service Student Email";
            $email_sent->token = $token;
            // check for failures
            if (\Mail::failures()) {
                $email_sent->is_sent = "Undelivered";
            } else {
                $email_sent->is_sent = "Sent";
            }
            $email_sent->save();
        }
    }

    //this email here under   is in Italian
    public function sendemailPaidToStudentAboutWalletUpdateForSchoolHouseAfterJobAccept($parentEmail, $studentEmail, $rentalObj,$acceptedUserObj) {
        if (!is_null($studentEmail) && !empty($studentEmail)) {
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $data = array('property_id' => $rentalObj->property->id, 'rentalObj' => $rentalObj, 'token' => $token,'userObj'=>$acceptedUserObj);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
            //     echo "Group ANme :". (isset($acceptedUserObj->group_info->group_name) && $acceptedUserObj->group_info->group_name != '' ? $acceptedUserObj->group_info->group_name : "nothing")."<br>";
            //     echo "<pre>";
            //         print_r($acceptedUserObj);
            //     echo "</pre>";
            //     exit;
            // }
            $subject = "Alternanza Scuola Lavoro in contesto estero - Gruppo ". (isset($acceptedUserObj->group_info->group_name) && $acceptedUserObj->group_info->group_name != '' ? $acceptedUserObj->group_info->group_name : "")." dal $check_in al $check_out - Scelta Famiglia Ospitante";

            //secondary_group_id
            // Alternanza in contesto estero: dettagli della School House (Famiglia ospitante), voli, informazioni generali sul viaggio, borse, contanti, mezzi, Wi-Fi, regolamenti compagnia aerea e varie. From $check_in to $check_out";
            $studentEmail = $acceptedUserObj->email;
           $isEmailSent =  \Mail::send('emails.acceptRequestTemplate', $data, function($message) use($studentEmail, $subject) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($studentEmail, 'work-related-learning student');
                $message->cc("madrelinguastaff@gmail.com", 'work-related-learning student')
                        ->subject($subject);
            });

            // $email_sent = new \App\EmailSent();
            // $email_sent->user_id = $rentalObj->user_id;
            // $email_sent->property_id = $rentalObj->prd_id;
            // $email_sent->email = $studentEmail;
            // $email_sent->sent_to = "Paid Service Student Email";
            // $email_sent->token = $token;
            // check for failures
            // if (\Mail::failures()) {
            //     $email_sent->is_sent = "Undelivered";
            // } else {
            //     $email_sent->is_sent = "Sent";
            // }
            // $email_sent->save();
        }
    }        

    public function sendPDFagreement($tutorEmail, $parentEmail, $studentEmail, $filename, $rentalObj, $hr_email, $hr_email_detail) {
           // if($_SERVER['REMOTE_ADDR'] == '93.33.90.87' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
           //      echo '<pre>';
           //          // print_r($property_status); one questionyes why dont you use prompt ip config_ I used to use from website and its accurate thats why. okay sorry for interruption. okay

           //          print_r($tutorEmail);
           //          echo "<br> HR <br>";
           //          print_r($hr_email);
           //          echo "<br> Rental obj <br>";
           //          print_r($rentalObj);
           //      echo '</pre>';
           //      // exit;
           //  }
    //        $random_token = rand("0", "9999999");
    //        $token = base64_encode($random_token);
    //        $data = array('name' => $rentalObj->user->name, 'token' => $token, 'rentalObj' => $rentalObj);
    //        \Mail::send('emails.studentpdfnew', $data, function($message) use($parentEmail, $filename, $rentalObj) {
    //            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
    //            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
    //            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
    //            $message->to('learningagreements@work-related-learning.com', 'Booking copy Recepit for admin')
    //                    ->subject('Alternanza in contesto estero: dettagli posti di lavoro e Tutor al lavoro dal ' . $check_in . ' al ' . $check_out . '.');
    //            $message->attach(public_path() . "/pdf/agreement/$filename");
            //well no ip condition here for work tutor
    //        });

            if (!empty($parentEmail)) {
                //$parentEmail = "basit.ullah.18@gmail.com";
                $random_token = rand("0", "9999999");
                $token = base64_encode($random_token);
                $data = array('name' => $rentalObj->user->name, 'token' => $token, 'rentalObj' => $rentalObj);
                \Mail::send('emails.parentpdfnew', $data, function($message) use($parentEmail, $filename, $rentalObj) {
                    $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
                    $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
                    $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                    $message->to($parentEmail, 'Parent');
                    $message->cc("madrelinguastaff@gmail.com", 'work-related-learning Parent');
                    $message->cc('learningagreements@work-related-learning.com', 'Booking copy Recepit for admin')
                            ->subject('Alternanza in contesto estero: dettagli posti di lavoro e Tutor al lavoro dal ' . $check_in . ' al ' . $check_out . '.');
                            if(is_array($filename)){
                                foreach($filename as $filePath=>$fileName){
                                    $message->attach(public_path() . "/pdf/".$filePath."/".$fileName);
                                }
                            }else{
                                $message->attach(public_path() . "/pdf/agreement/" . $filename);
                            }                        
                    // $message->attach(public_path() . "/pdf/agreement/$filename");
                });
                $email_sent = new \App\EmailSent();
                $email_sent->user_id = $rentalObj->user_id;
                $email_sent->property_id = $rentalObj->prd_id;
                $email_sent->email = $parentEmail;
                $email_sent->sent_to = "Parent Email";
                $email_sent->token = $token;
                // check for failures
                if (\Mail::failures()) {
                    $email_sent->is_sent = "Undelivered";
                } else {
                    $email_sent->is_sent = "Sent";
                }
                $email_sent->save();
    //            $data = array('name' => $rentalObj->user->name);
    //            \Mail::send('emails.parentpdf', $data, function($message) use($parentEmail, $filename) {
    //                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
    //                $message->to($parentEmail, 'Parent')
    //                        ->subject('Work-Related-Learning.com - Unpaid work experience - applicaiton confimration and Learning Agreement');
    //                $message->attach(public_path() . "/pdf/agreement/$filename");
    //            });
                //echo "email send to = ".$parentEmail;
                //exit;
            }
            if (!empty($studentEmail) && !is_null($studentEmail)) {
                $random_token = rand("0", "9999999");
                $token = base64_encode($random_token);
                $data = array('name' => $rentalObj->user->sname16, 'token' => $token, 'rentalObj' => $rentalObj);
                \Mail::send('emails.studentpdfnew', $data, function($message) use($studentEmail, $filename, $rentalObj) {
                    $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
                    $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
                    $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                    $message->to($studentEmail, 'Student');
                    $message->cc("madrelinguastaff@gmail.com", 'Student')
                            ->subject('Alternanza in contesto estero: dettagli posti di lavoro e Tutor al lavoro dal ' . $check_in . ' al ' . $check_out . '.');
                    if(is_array($filename)){
                        foreach($filename as $filePath=>$fileName){
                            $message->attach(public_path() . "/pdf/".$filePath."/".$fileName);
                        }
                    }else{
                        $message->attach(public_path() . "/pdf/agreement/" . $filename);
                    }
                    // $message->attach(public_path() . "/pdf/agreement/$filename");
                });
                $email_sent = new \App\EmailSent();
                $email_sent->user_id = $rentalObj->user_id;
                $email_sent->property_id = $rentalObj->prd_id;
                $email_sent->email = $studentEmail;
                $email_sent->sent_to = "Student Email";
                $email_sent->token = $token;
                // check for failures
                if (\Mail::failures()) {
                    $email_sent->is_sent = "Undelivered";
                } else {
                    $email_sent->is_sent = "Sent";
                }
                $email_sent->save();

    //            $data = array('name' => $rentalObj->user->sname16);
    //            \Mail::send('emails.emailpdf', $data, function($message) use($studentEmail, $filename) {
    //                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
    //                $message->to($studentEmail, 'Student')
    //                        ->subject('Learning Agreement');
    //                $message->attach(public_path() . "/pdf/agreement/" . $filename);
    //            });
            }
            //$data = array('name' => $rentalObj->user->school->tutor_name); $studentEmail sure_ this is user . user listing, parent and stuent get another email not this one for work tutor & user listing unpaid work okay let me confirm then .

            ///if ($rentalObj->is_hr != "1") {

    //            $tutor_name = isset($rentalObj->property->represent_name) ? $rentalObj->property->represent_name . " " : "";
    //            $tutor_name = $tutor_name . isset($rentalObj->property->represent_surname) ? $rentalObj->property->represent_surname : "";
    //            $data = array('name' => $tutor_name, 'property_id' => $rentalObj->property->id);
    //            \Mail::send('emails.emailpdf', $data, function($message) use($tutorEmail, $filename) {
    //                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
    //                $message->to($tutorEmail, 'Tutor')
    //                        ->subject('Learning Agreement');
    //                $message->attach(public_path() . "/pdf/agreement/" . $filename);
    //            });
            //} else {
                // sending email to HR if any
            if (!empty($hr_email) ) {
                    // if($hr_email == 'jinesh_hr.test@mydomainc.tk'){
                    //     $hr_email = 'objectscodes@gmail.com';
                    // }
                  $hr_name = isset($hr_email_detail->hr_represent_name) ? $hr_email_detail->hr_represent_name . " " : '' . isset($hr_email_detail->hr_represent_surname) ? $hr_email_detail->hr_represent_surname : "";
                  $data = array('name' => $hr_name);
                  $userListingEmail = (isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : '');
              // if($_SERVER['REMOTE_ADDR'] == '93.36.17.217'){
              //       $templateName  = 'emails.emailpdfHR_new'; //this is for new template for HR content.yes it will send old one , old one means no need to send new one? I disagree, old one is wrong thats why gave you new text, beause I am checking by sending and check for the correct content as there is new value tutor name and etails so I would first check for our end then make it live. I would have simply changed email contant as we found it was wrong, no ood sending wron gemails.. so remove this and make it as it was?no please change text in the email as I gave you okay wait

              //   }else{ correct name before I start this is the name so I have just copy, so yes yesokay
                    $templateName  = 'emails.emailpdfHR';
                // }

                   // echo "After Email set Detials : <br> tutor email = ".$tutorEmail."<br />";
                   // echo "HR email = ".$hr_email."<br />";
                   // echo "Parent email = ".$parentEmail."<br />";
                   // echo "Student email = ".$studentEmail."<br />";
                   // echo "User Listing Email =".$userListingEmail."<br>";
                   // echo "is HR = ".$rentalObj->is_hr."<br />";

                  \Mail::send('emails.emailpdfHR', $data, function($message) use($hr_email, $filename,$userListingEmail,$tutorEmail) {
                    
                  $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                  $message->to($hr_email, 'Human Resource');
                  $message->cc('madrelinguastaff@gmail.com', 'System email'); 
                  if(!empty($tutorEmail)){
                    // echo "<br> Fall into tutor Email condition.".$tutorEmail."<br>";
                    $message->cc($tutorEmail, 'Tutor'); 
                 } //else{
                    // echo "<br> Fall into Else tutor part"."<br> Tutor Email :".$tutorEmail."<br>";

                 // }              
                  // $message->cc($userListingEmail, 'Human Resource')
                  $message->cc($userListingEmail, 'User Listing')
                  ->subject('Learning Agreement');
                  if(is_array($filename)){
                        foreach($filename as $filePath=>$fileName){
                            $message->attach(public_path() . "/pdf/".$filePath."/".$fileName);
                        }
                    }else{
                        $message->attach(public_path() . "/pdf/agreement/" . $filename);
                    } 
                  // $message->attach(public_path() . "/pdf/agreement/" . $filename);
                  });
                  } 
            //}        
            if (!empty($tutorEmail) && !is_null($tutorEmail)) {
                $random_token = rand("0", "9999999");
                $token = base64_encode($random_token);
                $tutor_name = isset($rentalObj->property->represent_name) ? $rentalObj->property->represent_name . " " : "";
                $tutor_name = $tutor_name . isset($rentalObj->property->represent_surname) ? $rentalObj->property->represent_surname : "";
                $userListingEmail = (isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : ''); 
                $data = array('name' => $tutor_name, 'property_id' => $rentalObj->property->id, 'rentalObj' => $rentalObj, 'token' => $token);
                $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
                $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
                $subject = "FAO $tutor_name - Over 16 Italian Secondary Senior School student arriving on $check_in (and starting work on agreed date) and leaving on $check_out";
                
                    $templateName  = 'emails.worktutoremailpdf'; 

                \Mail::send($templateName, $data, function($message) use($tutorEmail, $filename, $subject,$userListingEmail) 
                {
                    $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                    $message->to($userListingEmail, 'User Listing');
                    $message->cc($tutorEmail, 'Tutor');
                    $message->cc("madrelinguastaff@gmail.com", 'System email')
                            ->subject($subject);
                    if(is_array($filename)){
                        foreach($filename as $filePath=>$fileName){
                            $message->attach(public_path() . "/pdf/".$filePath."/".$fileName);
                        }
                    }else{
                        $message->attach(public_path() . "/pdf/agreement/" . $filename);
                    }
                    // $message->attach(public_path() . "/pdf/agreement/" . $filename);
                });

                $email_sent = new \App\EmailSent();
                $email_sent->user_id = $rentalObj->user_id;
                $email_sent->property_id = $rentalObj->prd_id;
                $email_sent->email = $tutorEmail;
                $email_sent->sent_to = "Work Tutor Email";
                $email_sent->token = $token;
                // check for failures
                if (\Mail::failures()) {
                    $email_sent->is_sent = "Undelivered";
                } else {
                    $email_sent->is_sent = "Sent";
                }
                $email_sent->save();
            }

        }

    public function sendPDFagreement_14_11_2019($tutorEmail, $parentEmail, $studentEmail, $filename, $rentalObj, $hr_email, $hr_email_detail) {
        // if($_SERVER['REMOTE_ADDR'] == '93.36.90.227'){
        //     echo '<pre>';
        //         print_r($tutorEmail);
        //         echo "<br> HR <br>";
        //         print_r($hr_email);
        //         echo "<br> Rental obj <br>";
        //         print_r($rentalObj);
        //     echo '</pre>';
        //     // exit;
        // }
//        $random_token = rand("0", "9999999");
//        $token = base64_encode($random_token);
//        $data = array('name' => $rentalObj->user->name, 'token' => $token, 'rentalObj' => $rentalObj);
//        \Mail::send('emails.studentpdfnew', $data, function($message) use($parentEmail, $filename, $rentalObj) {
//            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
//            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
//            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
//            $message->to('learningagreements@work-related-learning.com', 'Booking copy Recepit for admin')
//                    ->subject('Alternanza in contesto estero: dettagli posti di lavoro e Tutor al lavoro dal ' . $check_in . ' al ' . $check_out . '.');
//            $message->attach(public_path() . "/pdf/agreement/$filename");
        //well no ip condition here for work tutor
//        });

        if (!empty($parentEmail)) {
            //$parentEmail = "basit.ullah.18@gmail.com";
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $data = array('name' => $rentalObj->user->name, 'token' => $token, 'rentalObj' => $rentalObj);
            \Mail::send('emails.parentpdfnew', $data, function($message) use($parentEmail, $filename, $rentalObj) {
                $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
                $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($parentEmail, 'Parent');
                $message->cc("madrelinguastaff@gmail.com", 'work-related-learning Parent');
                $message->cc('learningagreements@work-related-learning.com', 'Booking copy Recepit for admin')
                        ->subject('Alternanza in contesto estero: dettagli posti di lavoro e Tutor al lavoro dal ' . $check_in . ' al ' . $check_out . '.');
                        if(is_array($filename)){
                            foreach($filename as $filePath=>$fileName){
                                $message->attach(public_path() . "/pdf/".$filePath."/".$fileName);
                            }
                        }else{
                            $message->attach(public_path() . "/pdf/agreement/" . $filename);
                        }                        
                // $message->attach(public_path() . "/pdf/agreement/$filename");
            });
            $email_sent = new \App\EmailSent();
            $email_sent->user_id = $rentalObj->user_id;
            $email_sent->property_id = $rentalObj->prd_id;
            $email_sent->email = $parentEmail;
            $email_sent->sent_to = "Parent Email";
            $email_sent->token = $token;
            // check for failures
            if (\Mail::failures()) {
                $email_sent->is_sent = "Undelivered";
            } else {
                $email_sent->is_sent = "Sent";
            }
            $email_sent->save();
//            $data = array('name' => $rentalObj->user->name);
//            \Mail::send('emails.parentpdf', $data, function($message) use($parentEmail, $filename) {
//                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
//                $message->to($parentEmail, 'Parent')
//                        ->subject('Work-Related-Learning.com - Unpaid work experience - applicaiton confimration and Learning Agreement');
//                $message->attach(public_path() . "/pdf/agreement/$filename");
//            });
            //echo "email send to = ".$parentEmail;
            //exit;
        }
        if (!empty($studentEmail) && !is_null($studentEmail)) {
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $data = array('name' => $rentalObj->user->sname16, 'token' => $token, 'rentalObj' => $rentalObj);
            \Mail::send('emails.studentpdfnew', $data, function($message) use($studentEmail, $filename, $rentalObj) {
                $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
                $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($studentEmail, 'Student');
                $message->cc("madrelinguastaff@gmail.com", 'Student')
                        ->subject('Alternanza in contesto estero: dettagli posti di lavoro e Tutor al lavoro dal ' . $check_in . ' al ' . $check_out . '.');
                if(is_array($filename)){
                    foreach($filename as $filePath=>$fileName){
                        $message->attach(public_path() . "/pdf/".$filePath."/".$fileName);
                    }
                }else{
                    $message->attach(public_path() . "/pdf/agreement/" . $filename);
                }
                // $message->attach(public_path() . "/pdf/agreement/$filename");
            });
            $email_sent = new \App\EmailSent();
            $email_sent->user_id = $rentalObj->user_id;
            $email_sent->property_id = $rentalObj->prd_id;
            $email_sent->email = $studentEmail;
            $email_sent->sent_to = "Student Email";
            $email_sent->token = $token;
            // check for failures
            if (\Mail::failures()) {
                $email_sent->is_sent = "Undelivered";
            } else {
                $email_sent->is_sent = "Sent";
            }
            $email_sent->save();

//            $data = array('name' => $rentalObj->user->sname16);
//            \Mail::send('emails.emailpdf', $data, function($message) use($studentEmail, $filename) {
//                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
//                $message->to($studentEmail, 'Student')
//                        ->subject('Learning Agreement');
//                $message->attach(public_path() . "/pdf/agreement/" . $filename);
//            });
        }
        //$data = array('name' => $rentalObj->user->school->tutor_name); $studentEmail sure_ this is user . user listing, parent and stuent get another email not this one for work tutor & user listing unpaid work okay let me confirm then .

        if ($rentalObj->is_hr != "1") {

//            $tutor_name = isset($rentalObj->property->represent_name) ? $rentalObj->property->represent_name . " " : "";
//            $tutor_name = $tutor_name . isset($rentalObj->property->represent_surname) ? $rentalObj->property->represent_surname : "";
//            $data = array('name' => $tutor_name, 'property_id' => $rentalObj->property->id);
//            \Mail::send('emails.emailpdf', $data, function($message) use($tutorEmail, $filename) {
//                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
//                $message->to($tutorEmail, 'Tutor')
//                        ->subject('Learning Agreement');
//                $message->attach(public_path() . "/pdf/agreement/" . $filename);
//            });
        } else {
            // sending email to HR if any
             if (!empty($hr_email)) {
              $hr_name = isset($hr_email_detail->hr_represent_name) ? $hr_email_detail->hr_represent_name . " " : '' . isset($hr_email_detail->hr_represent_surname) ? $hr_email_detail->hr_represent_surname : "";
              $data = array('name' => $hr_name);
              $userListingEmail = (isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : '');
          // if($_SERVER['REMOTE_ADDR'] == '93.36.17.217'){
          //       $templateName  = 'emails.emailpdfHR_new'; //this is for new template for HR content.yes it will send old one , old one means no need to send new one? I disagree, old one is wrong thats why gave you new text, beause I am checking by sending and check for the correct content as there is new value tutor name and etails so I would first check for our end then make it live. I would have simply changed email contant as we found it was wrong, no ood sending wron gemails.. so remove this and make it as it was?no please change text in the email as I gave you okay wait

          //   }else{ correct name before I start this is the name so I have just copy, so yes yesokay
                $templateName  = 'emails.emailpdfHR';
            // }
              \Mail::send('emails.emailpdfHR', $data, function($message) use($hr_email, $filename,$userListingEmail,$tutorEmail) {
              $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
              $message->to($hr_email, 'Human Resource');
              if(!empty($tutorEmail)){
                $message->cc($tutorEmail, 'Tutor'); 
              }              
                $message->cc('madrelinguastaff@gmail.com', 'System email'); 
              // $message->cc($userListingEmail, 'Human Resource')
              $message->cc($userListingEmail, 'User Listing')
              ->subject('Learning Agreement');
              if(is_array($filename)){
                    foreach($filename as $filePath=>$fileName){
                        $message->attach(public_path() . "/pdf/".$filePath."/".$fileName);
                    }
                }else{
                    $message->attach(public_path() . "/pdf/agreement/" . $filename);
                } 
              // $message->attach(public_path() . "/pdf/agreement/" . $filename);
              });
              } 
        }        
        if (!empty($tutorEmail) && !is_null($tutorEmail)) {
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $tutor_name = isset($rentalObj->property->represent_name) ? $rentalObj->property->represent_name . " " : "";
            $tutor_name = $tutor_name . isset($rentalObj->property->represent_surname) ? $rentalObj->property->represent_surname : "";
            $userListingEmail = (isset($rentalObj->property->user->email) ? $rentalObj->property->user->email : ''); 
            $data = array('name' => $tutor_name, 'property_id' => $rentalObj->property->id, 'rentalObj' => $rentalObj, 'token' => $token);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            $subject = "FAO $tutor_name - Over 16 Italian Secondary Senior School student arriving on $check_in (and starting work on agreed date) and leaving on $check_out";
            
                $templateName  = 'emails.worktutoremailpdf';                

            \Mail::send($templateName, $data, function($message) use($tutorEmail, $filename, $subject,$userListingEmail) 
            {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($tutorEmail, 'Tutor');
                $message->cc("madrelinguastaff@gmail.com", 'System email');
                $message->cc($userListingEmail, 'User Listing')
                        ->subject($subject);
                if(is_array($filename)){
                    foreach($filename as $filePath=>$fileName){
                        $message->attach(public_path() . "/pdf/".$filePath."/".$fileName);
                    }
                }else{
                    $message->attach(public_path() . "/pdf/agreement/" . $filename);
                }
                // $message->attach(public_path() . "/pdf/agreement/" . $filename);
            });

            $email_sent = new \App\EmailSent();
            $email_sent->user_id = $rentalObj->user_id;
            $email_sent->property_id = $rentalObj->prd_id;
            $email_sent->email = $tutorEmail;
            $email_sent->sent_to = "Work Tutor Email";
            $email_sent->token = $token;
            // check for failures
            if (\Mail::failures()) {
                $email_sent->is_sent = "Undelivered";
            } else {
                $email_sent->is_sent = "Sent";
            }
            $email_sent->save();
        }

    }

    
    public function sendPDFagreement_new($tutorEmail, $parentEmail, $studentEmail, $filename, $rentalObj, $hr_email, $hr_email_detail,$filename1 = '') {

//        $random_token = rand("0", "9999999");
//        $token = base64_encode($random_token);
//        $data = array('name' => $rentalObj->user->name, 'token' => $token, 'rentalObj' => $rentalObj);
//        \Mail::send('emails.studentpdfnew', $data, function($message) use($parentEmail, $filename, $rentalObj) {
//            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
//            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
//            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
//            $message->to('learningagreements@work-related-learning.com', 'Booking copy Recepit for admin')
//                    ->subject('Alternanza in contesto estero: dettagli posti di lavoro e Tutor al lavoro dal ' . $check_in . ' al ' . $check_out . '.');
//            $message->attach(public_path() . "/pdf/agreement/$filename");
//        });

        if (!empty($parentEmail)) {
            //$parentEmail = "basit.ullah.18@gmail.com";
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $data = array('name' => $rentalObj->user->name, 'token' => $token, 'rentalObj' => $rentalObj);
            \Mail::send('emails.parentpdfnew', $data, function($message) use($parentEmail, $filename, $rentalObj) {
                $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
                $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                // $message->to($parentEmail, 'Parent'); //Student email
                $message->to($parentEmail, 'Student email');
                $message->cc("madrelinguastaff@gmail.com", 'work-related-learning Parent');
                $message->cc('learningagreements@work-related-learning.com', 'Booking copy Recepit for admin')
                        ->subject('Alternanza in contesto estero: dettagli posti di lavoro e Tutor al lavoro dal ' . $check_in . ' al ' . $check_out . '.');
                $message->attach(public_path() . "/pdf/agreement/$filename");
                
            });
            $email_sent = new \App\EmailSent();
            $email_sent->user_id = $rentalObj->user_id;
            $email_sent->property_id = $rentalObj->prd_id;
            $email_sent->email = $parentEmail;
            $email_sent->sent_to = "Parent Email";
            $email_sent->token = $token;
            // check for failures
            if (\Mail::failures()) {
                $email_sent->is_sent = "Undelivered";
            } else {
                $email_sent->is_sent = "Sent";
            }
            $email_sent->save();
//            $data = array('name' => $rentalObj->user->name);
//            \Mail::send('emails.parentpdf', $data, function($message) use($parentEmail, $filename) {
//                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
//                $message->to($parentEmail, 'Parent')
//                        ->subject('Work-Related-Learning.com - Unpaid work experience - applicaiton confimration and Learning Agreement');
//                $message->attach(public_path() . "/pdf/agreement/$filename");
//            });
            //echo "email send to = ".$parentEmail;
            //exit;
        }
        if (!empty($studentEmail) && !is_null($studentEmail)) {
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $data = array('name' => $rentalObj->user->sname16, 'token' => $token, 'rentalObj' => $rentalObj);
            \Mail::send('emails.studentpdfnew', $data, function($message) use($studentEmail, $filename, $rentalObj) {
                $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
                $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($studentEmail, 'Student');
                $message->cc("madrelinguastaff@gmail.com", 'Student')
                        ->subject('Alternanza in contesto estero: dettagli posti di lavoro e Tutor al lavoro dal ' . $check_in . ' al ' . $check_out . '.');
                $message->attach(public_path() . "/pdf/agreement/$filename");
            });
            $email_sent = new \App\EmailSent();
            $email_sent->user_id = $rentalObj->user_id;
            $email_sent->property_id = $rentalObj->prd_id;
            $email_sent->email = $studentEmail;
            $email_sent->sent_to = "Student Email";
            $email_sent->token = $token;
            // check for failures
            if (\Mail::failures()) {
                $email_sent->is_sent = "Undelivered";
            } else {
                $email_sent->is_sent = "Sent";
            }
            $email_sent->save();

        }
        //$data = array('name' => $rentalObj->user->school->tutor_name);
        if (!empty($tutorEmail) && !is_null($tutorEmail)) {
            $random_token = rand("0", "9999999");
            $token = base64_encode($random_token);
            $tutor_name = isset($rentalObj->property->represent_name) ? $rentalObj->property->represent_name . " " : "";
            $tutor_name = $tutor_name . isset($rentalObj->property->represent_surname) ? $rentalObj->property->represent_surname : "";
            $data = array('name' => $tutor_name, 'property_id' => $rentalObj->property->id, 'rentalObj' => $rentalObj, 'token' => $token);
            $check_in = isset($rentalObj->checkin) ? date("F j, Y", strtotime($rentalObj->checkin)) : "";
            $check_out = isset($rentalObj->checkout) ? date("F j, Y", strtotime($rentalObj->checkout)) : "";
            $subject = "FAO $tutor_name - Over 16 Italian Secondary Senior School student arriving on $check_in (and starting work on agreed date) and leaving on $check_out";
            \Mail::send('emails.worktutoremailpdf', $data, function($message) use($tutorEmail, $filename, $subject) {
                $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
                $message->to($tutorEmail, 'Tutor');
                $message->cc("madrelinguastaff@gmail.com", 'Tutor')
                        ->subject($subject);
                $message->attach(public_path() . "/pdf/agreement/" . $filename);
            });

            $email_sent = new \App\EmailSent();
            $email_sent->user_id = $rentalObj->user_id;
            $email_sent->property_id = $rentalObj->prd_id;
            $email_sent->email = $tutorEmail;
            $email_sent->sent_to = "Work Tutor Email";
            $email_sent->token = $token;
            // check for failures
            if (\Mail::failures()) {
                $email_sent->is_sent = "Undelivered";
            } else {
                $email_sent->is_sent = "Sent";
            }
            $email_sent->save();
        }
        
    }

    public function sendEvaluationEmail(RentalsEnquiry $enquiry) {
        $encode = $enquiry->id . "," . $enquiry->property->represent_email;
        $encodedValue = base64_encode($encode);
        $data = array('encodedValue' => $encodedValue);
        \Mail::send('emails.result', $data, function($message) use($enquiry) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($enquiry->property->represent_email, $enquiry->property->represent_email)
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Evaluate Student Performance');
        });
    }

    public function sendConfirmationEmail($confirmCode, User $user) {
        $data = array('confirmCode' => $confirmCode, 'username' => $user->name, 'email' => $user->email);
        \Mail::send('emails.verifyemail', $data, function($message) use($user) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user->email, $user->email)
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Verify your email address on www.work-related-learning.com');
        });
    }
    
    public function sendVoucherNewSchoolHouseEmail($voucher_interest, User $user) {
        $data = array('voucher_interest' => $voucher_interest, 'user' => $user);
        \Mail::send('emails.newSchoolHouseVoucherEmail', $data, function($message) use($user) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user->email, $user->email)
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    // ->subject('Your Interest in www.work-related-learning.com');
                    ->subject('Thank you for showing interest in www.work-related-learning.com');
        });
    }
    
    public function sendVoucherNewWorkTutorEmail($voucher_interest, User $user) {
        $data = array('voucher_interest' => $voucher_interest, 'user' => $user);
        \Mail::send('emails.newWorkTutorVoucherEmail', $data, function($message) use($user) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user->email, $user->email)
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    // ->subject('Your Interest in www.work-related-learning.com');
                    ->subject('Thank you for showing interest in www.work-related-learning.com');
        });
    }
    
    public function sendVoucherNewParentInteretedEmail($voucher_interest, User $user) {
        $data = array('voucher_interest' => $voucher_interest, 'user' => $user);
        \Mail::send('emails.newParentInteretedVoucherEmail', $data, function($message) use($user) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user->email,"New user")
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Your Interest in www.work-related-learning.com');
        });
    }
    
   public static function sendAdminAddUserVoucherCode(User $user, $voucher) {
        $data = array('voucher' => $voucher, 'user' => $user);
        \Mail::send('emails.sendVoucherNewParentInteretedEmail', $data, function($message) use($user) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user->email, $user->email)
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('User Wallet is updated - proceed to sending requests in www.work-related-learning.com');
        });
    }

    public function sendReviewSystemEmail($property, $user_detail) {
        $data = array('property' => $property, 'user_detail' => $user_detail);
        \Mail::send('emails.sendReviewEmail', $data, function($message) use($user_detail) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user_detail->email, $user_detail->name)
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Hi, how was your experience? Share your opinion and help other Student Guests pick the best places, they will help you next time.');
        });
    }

    public function sendReviewPaidSystemEmail($property, $user_detail) {
        $data = array('property' => $property, 'user_detail' => $user_detail);
        \Mail::send('emails.sendReviewPaidEmail', $data, function($message) use($user_detail) {
            $message->from('notification@work-related-learning.com', 'Work-Related-Learning');
            $message->to($user_detail->email, $user_detail->name)
                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
                    ->subject('Hi, how was your experience? Share your opinion and help other Student Guests pick the best places, they will help you next time.');
        });
    }

//    public function testMail() {
//        /* $mailData = ['Email' => 'test'];
//          $this->mail->send('rosesuresh10mca@gmail.com', 'suresh', 'Welcome mail', 'mail.welcome', $mailData, $this->adminMail, $this->adminName); */
//
//        $data = array(
//            'name' => "Learning Laravel",
//            'email' => "Learning Laravel",
//            'phone' => "Learning Laravel",
//        );
//
//        \Mail::send('mails.welcomeemail', $data, function ($message) {
//            $message->from('holidaysiles@gmail.com', 'Learning Laravel');
//            $message->to('rosesuresh10mca@gmail.com')
//                    ->cc("madrelinguastaff@gmail.com", 'work-related-learning')
//                    ->subject('Learning Laravel test email');
//        });

    /* Mail::send('emails.contact', $data, function ($message) use ($data) {
      $message->subject('Blog Contact Form: '.$data['name'])
      ->to(config('blog.contact_email'))
      ->replyTo($data['email']);
      }); */


    // $this->mail->sendTestMail('rosesuresh10mca@gmail.com', "gdfgdf");
//    }

    /* public function temporaryRegistrationMailForTeacher(User $user)
      {
      $subject = $this->lan->get('emails.teacher.temporary_regsitration.subject');
      $viewFile = $this->lan->get('emails.teacher.temporary_regsitration.template');
      $this->mail->send($user->email, $user->first_name, $subject, $viewFile, $user, $this->adminMail, $this->adminName);
      }

      public function welcomeRegistrationMailForStudent(User $user)
      {
      $subject = $this->lan->get('emails.student.welcome_email.subject');
      $viewFile = $this->lan->get('emails.student.welcome_email.template');
      $this->mail->send($user->email, $user->first_name, $subject, $viewFile, $user, $this->adminMail, $this->adminName);
      }
      public function forgetPasswordMailForUser(User $user, $password)
      {
      $subject = $this->lan->get('emails.user.forgotpassword.subject');
      $viewFile = $this->lan->get('emails.user.forgotpassword.template');
      $mailData = ['Email' => $user->email, 'Password' => $password];
      $this->mail->send($user->email, $user->first_name, $subject, $viewFile, $mailData, $this->adminMail, $this->adminName);
      }

      public function instituteAddedMailForInstituteAdd($email, $password)
      {
      $subject = $this->lan->get('emails.institute.institute_admin.subject');
      $viewFile = $this->lan->get('mails.add_institute');
      $mailData = ['User Name' => $email, 'Password' => $password];
      $this->mail->send($email, null, $subject, $viewFile, $mailData, $this->adminMail, $this->adminName);
      }

      public function welcomeRegistrationMailForTeacher($email, $password)
      {
      $subject = $this->lan->get('emails.institute.teacher.subject');
      $viewFile = $this->lan->get('emails.institute.teacher.template');
      $mailData = ['User Name' => $email, 'Password' => $password];
      $this->mail->send($email, null, $subject, $viewFile, $mailData, $this->adminMail, $this->adminName);
      }

      public function notificationDebugMail(User $user, Notification $notification)
      {
      $subject = 'Notification';
      $viewFile = $this->lan->get('emails.notification.template');
      $mailData = ['user' => $user, 'notification' => $notification];
      $this->mail->send($user->email, null, $subject, $viewFile, $mailData, $this->adminMail, $this->adminName);
      } */
}
