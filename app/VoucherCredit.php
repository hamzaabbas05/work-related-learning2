<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherCredit extends Model {

    public function user() {
        return $this->hasOne(User::class, "vouchercredit_id")->select("id", "vouchercredit_id", "name", "email", "lastname");
    }
    
    public function userCredit(){
        return $this->belongsTo(UserCredit::class);
    }
    
    public function userWallet(){
        return $this->belongsTo(UserWallet::class);
    }

}
