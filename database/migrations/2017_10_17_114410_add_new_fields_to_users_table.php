<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('tutor_name', 255)->nullable();
            $table->string('tutor_pob', 255)->nullable();
            $table->string('tutor_surname', 255)->nullable();
            $table->string('tutor_pan', 255)->nullable();
            $table->string('tutor_email', 255)->nullable();
            $table->string('tutor_phone', 255)->nullable();
            $table->string('tutor_emergency_phone', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('tutor_name');
            $table->dropColumn('tutor_pob');
            $table->dropColumn('tutor_surname');
            $table->dropColumn('tutor_pan');
            $table->dropColumn('tutor_email');
            $table->dropColumn('tutor_phone');
            $table->dropColumn('tutor_emergency_phone');
        });
    }
}
