<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLegalRepresentativeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_legal_representatives', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->smallInteger('is_legal_representative')->nullable()->default('0');
            $table->string('legal_representative_name', 255)->nullable();
            $table->string('legal_representative_surname', 255)->nullable();
            $table->string('legal_representative_dob', 255)->nullable();
            $table->text('legal_representative_birth_place')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_legal_representatives');
    }
}
