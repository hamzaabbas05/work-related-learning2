<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transfer_type', 255)->nullable();
            $table->smallInteger('is_active')->default("1")->nullable();
            $table->smallInteger('is_delete')->default("0")->nullable();
            $table->timestamps();
        });
        DB::table('transfer_types')->insert([
            ['transfer_type' => 'Airport Pick up and Drop off'],
            ['transfer_type' => 'Airport Pick up'],
            ['transfer_type' => 'Airport Drop off'],
            ['transfer_type' => 'Town Centre/Main public transport spot Pick up ad Drop off'],
            ['transfer_type' => 'Town Centre/Main public transport spot Pick up'],
            ['transfer_type' => 'Town Centre/Main public transport spot Drop off'],
            ['transfer_type' => 'Daily Pick up and drop off to ad from work/school/lessons/activity'],
            ['transfer_type' => 'Daily Pick up from work/school/lessons/activity'],
            ['transfer_type' => 'Daily Drop off to work/school/lessons/activity'],
            ['transfer_type' => 'Other'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transfer_types');
    }
}
