<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVerificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_verifications', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('personal_id_kind', 255)->nullable();
            $table->string('personal_id_expiry_date', 255)->nullable();
            $table->string('personal_id_picture_front', 255)->nullable();
            $table->string('personal_id_picture_back', 255)->nullable();
            $table->smallInteger('criminal_check_done')->nullable()->default('0');
            $table->string('criminal_check_last_done_year', 255)->nullable();
            $table->string('criminal_check_image', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_verifications');
    }
}
