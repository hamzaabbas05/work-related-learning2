<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_title', 50)->nullable();
            $table->text('site_slogan')->nullable();
            $table->string('superadmin_email', 50)->nullable();
            $table->string('contact_email', 50)->nullable();
            $table->string('support_email', 50)->nullable();
            $table->string('noreply_email', 50)->nullable();
            $table->string('contact_phone', 15)->nullable();
            $table->string('contact_phone_optional', 15)->nullable();
            $table->string('contact_skype', 50)->nullable();
            $table->string('open_timings', 50)->nullable();
            $table->text('contact_address')->nullable();
            $table->text('facebook_url')->nullable();
            $table->text('googleplus_url')->nullable();
            $table->text('twitter_url')->nullable();
            $table->string('fb_app_id', 100)->nullable();
            $table->string('fb_secret_key', 100)->nullable();
            $table->string('gplus_app_id', 100)->nullable();
            $table->string('gplus_secret_key', 100)->nullable();
            $table->string('logo', 100)->nullable();
            $table->string('favicon', 100)->nullable()->nullable();
            $table->string('no_image_profile', 100)->nullable();
            $table->string('no_image_property', 100)->nullable();
            $table->string('site_meta_title', 50)->nullable();
            $table->text('site_meta_keywords')->nullable();
            $table->text('site_meta_description')->nullable();
            $table->integer('currency_id')->unsigned()->nullable();
            $table->integer('language_id')->unsigned()->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
