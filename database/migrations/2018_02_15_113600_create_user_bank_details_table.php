<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBankDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bank_details', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('bank_name', 255)->nullable();
            $table->string('iban', 255)->nullable();
            $table->string('swift_code', 255)->nullable();
            $table->string('receiverType', 255)->nullable();
            $table->string('amountCurrency', 255)->nullable();
            $table->string('sourceCurrency', 255)->nullable();
            $table->string('targetCurrency', 255)->nullable();
            $table->string('sortCode', 255)->nullable();
            $table->string('accountNumber', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_bank_details');
    }
}
