<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBedTypeTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('bed_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bed_type', 255)->nullable();
            $table->smallInteger('is_active')->default("1")->nullable();
            $table->smallInteger('is_delete')->default("0")->nullable();
            $table->timestamps();
        });
        DB::table('bed_types')->insert([
            ['bed_type' => 'Single'],
            ['bed_type' => 'Double'],
            ['bed_type' => 'King size'],
            ['bed_type' => 'Queen size'],
            ['bed_type' => 'Bunk bed'],
            ['bed_type' => 'Futon'],
            ['bed_type' => 'Other'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('bed_types');
    }

}
