<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_base_attributes', function ($table) {
            $table->renameColumn('name', 'bed_no');
            $table->renameColumn('quantity', 'bed_room_no');
            $table->integer('bath_no');
            $table->integer('accomodates_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
