<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityTypeTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('activity_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('activity_type', 255)->nullable();
            $table->smallInteger('is_active')->default("1")->nullable();
            $table->smallInteger('is_delete')->default("0")->nullable();
            $table->timestamps();
        });
        DB::table('activity_types')->insert([
            ['activity_type' => 'Homely: D.I.Y. or gardening'],
            ['activity_type' => 'Homely: Doing the shopping'],
            ['activity_type' => 'Homely: Electronic games'],
            ['activity_type' => 'Homely: Films'],
            ['activity_type' => 'Homely: Going shopping'],
            ['activity_type' => 'Homely: Music (listening)'],
            ['activity_type' => 'Homely: Music (playing an instrument)'],
            ['activity_type' => 'Homely: Music (singing)'],
            ['activity_type' => 'Homely: Reading together'],
            ['activity_type' => 'Homely: Other'],
            ['activity_type' => 'Cultural: Arts and craft'],
            ['activity_type' => 'Cultural: Cinema'],
            ['activity_type' => 'Cultural: Cultural outings (not Tour)'],
            ['activity_type' => 'Cultural: Drama'],
            ['activity_type' => 'Cultural: Interesting'],
            ['activity_type' => 'Cultural: Nature outings (not Tour)'],
            ['activity_type' => 'Cultural: Photography'],
            ['activity_type' => 'Cultural: Other'],
            ['activity_type' => 'Sport: Sports centre weekly pass'],
            ['activity_type' => 'Sport: badminton'],
            ['activity_type' => 'Sport: basketball'],
            ['activity_type' => 'Sport: combat or martial sports'],
            ['activity_type' => 'Sport: cycling'],
            ['activity_type' => 'Sport: dance'],
            ['activity_type' => 'Sport: fishing'],
            ['activity_type' => 'Sport: fitness plan'],
            ['activity_type' => 'Sport: golf'],
            ['activity_type' => 'Sport: handball '],
            ['activity_type' => 'Sport: horse Riding'],
            ['activity_type' => 'Sport: rugby'],
            ['activity_type' => 'Sport: soccer'],
            ['activity_type' => 'Sport: swimming'],
            ['activity_type' => 'Sport: volleyball'],
            ['activity_type' => 'Sport: trekking'],
            ['activity_type' => 'Sport: tennis '],
            ['activity_type' => 'Sport: running'],
            ['activity_type' => 'Sport: body building'],
            ['activity_type' => 'Sport: water sports'],
            ['activity_type' => 'Sport: winter sports'],
            ['activity_type' => 'Sport: Other Sport'],
            ['activity_type' => 'Other'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('activity_types');
    }

}
