<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeDestinationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homedestinations', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('display_name');
            $table->decimal('lattitude', 9, 6);
            $table->decimal('longitude', 9, 6);
            $table->string('img_name');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
