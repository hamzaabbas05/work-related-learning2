<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWowTypesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('wow_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wow_type', 255)->nullable();
            $table->smallInteger('is_active')->default("1")->nullable();
            $table->smallInteger('is_delete')->default("0")->nullable();
            $table->timestamps();
        });
        DB::table('wow_types')->insert([
            ['wow_type' => 'Academical'],
            ['wow_type' => 'Event'],
            ['wow_type' => 'Concert'],
            ['wow_type' => 'Extreme Sport or Activity'],
            ['wow_type' => 'Folkloristic'],
            ['wow_type' => 'Really Local'],
            ['wow_type' => 'Car boot sale'],
            ['wow_type' => 'National relevance'],
            ['wow_type' => 'Magic'],
            ['wow_type' => 'Unique'],
            ['wow_type' => 'Party'],
            ['wow_type' => 'Religious'],
            ['wow_type' => 'Other'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('wow_types');
    }

}
