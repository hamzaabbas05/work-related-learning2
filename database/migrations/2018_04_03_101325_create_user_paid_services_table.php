<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaidServicesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('user_paid_services', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('house_name', 255)->nullable();
            $table->string('house_slogan', 255)->nullable();
            $table->text('house_desc')->nullable();
            $table->string('houseaddress', 255)->nullable();
            $table->string('houselatbox', 255)->nullable();
            $table->string('houselonbox', 255)->nullable();
            $table->string('street_numberh', 255)->nullable();
            $table->string('routeh', 255)->nullable();
            $table->string('localityh', 255)->nullable();
            $table->string('administrative_area_level_1h', 255)->nullable();
            $table->string('postal_codeh', 255)->nullable();
            $table->string('countryh', 255)->nullable();
            $table->smallInteger('house_property_differ')->nullable()->default('0');
            $table->string('propertyaddress', 255)->nullable();
            $table->string('street_numberp', 255)->nullable();
            $table->string('routep', 255)->nullable();
            $table->string('localityp', 255)->nullable();
            $table->string('administrative_area_level_1p', 255)->nullable();
            $table->string('postal_codep', 255)->nullable();
            $table->string('countryp', 255)->nullable();            
            $table->string('proplatbox', 255)->nullable();            
            $table->string('proplonbox', 255)->nullable();            
            $table->string('host_preference', 255)->nullable();            
            $table->text('service_included')->nullable();            
            $table->text('service_desc')->nullable();            
            $table->text('airport_way')->nullable();            
            $table->text('home_way')->nullable();            
            $table->text('mobile_use')->nullable();            
            $table->string('urban_location', 255)->nullable();            
            $table->string('property_region', 255)->nullable();            
            $table->string('local_amenity', 255)->nullable();            
            $table->text('amenity_desc')->nullable();            
            $table->string('facilities', 255)->nullable();           
            $table->text('facility_desc')->nullable();
            $table->smallInteger('smoking')->nullable()->default('0');
            $table->text('time_limits')->nullable();
            $table->text('house_rules')->nullable();
            $table->smallInteger('key_given')->nullable()->default('0');
            $table->string('guest_table', 255)->nullable();
            $table->text('other_house_rules')->nullable();
            $table->string('family_name', 255)->nullable();
            $table->smallInteger('is_language')->nullable()->default('0');
            $table->text('member_desc')->nullable();
            $table->string('family_situation', 255)->nullable();
            $table->string('host_experience', 255)->nullable();
            $table->string('home_type', 255)->nullable();
            $table->text('property_slogan')->nullable();
            $table->text('property_desc')->nullable();
            $table->string('room_no', 255)->nullable();
            $table->string('guest_room_no', 255)->nullable();
            $table->string('toilet_no', 255)->nullable();
            $table->string('shower_no', 255)->nullable();
            $table->string('bath_no', 255)->nullable();
            $table->smallInteger('is_active')->nullable()->default('1');
            $table->smallInteger('is_delete')->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('user_paid_services');
    }

}
