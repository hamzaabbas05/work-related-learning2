<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomFacilitiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('room_facilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('facility_type', 255)->nullable();
            $table->smallInteger('is_active')->default('1')->nullable();
            $table->smallInteger('is_delete')->default('0')->nullable();
            $table->timestamps();
        });
        DB::table('room_facilities')->insert([
            ['facility_type' => 'TV'],
            ['facility_type' => 'Aircoditioning'],
            ['facility_type' => 'View'],
            ['facility_type' => 'Safe'],
            ['facility_type' => 'Fridge/Minibar'],
            ['facility_type' => 'Wardrobe space'],
            ['facility_type' => 'Desk'],
            ['facility_type' => 'Indipendent entrance'],
            ['facility_type' => 'Other'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('room_facilities');
    }

}
