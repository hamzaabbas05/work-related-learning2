<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amenities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });

        Schema::create('amenity_property', function (Blueprint $table) {
            $table->integer('property_id')->unsigned();
            $table->integer('amenity_id')->unsigned();

            $table->foreign('property_id')
            ->references('id')
            ->on('property')
            ->onDelete('cascade');

            $table->foreign('amenity_id')
            ->references('id')
            ->on('amenities')
            ->onDelete('cascade');

            $table->primary(['property_id', 'amenity_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
