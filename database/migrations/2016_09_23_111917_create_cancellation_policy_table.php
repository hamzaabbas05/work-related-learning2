<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCancellationPolicyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancellationpolicy', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->nullable();
            $table->integer('currency_id')->unsigned();
            $table->integer('cancellation_type')->default(1);
            $table->string('cancellation_charge', 10);
            $table->text('desc');
            $table->integer('status')->default(1);
            $table->timestamps();

            $table->foreign('currency_id')
                  ->references('id')
                  ->on('currency')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
