<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTypeTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tour_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tour_type', 255)->nullable();
            $table->smallInteger('is_active')->default("1")->nullable();
            $table->smallInteger('is_delete')->default("0")->nullable();
            $table->timestamps();
        });
        DB::table('tour_types')->insert([
            ['tour_type' => 'Tourist Attraction'],
            ['tour_type' => 'City centre visit'],
            ['tour_type' => 'Other City visit'],
            ['tour_type' => 'Shopping'],
            ['tour_type' => 'Market place'],
            ['tour_type' => 'Castle'],
            ['tour_type' => 'Museum'],
            ['tour_type' => 'Night out'],
            ['tour_type' => 'Day out'],
            ['tour_type' => 'Eating Out / Restaurant'],
            ['tour_type' => 'Historical village'],
            ['tour_type' => 'Amusement Park'],
            ['tour_type' => 'National Park'],
            ['tour_type' => 'Seaside day out'],
            ['tour_type' => 'Local Tourist Attraction'],
            ['tour_type' => 'Other'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tour_types');
    }

}
