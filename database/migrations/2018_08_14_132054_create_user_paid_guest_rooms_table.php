<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaidGuestRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_paid_guest_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('room_name', 255)->nullable();
            $table->string('room_type_id', 255)->nullable();
            $table->text('room_description')->nullable();
            $table->string('room_facility', 255)->nullable();
            $table->text('room_guest_toilet')->nullable();
            $table->text('room_guest_shower')->nullable();
            $table->smallInteger('is_active')->default('1')->nullable();
            $table->smallInteger('is_delete')->default('0')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_paid_guest_rooms');
    }
}
