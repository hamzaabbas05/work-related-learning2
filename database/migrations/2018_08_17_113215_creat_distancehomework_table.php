<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatDistancehomeworkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distancehomework', function (Blueprint $table) {
            $table->increments('id');
            $table->text('paidAddress')->nullable();
            $table->bigInteger('paidPropId')->nullable();
            $table->text('unpaidAddress')->nullable();
            $table->bigInteger('unpaidPropId')->nullable();
            $table->string('time', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('distancehomework');
    }
}
