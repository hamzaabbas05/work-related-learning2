<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secondary_school_groups_flash', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group_name', 255)->nullable();
            $table->string('group_date_in', 255)->nullable();
            $table->string('group_date_out', 255)->nullable();
            $table->string('pick_up_where', 255)->nullable();
            $table->string('drop_off_where', 255)->nullable();
            $table->string('pick_up_time', 255)->nullable();
            $table->string('drop_off_time', 255)->nullable();
            $table->string('link_to_route_to_meeting_point', 255)->nullable();
            $table->string('link_to_route_from_meeting_point', 255)->nullable();
            $table->string('catching_the_what_bus_to', 255)->nullable();
            $table->string('catching_the_what_time_bus_to', 255)->nullable();
            $table->string('catching_the_what_bus_back', 255)->nullable();
            $table->string('catching_the_what_time_bus_back', 255)->nullable();
            $table->string('flight_to', 255)->nullable();
            $table->string('flight_back', 255)->nullable();
            $table->smallInteger('is_active')->default("1")->nullable();
            $table->smallInteger('is_delete')->default("0")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('secondary_school_groups_flash');
    }
}
