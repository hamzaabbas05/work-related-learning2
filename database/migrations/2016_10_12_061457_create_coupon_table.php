<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('offer_type');
            $table->tinyInteger('discount_type');
            $table->date('valid_range_from');
            $table->date('valid_range_to');
            $table->decimal('offer_amt', 5, 2);
            $table->Integer('max_count');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
