<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExtrainfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_extrainfo', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('second_citizenship')->nullable();
            $table->bigInteger('native_language_speaker')->nullable();
            $table->bigInteger('second_language_speaker')->nullable();
            $table->bigInteger('third_language_speaker')->nullable();
            $table->text('description_more_languages')->nullable();
            $table->string('member_assisting_you_name', 255)->nullable();
            $table->string('member_assisting_you_surname', 255)->nullable();
            $table->string('member_assisting_you_email', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_extrainfo');
    }
}
