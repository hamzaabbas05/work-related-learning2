<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('class_type', 255)->nullable();
            $table->smallInteger('is_active')->default("1")->nullable();
            $table->smallInteger('is_delete')->default("0")->nullable();
            $table->timestamps();
        });
        DB::table('class_types')->insert([
            ['class_type' => 'Individual lessons'],
            ['class_type' => 'Plan for few (sharing)'],
            ['class_type' => 'Small Group up to 4 people'],
            ['class_type' => 'Small Group up to 8 peaople'],
            ['class_type' => 'Traditional Group up to 15 people'],
            ['class_type' => 'Other'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_types');
    }
}
