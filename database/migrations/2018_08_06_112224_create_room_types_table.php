<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTypesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('room_type_really_room_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('room_type', 255)->nullable();
            $table->smallInteger('is_active')->default('1')->nullable();
            $table->smallInteger('is_delete')->default('0')->nullable();
            $table->timestamps();
        });

        DB::table('room_type_really_room_types')->insert([
            ['room_type' => 'Single'],
            ['room_type' => 'Single with big bed'],
            ['room_type' => 'Double'],
            ['room_type' => 'Twin'],
            ['room_type' => 'Twin with bunk beds'],
            ['room_type' => 'Twin with two big beds'],
            ['room_type' => 'Triple'],
            ['room_type' => 'Quadruple'],
            ['room_type' => 'More than 4 beds'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('room_type_really_room_types');
    }

}
