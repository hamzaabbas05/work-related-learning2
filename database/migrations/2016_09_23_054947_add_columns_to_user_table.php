<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('lastname', 50)->nullable();
            $table->string('profile_img_name', 50)->nullable();
            $table->string('profile_img_src', 50)->nullable();
            $table->string('phonenumber', 15)->nullable();
            $table->string('dob', 15)->nullable();
            $table->tinyInteger('gender');
            $table->string('paypalid', 100)->nullable();
            $table->text('about_yourself')->nullable();
            $table->text('school_desc')->nullable();
            $table->text('work_desc')->nullable();
            $table->text('emergency_contact_name')->nullable();
            $table->text('emergency_contact_phone')->nullable();
            $table->text('emergency_contact_email')->nullable();
            $table->text('emergency_contact_relationship')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->string('city', 100)->nullable();
            $table->string('state', 100)->nullable();
            $table->string('zipcode', 15)->nullable();
            $table->text('address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
