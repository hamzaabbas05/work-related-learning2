<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = array(
			array('language_name' => 'English', 'language_code' => 'EN'),
			array('language_name' => 'French', 'language_code' => 'FR'),
			array('language_name' => 'Spanish', 'language_code' => 'ES'),
			array('language_name' => 'Italian', 'language_code' => 'IT'),
			array('language_name' => 'Greek', 'language_code' => 'EL')
			);
        DB::table('language')->insert($languages);
    }
}
