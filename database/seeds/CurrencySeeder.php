<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = array(
			array('currency_name' => 'US Dollar', 'currency_code'=>"USD"),
			array('currency_name' => 'Euro', 'currency_code'=>"EUR"),
			array('currency_name' => 'Australian Dollar', 'currency_code'=>"AUD"),
			array('currency_name' => 'Malaysian Ringgit', 'currency_code'=>"MYR"),
			);
        DB::table('currency')->insert($currencies);
    }
}
