<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'site_title' => 'Holidaysiles',
            'site_slogan' => 'Enjoy your Holidays Here mad Enjoy Fun',
            'superadmin_email' => 'laurent@holidaysiles.com',
            'contact_email' => 'contact@holidaysiles.com',
            'support_email' => 'support@holidaysiles.com',
            'noreply_email' => 'noreply@holidasiles.com',
            'contact_phone' => '+91 1234567890',
            'contact_phone_optional' => '+91 1234567890',
            'contact_skype' => 'holidaysiles.skype',
            'open_timings' => '24*7',
            'contact_address' => 'Holidaysiles,France',
            'facebook_url' => 'https://www.facebook.com',
            'googleplus_url' => 'https://plus.google.com/',
            'twitter_url' => 'https://twitter.com/',
            'fb_app_id' => '1096178887103465',
            'fb_secret_key' => '26bfe117e7acfb4576ed9e3ed070d164',
            'gplus_app_id' => '743235069707-0jd1qi0g7imtjo39713e22e9039pmlfm.apps.googleusercontent.com',
            'gplus_secret_key' => 'GFQ8mEcgW7C6I7K1mr_95EnU',
            'logo' => 'logo.png',
            'favicon' => 'favicon.png',
			'no_image_profile' => 'noimg_profile.png',
            'no_image_property' => 'noimg_property.png',
            'site_meta_title' => 'Holidaysile',
            'site_meta_keywords' => 'Vacation Rentals, Homes, Apartments &amp; Rooms for Rent - Airbnb',
            'site_meta_description' => 'Rent unique accommodations from local hosts in 191+ countries. Feel at home anywhere you go in the world with Airbnb',
            'currency_id' => 1,
            'language_id' => 1,
		]);
    }
}
