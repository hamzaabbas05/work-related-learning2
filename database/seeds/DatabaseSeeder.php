<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/*$this->call(CountryTableSeeder::class);
    	$this->call(LanguageSeeder::class);
    	$this->call(CurrencySeeder::class);
        $this->call(SettingsTableSeeder::class);*/
        $this->call(CodeTableSeeder::class);
    }
}
