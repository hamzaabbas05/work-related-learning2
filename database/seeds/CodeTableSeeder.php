<?php
use Illuminate\Database\Seeder;

class CodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $codeValues = array(
              array(
                  'CodeType' => 'bannertype',
                  'Code'      => '1',
                  'Desc'  => 'Home Single Banner',
              ),
              array(
                  'CodeType' => 'bannertype',
                  'Code'      => '2',
                  'Desc'  => 'Home Slider',
              ),
              array(
                  'CodeType' => 'amenities',
                  'Code'      => '1',
                  'Desc'  => 'Common Amenities',
                   
              ),
              array(
                  'CodeType' => 'amenities',
                  'Code'      => '1',
                  'Desc'  => 'Additional Amenities',
              ),
               array(
                  'CodeType' => 'attributes',
                  'Code'      => '1',
                  'Desc'  => 'Safety Checklists',
              ),
              array(
                  'CodeType' => 'attributes',
                  'Code'      => '2',
                  'Desc'  => 'Special Features',
              ),
              array(
                  'CodeType' => 'cancellationtype',
                  'Code'      => '1',
                  'Desc'  => 'Fixed',
              ),
              array(
                  'CodeType' => 'cancellationtype',
                  'Code'      => '2',
                  'Desc'  => 'Percentage',
              ),
              );
        DB::table('Code')->insert( $codeValues);
    }
}
