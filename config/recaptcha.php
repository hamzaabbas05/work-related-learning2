<?php
// if($_SERVER['REMOTE_ADDR'] == '5.90.66.233'){
//     echo "<pre>";
//         print_r($_REQUEST);
//         print_r($_SERVER);
//     echo "</pre>";
// }



$reCAPTCHA = [

    /*
    |--------------------------------------------------------------------------
    | API Keys
    |--------------------------------------------------------------------------
    |
    | Set the public and private API keys as provided by reCAPTCHA.
    |
    | In version 2 of reCAPTCHA, public_key is the Site key,
    | and private_key is the Secret key.
    |
    */
    'public_key'     => env('RECAPTCHA_PUBLIC_KEY', ''),
    'private_key'    => env('RECAPTCHA_PRIVATE_KEY', ''),

    /*
    |--------------------------------------------------------------------------
    | Template
    |--------------------------------------------------------------------------
    |
    | Set a template to use if you don't want to use the standard one.
    |
    */
    'template'    => '',

    /*
    |--------------------------------------------------------------------------
    | Driver
    |--------------------------------------------------------------------------
    |
    | Determine how to call out to get response; values are 'curl' or 'native'.
    | Only applies to v2.
    |
    */
    'driver'      => 'curl',

    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    | Various options for the driver
    |
    */
    'options'     => [

        'curl_timeout' => 1,
        'curl_verify' => true,

    ],

    /*
    |--------------------------------------------------------------------------
    | Version
    |--------------------------------------------------------------------------
    |
    | Set which version of ReCaptcha to use.
    |
    */

    'version'     => 2,

];
// if($_SERVER['REMOTE_ADDR'] == '5.90.66.233' || $_SERVER['REMOTE_ADDR'] == '120.72.90.142'){
    // if($_SERVER['HTTP_HOST'] === 'learn-in-a-flash.com'){
    //     $reCAPTCHA['public_key'] = env('RECAPTCHA_LEARN_FLASH_PUBLIC_KEY', '');
    //     $reCAPTCHA['private_key'] = env('RECAPTCHA_LEARN_FLASH_PRIVATE_KEY', '');
    // }
    // echo "<pre>";
    //     print_r($reCAPTCHA);
    //     print_r($_REQUEST);
    //     print_r($_SERVER);
    // echo "</pre>";
// }

return $reCAPTCHA;