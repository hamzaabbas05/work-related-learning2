var checkinDate = "", checkoutDate = "";
var guests = "", roomTypes = "", priceRange = "260/800";
var bedroom = "", bathroom = "", beds = "";
var amenities = "", searchInitial = 0, homeTypes = "", roomTypes = "";
var allemail = [];
$(document).ready(function () {


    $("#basicsubmit").submit(function () {

        var check = 0;
        var exptitle = $("#exptitle").val();

        if ($('#checktutorid').prop("checked") == true) {

            var rname = $("#rname").val();
            var rsurname1 = $("#rsurname1").val();
            var rtax1 = $("#rtax1").val();
            var remail1 = $("#remail1").val();
            var rtel1 = $("#rtel1").val();
            var work_tutor_fit = $("#work_tutor_fit").val();
            var rborn1 = $("#rborn1").val();
            tutorrelation = $("#tutorrelation option:selected").val();
            workcategory = $("#workcategory option:selected").val();

            if ($.trim(exptitle) == '') {
                $(".exptitle").show();
                $(".exptitle").html("Work Experience: Title of the Listing is required");
                $("#exptitle").keydown(function () {
                    $(".exptitle").hide();
                    $(".exptitle").html("");
                });
                check = 1;
            }

            if ($('#workcategory option:selected').val() == 'undefined')
            {
                $(".workcategory").show();
                $(".workcategory").html("Please select Work Category");
                check = 1;

            } else {
                $(".workcategory").hide();
                $(".workcategory").html("");
            }

            if ($('#tutorrelation option:selected').val() == 'undefined')
            {
                $(".tutorrelationerr").show();
                $(".tutorrelationerr").html("Please select Tutor Relation");
                check = 1;

            } else {
                $(".tutorrelationerr").hide();
                $(".tutorrelationerr").html("");
            }
            if ($.trim(rname) == '') {
                $(".rnameerr").show();
                $(".rnameerr").html("Work Tutor Name is required");
                $("#rname").keydown(function () {
                    $(".rnameerr").hide();
                    $(".rnameerr").html("");
                });
                check = 1;
            }
            if ($.trim(rsurname1) == '') {
                $(".rsurname1").show();
                $(".rsurname1").html("Work Tutor SurName is required");
                $("#rsurname1").keydown(function () {
                    $(".rsurname1").hide();
                    $(".rsurname1").html("");
                });
                check = 1;
            }
            if ($.trim(rtax1) == '') {
                $(".rtax1").show();
                $(".rtax1").html("Work Tutor's Tax Identification Number is required");
                $("#rtax1").keydown(function () {
                    $(".rtax1").hide();
                    $(".rtax1").html("");
                });
                check = 1;
            }
            if ($.trim(remail1) == '') {
                $(".remail1").show();
                $(".remail1").html("Work Tutor's Email is required");
                $("#remail1").keydown(function () {
                    $(".remail1").hide();
                    $(".remail1").html("");
                });
                check = 1;
            }
            if (!(isValidEmailAddress(remail1))) {
                $(".remail1").show();
                $(".remail1").html("Work Tutor's Email is not Valid Email");
                $("#remail1").keydown(function () {
                    $(".remail1").hide();
                    $(".remail1").html("");
                });
                check = 1;

            }
            if ($.trim(rtel1) == '') {
                $(".rtel1").show();
                $(".rtel1").html("Work Tutor's Tel");
                $("#rtel1").keydown(function () {
                    $(".rtel1").hide();
                    $(".rtel1").html("");
                });

                check = 1;
            }
            if ($.trim(rborn1) == '') {
                $(".rborn1").show();
                $(".rborn1").html("Work Tutor Name is required");
                $("#rborn1").keydown(function () {
                    $(".rborn1").hide();
                    $(".rborn1").html("");
                });
                check = 1;
            }
            if ($.trim(work_tutor_fit) == '') {
                $(".work_tutor_fiterr").show();
                $(".work_tutor_fiterr").html("Why are you fit for being a Work Tutor?  is required");
                $("#work_tutor_fit").keydown(function () {
                    $(".work_tutor_fiterr").hide();
                    $(".work_tutor_fiterr").html("");
                });
                check = 1;
            }

            if (check == 1) {
                return false;
            }
            return true;

        } else if ($('#checktutorid').prop("checked") == false) {
            if ($.trim(exptitle) == '') {
                $(".exptitle").show();
                $(".exptitle").html("Work Experience: Title of the Listing is required");
                $("#exptitle").keydown(function () {
                    $(".exptitle").hide();
                    $(".exptitle").html("");
                });
                check = 1;
            }

            if ($('#workcategory option:selected').val() == 'undefined')
            {
                $(".workcategory").show();
                $(".workcategory").html("Please select Work Category");
                check = 1;

            } else {
                $(".workcategory").hide();
                $(".workcategory").html("");
            }
            if (check == 1) {
                return false;
            }
            return true;

        }


    });

    $("#form-signup").on('submit', function (e) {
        var cnt = 0;
        var check = 0;
        email = $("#email").val();
        firstname = $("#firstname").val();
        lastname = $("#lastname").val();
        password = $("#password").val();
        birthmonth = $("#bmonth").val();
        birthday = $("#bday").val();
        birthyear = $("#byear").val();
        if (firstname == "")
        {
            $("#signuploadimg").hide();
            $(".field-signupform-firstname").addClass("has-error");
            $("#firstname").next(".help-block-error").html("First name cannot be blank");
            check = 1;
            $("#firstname").keydown(function () {
                $(".field-signupform-firstname").removeClass("has-error");
                $("#firstname").next(".help-block-error").html("");
            });
        }
        if (lastname == "")
        {
            $("#signuploadimg").hide();
            $(".field-signupform-lastname").addClass("has-error");
            $("#lastname").next(".help-block-error").html("Last name cannot be blank");
            check = 1;
            $("#lastname").keydown(function () {
                $(".field-signupform-lastname").removeClass("has-error");
                $("#lastname").next(".help-block-error").html("");
            });
        }
        if (email == "")
        {
            $("#signuploadimg").hide();
            $(".field-signupform-email").addClass("has-error");
            $("#email").next(".help-block-error").html("Email cannot be blank");
            check = 1;
            $("#email").keydown(function () {
                $(".field-signupform-email").removeClass("has-error");
                $("#email").next(".help-block-error").html("");
            });
        }
        if (password == "")
        {
            $("#signuploadimg").hide();
            $(".field-signupform-password").addClass("has-error");
            $("#password").next(".help-block-error").html("Password cannot be blank");
            check = 1;
            $("#password").keydown(function () {
                $(".field-signupform-password").removeClass("has-error");
                $("#password").next(".help-block-error").html("");
            });
        }
        if (firstname.length < 3)
        {
            $("#signuploadimg").hide();
            $(".field-signupform-firstname").addClass("has-error");
            $("#firstname").next(".help-block-error").html("First name should have minimum 3 characters");
            check = 1;
            $("#firstname").keydown(function () {
                $(".field-signupform-firstname").removeClass("has-error");
                $("#firstname").next(".help-block-error").html("");
            });
        }
        if (lastname.length < 3)
        {
            $("#signuploadimg").hide();
            $(".field-signupform-lastname").addClass("has-error");
            $("#lastname").next(".help-block-error").html("Last name should have minimum 3 characters");
            check = 1;
            $("#lastname").keydown(function () {
                $(".field-signupform-lastname").removeClass("has-error");
                $("#lastname").next(".help-block-error").html("");
            });
        }
        if (!(isValidEmailAddress(email))) {
            $("#signuploadimg").hide();
            $(".field-signupform-email").addClass("has-error");
            $("#email").next(".help-block-error").html("Enter a valid email");
            check = 1;
            $("#email").keydown(function () {
                $(".field-signupform-email").removeClass("has-error");
                $("#email").next(".help-block-error").html("");
            });
        }
        if (password.length < 6)
        {
            $("#signuploadimg").hide();
            $(".field-signupform-password").addClass("has-error");
            $("#password").next(".help-block-error").html("Password should have minimum 6 characters");
            check = 1;
            $("#password").keydown(function () {
                $(".field-signupform-password").removeClass("has-error");
                $("#password").next(".help-block-error").html("");
            });
        }
        if (birthday == 0 || birthmonth == 0 || birthyear == 0) {
            $("#signuploadimg").hide();
            $("#bdayerr").html("Please select birthday date");
            check = 1;

        }

        if ($('#agree').prop("checked") == false) {
            $("#signuploadimg").hide();
            $("#agreeerr").html("Please check Agree to Terms and conditions");
            check = 1;
        }
        //alert(baseurl + '/validatedata');
        if (check == 1)
            return false;
        $.ajax({
            type: 'GET',
            url: baseurl + '/validatedata',
            async: false,
            beforeSend: function () {
                $("#signuploadimg").show();
            },
            data: {
                email: email,
            },
            success: function (data) {
                //alert(data);
                $("#signuploadimg").hide();
                if ($.trim(data) == 'exists') {
                    $(".field-signupform-email").addClass("has-error");
                    $("#email").next(".help-block-error").html("Email already exists");
                    cnt = 1;
                } else if ($.trim(data) == 'success') {
                    cnt = 0;
                }
            }
        });
        if (cnt == 1) {
            return false;
        } else
            return true;
    });

    $("#login-form").on('submit', function () {
        var cnt = 0;
        var check = 0;
        email = $("#login-email").val();
        password = $("#login-password").val();
        if (email == "")
        {
            $("#loginloadimg").hide();
            $(".field-signupform-email").addClass("has-error");
            $("#login-email").next(".help-block-error").html("Email cannot be blank.");
            check = 1;
            $("#login-email").keydown(function () {
                $(".field-signupform-email").removeClass("has-error");
                $("#login-email").next(".help-block-error").html("");
            });
        }
        if (!(isValidEmailAddress(email))) {
            $("#loginloadimg").hide();
            $(".field-signupform-email").addClass("has-error");
            $("#login-email").next(".help-block-error").html("Enter a valid email");
            check = 1;
            $("#login-email").keydown(function () {
                $(".field-signupform-email").removeClass("has-error");
                $("#login-email").next(".help-block-error").html("");
            });
        }
        if (password == "")
        {
            $("#loginloadimg").hide();
            $(".field-signupform-password").addClass("has-error");
            $("#login-password").next(".help-block-error").html("Password cannot be blank.");
            check = 1;
            $("#login-password").keydown(function () {
                $(".field-signupform-password").removeClass("has-error");
                $("#login-password").next(".help-block-error").html("");
            });
        }
        if (check == 1)
            return false;
        $.ajax({
            type: 'GET',
            url: baseurl + '/loginvalidate',
            async: false,
            beforeSend: function () {
                $("#loginloadimg").show();
            },
            data: {
                email: email,
                password: password
            },
            success: function (data) {
                $("#loginloadimg").hide();
                if ($.trim(data) == 'empty') {
                    $(".field-signupform-email").addClass("has-error");
                    $("#login-email").next(".help-block-error").html("Email cannot be blank.");
                    cnt = 1;
                    $("#login-email").keydown(function () {
                        $(".field-signupform-email").removeClass("has-error");
                        $("#login-email").next(".help-block-error").html("");
                    });
                } else if ($.trim(data) == 'error') {
                    $(".field-signupform-email").addClass("has-error");
                    $(".field-signupform-password").addClass("has-error");
                    $("#login-email").next(".help-block-error").html("User Credentials Wrong");
                    cnt = 1;
                    $("#login-email").keydown(function () {
                        $(".field-signupform-email").removeClass("has-error");
                        $("#login-email").next(".help-block-error").html("");
                    });
                    $("#login-password").keydown(function () {
                        $(".field-signupform-password").removeClass("has-error");
                        $("#login-password").next(".help-block-error").html("");
                    });
                } else if ($.trim(data) == "passerr") {
                    $(".field-signupform-password").addClass("has-error");
                    $("#login-password").next(".help-block-error").html("Incorrect Password");
                    cnt = 1;
                    $("#login-password").keydown(function () {
                        $(".field-signupform-password").removeClass("has-error");
                        $("#login-password").next(".help-block-error").html("");
                    });
                } else if ($.trim(data) == 'success') {
                    cnt = 0;
                }

            }
        });
        if (cnt == 1) {
            return false;
        } else
            return true;

    });

    $("#password-form").on('submit', function (e) {

        var cnt = 0;
        email = $("#passwordresetrequestform-email").val();
        $.ajax({
            type: 'POST',
            url: baseurl + '/password/email',
            async: false,
            data: {
                email: email,
            },
            success: function (data) {
                if ($.trim(data) == 'empty') {
                    $(".field-passwordresetrequestform-email").addClass("has-error");
                    $("#password-form #passwordresetrequestform-email").next(".help-block-error").html("Email cannot be blank.");
                    cnt = 1;
                    return false;
                } else if (!(isValidEmailAddress($.trim(email)))) {
                    $(".field-passwordresetrequestform-email").addClass("has-error");
                    $("#password-form #passwordresetrequestform-email").next(".help-block-error").html("Email address is not valid");
                    cnt = 1;
                    return false;
                } else if ($.trim(data) == 'error') {
                    $(".field-passwordresetrequestform-email").addClass("has-error");
                    $("#password-form #passwordresetrequestform-email").next(".help-block-error").html("Email not found");
                    cnt = 1;
                    return false;
                } else if ($.trim(data) == 'success') {
                    cnt = 0;
                }
            }
        });
        if (cnt == 1) {
            return false;
        }
    });

    $("#changepassword-form").submit(function () {
        var cnt = 0;
        var check = 0;
        oldpass = $("#oldpassword").val();
        newpass = $("#newpassword").val();
        confirmpass = $("#confirmpassword").val();
        useroldpass = $("#useroldpassword").val();
        if (oldpass.length < 6)
        {
            $(".field-resetpasswordform-oldpassword").addClass("has-error");
            $("#oldpassword").next(".help-block-error").html("Old password should be atleast 6 characters");
            check = 1;
        }
        if (newpass.length < 6)
        {
            $(".field-resetpasswordform-newpassword").addClass("has-error");
            $("#newpassword").next(".help-block-error").html("New password should be atleast 6 characters");
            check = 1;
        }
        if (confirmpass.length < 6)
        {
            $(".field-resetpasswordform-confirmpassword").addClass("has-error");
            $("#confirmpassword").next(".help-block-error").html("Confirm password should be atleast 6 characters");
            check = 1;
        }
        if (oldpass != useroldpass)
        {
            $(".field-resetpasswordform-oldpassword").addClass("has-error");
            $("#oldpassword").next(".help-block-error").html("Old password is incorrect");
            check = 1;
        }
        if (newpass != confirmpass)
        {
            $(".field-resetpasswordform-newpassword").addClass("has-error");
            $("#newpassword").next(".help-block-error").html("New password and confirm password should be same");
            $(".field-resetpasswordform-confirmpassword").addClass("has-error");
            $("#confirmpassword").next(".help-block-error").html("New password and confirm password should be same");
            check = 1;
        }
        if (check == 1)
        {
            return false;
        }
        return true;
    });

    $("#resetpassword-form").submit(function () {
        var cnt = 0;
        var check = 0;
        newpass = $("#newpassword").val();
        confirmpass = $("#confirmpassword").val();
        if (newpass.length < 6)
        {
            $(".field-resetpasswordform-password").addClass("has-error");
            $("#newpassword").next(".help-block-error").html("New password should be atleast 6 characters");
            check = 1;
        }
        if (confirmpass.length < 6)
        {
            $(".field-resetpasswordform-password").addClass("has-error");
            $("#confirmpassword").next(".help-block-error").html("Confirm password should be atleast 6 characters");
            check = 1;
        }
        if (newpass != confirmpass)
        {
            $(".field-resetpasswordform-newpassword").addClass("has-error");
            $("#newpassword").next(".help-block-error").html("New password and confirm password should be same");
            $(".field-resetpasswordform-confirmpassword").addClass("has-error");
            $("#confirmpassword").next(".help-block-error").html("New password and confirm password should be same");
            check = 1;
        }
        if (check == 1)
        {
            return false;
        }
        return true;
    });

    $("#weekly").on('click', function () {
        $("#show_fixed_payment").hide();
        $("#show_weekly_payment").show();
    });

    $("#fixed").on('click', function () {
        $("#show_weekly_payment").hide();
        $("#show_fixed_payment").show();
    });

    $(".home").click(function () {
        $(".home").removeClass("activebtn");
        $(this).addClass("activebtn");
    });

    $(".room").click(function () {
        $(".room").removeClass("activebtn");
        $(this).addClass("activebtn");
    });
    $(document).on("click", ".wishli", function () {
        $(this).find(".whitehrt").toggleClass('redhrt');
    });
    $(document).on("click", ".whitehrt", function () {
        hascls = $(this).hasClass("redhrt");
        if (hascls == "true")
            $(this).removeClass('redhrt');
        else
        {
            $(this).className += " redhrt";
        }
    });



    $(document).on('keyup', '#listingname', function () {
        var maxLen = 35;
        var listingname = $('#listingname').val();
        if ($.trim(listingname).length >= maxLen) {

            document.getElementById('listingname').value = $.trim(listingname).substring(0, maxLen);

            $(".field-listing-listingname").addClass("has-error");
            $("#listingname").next(".help-block-error").show();
            $("#listingname").next(".help-block-error").html("You have reached your maximum limit of characters allowed");

            $("#listingname").change(function () {
                $(".field-listing-listingname").removeClass("has-error");
                $("#listingname").next(".help-block-error").html("");
            });
            $('#charaNum').html('0 characters left');
            return false;
        } else {
            $('#charaNum').html(maxLen - listingname.length + ' characters left');
            $("#listingname").next(".help-block-error").hide();
        }
    });

    $(document).on('keyup', '#description', function () {
        var maxLen = 250;
        var listingname = $('#description').val();
        if ($.trim(listingname).length >= maxLen) {

            document.getElementById('description').value = $.trim(listingname).substring(0, maxLen);

            $(".field-listing-description").addClass("has-error");
            $("#description").next(".help-block-error").show();
            $("#description").next(".help-block-error").html("You have reached your maximum limit of characters allowed");

            $("#description").change(function () {
                $(".field-listing-description").removeClass("has-error");
                $("#description").next(".help-block-error").html("");
            });
            $('#chardescNum').html('0 characters left');
            return false;
        } else {
            $('#chardescNum').html(maxLen - listingname.length + ' characters left');
            $("#description").next(".help-block-error").hide();
        }
    });

    $("#nightlyprice").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 13 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $(".nightlypriceerr").html("Numbers Only").css('display', 'block');
            setTimeout(function () {
                $(".nightlypriceerr").slideUp();
                $('.nightlypriceerr').html('');
            }, 5000);
            return false;
        }
    });

    $("#securitydeposit").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 13 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $(".securityerrcls").html("Numbers Only").css('display', 'block');
            setTimeout(function () {
                $(".securityerrcls").slideUp();
                $('.securityerrcls').html('');
            }, 5000);
            return false;
        }
    });

    $("#medicalno, #fireno, #policeno").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 13 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $(".numbererrcls").html("Numbers Only").css('display', 'block');
            setTimeout(function () {
                $(".numbererrcls").slideUp();
                $('.numbererrcls').html('');
            }, 5000);
            return false;
        }
    });

    $("#minstay, #maxstay").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
            //display error message
            $(".stayerrcls").html("Numbers Only").css('display', 'block');
            setTimeout(function () {
                $(".stayerrcls").slideUp();
                $('.stayerrcls').html('');
            }, 5000);
            return false;
        }

    });

    $("#minstay, #maxstay").keyup(function (e) {
        //if the letter is not digit then display error and don't type anything
        minstay = $("#minstay").val();
        maxstay = $("#maxstay").val();
        if (minstay == "0") {
            $("#minstay").val("");
            $(".stayerrcls").html("Number shoud be greater than 0").css('display', 'block');
            setTimeout(function () {
                $(".stayerrcls").slideUp();
                $('.stayerrcls').html('');
            }, 5000);
        } else if (maxstay == "0") {
            $("#maxstay").val("");
            $(".stayerrcls").html("Number shoud be greater than 0").css('display', 'block');
            setTimeout(function () {
                $(".stayerrcls").slideUp();
                $('.stayerrcls').html('');
            }, 5000);
        }

    });

    $(".form-control").on("change", function () {
        //alert("yes changed");
        $("#field_changed").val("1");
    });
    $("input:checkbox").on("change", function () {
        //alert("yes changed");
        $("#field_changed").val("1");
    });

});

function IsAlphaNumeric(e) {
    var specialKeys = new Array();
    specialKeys.push(8); // Backspace
    specialKeys.push(9); // Tab
    specialKeys.push(46); // Delete
    specialKeys.push(36); // Home
    specialKeys.push(35); // End
    specialKeys.push(37); // Left
    specialKeys.push(39); // Right
    specialKeys.push(27); // Space
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var ret = ((keyCode >= 48 && keyCode <= 57)
            || (keyCode >= 65 && keyCode <= 90) || (keyCode == 32)
            || (keyCode >= 97 && keyCode <= 122) || (specialKeys
            .indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
    return ret;
}

function IsAlphaNumericnospace(e) {
    var specialKeys = new Array();
    specialKeys.push(8); // Backspace
    specialKeys.push(9); // Tab
    specialKeys.push(46); // Delete
    specialKeys.push(36); // Home
    specialKeys.push(35); // End
    specialKeys.push(37); // Left
    specialKeys.push(39); // Right
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var ret = ((keyCode >= 48 && keyCode <= 57)
            || (keyCode >= 65 && keyCode <= 90) || (keyCode != 32)
            || (keyCode >= 97 && keyCode <= 122) || (specialKeys
            .indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
    return ret;
}

function isAlpha(e) {
    var specialKeys = new Array();
    specialKeys.push(8); // Backspace
    specialKeys.push(9); // Tab
    specialKeys.push(46); // Delete
    specialKeys.push(36); // Home
    specialKeys.push(35); // End
    specialKeys.push(37); // Left
    specialKeys.push(39); // Right
    specialKeys.push(27); // Space
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode == 32)
            || (keyCode >= 97 && keyCode <= 122) || (specialKeys
            .indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
    return ret;
}

function isNumber(eve) {
    var charCode = (eve.which) ? eve.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function isValidEmailAddress(email) {
    var emailreg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return emailreg.test(email);
}

function emailEnd(str) {
    if (str != 188 && str != 32 && str != 13)
        return;
    var $input = $('.email-frm input'), $name, email = $input.val().replace(/[, ]/g, ''), MAX_W = 340, MIN_W = 100, new_w = 0;
    if (jQuery.inArray(email, allemail) !== -1) {
        $('#emailerr').show();
        $('#emailerr').html('Already you have used this email');
        setTimeout(function () {
            $("#emailerr").hide();
        }, 5000);
        return false;
    }
    if (/^[\w\.\-\+]+@[\w\.\-]+\.[a-z]+$/i.test(email)) {
        Array.prototype.remove = function (v) {
            this.splice(this.indexOf(v) == -1 ? this.length : this.indexOf(v), 1);
        }
        $name = $('<b class="name"><span class="email">' + email + '</span><button type="button" style="padding:0 3px;background-color:#fe5571;color:#ffffff;border:1px solid #fe5571;margin:1px 5px;" class="fa fa-remove removebtncls" onclick="$(this).parents(' + "'.name'" + ').remove();allemail.remove($(this).parent().find(' + "'.email'" + ').html());if ($(' + "'.email-frm .name'" + ').length<1) {$(' + "'.email-frm .add').text('Enter your friends email address')" + '}"></button></b>').insertBefore('.email-frm .add');
        new_w = $name[0].parentNode.offsetWidth - $name[0].offsetLeft - $name[0].offsetWidth - 20;
        if (new_w < MIN_W || $input[0].offsetLeft < 10)
            new_w = MAX_W;
        $input.val('').width(new_w);
        allemail.push(email);
    }
    add_email(email);
}


function savelistPaid() {
    var check = 0;
    hometype = $(".home.activebtn").find("input").val();
    if (hometype == undefined) {
        hometype = $("select.home").val();
    }
    $("#wcategory").val(hometype);
    // roomtype = $(".room.activebtn").find("input").val();
    //accommodate = $("#accommodate").val();
    workexp = $("#workexp").val();
    if (hometype == "" || hometype == undefined || hometype == 0) {
        $("#homeerr").show();
        $("#homeerr").html("Please select Work Category");
        check = 1;
    } else {
        $("#homeerr").hide();
        $("#homeerr").html("");
    }
    if ($.trim(workexp) == "") {
        $("#workexperr").show();
        $("#workexperr").html("Enter city");
        check = 1;
    } else {
        $("#workexperr").hide();
    }

    desc = $("#description").val();
    hour = $("#hour").val();
    schedule = $("#schedule").val();
    property_status = $("#property_status").val();

    if ($.trim(desc) == "") {
        $("#descriptionerr").show();
        $("#descriptionerr").html("Please Enter Duties");
        check = 1;
    } else {
        $("#descriptionerr").hide();
    }


    if ($.trim(hour) == "") {
        $("#hourerr").show();
        $("#hourerr").html("Please Enter Hour");
        check = 1;
    } else {
        $("#hourerr").hide();
    }
    if ($.trim(schedule) == "") {
        $("#scheduleerr").show();
        $("#scheduleerr").html("Please Enter Schedule");
        check = 1;
    } else {
        $("#scheduleerr").hide();
    }
    if (check == 1) {
        return false;
    } else {
        return true;
    }
}


function savelist() {
    var check = 0;
    /*if ($("#orgname").val() == "") {
     $("#rorgname").show();
     $("#rorgname").html("Please Enter Organisation Name");
     check = 1;
     } else {
     $("#rorgname").hide();
     $("#rorgname").html("");
     }
     legalnature = $("#legalnature option:selected").val();
     if ($('#legalnature option:selected').val() == 'undefined')
     {   
     $("#natureerr").show();
     $("#natureerr").html("Please select Nature");
     check = 1;
     
     }else {
     $("#natureerr").hide();
     $("#natureerr").html("");
     }
     
     tutorrelation = $("#tutorrelation").val();
     tutorfit = $("#tutorfit").val();
     if ($('#tutorrelation option:selected').val() == 'undefined')
     {   
     $("#tutorrelationerr").show();
     $("#tutorrelationerr").html("Please select Relation with Company");
     check = 1;
     
     }else {
     $("#tutorrelationerr").hide();
     $("#tutorrelationerr").html("");
     }
     
     
     if ($('#tutorfit').val() == '')
     {   
     $("#tutorfiterr").show();
     $("#tutorfiterr").html("Please Enter Why are you fit for being a Work Tutor?");
     check = 1;
     
     }else {
     $("#tutorfiterr").hide();
     $("#tutorfiterr").html("");
     }*/


    /*students = $("#students option:selected").val();
     
     if (students == 'undefined')
     {    
     $("#stuerr").show();
     $("#stuerr").html("Please select Students");
     check = 1;
     
     }else {     
     $("#stuerr").hide();
     $("#stuerr").html("");
     }*/


    hometype = $(".home.activebtn").find("input").val();
    if (hometype == undefined) {
        hometype = $("select.home").val();
    }
    // roomtype = $(".room.activebtn").find("input").val();
    //accommodate = $("#accommodate").val();
    workexp = $("#workexp").val();
    if (hometype == "" || hometype == undefined || hometype == 0) {
        $("#homeerr").show();
        $("#homeerr").html("Please select Work Category");
        check = 1;
    } else {
        $("#homeerr").hide();
        $("#homeerr").html("");
    }
    /*if (roomtype=="" || roomtype==undefined) {
     $("#hrerror").show();
     $("#hrerror").html("Please select room type");
     check = 1;
     }else{
     $("#hrerror").hide();
     }*/

    /* if ($("#rname1").val() == "") {
     $("#rname").show();
     $("#rname").html("Please Enter Name");
     check = 1;
     } else {
     $("#rname").hide();
     $("#rname").html("");
     }
     
     if ($("#rsurname1").val() == "") {
     $("#rsurname").show();
     $("#rsurname").html("Please Enter Surname");
     check = 1;
     } else {
     $("#rsurname").hide();
     $("#rsurname").html("");
     }
     
     if ($("#rbornin1").val() == "") {
     $("#rbornin").show();
     $("#rbornin").html("Please Enter When you born");
     check = 1;
     } else {
     $("#rbornin").hide();
     $("#rbornin").html("");
     }
     
     if ($("#rtax1").val() == "") {
     $("#rtax").show();
     $("#rtax").html("Please Enter TaxNumber");
     check = 1;
     } else {
     $("#rtax").hide();
     $("#rtax").html("");
     }
     
     if ($("#remail1").val() == "") {
     $("#remail").show();
     $("#remail").html("Please Enter Email");
     check = 1;
     } else {
     $("#remail").hide();
     $("#remail").html("");
     }
     
     if ($("#rtel1").val() == "") {
     $("#rtel").show();
     $("#rtel").html("Please Enter Tel");
     check = 1;
     } else {
     $("#rtel").hide();
     $("#rtel").html("");
     }
     remail = $("#remail1").val();
     
     if (!(isValidEmailAddress(remail))) {
     $("#remail").show();
     $("#remail").html("Please Enter Valid Email");
     check = 1;
     } else {
     $("#remail").hide();
     $("#remail").html("");
     }*/
    if ($.trim(workexp) == "") {
        $("#workexperr").show();
        $("#workexperr").html("Enter city");
        check = 1;
    } else {
        $("#workexperr").hide();
    }

    desc = $("#description").val();
    hour = $("#hour").val();
    schedule = $("#schedule").val();
    property_status = $("#property_status").val();

    if ($.trim(desc) == "") {
        $("#descriptionerr").show();
        $("#descriptionerr").html("Please Enter Duties");
        check = 1;
    } else {
        $("#descriptionerr").hide();
    }


    if ($.trim(hour) == "") {
        $("#hourerr").show();
        $("#hourerr").html("Please Enter Hour");
        check = 1;
    } else {
        $("#hourerr").hide();
    }



    if ($.trim(schedule) == "") {
        $("#scheduleerr").show();
        $("#scheduleerr").html("Please Enter Schedule");
        check = 1;
    } else {
        $("#scheduleerr").hide();
    }



    /*lat = $("#pro_lat").val();
     lon = $("#pro_lon").val();
     legal_address = $("#citynew").val();
     
     
     lstreet_number = $("#lstreet_number").val();
     lroute = $("#lroute").val();
     ladministrative_area_level_1 = $("#ladministrative_area_level_1").val();
     lpostal_code = $("#lpostal_code").val();
     lcountry = $("#lcountry").val();
     llocality = $("#llocality").val();*/


    /* oname = $("#orgname").val();
     rname = $("#rname1").val();
     rsur = $("#rsurname1").val();
     rborn = $("#rbornin1").val();
     remail = $("#remail1").val();
     rtax = $("#rtax1").val();
     rtel = $("#rtel1").val();*/



    if (check == 0) {

        $.ajax({
            type: 'POST',
            url: baseurl + '/rentals/saverental',
            async: false,
            data: {
                wcategory: hometype,
                workexp: workexp,
                desc: desc,
                schedule: schedule,
                hour: hour,
                property_status: property_status
            },
            success: function (data) {
                var result = data.split(',');

                if ($.trim(data) == "error")
                {
                    $("#emailverifyerr").show();
                    $("#emailverifyerr").html("You need to add your paypal id and need to verify your email id before add listing");
                    setTimeout(function () {
                        $("#emailverifyerr").hide();
                    }, 5000);
                }
                if ($.trim(data) == "emailerror")
                {
                    $("#emailverifyerr").show();
                    $("#emailverifyerr").html("You need to verify your email id before add listing");
                    setTimeout(function () {
                        $("#emailverifyerr").hide();
                    }, 5000);
                }
                if ($.trim(data) == "paypalerror")
                {
                    $("#emailverifyerr").show();
                    $("#emailverifyerr").html("You need to add your paypal id before add listing");
                    setTimeout(function () {
                        $("#emailverifyerr").hide();
                    }, 5000);
                }
                if ($.trim(result[0]) == "success")
                {
                    if (result[2] == 'admin') {
                        //alert("true");
                        window.location = baseurl + '/admin/property/basic/' + result[1];
                    } else {
                        // alert("false");
                        window.location = baseurl + '/rentals/basic/' + result[1];
                    }

                }
            }
        });
    }
}

function show_description()
{
    var check = 0;
    /*var hometype = $("#hometype").val();
     var roomtype = $("#roomtype").val();
     if(hometype == ''){
     $(".field-listing-hometype").addClass("has-error");
     $("#hometype").next(".help-block-error").html("Property type cannot be empty");
     check = 1;
     $("#hometype").change(function(){
     $(".field-listing-hometype").removeClass("has-error");
     $("#hometype").next(".help-block-error").html("");
     });
     
     }
     
     if(roomtype == ''){
     $(".field-listing-roomtype").addClass("has-error");
     $("#roomtype").next(".help-block-error").html("Select Room type");
     check = 1;
     $("#roomtype").change(function(){
     $(".field-listing-roomtype").removeClass("has-error");
     $("#roomtype").next(".help-block-error").html("");
     });
     }
     
     if(check == 1){
     return false;	
     }*/

    hometype = $("#hometype").val();
    roomtype = $("#roomtype").val();
    accommodates = $("#accommodates").val();
    bedrooms = $("#bedrooms").val();
    beds = $("#beds").val();
    bathrooms = $("#bathrooms").val();
    listingid = $("#listingid").val();

    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/savebasicslist',
        async: false,
        data: {
            hometype: hometype,
            roomtype: roomtype,
            accommodates: accommodates,
            bedrooms: bedrooms,
            beds: beds,
            bathrooms: bathrooms,
            listingid: listingid
        },
        success: function (data) {
            $("#showBasi").css('background', 'none');
            $("#showDesc").css('background', '#ddd');
            $("#basicsdiv").hide();
            $("#descriptiondiv").show();
            $(window).scrollTop(0);
        }
    });


}

function show_basics()
{
    $("#showBasi").css('background', '#ddd');
    $("#showDesc").css('background', 'none');
    $("#basicsdiv").show();
    $("#descriptiondiv").hide();
}

function show_location()
{
    var maxLen = 250;
    var listingname = $("#listingname").val();
    var descri = $("#description").val();
    listingid = $("#listingid").val();
    var check = 0;
    if ($.trim(listingname) == '') {
        $(".field-listing-listingname").addClass("has-error");
        $(".field-listing-listingname .help-block-error").html("Listing Name cannot be blank");
        $(".field-listing-listingname .help-block-error").show();
        check = 1;
        $("#listingname").keydown(function () {
            $(".field-listing-listingname").removeClass("has-error");
            $(".field-listing-listingname .help-block-error").html("");
        });
    }
    document.getElementById('description').value = $.trim(descri).substring(0, maxLen);
    var descri = $("#description").val();
    if ($.trim(descri).length >= maxLen) {
        $(".field-listing-description").addClass("has-error");
        $(".field-listing-description .help-block-error").show();
        $(".field-listing-description .help-block-error").html("You have reached your maximum limit of characters allowed");
    }
    var descri = $("#description").val();
    if ($.trim(descri) == '') {
        $(".field-listing-description").addClass("has-error");
        $(".field-listing-description .help-block-error").html("Description cannot be blank");
        $(".field-listing-description .help-block-error").show();
        check = 1;
        $("#description").keydown(function () {
            $(".field-listing-description").removeClass("has-error");
            $(".field-listing-description .help-block-error").html("");
        });
    }
    if (check == 1) {
        return false;
    }


    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/savedescriptionlist',
        async: false,
        data: {
            listingname: listingname,
            descri: descri,
            listingid: listingid
        },
        success: function (data) {
            $("#showDesc").css('background', 'none');
            $("#showLoc").css('background', '#ddd');
            $("#descriptiondiv").hide();
            $("#locationdiv").show();
            $(window).scrollTop(0);
        }
    });
}

function show_amenities()
{
    var check = 0;
    city = $("#city").val();
    state = $("#state").val();
    countries = $("#country option:selected").text();
    countryval = $("#country").val();
    zipcode = $("#zipcode").val();
    streetaddress = $("#streetaddress").val();
    accesscode = $("#accesscode").val();
    listingid = $("#listingid").val();
    if (countries == 'Select...') {
        country = "";
    } else {
        country = countries;
    }

    if ($.trim(country) == '') {
        $(".field-listing-country").addClass("has-error");
        $("#country").next(".help-block-error").html("Select Country");
        check = 1;
        $("#country").change(function () {
            $(".field-listing-country").removeClass("has-error");
            $("#country").next(".help-block-error").html("");
        });

    }
    if ($.trim(city) == '') {
        $(".field-listing-city").addClass("has-error");
        $("#city").next(".help-block-error").html("City cannot be blank");
        check = 1;
        $("#city").keydown(function () {
            $(".field-listing-city").removeClass("has-error");
            $("#city").next(".help-block-error").html("");
        });

    }
    if ($.trim(streetaddress) == '') {
        $(".field-listing-streetaddress").addClass("has-error");
        $("#streetaddress").next(".help-block-error").html("Street Address cannot be blank");
        check = 1;
        $("#streetaddress").keydown(function () {
            $(".field-listing-streetaddress").removeClass("has-error");
            $("#streetaddress").next(".help-block-error").html("");
        });

    }


    if (check == 1) {
        return false;
    } else
    {
        address = streetaddress + ',' + city + ',' + state + ',' + country + ',' + zipcode;
        if (showAddress(address))
        {
            showAddress(address);
            latitude = $("#latbox").val();
            longitude = $("#lonbox").val();
            if ($.trim(latitude) == "" || $.trim(longitude) == "")
            {
                $(".errcls").show();
                $(".errcls").html("Cannot get latitude and longitude. Please enter valid address");
                setTimeout(function () {
                    $(".errcls").slideUp();
                    $('.errcls').html('');
                }, 5000);
                check = 1;
            } else
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + '/user/listing/savelocationlist',
                    async: false,
                    data: {
                        country: countryval,
                        streetaddress: streetaddress,
                        accesscode: accesscode,
                        city: city,
                        state: state,
                        zipcode: zipcode,
                        listingid: listingid,
                        latitude: latitude,
                        longitude: longitude
                    },
                    success: function (data) {
                        $("#showLoc").css('background', 'none');
                        $("#showAmenities").css('background', '#ddd');
                        $("#locationdiv").hide();
                        $("#amenitiesdiv").show();
                        $(window).scrollTop(0);
                    }
                });
            }
        }
    }
}

function show_backlocation()
{
    $("#showLoc").css('background', '#ddd');
    $("#showAmenities").css('background', 'none');
    $("#amenitiesdiv").hide();
    $("#locationdiv").show();
    $(window).scrollTop(0);
}

function show_photos()
{
    var flag1 = 0;
    var flag2 = 0;
    var flag3 = 0;
    var flag4 = 0;
    var check = 0;
    var commonamenity = [];
    var additionalamenity = [];
    var specialfeature = [];
    var safetycheckarr = [];
    listingid = $("#listingid").val();
    var commonAmentities = document.getElementsByName('commonamenities[]');
    for (var i = 0; i < commonAmentities.length; i++) {
        if (commonAmentities[i].checked) {
            commonval = commonAmentities[i].value;
            commonamenity.push(commonval);
            flag1++;
        }
    }

    var additionalamenities = document.getElementsByName('additionalamenities[]');
    for (var i = 0; i < additionalamenities.length; i++) {
        if (additionalamenities[i].checked) {
            additionalval = additionalamenities[i].value;
            additionalamenity.push(additionalval);
            flag2++;
        }
    }
    var specialfeatures = document.getElementsByName('specialfeatures[]');
    for (var i = 0; i < specialfeatures.length; i++) {
        if (specialfeatures[i].checked) {
            specialval = specialfeatures[i].value;
            specialfeature.push(specialval);
            flag3++;
        }
    }
    var safetycheck = document.getElementsByName('safetycheck[]');
    for (var i = 0; i < safetycheck.length; i++) {
        if (safetycheck[i].checked) {
            safetyval = safetycheck[i].value;
            safetycheckarr.push(safetyval);
            flag4++;
        }
    }

    if (flag1 == 0 || flag2 == 0 || flag3 == 0 || flag4 == 0)
    {
        $(".amentierrcls").show();
        $(".amentierrcls").html("Select atleast one in every block ");
        setTimeout(function () {
            $(".amentierrcls").slideUp();
            $('.amentierrcls').html('');
        }, 5000);
        check = 1;
    }
    if (check == 1) {
        return false;
    }
    $.ajax({
        type: 'POST',
        url: baseurl + '/rentals/saveamenitylist',
        async: false,
        data: {
            commonamenity: commonamenity,
            additionalamenity: additionalamenity,
            specialfeature: specialfeature,
            safetycheck: safetycheckarr,
            listingid: listingid
        },
        success: function (data) {
            //$("#showAmenities").css('background','none');
            alert("Amenities Added Successfully");
            if (data == 'admin') {
                window.location = baseurl + '/admin/property/amenities/' + listingid;
            } else {
                window.location = baseurl + '/rentals/amenities/' + listingid;
            }


            /*$("#showPhoto").css('background','#ddd');
             $("#amenitiesdiv").hide();
             $("#photosdiv").show();
             $(window).scrollTop(0);*/
        }
    });
}

function show_safety()
{
    var uploadimg = $("#uploadedfiles").val();
    listingid = $("#listingid").val();
    if ($.trim(uploadimg) == "") {
        $(".photoerrcls").show();
        $(".photoerrcls").html("Upload atleast one image");
        setTimeout(function () {
            $(".photoerrcls").slideUp();
            $('.photoerrcls').html('');
        }, 5000);
        return false;
    }
    $.ajax({
        type: 'POST',
        url: baseurl + '/rentals/savephotolist',
        async: false,
        data: {
            uploadimg: uploadimg,
            listingid: listingid
        },
        success: function (data) {
            alert('Photo uploaded successfully');
            if (data == 'admin') {
                window.location = baseurl + '/admin/property/photos/' + listingid;
            } else {
                window.location = baseurl + '/rentals/photos/' + listingid;
            }

            /*$("#showPhoto").css('background','none');
             $("#showHomesafe").css('background','#ddd');
             $("#photosdiv").hide();
             $("#safetydiv").show();
             $(window).scrollTop(0);*/
        }
    });


}

function show_safety_paid()
{
    var uploadimg = $("#uploadedfiles").val();
    listingid = $("#listingid").val();
    if ($.trim(uploadimg) == "") {
        $(".photoerrcls").show();
        $(".photoerrcls").html("Upload atleast one image");
        setTimeout(function () {
            $(".photoerrcls").slideUp();
            $('.photoerrcls').html('');
        }, 5000);
        return false;
    }
    $.ajax({
        type: 'POST',
        url: baseurl + '/rentals/savephotolist',
        async: false,
        data: {
            uploadimg: uploadimg,
            listingid: listingid
        },
        success: function (data) {
            alert('Photo uploaded successfully');
            if (data == 'admin') {
                window.location = baseurl + '/admin/property/photosPaid/' + listingid;
            } else {
                window.location = baseurl + '/rentals/photos_paid/' + listingid;
            }

            /*$("#showPhoto").css('background','none');
             $("#showHomesafe").css('background','#ddd');
             $("#photosdiv").hide();
             $("#safetydiv").show();
             $(window).scrollTop(0);*/
        }
    });


}

function show_backamenities()
{
    $("#showPhoto").css('background', 'none');
    $("#showAmenities").css('background', '#ddd');
    $("#photosdiv").hide();
    $("#amenitiesdiv").show();
    $(window).scrollTop(0);
}

function show_price()
{
    var flag = 0;
    var check = 0;
    var emercheck = 0;
    var safetylist = [];
    listingid = $("#listingid").val();
    var safetycheck = document.getElementsByName('safetycheck[]');
    for (var i = 0; i < safetycheck.length; i++) {
        if (safetycheck[i].checked) {
            safetyval = safetycheck[i].value;
            safetylist.push(safetyval);
            flag++;
        }
    }

    if (flag == 0)
    {
        $(".safeerrcls").show();
        $(".safeerrcls").html("Select atleast one in safety checklist");
        setTimeout(function () {
            $(".safeerrcls").slideUp();
            $('.safeerrcls').html('');
        }, 5000);
        check = 1;
    }
    fireextinguisher = $("#fireextinguisher").val();
    firealarm = $("#firealarm").val();
    gasshutoffvalve = $("#gasshutoffvalve").val();
    emergencyexitinstruction = $("#emergencyexitinstruction").val();
    if ($.trim(fireextinguisher) == '') {
        $(".field-listing-fireextinguisher").addClass("has-error");
        $("#fireextinguisher").next(".help-block-error").html("Fire extinguisher cannot be blank");
        check = 1;
        $("#fireextinguisher").keydown(function () {
            $(".field-listing-fireextinguisher").removeClass("has-error");
            $("#fireextinguisher").next(".help-block-error").html("");
        });
    }
    if ($.trim(firealarm) == '') {
        $(".field-listing-firealarm").addClass("has-error");
        $("#firealarm").next(".help-block-error").html("Fire Alarm cannot be blank");
        check = 1;
        $("#firealarm").keydown(function () {
            $(".field-listing-firealarm").removeClass("has-error");
            $("#firealarm").next(".help-block-error").html("");
        });
    }
    if ($.trim(gasshutoffvalve) == '') {
        $(".field-listing-gasshutoffvalve").addClass("has-error");
        $("#gasshutoffvalve").next(".help-block-error").html("Gas shutoff valve cannot be blank");
        check = 1;
        $("#gasshutoffvalve").keydown(function () {
            $(".field-listing-gasshutoffvalve").removeClass("has-error");
            $("#gasshutoffvalve").next(".help-block-error").html("");
        });
    }
    if ($.trim(emergencyexitinstruction) == '') {
        $(".field-listing-emergencyexitinstruction").addClass("has-error");
        $("#emergencyexitinstruction").next(".help-block-error").html("Emergency exit instructions cannot be blank");
        check = 1;
        $("#emergencyexitinstruction").keydown(function () {
            $(".field-listing-emergencyexitinstruction").removeClass("has-error");
            $("#emergencyexitinstruction").next(".help-block-error").html("");
        });
    }
    medicalno = $("#medicalno").val();
    if ($.trim(medicalno) == '') {
        $(".field-listing-medicalno").addClass("has-error");
        $("#medicalno").next(".help-block-error").html("Medical Number is required");
        emercheck = 1;
        $("#medicalno").keydown(function () {
            $(".field-listing-medicalno").removeClass("has-error");
            $("#medicalno").next(".help-block-error").html("");
        });
    }

    fireno = $("#fireno").val();
    if ($.trim(fireno) == '') {
        $(".field-listing-fireno").addClass("has-error");
        $("#fireno").next(".help-block-error").html("Fire Number is required");
        emercheck = 1;
        $("#fireno").keydown(function () {
            $(".field-listing-fireno").removeClass("has-error");
            $("#fireno").next(".help-block-error").html("");
        });
    }
    policeno = $("#policeno").val();
    if ($.trim(policeno) == '') {
        $(".field-listing-policeno").addClass("has-error");
        $("#policeno").next(".help-block-error").html("Police Number is required");
        emercheck = 1;
        $("#policeno").keydown(function () {
            $(".field-listing-policeno").removeClass("has-error");
            $("#policeno").next(".help-block-error").html("");
        });
    }
    if (check == 1) {
        return false;
    }
    if (emercheck == 1) {
        $(".safeerrcls").show();
        $(".safeerrcls").html("Emergency Phone numbers required. Click edit button and fill");
        setTimeout(function () {
            $(".safeerrcls").slideUp();
            $('.safeerrcls').html('');
        }, 5000);
        return false;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/savesafetylist',
        async: false,
        data: {
            safetylist: safetylist,
            fireextinguisher: fireextinguisher,
            firealarm: firealarm,
            gasshutoffvalve: gasshutoffvalve,
            emergencyexitinstruction: emergencyexitinstruction,
            medicalno: medicalno,
            fireno: fireno,
            policeno: policeno,
            listingid: listingid
        },
        success: function (data) {
            $("#showHomesafe").css('background', 'none');
            $("#showPricing").css('background', '#ddd');
            $("#pricediv").show();
            $("#safetydiv").hide();
            $(window).scrollTop(0);
        }
    });


}

function show_backphotos()
{
    $("#showHomesafe").css('background', 'none');
    $("#showPhoto").css('background', '#ddd');
    $("#safetydiv").hide();
    $("#photosdiv").show();
    $(window).scrollTop(0);
}

function show_booking()
{
    var check = 0;
    var nightlyprice = $("#nightlyprice").val();
    currency = $("#currency").val();
    listingid = $("#listingid").val();
    var securitydeposit = $("#securitydeposit").val();
    if ($.trim(nightlyprice) == '') {
        $(".nightlypriceerr").show();
        $(".nightlypriceerr").html("Nightly Price is required");
        $("#nightlyprice").keydown(function () {
            $(".nightlypriceerr").hide();
            $(".nightlypriceerr").html("");
        });
        check = 1;
    }
    if (currency == "") {
        $(".field-listing-currency").addClass("has-error");
        $("#currency").next(".help-block-error").show();
        $("#currency").next(".help-block-error").html("Select any currency");

        $("#currency").change(function () {
            $(".field-listing-currency").removeClass("has-error");
            $("#currency").next(".help-block-error").html("");
        });
        check = 1;
    }
    /*if($.trim(securitydeposit) == ''){
     $(".securityerrcls").show();
     $(".securityerrcls").html("Security Deposit is required");
     $("#securitydeposit").keydown(function(){
     $(".securityerrcls").hide();
     $(".securityerrcls").html("");
     });
     check = 1;
     }*/
    if (check == 1) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + '/rentals/savepricelist',
        async: false,
        data: {
            nightlyprice: nightlyprice,
            securitydeposit: securitydeposit,
            currency: currency,
            listingid: listingid
        },
        success: function (data) {
            if (data == 'admin') {
                window.location = baseurl + '/admin/property/pricing/' + listingid;
            } else {
                window.location = baseurl + '/rentals/pricing/' + listingid;
            }

            /*$("#showPricing").css('background','none');
             $("#showBooking").css('background','#ddd');
             $("#bookingdiv").show();
             $("#pricediv").hide();
             $(window).scrollTop(0);*/
        }
    });

}

function savecalendar()
{
    var check = 0;
    var bookingavailability = $("#bookingavailability").val();
    listingid = $("#listingid").val();
    startDate = $("#startdate").val();
    EndDate = $("#enddate").val();
    //alert(startDate);
    if (bookingavailability == 1) {
        if (startDate == "") {
            $(".sdate").html("Start Date should not be empty").css('display', 'block');
            setTimeout(function () {
                $(".sdate").slideUp();
                $('.sdate').html('');
            }, 5000);
            return false;
        }
        if (EndDate == "") {
            $(".edate").html("End Date should not be empty").css('display', 'block');
            setTimeout(function () {
                $(".edate").slideUp();
                $('.edate').html('');
            }, 5000);
            return false;
        }

    }
    /* if(maxstay == ""){
     $(".stayerrcls").html("Maximum stay cannot be empty").css('display','block');
     setTimeout(function() {
     $(".stayerrcls").slideUp();
     $('.stayerrcls').html('');
     }, 5000);
     return false;
     }*/
    /*if (minstay>maxstay) {
     $(".stayerrcls").html("Minimum stay should be less than maximum stay").css('display','block');
     setTimeout(function() {
     $(".stayerrcls").slideUp();
     $('.stayerrcls').html('');
     }, 5000);
     return false;
     }*/


    $.ajax({
        type: 'POST',
        url: baseurl + '/rentals/savecalendar',
        async: false,
        data: {
            bookingavailability: bookingavailability,
            listingid: listingid,
            startDate: startDate,
            EndDate: EndDate

        },
        success: function (data) {

            if (data == 'admin') {
                window.location = baseurl + '/admin/property/calendarPaid/' + listingid;
            } else {
                location.reload();
                //window.location = baseurl + '/rentals/calendar/' + listingid;
            }
            /*$("#showPricing").css('background','none');
             $("#showBooking").css('background','#ddd');
             $("#bookingdiv").show();
             $("#pricediv").hide();
             $(window).scrollTop(0);*/
        }
    });

}

function savecalendarpaid()
{
    var check = 0;
    var bookingavailability = $("#bookingavailability").val();
    listingid = $("#listingid").val();
    startDate = $("#startdate").val();
    EndDate = $("#enddate").val();
    //alert(startDate);
    if (bookingavailability == 1) {
        if (startDate == "") {
            $(".sdate").html("Start Date should not be empty").css('display', 'block');
            setTimeout(function () {
                $(".sdate").slideUp();
                $('.sdate').html('');
            }, 5000);
            return false;
        }
        if (EndDate == "") {
            $(".edate").html("End Date should not be empty").css('display', 'block');
            setTimeout(function () {
                $(".edate").slideUp();
                $('.edate').html('');
            }, 5000);
            return false;
        }

    }
    $.ajax({
        type: 'POST',
        url: baseurl + '/rentals/savecalendar',
        async: false,
        data: {
            bookingavailability: bookingavailability,
            listingid: listingid,
            startDate: startDate,
            EndDate: EndDate

        },
        success: function (data) {

            if (data == 'admin') {
                window.location = baseurl + '/admin/property/calendar/' + listingid;
            } else {
                window.location = baseurl + '/rentals/calendar_paid/' + listingid;
            }
        }
    });

}







function save_booking_paid()
{
    var check = 0;
    var bookingstyle = $("#bookingstyle").val();
    listingid = $("#listingid").val();
    var houserules = $("#houserules").val();
    var link_to_extra_docs_necessary = $("#link_to_extra_docs_necessary").val();
    if ($.trim(houserules) == '') {
        $(".securityerrcls1").show();
        $(".securityerrcls1").html("Rules is required");
        $("#houserules").keydown(function () {
            $(".houserules").hide();
            $(".securityerrcls1").html("");
        });
        check = 1;
    }

    if (check == 1) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + '/rentals/savebookings',
        async: false,
        data: {
            bookingstyle: bookingstyle,
            houserules: houserules,
            link_to_extra_docs_necessary: link_to_extra_docs_necessary,
            listingid: listingid,
        },
        success: function (data) {
            if (data == 'admin') {
                window.location = baseurl + '/admin/property/bookingPaid/' + listingid;
            } else {
                window.location = baseurl + '/rentals/booking_paid/' + listingid;
            }
        }
    });

}

function save_booking()
{
    var check = 0;
    var bookingstyle = $("#bookingstyle").val();
    listingid = $("#listingid").val();
    var houserules = $("#houserules").val();
    var link_to_extra_docs_necessary = $("#link_to_extra_docs_necessary").val();
    //alert(listingid);
    /* var work_schedule = $("#work_schedule").val();
     var work_hours = $("#work_hours").val();*/
    // var cancellation = $("#cancellation").val();
    if ($.trim(houserules) == '') {
        $(".securityerrcls1").show();
        $(".securityerrcls1").html("Workspace Rules is required");
        $("#houserules").keydown(function () {
            $(".houserules").hide();
            $(".securityerrcls1").html("");
        });
        check = 1;
    }
    /*if($.trim(work_schedule) == ''){
     $(".work_schedule").show();
     $(".work_schedule").html("Work Schedule is required");
     $("#work_schedule").keydown(function(){
     $(".work_schedule").hide();
     $(".work_schedule").html("");
     });
     check = 1;
     } 
     if($.trim(work_hours) == '') {
     $(".work_hours").show();
     $(".work_hours").html("Work Hours is required");
     $("#work_hours").keydown(function(){
     $(".work_hours").hide();
     $(".work_hours").html("");
     });
     check = 1;
     }  */


    if (check == 1) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + '/rentals/savebookings',
        async: false,
        data: {
            bookingstyle: bookingstyle,
            houserules: houserules,
            link_to_extra_docs_necessary: link_to_extra_docs_necessary,
            listingid: listingid,
        },
        success: function (data) {
            if (data == 'admin') {
                window.location = baseurl + '/admin/property/booking/' + listingid;
            } else {
                window.location = baseurl + '/rentals/booking/' + listingid;
            }
            //window.location = baseurl + '/rentals/bookings/'+listingid;
            /*$("#showPricing").css('background','none');
             $("#showBooking").css('background','#ddd');
             $("#bookingdiv").show();
             $("#pricediv").hide();
             $(window).scrollTop(0);*/
        }
    });

}

function show_backsafety()
{
    $("#showHomesafe").css('background', '#ddd');
    $("#showPricing").css('background', 'none');
    $("#pricediv").hide();
    $("#safetydiv").show();
}

function show_calendar()
{

    $("#showBooking").css('background', 'none');
    $("#showCalendar").css('background', '#ddd');
    $("#calendardiv").show();
    $("#bookingdiv").hide();
    $("#bookavailability").show();
    $("#bookdate").hide();
}

function show_backprice()
{
    $("#showBooking").css('background', 'none');
    $("#showPricing").css('background', '#ddd');
    $("#bookingdiv").hide();
    $("#pricediv").show();
}

function show_profile()
{
    $("#profilediv").show();
    $("#calendardiv").hide();
}

function show_backbooking()
{
    disp = $("#bookavailability").css("display");
    if (disp == "none") {
        $("#calendardiv").show();
        $("#bookdate").hide();
        $("#bookavailability").show();
    } else
    {
        $("#showBooking").css('background', '#ddd');
        $("#showCalendar").css('background', 'none');
        $("#calendardiv").hide();
        $("#bookavailability").hide();
        $("#bookingdiv").show();
    }
}

function show_backbookingavail()
{
    $("#bookavailability").show();
    $("#bookdate").hide();
}

function show_backcalendar()
{
    $("#calendardiv").show();
    $("#profilediv").hide();
}

function show_request_book()
{
    /*$("#bookingtype").hide();
     $("#requestbook").show();*/
    $("#bookingstyle").val(1);
    $("#requestbook").css("cssText", "background-color: #4d4d4d !important;");
    $("#instantbook").css("cssText", "background-color: #fe5771 !important;");
}

function show_instant_book()
{
    /*$("#bookingtype").hide();
     $("#instantbook").show();*/
    $("#bookingstyle").val(2);
    $("#requestbook").css("cssText", "background-color: #fe5771 !important;");
    $("#instantbook").css("cssText", "background-color: #4d4d4d !important;");
}

function show_request_type()
{
    $("#requestbook").hide();
    $("#bookingtype").show();
}

function show_instant_type()
{
    $("#instantbook").hide();
    $("#bookingtype").show();
}

function update_booktype(bookavail, org)
{
    $("#bookingavailability").val(bookavail);
    $(".alwaysinnerdiv").css('background', '#f5f5f5');
    $(org).css('background', '#fff');
    if (bookavail == 2)
    {
        $("#bookavailability").hide();
        $("#bookdate").show();
    }
}

function chagecalendar()
{
    $("#bookavailability").show();
    $("#bookdate").hide();
}

function savefullist()
{
    listid = $("#listingid").val();
    hometype = $("#hometype").val();
    roomtype = $("#roomtype").val();
    accommodates = $("#accommodates").val();
    bedrooms = $("#bedrooms").val();
    beds = $("#beds").val();
    bathrooms = $("#bathrooms").val();
    listingname = $("#listingname").val();
    description = $("#description").val();
    country = $("#country").val();
    streetaddress = $("#streetaddress").val();
    accesscode = $("#accesscode").val();
    city = $("#city").val();
    state = $("#state").val();
    zipcode = $("#zipcode").val();
    commonamenities = [];
    additionalamenities = [];
    specialfeatures = [];
    safetychecklist = [];
    var commonamenity = [];
    var additionalamenity = [];
    var specialfeature = [];
    var safetylist = [];
    var flag1 = 0;
    var flag2 = 0;
    var flag3 = 0;
    var flag4 = 0;
    var check = 0;
    $('input[name="commonamenities[]"]:checked').each(function () {
        commonamenities.push($(this).val());
    });
    $('input[name="additionalamenities[]"]:checked').each(function () {
        additionalamenities.push($(this).val());
    });
    $('input[name="specialfeatures[]"]:checked').each(function () {
        specialfeatures.push($(this).val());
    });
    $('input[name="safetycheck[]"]:checked').each(function () {
        safetychecklist.push($(this).val());
    });
    var commonAmentities = document.getElementsByName('commonamenities[]');
    for (var i = 0; i < commonAmentities.length; i++) {
        if (commonAmentities[i].checked) {
            commonval = commonAmentities[i].value;
            commonamenity.push(commonval);
            flag1++;
        }
    }

    var additionalAmenities = document.getElementsByName('additionalamenities[]');
    for (var i = 0; i < additionalAmenities.length; i++) {
        if (additionalAmenities[i].checked) {
            additionalval = additionalAmenities[i].value;
            additionalamenity.push(additionalval);
            flag2++;
        }
    }
    var specialFeatures = document.getElementsByName('specialfeatures[]');
    for (var i = 0; i < specialFeatures.length; i++) {
        if (specialFeatures[i].checked) {
            specialval = specialFeatures[i].value;
            specialfeature.push(specialval);
            flag3++;
        }
    }
    var safetyCheck = document.getElementsByName('safetycheck[]');
    for (var i = 0; i < safetyCheck.length; i++) {
        if (safetyCheck[i].checked) {
            safetyval = safetyCheck[i].value;
            safetylist.push(safetyval);
            flag4++;
        }
    }
    fireextinguisher = $("#fireextinguisher").val();
    firealarm = $("#firealarm").val();
    gasshutoffvalve = $("#gasshutoffvalve").val();
    emergencyexitinstruction = $("#emergencyexitinstruction").val();
    medicalno = $("#medicalno").val();
    fireno = $("#fireno").val();
    policeno = $("#policeno").val();
    nightlyprice = $("#nightlyprice").val();
    currency = $("#currency").val();
    bookingstyle = $("#bookingstyle").val();
    bookingavailability = $("#bookingavailability").val();
    startdate = $("#startdate").val();
    enddate = $("#enddate").val();
    files = $("#uploadedfiles").val();
    latitude = $("#latbox").val();
    longitude = $("#lonbox").val();
    houserules = $("#houserules").val();
    securitydeposit = $("#securitydeposit").val();
    minstay = $("#minstay").val();
    maxstay = $("#maxstay").val();
    startdate = new Date(startdate);
    enddate = new Date(enddate);
    var timeDiff = Math.abs(enddate.getTime() - startdate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    if ($.trim(streetaddress) == '') {
        $(".field-listing-streetaddress").addClass("has-error");
        $("#streetaddress").next(".help-block-error").html("Street Address cannot be blank");
        $("#streetaddress").keydown(function () {
            $(".field-listing-streetaddress").removeClass("has-error");
            $("#streetaddress").next(".help-block-error").html("");
        });

    }
    if ($.trim(zipcode) == '') {
        $(".field-listing-zipcode").addClass("has-error");
        $("#zipcode").next(".help-block-error").html("Zipcode cannot be blank");
        $("#zipcode").keydown(function () {
            $(".field-listing-zipcode").removeClass("has-error");
            $("#zipcode").next(".help-block-error").html("");
        });

    }
    if (flag1 == 0 || flag2 == 0 || flag3 == 0)
    {
        $(".amentierrcls").show();
        $(".amentierrcls").html("Select atleast one in every block ");
        check = 1;
    }

    if (flag4 == 0)
    {
        $(".safeerrcls").show();
        $(".safeerrcls").html("Select atleast one in safety checklist");
        check = 1;
    }

    if ($.trim(country) == '') {
        $(".field-listing-country").addClass("has-error");
        $("#country").next(".help-block-error").html("Select Country");
        $("#country").change(function () {
            $(".field-listing-country").removeClass("has-error");
            $("#country").next(".help-block-error").html("");
        });

    }
    if ($.trim(city) == '') {
        $(".field-listing-city").addClass("has-error");
        $("#city").next(".help-block-error").html("City cannot be blank");
        $("#city").keydown(function () {
            $(".field-listing-city").removeClass("has-error");
            $("#city").next(".help-block-error").html("");
        });

    }
    if ($.trim(state) == '') {
        $(".field-listing-state").addClass("has-error");
        $("#city").next(".help-block-error").html("State cannot be blank");
        $("#state").keydown(function () {
            $(".field-listing-state").removeClass("has-error");
            $("#state").next(".help-block-error").html("");
        });

    }
    if ($.trim(streetaddress) == '') {
        $(".field-listing-streetaddress").addClass("has-error");
        $("#streetaddress").next(".help-block-error").html("Street Address cannot be blank");
        $("#streetaddress").keydown(function () {
            $(".field-listing-streetaddress").removeClass("has-error");
            $("#streetaddress").next(".help-block-error").html("");
        });

    }
    if ($.trim(files) == "") {
        $(".photoerrcls").show();
        $(".photoerrcls").html("Upload atleast one image");
    }
    if ($.trim(fireextinguisher) == '') {
        $(".field-listing-fireextinguisher").addClass("has-error");
        $("#fireextinguisher").next(".help-block-error").html("Fire extinguisher cannot be blank");
        $("#fireextinguisher").keydown(function () {
            $(".field-listing-fireextinguisher").removeClass("has-error");
            $("#fireextinguisher").next(".help-block-error").html("");
        });
    }
    if ($.trim(firealarm) == '') {
        $(".field-listing-firealarm").addClass("has-error");
        $("#firealarm").next(".help-block-error").html("Fire Alarm cannot be blank");
        $("#firealarm").keydown(function () {
            $(".field-listing-firealarm").removeClass("has-error");
            $("#firealarm").next(".help-block-error").html("");
        });
    }
    if ($.trim(gasshutoffvalve) == '') {
        $(".field-listing-gasshutoffvalve").addClass("has-error");
        $("#gasshutoffvalve").next(".help-block-error").html("Gas shutoff valve cannot be blank");
        $("#gasshutoffvalve").keydown(function () {
            $(".field-listing-gasshutoffvalve").removeClass("has-error");
            $("#gasshutoffvalve").next(".help-block-error").html("");
        });
    }
    if ($.trim(emergencyexitinstruction) == '') {
        $(".field-listing-emergencyexitinstruction").addClass("has-error");
        $("#emergencyexitinstruction").next(".help-block-error").html("Emergency exit instructions cannot be blank");
        $("#emergencyexitinstruction").keydown(function () {
            $(".field-listing-emergencyexitinstruction").removeClass("has-error");
            $("#emergencyexitinstruction").next(".help-block-error").html("");
        });
    }
    if ($.trim(medicalno) == '') {
        $(".field-listing-medicalno").addClass("has-error");
        $("#medicalno").next(".help-block-error").html("Medical Number is required");
        $("#medicalno").keydown(function () {
            $(".field-listing-medicalno").removeClass("has-error");
            $("#medicalno").next(".help-block-error").html("");
        });
    }

    if ($.trim(fireno) == '') {
        $(".field-listing-fireno").addClass("has-error");
        $("#fireno").next(".help-block-error").html("Fire Number is required");
        $("#fireno").keydown(function () {
            $(".field-listing-fireno").removeClass("has-error");
            $("#fireno").next(".help-block-error").html("");
        });
    }
    if ($.trim(policeno) == '') {
        $(".field-listing-policeno").addClass("has-error");
        $("#policeno").next(".help-block-error").html("Fire Number is required");
        $("#policeno").keydown(function () {
            $(".field-listing-policeno").removeClass("has-error");
            $("#policeno").next(".help-block-error").html("");
        });
    }
    if ($.trim(nightlyprice) == '') {
        $(".nightlypriceerr").show();
        $(".nightlypriceerr").html("Nightly Price is required");
        $("#nightlyprice").keydown(function () {
            $(".nightlypriceerr").hide();
            $(".nightlypriceerr").html("");
        });
    }
    if (currency == "") {
        $(".field-listing-currency").addClass("has-error");
        $("#currency").next(".help-block-error").show();
        $("#currency").next(".help-block-error").html("Select any currency");

        $("#currency").change(function () {
            $(".field-listing-currency").removeClass("has-error");
            $("#currency").next(".help-block-error").html("");
        });
    }
    if ($.trim(listingname) == '') {
        $(".field-listing-listingname").addClass("has-error");
        $(".field-listing-listingname .help-block-error").html("Listing Name cannot be blank");
        $(".field-listing-listingname .help-block-error").show();
        $("#listingname").keydown(function () {
            $(".field-listing-listingname").removeClass("has-error");
            $(".field-listing-listingname .help-block-error").html("");
        });
    }
    var descri = $("#description").val();
    if ($.trim(descri) == '') {
        $(".field-listing-description").addClass("has-error");
        $(".field-listing-description .help-block-error").html("Description cannot be blank");
        $(".field-listing-description .help-block-error").show();
        check = 1;
        $("#description").keydown(function () {
            $(".field-listing-description").removeClass("has-error");
            $(".field-listing-description .help-block-error").html("");
        });
    }
    if (bookingavailability == "onetime")
    {
        if (maxstay > diffDays) {
            $(".stayerrcls").html("Maximum stay should be less than no of booking days").css('display', 'block');
            setTimeout(function () {
                $(".stayerrcls").slideUp();
                $('.stayerrcls').html('');
            }, 5000);
            return false;
        }
    }
    if (maxstay == "") {
        $(".stayerrcls").html("Maximum stay cannot be empty").css('display', 'block');
        setTimeout(function () {
            $(".stayerrcls").slideUp();
            $('.stayerrcls').html('');
        }, 5000);
        return false;
    }
    if (minstay > maxstay) {
        $(".stayerrcls").html("Minimum stay should be less than maximum stay").css('display', 'block');
        setTimeout(function () {
            $(".stayerrcls").slideUp();
            $('.stayerrcls').html('');
        }, 5000);
        return false;
    }

    if (check == 1)
    {
        return false;
    }

    if (streetaddress != "" && zipcode != "" && $.trim(listingname) != '' && $.trim(description) != '' && country != "" &&
            $.trim(city) != '' && $.trim(state) != '' && $.trim(zipcode) != '' && $.trim(files) != "" &&
            $.trim(fireextinguisher) != "" && $.trim(firealarm) != "" && $.trim(gasshutoffvalve) != "" && $.trim(emergencyexitinstruction) != "" &&
            $.trim(medicalno) != "" && $.trim(fireno) != "" && $.trim(policeno) != "" && $.trim(nightlyprice) != "" && currency != "") {
        $.ajax({
            type: 'POST',
            url: baseurl + '/user/listing/savelist',
            async: false,
            data: {
                listid: listid,
                hometype: hometype,
                roomtype: roomtype,
                accommodates: accommodates,
                bedrooms: bedrooms,
                beds: beds,
                bathrooms: bathrooms,
                listingname: listingname,
                description: description,
                country: country,
                streetaddress: streetaddress,
                accesscode: accesscode,
                city: city,
                state: state,
                zipcode: zipcode,
                commonamenities: commonamenities,
                additionalamenities: additionalamenities,
                specialfeatures: specialfeatures,
                safetychecklist: safetychecklist,
                fireextinguisher: fireextinguisher,
                firealarm: firealarm,
                gasshutoffvalve: gasshutoffvalve,
                emergencyexitinstruction: emergencyexitinstruction,
                medicalno: medicalno,
                fireno: fireno,
                policeno: policeno,
                nightlyprice: nightlyprice,
                bookingstyle: bookingstyle,
                bookingavailability: bookingavailability,
                startdate: startdate,
                enddate: enddate,
                currency: currency,
                files: files,
                latitude: latitude,
                longitude: longitude,
                houserules: houserules,
                securitydeposit: securitydeposit,
                minstay: minstay,
                maxstay: maxstay
            },
            success: function (data) {
                window.location = data;
            }
        });
    }
}

function updateAminity(aminityType) {
    console.log("Aminity: " + amenities);
    if (amenities == "") {
        amenities = aminityType;
    } else {
        var amenitiesArray = new Array();
        amenitiesArray = amenities.split(',');
        var amenityIndex = amenitiesArray.indexOf(aminityType);
        if (amenityIndex > -1) {
            amenitiesArray.splice(amenityIndex, 1);
        } else {
            amenitiesArray.push(aminityType);
        }
        amenities = amenitiesArray.toString();
    }
    console.log("Aminity: " + amenities);
}

function updateHomeType(homeType) {
    console.log("homeType: " + homeType);
    if (homeTypes == "") {
        homeTypes = homeType;
    } else {
        var homeTypesArray = new Array();
        homeTypesArray = homeTypes.split(',');
        var homeTypeIndex = homeTypesArray.indexOf(homeType);
        if (homeTypeIndex > -1) {
            homeTypesArray.splice(homeTypeIndex, 1);
        } else {
            homeTypesArray.push(homeType);
        }
        homeTypes = homeTypesArray.toString();
    }
    console.log("homeType: " + homeTypes);
}

function updateSearchList(selector, type) {

    var location = $('#where-to-go').val();
    var checkinDate = "";
    var checkoutDate = "";

    /*var guests = "";
     var priceRange = "";
     var bedroom = "";
     var bathroom = "";
     var beds = "";
     var propertytype = "";
     var roomtype = "";
     var price_range = $('#price_range').val();
     value = $(".jslider-value.jslider-value-to > span").html();//alert(value);
     if (value>9999) {//alert("hello");
     $(".jslider-value.jslider-value-to > span").html('10000+');
     }
     else
     {
     $(".jslider-value.jslider-value-to > span").html(value);
     }    */
    checkinDate = $('#check-in').val();
    checkoutDate = $('#check-out').val();
    /*alert(type);
     if(type == 'indate'){
     checkinDate = $(selector).val();
     }else if(type == 'outdate'){
     checkoutDate = $(selector).val();
     }
     alert(checkinDate);*/
    /*else if(type == 'guest'){
     guests = $(selector).val();
     }else if(type == 'price'){
     priceRange = selector;
     }else if(type == 'more'){
     bedroom = $('#bedroom-count').val();
     bathroom = $('#bathroom-count').val();
     beds = $('#beds-count').val();
     }else if(type == 'room_type'){
     roomtype = $(selector).val();
     }else if(type == 'property_type'){
     propertytype = $(selector).val();
     }*/

    $.ajax({
        type: 'POST',
        url: baseurl + '/getsearchupdate',
        data: {
            location: location,
            checkinDate: checkinDate,
            checkoutDate: checkoutDate
                    /*guests: guests,
                     priceRange: priceRange,
                     bedroom: bedroom,
                     bathroom: bathroom,
                     beds: beds,
                     roomtype: roomtype,
                     propertytype: propertytype,
                     priceRange: price_range*/
        },
        success: function (data) {

            $('#search-data').html(data);
            var totalCount = $('.total-count-value').val();
            $('.total-count-detail').html(totalCount);
            $('.split_cell_1').scrollTop(0);
        }
    });
}





function updateSearchListbk(selector, type) {
    console.log("Selector: " + selector + " Type: " + type);
    var lat = $('#place-lat').val();
    var lng = $('#place-lng').val();
    var offset = 0;
    var limit = $('.limit').val();
    var searchType = 0;
    var currentPage = 1;
    var countryid = $("#countryid").val();
    if (searchInitial == 0) {
        searchInitial = 1;
        checkinDate = $('#check-in').val();
        checkoutDate = $('#check-out').val();
        guests = $('#guest-count').val();
        priceRange = $('#price_range').val();
    }

    value = $(".jslider-value.jslider-value-to > span").html();//alert(value);
    if (value > 9999) {//alert("hello");
        $(".jslider-value.jslider-value-to > span").html('10000+');
    } else
    {
        $(".jslider-value.jslider-value-to > span").html(value);
    }

    if (selector == 'pagination') {
        offset = (parseInt(type) - 1) * parseInt(limit);
        currentPage = type;
        searchType = 1;
    }
    //roomTypes = "";bedroom = "";bathroom = "";beds = "";amenities = "";
    if (type == 'indate') {
        checkinDate = $(selector).val();
    } else if (type == 'outdate') {
        checkoutDate = $(selector).val();
    } else if (type == 'guest') {
        guests = $(selector).val();
    } else if (type == 'price') {
        priceRange = selector;
    } else if (type == 'more') {
        bedroom = $('#bedroom-count').val();
        bathroom = $('#bathroom-count').val();
        beds = $('#beds-count').val();
    } else if (type == 'roomtype-checkbox') {
        var selectorDetails = selector.split('-');
        if (roomTypes == "") {
            roomTypes = selectorDetails[1];
        } else {
            var roomTypesArray = new Array();
            roomTypesArray = roomTypes.split(',');
            var roomTypeIndex = roomTypesArray.indexOf(selectorDetails[1]);
            if (roomTypeIndex > -1) {
                roomTypesArray.splice(roomTypeIndex, 1);
            } else {
                roomTypesArray.push(selectorDetails[1]);
            }
            roomTypes = roomTypesArray.toString();
        }
    }


    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/getsearchupdate',
        data: {
            checkinDate: checkinDate,
            checkoutDate: checkoutDate,
            guests: guests,
            priceRange: priceRange,
            bedroom: bedroom,
            bathroom: bathroom,
            amenities: amenities,
            currentPage: currentPage,
            searchType: searchType,
            homeTypes: homeTypes,
            roomTypes: roomTypes,
            offset: offset,
            limit: limit,
            beds: beds,
            lat: lat,
            lng: lng,
            countryid: countryid
        },
        success: function (data) {
            $('#search-data').html(data);
            var totalCount = $('.total-count-value').val();
            $('.total-count-detail').html(totalCount);
            $('.split_cell_1').scrollTop(0);
        }
    });
}

function updateSearchListmobile(selector, type) {
    console.log("Selector: " + selector + " Type: " + type);
    var lat = $('#place-lat').val();
    var lng = $('#place-lng').val();
    var offset = 0;
    var limit = $('.limit').val();
    var searchType = 0;
    var currentPage = 1;
    var countryid = $("#countryid").val();
    if (searchInitial == 0) {
        searchInitial = 1;
        checkinDate = $('#check-in').val();
        checkoutDate = $('#check-out').val();
        guests = $('#guest-count').val();
        priceRange = $('#price_range').val();
    }

    value = $(".jslider-value.jslider-value-to > span").html();//alert(value);
    if (value > 9999) {//alert("hello");
        $(".jslider-value.jslider-value-to > span").html('10000+');
    } else
    {
        $(".jslider-value.jslider-value-to > span").html(value);
    }

    if (selector == 'pagination') {
        offset = (parseInt(type) - 1) * parseInt(limit);
        currentPage = type;
        searchType = 1;
    }
    //roomTypes = "";bedroom = "";bathroom = "";beds = "";amenities = "";
    if (type == 'indate') {
        checkinDate = $(selector).val();
    } else if (type == 'outdate') {
        checkoutDate = $(selector).val();
    } else if (type == 'guest') {
        guests = $(selector).val();
    } else if (type == 'price') {
        priceRange = selector;
    } else if (type == 'more') {
        bedroom = $('#bedroom-count-mobile').val();
        bathroom = $('#bathroom-count-mobile').val();
        beds = $('#beds-count-mobile').val();
    } else if (type == 'roomtype-checkbox') {
        var selectorDetails = selector.split('-');
        if (roomTypes == "") {
            roomTypes = selectorDetails[1];
        } else {
            var roomTypesArray = new Array();
            roomTypesArray = roomTypes.split(',');
            var roomTypeIndex = roomTypesArray.indexOf(selectorDetails[1]);
            if (roomTypeIndex > -1) {
                roomTypesArray.splice(roomTypeIndex, 1);
            } else {
                roomTypesArray.push(selectorDetails[1]);
            }
            roomTypes = roomTypesArray.toString();
        }
    }


    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/getsearchupdate',
        data: {
            checkinDate: checkinDate,
            checkoutDate: checkoutDate,
            guests: guests,
            priceRange: priceRange,
            bedroom: bedroom,
            bathroom: bathroom,
            amenities: amenities,
            currentPage: currentPage,
            searchType: searchType,
            homeTypes: homeTypes,
            roomTypes: roomTypes,
            offset: offset,
            limit: limit,
            beds: beds,
            lat: lat,
            lng: lng,
            countryid: countryid
        },
        success: function (data) {
            $('#search-data').html(data);
            var totalCount = $('.total-count-value').val();
            $('.total-count-detail').html(totalCount);
            $('.split_cell_1').scrollTop(0);
        }
    });
}

function update_currency()
{
    currencyid = $("#currency").val();
    if ($.trim(currencyid) == "") {
        $(".field-listing-currency").addClass("has-error");
        $("#currency").next(".help-block-error").show();
        $("#currency").next(".help-block-error").html("Select any currency");

        $("#currency").change(function () {
            $(".field-listing-currency").removeClass("has-error");
            $("#currency").next(".help-block-error").html("");
        });
        return false;
    }
    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/getcurrencysymbol',
        async: false,
        data: {
            currencyid: currencyid
        },
        success: function (data) {
            $("#currencysymbol").html(data);
        }
    });
}

function start_file_upload()
{
    var inp = document.getElementById('uploadfile');
    uploadedfiles = $("#uploadedfiles").val();

    if (uploadedfiles != "")
    {
        uploaded = jQuery.parseJSON(uploadedfiles);
        uploadedlen = uploaded.length;
    } else
    {
        uploadedlen = 0;
    }
    imagesarr = [];
    var i = 0, len = inp.files.length, img, reader, file;


    remainfiles = parseInt(5) - parseInt(uploadedlen);

    if (len > remainfiles)
    {
        $(".photoerrcls").show();
        $(".photoerrcls").html("You can add only 5 images");
        setTimeout(function () {
            $(".photoerrcls").slideUp();
            $('.photoerrcls').html('');
        }, 5000);
        return false;
    }


    if (len == 0) {
        $(".photoerrcls").show();
        $(".photoerrcls").html("Please Select Image");
        setTimeout(function () {
            $(".photoerrcls").slideUp();
            $('.photoerrcls').html('');
        }, 5000);
        return false;
    }
    formdata = new FormData();
    formdata.append('listingid', $("#listingid").val());
    for (; i < len; i++) {
        file = inp.files[i];

        if (!!file.type.match(/image.*/)) {
            if (window.FileReader) {
                reader = new FileReader();
                reader.onloadend = function (e) {
                    //showUploadedItem(e.target.result, file.fileName);
                };
                reader.readAsDataURL(file);
            }
            if (formdata) {
                formdata.append("images[]", file);
            }
        }
    }

    if (formdata) {
        $.ajax({
            url: baseurl + '/rentals/startfileupload', // point to server-side PHP script 
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#loadingimg").show();
                $("#startuploadbtn").attr("disabled", "true");
            },
            success: function (res) {
                $("#imagenames").html("");
                $("#loadingimg").hide();
                if ($.trim(res) == "error") {
                    $(".photoerrcls").show();
                    $(".photoerrcls").html("File size is large");
                    setTimeout(function () {
                        $(".photoerrcls").slideUp();
                        $('.photoerrcls').html('');
                    }, 5000);
                    $("#startuploadbtn").removeAttr("disabled");
                    $("#uploadfile").val("");
                } else
                {
                    result = res.split("***");
                    inputfiles = $("#uploadedfiles").val();
                    if (inputfiles == "")
                        $("#uploadedfiles").val(result[1]);
                    else
                    {

                        existfiles = $("#uploadedfiles").val();
                        if (existfiles == "[]")
                        {
                            $("#uploadedfiles").val('');
                            /*newfiles = result[1].replace('[',''); 
                             newfiles = result[1].replace(']','');*/
                            $("#uploadedfiles").val(result[1]);
                        } else {
                            newfiles = result[1].replace('[', '');
                            existfiles = existfiles.replace(']', '');
                            $("#uploadedfiles").val(existfiles + ',' + newfiles);
                        }

                    }
                    $("#imagepreview").append(result[0]);
                    $("#startuploadbtn").removeAttr("disabled");
                    $("#uploadfile").val("");
                }
            }
        });
    }
}

function remove_image(org, imgname)
{
    $(org).hide();
    $(org).prev("img").hide();
    $(org).parent().remove();
    uploadedfiles = $("#uploadedfiles").val();
    filesarr = JSON.parse(uploadedfiles);
    filesarr = $.grep(filesarr, function (value) {
        return value != imgname;
    });

    if (filesarr.length >= 1) {
        files = JSON.stringify(filesarr);
        $("#uploadedfiles").val(files);
    } else {
        $("#uploadedfiles").val("");
    }

}

function show_more_amenity()
{
    $("#lessamenity").hide();
    $("#moreamenity").show();
}

function show_less_amenity()
{
    $("#moreamenity").hide();
    $("#lessamenity").show();
}

function initialize() {
    autocomplete = new google.maps.places.Autocomplete((document
            .getElementById('where-to-go')), {
        types: ['geocode']
    });
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        fillInAddress();
    });
}

function fillInAddress() {
    var lat = (document.getElementById('latitude'));
    var place = autocomplete.getPlace();
    var latitude = place.geometry.location.lat();
    var longitude = place.geometry.location.lng();
    $("#latitude").val(latitude);
    $("#longitude").val(longitude);
    $("#place-lat").val(latitude);
    $("#place-lng").val(longitude);
    //console.log("lat: "+latitude+" and long: "+longitude);
}

$(document).on('keyup', '#where-to-go', function (e) {
    if (e.which == 13) {
        var place = $("#where-to-go").val();
        place = place.replace(" ", "-");
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': place}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#place-lat").val(latitude);
                $("#place-lng").val(longitude);
                if ($.trim(latitude) != "" && $.trim(longitude) != "") {
                    //$("#searchcalendardiv").css("display","block");
                    searchlist();
                }
            }
        });
        //searchlist();
    }
});

$(document).on('keyup', '#where-to-go-main', function (e) {
    if (e.which == 13) {
        var place = $("#where-to-go-main").val();
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': place}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#place-lat").val(latitude);
                $("#place-lng").val(longitude);
                if ($.trim(latitude) != "" && $.trim(longitude) != "") {
                    $("#searchcalendardiv").css("display", "block");

                    //searchlist();
                }
            }

        });
        //searchlist();
    }
    var place = $("#where-to-go-main").val();

    if ($.trim(place) == "")
    {
        $("#searchcalendardiv").css("display", "none");

    }
});

$(document).on('change', '#where-to-go-main', function (e) {
    var place = $("#where-to-go-main").val();
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': place}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            $("#place-lat").val(latitude);
            $("#place-lng").val(longitude);
            if ($.trim(latitude) != "" && $.trim(longitude) != "") {
                $("#searchcalendardiv").css("display", "block");


            }
            $("#searchcalendardiv").css("display", "block");

        }
    });
    var place = $("#where-to-go-main").val();
    //alert(place);
    if ($.trim(place) == "")
    {
        $("#searchcalendardiv").css("display", "none");

    }
    //searchlist();
});

$(document).on('keyup', '#where-to-go-mobile', function (e) {
    if (e.which == 13) {

        var place = $("#where-to-go-mobile").val();
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': place}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#place-lat").val(latitude);
                $("#place-lng").val(longitude);
                if ($.trim(latitude) != "" && $.trim(longitude) != "") {
                    $("#searchcalendardiv").css("display", "block");
                    //searchlist();
                }
            }
        });
        //searchlist();
    }
    var place = $("#where-to-go-mobile").val();
    if ($.trim(place) == "")
    {
        $("#searchcalendardiv").css("display", "none");
    }
});

$(document).on('change', '#where-to-go-mobile', function (e) {

    var place = $("#where-to-go-mobile").val();
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': place}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            $("#place-lat").val(latitude);
            $("#place-lng").val(longitude);
            if ($.trim(latitude) != "" && $.trim(longitude) != "") {
                $("#searchcalendardiv").css("display", "block");
                //searchlist();
            }
        }
    });
    var place = $("#where-to-go-mobile").val();
    if ($.trim(place) == "")
    {
        $("#searchcalendardiv").css("display", "none");
    }
    //searchlist();
});

$(document).on('change', '#where-to-go', function (e) {
    var place = $("#where-to-go").val();
    var geocoder = new google.maps.Geocoder();
    place = place.replace(" ", "-");
    geocoder.geocode({'address': place}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            $("#place-lat").val(latitude);
            $("#place-lng").val(longitude);
            console.log(latitude, longitude);
            if ($.trim(latitude) != "" && $.trim(longitude) != "") {
                setTimeout(function () {
                    searchlist();
                }, 2000);

            }
        }
    });
    //searchlist();
});

function searchlist() {
    var place = $("#where-to-go").val();
    var latitude = $("#latitude").val();
    var longitude = $("#longitude").val();
    var checkin = $("#check-in").val();
    var checkout = $("#check-out").val();

    if ((latitude == "" || typeof (latitude) == 'undefined') && (longitude == "" || typeof (longitude) == 'undefined')) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': place}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#latitude").val(latitude);
                $("#longitude").val(longitude);
            }
        });
    }

    latitude = $("#latitude").val();
    longitude = $("#longitude").val();

    if (latitude == "" || typeof (latitude) == 'undefined') {
        latitude = $("#place-lat").val();
    }
    if (longitude == "" || typeof (longitude) == 'undefined') {
        longitude = $("#place-lng").val();
    }

    if (place == "") {
        $('.error-search').html('Enter a valid place');
        $('.error-search').slideDown();
    } else if (latitude == "" || longitude == "") {
        $('.error-search').html('Enter a valid place');
        $('.error-search').slideDown();
    } else if (checkin == "") {
        $('.error-search').html('Enter a check-in date');
        $('.error-search').slideDown();
    } else if (checkout == "") {
        $('.error-search').html('Enter a check-out date');
        $('.error-search').slideDown();
    } else {
        window.location = baseurl + "/search/" + place + "?lat=" + latitude + "&long=" + longitude + "&checkin=" + checkin +
                "&checkout=" + checkout;
    }
    setTimeout(function () {
        $(".error-search").slideUp();
        $('.error-search').html('');
    }, 5000);
}

function searchlistmain() {
    var place = $("#where-to-go-main").val();
    var latitude = $("#latitude").val();
    var longitude = $("#longitude").val();
    var checkin = $("#check-in-main").val();
    var checkout = $("#check-out-main").val();
    if ((latitude == "" || typeof (latitude) == 'undefined') && (longitude == "" || typeof (longitude) == 'undefined')) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': place}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#latitude").val(latitude);
                $("#longitude").val(longitude);
            }
        });
    }

    latitude = $("#latitude").val();
    longitude = $("#longitude").val();

    if (latitude == "" || typeof (latitude) == 'undefined') {
        latitude = $("#place-lat").val();
    }
    if (longitude == "" || typeof (longitude) == 'undefined') {
        longitude = $("#place-lng").val();
    }
    if (guests == "" || typeof (guests) == 'undefined') {
        guests = 1;
    }

    if (place == "") {
        $('.error-search').html('Enter a valid place');
        $('.error-search').slideDown();
    } else if (latitude == "" || longitude == "") {
        $('.error-search').html('Enter a valid place');
        $('.error-search').slideDown();
    } else if (checkin == "") {
        $('.error-search').html('Enter a check-in date');
        $('.error-search').slideDown();
    } else if (checkout == "") {
        $('.error-search').html('Enter a check-out date');
        $('.error-search').slideDown();
    } else {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': place}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#latitude").val(latitude);
                $("#longitude").val(longitude);
            }
            latlngurl = "?lat=" + latitude + "&long=" + longitude;
            if (checkin == "" || typeof (checkin) == 'undefined')
                checkinouturl = "";
            else
                checkinouturl = "&checkin=" + checkin + "&checkout=" + checkout;
            window.location = baseurl + "/search/" + place + latlngurl + checkinouturl;
        });

    }
    setTimeout(function () {
        $(".error-search").slideUp();
        $('.error-search').html('');
    }, 5000);
}

function searchlistmobile() {
    var place = $("#where-to-go-mobile").val();
    var latitude = $("#latitudemobile").val();
    var longitude = $("#longitudemobile").val();
    var checkin = $("#check-in-mobile").val();
    var checkout = $("#check-out-mobile").val();
    if ((latitude == "" || typeof (latitude) == 'undefined') && (longitude == "" || typeof (longitude) == 'undefined')) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': place}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#latitudemobile").val(latitude);
                $("#longitudemobile").val(longitude);
            }
        });
    }

    latitude = $("#latitudemobile").val();
    longitude = $("#longitudemobile").val();

    if (latitude == "" || typeof (latitude) == 'undefined') {
        latitude = $("#place-lat").val();
    }
    if (longitude == "" || typeof (longitude) == 'undefined') {
        longitude = $("#place-lng").val();
    }
    if (place == "") {
        $('.error-searchmobile').html('Enter a valid place');
        $('.error-searchmobile').slideDown();
    } else if (latitude == "" || longitude == "") {
        $('.error-searchmobile').html('Enter a valid place');
        $('.error-searchmobile').slideDown();
    } else if (checkin == "") {
        $('.error-searchmobile').html('Enter a check-in date');
        $('.error-searchmobile').slideDown();
    } else if (checkout == "") {
        $('.error-searchmobile').html('Enter a check-out date');
        $('.error-searchmobile').slideDown();
    } else {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': place}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#latitudemobile").val(latitude);
                $("#longitudemobile").val(longitude);
            }
            latlngurl = "?lat=" + latitude + "&long=" + longitude;
            if (checkin == "" || typeof (checkin) == 'undefined')
                checkinouturl = "";
            else
                checkinouturl = "&checkin=" + checkin + "&checkout=" + checkout;
            window.location = baseurl + "/search/" + place + "?lat=" + latitude + "&long=" + longitude + "&checkin=" + checkin +
                    "&checkout=" + checkout;


        });

    }
    setTimeout(function () {
        $(".error-searchmobile").slideUp();
        $('.error-searchmobile').html('');
    }, 5000);
}

function searchlistmains() {
    var place = $("#where-to-go-main").val();
    var latitude = $("#place-lat").val();
    var longitude = $("#place-lng").val();
    var checkin = $("#check-in-main").val();
    var checkout = $("#check-out-main").val();
    if ((latitude == "" || typeof (latitude) == 'undefined') && (longitude == "" || typeof (longitude) == 'undefined')) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': place}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#latitude").val(latitude);
                $("#longitude").val(longitude);
            }
        });
    }

    latitude = $("#latitude").val();
    longitude = $("#longitude").val();

    if (latitude == "" || typeof (latitude) == 'undefined') {
        latitude = $("#place-lat").val();
    }
    if (longitude == "" || typeof (longitude) == 'undefined') {
        longitude = $("#place-lng").val();
    }
    if (place == "") {
        $('.error-search').html('Enter a valid place');
        $('.error-search').slideDown();
    } else if (latitude == "" || longitude == "") {
        $('.error-search').html('Enter a valid place');
        $('.error-search').slideDown();
    } else if (checkin == "") {
        $('.error-search').html('Enter a check-in date');
        $('.error-search').slideDown();
    } else if (checkout == "") {
        $('.error-search').html('Enter a check-out date');
        $('.error-search').slideDown();
    } else {
        window.location = baseurl + "/search/" + place + "?lat=" + latitude + "&long=" + longitude + "&checkin=" + checkin +
                "&checkout=" + checkout;
    }
    setTimeout(function () {
        $(".error-search").slideUp();
        $('.error-search').html('');
    }, 5000);
}

function showpropertyaddress(event) {
    if (event.checked) {
        $('#property_address').css('display', 'block');
    } else {
        $('#property_address').css('display', 'none');

    }
}

function update_file_name() {
    var inp = document.getElementById('uploadfile');
    uploadedfiles = $("#uploadedfiles").val();
    var i = 0, len = inp.files.length, img, reader, file;
    $("#imagenames").html("");
    formdata = new FormData();
    for (; i < len; i++) {
        file = inp.files[i].name;
        if (i == 0) {
            $("#imagenames").append('<span>' + file + "</span>");
        } else
            $("#imagenames").append(', <span>' + file + "</span>");

    }
}

function send_reserve_request(listid)
{
    stdates = $("#startdate").val();
    stdate = new Date(stdates);
    eddates = $("#enddate").val();
    eddate = new Date(eddates);
    guests = $("#guests").val();


    if ($.trim(stdates) == "") {
        $("#maxstayerr").show();
        $("#maxstayerr").html("Select Start Date");
        $("#startdate").change(function () {
            $("#maxstayerr").hide();
            $("#maxstayerr").html("");
        });
        return false;
    }

    if ($.trim(eddates) == "") {
        $("#maxstayerr").show();
        $("#maxstayerr").html("Select End Date");
        $("#enddate").change(function () {
            $("#maxstayerr").hide();
            $("#maxstayerr").html("");
        });
        return false;
    }


    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    commissionamount = $("#commissionprice").val();
    /*sitecharge = $("#siteprice").html();
     taxamount = $("#taxprice").html();*/
    $.ajax({
        url: baseurl + '/sendrequest',
        type: "post",
        dataType: "html",
        data: {
            listid: listid,
            days: diffDays,
            sdate: stdates,
            edate: eddates,
            commissionamount: commissionamount,
            guests: guests
        },
        beforeSend: function () {
            $("#paypalloadingimg").show();
        },
        success: function (responce) {
            //alert(responce);
            //return false;
            if ($.trim(responce) == "error")
            {
                $("#paypalloadingimg").hide();
                $("#emailverifyerr").show();
                $("#emailverifyerr").html("You need to add your paypal id and need to verify your email id before booking");
                setTimeout(function () {
                    $("#emailverifyerr").hide();
                }, 5000);
            }
            if ($.trim(responce) == "emailerror")
            {
                $("#paypalloadingimg").hide();
                $("#emailverifyerr").show();
                $("#emailverifyerr").html("You need to verify your email id before booking");
                setTimeout(function () {
                    $("#emailverifyerr").hide();
                }, 5000);
            }
            if ($.trim(responce) == "paypalerror")
            {
                $("#paypalloadingimg").hide();
                $("#emailverifyerr").show();
                $("#emailverifyerr").html("You need to add your paypal id before booking");
                setTimeout(function () {
                    $("#emailverifyerr").hide();
                }, 5000);
            }
            if ($.trim(responce) == "EnquiryError")
            {
                $("#paypalloadingimg").hide();
                $("#emailverifyerr").show();
                $("#emailverifyerr").html("You are not allowed to send more than 3 requests on same dates");
                setTimeout(function () {
                    $("#emailverifyerr").hide();
                }, 5000);
            } else
            {
                //alert(responce);
                window.location = baseurl + '/request/booking/' + responce;
                /*$('.payment-form').html(responce);
                 $('.payment-form').submit();*/
            }
        },
    });


}

function instant_book(listid)
{
    stdates = $("#startdate").val();
    stdate = new Date(stdates);
    eddates = $("#enddate").val();
    eddate = new Date(eddates);
    guests = $("#guests").val();


    if ($.trim(stdates) == "") {
        $("#maxstayerr").show();
        $("#maxstayerr").html("Select Start Date");
        $("#startdate").change(function () {
            $("#maxstayerr").hide();
            $("#maxstayerr").html("");
        });
        return false;
    }

    if ($.trim(eddates) == "") {
        $("#maxstayerr").show();
        $("#maxstayerr").html("Select End Date");
        $("#enddate").change(function () {
            $("#maxstayerr").hide();
            $("#maxstayerr").html("");
        });
        return false;
    }


    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    commissionamount = $("#commissionprice").val();
    /*sitecharge = $("#siteprice").html();
     taxamount = $("#taxprice").html();*/
    $.ajax({
        url: baseurl + '/instantbook',
        type: "post",
        dataType: "html",
        data: {
            listid: listid,
            days: diffDays,
            sdate: stdates,
            edate: eddates,
            commissionamount: commissionamount,
            guests: guests
        },
        beforeSend: function () {
            $("#paypalloadingimg").show();
        },
        success: function (responce) {
            if ($.trim(responce) == "error")
            {
                $("#paypalloadingimg").hide();
                $("#emailverifyerr").show();
                $("#emailverifyerr").html("You need to add your paypal id and need to verify your email id before booking");
                setTimeout(function () {
                    $("#emailverifyerr").hide();
                }, 5000);
            }
            if ($.trim(responce) == "emailerror")
            {
                $("#paypalloadingimg").hide();
                $("#emailverifyerr").show();
                $("#emailverifyerr").html("You need to verify your email id before booking");
                setTimeout(function () {
                    $("#emailverifyerr").hide();
                }, 5000);
            }
            if ($.trim(responce) == "paypalerror")
            {
                $("#paypalloadingimg").hide();
                $("#emailverifyerr").show();
                $("#emailverifyerr").html("You need to add your paypal id before booking");
                setTimeout(function () {
                    $("#emailverifyerr").hide();
                }, 5000);
            } else
            {
                window.location = baseurl + '/checkout';
                /*$('.payment-form').html(responce);
                 $('.payment-form').submit();*/
            }
        },
    });


}













function send_reserve_request_mobile(listid)
{
    stdates = $("#startdatemobile").val();
    stdate = new Date(stdates);
    eddates = $("#enddatemobile").val();
    eddate = new Date(eddates);
    guests = $("#guestsmobile").val();


    if ($.trim(stdates) == "") {
        $("#maxstayerrmobile").show();
        $("#maxstayerrmobile").html("Select Start Date");
        $("#startdatemobile").change(function () {
            $("#maxstayerrmobile").hide();
            $("#maxstayerrmobile").html("");
        });
        return false;
    }

    if ($.trim(eddates) == "") {
        $("#maxstayerrmobile").show();
        $("#maxstayerrmobile").html("Select End Date");
        $("#enddatemobile").change(function () {
            $("#maxstayerrmobile").hide();
            $("#maxstayerrmobile").html("");
        });
        return false;
    }


    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    commissionamount = $("#commissionpricemobile").val();
    sitecharge = $("#sitepricemobile").html();
    taxamount = $("#taxpricemobile").html();
    $.ajax({
        url: baseurl + '/user/listing/sendrequest',
        type: "post",
        dataType: "html",
        data: {
            listid: listid,
            days: diffDays,
            sdate: stdates,
            edate: eddates,
            commissionamount: commissionamount,
            sitecharge: sitecharge,
            taxamount: taxamount,
            guests: guests
        },
        beforeSend: function () {
            $("#paypalloadingimgmobile").show();
        },
        success: function (responce) {
            if ($.trim(responce) == "error")
            {
                $("#paypalloadingimgmobile").hide();
                $("#emailverifyerrmobile").show();
                $("#emailverifyerrmobile").html("You need to add your paypal id and need to verify your email id before booking");
                setTimeout(function () {
                    $("#emailverifyerrmobile").hide();
                }, 5000);
            }
            if ($.trim(responce) == "emailerror")
            {
                $("#paypalloadingimgmobile").hide();
                $("#emailverifyerrmobile").show();
                $("#emailverifyerrmobile").html("You need to verify your email id before booking");
                setTimeout(function () {
                    $("#emailverifyerrmobile").hide();
                }, 5000);
            }
            if ($.trim(responce) == "paypalerror")
            {
                $("#paypalloadingimgmobile").hide();
                $("#emailverifyerrmobile").show();
                $("#emailverifyerrmobile").html("You need to add your paypal id before booking");
                setTimeout(function () {
                    $("#emailverifyerrmobile").hide();
                }, 5000);
            } else
            {
                $('.payment-form').html(responce);
                $('.payment-form').submit();
            }
        },
    });


}

function show_requestpopup() {
    $(".pos_abs.make_fix").css("display", "block");
    $(".pos_abs.make_fix").css("addclass", "fixed");
    $(".mobileviewreq").css("display", "block");
    $(".requestbtnmobile").hide();
}
function closereqpopup() {
    $(".pos_abs.make_fix").css("display", "none");
    $(".mobileviewreq").css("display", "none");
    $(".requestbtnmobile").show();
}

function change_reserve_status(resstatus, reserveid)
{
    if ($.trim(resstatus) == "cancel" || $.trim(resstatus) == 'accept' || $.trim(resstatus) == 'decline') {
        if (confirm("Are you sure you want to " + resstatus + " this trip ? ")) {
            $.ajax({
                url: baseurl + '/user/listing/changereservestatus', // point to server-side PHP script 
                type: "POST",
                dataType: "html",
                data: {
                    resstatus: resstatus,
                    reserveid: reserveid
                },
                success: function (res) {
                    $("#reserve_" + reserveid).remove();
                }
            });
        }

    } else {
        $.ajax({
            url: baseurl + '/user/listing/changereservestatus', // point to server-side PHP script 
            type: "POST",
            dataType: "html",
            data: {
                resstatus: resstatus,
                reserveid: reserveid
            },
            success: function (res) {
                $("#reserve_" + reserveid).remove();
                $("#reserve_" + reserveid).hide();
            }
        });

    }
}

function claim_securityfee(tripid, claimby)
{
    if (confirm("Are you sure you want to initiate claim? ")) {
        $.ajax({
            type: 'POST',
            url: baseurl + '/user/listing/claimsecurityfee',
            async: false,
            data: {
                tripid: tripid,
                claimby: claimby
            },
            success: function (data) {
                $("#reserveid").val(data);
                $("#claimbtn").hide();
                $("#sendbtn").removeAttr('disabled');
                $("#claimsuccess").show();
                setTimeout(function () {
                    $("#claimsuccess").hide();
                }, 5000);
                window.location.reload();
            }
        });
    }
}

function change_receiver_status(reserveid, status)
{
    if (status == "accepted") {
        statusmsg = "accept";
    } else if (status == "declined")
    {
        statusmsg = "decline";
    } else if (status == "solved")
    {
        statusmsg = "solve";
    }
    if (confirm("Are you sure you want to " + statusmsg + " claim? ")) {
        $.ajax({
            type: 'POST',
            url: baseurl + '/user/listing/changereceiverstatus',
            async: false,
            data: {
                reserveid: reserveid,
                status: status
            },
            success: function (data) {
                $("#acceptbtn").hide();
                $("#declinebtn").hide();
                if (status == "declined") {
                    $("#involvebtn").show();
                } else if (status == "solved")
                {
                    $("#solvediv").hide();
                    $("#solvebtn").hide();
                    $("#acceptbtn").hide();
                } else
                {
                    $("#solvediv").hide();
                    $("#solvebtn").hide();
                    $("#acceptbtn").hide();
                }
                window.location.reload();
            }
        });
    }
}

function change_claim_status(claimid, status)
{
    if (confirm("Are you sure you want to solve this claim? ")) {
        $.ajax({
            type: 'POST',
            url: baseurl + '/user/listing/changeclaimstatus',
            async: false,
            data: {
                claimid: claimid,
                status: status
            },
            success: function (data) {
                $("#solvediv").hide();
                $("#solvebtn").hide();
                window.location.reload();
            }
        });
    }
}

function show_basics_div(org)
{
    $(".commcls").hide();
    $("#basicsdiv").show();
    $("#listpropul").find("li").css("background", 'none');
    $(org).css('background', '#ddd');
}

function show_description_div(org)
{
    $(".commcls").hide();
    $("#descriptiondiv").show();
    $("#listpropul").find("li").css("background", 'none');
    $(org).css('background', '#ddd');
}

function show_location_div(org)
{
    $(".commcls").hide();
    $("#locationdiv").show();
    $("#listpropul").find("li").css("background", 'none');
    $(org).css('background', '#ddd');
}

function show_amenities_div(org)
{
    $(".commcls").hide();
    $("#amenitiesdiv").show();
    $("#listpropul").find("li").css("background", 'none');
    $(org).css('background', '#ddd');
}

function show_photos_div(org)
{
    $(".commcls").hide();
    $("#photosdiv").show();
    $("#listpropul").find("li").css("background", 'none');
    $(org).css('background', '#ddd');
}

function show_homesafety_div(org)
{
    $(".commcls").hide();
    $("#safetydiv").show();
    $("#listpropul").find("li").css("background", 'none');
    $(org).css('background', '#ddd');
}

function show_pricing_div(org)
{
    $(".commcls").hide();
    $("#pricediv").show();
    $("#listpropul").find("li").css("background", 'none');
    $(org).css('background', '#ddd');
}

function show_booking_div(org)
{
    $(".commcls").hide();
    $("#bookingdiv").show();
    $("#listpropul").find("li").css("background", 'none');
    $(org).css('background', '#ddd');
}

function show_calendar_div(org)
{
    $(".commcls").hide();

    if ($('#bookdate').css('display') != 'block')
    {
        $("#bookavailability").show();
    }
    $("#calendardiv").show();
    $("#listpropul").find("li").css("background", 'none');
    $(org).css('background', '#ddd');
}

function send_claim_message()
{
    messages = $("#claimmessage").val();
    tripid = $("#reserveid").val();
    userid = $("#userid").val();
    hostid = $("#hostid").val();

    if ($.trim(messages) == "") {
        $(".claimerrcls").show();
        $(".claimerrcls").html("Enter Claim Message");
        $("#claimmessage").keydown(function () {
            $(".claimerrcls").hide();
            $(".claimerrcls").html("");
        });
        return false;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/sendclaimmessage',
        async: false,
        data: {
            tripid: tripid,
            messages: messages,
            userid: userid,
            hostid: hostid,
        },
        beforeSend: function () {
            $("#loadingimg").show();
        },
        success: function (data) {
            $("#loadingimg").hide();
            $("#claimmessage").val("");
            getclaimmessage();
        }
    });
}


function approvedecline(status, enquiryid)
{
    $("#statuschange").css('display', 'block');
    if (status == "")
    {
        alert("Choose Atleast any one");
        window.location.reload();
        return false;
    }
    $.ajax({
        type: 'POST',
        url: baseurl + '/jobStatusUpdate',
        //async: false,
        data: {
            status: status,
            enquiryid: enquiryid
        },
        success: function (data) {
            if (data == 'success') {
                /* $("#approveerr").css('display', 'none');
                 $("#approveerr").html("");*/
                alert("Successfully updated");
                $("#statuschange").css('display', 'none');
                window.location.reload();
            } else {
                /*  $("#approveerr").css('display', 'block');
                 $("#approveerr").html("Already you Approved one Request which is synced with same date");
                 */
                alert("Already you Approved one Request which is synced with same date");
                $("#statuschange").css('display', 'none');
                //window.location.reload();
            }

        }
    });
}





function involve_admin(claimid)
{
    if (confirm("Are you sure you want to invlove admin in this claim? ")) {
        $.ajax({
            type: 'POST',
            url: baseurl + '/user/listing/involveadmin',
            async: false,
            data: {
                claimid: claimid
            },
            success: function (data) {
                $("#involvebtn").hide();
                window.location.reload();
            }
        });
    }
}

function save_lists()
{
    listarr = [];
    listingid = $("#listingid").val();
    $(".whitehrt").each(function () {
        iconclass = $(this).attr("class");
        selfind = iconclass.indexOf("redhrt");
        if (selfind > 0)
        {
            listid = $(this).attr("id");
            listarr.push(listid);
        } else
        {
            listid = $(this).attr("id");
        }
    });
    if (listarr == "") {
        alert("Please select any existing wishlist or create a new one");
        listarr = ["0"];
    } else {
        $.ajax({
            type: 'POST',
            url: baseurl + '/savewishlists',
            async: false,
            data: {
                listarr: listarr,
                listingid: listingid
            },
            success: function (data) {
                $('#myModal').modal('toggle');
            }
        });
    }
}

function showContainer(show_id, obj) {
    var check = $("#field_changed").val();
    if (check == "1") {
        alert("Kindly save your changes first and than proceed to next step");
    } else {
        $(".left_buttons").find("button").css("cssText", "background-color: #ccc !important;");
        $(obj).css("cssText", "background-color: #fe5771 !important;");
        $(".panel-default").hide();
        $("#" + show_id).show();
        $("#" + show_id).find(".panel-default").show();
        $("#emergency_contact").show();
        $("#current_page").val(show_id);
    }
}

function showSchoolHouseDiv(val, tab_value) {
    $(".school_houses_tabs").hide();
    $("#paid_service_tab").val(tab_value);
    if (val == 1) {
        $("#basics").css('display', 'block');
        $("#basicmenu").css("cssText", "background-color: #fe5771 !important;");
        $("#extramenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#propertymenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#propertyphotomenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#familyphotomenu").css("cssText", "background-color: #4d4d4d !important;");
    } else if (val == 2) {
        $("#extra").css('display', 'block');
        $("#basicmenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#extramenu").css("cssText", "background-color: #fe5771 !important;");
        $("#propertymenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#propertyphotomenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#familyphotomenu").css("cssText", "background-color: #4d4d4d !important;");
    } else if (val == 3) {
        $("#property").css('display', 'block');
        $("#basicmenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#extramenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#propertymenu").css("cssText", "background-color: #fe5771 !important;");
        $("#propertyphotomenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#familyphotomenu").css("cssText", "background-color: #4d4d4d !important;");
    } else if (val == 4) {
        $("#propertyphoto").css('display', 'block');
        $("#basicmenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#extramenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#propertymenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#propertyphotomenu").css("cssText", "background-color: #fe5771 !important;");
        $("#familyphotomenu").css("cssText", "background-color: #4d4d4d !important;");
    } else if (val == 5) {
        $("#familyphoto").css('display', 'block');
        $("#basicmenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#extramenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#propertymenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#propertyphotomenu").css("cssText", "background-color: #4d4d4d !important;");
        $("#familyphotomenu").css("cssText", "background-color: #fe5771 !important;");
    }
}

function showrelevantdiv(value)
{
    $('#orgdiv').css('display', 'none');
    $('#studentdiv').css('display', 'none');
    $('#parentdiv').css('display', 'none');

    $('#role').val(value);
    /* $("#orgname").prop('required',false);
     $("#aboutorg").prop('required',false);
     $("#legal_nature").prop('required',false);
     $("#orghr").prop('required',false);
     $("#orgemail").prop('required',false);
     $("#orgphone").prop('required',false);
     $("#why_fit").prop('required',false);
     $("#tutorrelation").prop('required',false);
     
     
     
     $("#orgaddress").prop('required',false);
     $("#street_numberorg").prop('required',false);
     $("#routeorg").prop('required',false);
     $("#localityorg").prop('required',false);
     $("#administrative_area_level_1org").prop('required',false);
     $("#postal_codeorg").prop('required',false);
     $("#countryorg").prop('required',false);
     $("#parentaddress").prop('required',false);
     $("#street_numberp").prop('required',false);
     $("#routep").prop('required',false);
     $("#localityp").prop('required',false);
     $("#administrative_area_level_1p").prop('required',false);
     $("#postal_codep").prop('required',false);
     $("#countryp").prop('required',false);
     
     $("#sraddress16").prop('required',false);
     $("#street_numbersr16").prop('required',false);
     $("#routesr16").prop('required',false);
     $("#localitysr16").prop('required',false);
     $("#administrative_area_level_1sr16").prop('required',false);
     $("#postal_codesr16").prop('required',false);
     $("#countrysr16").prop('required',false);
     
     $("#over18raddress").prop('required',false);
     $("#street_numbersr18").prop('required',false);
     $("#routesr18").prop('required',false);
     $("#localitysr18").prop('required',false);
     $("#administrative_area_level_1sr18").prop('required',false);
     $("#postal_codesr18").prop('required',false);
     $("#countrysr18").prop('required',false);*/
    if (value == 4)
    {
        $('#orgdiv').css('display', 'block');
        $('#studentdiv').css('display', 'none');
        $('#parentdiv').css('display', 'none');

        $("#orgbtn").css("cssText", "background-color: #fe5771 !important;");
        $("#stubtn").css("cssText", "background-color: #ccc !important;");
        $("#parbtn").css("cssText", "background-color: #ccc !important;");

        /* $("#orgname").prop('required',true);
         $("#aboutorg").prop('required',true);
         $("#legal_nature").prop('required',true);
         $("#orghr").prop('required',true);
         $("#orgemail").prop('required',true);
         $("#orgphone").prop('required',true);
         $("#why_fit").prop('required',true);
         $("#tutorrelation").prop('required',true);
         
         
         $("#orgaddress").prop('required',true);
         $("#street_numberorg").prop('required',true);
         $("#routeorg").prop('required',true);
         $("#localityorg").prop('required',true);
         $("#administrative_area_level_1org").prop('required',true);
         $("#postal_codeorg").prop('required',true);
         $("#countryorg").prop('required',true);*/

    } else if (value == 5) //parent
    {
        $('#orgdiv').css('display', 'none');
        $('#studentdiv').css('display', 'none');
        $('#parentdiv').css('display', 'block');
        $("#orgbtn").css("cssText", "background-color: #ccc !important;");
        $("#stubtn").css("cssText", "background-color: #ccc !important;");
        $("#parbtn").css("cssText", "background-color: #fe5771 !important;");
        /* Address*/
        /* $("#parentaddress").prop('required',true);
         $("#street_numberp").prop('required',true);
         $("#routep").prop('required',true);
         $("#localityp").prop('required',true);
         $("#administrative_area_level_1p").prop('required',true);
         $("#postal_codep").prop('required',true);
         $("#countryp").prop('required',true);*/

        /* Student Address*/
        /* $("#sraddress16").prop('required',true);
         $("#street_numbersr16").prop('required',true);
         $("#routesr16").prop('required',true);
         $("#localitysr16").prop('required',true);
         $("#administrative_area_level_1sr16").prop('required',true);
         $("#postal_codesr16").prop('required',true);
         $("#countrysr16").prop('required',true);*/

        /* Student16 Information*/
        /*$("#sname16").prop('required',true);
         $("#ssurname16").prop('required',true);
         $("#bmonth16").prop('required',true);
         $("#bday16").prop('required',true);
         $("#byear16").prop('required',true);
         $("#pob16").prop('required',true);
         $("#semail16").prop('required',true);
         $("#sphone16").prop('required',true);
         $("#nin16").prop('required',true);
         $("#family_notes").prop('required',true);
         $("#tutor_notes").prop('required',true);
         $("#profile-about16").prop('required',true);
         $("#learning_condition16").prop('required',true);
         $("#school_name16").prop('required',true);
         $("#class_letter16").prop('required',true);
         $("#class_number16").prop('required',true);
         $("#student_number16").prop('required',true);
         $("#academic_qualifications16").prop('required',true);
         $("#date_attain16").prop('required',true);
         $("#school_tutor_name16").prop('required',true);
         $("#school_tutor_email16").prop('required',true);
         $("#school_tutor_number16").prop('required',true);*/

    } else if (value == 6) //student
    {
        $('#orgdiv').css('display', 'none');
        $('#studentdiv').css('display', 'block');
        $('#parentdiv').css('display', 'none');
        $("#orgbtn").css("cssText", "background-color: #ccc !important;");
        $("#stubtn").css("cssText", "background-color: #fe5771 !important;");
        $("#parbtn").css("cssText", "background-color: #ccc !important;");
        /* Address*/
        /*$("#over18raddress").prop('required',true);
         $("#street_numbersr18").prop('required',true);
         $("#routesr18").prop('required',true);
         $("#localitysr18").prop('required',true);
         $("#administrative_area_level_1sr18").prop('required',true);
         $("#postal_codesr18").prop('required',true);
         $("#countrysr18").prop('required',true);
         
         $("#profile-about").prop('required',true);
         $("#learning_condition").prop('required',true);
         $("#school_name").prop('required',true);
         $("#class_letter").prop('required',true);
         $("#class_number").prop('required',true);
         $("#student_number").prop('required',true);
         $("#academic_qualifications").prop('required',true);
         $("#date_attain").prop('required',true);
         $("#school_tutor_name").prop('required',true);
         $("#school_tutor_email").prop('required',true);
         $("#school_tutor_number").prop('required',true);*/
    }

}

function showstudentDomicile(event)
{
    if (event.checked) {
        $('#s18domicilediv').css('display', 'block');
    } else {
        $('#s18domicilediv').css('display', 'none');

    }
}

function showparentdomicile(event)
{
    if (event.checked) {
        $('#pdomicilediv').css('display', 'block');


    } else {
        $('#pdomicilediv').css('display', 'none');

    }
}
function showstudent16domicilediv(event)
{
    if (event.checked) {
        $('#s16domicilediv').css('display', 'block');

    } else {
        $('#s16domicilediv').css('display', 'none');

    }
}



function showaddresdiv(event)
{
    if (event.checked) {
        $('#sdomicilediv').css('display', 'block');
    } else {
        $('#sdomicilediv').css('display', 'none');
    }
}




function showstaddresdiv(event)
{
    if (event.checked) {
        $('#stdomicilediv').css('display', 'block');
    } else {
        $('#stdomicilediv').css('display', 'none');
    }
}
function create_new_list()
{
    newlistname = $("#newlistname").val();
    newlistselector = "#newlistname";
    newlistliselector = "#listsdiv";
    if ($.trim(newlistname) == "") {
        newlistname = $("#newlistname1").val();
        newlistselector = "#newlistname1";
        newlistliselector = "#listsdiv1";
    }
    if ($.trim(newlistname) == "") {
        $(".listerr").show();
        $(".listerr").html("Enter List Name");
        $(newlistselector).keydown(function () {
            $(".listerr").hide();
            $(".listerr").html("");
        });
        return false;
    }
    $.ajax({
        type: 'POST',
        url: baseurl + '/createnewlist',
        async: false,
        data: {
            newlistname: newlistname
        },
        success: function (data) {
            if ($.trim(data) == "exists") {
                $(".listerr").show();
                $(".listerr").html("List name already exists");
                $(newlistselector).keydown(function () {
                    $(".listerr").hide();
                    $(".listerr").html("");
                });
                $(newlistselector).val("");
                return false;
            } else {
                newlist = '<li class="bg_white padding10 wishli">' + newlistname + '<div style="float:right;"><i class="fa fa-heart-o whitehrt redhrt" id="' + data + '"></i></div></li>';
                $(newlistliselector).append(newlist).trigger("create");
                $(newlistselector).val("");
            }
        }
    });
}

function remove_wish_list(wishlistid, propertyId)
{
    $.ajax({
        type: 'POST',
        url: baseurl + '/removewishlist',
        async: false,
        data: {
            wishlistid: wishlistid,
            propertyId: propertyId,
        },
        success: function (data) {
            $("#wish" + wishlistid).hide();
            window.location.reload();
        }
    });
}

function edit_list_name(listid)
{
    listname = $("#listname").val();
    wishlisturl = $("#editwishlisturl").val();
    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/editlistname',
        async: false,
        data: {
            listid: listid,
            listname: listname
        },
        beforeSend: function () {
            $("#loadingimg").show();
        },
        success: function (data) {
            $("#loadingimg").hide();
            $("#listname").val(listname);
            window.location = wishlisturl;
        }
    });
}

function send_contact_message()
{
    senderid = $("#sender").val();
    receiverid = $("#receiver").val();
    messages = $("#contactmessage").val();
    listingid = $("#enqid").val();
    proid = $("#proid").val();
    bno = $("#bno").val();


    $("#send_msg").attr('data-dismiss', '');
    if ($.trim(messages) == "") {
        $(".msgerrcls").show();
        $(".msgerrcls").html("Enter Contact Message");
        $("#contactmessage").keydown(function () {
            $(".msgerrcls").hide();
            $(".msgerrcls").html("");
        });
        return false;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + '/sendcontactmessage',
        async: false,
        data: {
            senderid: senderid,
            receiverid: receiverid,
            messages: messages,
            listingid: listingid,
            proid: proid,
            bno: bno
        },
        beforeSend: function () {
            $("#loadingimg").show();
        },
        success: function (data) {

            alert('message send successfuly');
            window.location.reload()
            /*$('#charNum').html('250'); //contact seller charater count
             $("#loadingimg").hide();
             $("#succmsg").show();
             $("#contactmessage").val("");
             $("#send_msg").attr('data-dismiss','modal');
             setTimeout(function() {
             $("#succmsg").hide();
             },500);
             getcontactmessage();*/


        }
    });
}

function change_message_type()
{
    msgtype = $("#selmessage").val();
    $.ajax({
        type: 'POST',
        url: baseurl + '/getmessages',
        async: false,
        data: {
            msgtype: msgtype
        },
        success: function (data) {
            $("#inboxdiv").html(data);
            //window.location.reload();
        }
    });
}
function sendtrustmail() {
    email = $("#loguseridemail").val();
    $.ajax({
        type: 'POST',
        url: baseurl + '/sendtrustmail',
        async: false,
        data: {
            email: email
        },
        beforeSend: function () {
            $("#loadingimg").show();
        },
        success: function (data) {
            if (data == 'success') {
                $("#loadingimg").hide();
                $("#succmsg").html("Email Sent Successfully.");
                setTimeout(function () {
                    $("#succmsg").hide();
                }, 1000);
            }
        }
    });
}

function edit_profile_basic() {
    $("#submiterr").hide();
    $("#submiterr").html("");
    var role = $("#role").val();
    var check = 0;
    if ($("#profile-firstname").val() == "") {
        $("#fnerr").show();
        $("#fnerr").html("First Name should not be blank");
        $("#profile-firstname").keydown(function () {
            $("#fnerr").hide();
            $("#fnerr").html("");
        });
        check = 1;
    }

    if ($("#profile-lastname").val() == "") {
        $("#lnerr").show();
        $("#lnerr").html("Last Name should not be blank");
        $("#profile-lastname").keydown(function () {
            $("#lnerr").hide();
            $("#lnerr").html("");
        });
        check = 1;
    }

    gender = $("#profile-gender option:selected").val();
    if ($.trim(gender) == "") {
        $("#generr").show();
        $("#generr").html("Select Gender");
        $("#profile-gender").change(function () {
            $("#generr").hide();
            $("#generr").html("");
        });
        check = 1;
    }


    bmonth = $("#bmonth option:selected").val();
    if ($.trim(bmonth) == "") {
        $("#doberr").show();
        $("#doberr").html("Select Month");
        $("#bmonth").change(function () {
            $("#doberr").hide();
            $("#doberr").html("");
        });
        check = 1;
    }


    bday = $("#bday option:selected").val();
    if ($.trim(bday) == "") {
        $("#doberr").show();
        $("#doberr").html("Select Day");
        $("#bday").change(function () {
            $("#doberr").hide();
            $("#doberr").html("");
        });
        check = 1;
    }


    byear = $("#byear option:selected").val();
    if ($.trim(byear) == "") {
        $("#doberr").show();
        $("#doberr").html("Select Year");
        $("#byear").change(function () {
            $("#doberr").hide();
            $("#doberr").html("");
        });
        check = 1;
    }


    if ($.trim($("#basicpob").val()) == "") {
        $("#basicpoberr").show();
        $("#basicpoberr").html("Place of birth should not be empty");
        $("#basicpob").keydown(function () {
            $("#basicpoberr").hide();
            $("#basicpoberr").html("");
        });
        check = 1;
    }

    country_id = $("#country_id option:selected").val();
    if ($.trim(country_id) == "") {
        $("#countryerr").show();
        $("#countryerr").html("Select Citizenship");
        $("#country_id").change(function () {
            $("#countryerr").hide();
            $("#countryerr").html("");
        });
        check = 1;
    }


    if ($.trim($("#phonenumber").val()) == "") {
        $("#phonenumbererr").show();
        $("#phonenumbererr").html("Place Enter Phonenumber");
        $("#phonenumber").keydown(function () {
            $("#phonenumbererr").hide();
            $("#phonenumbererr").html("");
        });
        check = 1;
    }

    if ($.trim($("#nin18").val()) == "") {
        $("#nin18err").show();
        $("#nin18err").html("Place Enter NIN ");
        $("#nin18").keydown(function () {
            $("#nin18err").hide();
            $("#nin18err").html("");
        });
        check = 1;
    }
    if (check == 1) {
        $("#submiterr").show();
        $("#submiterr").html("Please correct all validation errors: check you have not put text (i.e. a symbol like - or an empty space) where there is supposed to be a number or anything else but the email in its field (if you copy-pasted it you may have included a space)");
        return false;
    } else {
        return true;
    }
}

function edit_profile_studentguest() {
    $("#submiterr").hide();
    $("#submiterr").html("");
    var role = $("#role").val();
    var check = 0;
    if ($.trim($("#profile-about").val()) == "") {
        $("#profile-abouterr").show();
        $("#profile-abouterr").html("Place Enter About Yourself");
        $("#profile-about").keydown(function () {
            $("#profile-abouterr").hide();
            $("#profile-abouterr").html("");
        });
        check = 1;
    }

    if ($.trim($("#learning_condition").val()) == "") {
        $("#learning_conditionerr").show();
        $("#learning_conditionerr").html("Place Enter Learning Condition");
        $("#learning_condition").keydown(function () {
            $("#learning_conditionerr").hide();
            $("#learning_conditionerr").html("");
        });
        check = 1;
    }
    if ($.trim($("#family_notes18").val()) == "") {
        $("#family_notes18err").show();
        $("#family_notes18err").html("Place Enter Notes to host family");
        $("#family_notes18").keydown(function () {
            $("#family_notes18err").hide();
            $("#family_notes18err").html("");
        });
        check = 1;
    }



    if ($.trim($("#tutor_notes18").val()) == "") {
        $("#tutor_notes18err").show();
        $("#tutor_notes18err").html("Place Enter Notes to tutor");
        $("#tutor_notes18").keydown(function () {
            $("#tutor_notes18err").hide();
            $("#tutor_notes18err").html("");
        });
        check = 1;
    }
    school_name = $("#school_name option:selected").val();
    if ($.trim(school_name) == "") {
        $("#school_nameerr").show();
        $("#school_nameerr").html("Select Legal Nature");
        $("#school_name").change(function () {
            $("#school_nameerr").hide();
            $("#school_nameerr").html("");
        });
        check = 1;
    }

    if ($.trim($("#class_letter").val()) == "") {
        $("#class_lettererr").show();
        $("#class_lettererr").html("Place Enter ClassLetter");
        $("#class_letter").keydown(function () {
            $("#class_lettererr").hide();
            $("#class_lettererr").html("");
        });
        check = 1;
    }
    if ($.trim($("#class_number").val()) == "") {
        $("#class_numbererr").show();
        $("#class_numbererr").html("Place Enter ClassNumber");
        $("#class_number").keydown(function () {
            $("#class_numbererr").hide();
            $("#class_numbererr").html("");
        });
        check = 1;
    }
//    if ($.trim($("#student_number").val()) == "") {
//        $("#student_numbererr").show();
//        $("#student_numbererr").html("Place Enter Student Number");
//        $("#student_number").keydown(function () {
//            $("#student_numbererr").hide();
//            $("#student_numbererr").html("");
//        });
//        check = 1;
//    }
    if ($.trim($("#academic_qualifications").val()) == "") {
        $("#academic_qualificationserr").show();
        $("#academic_qualificationserr").html("Place Enter Academic Qualifications");
        $("#academic_qualifications").keydown(function () {
            $("#academic_qualificationserr").hide();
            $("#academic_qualificationserr").html("");
        });
        check = 1;
    }
    if ($.trim($("#date_attain").val()) == "") {
        $("#date_attainerr").show();
        $("#date_attainerr").html("Place Enter DateAttain");
        $("#date_attain").keydown(function () {
            $("#date_attainerr").hide();
            $("#date_attainerr").html("");
        });
        check = 1;
    }

    if ($.trim($("#over18raddress").val()) == "") {
        $("#over18raddresserr").show();
        $("#over18raddresserr").html("Place Enter Address ");
        $("#over18raddress").keydown(function () {
            $("#over18raddresserr").hide();
            $("#over18raddresserr").html("");
        });
        check = 1;
    }
    if ($.trim($("#street_numbersr18").val()) == "") {
        $("#street_numbersr18err").show();
        $("#street_numbersr18err").html("Place Enter Street Number ");
        $("#street_numbersr18").keydown(function () {
            $("#street_numbersr18err").hide();
            $("#street_numbersr18err").html("");
        });
        check = 1;
    }
    if ($.trim($("#routesr18").val()) == "") {
        $("#routesr18err").show();
        $("#routesr18err").html("Place Enter StreetName ");
        $("#routesr18").keydown(function () {
            $("#routesr18err").hide();
            $("#routesr18err").html("");
        });
        check = 1;
    }
    if ($.trim($("#localitysr18").val()) == "") {
        $("#localitysr18err").show();
        $("#localitysr18err").html("Place Enter Town/City ");
        $("#localitysr18").keydown(function () {
            $("#localitysr18err").hide();
            $("#localitysr18err").html("");
        });
        check = 1;
    }
    if ($.trim($("#administrative_area_level_1sr18").val()) == "") {
        $("#administrative_area_level_1sr18err").show();
        $("#administrative_area_level_1sr18err").html("Place Enter State/Region ");
        $("#administrative_area_level_1sr18").keydown(function () {
            $("#administrative_area_level_1sr18err").hide();
            $("#administrative_area_level_1sr18err").html("");
        });
        check = 1;
    }
    if ($.trim($("#postal_codesr18").val()) == "") {
        $("#postal_codesr18err").show();
        $("#postal_codesr18err").html("Place Enter Postal/Zip Code");
        $("#postal_codesr18").keydown(function () {
            $("#postal_codesr18err").hide();
            $("#postal_codesr18err").html("");
        });
        check = 1;
    }
    if ($.trim($("#countrysr18").val()) == "") {
        $("#countrysr18err").show();
        $("#countrysr18err").html("Place Enter County");
        $("#countrysr18").keydown(function () {
            $("#countrysr18err").hide();
            $("#countrysr18err").html("");
        });
        check = 1;
    }

    if ($('#s_res_dom_same').prop("checked") == true) {
        if ($.trim($("#s18domicileaddress").val()) == "") {
            $("#s18domicileaddresserr").show();
            $("#s18domicileaddresserr").html("Place Enter Address ");
            $("#s18domicileaddress").keydown(function () {
                $("#s18domicileaddresserr").hide();
                $("#s18domicileaddresserr").html("");
            });
            check = 1;
        }
        if ($.trim($("#street_numbersd18").val()) == "") {
            $("#street_numbersd18err").show();
            $("#street_numbersd18err").html("Place Enter Street Number ");
            $("#street_numbersd18").keydown(function () {
                $("#street_numbersd18err").hide();
                $("#street_numbersd18err").html("");
            });
            check = 1;
        }
        if ($.trim($("#routesd18").val()) == "") {
            $("#routesd18err").show();
            $("#routesd18err").html("Place Enter StreetName ");
            $("#routesd18").keydown(function () {
                $("#routesd18err").hide();
                $("#routesd18err").html("");
            });
            check = 1;
        }
        if ($.trim($("#localitysd18").val()) == "") {
            $("#localitysd18err").show();
            $("#localitysd18err").html("Place Enter Town/City ");
            $("#localitysd18").keydown(function () {
                $("#localitysd18err").hide();
                $("#localitysd18err").html("");
            });
            check = 1;
        }
        if ($.trim($("#administrative_area_level_1sd18").val()) == "") {
            $("#administrative_area_level_1sd18err").show();
            $("#administrative_area_level_1sd18err").html("Place Enter State/Region ");
            $("#administrative_area_level_1sd18").keydown(function () {
                $("#administrative_area_level_1sd18err").hide();
                $("#administrative_area_level_1sd18err").html("");
            });
            check = 1;
        }
        if ($.trim($("#postal_codesd18").val()) == "") {
            $("#postal_codesd18err").show();
            $("#postal_codesd18err").html("Place Enter Postal/Zip Code");
            $("#postal_codesd18").keydown(function () {
                $("#postal_codesd18err").hide();
                $("#postal_codesd18err").html("");
            });
            check = 1;
        }
        if ($.trim($("#countrysrd8").val()) == "") {
            $("#countrysrd8err").show();
            $("#countrysrd8err").html("Place Enter County");
            $("#countrysrd8").keydown(function () {
                $("#countrysrd8err").hide();
                $("#countrysrd8err").html("");
            });
            check = 1;
        }
    }
    if (check == 1) {
        $("#submiterr").show();
        $("#submiterr").html("Please correct all validation errors: check you have not put text (i.e. a symbol like - or an empty space) where there is supposed to be a number or anything else but the email in its field (if you copy-pasted it you may have included a space)");
        return false;
    } else {
        return true;
    }
}

function edit_profile_parent() {
    $("#submiterr").hide();
    $("#submiterr").html("");
    var check = 0;
    student_relationship = $("#student_relationship option:selected").val();
    if ($.trim(student_relationship) == "") {
        $("#student_relationshiperr").show();
        $("#student_relationshiperr").html("Select Month");
        $("#student_relationship").change(function () {
            $("#student_relationshiperr").hide();
            $("#student_relationshiperr").html("");
        });

        check = 1;
    }
    var school_tutor_email = $("#school_tutor_email16").val();
    if (!(isValidEmailAddress(school_tutor_email))) {
        $("#school_tutor_email16err").show();
        $("#school_tutor_email16err").html("Accompanying you Email should be valid email");
        $("#school_tutor_email16").keydown(function () {
            $("#school_tutor_email16err").hide();
            $("#school_tutor_email16err").html("");
        });
        check = 1;
    }



    if ($.trim($("#parentaddress").val()) == "") {
        $("#parentaddresserr").show();
        $("#parentaddresserr").html("Place Enter Address ");
        $("#parentaddress").keydown(function () {
            $("#parentaddresserr").hide();
            $("#parentaddresserr").html("");
        });
        check = 1;
    }
    if ($.trim($("#street_numberp").val()) == "") {
        $("#street_numberperr").show();
        $("#street_numberperr").html("Place Enter Street Number ");
        $("#street_numberp").keydown(function () {
            $("#street_numberperr").hide();
            $("#street_numberperr").html("");
        });
        check = 1;
    }
    if ($.trim($("#routep").val()) == "") {
        $("#routeperr").show();
        $("#routeperr").html("Place Enter StreetName ");
        $("#routep").keydown(function () {
            $("#routeperr").hide();
            $("#routeperr").html("");
        });
        check = 1;
    }
    if ($.trim($("#localityp").val()) == "") {
        $("#localityperr").show();
        $("#localityperr").html("Place Enter Town/City ");
        $("#localityp").keydown(function () {
            $("#localityperr").hide();
            $("#localityperr").html("");
        });
        check = 1;
    }
    if ($.trim($("#administrative_area_level_1p").val()) == "") {
        $("#administrative_area_level_1perr").show();
        $("#administrative_area_level_1perr").html("Place Enter State/Region ");
        $("#administrative_area_level_1p").keydown(function () {
            $("#administrative_area_level_1perr").hide();
            $("#administrative_area_level_1perr").html("");
        });
        check = 1;
    }
    if ($.trim($("#postal_codep").val()) == "") {
        $("#postal_codeperr").show();
        $("#postal_codeperr").html("Place Enter Postal/Zip Code");
        $("#postal_codep").keydown(function () {
            $("#postal_codeperr").hide();
            $("#postal_codeperr").html("");
        });
        check = 1;
    }
    if ($.trim($("#countryp").val()) == "") {
        $("#countryperr").show();
        $("#countryperr").html("Place Enter County");
        $("#countryp").keydown(function () {
            $("#countryperr").hide();
            $("#countryperr").html("");
        });
        check = 1;
    }
    if ($('#p_res_dom_same').prop("checked") == true) {
        if ($.trim($("#pdomicileaddress").val()) == "") {
            $("#pdomicileaddresserr").show();
            $("#pdomicileaddresserr").html("Place Enter Address ");
            $("#pdomicileaddress").keydown(function () {
                $("#pdomicileaddresserr").hide();
                $("#pdomicileaddresserr").html("");
            });
            check = 1;
        }
        if ($.trim($("#street_numberdp").val()) == "") {
            $("#street_numberdperr").show();
            $("#street_numberdperr").html("Place Enter Street Number ");
            $("#street_numberdp").keydown(function () {
                $("#street_numberdperr").hide();
                $("#street_numberdperr").html("");
            });
            check = 1;
        }
        if ($.trim($("#routedp").val()) == "") {
            $("#routedperr").show();
            $("#routedperr").html("Place Enter StreetName ");
            $("#routedp").keydown(function () {
                $("#routedperr").hide();
                $("#routedperr").html("");
            });
            check = 1;
        }
        if ($.trim($("#localitydp").val()) == "") {
            $("#localitydperr").show();
            $("#localitydperr").html("Place Enter Town/City ");
            $("#localitydp").keydown(function () {
                $("#localitydperr").hide();
                $("#localitydperr").html("");
            });
            check = 1;
        }
        if ($.trim($("#administrative_area_level_1dp").val()) == "") {
            $("#administrative_area_level_1dperr").show();
            $("#administrative_area_level_1dperr").html("Place Enter State/Region ");
            $("#administrative_area_level_1dp").keydown(function () {
                $("#administrative_area_level_1dperr").hide();
                $("#administrative_area_level_1dperr").html("");
            });
            check = 1;
        }
        if ($.trim($("#postal_codedp").val()) == "") {
            $("#postal_codedperr").show();
            $("#postal_codedperr").html("Place Enter Postal/Zip Code");
            $("#postal_codedp").keydown(function () {
                $("#postal_codedperr").hide();
                $("#postal_codedperr").html("");
            });
            check = 1;
        }
        if ($.trim($("#countrydp").val()) == "") {
            $("#countrydperr").show();
            $("#countrydperr").html("Place Enter County");
            $("#countrydp").keydown(function () {
                $("#countrydperr").hide();
                $("#countrydperr").html("");
            });
            check = 1;
        }

    }
    if ($.trim($("#sname16").val()) == "") {
        $("#sname16err").show();
        $("#sname16err").html("Place Enter Name");
        $("#sname16").keydown(function () {
            $("#sname16err").hide();
            $("#sname16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#ssurname16").val()) == "") {
        $("#ssurname16err").show();
        $("#ssurname16err").html("Place Enter SurName");
        $("#ssurname16").keydown(function () {
            $("#ssurname16err").hide();
            $("#ssurname16err").html("");
        });
        check = 1;
    }

    bmonth16 = $("#bmonth16 option:selected").val();
    if ($.trim(bmonth16) == "") {
        $("#bmonth16err").show();
        $("#bmonth16err").html("Select Month");
        $("#bmonth16").change(function () {
            $("#bmonth16err").hide();
            $("#bmonth16err").html("");
        });
        check = 1;
    }


    bday16 = $("#bday16 option:selected").val();
    if ($.trim(bday16) == "") {
        $("#bday16err").show();
        $("#bday16err").html("Select Day");
        $("#bday16").change(function () {
            $("#bday16err").hide();
            $("#bday16err").html("");
        });
        check = 1;
    }


    byear16 = $("#byear16 option:selected").val();
    if ($.trim(byear16) == "") {
        $("#byear16err").show();
        $("#byear16err").html("Select Year");
        $("#byear16").change(function () {
            $("#byear16err").hide();
            $("#byear16err").html("");
        });
        check = 1;
    }


    if ($.trim($("#pob16").val()) == "") {
        $("#pob16err").show();
        $("#pob16err").html("Place Enter Place of birth");
        $("#pob16").keydown(function () {
            $("#pob16err").hide();
            $("#pob16err").html("");
        });
        check = 1;
    }

    if ($.trim($("#semail16").val()) == "") {
        $("#semail16err").show();
        $("#semail16err").html("Place Enter Email");
        $("#semail16").keydown(function () {
            $("#semail16err").hide();
            $("#semail16err").html("");
        });
        check = 1;
    }

    var semail16 = $("#semail16").val();
    if (!(isValidEmailAddress(semail16))) {
        $("#semail16err").show();
        $("#semail16err").html("Place Enter Valid Email");
        $("#semail16").keydown(function () {
            $("#semail16err").hide();
            $("#semail16err").html("");
        });
        check = 1;
    }

    if ($.trim($("#sphone16").val()) == "") {
        $("#sphone16err").show();
        $("#sphone16err").html("Place Enter Pnonenumber");
        $("#sphone16").keydown(function () {
            $("#sphone16err").hide();
            $("#sphone16err").html("");
        });
        check = 1;
    }

    if ($.trim($("#nin16").val()) == "") {
        $("#nin16err").show();
        $("#nin16err").html("Place Enter NIN");
        $("#nin16").keydown(function () {
            $("#nin16err").hide();
            $("#nin16err").html("");
        });
        check = 1;
    }


    if ($.trim($("#family_notes").val()) == "") {
        $("#family_noteserr").show();
        $("#family_noteserr").html("Place Enter Notes to host family");
        $("#family_notes").keydown(function () {
            $("#family_noteserr").hide();
            $("#family_noteserr").html("");
        });
        check = 1;
    }
    if ($.trim($("#tutor_notes").val()) == "") {
        $("#tutor_noteserr").show();
        $("#tutor_noteserr").html("Place Enter Notes to tutor");
        $("#tutor_notes").keydown(function () {
            $("#tutor_noteserr").hide();
            $("#tutor_noteserr").html("");
        });
        check = 1;
    }
    if ($.trim($("#profile-about16").val()) == "") {
        $("#profile-about16err").show();
        $("#profile-about16err").html("Place Enter About Your Son/Daughter");
        $("#profile-about16").keydown(function () {
            $("#profile-about16err").hide();
            $("#profile-about16err").html("");
        });
        check = 1;
    }

    if ($.trim($("#learning_condition16").val()) == "") {
        $("#learning_condition16err").show();
        $("#learning_condition16err").html("Place Enter Learning Condition");
        $("#learning_condition16").keydown(function () {
            $("#learning_condition16err").hide();
            $("#learning_condition16err").html("");
        });
        check = 1;
    }
    school_name16 = $("#school_name16 option:selected").val();
    if ($.trim(school_name16) == "") {
        $("#school_name16err").show();
        $("#school_name16err").html("Select Legal Nature");
        $("#school_name16").change(function () {
            $("#school_name16err").hide();
            $("#school_name16err").html("");
        });
        check = 1;
    }

    if ($.trim($("#class_letter16").val()) == "") {
        $("#class_letter16err").show();
        $("#class_letter16err").html("Place Enter ClassLetter");
        $("#class_letter16").keydown(function () {
            $("#class_letter16err").hide();
            $("#class_letter16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#class_number16").val()) == "") {
        $("#class_number16err").show();
        $("#class_number16err").html("Place Enter ClassNumber");
        $("#class_number16").keydown(function () {
            $("#class_number16err").hide();
            $("#class_number16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#student_number16").val()) == "") {
        $("#student_number16err").show();
        $("#student_number16err").html("Place Enter Student Number");
        $("#student_number16").keydown(function () {
            $("#student_number16err").hide();
            $("#student_number16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#academic_qualifications16").val()) == "") {
        $("#academic_qualifications16err").show();
        $("#academic_qualifications16err").html("Place Enter Academic Qualifications");
        $("#academic_qualifications16").keydown(function () {
            $("#academic_qualifications16err").hide();
            $("#academic_qualifications16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#date_attain16").val()) == "") {
        $("#date_attain16err").show();
        $("#date_attain16err").html("Place Enter DateAttain");
        $("#date_attain16").keydown(function () {
            $("#date_attain16err").hide();
            $("#date_attain16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#sraddress16").val()) == "") {
        $("#sraddress16err").show();
        $("#sraddress16err").html("Place Enter Address ");
        $("#sraddress16").keydown(function () {
            $("#sraddress16err").hide();
            $("#sraddress16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#street_numbersr16").val()) == "") {
        $("#street_numbersr16err").show();
        $("#street_numbersr16err").html("Place Enter Street Number ");
        $("#street_numbersr16").keydown(function () {
            $("#street_numbersr16err").hide();
            $("#street_numbersr16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#routesr16").val()) == "") {
        $("#routesr16err").show();
        $("#routesr16err").html("Place Enter StreetName ");
        $("#routesr16").keydown(function () {
            $("#routesr16err").hide();
            $("#routesr16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#localitysr16").val()) == "") {
        $("#localitysr16err").show();
        $("#localitysr16err").html("Place Enter Town/City ");
        $("#localitysr16").keydown(function () {
            $("#localitysr16err").hide();
            $("#localitysr16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#administrative_area_level_1sr16").val()) == "") {
        $("#administrative_area_level_1sr16err").show();
        $("#administrative_area_level_1sr16err").html("Place Enter State/Region ");
        $("#administrative_area_level_1sr16").keydown(function () {
            $("#administrative_area_level_1sr16err").hide();
            $("#administrative_area_level_1sr16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#postal_codesr16").val()) == "") {
        $("#postal_codesr16err").show();
        $("#postal_codesr16err").html("Place Enter Postal/Zip Code");
        $("#postal_codesr16").keydown(function () {
            $("#postal_codesr16err").hide();
            $("#postal_codesr16err").html("");
        });
        check = 1;
    }
    if ($.trim($("#countrysr16").val()) == "") {
        $("#countrysr16err").show();
        $("#countrysr16err").html("Place Enter County");
        $("#countrysr16").keydown(function () {
            $("#countrysr16err").hide();
            $("#countrysr16err").html("");
        });
        check = 1;
    }

    if ($('#s16_res_dom_same').prop("checked") == true) {


        if ($.trim($("#daddress16").val()) == "") {
            $("#daddress16err").show();
            $("#daddress16err").html("Place Enter Address ");
            $("#daddress16").keydown(function () {
                $("#daddress16err").hide();
                $("#daddress16err").html("");
            });
            check = 1;
        }
        if ($.trim($("#street_numberd16").val()) == "") {
            $("#street_numberd16err").show();
            $("#street_numberd16err").html("Place Enter Street Number ");
            $("#street_numberd16").keydown(function () {
                $("#street_numberd16err").hide();
                $("#street_numberd16err").html("");
            });
            check = 1;
        }
        if ($.trim($("#routed16").val()) == "") {
            $("#routed16err").show();
            $("#routed16err").html("Place Enter StreetName ");
            $("#routed16").keydown(function () {
                $("#routed16err").hide();
                $("#routed16err").html("");
            });
            check = 1;
        }
        if ($.trim($("#localityd16").val()) == "") {
            $("#localityd16err").show();
            $("#localityd16err").html("Place Enter Town/City ");
            $("#localityd16").keydown(function () {
                $("#localityd16err").hide();
                $("#localityd16err").html("");
            });
            check = 1;
        }
        if ($.trim($("#administrative_area_level_1d16").val()) == "") {
            $("#administrative_area_level_1d16err").show();
            $("#administrative_area_level_1d16err").html("Place Enter State/Region ");
            $("#administrative_area_level_1d16").keydown(function () {
                $("#administrative_area_level_1d16err").hide();
                $("#administrative_area_level_1d16err").html("");
            });
            check = 1;
        }
        if ($.trim($("#postal_coded16").val()) == "") {
            $("#postal_coded16err").show();
            $("#postal_coded16err").html("Place Enter Postal/Zip Code");
            $("#postal_coded16").keydown(function () {
                $("#postal_coded16err").hide();
                $("#postal_coded16err").html("");
            });
            check = 1;
        }
        if ($.trim($("#countryd16").val()) == "") {
            $("#countryd16err").show();
            $("#countryd16err").html("Place Enter County");
            $("#countryd16").keydown(function () {
                $("#countryd16err").hide();
                $("#countryd16err").html("");
            });
            check = 1;
        }
    }
    if (check == 1) {
        $("#submiterr").show();
        $("#submiterr").html("Please correct all validation errors: check you have not put text (i.e. a symbol like - or an empty space) where there is supposed to be a number or anything else but the email in its field (if you copy-pasted it you may have included a space)");
        return false;
    } else {
        return true;
    }
}

function edit_profile_experience() {
    $("#submiterr").hide();
    $("#submiterr").html("");
    var check = 0;
    var orgemail = $("#orgemail").val();
    if (!(isValidEmailAddress(orgemail))) {
        $("#orgemailerr").show();
        $("#orgemailerr").html("Organization Email should be valid email");
        $("#orgemail").keydown(function () {
            $("#orgemailerr").hide();
            $("#orgemailerr").html("");
        });
        check = 1;
    }

    if ($.trim($("#orgname").val()) == "") {
        $("#orgnameerr").show();
        $("#orgnameerr").html("Place Enter Organization Name ");
        $("#orgname").keydown(function () {
            $("#orgnameerr").hide();
            $("#orgnameerr").html("");
        });
        check = 1;
    }

    if ($.trim($("#aboutorg").val()) == "") {
        $("#aboutorgerr").show();
        $("#aboutorgerr").html("Place Enter About Organization ");
        $("#aboutorg").keydown(function () {
            $("#aboutorgerr").hide();
            $("#aboutorgerr").html("");
        });
        check = 1;
    }


    if ($.trim($("#orgemail").val()) == "") {
        $("#orgemailerr").show();
        $("#orgemailerr").html("Place Enter Valid Organization Email ");
        $("#orgemail").keydown(function () {
            $("#orgemailerr").hide();
            $("#orgemailerr").html("");
        });
        check = 1;
    }

    var orgemail = $("#orgemail").val();
    if (!(isValidEmailAddress(orgemail))) {
        $("#orgemailerr").show();
        $("#orgemailerr").html("Place Enter Valid Organization Email");
        $("#orgemail").keydown(function () {
            $("#orgemailerr").hide();
            $("#orgemailerr").html("");
        });
        check = 1;
    }

    if ($.trim($("#orgphone").val()) == "") {
        $("#orgphoneerr").show();
        $("#orgphoneerr").html("Place Enter Organization Phonenumber ");
        $("#orgphone").keydown(function () {
            $("#orgphoneerr").hide();
            $("#orgphoneerr").html("");
        });
        check = 1;
    }



    if ($.trim($("#why_fit").val()) == "") {
        $("#why_fiterr").show();
        $("#why_fiterr").html("Place Enter Why fit to be tutor? ");
        $("#why_fit").keydown(function () {
            $("#why_fiterr").hide();
            $("#why_fiterr").html("");
        });
        check = 1;
    }
    legal_nature = $("#legal_nature option:selected").val();
    if ($.trim(legal_nature) == "") {
        $("#legal_natureerr").show();
        $("#legal_natureerr").html("Select Legal Nature");
        $("#legal_nature").change(function () {
            $("#legal_natureerr").hide();
            $("#legal_natureerr").html("");
        });
        check = 1;
    }
    orghr = $("#orghr option:selected").val();
    if ($.trim(orghr) == "") {
        $("#orghrerr").show();
        $("#orghrerr").html("Select Humanresource");
        $("#orghr").change(function () {
            $("#orghrerr").hide();
            $("#orghrerr").html("");
        });
        check = 1;
    }

    tutorrelation = $("#tutorrelation option:selected").val();
    if ($.trim(tutorrelation) == "") {
        $("#tutorrelationerr").show();
        $("#tutorrelationerr").html("Select Releation with Company");
        $("#tutorrelation").change(function () {
            $("#tutorrelationerr").hide();
            $("#tutorrelationerr").html("");
        });
        check = 1;
    }

    var wstreet_number = $("#street_numberorg").val();
    var wroute = $("#routeorg").val();
    var wlocality = $("#localityorg").val();
    var wadministrative_area_level_1 = $("#administrative_area_level_1org").val();
    var wpostal_code = $("#postal_codeorg").val();
    var wcountry = $("#countryorg").val();


    if ($.trim($("#orgaddress").val()) == "") {
        $("#orgaddresserr").show();
        $("#orgaddresserr").html("Place Enter Address ");
        $("#orgaddress").keydown(function () {
            $("#orgaddresserr").hide();
            $("#orgaddresserr").html("");
        });
        check = 1;
    }
    if ($.trim($("#street_numberorg").val()) == "") {
        $("#street_numberorgerr").show();
        $("#street_numberorgerr").html("Place Enter Street Number ");
        $("#street_numberorg").keydown(function () {
            $("#street_numberorgerr").hide();
            $("#street_numberorgerr").html("");
        });
        check = 1;
    }
    if ($.trim($("#routeorg").val()) == "") {
        $("#routeorgerr").show();
        $("#routeorgerr").html("Place Enter StreetName ");
        $("#routeorg").keydown(function () {
            $("#routeorgerr").hide();
            $("#routeorgerr").html("");
        });
        check = 1;
    }
    if ($.trim($("#localityorg").val()) == "") {
        $("#localityorgerr").show();
        $("#localityorgerr").html("Place Enter Town/City ");
        $("#localityorg").keydown(function () {
            $("#localityorgerr").hide();
            $("#localityorgerr").html("");
        });
        check = 1;
    }
    if ($.trim($("#administrative_area_level_1org").val()) == "") {
        $("#administrative_area_level_1orgerr").show();
        $("#administrative_area_level_1orgerr").html("Place Enter State/Region ");
        $("#administrative_area_level_1org").keydown(function () {
            $("#administrative_area_level_1orgerr").hide();
            $("#administrative_area_level_1orgerr").html("");
        });
        check = 1;
    }
    if ($.trim($("#postal_codeorg").val()) == "") {
        $("#postal_codeorgerr").show();
        $("#postal_codeorgerr").html("Place Enter Postal/Zip Code");
        $("#postal_codeorg").keydown(function () {
            $("#postal_codeorgerr").hide();
            $("#postal_codeorgerr").html("");
        });
        check = 1;
    }
    if ($.trim($("#countryorg").val()) == "") {
        $("#countryorgerr").show();
        $("#countryorgerr").html("Place Enter County");
        $("#countryorg").keydown(function () {
            $("#countryorgerr").hide();
            $("#countryorgerr").html("");
        });
        check = 1;
    }

    address = wstreet_number + ',' + wroute + ',' + wlocality + ',' + wadministrative_area_level_1 + ',' + wpostal_code + ',' + wcountry;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            $('#orglonbox').val(longitude);
            $('#orglatbox').val(latitude);
        } else {
            $("#countryorgerr").show();
            $("#countryorgerr").html("Place Enter County");
        }
    });
    if (check == 1) {
        $("#submiterr").show();
        $("#submiterr").html("Please correct all validation errors: check you have not put text (i.e. a symbol like - or an empty space) where there is supposed to be a number or anything else but the email in its field (if you copy-pasted it you may have included a space)");
        return false;
    } else {
        return true;
    }
}

function edit_profile_paidservices(){
    
}

function edit_profile() {
    $("#submiterr").hide();
    $("#submiterr").html("");

    // userrole = $("#userrole option:selected").val();
    var role = $("#role").val();
    var check = 0;
    /*if($.trim(userrole) == ""){
     $("#userroleerr").show();
     $("#userroleerr").html("Select Profile Type");
     $("#userrole").change(function(){
     $("#userroleerr").hide();
     $("#userroleerr").html("");
     });
     check =1;
     }*/


    userrole = 0;
    if (userrole == 4)
    {

    }

    if (role == 4) {

    }

    if (role == 6) {



    }

    if (role == 5) {




    }
    if (check == 1) {
        $("#submiterr").show();
        $("#submiterr").html("Please correct all validation errors: check you have not put text (i.e. a symbol like - or an empty space) where there is supposed to be a number or anything else but the email in its field (if you copy-pasted it you may have included a space)");

        return false;

    } else {
        return true;
    }

    /*gender = $("#paypalid").val();
     if($.trim(gender) == ""){
     $("#payerr").show();
     $("#payerr").html("Paypal id should not be blank");
     $("#paypalid").keydown(function(){
     $("#payerr").hide();
     $("#payerr").html("");
     });
     return false;
     }		
     if (!(isValidEmailAddress(gender))) {
     $("#payerr").show();
     $("#payerr").html("Paypal id should be valid");
     $("#paypalid").keydown(function(){
     $("#payerr").hide();
     $("#payerr").html("");
     });
     return false;  
     }	
     about = $("#profile-about").val();
     if($.trim(about) == ""){
     $(".field-profile-about").addClass("has-error");
     $("#profile-about").next(".help-block-error").html("About cannot be blank.");
     $("#profile-about").keydown(function(){
     $(".field-profile-about").removeClass("has-error");
     $("#profile-about").next(".help-block-error").html("");
     });
     return false;
     }*/

}

function show_checkin(reserveid)
{
    $("#checkinbtn" + reserveid).hide();
    $("#checkindate" + reserveid).show();
}

function show_checkout(reserveid)
{
    $("#checkoutbtn" + reserveid).hide();
    $("#checkoutdate" + reserveid).show();
}

function save_checkin(reserveid) {
    checkindate = $("#checkin" + reserveid).val();
    inhr = $("#inhr" + reserveid).val();
    inmin = $("#inmin" + reserveid).val();
    insec = $("#insec" + reserveid).val();
    if (inhr != 'HH' && inmin != 'MM' && insec != 'SS' && checkindate != '') {
        $.ajax({
            type: 'POST',
            url: baseurl + '/user/listing/savecheckin',
            async: false,
            data: {
                reserveid: reserveid,
                checkindate: checkindate,
                inhr: inhr,
                inmin: inmin,
                insec: insec
            },
            success: function (data) {
                datas = data.split("***");
                if ($.trim(datas[0]) == "success") {
                    //$("#checkindate"+reserveid).hide();
                    $("#checkindate" + reserveid).html(datas[1]);
                    $("#checkout_" + reserveid).html('');
                    $("#checkout_" + reserveid).html('<input type="button" id="checkoutbtn' + reserveid + '" value="Check Out" class="btn btn-danger" onclick="show_checkout(' + reserveid + ')">');
                }
            }
        });
    }
}

function save_checkout(reserveid) {
    checkoutdate = $("#checkout" + reserveid).val();
    outhr = $("#outhr" + reserveid).val();
    outmin = $("#outmin" + reserveid).val();
    outsec = $("#outsec" + reserveid).val();
    if (outhr != 'HH' && outmin != 'MM' && outsec != 'SS' && checkoutdate != '') {
        $.ajax({
            type: 'POST',
            url: baseurl + '/user/listing/savecheckout',
            async: false,
            data: {
                reserveid: reserveid,
                checkoutdate: checkoutdate,
                outhr: outhr,
                outmin: outmin,
                outsec: outsec
            },
            success: function (data) {
                datas = data.split("***");
                if ($.trim(datas[0]) == "success") {
                    //$("#checkindate"+reserveid).hide();
                    $("#checkoutdate" + reserveid).html(datas[1]);
                }
            }
        });
    }
}

/******************Rating Starts**************************/
$(document).on('mouseover', '.one', function () {
    $('.rating').removeClass('active');
    $('.one').addClass('hover');
    $('.two').removeClass('hover');
    $('.three').removeClass('hover');
    $('.four').removeClass('hover');
    $('.five').removeClass('hover');
    if ($('.one').hasClass('fa-star-o')) {
        $('.one').addClass('fa-star');
        $('.one').removeClass('fa-star-o');
    }
    $('.current-rate').html('1');
});
$(document).on('mouseover', '.two', function () {
    $('.rating').removeClass('active');
    $('.one').addClass('hover');
    $('.two').addClass('hover');
    $('.three').removeClass('hover');
    $('.four').removeClass('hover');
    $('.five').removeClass('hover');
    if ($('.one').hasClass('fa-star-o')) {
        $('.one').addClass('fa-star');
        $('.one').removeClass('fa-star-o');
    }
    if ($('.two').hasClass('fa-star-o')) {
        $('.two').addClass('fa-star');
        $('.two').removeClass('fa-star-o');
    }

    $('.current-rate').html('2');
});
$(document).on('mouseover', '.three', function () {
    $('.rating').removeClass('active');
    $('.one').addClass('hover');
    $('.two').addClass('hover');
    $('.three').addClass('hover');
    $('.four').removeClass('hover');
    $('.five').removeClass('hover');

    if ($('.one').hasClass('fa-star-o')) {
        $('.one').addClass('fa-star');
        $('.one').removeClass('fa-star-o');
    }
    if ($('.two').hasClass('fa-star-o')) {
        $('.two').addClass('fa-star');
        $('.two').removeClass('fa-star-o');
    }
    if ($('.three').hasClass('fa-star-o')) {
        $('.three').addClass('fa-star');
        $('.three').removeClass('fa-star-o');
    }


    $('.current-rate').html('3');
});
$(document).on('mouseover', '.four', function () {
    $('.rating').removeClass('active');
    $('.one').addClass('hover');
    $('.two').addClass('hover');
    $('.three').addClass('hover');
    $('.four').addClass('hover');
    $('.five').removeClass('hover');

    if ($('.one').hasClass('fa-star-o')) {
        $('.one').addClass('fa-star');
        $('.one').removeClass('fa-star-o');
    }
    if ($('.two').hasClass('fa-star-o')) {
        $('.two').addClass('fa-star');
        $('.two').removeClass('fa-star-o');
    }
    if ($('.three').hasClass('fa-star-o')) {
        $('.three').addClass('fa-star');
        $('.three').removeClass('fa-star-o');
    }
    if ($('.four').hasClass('fa-star-o')) {
        $('.four').addClass('fa-star');
        $('.four').removeClass('fa-star-o');
    }
    $('.one').addClass('fa-star');
    $('.one').removeClass('fa-star-o');
    $('.two').addClass('fa-star');
    $('.two').removeClass('fa-star-o');
    $('.three').addClass('fa-star');
    $('.three').removeClass('fa-star-o');
    $('.four').addClass('fa-star');
    $('.four').removeClass('fa-star-o');
    $('.current-rate').html('4');
});
$(document).on('mouseover', '.five', function () {
    $('.rating').removeClass('active');
    $('.one').addClass('hover');
    $('.two').addClass('hover');
    $('.three').addClass('hover');
    $('.four').addClass('hover');
    $('.five').addClass('hover');

    if ($('.rating').hasClass('fa-star-o')) {
        $('.rating').addClass('fa-star');
        $('.rating').removeClass('fa-star-o');
    }

    $('.current-rate').html('5');
});
$(document).on('mouseout', '.rating', function () {
    $('.rating').removeClass('hover');
    if ($('.rating').hasClass('fa-star')) {
        $('.rating').addClass('fa-star-o');
        $('.rating').removeClass('fa-star');
    }
    $('.rating').removeClass('fa-star');
    $('.rating').addClass('fa-star-o');
    if (rating != 0) {
        switch (rating) {
            case '5':
                $('.rating').addClass('active');

                $('.rating').addClass('fa-star');
                $('.rating').removeClass('fa-star-o');
                break;
            case '4':
                $('.four').addClass('active');

                $('.one').addClass('fa-star');
                $('.one').removeClass('fa-star-o');
                $('.two').addClass('fa-star');
                $('.two').removeClass('fa-star-o');
                $('.three').addClass('fa-star');
                $('.three').removeClass('fa-star-o');
                $('.four').addClass('fa-star');
                $('.four').removeClass('fa-star-o');
                $('.five').removeClass('fa-star');
                $('.five').addClass('fa-star-o');
                break;
            case '3':
                $('.three').addClass('active');

                $('.one').addClass('fa-star');
                $('.one').removeClass('fa-star-o');
                $('.two').addClass('fa-star');
                $('.two').removeClass('fa-star-o');
                $('.three').addClass('fa-star');
                $('.three').removeClass('fa-star-o');
                $('.four').removeClass('fa-star');
                $('.four').addClass('fa-star-o');
                $('.five').removeClass('fa-star');
                $('.five').addClass('fa-star-o');
                break;
            case '2':
                $('.two').addClass('active');

                $('.one').addClass('fa-star');
                $('.one').removeClass('fa-star-o');
                $('.two').addClass('fa-star');
                $('.two').removeClass('fa-star-o');
                $('.three').removeClass('fa-star');
                $('.three').addClass('fa-star-o');
                $('.four').removeClass('fa-star');
                $('.four').addClass('fa-star-o');
                $('.five').removeClass('fa-star');
                $('.five').addClass('fa-star-o');
                break;
            case '1':
                $('.one').addClass('active');

                $('.one').addClass('fa-star');
                $('.one').removeClass('fa-star-o');
                $('.two').removeClass('fa-star');
                $('.two').addClass('fa-star-o');
                $('.three').removeClass('fa-star');
                $('.three').addClass('fa-star-o');
                $('.four').removeClass('fa-star');
                $('.four').addClass('fa-star-o');
                $('.five').removeClass('fa-star');
                $('.five').addClass('fa-star-o');
                break;
        }
    }
    $('.current-rate').html(rating);
});

function ratingClick(value) {
    switch (value) {
        case "5":
            $('.rating').addClass('active');
            $('.rating').addClass('fa-star');
            $('.rating').removeClass('fa-star-o');

            break;
        case "4":
            $('.four').addClass('active');

            if ($('.rating.one').hasClass('active')) {
                $('.one').addClass('fa-star');
                $('.one').removeClass('fa-star-o');
            }
            if ($('.rating.two').hasClass('active')) {
                $('.two').addClass('fa-star');
                $('.two').removeClass('fa-star-o');
            }
            if ($('.rating.three').hasClass('active')) {
                $('.three').addClass('fa-star');
                $('.three').removeClass('fa-star-o');
            }
            if ($('.rating.four').hasClass('active')) {
                $('.four').addClass('fa-star');
                $('.four').removeClass('fa-star-o');
            }
            if ($('.rating.five').hasClass('fa-star')) {
                $('.five').removeClass('fa-star');
                $('.five').removeClass('fa-star-o');
            }
            break;
        case '3':
            $('.three').addClass('active');

            $('.one').addClass('fa-star');
            $('.one').removeClass('fa-star-o');
            $('.two').addClass('fa-star');
            $('.two').removeClass('fa-star-o');
            $('.three').addClass('fa-star');
            $('.three').removeClass('fa-star-o');
            $('.four').removeClass('fa-star');
            $('.four').addClass('fa-star-o');
            $('.five').removeClass('fa-star');
            $('.five').addClass('fa-star-o');
            break;
        case '2':
            $('.two').addClass('active');

            $('.one').addClass('fa-star');
            $('.one').removeClass('fa-star-o');
            $('.two').addClass('fa-star');
            $('.two').removeClass('fa-star-o');
            $('.three').removeClass('fa-star');
            $('.three').addClass('fa-star-o');
            $('.four').removeClass('fa-star');
            $('.four').addClass('fa-star-o');
            $('.five').removeClass('fa-star');
            $('.five').addClass('fa-star-o');
            break;
        case '1':
            $('.one').addClass('active');

            $('.one').addClass('fa-star');
            $('.one').removeClass('fa-star-o');
            $('.two').removeClass('fa-star');
            $('.two').addClass('fa-star-o');
            $('.three').removeClass('fa-star');
            $('.three').addClass('fa-star-o');
            $('.four').removeClass('fa-star');
            $('.four').addClass('fa-star-o');
            $('.five').removeClass('fa-star');
            $('.five').addClass('fa-star-o');
            break;
    }
    $('.current-rate').html(value);
    $('#rateval').val(value);
    rating = value;
}
/*************************Rating Ends***************************/

function review_trip(tripid) {
    $("#tripid").val(tripid);
}

function edit_review(tripid) {
    $("#tripid").val(tripid);
    reviewcont = $("#review" + tripid).html();
    $("#reviewmsg").val(reviewcont);
}

function savereview() {
    tripid = $("#tripid").val();
    rating = $(".current-rate").html();
    review = $("#reviewmsg").val();

    if (rating == 0) {
        $("#errorBox").show();
        $("#errorBox").html("Please Fill the Rating");
        setTimeout(function () {
            $('#errorBox').fadeOut('slow');
        }, 3000);
        return false;
    } else if (review == "") {
        $("#errorBoxr").show();
        $('#errorBoxr').html('Please Fill the Review');
        setTimeout(function () {
            $('#errorBoxr').fadeOut('slow');
        }, 3000);
        return false;
    } else {


        $.ajax({
            type: 'POST',
            url: baseurl + '/user/listing/savereview',
            async: false,
            data: {
                tripid: tripid,
                rating: rating,
                review: review
            },
            success: function (data) {
                if ($.trim(data) == "success") {
                    $("#reviewsave").attr('data-dismiss', 'modal');
                    $("#reviewbtn").html('<a href="' + baseurl + '/user/listing/reviewsbyyou">View Review</a>');
                }
            }
        });
    }
}

function reviewedit() {
    tripid = $("#tripid").val();
    review = $("#reviewmsg").val();
    if ($.trim(review) == "")
    {
        $("#reviewediterr").show();
        $("#reviewediterr").html("Please enter the review");
        setTimeout(function () {
            $("#reviewediterr").hide();
        }, 5000);
    } else
    {
        $.ajax({
            type: 'POST',
            url: baseurl + '/user/listing/reviewedit',
            async: false,
            data: {
                tripid: tripid,
                review: review
            },
            success: function (data) {
                if ($.trim(data) == "success") {
                    $("#revieweditbtn").attr('data-dismiss', 'modal');
                    $("#review" + tripid).html(review);
                }
            }
        });
    }
}

function print_doc() {
    window.print();
}

function change_currency() {
    var currencyid = $("#currency_select").val();
    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/changecurrency',
        async: false,
        data: {
            currencyid: currencyid,
        },
        success: function (data) {
            window.location.reload();
        }
    });
}

function change_language() {
    var language = $("#language_select").val();
    $.ajax({
        type: 'POST',
        url: baseurl + '/language',
        async: false,
        data: {
            language: language,
        },
        success: function (data) {
            window.location.reload();
        }
    });
}

function show_list_popup(event, listid) {
    event.preventDefault();
    //alert("yes here");
    $.ajax({
        type: 'POST',
        url: baseurl + '/getlistpopup',
        async: false,
        data: {
            listid: listid
        },
        success: function (data) {
            datas = data.split("*****");
            $("#listimage").css("background-image", "url(" + datas[1] + ")");
            $("#listnames").html(datas[0]);
            $("#listingid").val(listid);
        }
    });
}

function hide_more_txt(morecls)
{
    $(morecls).hide();
}

function show_more_txt(morecls)
{
    $("#amenitymore").show();
}

function show_more_txt1()
{
    $("#propertymore").show();
}

function generate_code()
{
    codenumber = Math.floor(Math.random() * 9000) + 1000;
    $("#codetext").html('Your generated code is ' + codenumber + '. Enter this code to verify');
    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/saveusercode',
        async: false,
        data: {
            codenumber: codenumber
        },
        success: function (data) {
            $("#codeblock").show();
        }
    });
}

function verify_code()
{
    verifycode = $("#verifycode").val();
    $.ajax({
        type: 'POST',
        url: baseurl + '/user/listing/verifyusercode',
        async: false,
        data: {
            verifycode: verifycode
        },
        success: function (data) {
            if ($.trim(data) == 'success') {
                $("#codesuccess").show();
                $("#codeerror").hide();
                $("#codesuccess").html("Code verified successfully");
                setTimeout(function () {
                    $("#codesuccess").hide();
                }, 5000);
                $("#phoneverify").html("Verified");
            } else
            {
                $("#codesuccess").hide();
                $("#codeerror").show();
                $("#codeerror").html("Code does not match");
                setTimeout(function () {
                    $("#codeerror").hide();
                }, 5000);
            }
        }
    });
}
