/**
 * To handle the map functions
 * in the search page of listing
 */
var markers = [];
var map, bounds, infowindow;
function initMap() {
   infowindow = new google.maps.InfoWindow();
   bounds = new google.maps.LatLngBounds();
   autocomplete = new google.maps.places.Autocomplete((document
      .getElementById('where-to-go')), {
    types : [ 'geocode' ]
  });
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
      fillInAddress();
      //To  focus the map at the place address  
      var place = autocomplete.getPlace();
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(15);
        }
        // To auto filter the search details: as the address changed
        filterDetails();
  });

 

  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: baseLat, lng: baselng},
    zoom: zoomval,
    //mapTypeId: 'roadmap',
    mapTypeId: 'satellite',
    styles: [{
      featureType: 'poi',
      stylers: [{ visibility: 'off' }]  // Turn off points of interest.
    }, {
      featureType: 'transit.station',
      stylers: [{ visibility: 'off' }]  // Turn off bus stations, train stations, etc.
    }],
    disableDoubleClickZoom: true
  });
  
    /***Autocomplete with address field on search page**/
    //var addressAutocomplete = new google.maps.places.Autocomplete((document.getElementById('searchAddress')), {    /*types : [ 'geocode' ]*/ });
    /*google.maps.event.addListener(addressAutocomplete, 'place_changed', function() {
        var place = addressAutocomplete.getPlace();
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(15);
        }
        // To auto filter the search details: as the address changed
        filterDetails();
    });*/


      google.maps.event.addListener(map,'mousedown',function(event) { 
        searchmove = $('input[name="searchmove"]:checked').val();
        if(searchmove=="on")
        {
            var place = event.latLng;
            lat = place.lat();
            lng = place.lng();
              var geocoder;
              geocoder = new google.maps.Geocoder();
              var latlng = new google.maps.LatLng(lat, lng);

              geocoder.geocode(
                  {'latLng': latlng}, 
                  function(results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                              if (results[0]) {
                                  var add= results[0].formatted_address ;
                                  var  value=add.split(",");

                                  count=value.length;
                                  country=value[count-1];
                                  state=value[count-2];
                                  city=value[count-3];
                                  placename = city+","+state+","+country;
                                  $("#where-to-go").val(placename);
                                  $('#place-lat').val(lat);
                                  $('#place-lng').val(lng);
                                  updateSearchList('#guest-count', 'guest');
                                  //alert("city name is: " + city);
                              }
                              else  {
                                  //alert("address not found");
                              }
                      }
                       else {
                          //alert("Geocoder failed due to: " + status);
                      }
                  }
              );
        }
});
  //plotMarkers();
  filterDetails();
}



google.maps.event.addDomListener(window, 'load', initMap);


function plotMarkers(){
  for(var i=0; i < markerPoints.length; i++){
    addMarker(markerPoints[i], infoMarker[i]);
  }
  //showMarkers();
  /*setTimeout(function(){
    deleteMarkers();
    }, 10000);*/
}
var redimagepath = "http://"+window.location.hostname+'/frontendassets/images/red-dot.png';
var greenimagepath = "http://"+window.location.hostname+'/frontendassets/images/green-dot.png';
//Adds a marker to the map and push to the array.
function addMarker(location, info) {

var marker = new google.maps.Marker({
 position: location,
 icon : redimagepath,
 map: map
});
var infowindow = new google.maps.InfoWindow();
google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(info);
    infowindow.open(map, this);
});
markers.push(marker);
}
showme = function (index) {
    //    markers[index].setAnimation(google.maps.Animation.BOUNCE);
  //markers[index].setIcon(greenimagepath);

}
hideme = function (index) {
//markers[index].setAnimation(null);
//markers[index].setIcon(redimagepath);
//markers[index].setIcon('../images/red-dot.png');
}
//Sets the map on all markers in the array.
function setMapOnAll(map) {
for (var i = 0; i < markers.length; i++) {
 markers[i].setMap(map);
}
}

//Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
setMapOnAll(null);
}

//Shows any markers currently in the array.
function showMarkers() {
setMapOnAll(map);
}

//Deletes all markers in the array by removing references to them.
function deleteMarkers() {
clearMarkers();
markers = [];
}