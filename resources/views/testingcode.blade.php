<div class="modal fade" id="airfcfx-mobile-cal" role="dialog"> 
				<div class="modal-dialog mobile-cal-cnt">
					<div class="mobile-cal-header">
						<button class="close airfcfx-mobile-cal-close" type="button" data-dismiss="modal">×</button>
						Search					</div>
					<div class="mobile-cal-body">
						<input id="where-to-go-mobile" type="text" value="" placeholder="Where do u want to go?" class="mobile-cal-input-100 form-control form_text1 where-to-go" /> 
						<input type="hidden" id="latitudemobile" value="">
						<input type="hidden" id="longitudemobile" value="">
						<input id="check-in-mobile" value="" readonly class="mobile-cal-input-50 airfcfx-check-cal form-control cal form_text2 check-in" placeholder="Check in">
						<input id="check-out-mobile" value="" readonly class="pull-right mobile-cal-input-50 airfcfx-check-cal form-control cal form_text2 check-out" placeholder="Check Out ">
						<select id="guest-count-mobile" class="mobile-cal-input-100 airfcfx-guest-count form-control form_text2 guest-count">
						 				</select>
					 
						<div class="error-searchmobile" style="float:right;"></div>
						<button class="airfcfx-mobile-cal-btn airfcfx-slider-btn btn btn_search" onclick="searchlistmobile();">Find a place</button>
            
					</div>
				</div>
			</div>
				
				 