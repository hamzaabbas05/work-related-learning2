<html><head>
        <style> 
            .cust_p{
                line-height:1.38;margin-top:0pt;margin-bottom:0pt;margin-left: -36.75pt;margin-right: -29.25pt;text-align: center;
            }
            .cust_title{
                font-size: 12pt; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }
            .cust_address{
                font-size: 11pt; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }
            .cust_h1{
                line-height:1.38;margin-top:24pt;margin-bottom:6pt;margin-left: -36.75pt;margin-right: -29.25pt;text-align: center;
            }
            .cust_main_title {
                font-size: 14pt; font-family: Arial; color: rgb(7, 55, 99); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }
            .cust_span1 {
                font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }
            .main_div {
                margin: 0 auto;width: 100%;position: relative;;
            }
            table {
                border-width: initial; border-style: none; border-color: initial; border-collapse: collapse;margin: 0 auto;position: relative;
            }
            table th {
                border-left:solid #000000 1pt;border-right:solid #000000 1pt;border-bottom:solid #000000 1pt;border-top:solid #000000 1pt;vertical-align:top;padding:5pt 5pt 5pt 5pt;
            }
            table td {
                border-left:solid #000000 1pt;border-right:solid #000000 1pt;border-bottom:solid #000000 1pt;border-top:solid #000000 1pt;vertical-align:top;padding:5pt 5pt 5pt 5pt;   
            }
            .cust_footer_div {
                width: 60%;margin:0 auto;
            }
            .cust_footer_p {
                line-height:1.38;margin-left: 60px;
            }
            .cust_footer_span {
                font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }

        </style>
    </head><body>
        <p dir="ltr" class="cust_p">
            <span id="docs-internal-guid-0af5eb38-7fff-dcf3-8c8e-c9bc1f21cb64">
                <span class="cust_title">Secondary Education Institute (name of student&#39;s school): <span style="text-decoration:underline">{{isset($rentalObj->user->school->name)?$rentalObj->user->school->name:""}}</span></span>
            </span>
        </p>
        <p dir="ltr" class="cust_p">
            <span id="docs-internal-guid-0af5eb38-7fff-dcf3-8c8e-c9bc1f21cb64"><span class="cust_address">Address: <span style="text-decoration:underline">{{isset($rentalObj->user->school->legal_address)?$rentalObj->user->school->legal_address:""}}</span></span>
            </span>
        </p>
        <h1 dir="ltr" class="cust_h1">
            <span id="docs-internal-guid-0af5eb38-7fff-dcf3-8c8e-c9bc1f21cb64">
                <span class="cust_main_title">Work Related Learning activity - Register (certified hours)</span>
            </span>
        </h1>
        <p>&nbsp;</p>
        <p dir="ltr" class="cust_p">
            <span id="docs-internal-guid-0af5eb38-7fff-dcf3-8c8e-c9bc1f21cb64">
                <?php if (is_null($rentalObj->user->student_relationship)) { ?>
                    <span class="cust_span1">Student <span style="text-decoration:underline">{{$rentalObj->user->name}} {{$rentalObj->user->lastname}}</span> ;
                    <?php } else { ?>
                        <span class="cust_span1">Student <span style="text-decoration:underline">{{$rentalObj->user->sname16}} {{$rentalObj->user->ssurname16}}</span> ;
                        <?php } ?>

                        Class: <span style="text-decoration:underline">{{$users[0]->class_number}}{{$users[0]->class_letter }}</span>;
                        

                        Hosting Organisation: <span style="text-decoration:underline">{{$rentalObj->host->orgname}}</span>;</span>
                </span>
        </p>
        <p>&nbsp;</p>
        <div dir="ltr" class="main_div">
            <table border="2" align="center">
                <colgroup>
                    <col width="60"/>
                    <col width="119"/>
                    <col width="119"/>
                    <col width="70"/>
                    <col width="202"/>
                    <col width="143"/>
                </colgroup>
                <tbody>
                    <tr align="center">
                        <th>Date</th>
                        <th>Morning working hours</th>
                        <th>Afternoon working hours</th>
                        <th>Number of hours</th>
                        <th>Daily activity performed</th>
                        <th>(Work Tutor&rsquo;s) Signature</th>
                    </tr>
                    <?php for ($i = 0; $i < 6; $i++) {
                        ?>
                        <tr style="height:16pt">
                            <td rowspan="4">
                                &nbsp;</td>
                            <td rowspan="2">
                                &nbsp;</td>
                            <td rowspan="2">
                                &nbsp;</td>
                            <td rowspan="4">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td rowspan="4">
                                &nbsp;</td>
                        </tr>
                        <tr style="height:16pt">
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr style="height:16pt">
                            <td rowspan="2">
                                &nbsp;</td>
                            <td rowspan="2">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr style="height:16pt">
                            <td>
                                &nbsp;</td>
                        </tr>
                        <?php }
                    ?>                                
                </tbody>
            </table>
        </div>
        <p>&nbsp;</p>
        <div class="cust_footer_div">
            <p dir="ltr" class="cust_footer_p">
                <span>
                    <span class="cust_footer_span">Work Tutor&rsquo;s optional comment: ___________________________________________________</span>
                </span>
            </p>
        </div>
        <div>&nbsp;</div>
    </body></html>