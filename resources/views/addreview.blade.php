@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">    

        <div class="col-xs-12 col-sm-12 margin_top20">
            <div class="airfcfx-panel panel panel-default">

                {!! Form::open(array('url' => 'user/review_submit',   'method' => 'POST')) !!}
                @include('partials.errors')

                <input type="hidden" value="" id="ratingval" name="ratingval">
                <input type="hidden" value="{{$enquiryObj->user_id}}" id="reviewerId" name="reviewerId">
                <input type="hidden" value="{{$enquiryObj->prd_id}}" id="rentalId" name="rentalId">
                <input type="hidden" value="{{$enquiryObj->renter_id}}" id="reviewedId" name="reviewedId">
                <input type="hidden" value="{{$enquiryObj->id}}" id="enquiryId" name="enquiryId">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">Add Your Review </h3>
                </div>

                <div class="airfcfx-panel-padding panel-body">
                    <div class="row">
                        <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Property Information</label>
                            <div class="col-sm-9">
                                <div class="form-group field-resetpasswordform-newpassword required">

                                    <textarea disabled="true" class="form-control"  name="password" style="width:50%;">{{$enquiryObj->property->title}} </textarea>

                                    <p class="help-block help-block-error"></p>
                                </div>                            </div>
                        </div>

                        <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Your Comments</label>
                            <div class="col-sm-9">
                                <div class="form-group field-resetpasswordform-confirmpassword required">
                                    <textarea class="form-control" rows="10" cols="15" name="description" required>  </textarea>

                                    <p class="help-block help-block-error"></p>
                                </div>                            </div>
                        </div>


                        <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Your Rating</label>
                            <div class="col-sm-9">
                                <div class="form-group field-resetpasswordform-confirmpassword required">
                                    <div id="jRate"></div>
                                    <p class="help-block help-block-error"></p>
                                </div>                            </div>
                        </div>



                    </div> <!--row end -->
                </div>
                <div class="airfcfx-panel-footer panel-footer">
                    <div class="text-right"><button class="airfcfx-panel btn btn_email margin_bottom10">Submit</button></div>
                </div>
                </form>      </div>  <!--Panel end -->       


            <!--div class="panel panel-default">
                <div class="panel-heading profile_menu1">
                  <h3 class="panel-title">Login Notifications  </h3>
                </div>
                
                <div class="panel-body">
                      <div class="row margin_top10">
                                      <div class="col-xs-12">                        
                              <div class="checkbox margin_bottom20">
                                      <label>
                                        <input type="checkbox">   Turn on login notifications  
                                      </label>
                                    </div>
                              
                              <p>Login notifications are an extra security feature. When you turn them on, we’ll let you know when anyone logs into your Airbnb account from a new browser. This helps keep your account safe. </p>
                              </div>
                          </div> 
                </div>
                <div class="panel-footer">
                      <div class="text-right"><button class="btn btn_email  ">Save</button></div>
                </div>
                
            </div-->  <!--Panel end -->  




        </div> <!--col-sm-9 end -->

    </div> <!--container end -->
</div>

<script src="{{ asset('frontendassets/js/jRate.js')}}"></script> 
<script>

    $(document).ready(function () {
        $(".show_ph").click(function () {
            $(".add_phone").show();
            $(".show_ph").hide();
        });
        $(".add_cont").click(function () {
            $(".add_contact").toggle();
        });
        $(".add_ship").click(function () {
            $(".add_shipping").toggle();
        });
        $("#jRate").jRate({
            startColor: 'cyan',
            endColor: 'blue',
            precision: 1,
            onSet: function (rating) {
                $('#ratingval').val(rating);
            }
        });
    });
</script>  

<script type="text/javascript">


</script>
@stop