 <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- <title>Laravel</title> -->
    </head>
    
@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<style type="text/css">
    .stripe-button-el{display: none;}
</style>
<div class="bg_gray1">
    <div class="container">  
        <div class="col-xs-12 col-sm-12 margin_top20">        

            <div class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel-padding airfcfx-panel panel-heading profile_menu1">
                    <h3 class="panel-title">Buy Nights</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="trips-cnt margin_top10 margin_bottom20 paddingleft table-responsive col-sm-12 col-lg-12">
                            <table class="table table_no_border tripdesign">
                                <tbody>
                                    <tr>
                                    <td>
                                        <form action="{{url('user/nightCheckoutEuro')}}" method="POST">
                                        {{ csrf_field() }}
                                            <!-- data-key="pk_test_pIaGoPD69OsOWmh1FIE8Hl4J" -->
                                            <!-- {{env('STRIPE_KEY')}} -->
                                            <!-- data-amount="13131" -->
                                            <!-- data-currency="{{env('STRIPE_CURRENCY')}}" -->

                                            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="pk_live_IQpSff9ejsp7Yzq2Lyi5PK3q00L0Wr2Y9Z"
                                            data-amount="00100"
                                            data-name="Stripe PG"
                                            data-description="Online course about integrating Stripe"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-locale="auto"
                                            data-email="esperienzainglese@gmail.com"                                            
                                            data-currency="EUR"
                                            >

                                            </script>
                                            <!-- <input type="hidden" name="amount" value="13131" id="7amounts" class="amount"> -->
                                            <input type="hidden" name="amount" value="1" id="7amounts" class="amount">
                                            <input type="hidden" name="nights" value="7" id="7nights" class="nights">
                                            <input type="submit" class="btn" value="Buy One week" name="buy_nights">
                                        </form>
                                    </td>
                                    <td>
                                        <form action="{{url('user/nightCheckoutEuro')}}" method="POST">
                                        {{ csrf_field() }}            <!-- data-key="pk_test_pIaGoPD69OsOWmh1FIE8Hl4J" -->
                                            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="{{env('STRIPE_KEY')}}"
                                            data-amount="19441"
                                            data-name="Stripe Demo"
                                            data-description="Online course about integrating Stripe"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-locale="auto"
                                            data-email="esperienzainglese@gmail.com"
                                            data-currency="{{env('STRIPE_CURRENCY')}}">
                                            </script>
                                            <input type="hidden" name="amount" value="19441" id="14amount" class="amount">
                                            <input type="hidden" name="nights" value="14" id="14nights" class="nights">
                                            <input type="submit" class="btn" value="Buy Two weeks" name="buy_nights">
                                        </form>
                                    </td>
                                    <td>
                                        <form action="{{url('user/nightCheckoutEuro')}}" method="POST">
                                        {{ csrf_field() }}            <!-- data-key="pk_test_pIaGoPD69OsOWmh1FIE8Hl4J" -->
                                            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="{{env('STRIPE_KEY')}}"
                                            data-amount="24441"
                                            data-name="Stripe Demo"
                                            data-description="Online course about integrating Stripe"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-locale="auto"
                                            data-email="esperienzainglese@gmail.com"
                                            data-currency="{{env('STRIPE_CURRENCY')}}">
                                            </script>
                                            <input type="hidden" name="amount" value="24441" id="21amount" class="amount">
                                            <input type="hidden" name="nights" value="21" id="21nights" class="nights">
                                            <input type="submit" class="btn" value="Buy Three weeks" name="buy_nights">
                                        </form>                                    
                                    <td>
                                        <!-- <form name="frm_to_buy" method="POST" action=""> -->
                                            <button class="btn" onClick="alert('You can send details on this email esperienzainglese@gmail.com');">More Nights</button>
                                        <!-- </form> -->
                                        </td>
                                    </tr>
                                    <?php
                                    
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div> <!--row end -->
                </div>
            </div> <!--Panel end -->
        </div>
    </div>
</div>@stop