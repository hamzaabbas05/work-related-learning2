@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
      <div class="container">    
	<div class="row">
    	
        <div class="col-xs-12 margin_top20 margin_bottom20">
        	<div class="col-sm-12">
                <div class="panel panel-default margin_top30">
<?php if ($result == "success") { ?> 
                  <div class="panel-body padding10">
	
          		<center class="payment-success"><p>Thanks for your Booking.</p><p><span>Your Payment Successfully completed</span>.</p></center></div>  <!--Panel end -->       
     
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">Booking No</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->Bookingno}}</label> 
                       </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">Booking Date</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->dateAdded}}</label> 
                       </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">Property Name</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->property->title}}</label> 
                       </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">Property Location</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->property->auto_complete}}</label> 
                       </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">CheckIn</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->checkin}}</label> 
                       </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">CheckOut</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->checkout}}</label> 
                       </div>
                        </div>
                         <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">Guests</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->NoofGuest}}</label> 
                       </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">Booking Status</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->booking_status}}</label> 
                       </div>
                        </div>


                        <div class="margin_bottom20"></div> <?php } else if ($result == "pending"){ ?> <div class="panel-body padding10"><center class="payment-success"><p>Your Payment is in Pending.</p><p> </p></center></div>  <!--Panel end -->     <?php } else if ($result == "pending"){ ?>  
                                <div class="panel-body padding10"><center class="payment-success"><p>Your Payment is in Pending.</p><p> </p></center></div><?php } ?>



</div>  
  </div> 
</div>
</div>
</div><!--container end -->
</div>

@stop