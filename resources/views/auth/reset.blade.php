@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
<div class="pos_rel bg_gray1">
	<div class="container">
  <div class="modal-dialog login_width" role="document">
    <div class="modal-content" style="box-shadow: none;">
      <div class="modal-body text-center">
                   @include('partials.errors')
        <div class="login_or border_bottom margin_top10"><span>Reset password</span></div>
                  <form id="" action="{{url('password/reset')}}" method="post" role="form">
             
                  <input type="hidden" name="token" value="{{$token}}">
                <div class="form-group field-signupform-email required">

<input type="text"  class="form-control margin_top30 margin_bottom10" name="email" placeholder="Register Email">

<p class="help-block help-block-error"></p>
</div>
                <div class="form-group field-signupform-password required">

<input type="password"  class="form-control margin_bottom10" name="password" placeholder="Password">

<p class="help-block help-block-error"></p>
</div>

 <div class="form-group field-signupform-password required">

<input type="password"  class="form-control margin_bottom10" name="password_confirmation" placeholder="Confirm Password">

<p class="help-block help-block-error"></p>
</div>
  <div class="form-group">
                    <button type="submit" class="btn btn_email margin_top10 width100" name="login-button">Submit</button>                </div>
				<img id="loginloadimg" src="{{asset('fronendassets/images/load.gif')}}" class="loading" style="margin-top:-1px;">            </form>      </div>  

    </div>
  </div>
    </div> <!-- container end -->
</div> <!-- list_bg end -->

 
@stop