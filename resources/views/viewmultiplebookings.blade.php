@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">    
        <div class="row">

            <div class="col-xs-12 margin_top20 margin_bottom20">
                <div class="col-sm-12">
				@include('layouts.errorsuccess')
                    <div class="panel panel-default margin_top30">

		
                        <div class="panel-body padding10">
                            <?php if (!\Session::has('bookingno')) { ?> 
                                <center class="payment-success">
                                    <p>Thanks for your application!</p>
                                    <p>
                                        <span>
                                            Organisation (for jobs) and School House (for paid services) may confirm or decline your request, 
                                            in the first case you will receive all the details and instructions via email, else we will contact 
                                            organisations according to your second and third choice (this may take weeks), for School Houses we 
                                            will send further invitation to select another.
<!--                                            Organisation may confirm or decline your application, in the first case you will receive all the documentation 
                                            via email and in the latter case an email notifying you. Expect messages, job interview, CV and other 
                                            documentation request (as references) so to reply promptly.-->
                                        </span>.
                                    </p>
                                </center>   
                                    <?php } ?>  				   
                        </div>  <!--Panel end -->       

                        <div class="row">
                            <div class="col-md-12" style="padding:30px;" >
								<table class="booking-requests-table" style="width:100%;text-align:center" border="1">
									<thead>
										<tr>
											<th>Booking No</th>
											<th>WorkExperience Title</th>
											<th>Booking date</th>
											<th>CheckIn date</th>
											<th>Checkout date</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
									<?php 
									foreach($all_requests as $request){
										if($request->property->property_type == 26){
											$bed_guest_room_id = $request->property->bed_guest_room_id;
											$bed_type_id = $request->property->bed_type_id;
											
											$guest_room = \App\UserPaidGuestRoom::where('id',$bed_guest_room_id)->first();
											$room_type_id = $guest_room->room_type_id;
											$room_type = \App\RealRoomTypes::where("is_active", "1")->where("id", $room_type_id)->first();
											
											$bed_type = \App\BedType::where(["is_active" => "1", "id" => $bed_type_id])->first();
											
											
											$room_name = $room_type->room_type.' Room ';
											$bed_name = $bed_type->bed_type.' Bed';
											$request_title = $room_name.' - '.$bed_name;
										}
										if($request->property->property_type == 24){
											$bed_guest_room_id = $request->property->bed_guest_room_id;
											
											$class_room = \App\UserPaidGuestRoom::where('id',$bed_guest_room_id)->first();
											
											$room_name = $class_room->room_name.' ';
											$bed_name = $request->property->exptitle;
											$request_title = $bed_name;
										}
										
										echo '<tr>
												<td>'.$request->Bookingno.'</td>
												<td>'.$request_title.'</td>
												<td>'.$request->dateAdded.'</td>
												<td>'.$request->checkin.'</td>
												<td>'.$request->checkout.'</td>
												<td>'.$request->approval.'</td>
											</tr>';										
									}
									?>
									</tbody>
								</table>
                              
                            </div>
                            
                        </div>
                        
                        <?php if (\Session::has('bookingno')) {
                            \Session::forget('bookingno'); ?> 
                            {!! Form::open(array('route' => 'getCheckout', 'method' => 'post')) !!}
                            {!! Form::hidden('enquiryId', $enquiryObj->id) !!}
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 text-right">
                                    <label class="profile_label">Checkout</label> 
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <input type="submit" value="Pay With Paypal" class="btn btn-danger">
                                </div>
                            </div>
                            </form>
<?php } ?>


                        <div class="margin_bottom20"></div>
                    </div>  
                </div> 
            </div>
        </div>
    </div><!--container end -->
</div>

@stop