@extends('layouts.frontdefault')
@section('header_styles')
<link href="{{ asset('frontendassets/css/jslider.css')}}" rel="stylesheet">
@stop
@section('header_js')
<script src="{{ asset('frontendassets/js/jquery.slider.bundle.js')}}"></script>
<script src="{{ asset('frontendassets/js/jquery.slider.js')}}"></script>
@stop
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('propertySettings', 'App\PropertySettings')
<?php 
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<div class="navbar-fixed-top header_top_invidual">
<div class="container">

<div class="row">
<div class="padding-top10 col-xs-12 col-sm-8">
<ul class="list-inline">
<li><a href="#about" class="text_white rm_text_deco1 text_focus_color highlightcls" id="aboutcls"> About this listing</a></li>
<li><a href="#photo" class="text_white rm_text_deco1 text_focus_color highlightcls" id="photocls">Photos </a></li>
<!--li><a href="#review" class="text_white rm_text_deco1"> Reviews</a></li-->
<li><a href="#host" class="text_white rm_text_deco1 text_focus_color highlightcls" id="hostcls">The Host</a></li>
<li><a href="#location" class="text_white rm_text_deco1 text_focus_color highlightcls" id="locationcls">Location</a></li>
</ul>
</div><!--col-sm-8 end-->

<div class="col-xs-12 col-sm-4 padding-right0">

<div class="bg_black header_adj">

<div class="row">

<div class="margin_topr10 col-xs-12 col-sm-8">
    <h3 class="margin_bottom0">€ {{$propertyObj->price_night}}</h3>
</div><!--col-sm-8 end--> 
<button type="button" class="close mobileviewreq" onclick="closereqpopup();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<div class="col-xs-12 col-sm-4 text-right headreqmob">
<p>Per Night</p>
</div><!--col-sm-4 end--> 
</div><!--row end-->

</div><!--header_adj end-->

</div><!--col-sm-4 end-->

</div><!--row end-->

</div><!--container end-->
</div><!--row end-->
<?php foreach($propertyObj->propertyimages as $img) { 
            $imgURL = asset('images/rentals/'.$img->property_id.'/'.$img->img_name);
        }
?>


<div class="banner2" style='background-image:url(<?php echo $imgURL;?>);'></div>  


<div class="bg_white border1 ">

<div class="container tpp">
<div class="row">
<div class="col-xs-12 col-sm-3 col-md-2 text-center">
    <div class="profile margin_top30 center-block" style="background-image:url({{asset('images/profile/'.$propertyObj->user->profile_img_name)}});"> </div> 
<!--    <a href="" class="text_gray1 margin_top20 center-block">{{$propertyObj->user->name}}</a>-->
</div><!--col-sm-2 end-->
<input type="hidden" id="listingid" value="{{$propertyObj->id}}"><div class="col-xs-12 col-sm-5 col-md-6 margin_top30 margin_bottom20 listtxtalgn"><h3 class="font-size22"><b>{{$propertyObj->title}}
</b></h3>
 <p class="text_gray1">{{$propertyObj->auto_complete}}</p>

<div class="row text-center">
<div class="col-xs-12 col-sm-3 margin_top20 listroomdiv">
<p class="text_gray1"><span class="height42"><img class="margin_bottom5" src="{{ asset('frontendassets/images/room.png')}}"></span><br/>{{$propertyObj->propertytype->name}}</p>
</div><!--col-sm-3 end-->
<div class="col-xs-12 col-sm-3 margin_top20 listroomdiv">
<p class="text_gray1"><span class="height42"><img class="margin_bottom5" src="{{ asset('frontendassets/images/users.png')}}"></span><br/>{{$propertyObj->accomodates}} Guests </p>
</div><!--col-sm-3 end-->
<div class="col-xs-12 col-sm-3 margin_top20 listroomdiv">
<p class="text_gray1"><span class="height42"><img class="margin_bottom5" src="{{ asset('frontendassets/images/door.png')}}"></span><br/> {{$propertyObj->bed_rooms}} Bedroom </p>
</div><!--col-sm-3 end-->
<div class="col-xs-12 col-sm-3 margin_top20 listroomdiv">
<p class="text_gray1"><span class="height42"> <img class="margin_bottom5" src="{{ asset('frontendassets/images/bed.png')}}"> </span><br/> {{$propertyObj->beds}} Bed </p>
</div><!--col-sm-3 end-->
</div><!--row end-->

</div><!--col-sm-6 end-->

<div class="col-xs-12 col-sm-4 col-md-4 pos_rel wid">

<div class="pos_abs make_fix">

<div class="border1  bg_white">

<div class="bg_black padding10 margin_adj clearfix ">

<div class="col-xs-12 col-sm-8">
<h3 class="margin_bottom0"> € {{$propertyObj->price_night}} </h3>
</div><!--col-sm-8 end--> 
<button type="button" class="close mobileviewreq" onclick="closereqpopup();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<div class="col-xs-12 col-sm-4 text-right headreqmob">
<p>Per Night</p>
</div><!--col-sm-4 end--> 

</div><!--row end-->


<div class="table1 margin_top10 margin_left5 margin_bottom10 padd_5_10 checkinalgn caltxt">

<div class="checkindivcls">
<label>Check In</label>
<input type="text" id="startdate" readonly class="font-size12 form-control margin10 cal" placeholder="DD-MM-YYYY">
</div>
<div class="checkindivcls">
<label>Check Out</label>
<input type="text" id="enddate" readonly class="font-size12 form-control margin10 cal" placeholder="DD-MM-YYYY">
</div>
<div class="guestdivcls">
<label>Guests</label>
<select class="form-control margin10" id="guests">

<?php for($i=1; $i<=$propertyObj->accomodates; $i++) { ?>
                              <option  value="{{$i}}"><?php echo "Guest".$i;?></option>
                              <?php  } ?>
</select>
</div>
</div><!--table1 end-->
<div id="pricediv" class="hiddencls">
<input type="hidden" id="commissionprice">
    <div class="table1 padd_5_10">
        <div class="table1">
            <div class="tab_cel padding5 ">
                {{$propertyObj->price_night}} x <span id="noofdays"></span> nights</div>
            <div class="tab_cel padding5 " style="text-align:right;">
               € <b> <span id="listtotalprice"></span></b>
            </div>
        </div> 
          <div class="table1">
            <div class="tab_cel padding5 ">
            Guest Commission
            </div>
            <div class="tab_cel padding5 " style="text-align:right;">
               € <b> <span id="guest_commission"></span></b>
            </div>
        </div> 

        <div class="table1">
            <div class="tab_cel padding5 ">
            Tax
            </div>
            <div class="tab_cel padding5 " style="text-align:right;">
               € <b> <span id="tax"></span></b>
            </div>
        </div> 
 <hr class="margin3" />
        <div class="table1">
         <hr class="margin3" />
        <div class="table1">
            <div class="tab_cel padding5 ">
              Total 
            </div>
            <div class="tab_cel padding5 " style="text-align:right;">
               € <span id="totalprice"></span>
            </div>
        </div>
    </div>
</div>
<div class="errcls centertxt" id="maxstayerr"></div>

<div class="padding10 margin_left5 margin_right5 margin_bottom10">
@if (Auth::guest())
<a href="{{url('signin')}}" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" >Login</a>
@else
<button type="button" onclick="send_reserve_request({{$propertyObj->id}});" id="requestbtn" class="btn-pad btn btn-danger full_width"><b>Request Book</b></button>
@endif
 <center><img id="paypalloadingimg" src="/images/load.gif" class="loading" style="margin-top:5px;"></center>
    <div class="errcls centertxt" id="emailverifyerr"></div>
    </div>



<div class="payment-form hiddencls"></div>
</div><!--border1 end-->
<div class="bg_white  padd_lf_rg_15 border1 margin_top50">

@if (Auth::guest())
<a href="{{url('signin')}}" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" >Login
 
</a>
@else
 <button type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" data-toggle="modal" data-target="#myModal" onclick="show_list_popup(event,<?php echo $propertyObj->id;?>);">
<i class="fa fa-heart-o"></i> Save To Wish List</button>@endif






<div class="row  text-center margin_top20">
<!--<div class="col-xs-12 col-sm-4   bg_white padding10 border_right">
<a href="#" class="text_black"><p> <i class="fa fa-envelope-o"></i> Email</p></a>
</div> -->
<!-- <div class="col-xs-12 col-sm-4  bg_white padding10 border_right">
<a href="#" class="text_black"><p> <i class="fa fa-comment"></i>Messenger </p></a>
</div> -->
<!-- <div class="col-xs-12 col-sm-4  bg_white padding10">
<a href="#" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle text_black"><p> <i class="fa fa-ellipsis-h"></i>More</p></a>

<ul class="dropdown-menu padding20">
                            <a href="#" class="text_black"><li class="">Facebook</li></a>
                            <a href="#" class="text_black"><li class=" margin_top5">Twitter</li></a>
                            <a href="#" class="text_black"><li class=" margin_top5">Embedded this Link</li></a>
                            <a href="#" class="text_black"><li class=" margin_top5">Google +</li></a>
                            <a href="#" class="text_black"><li class=" margin_top5">Pinterest</li></a>
                        </ul>

                    
</div> -->
</div></div><!--bg_white end-->

</div><!--pos_end-->

</div><!--col-sm-4 end-->



</div><!--row end-->

</div><!--container end-->
</div><!--bg_white end-->
<body class="bg_gray1">
<div class="container">


<div class="row">
<div class="col-xs-12 col-sm-12 col-md-8">
<span id="about" class="scrollhighlight"> </span>
<h3 class="margin_top30 "><b>About this listing </b></h3>

<p class="margin_top20" style="word-wrap:break-word;">{{$propertyObj->description}}</p>
<div class="border_bottom margin_top10"></div>


<div class="row">
<div class="col-xs-12 col-sm-4">
<p class="margin_top20 headtxt text_gray1">The Space</p>
</div><!--col-sm-4 end-->

<div class="col-xs-12 col-sm-4">
<p class="margin_top20">Accommodates: <b class="font-size14">{{$propertyObj->accomodates}}</b></p>
<p>Bathrooms: <b class="font-size14">{{$propertyObj->bathrooms}}</b></p>
<p>Bedrooms: <b class="font-size14">{{$propertyObj->bed_rooms}}</b></p></div><!--col-sm-4 end-->

<div class="col-xs-12 col-sm-4">
<p class="margin_top20">Beds: <b class="font-size14">{{$propertyObj->beds}}</b></p>
<p>Property type:<b class="font-size14"> {{$propertyObj->propertytype->name}}</b></p>
<p>Room type: <b class="font-size14"> {{$propertyObj->roomtype->name}}</b></p>

</div><!--col-sm-4 end-->
</div><!--row end-->


<div class="border_bottom margin_top10"></div>

<div class="row">
<div class="col-xs-12 col-sm-4">
<p class="margin_top20 headtxt text_gray1">Amenities</p>
</div><!--col-sm-4 end--><div id="lessamenity">
    <?php $i = 1;foreach($propertyObj->amenities as $amenity) {
            if ($amenity->type == 1) {
        ?> 

        <div class="col-xs-12 col-sm-4">
         <p class="margin_top20">
         <div class="airfcfx-amenities-icon" id="wifi-icon" style="background-image:url({{asset('images/amenity/'.$amenity->icon)}}) !important;"></div>
         <span class="airfcfx-amenity-name" title="" data-placement="top" data-toggle="tooltip" data-original-title="Coolness">{{$amenity->name}}</span></p></div>
         
 <?php if($i == 2) { ?><div class="margin_top15 col-xs-12 col-sm-4"></div> <?php }
?> 


 <?php $i++;} } ?>
 
 
        </div>
         </div>





        <!--row end-->

<div class="border_bottom margin_top10"></div>

<div class="row" id="prices">
<div class="col-xs-12 col-sm-4">
<p class="margin_top20 headtxt text_gray1">Prices</p>
</div><!--col-sm-4 end-->

<div class="col-xs-12 col-sm-5"><p class="margin_top20">  Price: <b>€ {{$propertyObj->price_night}} / night</b></p><p class="margin_top20">  Security Deposit:  <b>€ {{$propertyObj->secutity_deposit}}</b></p></div><!--col-sm-4 end-->

</div><!--row end-->

<div class="border_bottom margin_top10"></div>


<div class="row" id="description">
<div class="col-xs-12 col-sm-4">
<p class="margin_top20 headtxt text_gray1">Description</p>
</div><!--col-sm-4 end-->

<div class="col-xs-12 col-sm-8">
<p class="margin_top20" style="word-wrap:break-word;">
 {{$propertyObj->description}}
</p>
</div><!--col-sm-8 end-->

</div><!--row end-->
<div class="border_bottom margin_top10"></div>
<div class="row" id="houserules">
<div class="col-xs-12 col-sm-4">
<p class="margin_top20 headtxt text_gray1">House Rules</p>
</div><!--col-sm-4 end-->

<div class="col-xs-12 col-sm-8">
<p class="margin_top20" style="word-wrap:break-word;">
 {{$propertyObj->houserules}}
</p>
</div><!--col-sm-8 end-->

</div>
<div class="border_bottom margin_top10"></div>


<div class="row">
<div class="col-xs-12 col-sm-4">
<p class="margin_top20 headtxt text_gray1">Safety Features</p>
</div><!--col-sm-4 end-->

        <?php $i = 1;foreach($propertyObj->amenities as $amenity) {
            if ($amenity->type == 3) {
        ?> 

        <div class="col-xs-12 col-sm-4">
        <p class="margin_top20">{{$amenity->name}}</p>
        </div>
        <?php if($i == 2) { ?><div class="margin_top15 col-xs-12 col-sm-4"></div> <?php }
?> 
<?php $i++;} }?>


        </div><!--row end-->


<div class="border_bottom margin_top20"></div>


<div class="row" id="availability">
<div class="col-xs-12 col-sm-4">
<p class="margin_top20 headtxt text_gray1">Availability</p>
</div><!--col-sm-4 end-->

<div class="col-xs-12 col-sm-4">

<p class="margin_top20"><b>{{$propertyObj->min_stay}} night</b> minimum stay</p>
</div><!--col-sm-4 end-->
<div class="col-xs-12 col-sm-4">

<p class="margin_top20"><b>{{$propertyObj->max_stay}} night</b> maximum stay</p>
</div>

<div class="airfcfx-view-calendar col-xs-12 col-sm-4">

<a href="javascript:void(0);" onclick="focus_checkin();"><p class=" text-danger margin_top20"><b> + View Calendar</b></p></a>
</div><!--col-sm-4 end-->

</div><!--row end--><span class=" scrollhighlight" id="photo">

 

<link rel="stylesheet" href="{{ asset('frontendassets/css/supersized.css')}}" type="text/css" media="screen" />
<link rel="stylesheet" href="{{ asset('frontendassets/css/supersized.shutter.css')}}" type="text/css" media="screen" />
<script type="text/javascript" src="{{ asset('frontendassets/js/jquery.easing.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('frontendassets/js/supersized.3.2.7.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('frontendassets/js/supersized.shutter.min.js')}}"></script>

 


</div><!--col-sm-8 end-->

</div><!--row end-->

</div><!--container end-->
</body>
<!-- <div class="bg_white margin_top30 border1 hiddencls">
<div class="container">
<h3 class="margin_top20">1 Review <span id="review"></span></h3>


<div class="row">
<div class="col-xs-12 col-sm-8">

<div class="row">
<div class="col-xs-12 col-sm-4 text-center padd_bottom30">
<div class="profile margin_top20 center-block" style="background-image:url({{asset('images/profile/'.$propertyObj->user->profile_img_name)}});"> </div>
<a class="text_gray1 margin_top10 center-block" href="#">Gabrielle</a>
</div> 

<div class="col-xs-12 col-sm-8">
<p>Gabrielle is a delightful person and a fantastic host. Nothing was too much trouble and she made sure I had everything I needed. Gabrielle introduced me to her neighbours/friends, which made me feel like </p>


<div class="row margin_top20">
<div class="col-xs-12 col-sm-6"><p class="text-muted margin_top10">October 2015</p></div><!--col-sm-6 end-->
<!-- <div class="col-xs-12 col-sm-6 text-right"><button type="button" class="btn btn-default"> <i class="fa fa-thumbs-o-up"></i> Helpful</button></div> -->
</div> 



</div> 
</div> 

</div> 
</div> 


</div> 
</div>  -->

<!-- <div class="bg_white margin_top30 border1">
<div class="container">
<h3 class="margin_top20 margin_bottom10"><b>No reviews yet</b></h3>
<div class="margin_bottom20">Stay here and you could give this host their first review!</div>
</div>
</div> -->




<div class="container margin_top30 stopfixer">
<h3><b>About the Host, {{$propertyObj->user->name}} <span id="host" class="scrollhighlight"></span></b></h3>

<div class="row">
<div class="col-xs-12 col-sm-12"col-md-8>


<div class="row">

<div class="col-xs-12 col-sm-4 col-md-2">
<div class="profile margin_top20 center-block" style="background-image:url({{asset('images/profile/'.$propertyObj->user->profile_img_name)}});"> </div>
</div><!--col-sm-4 end-->

<div class="col-xs-12 col-sm-8 col-md-2 xs-center-txt">
<p class="margin_top40">{{$propertyObj->auto_complete}}<br />Member since {{date_format($propertyObj->user->created_at, 'jS F Y')}}</p></div>

<!--col-sm-4 end-->

</div><!--row end-->


<div class="row hiddencls">

<div class="col-xs-12 col-sm-4">
<p class="margin_top30">Connections</p>
</div><!--col-sm-4 end-->

<div class="col-xs-12 col-sm-8">
<p class="margin_top20">Are you or your friends connected with this host?</p>
<button type="button" class="btn fb_btn margin_top10"> <i class="fa fa-facebook"></i>  Connect With Facebook</button>
</div><!--col-sm-4 end-->

</div><!--row end--><div class="row hiddencls">

<div class="col-xs-12 col-sm-4">
<p class="margin_top30">Trust</p>
</div><!--col-sm-4 end-->

<div class="col-xs-12 col-sm-8">

<button type="button" class="btn btn-warning margin_top20"> 1</button>
<p>Review</p>
</div><!--col-sm-4 end-->

</div><!--row end-->

</div><!--col-sm-8 end--></div><!--row end-->


<!--iframe src="https://www.google.com/maps/@11.0136259,76.8978671,14.25z?hl=en-US"  width="100%" 
height="450" frameborder="0" style="border:0; " class="margin_top30 map"  ></iframe-->
<span id="location" class="scrollhighlight"></span>
<div class="map margin_bottom30">
<span class="map1"></span>
<!--span id="location"></span-->
<div id="map_canvas" frameborder="0" style="border:0; " class="margin_top30 map"></div>



</div>


<h3 class="margin_top30 hiddencls">Explore other options in and around 17th arrondissement </h3>

<p class="margin_bottom30 hiddencls">More places to stay in 17th arrondissement: Houses · Bed & Breakfasts · Lofts · Villas · Condominiums<br /><br />
People also stay in Saint-Ouen · Levallois-Perret · Asnieres-sur-Seine · Neuilly-sur-Seine · Courbevoie
</p>



</div><!--container end-->





<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog listpopupwidth" role="document">
    <div class="modal-content">

      <div class="modal-body padding0">
      	<div class="toplistdiv" style="display:none;">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	      	<span aria-hidden="true">&times;</span>
	      </button>   
	      <h3>Save to Wish List</h3>          
	      <hr />
	</div>
     
      	  <div class="airfcfx-leftlistdiv leftlistdiv">
                  	  	<div class="banner2 banner2hgt"  id="listimage"></div>
      	  </div>
	<div class="airfcfx-rightlistdiv-cnt">
	 <div class="airfcfx-rightlistdiv rightlistdiv padding20 wishlisthgt">
	 
	 <div class="airfcfx-topfullviewdiv topfullviewdiv">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
	      <h3>Save to Wish List</h3><hr />
         </div>
		 
	 <div class="airfcfx-wishlist-contianer" id="listnames"> 

     </div>
		  </div>
		  <div class="airfcfx-wish-createlist-cnt">
			  <input type="text" id="newlistname1" class="airfcfx-listtxt listtxt" value="" placeholder="Create New List" maxlength="20">
			  <input type="button" value="create" class="airfcfx-createbtn btn btn-danger createbtn" onclick="create_new_list();">
		  </div> 
          <div class="airfcfx-wishlist-btn-cnt">
        	<input type="button" value="Cancel" class="airfcfx-cancelsze btn btn_email cancelsze cancelbtn " data-dismiss="modal">
			<input type="button" value="Save" data-dismiss="modal" class="airfcfx-savebtn btn btn-primary savebtn pull-right" onclick="save_lists();">
			<div class="errcls listerr"></div>
         </div>
		</div> 
    		  
	  </div>
      <div class="clear">

      </div>            
  </div>
</div>
</div>

<div class="modal" id="contactform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header no_border">
        <button type="button" class="close" data-dismiss="modal" onclick="checkitclear();" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
      </div>
      <div class="modal-body">

        <div class="col-xs-12 contact-txtbox-align">
        <h3>Contact Host</h3>
        <p>Write your message to Host:</p>
        </div>
        <div class="col-xs-12 contact-txtbox-align margin_top20">
            <textarea id="contactmessage" style="resize:none" cols="50" rows="5" class="form-control" maxlength="250"></textarea>
            <div id="charNum"></div>
        </div>
      </div>
      <input type="hidden" id="userid" value="4">
      <input type="hidden" id="hostid" value="4">
      <input type="hidden" id="listingid" value="893">
            <div class="clear"></div><br />
    <div class="modal-footer">
        <input type="button" id="send_msg" value="Send Message" class="btn btn-danger" onclick="send_contact_message();">
        <img id="loadingimg" src="/images/load.gif" class="loading" style="margin-top:-1px;">        <div id="succmsg" class="successtxt hiddencls">Message Sent Successfully</div><br/>
        <div class="msgerrcls"></div>
      
    </div>
	  </div>
      <div class="clear">
      </div>            
  </div>
</div>  

<!--request book mobile popup-->
<div class="modal fade" id="airfcfx-mobile-req" role="dialog"> 
				<div class="modal-dialog mobile-cal-cnt">
					<div class="mobile-cal-header">
						<button class="close airfcfx-mobile-cal-close" type="button" data-dismiss="modal" onclick="closereqpopup();">×</button>
						Booking						<div class="airfcfx-mobile-cal-blackdiv">
							<div class="airfcfx-mobile-black-innerdiv airfcfx-mobile-rate">฿17409.6</div>
							<div class="airfcfx-mobile-black-innerdiv per-night">Per Night</div>
						</div>
					</div>
					<div class="mobile-cal-body">
						<input id="startdatemobile" value="" readonly class="mobile-cal-input-50 airfcfx-check-cal form-control cal form_text2 check-in" placeholder="Check in">
						<input id="enddatemobile" value="" readonly class="pull-right mobile-cal-input-50 airfcfx-check-cal form-control cal form_text2 check-out" placeholder="Check Out ">
						<select id="guestsmobile" class="mobile-cal-input-100 airfcfx-guest-count form-control form_text2 guest-count">
                            <option value=1>1</option><option value=2>2</option>						</select>
                        <div id="pricedivmobile" class="hiddencls">
<input type="hidden" id="commissionpricemobile">
    <div class="table1 padd_5_10">
        <div class="table1">
            <div class="tab_cel padding5 ">
                17409.6 x <span id="noofdaysmobile"></span> nights+ Security Deposit+ Commission</div>
            <div class="tab_cel padding5 " style="text-align:right;">
                <b>฿ <span id="listtotalpricemobile"></span></b>
            </div>
        </div><hr class="margin3" />
        <div class="table1">
            <div class="tab_cel padding5 ">
               Service fee
            </div>
            <div class="tab_cel padding5 " style="text-align:right;">
                ฿ <span id="sitepricemobile"></span>
            </div>
        </div><hr class="margin3" />
        <div class="table1">
            <div class="tab_cel padding5 ">
               Occupancy Taxes
            </div>
            <div class="tab_cel padding5 " style="text-align:right;">
               ฿ <span id="taxpricemobile"></span>
            </div>
        </div><hr class="margin3" />
        <div class="table1">
            <div class="tab_cel padding5 ">
              Total 
            </div>
            <div class="tab_cel padding5 " style="text-align:right;">
               ฿ <span id="totalpricemobile"></span>
            </div>
        </div>
    </div>
</div>
                        <div class="errcls centertxt" id="maxstayerrmobile"></div>    <center><img id="paypalloadingimg" src="/images/load.gif" class="loading" style="margin-top:5px;"></center>
    <div class="errcls centertxt" id="emailverifyerr"></div>					</div>
				</div>
			</div>

<!--instant book mobile popup-->
<div class="modal fade" id="airfcfx-mobile-inst" role="dialog"> 
				<div class="modal-dialog mobile-cal-cnt">
					<div class="mobile-cal-header airfcfx-0-bottom-padding">
						<button class="close airfcfx-mobile-cal-close" type="button" data-dismiss="modal">×</button>
						Search
						<div class="airfcfx-mobile-cal-blackdiv">
							<div class="airfcfx-mobile-black-innerdiv airfcfx-mobile-rate">$25640</div>
							<div class="airfcfx-mobile-black-innerdiv per-night">Per Night</div>
						</div>
					</div>
					<div class="mobile-cal-body">
						<input id="check-in-main" value="" readonly class="mobile-cal-input-50 airfcfx-check-cal form-control cal form_text2 check-in" placeholder="Check in">
						<input id="check-out-main" value="" readonly class="pull-right mobile-cal-input-50 airfcfx-check-cal form-control cal form_text2 check-out" placeholder="Check Out ">
						<select id="guest-count" class="mobile-cal-input-100 airfcfx-guest-count form-control form_text2 guest-count"><option>Guest</option>
						</select>
						<div class="airfcfx-room-book-detail">
							<div class="airfcfx-room-book-detail-cnt">
								<div class="airfcfx-room-book-detail-txt">₹15143 x 2 nights</div>
								<div class="airfcfx-room-book-detail-rate">₹30286</div>
							</div>
							<div class="airfcfx-room-book-detail-cnt">
								<div class="airfcfx-room-book-detail-txt">Cleaning Fee</div>
								<div class="airfcfx-room-book-detail-rate">₹30286</div>
							</div>
							<div class="airfcfx-room-book-detail-cnt">
								<div class="airfcfx-room-book-detail-txt">Service Fee</div>
								<div class="airfcfx-room-book-detail-rate">₹30286</div>
							</div>
							<div class="airfcfx-room-book-detail-cnt">
								<div class="airfcfx-room-book-detail-txt">Total</div>
								<div class="airfcfx-room-book-detail-rate">₹30286</div>
							</div>
						</div>
						
						<button class="airfcfx-mobile-cal-btn airfcfx-slider-btn btn btn_search">Instant book</button>
					</div>
				</div>
			</div>
 
  
<script type="text/javascript">
    function checkitclear(){
        //$('#contactmessage').val('');
        $('.msgerrcls').val('');
        $('.msgerrcls').hide();
    }

</script>		  
<!--script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script-->

<script>
    /*var mapOptions = {
	zoom: 10,
	mapTypeId: google.maps.MapTypeId.SATELLITE,
	scrollwheel: false      
}
map = new google.maps.Map(document.getElementById("map"), mapOptions);*/

/*function initialize() {
    lat = 53.342648;
    long = -6.248425;
    var map_canvas = document.getElementById('map_canvas');
    var map_options = {
        center: new google.maps.LatLng(lat, long),
        zoom:8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(map_canvas, map_options)
        }
google.maps.event.addDomListener(window, 'load', initialize);*/
    
            var map;
            var geocoder;
            var centerChangedLast;
            var reverseGeocodedLast;
            var currentReverseGeocodeResponse;

            function initialize() {
	autocomplete = new google.maps.places.Autocomplete((document
			.getElementById('where-to-go')), {
		types : [ 'geocode' ]
	});
	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		fillInAddress();
	});      

        var geocoder = new google.maps.Geocoder();
        var address = '220 boyne st,Dublin';

var latitude;
var longitude;
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            latitude = results[0].geometry.location.lat();
            longitude = results[0].geometry.location.lng();
                var latlng = new google.maps.LatLng(latitude,longitude);
                var myOptions = {
                    zoom: 10,
                    center: latlng,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

var myCity = new google.maps.Circle({
  center:latlng,
  radius:7000,
  strokeColor:"#8fe3de",
  strokeOpacity:0.8,
  strokeWeight:2,
  fillColor:"#8cdfd9",
  fillOpacity:0.4
  });

myCity.setMap(map);

                geocoder = new google.maps.Geocoder();

                /*var marker = new google.maps.Marker({
                    position: latlng,
		    icon : '../images/green-dot.png',
                    map: map,
                    title: "Hello World!"
                });*/
          } 
        });                 

            }
            google.maps.event.addDomListener(window, 'load', initialize);

//slideer stop
$('.carousel').carousel({
  interval: false
})
	

//Nav Fixed
$(document).ready(function() {
     var a = $(".wid").outerWidth();
       
 $(".make_fix").css('width', a-15);
     
	 var project2 = $('.tpp').offset();
	var $window = $(window);
$window.scroll(function() {
//start fix
		if ( $window.scrollTop() >= project2.top) {
            $(".make_fix").addClass("fixed");
			jQuery('.header_top_invidual').addClass('filter_menu1');
		}
		else {
            $(".make_fix").removeClass("fixed");
			jQuery('.header_top_invidual').removeClass('filter_menu1');
        }	
		
    });	
	});
	
//clouse fix
$(document).ready(function() {
	var project1 = $('.stopfixer').offset();
	var $window = $(window);
    var valueofmap = project1.top - '100';
    $window.scroll(function() {
        if ( $window.scrollTop() >= valueofmap) {
           // alert(project1.top);
            $(".make_fix").addClass("fixed_rm");
        }
        else {
            $(".make_fix").removeClass("fixed_rm");
        }
		 });	
});


//Owl Carasel


    $(document).ready(function() {
     
      //var owl = $("#owl-demo");
     
      /*owl.owlCarousel({
          items : 3, //10 items above 1000px browser width
          itemsDesktop : [3,5], //5 items between 1000px and 901px
          itemsDesktopSmall : [3,3], // betweem 900px and 601px
          itemsTablet: [3,2], //2 items between 600 and 0
          itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
      });*/
     
      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })

    });
	
//width of filter

$(document).ready(function(){
   
$(window).scroll(function(){
    $("#startdate").datepicker("hide");
    $("#enddate").datepicker("hide");
    $("#startdate").blur();
    $("#enddate").blur();
    $("#startdatemobile").datepicker("hide");
    $("#enddatemobile").datepicker("hide");
    $("#startdatemobile").blur();
    $("#enddatemobile").blur();    
});
$("#startdate").keydown(function(event){
if (event.which == 13) {
$("#startdate").readonlyDatepicker(true);
}
});
$("#enddate").keydown(function(event){
if (event.which == 13) {
$("#enddate").readonlyDatepicker(true);
}
});
 $("#startdatemobile").keydown(function(event){
if (event.which == 13) {
$("#startdatemobile").readonlyDatepicker(true);
}
});
$("#enddatemobile").keydown(function(event){
if (event.which == 13) {
$("#enddatemobile").readonlyDatepicker(true);
}
});
    });

</script>
<script>

	 var dateRange = [];
     var beforedate = [];
     var afterdate = [];

       $(function () {
        startdate = '';
        enddate = '';
        maxstay = <?php echo $propertyObj->max_stay?>;
        listid = <?php echo $propertyObj->id?>;
        startdates = "";
        enddates = "";
        for(i=0;i<startdates.length;i++)
        {
            fromdate = new Date(startdates[i]*1000);
            fromdate.setDate(fromdate.getDate()-1);
            beforedate.push($.datepicker.formatDate('mm/dd/yy', fromdate));
            todate = new Date(enddates[i]*1000);
            todate.setDate(todate.getDate());
            afterdate.push($.datepicker.formatDate('mm/dd/yy',todate));
            for (var d = new Date((startdates[i]*1000));
            d < new Date(enddates[i]*1000);
            d.setDate(d.getDate() + 1)) {
                dateRange.push($.datepicker.formatDate('yy-mm-dd', d));
            }
        }//alert(beforedate);
        
        var sdate = new Date(startdate * 1000);
        var edate = new Date(enddate * 1000);
        var sedate = new Date(enddate * 1000);
        sedate = new Date(sedate.setDate(sedate.getDate()-1));
        if (startdate=="") {
            sdate = "";
        }
        if (enddate=="") {
            edate = "";
            sedate = "";
        }
        if (sdate!="") {
            todaydate = new Date();
            if (sdate<todaydate) {
                minimumdate = new Date();
                endminimumdate = new Date();
                endminimumdate.setDate(endminimumdate.getDate() + 1);
            }
            else
            {
                minimumdate = new Date(sdate.setDate(sdate.getDate()));
                endminimumdate = new Date(sdate.setDate(sdate.getDate()+1));
            }
        }
        else
        {
            minimumdate = new Date();
            endminimumdate = new Date();
            endminimumdate.setDate(endminimumdate.getDate() + 1);            
        }
        



        
        $("#startdate").datepicker({
            beforeShowDay: disableDates,
            minDate:minimumdate,
            maxDate:sedate,
            onSelect: function (selectedDate) {
                $("#requestbtn").attr("disabled",false);
                var orginalDate = new Date(selectedDate);
                orginalDate.setDate(orginalDate.getDate() + 1);
                //var monthsAddedDate = new Date(new Date(orginalDate).setMonth(orginalDate.getMonth() + 3));
                $("#enddate").datepicker("option", 'minDate', orginalDate);
                $("#enddate").datepicker("option", 'maxDate', edate);
                stdates = $("#startdate").val();
                stdate = new Date(stdates);
                eddates = $("#enddate").val();
                eddate = new Date(eddates);
                var seldateRange = [];
                if (stdates!="" && eddates!="") {
                    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                     
                    selstartdate = stdate.getTime();
                    selenddate = eddate.getTime();
                    for (var dates = new Date(selstartdate);
                    dates < new Date(selenddate);
                    dates.setDate(dates.getDate() + 1)) {
                        seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                    }
                    var scnt = 0;
                    for(a=0;a<seldateRange.length;a++)
                    {
                        if ($.inArray(seldateRange[a],dateRange)>=0) {
                            scnt++;
                        }
                    }
                    if (scnt>0) {
                        $("#pricediv").hide();
                        $("#requestbtn").attr("disabled",true);
                        $("#maxstayerr").html("Can not book those days");
                    }
                    else
                    {
                        scnt = 0;
                        $("#requestbtn").attr("disabled",true);
                        $("#maxstayerr").html("");                        
                        if (diffDays>maxstay && maxstay!=0) {
                            $("#pricediv").hide();
                            $("#requestbtn").attr("disabled",true);
                            $("#maxstayerr").html("Maximum stay should be "+maxstay+" nights")
                        }
                        else
                        {
                            $.ajax({
                                url: baseurl+'/showpricedetail', // point to server-side PHP script 
                                type: "POST",
                                async: false,
                                data: {
                                    listid : listid,
                                    noofdays : diffDays
                                },
                                success: function (res) {
                                      /*result = res.split("***");
                                    listtotal = parseFloat (result[1]) + parseFloat (result[2]) + parseFloat (result[5]);
                                    totalprice = parseFloat(listtotal) + parseFloat(result[3]) + parseFloat(result[4]);
                                    listtotal = listtotal.toFixed(2);
                                    totalprice = totalprice.toFixed(2);                                    
                                    $("#taxprice").html(result[4]);
                                    $("#siteprice").html(result[3]);
                                    $("#listtotalprice").html(listtotal);
                                    $("#totalprice").html(totalprice);
                                    $("#pricediv").show();
                                    $("#noofdays").html(diffDays);
                                    $("#commissionprice").val(result[2]);
                                    $("#requestbtn").attr("disabled",false);
                                    $("#maxstayerr").html("");*/
                                    result = res.split("***");
                                    alert(result);
                                    /*listtotal = parseFloat (result[1]) + parseFloat (result[2]) + parseFloat (result[5]);*/
                                    listtotal = parseFloat (result[1]);
                                    totalprice = parseFloat(result[1]) + parseFloat(result[2])+ parseFloat(result[3]);
                                    guest_commission = parseFloat (result[2]);
                                    tax = parseFloat (result[3]);
                                    listtotal = listtotal.toFixed(2);
                                    totalprice = totalprice.toFixed(2);
                                    guest_commission = guest_commission.toFixed(2);
                                    tax = tax.toFixed(2);
                                    /*$("#taxprice").html(result[4]);
                                    $("#siteprice").html(result[3]);*/
                                    $("#listtotalprice").html(listtotal);
                                    $("#guest_commission").html(guest_commission);
                                    $("#tax").html(tax);
                                    $("#totalprice").html(totalprice);
                                    $("#pricediv").show();
                                    $("#noofdays").html(diffDays);
                                    $("#commissionprice").val(result[2]);
                                    $("#requestbtn").attr("disabled",false);
                                    $("#maxstayerr").html("");
                                }
                            });                        
                        }
                    }
                }
            },
            onClose: function (selectedDate) {
			$("#enddate").datepicker('show');
            }
        });

        $("#enddate").datepicker({
            beforeShowDay: disableDates,
            minDate:endminimumdate,
            maxDate:edate,
            onSelect: function (selectedDate) {
                $("#requestbtn").attr("disabled",false);
                var orginalDate = new Date(selectedDate);
                orginalDate.setDate(orginalDate.getDate() - 1);
                //var monthsAddedDate = new Date(new Date(orginalDate).setMonth(orginalDate.getMonth() - 3));
                //$("#startdate").datepicker("option", 'maxDate', orginalDate);
                stdates = $("#startdate").val();
                stdate = new Date(stdates);
                eddates = $("#enddate").val();
                eddate = new Date(eddates);
                var seldateRange = [];
                if (stdates!="" && eddates!="") {
                    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    selstartdate = stdate.getTime();
                    selenddate = eddate.getTime();
                    for (var dates = new Date(selstartdate);
                    dates < new Date(selenddate);
                    dates.setDate(dates.getDate() + 1)) {
                        seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                    }
                    var ecnt = 0;
                    for(b=0;b<seldateRange.length;b++)
                    {
                        if ($.inArray(seldateRange[b],dateRange)>=0) {
                            ecnt++;
                        }
                    }
                    if (ecnt>0) {
                        $("#pricediv").hide();
                        $("#requestbtn").attr("disabled",true);
                        $("#maxstayerr").html("Can not book those days");
                    }
                    else
                    {
                        ecnt = 0;
                        $("#requestbtn").attr("disabled",true);
                        $("#maxstayerr").html("");
                        if (diffDays>maxstay && maxstay!=0) {
                            $("#pricediv").hide();
                            $("#requestbtn").attr("disabled",true);
                            $("#maxstayerr").html("Maximum stay should be "+maxstay+" nights")
                        }
                        else
                        {
                            $.ajax({
                                url: baseurl+'/showpricedetail', // point to server-side PHP script 
                                type: "POST",
                                async: false,
                                data: {
                                    listid : listid,
                                    noofdays : diffDays
                                },
                                success: function (res) {
                                    /*result = res.split("***");
                                    listtotal = parseFloat (result[1]) + parseFloat (result[2]) + parseFloat (result[5]);
                                    totalprice = parseFloat(listtotal) + parseFloat(result[3]) + parseFloat(result[4]);
                                    listtotal = listtotal.toFixed(2);
                                    totalprice = totalprice.toFixed(2);                                    
                                    $("#taxprice").html(result[4]);
                                    $("#siteprice").html(result[3]);
                                    $("#listtotalprice").html(listtotal);
                                    $("#totalprice").html(totalprice);
                                    $("#pricediv").show();
                                    $("#noofdays").html(diffDays);
                                    $("#commissionprice").val(result[2]);
                                    $("#requestbtn").attr("disabled",false);
                                    $("#maxstayerr").html("");*/
                                    result = res.split("***");
                                    //alert(result);
                                    /*listtotal = parseFloat (result[1]) + parseFloat (result[2]) + parseFloat (result[5]);*/
                                    listtotal = parseFloat (result[1]);
                                    totalprice = parseFloat(result[1]) + parseFloat(result[2])+ parseFloat(result[3]);
                                    guest_commission = parseFloat (result[2]);
                                    tax = parseFloat (result[3]);
                                    listtotal = listtotal.toFixed(2);
                                    totalprice = totalprice.toFixed(2);
                                    guest_commission = guest_commission.toFixed(2);
                                    tax = tax.toFixed(2);
                                    /*$("#taxprice").html(result[4]);
                                    $("#siteprice").html(result[3]);*/
                                    $("#listtotalprice").html(listtotal);
                                    $("#guest_commission").html(guest_commission);
                                    $("#tax").html(tax);
                                    $("#totalprice").html(totalprice);
                                    $("#pricediv").show();
                                    $("#noofdays").html(diffDays);
                                    $("#commissionprice").val(result[2]);
                                    $("#requestbtn").attr("disabled",false);
                                    $("#maxstayerr").html("");
                                }
                            });                        
                        }
                    }
                }                
            }
        });

        $("#startdatemobile").datepicker({
            beforeShowDay: disableDates,
            minDate:minimumdate,
            maxDate:sedate,
            onSelect: function (selectedDate) {
                var orginalDate = new Date(selectedDate);
                orginalDate.setDate(orginalDate.getDate() + 1);
                //var monthsAddedDate = new Date(new Date(orginalDate).setMonth(orginalDate.getMonth() + 3));
                $("#enddatemobile").datepicker("option", 'minDate', orginalDate);
                $("#enddatemobile").datepicker("option", 'maxDate', edate);
                stdates = $("#startdatemobile").val();
                stdate = new Date(stdates);
                eddates = $("#enddatemobile").val();
                eddate = new Date(eddates);
                var seldateRange = [];
                if (stdates!="" && eddates!="") {
                    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    
                    selstartdate = stdate.getTime();
                    selenddate = eddate.getTime();
                    for (var dates = new Date(selstartdate);
                    dates < new Date(selenddate);
                    dates.setDate(dates.getDate() + 1)) {
                        seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                    }
                    var scnt = 0;
                    for(a=0;a<seldateRange.length;a++)
                    {
                        if ($.inArray(seldateRange[a],dateRange)>=0) {
                            scnt++;
                        }
                    }
                    if (scnt>0) {
                        $("#pricedivmobile").hide();
                        $("#requestbtn_mobile").attr("disabled",true);
                        $("#maxstayerrmobile").html("Can not book those days");
                    }
                    else
                    {
                        scnt = 0;
                        $("#requestbtn_mobile").attr("disabled",true);
                        $("#maxstayerrmobile").html("");                        
                        if (diffDays>maxstay && maxstay!=0) {
                            $("#pricedivmobile").hide();
                            $("#requestbtn_mobile").attr("disabled",true);
                            $("#maxstayerrmobile").html("Maximum stay should be "+maxstay+" nights")
                        }
                        else
                        {
                            $.ajax({
                                url: baseurl+'/user/listing/showpricedetail', // point to server-side PHP script 
                                type: "POST",
                                async: false,
                                data: {
                                    listid : listid,
                                    noofdays : diffDays
                                },
                                success: function (res) {
                                    result = res.split("***");
                                    listtotal = parseFloat (result[1]) + parseFloat (result[2]) + parseFloat (result[5]);
                                    totalprice = parseFloat(listtotal) + parseFloat(result[3]) + parseFloat(result[4]);
                                    listtotal = listtotal.toFixed(2);
                                    totalprice = totalprice.toFixed(2);
                                    $("#taxpricemobile").html(result[4]);
                                    $("#sitepricemobile").html(result[3]);
                                    $("#listtotalpricemobile").html(listtotal);
                                    $("#totalpricemobile").html(totalprice);
                                    $("#pricedivmobile").show();
                                    $("#noofdaysmobile").html(diffDays);
                                    $("#commissionpricemobile").val(result[2]);
                                    $("#requestbtn_mobile").attr("disabled",false);
                                    $("#maxstayerrmobile").html("");
                                }
                            });                        
                        }
                    }
                }
            },
            onClose: function (selectedDate) {
            $("#enddatemobile").datepicker('show');
            }
        });

        $("#enddatemobile").datepicker({
            beforeShowDay: disableDates,
            minDate:endminimumdate,
            maxDate:edate,
            onSelect: function (selectedDate) {
                var orginalDate = new Date(selectedDate);
                orginalDate.setDate(orginalDate.getDate() - 1);
                //var monthsAddedDate = new Date(new Date(orginalDate).setMonth(orginalDate.getMonth() - 3));
                //$("#startdate").datepicker("option", 'maxDate', orginalDate);
                stdates = $("#startdatemobile").val();
                stdate = new Date(stdates);
                eddates = $("#enddatemobile").val();
                eddate = new Date(eddates);
                var seldateRange = [];
                if (stdates!="" && eddates!="") {
                    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    selstartdate = stdate.getTime();
                    selenddate = eddate.getTime();
                    for (var dates = new Date(selstartdate);
                    dates < new Date(selenddate);
                    dates.setDate(dates.getDate() + 1)) {
                        seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                    }
                    var ecnt = 0;
                    for(b=0;b<seldateRange.length;b++)
                    {
                        if ($.inArray(seldateRange[b],dateRange)>=0) {
                            ecnt++;
                        }
                    }
                    if (ecnt>0) {
                        $("#pricedivmobile").hide();
                        $("#requestbtn_mobile").attr("disabled",true);
                        $("#maxstayerrmobile").html("Can not book those days");
                    }
                    else
                    {
                        ecnt = 0;
                        $("#requestbtn_mobile").attr("disabled",true);
                        $("#maxstayerrmobile").html("");
                        if (diffDays>maxstay && maxstay!=0) {
                            $("#pricedivmobile").hide();
                            $("#requestbtn_mobile").attr("disabled",true);
                            $("#maxstayerrmobile").html("Maximum stay should be "+maxstay+" nights")
                        }
                        else
                        {
                            $.ajax({
                                url: baseurl+'/user/listing/showpricedetail', // point to server-side PHP script 
                                type: "POST",
                                async: false,
                                data: {
                                    listid : listid,
                                    noofdays : diffDays
                                },
                                success: function (res) {
                                    result = res.split("***");
                                    listtotal = parseFloat (result[1]) + parseFloat (result[2]) + parseFloat (result[5]);
                                    totalprice = parseFloat(listtotal) + parseFloat(result[3]) + parseFloat(result[4]);
                                    listtotal = listtotal.toFixed(2);
                                    totalprice = totalprice.toFixed(2);                                    
                                    $("#taxpricemobile").html(result[4]);
                                    $("#sitepricemobile").html(result[3]);
                                    $("#listtotalpricemobile").html(listtotal);
                                    $("#totalpricemobile").html(totalprice);
                                    $("#pricedivmobile").show();
                                    $("#noofdaysmobile").html(diffDays);
                                    $("#commissionpricemobile").val(result[2]);
                                    $("#requestbtn_mobile").attr("disabled",false);
                                    $("#maxstayerrmobile").html("");
                                }
                            });                        
                        }
                    }
                }                
            }
        });
      
    });	
	
        var disableDates = function(dt) {
                var dateString = jQuery.datepicker.formatDate('yy-mm-dd', dt);
                return [dateRange.indexOf(dateString) == -1];
        }
        
        function focus_checkin() {
                var visible = $("#startdate").datepicker("widget").is(":visible");
                $("#startdate").datepicker(visible ? "hide" : "show");
            //$("#startdate").datepicker('show');
        }
</script>

<style>
#map_canvas {
width: 100%;
height: 450px;
}
.owl-item
{
    float: left;
}
</style>

<div class="modal fade" id="reviewpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog emergency_width" role="document">
    <div class="modal-content">
      <div class="modal-header no_border">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
      </div>
      <div class="modal-body">
            <form id="password-form" action="/user/listing/view/ODkzXzk3Mzk=" method="post" role="form">
<input type="hidden" name="_csrf" value="dTY0a1piN1ghUlUMLgN.IiNgaxsUIE1sQlRRWhJTAD4vVW4MFjVvNw==">			<input type="hidden" id="tripid" value="">
			<div class="form-group">
			Your Ratings :			<span class="text-warning">
				<i class="fa fa-star-o static-rating rating one cur" id="rateone" onclick="ratingClick('1');"></i>
				<i class="fa fa-star-o static-rating rating two cur" id="ratetwo" onclick="ratingClick('2');"></i>
				<i class="fa fa-star-o static-rating rating three cur" id="ratethree" onclick="ratingClick('3');"></i>
				<i class="fa fa-star-o static-rating rating four cur" id="ratefour" onclick="ratingClick('4');"></i>
				<i class="fa fa-star-o static-rating rating five cur" id="ratefive" onclick="ratingClick('5');"></i>
			</span>
				&nbsp;<span class="current-rate">0</span>Out of 5			</div>
			<div class="form-group">
				Your Review : <textarea maxlength="180" id="reviewmsg" rows="3" cols="20" style="vertical-align: middle;"></textarea>
			</div>
			<div class="login_or border_bottom margin_top10"></div>      
                <div class="form-group text-center">
                    <input type="button" class="btn btn_email margin_top10" id="reviewsave" value="Save " onclick="savereview();">
                </div>

            </form>            </div>
           
    </div>
  </div>
</div>
@stop
