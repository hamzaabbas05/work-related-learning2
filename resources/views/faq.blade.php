@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
@section('content')
<div class="help-view">

      <div class="container margin_bottom30">    

        
        <div class="col-xs-12 col-sm-12 margin_top20">
            <div class="col-xs-12 helpdiv">
                
            <h2 class="text-center">FAQ</h2><div class="col-xs-12" style="margin-top:4px;">
			<div class="profile_head">
    <div class="container">    
        <ul class="profile_head_menu list-unstyled">
        <li {!! (Request::is('faq/work') ? 'class="active"' : '') !!}><a href="{{URL::to('faq/work')}}">Unpaid work experiences - FAQ</a></li>
        <li {!! (Request::is('faq/listing') || Request::is('faq/listing')? 'class="active"' : '') !!}><a href="{{URL::to('faq/listing')}}">Listing Unpaid Work Experience - FAQ</a></li>
        <li {!! (Request::is('faq/schoolhouses') || Request::is('faq/schoolhouses')? 'class="active"' : '') !!}><a href="{{URL::to('faq/schoolhouses')}}">About School Houses - FAQ</a></li>
        </ul>
    </div> <!--container end -->
</div> <!--profile_head end -->	  
        <?php $i =1 ;foreach($faqObject as $obj) { 

        	$href = "#faq-cat-1-sub-".$obj->id;
        	$idhref = "faq-cat-1-sub-".$obj->id;
        	?>                           

<div class="panel panel-default panel-faq">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion-cat-1" href="{{$href}}" class="collapsed" aria-expanded="false">
<h4 class="panel-title">{{$i." ) ".$obj->question}}
<span class="pull-right"></span>
</h4>
</a>
</div>
<div id="{{$idhref}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
<div class="panel-body" style="display: block;">
 {!! html_entity_decode($obj->answer) !!}
</div>
</div>
</div>
<?php $i++;} ?>
                                            
                                        
			</div>
 </div>
      </div> <!--col-sm-9 end -->
        
    </div> <!--container end -->

</div>
@stop