@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
@inject('settings', 'App\Settings')
<?php $settingsObj = $settings->settingsInfo();?>
<div class="bg_gray1">
      <div class="container"> 



<div class="xs-no-hor-padding col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <div class="airfcfx-panel panel panel-default margin_top20">
                  <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1"> 
                  <?php $imgpath1 = asset('images/profile/'.Auth::user()->profile_img_name);
                  $userid = Auth::user()->id;
                  $receiverId = $enquiryObj->user_id;
                  if($userid != $enquiryObj->renter_id){
                  	$receiverId = $enquiryObj->renter_id;
                  }

			if (is_null(Auth::user()->profile_img_name)) {
			$imgpath1 = asset('images/noimage/'.$settingsObj->no_image_profile);
			}     ?>            	
					<h3>View Conversation Messages - {{$enquiryObj->Bookingno}}</h3>
                  </div>
                  <div class="airfcfx-panel-padding panel-body">
 					<input type="hidden" id="enqid" value="{{$enqId}}">
 					<input type="hidden" id="sender" value="{{$userid}}">
 					<input type="hidden" id="receiver" value="{{$receiverId}}">
 					<input type="hidden" id="proid" value="{{$enquiryObj->prd_id}}">
 			 <input type="hidden" id="bno" value="{{$enquiryObj->Bookingno}}">
 			 

 					
				 
                    <div class="airfcfx-message-send-cnt col-xs-12 no-hor-padding">
						<span class="airfcfx-user-icon profile_pict inlinedisplay" style="background-image:url({{$imgpath1}});float:left;"></span>
						<textarea rows="2" cols="80" class="airfcfx-inbox-txtarea txtarea contactextarea" id="contactmessage" maxlength="250" style="float:left;"></textarea>
						<input type="button" value="Send" class="airfcfx-btn-min-width airfcfx-panel btn btn-danger sendbtn tripsendbtn" style="top:0px;float:left;" onclick="send_contact_message();">
						<div class="msgerrcls" style="margin-left: 70px;margin-top: 70px;"></div>
						</div> 
 		<div id="messagesdiv">

 		<?php foreach($conversation as $obj) {

 			if($obj->senderId == Auth::user()->id) { 
 			$imgpath = asset('images/profile/'.Auth::user()->profile_img_name);
			if (is_null(Auth::user()->profile_img_name)) {
			$imgpath = asset('images/noimage/'.$settingsObj->no_image_profile);
			}
						 


 		 ?> 

					<div class="claimleft">
					<div class="airfcfx-claimleftimgdiv claimleftimgdiv">
					<span class="airfcfx-user-icon profile_pict inlinedisplay" style="background-image:url({{$imgpath}});"></span>
					</div>
					<div class="airfcfx-claimrighttextdiv claimrighttextdiv"><span class="airfcfx-left-chat-arrow"></span>
					<a href="">{{$obj->sender->name}}</a> 
					<span class="airfcfx-message-date padleft">{{date("F jS, Y", strtotime($obj->dateAdded))}}</span>
					<br><span class="mobmsgalgn">{{$obj->message}}</span>
					</div>
					</div>
		 
<?php } else { 

	$imgpath = asset('images/profile/'.$obj->sender->profile_img_name);
			if (is_null(Auth::user()->profile_img_name)) {
			$imgpath = asset('images/noimage/'.$settingsObj->no_image_profile);
			}

	?> 
	 
	 
		<div class="claimright">
		<div class="claimdiv">
		<div class="airfcfx-claimrightimgdiv claimrightimgdiv">
		<span class="airfcfx-user-icon profile_pict inlinedisplay" style="background-image:url({{$imgpath}});"></span>
		</div>
		<div class="airfcfx-claimlefttextdiv claimlefttextdiv"><span class="airfcfx-right-chat-arrow"></span>
		<span class="airfcfx-message-date padright">{{$obj->sender->name}}</span>
		<a href="">{{date("F jS, Y", strtotime($obj->dateAdded))}}</a>
		<br><span class="mobmsgalgn">{{$obj->message}}</span>
		</div>
		</div>
		</div>


		<?php } ?> <div class="clear"></div><?php } ?>
 		 




 		</div>
 		



 		</div>
                </div>
                    
            </div>

            </div>
            </div>

            @stop