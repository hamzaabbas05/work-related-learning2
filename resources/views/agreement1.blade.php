<html>
    <head>
        <style>
            .divbottom 
            {
                margin-bottom:10px;
            }

            .margintop
            {
                margin-top:50px;
            }


        </style>
    </head>
    <body>
        <div style="margin-left:60px;">
            <div>
                {!! html_entity_decode($cmsObj->page_desc) !!}
                <h2>1) INSTITUTE OF HIGHER EDUCATION - PROMOTER</h2></div>
            <div class="divbottom">Institute (hereinafter referred to as Promoter):{{$rentalObj->user->school->name}}</div>
            <div class="divbottom">Mechano-graphical code:{{$rentalObj->user->school->Mecanographiccode}}</div>
            <!-- Pending -- >
            
            <div class="divbottom" ><h3>Registered office/Legal Address</h3></div> 
            <div class="divbottom">Address street name:{{$rentalObj->user->school->lroute}}</div>
            <div class="divbottom">Address street number:{{$rentalObj->user->school->lstreet_number}}</div>
            <div class="divbottom">Town/City:{{$rentalObj->user->school->llocality}}</div>
            <div class="divbottom">State/Region:{{$rentalObj->user->school->ladministrative_area_level_1}}</div>
            <div  class="divbottom">Provincia:</div>
            <div class="divbottom">Postal code:{{$rentalObj->user->school->lpostal_code}}</div>
            <div class="divbottom">Country:{{$rentalObj->user->school->lcountry}}</div> 
            <div class="divbottom">Telephone:{{$rentalObj->user->school->phonenumber}}</div> 
            <div class="divbottom">e-mail:{{$rentalObj->user->school->school_email}}</div> 
            <!-- Pending End-->

            <div class="divbottom">Headmaster:{{$rentalObj->user->school->HeadMaster}}</div>
            <!-- Doubt -->
            <div class="divbottom"><p>Represented by (as School Tutor / Internal Tutor)</p></div>
            <div class="divbottom">Name of School Tutor:{{$rentalObj->user->tutor_name}}</div>
            <div class="divbottom">Surname of School Tutor:{{$rentalObj->user->tutor_surname}}</div>
            <div class="divbottom">Place of birth:{{$rentalObj->user->tutor_pob}}</div>
            <div class="divbottom">Tax Code:{{$rentalObj->user->tutor_pan}}</div>
            <div class="divbottom">e- mail:{{$rentalObj->user->tutor_email}}</div> 
            <div class="divbottom">Telephone:{{$rentalObj->user->tutor_phone}}</div> 

            <div><h2>2) ORGANISATION - THE HOSTING PARTY</h2></div>
            <div class="divbottom">Organisation Name:{{$rentalObj->host->orgname}}</div>
            <div class="divbottom">Tax Code - Tax identification number:{{$rentalObj->host->NIN18}}</div>
            <!-- Legal Address -->
            <div class="divbottom" ><h3>Registered office/Legal Address</h3></div></tr>
        <div class="divbottom">Address street name:{{$rentalObj->host->router18}}</div>
        <div class="divbottom">Address street number:{{$rentalObj->host->street_numberr18}}</div>
        <div class="divbottom">Town/City:{{$rentalObj->host->localityr18}}</div>
        <div class="divbottom">State/Region:{{$rentalObj->host->administrative_area_level_1r18 }}</div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code:{{$rentalObj->host->postal_coder18}}</div>
        <div class="divbottom">Country:{{$rentalObj->host->countryr18}}</div>
        <!-- End Legal Address -->
        <?php if ($rentalObj->property->addr_work_org_same == 0) { ?>  
            <!-- Operative Address -->
            <div class="divbottom" ><h3>Operative Address where the work related learning traineeship takes place :</h3></div>
            <div class="divbottom">Address street name:{{$rentalObj->property->wroute}}</div>
            <div class="divbottom">Address street number:{{$rentalObj->property->wstreet_number}}</div>
            <div class="divbottom">Town/City:{{$rentalObj->property->wlocality}}</div>
            <div class="divbottom">State/Region:{{$rentalObj->property->wadministrative_area_level_1 }}</div>
            <div  class="divbottom">Provincia:</div>
            <div class="divbottom">Postal code:{{$rentalObj->property->wpostal_code}}</div>
            <div class="divbottom">Country:{{$rentalObj->property->wcountry}}</div>
            <div class="divbottom">Telephone:{{$rentalObj->property->location_email}}</div>
            <div class="divbottom">e-mail:{{$rentalObj->property->location_telephone}}</div>
            <!-- End Legal Address -->

        <?php } else { ?>
            <div class="divbottom" ><h3>Operative Address where the work related learning traineeship takes place :</h3></div>
            <div class="divbottom">Address street name:{{$rentalObj->host->router18}}</div>
            <div class="divbottom">Address street number:{{$rentalObj->host->street_numberr18}}</div>
            <div class="divbottom">Town/City:{{$rentalObj->host->localityr18}}</div>
            <div class="divbottom">State/Region:{{$rentalObj->host->administrative_area_level_1r18 }}</div>
            <div  class="divbottom">Provincia:</div>
            <div class="divbottom">Postal code:{{$rentalObj->host->postal_coder18}}</div>
            <div class="divbottom">Country:{{$rentalObj->host->countryr18}}</div>
            <div class="divbottom">Telephone:{{$rentalObj->host->orgphone}}</div>
            <div class="divbottom">e-mail:{{$rentalObj->host->orgemail}}</div>
        <?php } ?>

        <div class="divbottom"> Further (possible) operating office to conduct the Internship: 
            <?php if ($rentalObj->property->work_other_check == 1) {
                echo "Yes";
            } else {
                echo "no";
            }
            ?>
            <!--<input type="radio" <?php //if ($rentalObj->property->work_other_check == 1) { ?> checked <?php// } ?> name="ss">Yes
            <input type="radio" <?php //if ($rentalObj->property->work_other_check == 0) { ?> checked <?php //} ?> name="ss">No-->
        </div>
        <div class="divbottom"> Other operative Address where the work related learning traineeship takes place: </div> 
        <div class="divbottom"> {!!$rentalObj->property->location_other_address!!}</div> 
        <?php
        $ln = "";
        if ($rentalObj->host->legal_nature == 1) {
            $ln = "Public";
        } else if ($rentalObj->host->legal_nature == 0) {
            $ln = "Private";
        }
        ?>

        <div class="divbottom"> Legal Nature (private or public): {{$ln}} </div> 
        <div class="divbottom" ><h3>RAMON CODE [CODICE ATECO 2007]: 99.00 Extra-territorial organizations and bodies [99.00 Organizzazioni ed Organi extraterritoriali]</h3></div> 
        <div class="divbottom">Human Resources:<?php if (!is_null($rentalObj->user->roomtype)) {
            echo $rentalObj->user->roomtype->name;
        }
        ?> </div> 


        <!-- Work Tutor Infomation  -->
        <div class="divbottom" ><h3> Represented by (as Work Tutor) </h3></div> 

        <div class="divbottom">Name of Work Tutor: {{$rentalObj->property->represent_name}}</div> 
        <div class="divbottom"> Surname of Work Tutor:{{$rentalObj->property->represent_surname}}</div> 
        <div class="divbottom"> Place of birth: {{$rentalObj->property->represent_born}}</div>
        <div class="divbottom">Tax Code - Tax identification number:{{$rentalObj->property->represent_tax_no}}</div> 
        <div class="divbottom"> e- mail:  {{$rentalObj->property->represent_email}}</div> 
        <div class="divbottom"> Telephone:{{$rentalObj->property->represent_phone}} </div> 

        <div class="divbottom" ><h3> as EXTERNAL TUTOR (WORK TUTOR)</h3></div> 
        <div class="divbottom"> Role/Relationship in the Company: {{$rentalObj->property->tutorrelation->name}}</div> 
        <div class="divbottom"> Work Experience and skills you have (brief description of the experience and professional competencies as proof of the adequacy of your services as work Tutor: {{$rentalObj->property->work_tutor_fit}}</div> 
        <!-- End Work Tutor Infomation  -->

        <div class="divbottom">COMPENSATION: <span style="font-size: 12px;">(no compensation foreseen)</span> </div> 
        <div class="divbottom">TRAINEESHIPS IN PROGRESS: <span style="font-size: 12px;">(this will be completed by student on arrival, ask Work Tutor)</span></div> 
        <div class="divbottom">Number curricular traineeships in progress: </div> 
        <div class="divbottom">Number extracurricular traineeships in progress: </div> 
        <div class="divbottom">Number of trainees assigned to commencement of the traineeship:  </div> 


        <div><h2>3) STUDENT TRAINEE </h2></div>
        <div class="divbottom">Name of Student:{{$rentalObj->user->sname16}}</div>
        <div class="divbottom">Surname of Student:{{$rentalObj->user->ssurname16}}</div>
        <div class="divbottom">Place of birth:{{$rentalObj->user->pob16}}</div>
        <div class="divbottom">Date of Birth:{{$rentalObj->user->dob16}}</div>
        <div class="divbottom">Tax Code - Tax identification number: {{$rentalObj->user->NIN16}}</div>
        <div class="divbottom">e- mail:{{$rentalObj->user->semail16}}</div>
        <div  class="divbottom">Telephone:{{$rentalObj->user->sphone16}} </div>


        <div class="divbottom" ><h3>Residency Address</h3></div> 
        <div class="divbottom">Address street name:{{$rentalObj->user->router16}}</div>
        <div class="divbottom">Address street number:{{$rentalObj->user->street_numberr16}}</div>
        <div class="divbottom">Town/City:{{$rentalObj->user->localityr16}}</div>
        <div class="divbottom">State/Region:{{$rentalObj->user->administrative_area_level_1r16}}</div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code:{{$rentalObj->user->postal_coder16}}</div>
        <div class="divbottom">Country:{{$rentalObj->user->countryr16}}</div>



<?php if ($rentalObj->user->s16_res_dom_same == 0) { ?>    
            <div class="divbottom" ><h3>Domicile Address</h3></div></tr>
        <div class="divbottom">Address street name:{{$rentalObj->user->routerd6}}</div>
        <div class="divbottom">Address street number:{{$rentalObj->user->street_numberd16}}</div>
        <div class="divbottom">Town/City:{{$rentalObj->user->localityd16}}</div>
        <div class="divbottom">State/Region:{{$rentalObj->user->administrative_area_level_1d16}}</div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code:{{$rentalObj->user->postal_coded16}}</div>
        <div class="divbottom">Country:{{$rentalObj->user->countryd16}}</div>
<?php } ?>





    <div class="divbottom">CONDITIONS OF THE TRAINEE AT INITIATING TRAINEESHIP:{{$rentalObj->user->medical_learning}}</div> 
    <div class="divbottom">ACADEMIC QUALIFICATIONS: {{$rentalObj->user->academic_qualifications}}</div> 

    <div><h2>4) PARENT / LEGAL GUARDIAN OF THE UNDERAGE STUDENT TRAINEE</h2></div>
<?php if (is_null($rentalObj->user->student_relationship)) { ?>

        <div class="divbottom">Name: </div>
        <div class="divbottom">Surname: </div>
        <div class="divbottom">Place of birth: </div>
        <div class="divbottom">Date of Birth: </div>
        <div class="divbottom">Tax Code - Tax identification number: </div>

        <!-- Parent / Above 18 Address -->
        <div class="divbottom" ><h3>Residency Address</h3></div></tr>
    <div class="divbottom">Address street name: </div>
    <div class="divbottom">Address street number: </div>
    <div class="divbottom">Town/City: </div>
    <div class="divbottom">State/Region: </div>
    <div  class="divbottom">Provincia</div>
    <div class="divbottom">Postal code: </div>
    <div class="divbottom">Country: </div>


    <div class="divbottom">As: </div>
    <p><p>In case the data herein shown is the same for the student trainee and Parent it is because the student is over 18 and therefore we do not need any consent, signature for disclaimer or waivier for an underage trainee.</p>

<?php } else { ?>  
    <div class="divbottom">Name:{{$rentalObj->user->name}}</div>
    <div class="divbottom">Surname:{{$rentalObj->user->lastname}}</div>
    <div class="divbottom">Place of birth:{{$rentalObj->user->basicpob}}</div>
    <div class="divbottom">Date of Birth:{{$rentalObj->user->dob}}</div>
    <div class="divbottom">Tax Code - Tax identification number:{{$rentalObj->user->NIN18}}</div>

    <!-- Parent / Above 18 Address -->
    <div class="divbottom" ><h3>Residency Address</h3></div></tr>
    <div class="divbottom">Address street name:{{$rentalObj->user->raddress18}}</div>
    <div class="divbottom">Address street number:{{$rentalObj->user->street_numberr18}}</div>
    <div class="divbottom">Town/City:{{$rentalObj->user->localityr18}}</div>
    <div class="divbottom">State/Region:{{$rentalObj->user->administrative_area_level_1r18}}</div>
    <div  class="divbottom">Provincia</div>
    <div class="divbottom">Postal code:{{$rentalObj->user->postal_coder18}}</div>
    <div class="divbottom">Country:{{$rentalObj->user->countryr18}}</div>

    <?php if ($rentalObj->user->s_res_dom_same == 0) { ?> 
        <!-- Parent / Above 18 Address -->
        <div class="divbottom" ><h3>Domicile Address</h3></div></tr>
        <div class="divbottom">Address street name:{{$rentalObj->user->routed18}}</div>
        <div class="divbottom">Address street number:{{$rentalObj->user->street_numberd18}}</div>
        <div class="divbottom">Town/City:{{$rentalObj->user->localityd18}}</div>
        <div class="divbottom">State/Region:{{$rentalObj->user->administrative_area_level_1d18}}</div>
        <div  class="divbottom">Provincia</div>
        <div class="divbottom">Postal code:{{$rentalObj->user->postal_coded18}}</div>
        <div class="divbottom">Country:{{$rentalObj->user->countryd18}}</div>
    <?php } ?>

    <!-- End Parent / Above 18 Address -->
    <?php
    $relationship = "";
    if ($rentalObj->user->student_relationship == 1) {
        $relationship = "Mother";
    } elseif ($rentalObj->user->student_relationship == 2) {
        $relationship = "Father";
    } else if ($rentalObj->user->student_relationship == 3) {
        $relationship = "Legal Gaurdian";
    }
    ?>
    <div class="divbottom">As: {{$relationship}}</div>
    <p><p>In case the data herein shown is the same for the student trainee and Parent it is because the student is over 18 and therefore we do not need any consent, signature for disclaimer or waivier for an underage trainee.</p>

<?php } ?>


<div style="text-align:center">AGREE AND STIPULATE:</div>
{!! html_entity_decode($stipulate->page_desc) !!}

<div style="text-align:center">ARTICLE 1 <br>
    {{$article1->page_name}}
</div>
{!! html_entity_decode($article1->page_desc) !!}

<div style="text-align:center">ARTICLE 2 <br>
    {{$article2->page_name}}
</div>
{!! html_entity_decode($article2->page_desc) !!}


<div style="text-align:center">ARTICLE 3 <br>
    {{$article3->page_name}}
</div>
{!! html_entity_decode($article3->page_desc) !!}


<div style="text-align:center">ARTICLE 4 <br>
    {{$article4->page_name}}
</div>
{!! html_entity_decode($article4->page_desc) !!}

<div style="text-align:center">ARTICLE 5 <br>
    {{$article5->page_name}}
</div>
{!! html_entity_decode($article5->page_desc) !!}


<div style="text-align:center">ARTICLE 6 <br>
    {{$article6->page_name}}
</div>
{!! html_entity_decode($article6->page_desc) !!}


<div style="text-align:center">ARTICLE 7 <br>
    {{$article7->page_name}}
</div>
{!! html_entity_decode($article7->page_desc) !!}


<div style="text-align:center">ARTICLE 8 <br>
    AGREEMENT – INDIVIDUAL EDUCATIONAL PROJECT<br>
    Internship Training Curricula</div>
<div class="divbottom">Date of Agreement:{{$rentalObj->dateAdded}}</div>
<div class="divbottom">Reference number of Agreement:{{$rentalObj->Bookingno}}</div>


<!-- REQUIRED INSURANCE GUARANTEES -->
<div style="text-align:center"><h3>REQUIRED INSURANCE GUARANTEES</h3></div>
<div class="divbottom">Policy with:{{$rentalObj->user->school->insurancewithcompany}}</div>
<div class="divbottom">Injuries policy no:{{$rentalObj->user->school->noofpolicy}}</div>
<div class="divbottom">Liability no :{{$rentalObj->user->school->noofpolicythirdparty}}
</div>
<div class="divbottom"> Legal protection n:{{$rentalObj->user->school->legalexpcovernumber}}
</div>

<!--DURATION, OBJECTIVES AND METHODS OF THE INTERNSHIP -->
<div style="text-align:center"><h3>DURATION, OBJECTIVES AND METHODS OF THE INTERNSHIP</h3></div>
<div class="divbottom">Duration in hours at the headquarters of the host organization:{{$rentalObj->property->work_hours}}</div>
<div class="divbottom">Start Date:{{$rentalObj->checkin}}
</div>
<div class="divbottom">End Date:{{$rentalObj->checkout}} 
</div>

<div class="divbottom">Hours (daily and weekly) - Description – Enter the scheduled number of daily and weekly hours:  {{$rentalObj->property->work_schedule}}
</div>

<!--OTHER EVENTUAL WORK LOCATIONS -->
<div style="text-align:center"><h3>OTHER EVENTUAL WORK LOCATIONS</h3></div>
<p>See as shown in the figures of the Host Organization.</p>


<!--AREA(S) OF INCLUSION -->
<div style="text-align:center"><h3>AREA(S) OF INCLUSION</h3></div>
<div class="divbottom">
    <!--For example: Professional area - administration, accounting, secretarial. :{{$rentalObj->property->propertytype->name}}-->
    {{$rentalObj->property->propertytype->name}}
</div>
<div class="divbottom" ><h3>ACTIVITY OBJECTIVE OF INTERNSHIP </h3></div> 
<div class="divbottom">Duties : {{$rentalObj->property->description}}
</div>
<div class="divbottom">Rules: {{$rentalObj->property->houserules}}
</div>
<div class="divbottom">Description: {{$rentalObj->property->exptitle}}
</div>
<div class="divbottom">Further optional description: {{$rentalObj->property->optional_description}}
</div>
<div class="divbottom">School Tutor extra optional Description : </div>


<!-- <div class="divbottom" ><h3>OTHER POSSIBLE LOCATIONS WHERE THE WORK PLACE WILL TAKE PLACE
</h3></div> 
<div class="divbottom"> Would students do work at other addresses?: 

<input type="radio" <?php if ($rentalObj->property->work_other_check == 1) { ?> checked <?php } ?> name="ss">Yes
<input type="radio" <?php if ($rentalObj->property->work_other_check == 0) { ?> checked <?php } ?> name="ss">No
</div>
<div class="divbottom">Describe other workplaces and eventually addresses: {{$rentalObj->property->location_other_address}}
</div>
<div class="divbottom" ><h3>AREA OF INSERTION</h3></div> 

<div class="divbottom" ><h3>ACTIVITIES - PURPOSE OF THE TRAINING 
</h3></div>  -->

<div style="text-align:center">{{$educationalobjectives->page_name}} <br></div>
{!! html_entity_decode($educationalobjectives->page_desc) !!}       

<div style="text-align:center">ARTICLE 9 <br>
    {{$article9->page_name}}
</div>
{!! html_entity_decode($article9->page_desc) !!}   


<div style="text-align:center">ARTICLE 10 <br>
    {{$article10->page_name}}
</div>
{!! html_entity_decode($article10->page_desc) !!}  


<div><h3>SIGNATURES</h3></div>
<div class="divbottom">Nr. Convention of reference:{{$rentalObj->Bookingno}}</div>
<div class="divbottom"><h3>Signature and stamp</h3></div>
<div class="divbottom">High School Principal of School</div>
<div class="divbottom margintop">Internal Tutor of School</div>
<div class="divbottom">Location & Date</div><br><br>

<div class="divbottom margintop">External Tutor at the workplace</div>
<div class="divbottom">Location & Date</div><br><br>

<div class="divbottom"><h3>Signature</h3></div>
<div class="divbottom margintop">Student Intern</div>
<div class="divbottom margintop">Parent/Legal Guardian of minor intern</div>
<div class="divbottom">Location & Date: --------------------                      ---------------------</div>
</div>
</body>
</html>