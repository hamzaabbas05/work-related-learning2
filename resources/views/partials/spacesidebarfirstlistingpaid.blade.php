<div class="col-xs-12 col-sm-3 margin_top30 margin_bottom30 sdecls"
     style="width: 18%;">
    <ul class="profile_left list-unstyled" id="listpropul">

        <li   {!! (Request::is('rentals/paid_firt_listing/*') ? 'style="background: #ddd"' : '') !!}  id="showBasi"><a href="{{URL::to('rentals/paid_firt_listing/'.$propertyObj->id)}}">Basics</a></li>
        <li  {!! (Request::is('rentals/first_description_paid/*') ? 'style="background: #ddd"' : '') !!} id="showDesc"><a href="{{URL::to('rentals/first_description_paid/'.$propertyObj->id)}}">Description</a></li>
        <li  {!! (Request::is('rentals/first_photos_paid/*') ? 'style="background: #ddd"' : '') !!} id="showPhoto"><a href="{{URL::to('rentals/first_photos_paid/'.$propertyObj->id)}}">Photos</a></li>
        <li  {!! (Request::is('rentals/first_location_paid/*') ? 'style="background: #ddd"' : '') !!} id="showLoc"><a href="{{URL::to('rentals/first_location_paid/'.$propertyObj->id)}}">Location</a></li>
<!--         <li  {!! (Request::is('rentals/pricing/*') ? 'style="background: #ddd"' : '') !!} id="showPricing"><a href="{{URL::to('rentals/pricing/'.$propertyObj->id)}}">Pricing</a></li> 
        <li {!! (Request::is('rentals/booking_paid/*') ? 'style="background: #ddd"' : '') !!}  id="showBooking"><a href="{{URL::to('rentals/booking_paid/'.$propertyObj->id)}}">Rules</a></li>-->
        <li  {!! (Request::is('rentals/first_calendar_paid/*') ? 'style="background: #ddd"' : '') !!} id="showCalendar"><a href="{{URL::to('rentals/first_calendar_paid/'.$propertyObj->id)}}">Calendar</a></li>
    </ul>

</div>