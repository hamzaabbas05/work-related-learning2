<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog emergency_width" role="document">
        <div class="modal-content">
            <div class="modal-header no_border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
            </div>
            <div class="modal-body text-center">
                <label class="selectalign">Medical</label>
                <div class="mediumlargediv">
                    <div class="form-group field-listing-medicalno">

                        <input type="text" id="medicalno" class="form-control margin_bottom10 divmediumtext" name="Listing[medicalno]" value="">

                        <p class="help-block help-block-error"></p>
                    </div>			</div>			
                <br />
                <label class="selectalign">Fire</label>
                <div class="mediumlargediv">
                    <div class="form-group field-listing-fireno">

                        <input type="text" id="fireno" class="form-control margin_bottom10 divmediumtext" name="Listing[fireno]" value="">

                        <p class="help-block help-block-error"></p>
                    </div>			</div>			
                <br />
                <label class="selectalign">Police</label>
                <div class="mediumlargediv">
                    <div class="form-group field-listing-policeno">

                        <input type="text" id="policeno" class="form-control margin_bottom10 divmediumtext" name="Listing[policeno]" value="">

                        <p class="help-block help-block-error"></p>
                    </div>			</div>			
                <br />
                <div class="numbererrcls" style="clear: both;"></div><br/>
            </div>
            <div class="modal-footer clear">
                <input type="button" value="Save" class="btn btn_email" data-dismiss="modal">
            </div>            
        </div>
    </div>
</div>
<!--script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script-->
<script type="text/javascript">
    function checktutor(event)
    {
        if (event.checked) {
            $('#worktutor').css('display', 'block');
            /*$("#tutorrelation").prop('required',true);
             $("#work_tutor_fit").prop('required',true);
             $("#rname").prop('required',true);
             $("#rsurname1").prop('required',true);
             $("#rtax1").prop('required',true);
             $("#remail1").prop('required',true);
             $("#rtel1").prop('required',true);
             $("#rborn1").prop('required',true);*/
        } else {
            $('#worktutor').css('display', 'none');
            /*$("#tutorrelation").prop('required',false);
             $("#work_tutor_fit").prop('required',false);
             $("#rname").prop('required',false);
             $("#rsurname1").prop('required',false);
             $("#rtax1").prop('required',false);
             $("#remail1").prop('required',false);
             $("#rtel1").prop('required',false);
             $("#rborn1").prop('required',false);*/
        }
    }

    function checkhuman(event)
    {
        if (event.checked) {
            $('#human_resources').css('display', 'block');
        } else {
            $('#human_resources').css('display', 'none');
        }
    }

    function showAddress() {
        var check = 0;


        listingid = $("#listingid").val();
        email = $("#location_email").val();
        telephone = $("#location_telephone").val();
        otheraddress = $("#location_other_address").val();


        /*legal_address = $("#legal_address").val();
         lstreet_number = $("#lstreet_number").val();
         lroute = $("#lroute").val();
         ladministrative_area_level_1 = $("#ladministrative_area_level_1").val();
         lpostal_code = $("#lpostal_code").val();
         lcountry = $("#lcountry").val();
         llocality = $("#llocality").val();*/


        workplace_address = $("#workplace_address").val();
        wstreet_number = $("#wstreet_number").val();
        wroute = $("#wroute").val();
        wadministrative_area_level_1 = $("#wadministrative_area_level_1").val();
        wpostal_code = $("#wpostal_code").val();
        wcountry = $("#wcountry").val();
        wlocality = $("#wlocality").val();

        work_other_check = 0;

        addr_work_org_same = 1;
        if ($('#checkaddr').prop("checked") == true) {
            addr_work_org_same = 0;
//            if ($.trim(workplace_address) == '') {
//                $(".field-listing-workplace_address").addClass("has-error");
//                $("#workplace_address").next(".help-block-error").html("workplace_address cannot be blank");
//                check = 1;
//                $("#workplace_address").keydown(function () {
//                    $(".field-listing-legal_address").removeClass("has-error");
//                    $("#legal_address").next(".help-block-error").html("");
//                });
//
//            }
//
//            if ($.trim(wstreet_number) == '') {
//                $(".field-listing-wstreet_number").addClass("has-error");
//                $("#wstreet_number").next(".help-block-error").html("WorkplaceAdreess-Street Number cannot be blank");
//                check = 1;
//                $("#wstreet_number").keydown(function () {
//                    $(".field-listing-wstreet_number").removeClass("has-error");
//                    $("#wstreet_number").next(".help-block-error").html("");
//                });
//
//            }
//            if ($.trim(wroute) == '') {
//                $(".field-listing-wroute").addClass("has-error");
//                $("#wroute").next(".help-block-error").html("WorkplaceAdreess-Street Name cannot be blank");
//                check = 1;
//                $("#wroute").keydown(function () {
//                    $(".field-listing-wroute").removeClass("has-error");
//                    $("#wroute").next(".help-block-error").html("");
//                });
//
//            }
//            if ($.trim(wadministrative_area_level_1) == '') {
//                $(".field-listing-wadministrative_area_level_1").addClass("has-error");
//                $("#wadministrative_area_level_1").next(".help-block-error").html("WorkplaceAdreess-Town/city cannot be blank");
//                check = 1;
//                $("#wadministrative_area_level_1").keydown(function () {
//                    $(".field-listing-wadministrative_area_level_1").removeClass("has-error");
//                    $("#wadministrative_area_level_1").next(".help-block-error").html("");
//                });
//
//            }
//
//
//
//            if ($.trim(wlocality) == '') {
//                $(".field-listing-wlocality").addClass("has-error");
//                $("#wlocality").next(".help-block-error").html("WorkplaceAdreess-state/region cannot be blank");
//                check = 1;
//                $("#wlocality").keydown(function () {
//                    $(".field-listing-wlocality").removeClass("has-error");
//                    $("#wlocality").next(".help-block-error").html("");
//                });
//
//            }
//
//            if ($.trim(wcountry) == '') {
//                $(".field-listing-wcountry").addClass("has-error");
//                $("#wcountry").next(".help-block-error").html("WorkplaceAdreess-Country cannot be blank");
//                check = 1;
//                $("#wcountry").keydown(function () {
//                    $(".field-listing-wcountry").removeClass("has-error");
//                    $("#wcountry").next(".help-block-error").html("");
//                });
//
//            }
//
//            if ($.trim(wpostal_code) == '') {
//                $(".field-listing-wpostal_code").addClass("has-error");
//                $("#wpostal_code").next(".help-block-error").html("WorkplaceAdreess-Postal/zipcode cannot be blank");
//                check = 1;
//                $("#wpostal_code").keydown(function () {
//                    $(".field-listing-wpostal_code").removeClass("has-error");
//                    $("#wpostal_code").next(".help-block-error").html("");
//                });
//
//            }
//
//            if ($.trim(email) == '') {
//                $(".field-listing-location_email").addClass("has-error");
//                $("#location_email").next(".help-block-error").html("Workspace Email cannot be blank");
//                check = 1;
//                $("#location_email").keydown(function () {
//                    $(".field-listing-location_email").removeClass("has-error");
//                    $("#location_email").next(".help-block-error").html("");
//                });
//
//            }
//            if (!(isValidEmailAddress(email))) {
//                $(".field-listing-location_email").addClass("has-error");
//                $("#location_email").next(".help-block-error").html("Workplace Email should be valid");
//                check = 1;
//                $("#location_email").keydown(function () {
//                    $(".field-listing-location_email").removeClass("has-error");
//                    $("#location_email").next(".help-block-error").html("");
//                });
//            }
//            if ($.trim(telephone) == '') {
//                $(".field-listing-location_telephone").addClass("has-error");
//                $("#location_telephone").next(".help-block-error").html("Workplace Telephone cannot be blank");
//                check = 1;
//                $("#location_telephone").keydown(function () {
//                    $(".field-listing-location_telephone").removeClass("has-error");
//                    $("#location_telephone").next(".help-block-error").html("");
//                });
//
//            }

        }


        /*if($.trim(legal_address) == ''){
         $(".field-listing-legal_address").addClass("has-error");
         $("#legal_address").next(".help-block-error").html("Legal Address cannot be blank");
         check = 1;
         $("#legal_address").keydown(function(){
         $(".field-listing-legal_address").removeClass("has-error");
         $("#legal_address").next(".help-block-error").html("");
         });
         
         }
         
         if($.trim(lstreet_number) == ''){
         $(".field-listing-lstreet_number").addClass("has-error");
         $("#lstreet_number").next(".help-block-error").html("LegalAdreess-Street Number cannot be blank");
         check = 1;
         $("#lstreet_number").keydown(function(){
         $(".field-listing-lstreet_number").removeClass("has-error");
         $("#lstreet_number").next(".help-block-error").html("");
         });
         
         }
         
         
         if($.trim(lroute) == ''){
         $(".field-listing-lroute").addClass("has-error");
         $("#lroute").next(".help-block-error").html("LegalAdreess-Street Name cannot be blank");
         check = 1;
         $("#lroute").keydown(function(){
         $(".field-listing-lroute").removeClass("has-error");
         $("#lroute").next(".help-block-error").html("");
         });
         
         }
         
         
         if($.trim(ladministrative_area_level_1) == ''){
         $(".field-listing-ladministrative_area_level_1").addClass("has-error");
         $("#ladministrative_area_level_1").next(".help-block-error").html("LegalAdreess-Town/city cannot be blank");
         check = 1;
         $("#ladministrative_area_level_1").keydown(function(){
         $(".field-listing-ladministrative_area_level_1").removeClass("has-error");
         $("#ladministrative_area_level_1").next(".help-block-error").html("");
         });
         
         }
         
         if($.trim(llocality) == ''){
         $(".field-listing-llocality").addClass("has-error");
         $("#llocality").next(".help-block-error").html("LegalAdreess-State/Region cannot be blank");
         check = 1;
         $("#llocality").keydown(function(){
         $(".field-listing-llocality").removeClass("has-error");
         $("#llocality").next(".help-block-error").html("");
         });
         
         }
         
         
         
         if($.trim(lpostal_code) == ''){
         $(".field-listing-lpostal_code").addClass("has-error");
         $("#lpostal_code").next(".help-block-error").html("LegalAdreess-Postal/zipcode cannot be blank");
         check = 1;
         $("#lpostal_code").keydown(function(){
         $(".field-listing-lpostal_code").removeClass("has-error");
         $("#lpostal_code").next(".help-block-error").html("");
         });
         
         }
         
         if($.trim(lcountry) == ''){
         $(".field-listing-lcountry").addClass("has-error");
         $("#lcountry").next(".help-block-error").html("LegalAdreess-Country cannot be blank");
         check = 1;
         $("#lcountry").keydown(function(){
         $(".field-listing-lcountry").removeClass("has-error");
         $("#lcountry").next(".help-block-error").html("");
         });
         
         }*/

        if ($('#work_other_check').prop("checked") == true) {
            work_other_check = 1;

            if ($.trim(otheraddress) == '') {
                $(".field-listing-location_other_address").addClass("has-error");
                $("#location_other_address").next(".help-block-error").html("Other Address cannot be blank");
                check = 1;
                $("#location_other_address").keydown(function () {
                    $(".field-listing-location_other_address").removeClass("has-error");
                    $("#location_other_address").next(".help-block-error").html("");
                });

            }
        }
        if (check == 1) {
            return false;
        } else
        {
            if ($('#checkaddr').prop("checked") == true) {
                address = wstreet_number + ',' + wroute + ',' + wlocality + ',' + wadministrative_area_level_1 + ',' + wpostal_code + ',' + wcountry;
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();
                        $("#latbox").val(latitude);
                        $("#lonbox").val(longitude);
                        $.ajax({
                            type: 'POST',
                            url: baseurl + '/rentals/savelocationlist',
                            async: false,
                            data: {
                                listingid: listingid,
                                latitude: latitude,
                                longitude: longitude,
                                telephone: telephone,
                                email: email,
                                addr_work_org_same: addr_work_org_same,
                                otheraddress: otheraddress,
                                workplace_address: workplace_address,
                                wstreet_number: wstreet_number,
                                wroute: wroute,
                                wadministrative_area_level_1: wadministrative_area_level_1,
                                wpostal_code: wpostal_code,
                                wcountry: wcountry,
                                wlocality: wlocality,
                                work_other_check: work_other_check
                            },
                            success: function (data) {
                                if (data == 'admin') {
                                    window.location = baseurl + '/admin/property/location/' + listingid;
                                } else {
                                    window.location = baseurl + '/rentals/location/' + listingid;
                                }

                                /*$("#showLoc").css('background','none');
                                 $("#showAmenities").css('background','#ddd');
                                 $("#locationdiv").hide();
                                 $("#amenitiesdiv").show();*/
                            }
                        });
                    } else {
                        $(".errcls").show();
                        $(".errcls").html("Cannot get latitude and longitude. Please enter valid address");
                        setTimeout(function () {
                            $(".errcls").slideUp();
                            $('.errcls').html('');
                        }, 5000);
                        check = 1;
                    }


                });
            } else {
                var latitude = $("#orglat").val();
                var longitude = $("#orglan").val();
                $.ajax({
                    type: 'POST',
                    url: baseurl + '/rentals/savelocationlist',
                    async: false,
                    data: {
                        listingid: listingid,
                        latitude: latitude,
                        longitude: longitude,
                        telephone: telephone,
                        email: email,
                        addr_work_org_same: addr_work_org_same,
                        otheraddress: otheraddress,
                        workplace_address: workplace_address,
                        wstreet_number: wstreet_number,
                        wroute: wroute,
                        wadministrative_area_level_1: wadministrative_area_level_1,
                        wpostal_code: wpostal_code,
                        wcountry: wcountry,
                        wlocality: wlocality,
                        work_other_check: work_other_check
                    },
                    success: function (data) {
                        if (data == 'admin') {
                            window.location = baseurl + '/admin/property/location/' + listingid;
                        } else {
                            window.location = baseurl + '/rentals/location/' + listingid;
                        }

                        /*$("#showLoc").css('background','none');
                         $("#showAmenities").css('background','#ddd');
                         $("#locationdiv").hide();
                         $("#amenitiesdiv").show();*/
                    }
                });

            }

        }
    }
    
    function showAddressPaid() {
        
        var check = 0;


        listingid = $("#listingid").val();
        email = $("#location_email").val();
        telephone = $("#location_telephone").val();
        otheraddress = $("#location_other_address").val();
        workplace_address = $("#workplace_address").val();
        wstreet_number = $("#wstreet_number").val();
        wroute = $("#wroute").val();
        wadministrative_area_level_1 = $("#wadministrative_area_level_1").val();
        wpostal_code = $("#wpostal_code").val();
        wcountry = $("#wcountry").val();
        wlocality = $("#wlocality").val();

        work_other_check = 0;

        addr_work_org_same = 1;
        if ($('#checkaddr').prop("checked") == true) {
            addr_work_org_same = 0;
        }

        if ($('#work_other_check').prop("checked") == true) {
            work_other_check = 1;

            if ($.trim(otheraddress) == '') {
                $(".field-listing-location_other_address").addClass("has-error");
                $("#location_other_address").next(".help-block-error").html("Other Address cannot be blank");
                check = 1;
                $("#location_other_address").keydown(function () {
                    $(".field-listing-location_other_address").removeClass("has-error");
                    $("#location_other_address").next(".help-block-error").html("");
                });

            }
        }
        if (check == 1) {
            return false;
        } else
        {
            if ($('#checkaddr').prop("checked") == true) {
                address = wstreet_number + ',' + wroute + ',' + wlocality + ',' + wadministrative_area_level_1 + ',' + wpostal_code + ',' + wcountry;
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();
                        $("#latbox").val(latitude);
                        $("#lonbox").val(longitude);
                        $.ajax({
                            type: 'POST',
                            url: baseurl + '/rentals/savelocationlist',
                            async: false,
                            data: {
                                listingid: listingid,
                                latitude: latitude,
                                longitude: longitude,
                                telephone: telephone,
                                email: email,
                                addr_work_org_same: addr_work_org_same,
                                otheraddress: otheraddress,
                                workplace_address: workplace_address,
                                wstreet_number: wstreet_number,
                                wroute: wroute,
                                wadministrative_area_level_1: wadministrative_area_level_1,
                                wpostal_code: wpostal_code,
                                wcountry: wcountry,
                                wlocality: wlocality,
                                work_other_check: work_other_check
                            },
                            success: function (data) {
                                //alert(data);
                                if (data == 'admin') {
                                    window.location = baseurl + '/admin/property/locationPaid/' + listingid;
                                } else {
                                    var first_listing = $("#first_listing").val();
                                    if(first_listing == "first_listing") {
                                        window.location = baseurl + '/rentals/first_location_paid/' + listingid;
                                    } else {
                                        window.location = baseurl + '/rentals/location_paid/' + listingid;
                                    }
                                }
                            }
                        });
                    } else {
                        $(".errcls").show();
                        $(".errcls").html("Cannot get latitude and longitude. Please enter valid address");
                        setTimeout(function () {
                            $(".errcls").slideUp();
                            $('.errcls').html('');
                        }, 5000);
                        check = 1;
                    }


                });
            } else {
                var latitude = $("#orglat").val();
                var longitude = $("#orglan").val();
                $.ajax({
                    type: 'POST',
                    url: baseurl + '/rentals/savelocationlist',
                    async: false,
                    data: {
                        listingid: listingid,
                        latitude: latitude,
                        longitude: longitude,
                        telephone: telephone,
                        email: email,
                        addr_work_org_same: addr_work_org_same,
                        otheraddress: otheraddress,
                        workplace_address: workplace_address,
                        wstreet_number: wstreet_number,
                        wroute: wroute,
                        wadministrative_area_level_1: wadministrative_area_level_1,
                        wpostal_code: wpostal_code,
                        wcountry: wcountry,
                        wlocality: wlocality,
                        work_other_check: work_other_check
                    },
                    success: function (data) {
                        if (data == 'admin') {
                            window.location = baseurl + '/admin/property/locationPaid/' + listingid;
                        } else {
                            var first_listing = $("#first_listing").val();
                            if(first_listing == "first_listing") {
                                window.location = baseurl + '/rentals/first_location_paid/' + listingid;
                            } else {
                                window.location = baseurl + '/rentals/location_paid/' + listingid;
                            }
                            //window.location = baseurl + '/rentals/location_paid/' + listingid;
                        }
                    }
                });

            }

        }
    }
</script>
<script>

    /*$(function () {
     $("#startdate").datepicker({
     dateFormat: 'yy/mm/dd',
     minDate:new Date(),
     onSelect: function (selected) {
     var dt = new Date(selected);
     dt.setDate(dt.getDate() + 1);
     $("#enddate").datepicker("option", "minDate", dt);
     }
     });
     edate = new Date();
     edate.setDate(edate.getDate()+1);
     $("#enddate").datepicker({
     dateFormat: 'yy/mm/dd',
     minDate:edate,
     onSelect: function (selected) {
     var dt = new Date(selected);
     dt.setDate(dt.getDate() - 1);
     $("#startdate").datepicker("option", "maxDate", dt);
     }
     });
     
     });*/



</script>
<!--script type="text/javascript">
        //$("#input-id").fileinput({'showUpload':false, 'previewFileType':'any'});
</script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
<link href="css/fileupload/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="js/fileupload/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="js/fileupload/fileinput.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>

<script src="js/fileupload/fileinput_locale_<lang>.js"></script-->