<div class="airfcfx-panel panel panel-default">
    <?php /*<div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">Parent of Under 18</h3>
        <br>
        <p>Either parent, or the legal guardian, signing the Learning Agreement must complete "Basic Info" and here under to allow Student Guests who are under 18 to take part.</p>
    </div> -->
    <div class="airfcfx-panel-padding panel-body">
        <div class="row ">
            <div class="airfcfx-panel-padding panel-body">
                <div class="col-xs-12 margin_top10">
                    <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Relationship with Student hereunder<i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <select class="form-control" style="width:auto;" id="student_relationship" name="student_relationship">
                            <option value="" >Select</option>
                            <option value="1" <?php if ($userObject->student_relationship == 1) { ?> selected <?php } ?>>Mother</option>
                            <option value="2" <?php if ($userObject->student_relationship == 2) { ?> selected <?php } ?>>Father</option>
                            <option value="3" <?php if ($userObject->student_relationship == 3) { ?> selected <?php } ?>>Legal Guardian
                            </option>
                        </select>

                        <div class="errcls" id="student_relationshiperr" style="clear: both;"></div><br/>
                    </div>
                </div> 

                <div class="col-xs-12 margin_top10">
                    <div class="col-xs-12 col-sm-3 text-right">
                        <input type="checkbox" <?php if ($userObject->check_allow == 1) { ?> checked="" <?php } ?> id="allowcheck_out" name="check_allow" required="" />
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="form-group field-profile-firstname required">
                            I allow my son/daughter to leave the Host family after dinner until 10.00 pm.
                            <p class="help-block help-block-error" style="display: none; color: red;" id='show_allow_check_out'>* If you wish to deny this consent, please contact us.</p>
                        </div>                        
                    </div>
                </div> 
                <div class="col-xs-12 margin_top10">
                    <div class="col-xs-12 col-sm-3 text-right">
                        <input type="checkbox" value="1" <?php if (isset($userObject->check_minors_allow) && $userObject->check_minors_allow == 1) { ?> checked="" <?php } ?> id="allowcheck_adult" name="check_minors_allow" required=""/>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="form-group field-profile-firstname required">
                            I accept my son/daughter to be, work and stay, with minors or adults. 
                            <p class="help-block help-block-error" style="display: none; color: red;" id='show_allow_check_adult'>* If you wish to deny this consent, please contact us.</p>
                        </div>                        
                    </div>
                </div> 
                <div class="col-xs-12 margin_top10">
                    <div class="col-xs-12 col-sm-3 text-right">
                        <input type="checkbox" value="1" <?php if (isset($userObject->check_same_nationality_allow) && $userObject->check_same_nationality_allow == 1) { ?> checked="" <?php } ?> id="allowcheck" name="check_same_nationality_allow" />
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="form-group field-profile-firstname required">
                            I allow my son/daughter to be, work or stay, with Student Guests of the same Nationality (for example to share the room)
                            <p class="help-block help-block-error"></p>
                        </div>                        
                    </div>
                </div> 
                <!--col-xs-12 end -->
                
                @include('partials.address.parent_residence')
                <div class="col-xs-12 margin_top10">
                    <div class="col-xs-12 col-sm-3 text-right">

                        <input type="checkbox" <?php if ($userObject->s_res_dom_same == 0) { ?> checked <?php } ?> id="p_res_dom_same" name="p_res_dom_same" onchange="showparentdomicile(this)">
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="form-group field-profile-firstname required">

                            Check If Residency and Domicile Address are different

                            <p class="help-block help-block-error"></p>
                        </div>                        </div>
                </div> <!--col-xs-12 end -->
                <?php
                $dstyle = 'display:none';
                if ($userObject->s_res_dom_same == 0) {
                    $dstyle = 'display:block';
                }
                ?>
                <div class="col-xs-12 margin_top10" id="pdomicilediv" style="{{$dstyle}}">
                    <div class="col-xs-12 col-sm-3 text-right">
                        <label class="padding0 profile_label">Domicile Address 
                        </label> 
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        @include('partials.address.parent_domicile')
                    </div>
                </div> <!-- Address End -->

            </div>
        </div>
    </div>*/ ?>
</div>
<div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">Over 16 year old Student details</h3>
        <br>
        <p>If you have more than one child taking part, you must create a new users (with another email).</P>
    </div>
    <div class="airfcfx-panel-padding panel-body 123">
        @include('partials.pre_enrol_studentunder16')
    </div>
</div>
<div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">School Info</h3>
        <br>
        <p>Select from the list provided herein, if your school is not in the list, contact us via the email on the footer.</P>
        <p>If you don't know who your School Tutor is, ask your School.</P>
        <p>Your School Tutor may temporarily be the Person travelling with you.</P>
    </div>
    <div class="airfcfx-panel-padding panel-body">
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">School Name</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">



                    <select id="school_name16" name="school_name16" class="form-control"  > 
                        <option value="">Select School</option>
                        <?php foreach ($schools as $sc) { ?> <option <?php if ($sc->id == $userObject->school_name) { ?> selected <?php } ?> value="{{$sc->id}}">{{$sc->name}}</option><?php } ?>
                    </select>




                    <p class="help-block help-block-error"></p>
                </div>                   <div class="errcls" id="school_name16err" style="clear: both;"></div><br/>          </div>
        </div> <!--col-xs-12 end -->


        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Class Letter</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_letter16" class="form-control" name="class_letter16" value="{{$userObject->class_letter}}"  >

                    <p class="help-block help-block-error"></p>
                </div>           <div class="errcls" id="class_letter16err" style="clear: both;"></div><br/>                  </div>
        </div> <!--col-xs-12 end -->


        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Class Number</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_number16" class="form-control" name="class_number16" value="{{$userObject->class_number}}"  >

                    <p class="help-block help-block-error"></p>
                </div>          <div class="errcls" id="class_number16err" style="clear: both;"></div><br/>                   </div>
        </div> <!--col-xs-12 end -->

    </div>
</div>
<div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">In case of group departure</h3>
        <p>If your group is not in the list, please contact us at the email here under in the footer.</p>
        <p>If you are travelling alone, or in a small group without an adult travelling with you, ignore this part.</p>
    </div>
    <div class="airfcfx-panel-padding panel-body">
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Select your Group</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">
                    <select id="secondary_group_parent_id" class="form-control" name="secondary_group_parent_id">
                        <option value="">-- Select Group --</option>
                        @if(isset($groups) && sizeof($groups) > 0)
                        @foreach($groups as $group)
                        <?php
                        $group_in = isset($group->group_date_in) ? date("Y-m-d", strtotime($group->group_date_in)) : "";
                        $current_date = date("Y-m-d");
                        if ($group_in > $current_date) {
                            ?>
                            <option value="{{$group->id}}" @if(isset($userObject->secondary_group_id) && $userObject->secondary_group_id == $group->id) selected="" @endif>{{$group->group_name}}</option>
                        <?php } ?>
                        @endforeach
                        @endif
                    </select>
                    <p class="help-block help-block-error"></p>
                </div>         
                <div class="errcls" id="secondary_group_parent_iderr" style="clear: both;"></div><br/>
            </div>
        </div>
    </div>
</div>