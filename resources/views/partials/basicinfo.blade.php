@inject('country', 'App\Country')
<?php
$countries = $country->getall();
?>
<div class="col-xs-12 margin_top10" style="display: none;">
    <div class="col-xs-12 col-sm-3 text-right">

        <label class="profile_label">Add Information for </label> 
    </div>

    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname required">

            <button  style="{{$orgbtnstyle}}" id="orgbtn" class="btn btn-success" type="button" onclick="showrelevantdiv(4)">Organization</button>
            <button style="{{$stubtnstyle}}" id="stubtn" class="btn btn-info" type="button" onclick="showrelevantdiv(6)">Over 18 High school Student</button>
            <button style="{{$parbtnstyle}}"  id="parbtn" class="btn btn-warning" type="button" onclick="showrelevantdiv(5)">Parent</button>

   <!-- <select onchange="showrelevantdiv(this.value)" class="form-control" style="width:auto;" id="userrole" name="userrole" required  >
                                  <option value="" >Select</option>
                                  <option <?php if ($roleId == 4) { ?> selected <?php } ?> value="4">Organization</option>
                                  <option <?php if ($roleId == 5) { ?> selected <?php } ?> value="5">Parent</option>
                                  <option <?php if ($roleId == 6) { ?> selected <?php } ?> value="6">Student</option>
                        </select> -->

        </div>         <!--  <div class="errcls" id="userroleerr" style="clear: both;"></div><br/> -->


    </div>
</div> <!--col-xs-12 end -->                
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">

        <label class="profile_label">Name </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname required">

            <input type="text" id="profile-firstname" class="form-control" name="name" value="{{$userObject->name}}" maxlength="40" placeholder="First Name(s)" onkeypress="return isAlpha(event)" required>

            <p class="help-block help-block-error"></p>
        </div>           <div class="errcls" id="fnerr" style="clear: both;"></div><br/>


    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Surname </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-lastname required">

            <input type="text" id="profile-lastname" class="form-control" name="lastname" maxlength="40" placeholder="Family name(s)/Surname(s)" onkeypress="return isAlpha(event)" value="{{$userObject->lastname}}" required>

            <p class="help-block help-block-error"></p>
        </div>                       <div class="errcls" id="lnerr" style="clear: both;"></div><br/>

    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">I Am <i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <select class="form-control" style="width:auto;" id="profile-gender" name="gender" required>
            <option value="" >Gender</option>
            <option value="1" <?php if ($userObject->gender == 1) { ?> selected <?php } ?>>Male</option>
            <option value="2" <?php if ($userObject->gender == 2) { ?> selected <?php } ?>>Female</option>
            <option value="3" <?php if ($userObject->gender == 3) { ?> selected <?php } ?>>Other</option>
        </select>
       <!-- <p class="margin_top_5 text_gray1">This is only shared once a "Request" is confirmed.</p>  -->
        <div class="errcls" id="generr" style="clear: both;"></div><br/>
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Birth Date <i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
    </div>
    
    <div class="airfcfx-profile-bd col-xs-12 col-sm-9">
        <?php
        $day = "";
        $month = "";
        $year = "";
        if (!is_null($userObject->dob)) {
            $dob = $userObject->dob;
            $dobArray = explode("/", $dob);
            $day = $dobArray[0];
            $month = $dobArray[1];
            $year = $dobArray[2];
        }
        ?>



        <select class="form-control col-sm-4" id="bmonth" style="width:95px;" name="bmonth" required>
            <option <?php if ($month == "") { ?> selected <?php } ?> value="">Month</option>
            <option value="1" <?php if ($month == 1) { ?> selected <?php } ?>>January</option>
            <option value="2" <?php if ($month == 2) { ?> selected <?php } ?>>February</option>
            <option value="3" <?php if ($month == 3) { ?> selected <?php } ?>>March</option>
            <option value="4" <?php if ($month == 4) { ?> selected <?php } ?>>April</option>
            <option value="5" <?php if ($month == 5) { ?> selected <?php } ?>>May</option>
            <option value="6" <?php if ($month == 6) { ?> selected <?php } ?>>June</option>
            <option value="7" <?php if ($month == 7) { ?> selected <?php } ?>>July</option>
            <option value="8" <?php if ($month == 8) { ?> selected <?php } ?>>August</option>
            <option value="9" <?php if ($month == 9) { ?> selected <?php } ?>>September</option>
            <option value="10" <?php if ($month == 10) { ?> selected <?php } ?>>October</option>
            <option value="11" <?php if ($month == 11) { ?> selected <?php } ?>>November</option>
            <option value="12" <?php if ($month == 12) { ?> selected <?php } ?>>December</option>

        </select>
        <select class="form-control col-sm-4 margin_left10" id="bday" style="width:80px;" name="bday" required>
            <option value="" <?php if ($day == "") { ?> selected <?php } ?>>Day</option>
            <?php for ($i = 1; $i <= 31; $i++) { ?>
                <option <?php if ($day == $i) { ?> selected <?php } ?> value="{{$i}}">{{$i}}</option>
            <?php } ?>
        </select>
        <select class="form-control col-sm-4 margin_left10" id="byear" style="width:90px;" name="byear" required>
            <option value="" <?php if ($year == "") { ?> selected <?php } ?>>Year</option>
            <?php
            $currentYear = date("Y");
            $startYear = $currentYear - 18;
            $baseYear = 1900;
            for ($i = $startYear; $i >= $baseYear; $i--) {
                ?>
                <option <?php if ($year == $i) { ?> selected <?php } ?> value="{{$i}}">{{$i}}</option>
            <?php } ?>
        </select>
        <br/><br/>

        <div class="errcls" id="doberr" style="clear: both;"></div><br/>

    </div>
</div> <!--col-xs-12 end -->
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">

        <label class="profile_label">Place Of Birth </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname required">

            <input type="text" id="basicpob" class="form-control" name="basicpob" value="{{$userObject->basicpob}}" required>

            <p class="help-block help-block-error"></p>
        </div>         
        <div class="errcls" id="basicpoberr" style="clear: both;"></div><br/>

    </div>
</div>


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Country of Citizenship </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">

            {!! Form::select('country_id', $countries, $userObject->country_id, 
            ['class'=>'form-control', 'name' => 'country_id', 'id' => 'country_id', 'required']) !!}

            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
        <div class="errcls" id="countryerr" style="clear: both;"></div><br/>
    </div>
</div> 

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Email Address <i class="fa fa-lock profile_icon" 
                                                      data-toggle="tooltip" data-placement="top" title="Private"></i></label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <input type="text" name="email" class="form-control " disabled="true" value="{{$userObject->email}}" placeholder="" style="" />
<!--        <p class="margin_top_5 text_gray1">This is only shared once a "booking" is confirmed.
        </p>-->
    </div>
</div> <!--col-xs-12 end -->



<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Phone Number <i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i></label> 
    </div>
    <div class="col-xs-12 col-sm-9" id="phoneverify">
        <input type="text"  id="phonenumber" class="form-control"  name="phonenumber" placeholder="Include International code, i.e. for the UK 0044" value="{{$userObject->phonenumber}}" onkeypress="return isNumber(event)" required onclick="//generate_code();">
        <br /><br />
        <!-- <div id="codeblock" class="hiddencls">
                    <div id="codetext"></div>
                        <input type="text" value="" class="form-control smalltext" id="verifycode">
                        <input type="button" value="Verify" class="btn btn_email" onclick="//verify_code();">
                        <div id="codesuccess" class="successtxt"></div>
                        <div id="codeerror" class="errcls"></div>
                    </div> -->

        <div class="errcls" id="phonenumbererr" style="clear: both;"></div><br/>                
    </div>


</div> 

<!--col-xs-12 end -->
