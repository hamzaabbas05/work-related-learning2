@if (session()->has('flash_notification.message') && session('flash_notification.level') != "referral_error")
<div class="alert alert-{{ session('flash_notification.level') }}" style="z-index: 100;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        {!! session('flash_notification.message') !!}
    </div>
@endif