@if ($errors->has())
<div>
    <p style="color:red;">
        @foreach ($errors->all() as $error)
        {!! $error !!}<br />		
        @endforeach
    </p>
</div>
@elseif(Session::has('error'))
<div class="alert alert-danger" style="z-index: 100;">
    {{Session::get('error')}}
    <a style="float: right; border: 1px solid; border-radius: 2px; padding: 0px 4px; text-decoration: none; cursor: pointer;" onclick="closeErrorMsg(this)">X</a>
</div>
@endif