@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
      <div class="container">  


<div class="col-xs-12 col-sm-12 margin_top20">        
        
        <div class="airfcfx-panel panel panel-default">
          <div class="airfcfx-panel-padding airfcfx-panel panel-heading profile_menu1">
            <h3 class="panel-title">Your Trips</h3>
          </div>
          
          <div class="panel-body">
            <div class="row">
				<div class="trips-cnt margin_top10 margin_bottom20 paddingleft table-responsive col-sm-12 col-lg-12"><table class="table table_no_border tripdesign">
                                	<thead>
                                      <tr class="review_tab">
                                      <th>Sno</th>
                                        <th>Dates and Location</th>
									 	                     <th>Booking No</th>
                                        <th>Booking</th>
                                         <th>Approval</th>
                                        
                                      </tr>
                                    </thead><tbody>
                                    <?php $i =1 ;foreach($tripsObj as $trips) { 
                                          $class = "successtxt";
                                          if ($trips->booking_status == 'Pending') {
                                    			$class = "text-danger";
                                    		} if ($trips->booking_status == 'Cancelled') {
                                    			$class = "text-medium";
                                    		}

                                         $class1 = "successtxt";
                                          if ($trips->approval == 'Pending') {
                                          $class1 = "text-danger";
                                        } if ($trips->approval == 'Decline') {
                                          $class1 = "text-medium";
                                        }
                                    		
                                    		
                                    		$id = "reserve".$trips->id;
                                    	?> 
                                 <tr id="{{$id}}">
								<td>{{$i}}</td>
						 <td class="airfcfx-breakword"><p class="airfcfx-td-dtnloc-trp">{{$trips->checkin}}- {{$trips->checkout}}</p><p class="airfcfx-td-dtnloc-trp"><a class="text-danger" href="">{{$trips->property->auto_complete}}</a></p>
							<p class="airfcfx-td-dtnloc-trp"></p><div>{{$trips->property->title}}</div><div>Host : {{$trips->property->user->name}}</div><p></p></td>
						  <td align="center">{{$trips->Bookingno}}</td>
							<td class="airfcfx-min-width-80px">
							<p class="{{$class}}"><b>{{$trips->booking_status}}</b></p></td>
              <td class="airfcfx-min-width-80px">
              <p class="{{$class1}}"><b>{{$trips->approval}}</b></p></td>
            
                       
                </tr>
            <?php $i++;} ?>

 					</tbody></table></div>                          

                 </div> <!--row end -->

          </div>
          
        </div> <!--Panel end -->
        
         
        
        
        
    </div>


    </div>
    </div>@stop