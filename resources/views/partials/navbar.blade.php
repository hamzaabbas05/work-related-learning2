@inject('settings', 'App\Settings')
<?php
$settingsObj = $settings->settingsInfo();
$logoname = $settingsObj->logo;
?>
@inject('propertyType', 'App\PropertyType')
@inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();

$protype = $propertyType->getAll(true);
?>
<nav class="navbar navbar-default norm_nav ">
    <div class="navbar-header">
        <button aria-controls="navbar" aria-expanded="false"
                data-target="#navbar" data-toggle="collapse"
                class="airfcfx-mobile-navbar navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span> <span
                class="icon-bar"></span> <span class="icon-bar"></span> <span
                class="icon-bar"></span>
        </button>

        <a href="{{url('/')}}" class="navbar-brand"> 
            <img
                src="<?php echo url('images/logo/' . $logoname) ?>" style="height:75px;" alt="Work-Related-Learning"
                alt="Work-Related-Learning" class="img-responsive margin_top10" /> 
        </a>
        <div class="pos_rel pull-left search_sec">
            <input id="where-to-go-main" type="text" class="form-control" placeholder="Search for Work Experience & Paid Services - Where would you like to?"> 
            <i
                class="fa fa-search over_input"></i>
            <input id="place-lat" type="hidden" value="">
            <input id="place-lng" type="hidden" value="">
        </div>
    </div>

    <div class="navbar-collapse collapse" id="navbar">

        <ul class="nav navbar-nav navbar-right">

            @if (Auth::guest())
            <li class="dropdown"><a href="{{url('faq/work')}}" class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-question-icon"></span><span class="airfcfx-menu">Help</span></a>
            </li>
            <li class="dropdown"><a href="{{url('signup')}}" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="modal" class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-signup-icon"></span><span class="airfcfx-menu">Sign Up</span></a>
            </li>
            <li class="dropdown"><a href="{{url('signin')}}" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="modal" class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-login-icon"></span><span class="airfcfx-menu">Login</span></a>
            </li>
            @else
            <?php if (!is_null(Auth::user()->properties) && Auth::user()->properties->count() > 0) { ?>
                <li class="dropdowns"><a href="#" aria-expanded="false"
                                         aria-haspopup="true" role="button" data-toggle="dropdown"
                                         class="airfcfx-menu-link dropdown-toggle pos_rel"><span class="airfcfx-menu-host-icon"></span>
                        <span class="airfcfx-menu">Host</span> 
                    </a>

                    <ul class="dropdown-menu padding20 profil_menu">
                        <a href="{{URL::to('user/listing/rentals/active')}}" class="rm_text_deco"><li
                                class="margin_top10 margin_bottom10">Your Listings</li> </a>					
                        <div class="border_bottom"></div>
                        
                        <a href="{{URL::to('user/reservations')}}" class="rm_text_deco"><li
                                class="margin_top10 margin_bottom10">Your Reservations</li> </a>
                        <div class="border_bottom"></div>
                        <a href="{{URL::to('rentals/add')}}" class="rm_text_deco"><li
                                class="margin_top10 margin_bottom10">Add Listing</li> </a>
                        <div class="border_bottom"></div>
                        <a href="{{URL::to('user/transactions/completed')}}" class="rm_text_deco">
                            <li class="margin_top10 margin_bottom10">Transaction History</li>
                        </a>
                    </ul>					
                </li>  <?php } ?>
            <li class="dropdowns "><a href="" aria-expanded="false"
                                      aria-haspopup="true" role="button" data-toggle="dropdown"
                                      class="airfcfx-menu-link dropdown-toggle pos_rel"><span class="airfcfx-menu-message-icon"></span><span class="airfcfx-menu">Messages</span>  
                </a>

                <ul class="dropdown-menu padding20 profil_menu">
                    <a href="{{URL::to('user/messages')}}" class="rm_text_deco"><li
                            class="margin_top10 margin_bottom10">Inbox</li> </a>					
                    <div class="border_bottom"></div>



                </ul>					
            </li>
            <li class="dropdown"><a href="{{URL::to('faq/work')}}"  class="dropdown-toggle airfcfx-menu-link"><span class="airfcfx-menu-question-icon"></span><span class="airfcfx-menu">Help</span>
                </a>
            </li>
            <li class="dropdowns">
                <a href="/dashboard"aria-expanded="false"
                   aria-haspopup="true" role="button" data-toggle="dropdown"
                   class="airfcfx-menu-link dropdown-toggle pos_rel margin_right padd_right_60"> <span class="airfcfx-profilename">{{Auth::user()->name}}</span>
                       <?php
                       $imgpath = asset('images/profile/' . Auth::user()->profile_img_name);
                       if (is_null(Auth::user()->profile_img_name)) {
                           $imgpath = asset('images/noimage/' . $settingsObj->no_image_profile);
                       }
                       ?>


                    <span class="profile_pict pos_abs" style="background-image:url({{$imgpath}}));height:40px;width:40px;"></span>				</a>
                <ul class="dropdown-menu padding20 profil_menu">

                    <a href="{{URL::to('viewuser')}}"
                       class="rm_text_deco"><li class=" margin_bottom10">My Profile</li>
                    </a>
                    <div class="border_bottom"></div>
                    <a href="{{URL::to('user/dashboard')}}" class="rm_text_deco">
                        <li class="margin_top10 margin_bottom10">View Dashboard</li>
                    </a><div class="border_bottom"></div>
                    <a href="{{URL::to('user/wishlist')}}" class="rm_text_deco">
                        <li class="margin_top10 margin_bottom10">My Wishlists</li>
                    </a><div class="border_bottom"></div>
                    <a href="{{URL::to('user/listing/rentals/active')}}"
                       class="rm_text_deco"><li class="margin_top10 margin_bottom10">Your Listings</li>
                    </a><div class="border_bottom"></div>
                    <a href="/"
                       class="rm_text_deco"><li class="margin_top10 margin_bottom10">Find a Job or a School House</li>
                    </a>
                    <div class="border_bottom"></div>

                    <!-- <a href="{{URL::to('user/listing/wishlists')}}" class="rm_text_deco"><li
                            class="margin_top10 margin_bottom10">Wish List</li> </a>
                    <div class="border_bottom"></div> -->
                    <a href="{{URL::to('user/edit')}}" class="rm_text_deco"><li
                            class="margin_top10 margin_bottom10">Edit Profile</li> </a>

                    <div class="border_bottom"></div>
                    <a href="{{URL::to('user/changepassword')}}" class="rm_text_deco">
                        <li class="margin_top10 margin_bottom10">Account Settings</li>
                    </a>
                    <div class="border_bottom"></div>
                    <a href="{{URL::to('logout')}}" class="rm_text_deco"><li
                            class="margin_top10">Logout</li> </a>
                </ul>
            </li>
            @endif
        </ul>
    </div>
    <!--/.nav-collapse -->

</nav>

<!--search drop down calendar-->

<div class="container hiddencls" id="searchcalendardiv">
    <div class="col-lg-12 airfcfx-dd-calendar-cnt">
        <div class="airfcfx-dd-calendar border1  bg_white caltxt">

            <div class="table1 margin_top10 margin_left5 margin_bottom10 padd_5_10 checkinalgn">

                <div class="checkindivcls" style="width: 48%;">
                    <label>Check In</label>
                    <input id="check-in-main" type="text" class="form-control" placeholder="DD-MM-YYYY" value="" />
                </div>
                <div class="checkindivcls" style="width: 48%;">
                    <label>Check Out</label>
                    <input id="check-out-main" type="text" class="form-control" placeholder="DD-MM-YYYY" value="" style="border-right: 1px solid #ccc !important;"/>
                </div>
<!--                <div class="guestdivcls">
                    <label>Guests</label>
                    <select class="form-control margin10" id="guest-count">

                        <?php for ($i = 1; $i <= $PropertySettingsObj->accomodates_no; $i++) { ?>
                            <option value="{{$i}}"><?php echo "Guest" . $i; ?></option>
                        <?php } ?>


                    </select>
                </div>-->
            </div><!--table1 end-->
<!--            <div class="airfcfx-dd-calendar-rt-txt col-lg-12">Room Type</div>

            <div class="airfcfx-dd-cal-RT">
                <div class="pull-left">
                    <select class="form-control" id="property_type" >
                        <option value="">Select Property Type</option>
                        <?php foreach ($protype as $obj) { ?>
                            <option value="{{$obj->id}}">{{$obj->name}}</option>
                        <?php } ?>
                    </select>

                </div>
            </div>-->
            <div class="airfcfx-search-dd-findplace-btn">
                <button class="airfcfx-findplace-btn airfcfx-panel btn btn_email" onclick="searchlistmains()">Find a place</button>
            </div>
        </div><!--border1 end-->
    </div></div>