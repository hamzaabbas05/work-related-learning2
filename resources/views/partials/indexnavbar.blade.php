@inject('settings', 'App\Settings')
<?php $settingsObj = $settings->settingsInfo();
$logoname = $settingsObj->logo; ?>
<nav class="navbar navbar-default menu_bg">
    
    <div class="airfcfx-home-cnt-width container">
    
            <div class="padding-left-15 navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="airfcfx-mobile-navbar navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{url('/')}}" class="navbar-brand">
                  <img src="<?php echo url('images/logo/'.$logoname)?>" alt="holidaysiles" style="height:100px;" class="img-responsive logomobile margin_top15" />
                </a>
            </div>

            <div class="navbar-collapse collapse" id="navbar">
                <ul class="airfcfx-home-menu nav navbar-nav navbar-right">

				   @if (Auth::guest())
                   <li><a href="#" data-toggle="modal" data-target="#signupModal" > Join now</a>
                   </li>
                    <li><a href="#" id="modellogin" data-toggle="modal" data-target="#loginModal">Sign In</a>
                    </li>@endif
                     
                    @if (Auth::guest())
                    <li><a href="#" data-toggle="modal" data-target="#loginModal" class="
                    airfcfx-list-space-btn"> List Work Experience or School House</a></li>@else
                   
                     <li class="">
                <a href="/dashboard" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle pos_rel margin_right padd_right_60"> {{Auth::user()->name}}                </a>                        
                    <ul class="airfcfx-arrow-dropdown dropdown-menu padding20 profil_menu">
                        <div class="airfcfx-dd-arrow">
                        </div>
                        <a href="{{URL::to('user/dashboard')}}" class="rm_text_deco"><li class=" margin_bottom10">Dashboard</li>
                        </a>
                        <div class="border_bottom"></div>
                        <a href="{{URL::to('user/listing/rentals/active')}}" class="rm_text_deco"><li class="margin_top10 margin_bottom10">Your Listings</li>
                        </a>
                        <div class="border_bottom"></div>
                        <a href="{{URL::to('user/trips')}}" class="rm_text_deco"><li class="margin_top10 margin_bottom10">Your Trips</li>
                        </a>
                        <div class="border_bottom"></div>
                        <a href="{{URL::to('user/wishlist')}}" class="rm_text_deco"><li class="margin_top10 margin_bottom10">Wish List</li> </a>
                        <div class="border_bottom"></div>
                        <a href="{{URL::to('user/edit')}}" class="rm_text_deco"><li class="margin_top10 margin_bottom10">Edit Profile</li> </a>
                         
                        <div class="border_bottom"></div>
                        <!--<a href="{{URL::to('user/settings')}}" class="rm_text_deco">-->
                        <a href="{{URL::to('user/wallet')}}" class="rm_text_deco">
                            <li class="margin_top10 margin_bottom10">Account Settings</li>
                        </a>
                        <div class="border_bottom"></div>
                        <a href="{{URL::to('logout')}}" class="rm_text_deco"><li class="margin_top10">Logout</li> </a>
                    </ul>
                    </li>
                    <li>
                    <a href="{{URL::to('logout')}}">Sign Out</a>
                    </li>
                                        <li><a href="{{URL::to('faq/work')}}" class="airfcfx-menu-help-txt"> Help</a>
                    </li>
                                        <li><a href="{{URL::to('rentals/add')}}" class="airfcfx-list-space-btn"> List Work Experience or School House</a>
                                        </li>
                    @endif
				</ul>
            </div>
            <!--/.nav-collapse -->
    
        </div><!--container end-->
            
    </nav>