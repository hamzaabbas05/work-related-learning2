<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="padding0 profile_label">Emergency Contact <i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i></label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <a href="javascript:void(0);" class="text-danger add_cont" data-id="1"> <i class="fa fa-plus text-danger"></i> Add Contact</a>
        <p class="margin_top_5 text_gray1">Give our Customer Experience team a trusted contact we can alert in an urgent situation.</p>
    </div>

</div> <!--col-xs-12 end -->
<div class="row add_contact" id="add_contact" style="display:none;">
    <div class="col-xs-12 margin_top10">
        <div class="col-xs-12 col-sm-3 text-right">
            <label class="profile_label">Name and Surname</label> 
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="form-group field-profile-emergencyname">

                <input type="text" id="profile-emergencyname" class="form-control" name="emergency_contact_name" value="{{$userObject->emergency_contact_name}}">

                <p class="help-block help-block-error"></p>
            </div>                                </div>
    </div> <!--col-xs-12 end -->

    <div class="col-xs-12 margin_top10">
        <div class="col-xs-12 col-sm-3 text-right">
            <label class="profile_label">Phone</label> 
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="form-group field-profile-emergencyno">

                <input type="text" id="profile-emergencyno" class="form-control" name="emergency_contact_phone" value="{{$userObject->emergency_contact_phone}}" onkeypress="return isNumber(event)">

                <p class="help-block help-block-error"></p>
            </div>                                </div>
    </div> <!--col-xs-12 end -->

    <div class="col-xs-12 margin_top10">
        <div class="col-xs-12 col-sm-3 text-right">
            <label class="profile_label">Email</label> 
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="form-group field-profile-emergencyemail">

                <input type="text" id="profile-emergencyemail" class="form-control" name="emergency_contact_email" value="{{$userObject->emergency_contact_email}}">

                <p class="help-block help-block-error"></p>
            </div>                                </div>
    </div> <!--col-xs-12 end -->

    <div class="col-xs-12 margin_top10">
        <div class="col-xs-12 col-sm-3 text-right">
            <label class="profile_label">Relationship</label> 

        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="form-group field-profile-emergencyrelation">

                <input type="text" id="profile-emergencyrelation" class="form-control" name="emergency_contact_relationship" value="{{$userObject->emergency_contact_relationship}}">

                <p class="help-block help-block-error"></p>
            </div>                                </div>
    </div> <!--col-xs-12 end -->
</div> <!--add_contact end -->