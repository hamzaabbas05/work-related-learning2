<style type="text/css">

    .menumargin
    {
        margin-right:4px;
    }
</style>

<div class="col-xs-12 col-sm-12 margin_top20">
    <div class="btn-group">
        <a id="basicmenu" onclick="showSchoolHouseDiv('basicmenu', 'basics', 'basics')"  type="button"   class="btn btn-primary menumargin">School House</a>

        <a id="propertymenu" style="background-color: #4d4d4d !important;" onclick="showSchoolHouseDiv('propertymenu', 'property_info', 'property')"  type="button"    class="btn btn-primary menumargin">Property</a>
        <!-- <a id="propertyphotomenu" style="background-color: #4d4d4d !important;" onclick="showSchoolHouseDiv(4, 'property_image')"  type="button"    class="btn btn-primary menumargin">Property Images</a>
        -->
        <a id="familyphotomenu" style="background-color: #4d4d4d !important;" onclick="showSchoolHouseDiv('familyphotomenu', 'family_image', 'familyphoto')"  type="button"    class="btn btn-primary menumargin">Family</a>
        <a id="extramenu" style="background-color: #4d4d4d !important;" onclick="showSchoolHouseDiv('extramenu', 'extra', 'extra')"  type="button"   class="btn btn-primary menumargin">Extra</a>
        <a id="includedServicesBtn" style="background-color: #4d4d4d !important;" onclick="showSchoolHouseDiv('includedServicesBtn', 'includedServices', 'includedServices')"  type="button"   class="btn btn-primary menumargin">Included Services</a>
    </div>

</div>