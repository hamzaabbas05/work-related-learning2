<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Over 16 Student Name</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="sname16" class="form-control" name="sname16" value="{{$userObject->sname16}}">

            <p class="help-block help-block-error"></p>
        </div>    <div class="errcls" id="sname16err" style="clear: both;"></div><br/>                         
    </div>
</div> <!--col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Over 16 Student Surname</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="ssurname16" class="form-control" name="ssurname16" value="{{$userObject->ssurname16}}">

            <p class="help-block help-block-error"></p>
        </div>                    <div class="errcls" id="ssurname16err" style="clear: both;"></div><br/>         </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Over 16 Student Email</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="semail16" class="form-control" name="semail16" value="{{$userObject->semail16}}">

            <p class="help-block help-block-error"></p>
        </div>           <div class="errcls" id="semail16err" style="clear: both;"></div><br/>                  </div>
</div> <!--col-xs-12 end -->
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Over 16 Student Phone</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="number" id="sphone16" class="form-control" name="sphone16" value="{{$userObject->sphone16}}">

            <p class="help-block help-block-error"></p>
        </div>          <div class="errcls" id="sphone16err" style="clear: both;"></div><br/>                   </div>
</div> <!--col-xs-12 end -->