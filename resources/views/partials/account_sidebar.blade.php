<div class="col-xs-12 col-sm-3 margin_top20">
    <ul class="profile_left list-unstyled">
        <!-- <li><a href="">Notifications</a></li>
<li><a href="s">User Notifications</a></li> -->
        <li {!! (Request::is('user/changepassword') ? 'class="active"' : '') !!}><a href="{{URL::to('user/changepassword')}}">Security</a></li>
        <li {!! (Request::is('user/transactions/*') ? 'class="active"' : '') !!}><a href="{{URL::to('user/transactions/completed')}}">Transaction History</a></li>
        <li {!! (Request::is('user/wallet') ? 'class="active"' : '') !!}><a href="{{URL::to('user/wallet')}}">Wallet Credits</a></li>  
</ul> 
</div> <!--col-sm-3 end -->