@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
<div class="container">
@include('partials.profile_sidebar')
@inject('country', 'App\Country')
<?php 
$countries = $country->getall();
?>
<div class="col-xs-12 col-sm-9 margin_top20">
			 
             {!! Form::open(array('url' => 'user/profile_submit', 'id' => "form-edit", 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
             @include('partials.errors')
             <div class="airfcfx-panel panel panel-default">
              <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                <h3 class="airfcfx-panel-title panel-title">Over 18 User Basic Info (Under 18 Student's Parent - Work Tutor - Over 18 Student)</h3>
              </div>
              
              <div class="airfcfx-panel-padding panel-body">
                <div class="row ">
                	<div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        
                        <label class="profile_label">First Name</label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                         <div class="form-group field-profile-firstname required">

<input type="text" id="profile-firstname" class="form-control" name="name" value="{{$userObject->name}}" maxlength="30" placeholder="First Name" onkeypress="return isAlpha(event)">

<p class="help-block help-block-error"></p>
</div>           <div class="errcls" id="fnerr" style="clear: both;"></div><br/>
                                   

 </div>
                    </div> <!--col-xs-12 end -->
                    
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Last Name</label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                        <div class="form-group field-profile-lastname required">

<input type="text" id="profile-lastname" class="form-control" name="lastname" maxlength="30" placeholder="Last Name" onkeypress="return isAlpha(event)" value="{{$userObject->lastname}}">

<p class="help-block help-block-error"></p>
</div>                        <p class="margin_top_5 text_gray1">This is only shared once a "booking" is confirmed by the Work Tutor.
</p> <div class="errcls" id="lnerr" style="clear: both;"></div><br/>
                      
                        </div>
                    </div> <!--col-xs-12 end -->
                    
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">I Am<i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                        <select class="form-control" style="width:auto;" id="profile-gender" name="gender">
						          <option value="" >Gender</option>
								  <option value="1" <?php if ($userObject->gender == 1) { ?> selected <?php }?>>Male</option>
								  <option value="2" <?php if ($userObject->gender == 2) { ?> selected <?php }?>>Female</option>
								  <option value="3" <?php if ($userObject->gender == 3) { ?> selected <?php }?>>Other</option>
                        </select>
                        <p class="margin_top_5 text_gray1">This is only shared once a "booking" is confirmed by the Work Tutor.</p>
                        <div class="errcls" id="generr" style="clear: both;"></div><br/>
                        </div>
                    </div> <!--col-xs-12 end -->
                    
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Birth Date <i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
                        </div>
                        <div class="airfcfx-profile-bd col-xs-12 col-sm-9">
                        <?php 
                        $day = "";
                        $month = "";
                        $year = "";
                        if(!is_null($userObject->dob)) {
                             $dob = $userObject->dob;
                            $dobArray = explode("/", $dob);
                            $day = $dobArray[0];
                            $month = $dobArray[1];
                            $year = $dobArray[2];
                        }
                           
                        ?>



						<select class="form-control col-sm-4" id="bmonth" style="width:95px;" name="bmonth">
						<option <?php if ($month == "") { ?> selected <?php }?>value="">Month</option>
        <option value="1" <?php if ($month == 1) { ?> selected <?php }?>>January</option>
        <option value="2" <?php if ($month == 2) { ?> selected <?php }?>>February</option>
        <option value="3" <?php if ($month == 3) { ?> selected <?php }?>>March</option>
        <option value="4" <?php if ($month == 4) { ?> selected <?php }?>>April</option>
        <option value="5" <?php if ($month == 5) { ?> selected <?php }?>>May</option>
        <option value="6" <?php if ($month == 6) { ?> selected <?php }?>>June</option>
        <option value="7" <?php if ($month == 7) { ?> selected <?php }?>>July</option>
        <option value="8" <?php if ($month == 8) { ?> selected <?php }?>>August</option>
        <option value="9" <?php if ($month == 9) { ?> selected <?php }?>>September</option>
        <option value="10" <?php if ($month == 10) { ?> selected <?php }?>>October</option>
        <option value="11" <?php if ($month == 11) { ?> selected <?php }?>>November</option>
        <option value="12" <?php if ($month == 12) { ?> selected <?php }?>>December</option>

                         </select>
                        <select class="form-control col-sm-4 margin_left10" id="bday" style="width:80px;" name="bday">
						  <option value="" <?php if ($day == "") { ?> selected <?php }?>>Day</option>
        <?php for ($i = 1; $i <= 31; $i++) { ?>
          <option <?php if ($day == $i) { ?> selected <?php }?> value="{{$i}}">{{$i}}</option>
          <?php } ?>
                        </select>
                        <select class="form-control col-sm-4 margin_left10" id="byear" style="width:90px;" name="byear">
                         <option value="" <?php if ($year == "") { ?> selected <?php }?>>Year</option>
        <?php $currentYear = date("Y"); 
              $startYear = $currentYear-18;
              $baseYear = 1950;
              for ($i = $startYear;$i>=$baseYear;$i--) { ?>
                <option <?php if ($year == $i) { ?> selected <?php }?> value="{{$i}}">{{$i}}</option>
              <?php } ?>
                        </select>
                        <br/><br/>
                        <p class="margin_top_5 text_gray1">This is only shared once a "booking" is confirmed by the Work Tutor.
</p>
 <div class="errcls" id="doberr" style="clear: both;"></div><br/>

                        </div>
                    </div> <!--col-xs-12 end -->
                    
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Email Address <i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i></label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                        <input type="text" name="email" class="form-control " disabled="true" value="{{$userObject->email}}" placeholder="" style="" />
                        <p class="margin_top_5 text_gray1">This is only shared once a "booking" is confirmed by the Work Tutor.
</p>
                        </div>
                    </div> <!--col-xs-12 end -->
                    
                           
                    
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Phone Number<i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i></label> 
                        </div>
												<div class="col-xs-12 col-sm-9" id="phoneverify">
							<input type="text"  class="form-control"  name="phonenumber" value="{{$userObject->phonenumber}}" onkeypress="return isNumber(event)" onclick="//generate_code();">
							<br /><br />
							<!-- <div id="codeblock" class="hiddencls">
										<div id="codetext"></div>
											<input type="text" value="" class="form-control smalltext" id="verifycode">
											<input type="button" value="Verify" class="btn btn_email" onclick="//verify_code();">
											<div id="codesuccess" class="successtxt"></div>
											<div id="codeerror" class="errcls"></div>
										</div> -->
						</div>
						                      
                            
                        </div> 

                         <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Country of Citizenship</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-state required">

   {!! Form::select('country_id', $countries, $userObject->country_id, ['class'=>'form-control', 'name' => 'country_id', 'id' => 'country_id', 'required']) !!}

<p class="help-block help-block-error"></p>
</div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
                            </div>
                        </div> <!--col-xs-12 end -->
                           <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Relationship with Over 16 student hereunder<i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                        <select class="form-control" style="width:auto;" id="relation_gender" name="student_relationship">
                                  <option value="" >Select</option>
                                  <option value="1" <?php if ($userObject->student_relationship == 1) { ?> selected <?php }?>>Mother</option>
                                  <option value="2" <?php if ($userObject->student_relationship == 2) { ?> selected <?php }?>>Father</option>
                                  <option value="3" <?php if ($userObject->student_relationship == 3) { ?> selected <?php }?>>Legal Guardian
</option>
                        </select>
                        <p class="margin_top_5 text_gray1">This is only shared once a "booking" is confirmed by the Work Tutor.</p>
                        <div class="errcls" id="generr" style="clear: both;"></div><br/>
                        </div>
                    </div> <!--col-xs-12 end -->
                       <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        
                        <label class="profile_label">NIN - National Insurance Number (SSN in the USA, Codice Fiscale in Italy)</label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                         <div class="form-group field-profile-firstname required">

<input type="text" id="nin18" class="form-control" name="NIN18" value="{{$userObject->NIN18}}" maxlength="30" placeholder="National Insurance Number" >

<p class="help-block help-block-error"></p>
</div>                        </div>
                    </div> <!--col-xs-12 end -->
                        

                      <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        
                        <input type="checkbox" <?php if ($userObject->check_allow == 1) { ?> checked <?php } ?> id="allowcheck" name="check_allow">
                        </div>
                        <div class="col-xs-12 col-sm-9">
                         <div class="form-group field-profile-firstname required">

 I allow my son/daughter to leave the Host family after dinner until 10pm

<p class="help-block help-block-error"></p>
</div>                        </div>
                    </div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="padding0 profile_label">Add Address 
</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">                               
                            <a href="javascript:void(0);" class="text-danger add_shipd" > <i class="fa fa-plus text-danger"></i> Add  Address 
</a>
                                <div class="row add_shippingd padding10" style="display:none;">
                                                                
                                <div class=" margin_top10 ">
                                <div class="col-xs-12 col-sm-12" style="padding:0px;">
                                <label class="profile_label"> Address Line1</label>       
                                <div class="form-group field-shippingaddress-address1">
<input type="text" id="daddress18"   class="form-control margin_bottom10" name="daddress18" value="{{$userObject->daddress18}}">

<p class="help-block help-block-error"></p>
</div>                          
                                </div>
                               
                                </div>
                  
                                <div class="margin_top10">                                    
                                    <label class="profile_label">Street Address Number</label>
                                    <div class="form-group field-shippingaddress-city">

<input type="text" id="street_numberd18" class="form-control margin_bottom10" name="street_numberd18" value="{{$userObject->street_numberd18}}">

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>

                                        <div class=" margin_top10">                                    
                                    <label class="profile_label">Street Address Name</label>  
                                    <div class="form-group field-shippingaddress-state">

<input type="text" id="routed18" class="form-control margin_bottom10" name="routed18" value="{{$userObject->routed18}}">

<p class="help-block help-block-error"></p>
</div>                                   
                                </div> <!--col-xs-12 end -->

   <div class=" margin_top10">                                    
                                    <label class="profile_label">Town/City</label>  
                                    <div class="form-group field-shippingaddress-state">

<input type="text" id="localityd18" class="form-control margin_bottom10" name="localityd18" value="{{$userObject->localityd18}}">

<p class="help-block help-block-error"></p>
</div>                                   
                                </div> <!--col-xs-12 end -->

<div class=" margin_top10">                                    
                                    <label class="profile_label">State/Region</label>  
                                    <div class="form-group field-shippingaddress-state">

<input type="text" id="administrative_area_level_1d18" class="form-control margin_bottom10" name="administrative_area_level_1d18" value="{{$userObject->administrative_area_level_1d18}}">

<p class="help-block help-block-error"></p>
</div>                                   
                                </div> <!--col-xs-12 end -->


                                
                                <div class=" margin_top10">                                    
                                    <label class="profile_label">ZIP / Postal Code</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="postal_coded18" class="form-control margin_bottom10" name="postal_coded18" value="{{$userObject->postal_coded18}}" onkeypress="return isNumber(event)">

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>

 <div class=" margin_top10">                                    
                                    <label class="profile_label">Country</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="countryd18" class="form-control margin_bottom10" name="countryd18" value="{{$userObject->countryd18}}" >

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>

                            <!--  <div class="text-center">
                                    <button class="btn btn_email airfcfx-panel pull-right" data-toggle="tooltip" data-placement="bottom" title="Please enter your address to continue">Save</button>
                                </div> -->
                               
                                </div>
                            </div>
                    </div> <!--col-xs-12 end -->

                       
                    
                </div> <!--row end -->
                
              </div>
              
            </div> <!--Panel end -->
            
        	<div class="airfcfx-panel panel panel-default">
              <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                <h3 class="airfcfx-panel-title panel-title">Over 16  High School Student</h3>
              </div>
              
              <div class="airfcfx-panel-padding panel-body">
                <div class="row">
                 <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Over 16 Student Name</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="sname16" class="form-control" name="sname16" value="{{$userObject->sname16}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->


                     <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Over 16 Student surname</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="ssurname16" class="form-control" name="ssurname16" value="{{$userObject->ssurname16}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->

                      <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Over 16 Student Email</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="semail16" class="form-control" name="semail16" value="{{$userObject->semail16}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->


                      <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Over 16 Student Phone</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="sphone16" class="form-control" name="sphone16" value="{{$userObject->sphone16}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->


                  <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Describe Yourself</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-about">

<textarea id="profile-about" class="form-control" name="about_yourself" rows="5">{{$userObject->about_yourself}}</textarea>

<p class="help-block help-block-error"></p>
</div>                            
                            </div>
                        </div> <!--col-xs-12 end -->
                	 <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Birth Date <i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
                        </div>
                        <div class="airfcfx-profile-bd col-xs-12 col-sm-9">
                        <?php 
                        $day = "";
                        $month = "";
                        $year = "";
                        if(!is_null($userObject->dob16)) {
                             $dob = $userObject->dob16;
                            $dobArray = explode("/", $dob);
                            $day = $dobArray[0];
                            $month = $dobArray[1];
                            $year = $dobArray[2];
                        }
                           
                        ?>
                    <select class="form-control col-sm-4" style="width:95px;" name="bmonth16">
                        <option <?php if ($month == "") { ?> selected <?php }?>value="">Month</option>
        <option value="1" <?php if ($month == 1) { ?> selected <?php }?>>January</option>
        <option value="2" <?php if ($month == 2) { ?> selected <?php }?>>February</option>
        <option value="3" <?php if ($month == 3) { ?> selected <?php }?>>March</option>
        <option value="4" <?php if ($month == 4) { ?> selected <?php }?>>April</option>
        <option value="5" <?php if ($month == 5) { ?> selected <?php }?>>May</option>
        <option value="6" <?php if ($month == 6) { ?> selected <?php }?>>June</option>
        <option value="7" <?php if ($month == 7) { ?> selected <?php }?>>July</option>
        <option value="8" <?php if ($month == 8) { ?> selected <?php }?>>August</option>
        <option value="9" <?php if ($month == 9) { ?> selected <?php }?>>September</option>
        <option value="10" <?php if ($month == 10) { ?> selected <?php }?>>October</option>
        <option value="11" <?php if ($month == 11) { ?> selected <?php }?>>November</option>
        <option value="12" <?php if ($month == 12) { ?> selected <?php }?>>December</option>

                         </select>
                        <select class="form-control col-sm-4 margin_left10" style="width:80px;" name="bday16">
                          <option value="" <?php if ($day == "") { ?> selected <?php }?>>Day</option>
        <?php for ($i = 1; $i <= 31; $i++) { ?>
          <option <?php if ($day == $i) { ?> selected <?php }?> value="{{$i}}">{{$i}}</option>
          <?php } ?>
                        </select>
                        <select class="form-control col-sm-4 margin_left10" style="width:90px;" name="byear16">
                         <option value="" <?php if ($year == "") { ?> selected <?php }?>>Year</option>
        <?php $currentYear = date("Y"); 
              $startYear = $currentYear-18;
              $baseYear = 1950;
              for ($i = $startYear;$i>=$baseYear;$i--) { ?>
                <option <?php if ($year == $i) { ?> selected <?php }?> value="{{$i}}">{{$i}}</option>
              <?php } ?>
                        </select>
                        <br/><br/>
                        
                        </div>
                    </div> <!--col-xs-12 end -->
                    
                    <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Place of Birth</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-work">

<input type="text" id="pob16" class="form-control" name="pob16" value="{{$userObject->pob16}}">

<p class="help-block help-block-error"></p>
</div>                            
                            </div>
                    </div> <!--col-xs-12 end -->

     <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Medical or learning Condition</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-work">
<textarea id="learning_condition" class="form-control" rows="10"  name="medical_learning">{{$userObject->medical_learning}}</textarea>


<p class="help-block help-block-error"></p>
</div>                            
                            </div>
                    </div> <!--col-xs-12 end -->


                         <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Notes for Host family</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-work">

<textarea id="family_notes" class="form-control" rows="10" name="host_family_notes">{{$userObject->host_family_notes}}</textarea>


<p class="help-block help-block-error"></p>
</div>                            
                            </div>
                    </div> <!--col-xs-12 end -->



                         <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Notes for work tutor</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-work">

<textarea id="tutor_notes" class="form-control" rows="10" name="work_tutor_notes">{{$userObject->work_tutor_notes}}</textarea>


<p class="help-block help-block-error"></p>
</div>                            
                            </div>
                    </div> <!--col-xs-12 end -->


                         <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">NIN - National Insurance Number (SSN in the USA, Codice Fiscale in Italy)</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-work">

<input type="text" id="nin16" class="form-control" rows="10" value="{{$userObject->NIN16}}" name="NIN16">

<p class="help-block help-block-error"></p>
</div>                            
                            </div>
                    </div> <!--col-xs-12 end -->

                   <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">School Name</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

 

<select name="school_name" class="form-control"> 
<option value="">Select school</option>
<?php foreach($schools as $sc) { ?> <option <?php if($sc->id == $userObject->school_name) { ?> selected <?php } ?> value="{{$sc->id}}">{{$sc->name}}</option><?php }?>
</select>




<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->

                    <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">class letter</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="class_letter" class="form-control" name="class_letter" value="{{$userObject->class_letter}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->

                    <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">class number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="class_number" class="form-control" name="class_number" value="{{$userObject->class_number}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->

                    <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">matricule/student number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="student_number" class="form-control" name="student_number" value="{{$userObject->student_number}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->

                    <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Academic Qualifications <span clolor="#ccc">eg. Middle school diploma/licenza media</span></label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="academic_qualifications" class="form-control" name="academic_qualifications" value="{{$userObject->academic_qualifications}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->

                    <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Date of attainment</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="date_attain" class="form-control" name="date_of_attainment" value="{{$userObject->date_of_attainment}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="padding0 profile_label">Add Domiciliary Address
</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">                               
                            <a href="javascript:void(0);" class="text-danger add_shipd16" > <i class="fa fa-plus text-danger"></i> Add Domiciliary Address
</a>
                                <div class="row add_shippingd16 padding10" style="display:none;">
                                                                
                                <div class=" margin_top10 ">
                                <div class="col-xs-12 col-sm-12" style="padding:0px;">
                                <label class="profile_label">Domiciliary Address Line1</label>       
                                <div class="form-group field-shippingaddress-address1">
<input type="text" id="daddress16" class="form-control margin_bottom10" name="daddress16" value="{{$userObject->daddress16}}">

<p class="help-block help-block-error"></p>
</div>                          
                                </div>
                               
                                </div>
                                
                        
                                <div class="margin_top10">                                    
                                    <label class="profile_label">Street Address Number</label>
                                    <div class="form-group field-shippingaddress-city">

<input type="text" id="street_numberd16" class="form-control margin_bottom10" name="street_numberd16" value="{{$userObject->street_numberd16}}">

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>

                                        <div class=" margin_top10">                                    
                                    <label class="profile_label">Street Address Name</label>  
                                    <div class="form-group field-shippingaddress-state">

<input type="text" id="routed16" class="form-control margin_bottom10" name="routed16" value="{{$userObject->routed16}}">

<p class="help-block help-block-error"></p>
</div>                                   
                                </div> <!--col-xs-12 end -->
                                
                                <div class=" margin_top10">                                    
                                    <label class="profile_label">Town/City</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="localityd16" class="form-control margin_bottom10" name="localityd16" value="{{$userObject->localityd16}}" onkeypress="return isNumber(event)">

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>

                                    <div class=" margin_top10">                                    
                                    <label class="profile_label">State/Region</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="administrative_area_level_1d16" class="form-control margin_bottom10" name="administrative_area_level_1d16" value="{{$userObject->administrative_area_level_1d16}}"  >

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>


                                         <div class=" margin_top10">                                    
                                    <label class="profile_label">ZIP / Postal Code</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="postal_coded16" class="form-control margin_bottom10" name="postal_coded16" value="{{$userObject->postal_coded16}}"  >

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>
                               <div class=" margin_top10">                                    
                                    <label class="profile_label">Country</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="countryd16" class="form-control margin_bottom10" name="countryd16" value="{{$userObject->countryd16}}"  >

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>
                              
                                
                               <!--  <div class="text-center">
                                    <button class="btn btn_email airfcfx-panel pull-right" data-toggle="tooltip" data-placement="bottom" title="Please enter your address to continue">Save</button>
                                </div> -->
                               
                                </div>
                            </div>
                    </div> <!--col-xs-12 end -->
 <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="padding0 profile_label">Add Residency Address
</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">                               
                            <a href="javascript:void(0);" class="text-danger add_ship" > <i class="fa fa-plus text-danger"></i> Add Residency Address
</a>
                                <div class="row add_shipping padding10" style="display:none;">
                                                                
                                <div class=" margin_top10 ">
                                <div class="col-xs-12 col-sm-12" style="padding:0px;">
                                <label class="profile_label">Residency Address [city,state,pincode]</label>       
                                <div class="form-group field-shippingaddress-address1">
<input type="text" id="raddress16" class="form-control margin_bottom10" name="raddress16" value="{{$userObject->raddress16}}">


<p class="help-block help-block-error"></p>
</div>                          
                                </div>
                               
                                </div>
                                 <div class="margin_top10">                                    
                                    <label class="profile_label">Street Address Number</label>
                                    <div class="form-group field-shippingaddress-city">

<input type="text" id="street_numberr16" class="form-control margin_bottom10" name="street_numberr16" value="{{$userObject->street_numberr16}}">

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>
                                 <div class=" margin_top10">                                    
                                    <label class="profile_label">Street Address Name</label>  
                                    <div class="form-group field-shippingaddress-state">

<input type="text" id="router16" class="form-control margin_bottom10" name="routed16" value="{{$userObject->router16}}">

<p class="help-block help-block-error"></p>
</div>                                   
                                </div> <!--col-xs-12 end -->
                                
                                <div class=" margin_top10">                                    
                                    <label class="profile_label">Town/City</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="localityr16" class="form-control margin_bottom10" name="localityr16" value="{{$userObject->localityr16}}" onkeypress="return isNumber(event)">

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>

                                    <div class=" margin_top10">                                    
                                    <label class="profile_label">State/Region</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="administrative_area_level_1r16" class="form-control margin_bottom10" name="administrative_area_level_1r16" value="{{$userObject->administrative_area_level_1r16}}"  >

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>


                                         <div class=" margin_top10">                                    
                                    <label class="profile_label">ZIP / Postal Code</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="postal_coder16" class="form-control margin_bottom10" name="postal_coder16" value="{{$userObject->postal_coder16}}"  >

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>
                               <div class=" margin_top10">                                    
                                    <label class="profile_label">Country</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="countryr16" class="form-control margin_bottom10" name="countryr16" value="{{$userObject->countryr16}}"  >

<p class="help-block help-block-error"></p>
</div>                                     
                                </div>
                              
                                
                                
                                
                             
                               <!--  <div class="text-center">
                                    <button class="btn btn_email airfcfx-panel pull-right" data-toggle="tooltip" data-placement="bottom" title="Please enter your address to continue">Save</button>
                                </div> -->
                               
                                </div>
                            </div>
                    </div> <!--col-xs-12 end -->









                    <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">School Tutor - Name and Surname</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="school_tutor_name" class="form-control" name="school_tutor_name" value="{{$userObject->school_tutor_name}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->

                     <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">School Tutor - Email</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="school_tutor_email" class="form-control" name="school_tutor_email" value="{{$userObject->school_tutor_email}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->

                     <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">School Tutor - Number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                             <div class="form-group field-profile-school">

<input type="text" id="school_tutor_number" class="form-control" name="school_tutor_number" value="{{$userObject->school_tutor_number}}">

<p class="help-block help-block-error"></p>
</div>                            </div>
                    </div> <!--col-xs-12 end -->
                    
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                        <label class="padding0 profile_label">Emergency Contact <i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i></label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                        <a href="javascript:void(0);" class="text-danger add_cont" > <i class="fa fa-plus text-danger"></i> Add Contact</a>
                        <p class="margin_top_5 text_gray1">Give our Customer Experience team a trusted contact we can alert in an urgent situation.</p>
                        </div>
                        
                    </div> <!--col-xs-12 end -->
                    
                     <div class="row add_contact" style="display:none;">
                        	<div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Name</label> 
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-emergencyname">

<input type="text" id="profile-emergencyname" class="form-control" name="emergency_contact_name" value="{{$userObject->emergency_contact_name}}">

<p class="help-block help-block-error"></p>
</div>                                </div>
                            </div> <!--col-xs-12 end -->
                    
                            <div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Phone</label> 
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-emergencyno">

<input type="text" id="profile-emergencyno" class="form-control" name="emergency_contact_phone" value="{{$userObject->emergency_contact_phone}}" onkeypress="return isNumber(event)">

<p class="help-block help-block-error"></p>
</div>                                </div>
                            </div> <!--col-xs-12 end -->
                            
                            <div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Email</label> 
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-emergencyemail">

<input type="text" id="profile-emergencyemail" class="form-control" name="emergency_contact_email" value="{{$userObject->emergency_contact_email}}">

<p class="help-block help-block-error"></p>
</div>                                </div>
                            </div> <!--col-xs-12 end -->
                    
                            <div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Relationship</label> 

                                </div>
                                <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-emergencyrelation">

<input type="text" id="profile-emergencyrelation" class="form-control" name="emergency_contact_relationship" value="{{$userObject->emergency_contact_relationship}}">

<p class="help-block help-block-error"></p>
</div>                                </div>
                            </div> <!--col-xs-12 end -->
                        </div> <!--add_contact end -->
                    
                   


                    
                    
              </div> <!--row end -->
              </div>
              
            </div> <!--Panel end -->
            
         
       
        
        <div class="form-group">
            <button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20" onclick="return edit_profile();">Submit</button> </div>
     </form>
      </div> <!--col-sm-9 end -->
        
    </div> <!--container end -->
</div>
	
 	
	</form><script>
$(document).ready(function(){   
$('#form-edit').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

    $(".show_ph").click(function(){
        $(".add_phone").show();
		$(".show_ph").hide();
    });
	$(".add_cont").click(function(){
        $(".add_contact").toggle();		
    });
	$(".add_ship").click(function(){
        $(".add_shipping").toggle();		
    });

    $(".add_shipd").click(function(){
        $(".add_shippingd").toggle();        
    });

    $(".add_shipd16").click(function(){
        $(".add_shippingd16").toggle();        
    });
});
</script>
<style type="text/css">
.field-profile-phoneno
{
	display:inline;
}
.help-block-error{
clear: both;
}
</style>

 <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };
google.maps.event.addDomListener(window, 'load', function () {

    var places = new google.maps.places.Autocomplete(document.getElementById('daddress18'));
            google.maps.event.addListener(places, 'place_changed', function () {
                    var place = places.getPlace();

                    for (var component in componentForm) {
                        var cstr = "d18";
                        document.getElementById(component+cstr).value = '';
                        document.getElementById(component+cstr).disabled = false;
                    }

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        var cstr = "d18";
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType+cstr).value = val;
                        }
                    }
             });

    var places1= new google.maps.places.Autocomplete(document.getElementById('daddress16'));
            google.maps.event.addListener(places1, 'place_changed', function () {
                    var place = places1.getPlace();

                    for (var component in componentForm) {
                        var cstr = "d16";
                        document.getElementById(component+cstr).value = '';
                        document.getElementById(component+cstr).disabled = false;
                    }

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        var cstr = "d16";
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType+cstr).value = val;
                        }
                    }
             });


    var places2 = new google.maps.places.Autocomplete(document.getElementById('raddress16'));
            google.maps.event.addListener(places2, 'place_changed', function () {
                    var place = places2.getPlace();

                    for (var component in componentForm) {
                        var cstr = "r16";
                        document.getElementById(component+cstr).value = '';
                        document.getElementById(component+cstr).disabled = false;
                    }

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        var cstr = "r16";
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType+cstr).value = val;
                        }
                    }
             });
});
      

       
    </script>
 @stop