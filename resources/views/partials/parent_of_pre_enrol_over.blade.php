<div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">School Info</h3>
        <br>
        <p>Select from the list provided herein, if your school is not in the list, contact us via the email on the footer.</P>
        <p>If you don't know who your School Tutor is, ask your School.</P>
        <p>Your School Tutor may temporarily be the Person travelling with you.</P>
    </div>
    <div class="airfcfx-panel-padding panel-body">
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">School Name</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">



                    <select id="school_name16" name="school_name16" class="form-control"  > 
                        <option value="">Select School</option>
                        <?php foreach ($schools as $sc) { ?> <option <?php if ($sc->id == $userObject->school_name) { ?> selected <?php } ?> value="{{$sc->id}}">{{$sc->name}}</option><?php } ?>
                    </select>




                    <p class="help-block help-block-error"></p>
                </div>                   <div class="errcls" id="school_name16err" style="clear: both;"></div><br/>          </div>
        </div> <!--col-xs-12 end -->


        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Class Letter</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_letter16" class="form-control" name="class_letter16" value="{{$userObject->class_letter}}"  >

                    <p class="help-block help-block-error"></p>
                </div>           <div class="errcls" id="class_letter16err" style="clear: both;"></div><br/>                  </div>
        </div> <!--col-xs-12 end -->


        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Class Number</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_number16" class="form-control" name="class_number16" value="{{$userObject->class_number}}"  >

                    <p class="help-block help-block-error"></p>
                </div>          <div class="errcls" id="class_number16err" style="clear: both;"></div><br/>                   </div>
        </div> <!--col-xs-12 end -->

    </div>
</div>
<div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">In case of group departure</h3>
        <p>If your group is not in the list, please contact us at the email here under in the footer.</p>
        <p>If you are travelling alone, or in a small group without an adult travelling with you, ignore this part.</p>
    </div>
    <div class="airfcfx-panel-padding panel-body">
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Select your Group</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">
                    <select id="secondary_group_parent_id" class="form-control" name="secondary_group_parent_id">
                        <option value="">-- Select Group --</option>
                        @if(isset($groups) && sizeof($groups) > 0)
                        @foreach($groups as $group)
                        <?php
                        $group_in = isset($group->group_date_in) ? date("Y-m-d", strtotime($group->group_date_in)) : "";
                        $current_date = date("Y-m-d");
                        if ($group_in > $current_date) {
                            ?>
                            <option value="{{$group->id}}" @if(isset($userObject->secondary_group_id) && $userObject->secondary_group_id == $group->id) selected="" @endif>{{$group->group_name}}</option>
                        <?php } ?>
                        @endforeach
                        @endif
                    </select>
                    <p class="help-block help-block-error"></p>
                </div>         
                <div class="errcls" id="secondary_group_parent_iderr" style="clear: both;"></div><br/>
            </div>
        </div>
    </div>
</div>