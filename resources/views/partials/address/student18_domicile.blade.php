      <div class=" margin_top10 ">
                                <div class="col-xs-12 col-sm-12" style="padding:0px;">
                                <label class="profile_label">Domiciliary - Start typing, add postal code, and select from list</label>       
                                <div class="form-group field-shippingaddress-address1">
<input type="text" id="s18domicileaddress" class="form-control margin_bottom10" name="s18domicileaddress" value="{{$userObject->daddress18}}">

<p class="help-block help-block-error"></p>
</div>    <div class="errcls" id="s18domicileaddresserr" style="clear: both;"></div><br/>                       
                                </div>
                               
                                </div>
                                
                        
                                <div class="margin_top10">                                    
                                    <label class="profile_label">Street Address Number</label>
                                    <div class="form-group field-shippingaddress-city">

<input type="text" id="street_numbersd18" class="form-control margin_bottom10" name="street_numbersd18" value="{{$userObject->street_numberd18}}">

<p class="help-block help-block-error"></p><div class="errcls" id="street_numbersd18err" style="clear: both;"></div><br/> 
</div>                                     
                                </div>

                                        <div class=" margin_top10">                                    
                                    <label class="profile_label">Street Address Name</label>  
                                    <div class="form-group field-shippingaddress-state">

<input type="text" id="routesd18" class="form-control margin_bottom10" name="routesd18" value="{{$userObject->routed18}}">

<p class="help-block help-block-error"></p><div class="errcls" id="routesd18err" style="clear: both;"></div><br/> 
</div>                                   
                                </div> <!--col-xs-12 end -->
                                
                                <div class=" margin_top10">                                    
                                    <label class="profile_label">Town/City</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="localitysd18" class="form-control margin_bottom10" name="localitysd18" value="{{$userObject->localityd18}}"  >

<p class="help-block help-block-error"></p><div class="errcls" id="localitysd18err" style="clear: both;"></div><br/> 
</div>                                     
                                </div>

                                    <div class=" margin_top10">                                    
                                    <label class="profile_label">State/Region</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="administrative_area_level_1sd18" class="form-control margin_bottom10" name="administrative_area_level_1sd18" value="{{$userObject->administrative_area_level_1d18}}"  >

<p class="help-block help-block-error"></p><div class="errcls" id="administrative_area_level_1sd18err" style="clear: both;"></div><br/> 
</div>                                     
                                </div>


                                         <div class=" margin_top10">                                    
                                    <label class="profile_label">ZIP / Postal Code</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="postal_codesd18" class="form-control margin_bottom10" name="postal_codesd18" value="{{$userObject->postal_coded18}}"  >

<p class="help-block help-block-error"></p><div class="errcls" id="postal_codesd18err" style="clear: both;"></div><br/> 
</div>                                     
                                </div>
                               <div class=" margin_top10">                                    
                                    <label class="profile_label">Country</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="countrysd18" class="form-control margin_bottom10" name="countrysd18" value="{{$userObject->countryd18}}"  >

<p class="help-block help-block-error"></p><div class="errcls" id="countrysd18err" style="clear: both;"></div><br/> 
</div>                                     
                                </div>
                              
                                
                               <!--  <div class="text-center">
                                    <button class="btn btn_email airfcfx-panel pull-right" data-toggle="tooltip" data-placement="bottom" title="Please enter your address to continue">Save</button>
                                </div> -->