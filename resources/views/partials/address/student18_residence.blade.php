<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="padding0 profile_label">Residency - Start typing, add postal code, and select from list
        </label> 
    </div>
    <div class="col-xs-12 col-sm-9">                               
        <div class="row add_shipping padding10" style="display:block;">

            <div class=" margin_top10 ">
                <div class="col-xs-12 col-sm-12" style="padding:0px;">
                    <label class="profile_label">Residency Address [city,state,pincode]</label>       
                    <div class="form-group field-shippingaddress-address1">
                        <input type="text" id="over18raddress" class="form-control margin_bottom10" name="over18raddress" value="{{$userObject->raddress18}}">


                        <p class="help-block help-block-error"></p><div class="errcls" id="over18raddresserr" style="clear: both;"></div><br/> 
                    </div>                          
                </div>

            </div>
            <div class="margin_top10">                                    
                <label class="profile_label">Street Address Number</label>
                <div class="form-group field-shippingaddress-city">

                    <input type="text" id="street_numbersr18" class="form-control margin_bottom10" name="street_numbersr18" value="{{$userObject->street_numberr18}}"  >

                    <p class="help-block help-block-error"></p><div class="errcls" id="street_numbersr18err" style="clear: both;"></div><br/> 
                </div>                                     
            </div>
            <div class=" margin_top10">                                    
                <label class="profile_label">Street Address Name</label>  
                <div class="form-group field-shippingaddress-state">

                    <input type="text" id="routesr18" class="form-control margin_bottom10" name="routesr18" value="{{$userObject->router18}}"  >

                    <p class="help-block help-block-error"></p><div class="errcls" id="routesr18err" style="clear: both;"></div><br/> 
                </div>                                   
            </div> <!--col-xs-12 end -->

            <div class=" margin_top10">                                    
                <label class="profile_label">Town/City</label>
                <div class="form-group field-shippingaddress-zipcode">

                    <input type="text" id="localitysr18" class="form-control margin_bottom10" name="localitysr18" value="{{$userObject->localityr18}}"    >

                    <p class="help-block help-block-error"></p><div class="errcls" id="localitysr18err" style="clear: both;"></div><br/> 
                </div>                                     
            </div>

            <div class=" margin_top10">                                    
                <label class="profile_label">State/Region</label>
                <div class="form-group field-shippingaddress-zipcode">

                    <input type="text" id="administrative_area_level_1sr18" class="form-control margin_bottom10" name="administrative_area_level_1sr18"   value="{{$userObject->administrative_area_level_1r18}}"  >

                    <p class="help-block help-block-error"></p><div class="errcls" id="administrative_area_level_1sr18err" style="clear: both;"></div><br/> 
                </div>                                     
            </div>


            <div class=" margin_top10">                                    
                <label class="profile_label">ZIP / Postal Code</label>
                <div class="form-group field-shippingaddress-zipcode">

                    <input type="text" id="postal_codesr18" class="form-control margin_bottom10" name="postal_codesr18" value="{{$userObject->postal_coder18}}"   >

                    <p class="help-block help-block-error"></p><div class="errcls" id="postal_codesr18err" style="clear: both;"></div><br/> 
                </div>                                     
            </div>
            <div class=" margin_top10">                                    
                <label class="profile_label">Country</label>
                <div class="form-group field-shippingaddress-zipcode">

                    <input type="text" id="countrysr18" class="form-control margin_bottom10" name="countrysr18" value="{{$userObject->countryr18}}"   >

                    <p class="help-block help-block-error"></p>
                </div>    <div class="errcls" id="countrysr18err" style="clear: both;"></div><br/>                                  
            </div>
        </div>
    </div>
</div> <!--Residency Address End -->