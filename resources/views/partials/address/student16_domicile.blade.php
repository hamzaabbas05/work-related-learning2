      <div class=" margin_top10 ">
                                <div class="col-xs-12 col-sm-12" style="padding:0px;">
                                <label class="profile_label">Domiciliary - Start typing, add postal code, and select from list</label>       
                                <div class="form-group field-shippingaddress-address1">
<input type="text" id="daddress16" class="form-control margin_bottom10" name="daddress16" value="{{$userObject->daddress16}}">

<p class="help-block help-block-error"></p>
</div>                  
<div class="errcls" id="daddress16err" style="clear: both;"></div><br/>          
                                </div>
                               
                                </div>
                                
                        
                                <div class="margin_top10">                                    
                                    <label class="profile_label">Street Address Number</label>
                                    <div class="form-group field-shippingaddress-city">

<input type="text" id="street_numberd16" class="form-control margin_bottom10" name="street_numberd16" value="{{$userObject->street_numberd16}}">

<p class="help-block help-block-error"></p> 
<div class="errcls" id="street_numberd16err" style="clear: both;"></div><br/> 
</div>                                     
                                </div>

                                        <div class=" margin_top10">                                    
                                    <label class="profile_label">Street Address Name</label>  
                                    <div class="form-group field-shippingaddress-state">

<input type="text" id="routed16" class="form-control margin_bottom10" name="routed16" value="{{$userObject->routed16}}">

<p class="help-block help-block-error"></p> 
<div class="errcls" id="routed16err" style="clear: both;"></div><br/> 
</div>                                   
                                </div> <!--col-xs-12 end -->
                                
                                <div class=" margin_top10">                                    
                                    <label class="profile_label">Town/City</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="localityd16" class="form-control margin_bottom10" name="localityd16" value="{{$userObject->localityd16}}" onkeypress="return isNumber(event)">

<p class="help-block help-block-error"></p> 
<div class="errcls" id="localityd16err" style="clear: both;"></div><br/> 
</div>                                     
                                </div>

                                    <div class=" margin_top10">                                    
                                    <label class="profile_label">State/Region</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="administrative_area_level_1d16" class="form-control margin_bottom10" name="administrative_area_level_1d16" value="{{$userObject->administrative_area_level_1d16}}"  >

<p class="help-block help-block-error"></p> 
<div class="errcls" id="administrative_area_level_1d16err" style="clear: both;"></div><br/> 
</div>                                     
                                </div>


                                         <div class=" margin_top10">                                    
                                    <label class="profile_label">ZIP / Postal Code</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="postal_coded16" class="form-control margin_bottom10" name="postal_coded16" value="{{$userObject->postal_coded16}}"  >

<p class="help-block help-block-error"></p> 
<div class="errcls" id="postal_coded16err" style="clear: both;"></div><br/> 
</div>                                     
                                </div>
                               <div class=" margin_top10">                                    
                                    <label class="profile_label">Country</label>
                                    <div class="form-group field-shippingaddress-zipcode">

<input type="text" id="countryd16" class="form-control margin_bottom10" name="countryd16" value="{{$userObject->countryd16}}"  >

<p class="help-block help-block-error"></p> 
<div class="errcls" id="countryd16err" style="clear: both;"></div><br/> 
</div>                                     
                                </div>
                              
                                
                               <!--  <div class="text-center">
                                    <button class="btn btn_email airfcfx-panel pull-right" data-toggle="tooltip" data-placement="bottom" title="Please enter your address to continue">Save</button>
                                </div> -->