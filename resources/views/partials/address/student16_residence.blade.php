<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="padding0 profile_label">Residency - Start typing, add postal code, and select from list
        </label> 
    </div>
    <div class="col-xs-12 col-sm-9">                               
        <div class="row add_shipping padding10" style="display:block;">

            <div class=" margin_top10 ">
                <div class="col-xs-12 col-sm-12" style="padding:0px;">
                    <label class="profile_label">Residency Address [city,state,pincode]</label>       
                    <div class="form-group field-shippingaddress-address1">
                        <input type="text" id="sraddress16" class="form-control margin_bottom10" name="sraddress16" value="{{$userObject->raddress16}}"  >


                        <p class="help-block help-block-error"></p>
                    </div>                      <div class="errcls" id="sraddress16err" style="clear: both;"></div><br/>     
                </div>

            </div>
            <div class="margin_top10">                                    
                <label class="profile_label">Street Address Number</label>
                <div class="form-group field-shippingaddress-city">

                    <input type="text" id="street_numbersr16" class="form-control margin_bottom10" name="street_numbersr16" value="{{$userObject->street_numberr16}}"  >

                    <p class="help-block help-block-error"></p><div class="errcls" id="street_numbersr16err" style="clear: both;"></div><br/> 
                </div>                                     
            </div>
            <div class=" margin_top10">                                    
                <label class="profile_label">Street Address Name</label>  
                <div class="form-group field-shippingaddress-state">

                    <input type="text" id="routesr16" class="form-control margin_bottom10" name="routesr16" value="{{$userObject->router16}}"  >

                    <p class="help-block help-block-error"></p><div class="errcls" id="routesr16err" style="clear: both;"></div><br/> 
                </div>                                   
            </div> <!--col-xs-12 end -->

            <div class=" margin_top10">                                    
                <label class="profile_label">Town/City</label>
                <div class="form-group field-shippingaddress-zipcode">

                    <input type="text" id="localitysr16" class="form-control margin_bottom10" name="localitysr16" value="{{$userObject->localityr16}}"   >

                    <p class="help-block help-block-error"></p><div class="errcls" id="localitysr16err" style="clear: both;"></div><br/> 
                </div>                                     
            </div>

            <div class=" margin_top10">                                    
                <label class="profile_label">State/Region</label>
                <div class="form-group field-shippingaddress-zipcode">

                    <input type="text" id="administrative_area_level_1sr16" class="form-control margin_bottom10" name="administrative_area_level_1sr16"   value="{{$userObject->administrative_area_level_1r16}}"  >

                    <p class="help-block help-block-error"></p><div class="errcls" id="administrative_area_level_1sr16err" style="clear: both;"></div><br/> 
                </div>                                     
            </div>


            <div class=" margin_top10">                                    
                <label class="profile_label">ZIP / Postal Code</label>
                <div class="form-group field-shippingaddress-zipcode">

                    <input type="text" id="postal_codesr16" class="form-control margin_bottom10" name="postal_codesr16" value="{{$userObject->postal_coder16}}"   >

                    <p class="help-block help-block-error"></p><div class="errcls" id="postal_codesr16err" style="clear: both;"></div><br/> 
                </div>                                     
            </div>
            <div class=" margin_top10">                                    
                <label class="profile_label">Country</label>
                <div class="form-group field-shippingaddress-zipcode">

                    <input type="text" id="countrysr16" class="form-control margin_bottom10" name="countrysr16" value="{{$userObject->countryr16}}"   >

                    <p class="help-block help-block-error"></p><div class="errcls" id="countrysr16err" style="clear: both;"></div><br/> 
                </div>                                     
            </div>
        </div>
    </div>
</div> <!--Residency Address End -->
