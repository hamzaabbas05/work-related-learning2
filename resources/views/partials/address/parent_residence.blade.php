<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="padding0 profile_label">Residency - Start typing, add postal code, and select from list
        </label> 
    </div>
    <div class="col-xs-12 col-sm-9">                               
        <div class="row add_shipping padding10" style="display:block;">
            <div class=" margin_top10 ">
                <div class="col-xs-12 col-sm-12" style="padding:0px;">
                    <label class="profile_label"> Address Line1</label>       
                    <div class="form-group field-shippingaddress-address1">
                        <input type="text" id="parentaddress"   class="form-control margin_bottom10" name="parentaddress" value="{{$userObject->raddress18}}" >

                        <p class="help-block help-block-error"></p>
                    </div>                      
                    <div class="errcls" id="raddress18err" style="clear: both;"></div><br/>      
                </div>

            </div>

            <div class="margin_top10">                                    
                <label class="profile_label">Street Address Number</label>
                <div class="form-group field-shippingaddress-city">

                    <input type="text" id="street_numberp" class="form-control margin_bottom10" name="street_numberp" value="{{$userObject->street_numberr18}}"  >

                    <p class="help-block help-block-error"></p> 
                    <div class="errcls" id="street_numberperr" style="clear: both;"></div><br/> 
                </div>                                     
            </div>

            <div class=" margin_top10">                                    
                <label class="profile_label">Street Address Name</label>  
                <div class="form-group field-shippingaddress-state">

                    <input type="text" id="routep" class="form-control margin_bottom10" name="routep" value="{{$userObject->router18}}"  >

                    <p class="help-block help-block-error"></p> 
                    <div class="errcls" id="routeperr" style="clear: both;"></div><br/> 
                </div>                                   
            </div> <!--col-xs-12 end -->

            <div class=" margin_top10">                                    
                <label class="profile_label">Town/City</label>  
                <div class="form-group field-shippingaddress-state">

                    <input type="text" id="localityp" class="form-control margin_bottom10" name="localityp" value="{{$userObject->localityr18}}"  >

                    <p class="help-block help-block-error"></p> 
                    <div class="errcls" id="localityperr" style="clear: both;"></div><br/> 
                </div>                                   
            </div> <!--col-xs-12 end -->

            <div class=" margin_top10">                                    
                <label class="profile_label">State/Region</label>  
                <div class="form-group field-shippingaddress-state">

                    <input type="text" id="administrative_area_level_1p" class="form-control margin_bottom10" name="administrative_area_level_1p" value="{{$userObject->administrative_area_level_1r18}}"  >

                    <p class="help-block help-block-error"></p> 
                    <div class="errcls" id="administrative_area_level_1perr" style="clear: both;"></div><br/> 
                </div>                                   
            </div> <!--col-xs-12 end -->



            <div class=" margin_top10">                                    
                <label class="profile_label">ZIP / Postal Code</label>
                <div class="form-group field-shippingaddress-zipcode">

                    <input type="text" id="postal_codep" class="form-control margin_bottom10" name="postal_codep" value="{{$userObject->postal_coder18}}"   >

                    <p class="help-block help-block-error"></p> 
                    <div class="errcls" id="postal_codeperr" style="clear: both;"></div><br/> 
                </div>                                     
            </div>

            <div class=" margin_top10">                                    
                <label class="profile_label">Country</label>
                <div class="form-group field-shippingaddress-zipcode">

                    <input type="text" id="countryp" class="form-control margin_bottom10" name="countryp" value="{{$userObject->countryr18}}"   >

                    <p class="help-block help-block-error"></p> 
                    <div class="errcls" id="countryperr" style="clear: both;"></div><br/> 
                </div>                                     
            </div> 
        </div>
    </div>
</div> <!--Residency Address End -->
