<div class="airfcfx-home-cnt-width container pos_rel">
<div class="text-center margin_top50 margin_bottom30">
            <h1 class="airfcfx-txt-capitalize text-uppercase">Recently Viewed</h1>
			<h4>Find the most recent listings you discovered</h4>
    	</div>	
        <div id="owl-demos" class="owl-carousel owl-theme">
            <div class="owl-wrapper-outer">
                <div class="owl-wrapper" style="left: 0px; display: block; transition: all 1000ms ease 0s; transform: translate3d(0px, 0px, 0px);"><div class="airfcfx-homerecent-pad-10 col-xs-12 col-sm-4"><a class="rm_text_deco" href="http://airfinchscript.com/user/listing/view/OTg3XzgzNzk=">
                        <div class="item">
                            <div class=" bg_img " style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/listings/1479133414_4_0.jpg&amp;w=465&amp;h=330);">
                                <div class="bg_img1" style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/users/4_adframe546.jpg&amp;w=56&amp;h=56);">
                                </div>
                                <div class="price"><h3> € 1210.13</h3>
                                </div></div>
                            <p class="margin_right60 margin_top10 margin_left10 fa-1x pointer">Test TEst </p>
                            <p class="small margin_left10 text_gray1"><b>  Event Space  </b></p>
                        </div></a>
                    </div><div class="airfcfx-homerecent-pad-10 col-xs-12 col-sm-4"><a class="rm_text_deco" href="http://airfinchscript.com/user/listing/view/OTg4XzQzNjk=">
                        <div class="item">
                            <div class=" bg_img " style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/listings/1479133401_64_0.jpg&amp;w=465&amp;h=330);">
                                <div class="bg_img1" style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/users/64_ortegacoup.png&amp;w=56&amp;h=56);">
                                </div>
                                <div class="price"><h3> € 67.15</h3>
                                </div></div>
                            <p class="margin_right60 margin_top10 margin_left10 fa-1x pointer">Test listing </p>
                            <p class="small margin_left10 text_gray1"><b>  Pop Shop  </b></p>
                        </div></a>
                    </div><div class="airfcfx-homerecent-pad-10 col-xs-12 col-sm-4"><a class="rm_text_deco" href="http://airfinchscript.com/user/listing/view/MTJfMjUxNg==">
                        <div class="item">
                            <div class=" bg_img " style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/listings/1458554465_1_0.jpg&amp;w=465&amp;h=330);">
                                <div class="bg_img1" style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/users/1_2016022610.JPG&amp;w=56&amp;h=56);">
                                </div>
                                <div class="price"><h3> € 11.1</h3>
                                </div></div>
                            <p class="margin_right60 margin_top10 margin_left10 fa-1x pointer">Amazing view studio Notre Dame </p>
                            <p class="small margin_left10 text_gray1"><b>  Pop Shop  </b></p>
                        </div></a>
                    </div></div>
            </div>
        </div>
    </div>