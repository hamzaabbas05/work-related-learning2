<div class="col-xs-12 col-sm-2 margin_top20">
    <ul class="profile_left list-unstyled">
        <p> JOB LISTINGS</p>
        <li {!! (Request::is('user/listing/rentals/active') ? 'class="active"' : '') !!} ><a href="{{URL::to('user/listing/rentals/active')}}" >Active</a></li>            
        <li {!! (Request::is('user/listing/rentals/pending') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/listing/rentals/pending')}}" >Pending</a></li>
        <p> STUDENT'S JOB APPLICATIONS</p>
                <li {!! (Request::is('user/reservations') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/reservations')}}" >Student's job applications</a></li>
        <li {!! (Request::is('user/reservations/unapprove') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/reservations/unapprove')}}" >UnApproved applications</a></li>
<!--        <li {!! (Request::is('user/listing-flash/rentals/active') ? 'class="active"' : '') !!} ><a href="{{URL::to('user/listing-flash/rentals/active')}}" >Active Job Listing</a></li>            
        <li {!! (Request::is('user/listing-flash/rentals/pending') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/listing-flash/rentals/pending')}}" >Pending Job Listing</a></li>-->
<br/>
<p> PAID SERVICES</p>
        <li {!! (Request::is('user/listing-paid/rentals/active') ? 'class="active"' : '') !!} ><a href="{{URL::to('user/listing-paid/rentals/active')}}" >Active School House service</a></li>            
        <li {!! (Request::is('user/listing-paid/rentals/pending') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/listing-paid/rentals/pending')}}" >Pending School House service</a></li>
        <p> STUDENT'S SERVICE REQUESTS
     </p>
        <li {!! (Request::is('user/schoolHouses') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/schoolHouses/')}}" >Your School Houses Reservations</a></li>
        <li {!! (Request::is('user/schoolHouses/unapprove') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/schoolHouses/unapprove')}}" >UnApproved Reservations</a></li>
        <!-- <li {!! (Request::is('user/reservations/cancelled') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/reservations/cancelled')}}" >Cancelled Reservations</a></li> -->
        <!--  <li {!! (Request::is('user/reservations/requirements') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/trust')}}" >UnApproved Reservations</a></li> -->
        <!-- <li {!! (Request::is('user/reviews') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/reviews')}}" >Reviews</a></li> -->
    </ul>
    <a href="{{URL::to('rentals/add')}}">
        <button class="airfcfx-red-btn airfcfx-panel btn btn_dashboard margin_top20">Add New Listings</button>
    </a>
</div> <!--col-sm-3 end -->