<div class="col-xs-12 col-sm-3 margin_top30 margin_bottom30 sdecls"
     style="width: 18%;">
    <ul class="profile_left list-unstyled" id="listpropul">

        <li   {!! (Request::is('rentals/basic/*') ? 'style="background: #ddd"' : '') !!}  id="showBasi"><a href="{{URL::to('rentals/basic/'.$propertyObj->id)}}">Basics</a></li>
        <li  {!! (Request::is('rentals/description/*') ? 'style="background: #ddd"' : '') !!} id="showDesc"><a href="{{URL::to('rentals/description/'.$propertyObj->id)}}">Description</a></li>
        <!-- <li  {!! (Request::is('rentals/amenities/*') ? 'style="background: #ddd"' : '') !!} id="showAmenities"><a href="{{URL::to('rentals/amenities/'.$propertyObj->id)}}">Amenities</a></li> -->
        <li  {!! (Request::is('rentals/photos/*') ? 'style="background: #ddd"' : '') !!} id="showPhoto"><a href="{{URL::to('rentals/photos/'.$propertyObj->id)}}">Photos</a></li>
        <li  {!! (Request::is('rentals/location/*') ? 'style="background: #ddd"' : '') !!} id="showLoc"><a href="{{URL::to('rentals/location/'.$propertyObj->id)}}">Location</a></li>
<!--         <li  {!! (Request::is('rentals/pricing/*') ? 'style="background: #ddd"' : '') !!} id="showPricing"><a href="{{URL::to('rentals/pricing/'.$propertyObj->id)}}">Pricing</a></li> -->
        <li {!! (Request::is('rentals/bookings/*') ? 'style="background: #ddd"' : '') !!}  id="showBooking"><a href="{{URL::to('rentals/booking/'.$propertyObj->id)}}">Workplace Rules</a></li>
        <li  {!! (Request::is('rentals/calendar/*') ? 'style="background: #ddd"' : '') !!} id="showCalendar"><a href="{{URL::to('rentals/calendar/'.$propertyObj->id)}}">Calendar</a></li>
    </ul>

</div>