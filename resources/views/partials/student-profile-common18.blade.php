<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe Yourself</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-about">

            <textarea id="profile-about" class="form-control" name="about_yourself" rows="5"  >{{$userObject->about_yourself}}</textarea>

            <p class="help-block help-block-error"></p>
        </div> <div class="errcls" id="profile-abouterr" style="clear: both;"></div><br/>                            
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Medical or learning Condition</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-work">
            <textarea id="learning_condition" class="form-control" rows="10"  name="medical_learning"  >{{$userObject->medical_learning}}</textarea>


            <p class="help-block help-block-error"></p>
        </div>      <div class="errcls" id="learning_conditionerr" style="clear: both;"></div><br/>                       
    </div>
</div> <!--col-xs-12 end -->
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Notes for Host family</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-work">

            <textarea id="family_notes18" class="form-control" rows="10" name="family_notes18">{{$userObject->host_family_notes}}</textarea>


            <p class="help-block help-block-error"></p>
        </div>      <div class="errcls" id="family_notes18err" style="clear: both;"></div><br/>                       
    </div>
</div> <!--col-xs-12 end -->



<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Notes for work tutor</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-work">

            <textarea id="tutor_notes18" class="form-control" rows="10" name="tutor_notes18">{{$userObject->work_tutor_notes}}</textarea>


            <p class="help-block help-block-error"></p>
        </div>        <div class="errcls" id="tutor_notes18err" style="clear: both;"></div><br/>                     
    </div>
</div> <!--col-xs-12 end -->




<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Academic Qualifications <span clolor="#ccc">eg. Middle school diploma/licenza media</span></label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="academic_qualifications" class="form-control" name="academic_qualifications" value="{{$userObject->academic_qualifications}}"  >

            <p class="help-block help-block-error"></p>
        </div>     <div class="errcls" id="academic_qualificationserr" style="clear: both;"></div><br/>                        </div>
</div> <!--col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Date of attainment</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="date_attain" class="form-control" name="date_of_attainment" value="{{$userObject->date_of_attainment}}"  >

            <p class="help-block help-block-error"></p>
        </div>      
        <div class="errcls" id="date_attainerr" style="clear: both;"></div><br/>                       
    </div>
</div> <!--col-xs-12 end -->
