<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Over 16 Student Name</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="sname16" class="form-control" name="sname16" value="{{$userObject->sname16}}">

            <p class="help-block help-block-error"></p>
        </div>    <div class="errcls" id="sname16err" style="clear: both;"></div><br/>                         
    </div>
</div> <!--col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Over 16 Student Surname</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="ssurname16" class="form-control" name="ssurname16" value="{{$userObject->ssurname16}}">

            <p class="help-block help-block-error"></p>
        </div>                    <div class="errcls" id="ssurname16err" style="clear: both;"></div><br/>         </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">I Am<i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <select class="form-control" style="width:auto;" id="profile-gender" name="gender16">
            <option value="" >Gender</option>
            <option value="Male" <?php if ($userObject->gender16 == 'Male') { ?> selected <?php } ?>>Male</option>
            <option value="Female" <?php if ($userObject->gender16 == 'Female') { ?> selected <?php } ?>>Female</option>
            <option value="Other" <?php if ($userObject->gender16 == 'Other') { ?> selected <?php } ?>>Other</option>
        </select>
        <p class="margin_top_5 text_gray1">This is only shared once a "Request" is confirmed.</p>
        <div class="errcls" id="generr" style="clear: both;"></div><br/>
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Birth Date <i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
    </div>
    <div class="airfcfx-profile-bd col-xs-12 col-sm-9">
        <?php
        $day = "";
        $month = "";
        $year = "";
        if (!is_null($userObject->dob16)) {
            $dob = $userObject->dob16;
            $dobArray = explode("/", $dob);
            $day = $dobArray[0];
            $month = $dobArray[1];
            $year = $dobArray[2];
        }
        ?>
        <select class="form-control col-sm-4" id="bmonth16" style="width:95px;" name="bmonth16">
            <option <?php if ($month == "") { ?> selected <?php } ?>value="">Month</option>
            <option value="1" <?php if ($month == 1) { ?> selected <?php } ?>>January</option>
            <option value="2" <?php if ($month == 2) { ?> selected <?php } ?>>February</option>
            <option value="3" <?php if ($month == 3) { ?> selected <?php } ?>>March</option>
            <option value="4" <?php if ($month == 4) { ?> selected <?php } ?>>April</option>
            <option value="5" <?php if ($month == 5) { ?> selected <?php } ?>>May</option>
            <option value="6" <?php if ($month == 6) { ?> selected <?php } ?>>June</option>
            <option value="7" <?php if ($month == 7) { ?> selected <?php } ?>>July</option>
            <option value="8" <?php if ($month == 8) { ?> selected <?php } ?>>August</option>
            <option value="9" <?php if ($month == 9) { ?> selected <?php } ?>>September</option>
            <option value="10" <?php if ($month == 10) { ?> selected <?php } ?>>October</option>
            <option value="11" <?php if ($month == 11) { ?> selected <?php } ?>>November</option>
            <option value="12" <?php if ($month == 12) { ?> selected <?php } ?>>December</option>

        </select><div class="errcls" id="bmonth16err" style="clear: both;"></div><br/> 
        <select id="bday16" class="form-control col-sm-4 margin_left10" style="width:80px;" name="bday16">
            <option value="" <?php if ($day == "") { ?> selected <?php } ?>>Day</option>
            <?php for ($i = 1; $i <= 31; $i++) { ?>
                <option <?php if ($day == $i) { ?> selected <?php } ?> value="{{$i}}">{{$i}}</option>
            <?php } ?>
        </select><div class="errcls" id="bday16err" style="clear: both;"></div><br/> 
        <select id="byear16" class="form-control col-sm-4 margin_left10" style="width:90px;" name="byear16">
            <option value="" <?php if ($year == "") { ?> selected <?php } ?>>Year</option>
            <?php
            $currentYear = date("Y");
            $startYear = $currentYear - 14;
            $baseYear = 1950;
            for ($i = $startYear; $i >= $baseYear; $i--) {
                ?>
                <option <?php if ($year == $i) { ?> selected <?php } ?> value="{{$i}}">{{$i}}</option>
            <?php } ?>
        </select><div class="errcls" id="byear16err" style="clear: both;"></div><br/> 
        <br/><br/>

    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Place of Birth</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-work">

            <input type="text" id="pob16" class="form-control" name="pob16" value="{{$userObject->pob16}}">

            <p class="help-block help-block-error"></p>
        </div>                <div class="errcls" id="pob16err" style="clear: both;"></div><br/>             
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Over 16 Student Email</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="semail16" class="form-control" name="semail16" value="{{$userObject->semail16}}">

            <p class="help-block help-block-error"></p>
        </div>           <div class="errcls" id="semail16err" style="clear: both;"></div><br/>                  </div>
</div> <!--col-xs-12 end -->
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Over 16 Student Phone</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="number" id="sphone16" class="form-control" name="sphone16" value="{{$userObject->sphone16}}">

            <p class="help-block help-block-error"></p>
        </div>          <div class="errcls" id="sphone16err" style="clear: both;"></div><br/>                   </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">NIN - National Insurance Number (SSN in the USA, Codice Fiscale in Italy)</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-work">

            <input type="text" id="nin16" class="form-control" rows="10" value="{{$userObject->NIN16}}" name="NIN16">

            <p class="help-block help-block-error"></p>
        </div>        <div class="errcls" id="nin16err" style="clear: both;"></div><br/>                     
    </div>
</div> <!--col-xs-12 end -->
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Notes for Host family</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-work">

            <textarea id="family_notes" class="form-control" rows="10" name="host_family_notes">{{$userObject->host_family_notes}}</textarea>


            <p class="help-block help-block-error"></p>
        </div>      <div class="errcls" id="family_noteserr" style="clear: both;"></div><br/>                       
    </div>
</div> <!--col-xs-12 end -->



<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Notes for work tutor</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-work">

            <textarea id="tutor_notes" class="form-control" rows="10" name="work_tutor_notes">{{$userObject->work_tutor_notes}}</textarea>


            <p class="help-block help-block-error"></p>
        </div>        <div class="errcls" id="tutor_noteserr" style="clear: both;"></div><br/>                     
    </div>
</div> <!--col-xs-12 end -->
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe About your Son/Daughter</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-about">

            <textarea id="profile-about16" class="form-control" name="profile-about16" rows="5"  >{{$userObject->about_yourself}}</textarea>

            <p class="help-block help-block-error"></p>
        </div>         <div class="errcls" id="profile-about16err" style="clear: both;"></div><br/>                    
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Medical or learning Condition</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-work">
            <textarea id="learning_condition16" class="form-control" rows="10"  name="medical_learning16"  >{{$userObject->medical_learning}}</textarea>


            <p class="help-block help-block-error"></p>
        </div>            <div class="errcls" id="learning_condition16err" style="clear: both;"></div><br/>                 
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Academic Qualifications <span clolor="#ccc">eg. Middle school diploma/licenza media</span></label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="academic_qualifications16" class="form-control" name="academic_qualifications16" value="{{$userObject->academic_qualifications}}"  >

            <p class="help-block help-block-error"></p>
        </div>             <div class="errcls" id="academic_qualifications16err" style="clear: both;"></div><br/>                </div>
</div> <!--col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Date of attainment</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="date_attain16" class="form-control" name="date_of_attainment16" value="{{$userObject->date_of_attainment}}"  >

            <p class="help-block help-block-error"></p>
        </div>             <div class="errcls" id="date_attain16err" style="clear: both;"></div><br/>                </div>
</div> <!--col-xs-12 end -->
