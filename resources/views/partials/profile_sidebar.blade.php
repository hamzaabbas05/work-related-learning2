@inject('country', 'App\Country')
<?php
$countries = $country->getall();

$roleId = 0;
$orgdiv = 'display:none';
$studiv = 'display:none';
$pardiv = 'display:none';
$orgbtnstyle = "background-color:#ccc !important";
$stubtnstyle = "background-color:#ccc !important";
$parbtnstyle = "background-color:#fe5771 !important";
$disableStyle = '';
//if (\Auth::user()->hasRole('Organization')) {
//    $orgbtnstyle = "background-color:#fe5771 !important";
//    $roleId = 4;
//    $orgdiv = 'display:block';
//    $disableStyle = 'disabled = "true"';
//} elseif (\Auth::user()->hasRole('Parent')) {
//    $parbtnstyle = "background-color:#fe5771 !important";
//    $roleId = 5;
//    $pardiv = 'display:block';
//    $disableStyle = 'disabled = "true"';
//} elseif (\Auth::user()->hasRole('Student')) {
//    $stubtnstyle = "background-color:#fe5771 !important";
//    $roleId = 6;
//    $studiv = 'display:block';
//    $disableStyle = 'disabled = "true"';
//}
?>
<style type="text/css">
    .left_buttons button {
        margin-top: 10px;
    }
    .left_buttons label {
        margin-top: 0px;
    }
</style>
<div class="col-xs-12 col-sm-3 margin_top20" style="margin-bottom: 20px;">
    <ul class="profile_left list-unstyled">

        <!-- <li {!! (Request::is('user/reviews') ? 'class="active"' : '') !!}><a  href="{{URL::to('user/reviews')}}" >Reviews</a></li> -->
    </ul>
    <!-- <a href="{{URL::to('user/view')}}"><button class="airfcfx-panel btn btn_google margin_top20">View Profile</button></a>
    -->

    <div class="form-group field-profile-firstname required left_buttons" style="margin-top: 20px;">
        <label class="profile_label col-lg-12">
            USER
        </label>
        <a href="{{URL::to('user/edit')}}">
            <button  style="@if(Request::is('user/edit')) {{$parbtnstyle}} @else {{$orgbtnstyle}} @endif" class="btn btn-success col-lg-12" type="button" >Basic Info</button>
        </a>
        <a href="{{URL::to('user/trust')}}">
            <button  style="@if(Request::is('user/trust')) {{$parbtnstyle}} @else {{$orgbtnstyle}} @endif" class="btn btn-success col-lg-12" type="button" >Trust and Verification</button>
        </a>
        <a href="{{URL::to('user/extraInfo')}}">
            <button  style="@if(Request::is('user/extraInfo')) {{$parbtnstyle}} @else {{$orgbtnstyle}} @endif" class="btn btn-success col-lg-12" type="button" >Extra Info</button>
        </a>
        <div class="col-lg-12">
            <hr style="height: 2px; width: 100%; background-color: #fe5771;"/>
        </div>
        <!-- in start and <button  style="{{$orgbtnstyle}}" class="btn btn-success col-lg-12" type="button" onclick="showContainer('', this)">Extra</button>
        at end -->
        <label class="profile_label col-lg-12">
            NOW TRAVELLING
        </label>
        <a href="{{URL::to('user/preEnrolment')}}">
            <button  style="@if(Request::is('user/preEnrolment')) {{$parbtnstyle}} @else {{$orgbtnstyle}} @endif" class="btn btn-success col-lg-12" type="button"> Pre-enrol - Parent of Under 18</button>
        </a>
        <a href="{{URL::to('user/preEnrolmentOver18')}}">
            <button  style="@if(Request::is('user/preEnrolmentOver18')) {{$parbtnstyle}} @else {{$orgbtnstyle}} @endif" class="btn btn-success col-lg-12" type="button"> Pre-enrol - Over 18 Student </button>
        </a>
        <a href="{{URL::to('user/parent')}}">
            <button  style="@if(Request::is('user/parent')) {{$parbtnstyle}} @else {{$orgbtnstyle}} @endif" class="btn btn-success col-lg-12" type="button">Complete - Parent of Under 18</button>
        </a>
        <a href="{{URL::to('user/studentGuest')}}">
            <button  style="@if(Request::is('user/studentGuest')) {{$parbtnstyle}} @else {{$orgbtnstyle}} @endif" class="btn btn-success col-lg-12" type="button">Complete - Student Guest (over 18)</button>
        </a>
        
        <div class="col-lg-12">
            <hr style="height: 2px; width: 100%; background-color: #fe5771;"/>
        </div>
        <label class="profile_label col-lg-12">
            NOW LISTING
        </label>
        <a href="{{URL::to('user/workExperience')}}">
            <button  style="@if(Request::is('user/workExperience')) {{$parbtnstyle}} @else {{$orgbtnstyle}} @endif" class="btn btn-success col-lg-12" type="button">Unpaid Work Experience</button>
        </a>
        <a href="{{URL::to('user/paidServices')}}">
            <button  style="@if(Request::is('user/paidServices')) {{$parbtnstyle}} @else {{$orgbtnstyle}} @endif" class="btn btn-success col-lg-12" type="button">Paid Services</button>
        </a>
        <!-- in start and at end  <button  style="{{$orgbtnstyle}}" class="btn btn-success col-lg-12" type="button" onclick="showContainer('', this)">High School</button>
         <button  style="{{$orgbtnstyle}}" class="btn btn-success col-lg-12" type="button" onclick="showContainer('', this)">Person Involved</button>
         <button  style="{{$orgbtnstyle}}" class="btn btn-success col-lg-12" type="button" onclick="showContainer('', this)">Work Tutor More</button>
         <button  style="{{$orgbtnstyle}}" class="btn btn-success col-lg-12" type="button" onclick="showContainer('', this)">School Tutor</button>
        -->
    </div>

</div> <!--col-sm-3 end -->
