<div class="airfcfx-panel panel panel-default">
    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">Unpaid Work Experience - Organisation details</h3>
    </div>
    <div class="airfcfx-panel-padding panel-body">
        <div class="row">
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Organisation Name</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-school">

                        <input type="text" id="orgname" class="form-control" name="orgname" placeholder="Registered name" value="{{$userObject->orgname}}">

                        <p class="help-block help-block-error"></p>
                    </div>  <div class="errcls" id="orgnameerr" style="clear: both;"></div><br/>                       

                </div>
            </div> <!--col-xs-12 end -->

            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">About Organization</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-school">
                        <input type="text" id="aboutorg" class="form-control" name="aboutorg" 
                                  placeholder="Briefly describe, max 5 lines without any personal or private info." value="{{$userObject->aboutorg}}">

<!--                        <textarea id="aboutorg" class="form-control" name="aboutorg" placeholder="Briefly describe, max 5 lines, without any personal or private info as this will be public info, for example: A local Primary school with 54 classrooms. We run various projects to enhance our students' ..."   >{{$userObject->aboutorg}}</textarea>-->

                        <p class="help-block help-block-error"></p>
                    </div>                         
                    <div class="errcls" id="aboutorgerr" style="clear: both;"></div><br/> 
                </div>
            </div> <!--col-xs-12 end -->

            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Organisation Tax / Registration Number</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-school">
                        <input type="text" id="organisation_tax_registration_number" class="form-control" name="organisation_tax_registration_number" value="{{$userObject->organisation_tax_registration_number}}">
                        <p class="help-block help-block-error"></p>
                    </div>  <div class="errcls" id="orgnameerr" style="clear: both;"></div><br/>                       

                </div>
            </div>


            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Legal Nature</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-school">




                        <select id="legal_nature" class="form-control listselect selectsze" name="legal_nature"  >


                            <option value=""> Select </option>
                            <option <?php if ($userObject->legal_nature == 1) { ?> selected <?php } ?> value="1">Public</option>
                            <option <?php if ($userObject->legal_nature === 0) { ?> selected <?php } ?> value="0">Private</option>
                        </select>

                        <p class="help-block help-block-error"></p>
                    </div>         
                    <div class="errcls" id="legal_natureerr" style="clear: both;"></div><br/> 


                </div>
            </div> <!--col-xs-12 end -->

            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Workforce</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-school">
                        <select id="orghr" class="form-control listselect selectsze" name="orghr">
                            <option value="">Select</option>
                            <?php foreach ($roomTypes as $obj) { ?>
                                <option value="{{$obj->id}}" <?php if ($obj->id == $userObject->orghr) { ?> selected <?php } ?> >{{$obj->name}}</option>
                            <?php } ?>
                        </select>

                        <p class="help-block help-block-error"></p>
                    </div>             

                    <div class="errcls" id="orghrerr" style="clear: both;"></div><br/> 



                </div>
            </div> <!--col-xs-12 end -->
            <input type="hidden" id="orglatbox" name="orglatbox" value="{{$userObject->org_lat}}">
            <input type="hidden" id="orglonbox" name="orglonbox" value="{{$userObject->org_lan}}">
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Organisation Email</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-school">

                        <input type="email" id="orgemail" class="form-control" name="orgemail" value="{{$userObject->orgemail}}" >
                        <p class="help-block help-block-error"></p>
                    </div>                          
                    <div class="errcls" id="orgemailerr" style="clear: both;"></div><br/>


                </div>
            </div> <!--col-xs-12 end -->
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Organisation Phone</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-school">
                        <input type="number"  id="orgphone" class="form-control" name="orgphone" value="{{$userObject->orgphone}}">
                        <p class="help-block help-block-error"></p>
                    </div>                           
                    <div class="errcls" id="orgphoneerr" style="clear: both;"></div><br/> 
                </div>
            </div> <!--col-xs-12 end -->
            <hr style="border: 1px solid #ddd;"/>
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-2 text-right">
                    <input type="checkbox" value="1" 
                           <?php if (isset($user_legal->is_legal_representative) && $user_legal->is_legal_representative == 1) { ?> checked <?php } ?> 
                           name="is_legal_representative" onchange="showLegalRepresentative()" /> 
                </div>
                <div class="col-xs-12 col-sm-10">
                    <div class="form-group field-profile-firstname required" style="margin-left: 0px;">
                        IF Legal Representative NOT YOU
                        <p class="help-block help-block-error"></p>
                    </div>                        
                </div>
            </div>
            <?php
            $legal_display = 'display: none;';
            if (isset($user_legal->is_legal_representative) && $user_legal->is_legal_representative == 1) {
                $legal_display = 'display: block;';
            }
            ?>
            <div id="legal_representative" style="{{$legal_display}}">
                <div class="col-xs-12 margin_top10">
                    <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Representative Name</label> 
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="form-group field-profile-school">
                            <input type="text" id="legal_rep_name" class="form-control" name="legal_representative_name" value="{{isset($user_legal->legal_representative_name)?$user_legal->legal_representative_name:''}}" />
                            <p class="help-block help-block-error"></p>
                        </div>                          
                        <div class="errcls" id="orgemailerr" style="clear: both;"></div><br/>
                    </div>
                </div>
                <div class="col-xs-12 margin_top10">
                    <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Representative Surname</label> 
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="form-group field-profile-school">
                            <input type="text" id="legal_rep_surname" class="form-control" name="legal_representative_surname" value="{{isset($user_legal->legal_representative_surname)?$user_legal->legal_representative_surname:''}}" />
                            <p class="help-block help-block-error"></p>
                        </div>                          
                        <div class="errcls" id="orgemailerr" style="clear: both;"></div><br/>
                    </div>
                </div>
                <div class="col-xs-12 margin_top10">
                    <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Representative DOB</label> 
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="form-group field-profile-school">
                            <input type="text" id="legal_rep_dob" class="form-control normal_datepicker" placeholder="D-M-Y" name="legal_representative_dob" value="{{isset($user_legal->legal_representative_dob)?$user_legal->legal_representative_dob:''}}" />
                            <p class="help-block help-block-error"></p>
                        </div>                          
                        <div class="errcls" id="orgemailerr" style="clear: both;"></div><br/>
                    </div>
                </div>
                <div class="col-xs-12 margin_top10">
                    <div class="col-xs-12 col-sm-3 text-right">
                        <label class="profile_label">Representative Birth Place</label> 
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="form-group field-profile-school">
                            <input type="text" id="legal_rep_place" class="form-control" name="legal_representative_birth_place" value="{{isset($user_legal->legal_representative_birth_place)?$user_legal->legal_representative_birth_place:''}}" />
                            <p class="help-block help-block-error"></p>
                        </div>
                        <div class="errcls" id="orgemailerr" style="clear: both;"></div><br/>
                    </div>
                </div>
            </div>
            <hr style="border: 1px solid #ddd;"/>





            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="padding0 profile_label">Legal Address 
                    </label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    @include('partials.address.org_residence')
                </div>
            </div> <!-- Address End -->
            <h3>As Work Tutor</h3>
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Relationship in the Company</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-school">

                        <select id="tutorrelation" name="tutorrelation" class="form-control" >
                            <option value="undefined">Select</option>
                            <?php foreach ($tutorRelation as $rs) { ?>
                                <option value="{{$rs->id}}" <?php if ($rs->id == $userObject->tutorrelation) { ?> selected <?php } ?>>{{$rs->name}}</option>
                            <?php } ?>
                        </select>

                        <p class="help-block help-block-error"></p>
                    </div>                           
                    <div class="errcls" id="tutorrelationerr" style="clear: both;"></div><br/> 


                </div>
            </div>

            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Why are you fit for being a Work Tutor?</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-school">

                        <textarea  id="why_fit" class="form-control" name="why_fit" > {{$userObject->why_fit}}</textarea>

                        <p class="help-block help-block-error"></p>
                    </div>                           
                    <div class="errcls" id="why_fiterr" style="clear: both;"></div><br/> 

                </div>
            </div>
        </div>
    </div>
</div>