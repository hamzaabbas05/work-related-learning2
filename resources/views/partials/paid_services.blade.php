<div class="airfcfx-panel panel panel-default">
    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">School House details</h3>
    </div>
    <div class="airfcfx-panel-padding panel-body">
        <div class="row">@include('partials.schoolhouse_menu')</div>
        <div class="row school_houses_tabs" id="basics">
            @include('partials.schoolhouse.basics')
        </div> <!-- Info row end -->
        <div class="row school_houses_tabs" id="extra" style="display:none">
            @include('partials.schoolhouse.extras')
        </div> <!-- Info row end -->
        <div class="row school_houses_tabs" id="property" style="display:none">
            @include('partials.schoolhouse.property')
        </div> <!-- Info row end -->

        <div class="row school_houses_tabs" id="propertyphoto" style="display:none">
            @include('partials.schoolhouse.propertyimage')
        </div> <!-- Info row end -->

        <div class="row school_houses_tabs" id="familyphoto" style="display:none">
            @include('partials.schoolhouse.familyphoto')
        </div> <!-- Info row end -->
        
        <div class="row school_houses_tabs" id="includedServices" style="display:none">
            @include('partials.schoolhouse.included_services')
        </div> <!-- Info row end -->
        
        <input type="hidden" name="paid_service_tab" id="paid_service_tab" value="basics" />
    </div>
</div>