                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           @inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<style type="text/css">
    .showTypeFields {
        /*margin-left: 0px !important;*/
    }
    .showTypeFields .form-group{
        margin-left: 0px !important;
    }
</style>
{!! Form::open(array('url' => 'rentals/saveSchoolHouse', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
<div class="col-xs-12 col-sm-12 col-md-9 margin_top20 margin_bottom20" style="display: block;">
    <div class="listing-create">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="col-xs-12 airfcfx-createlist">
                <label>
                    What type of Listing?
                    <br />
                    <span style="font-size: 12px;"><b>Note:</b> Type of listing will not be editable once you hit Continue.</span>
                    <br />
                    <span style="font-size: 12px;"><b>To add a Bed:</b> If you would like to add a Guest Bed click <a href="{{URL::to('rentals/add/school-house-bed')}}">here</a>.</span>
                    
                </label><br/>
                <?php
                $i = 1;
                $propertyTypeAdded = array();
                foreach ($propertyTypes as $obj) {
                    //if ($i <= 3) {
                    array_push($propertyTypeAdded, $obj->id);
                    ?> 
                    <button type="button" class="btn btn_list home" value="2" data-toggle="tooltip" data-placement="bottom" title="" onclick="showTypeFields('{{strtolower($obj->name)}}')">
                        <i class="fa fa-building-o list_icon"></i> {{$obj->name}}
                        <input type="hidden" value="{{$obj->id}}" id="<?php echo "home" . $obj->id ?>">
                    </button>
                    <?php
                    //} 
                    $i++;
                }
                ?>
                <div id="homeerr" class="errcls"></div>
                <input type="hidden" value="" name="wcategory" id="wcategory" />
            </div> <!-- col-sm-12 end -->

            <div class="col-xs-12 form-group margin_top20 showTypeFields" id="bed_type" style="display: none;">
                <div class="col-xs-12 form-group margin_top20">
                    <label>Guest Room</label>
                    <div class="form-group">
                        <select name="bed_guest_room_id" class="form-control">
                            <option value="">Select Guest Room</option>
                            @if(isset($userObject->user_guest_rooms) && sizeof($userObject->user_guest_rooms) > 0)
                            @foreach($userObject->user_guest_rooms as $gr_key => $guest_room)
                            <option value="{{$guest_room->id}}">{{$guest_room->room_name}} {{isset($guest_room->realRoomType->room_type)?"(".$guest_room->realRoomType->room_type.")":''}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 form-group margin_top20">
                    <label>Bed Type</label>
                    <div class="form-group">
                        <select name="bed_type_id" class="form-control">
                            <option value="">Select Bed Type</option>
                            @if(isset($bed_types) && sizeof($bed_types) > 0)
                            @foreach($bed_types as $bed_type)
                            <option value="{{$bed_type->id}}">{{$bed_type->bed_type}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 form-group margin_top20 showTypeFields" id="class_type" style="display: none;">
                <div class="col-xs-12 form-group margin_top20">
                    <label>Class Type</label>
                    <div class="form-group">
                        <select name="class_type_id" class="form-control">
                            <option value="">Select Class Type</option>
                            @if(isset($class_types) && sizeof($class_types) > 0)
                            @foreach($class_types as $class_type)
                            <option value="{{$class_type->id}}">{{$class_type->class_type}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 form-group margin_top20">
                    <label>Class Duration Frequency</label>
                    <div class="form-group">
                        <textarea rows="10" class="form-control"  id="class_duration_frequency" name="class_duration_frequency" placeholder="Eg1 50 minute lessons 5 times a week; 20 lessons, four 45 minute daily lessons, totalling 15 hours a week."></textarea>
                    </div>
                </div> 
            </div>
            <div class="col-xs-12 form-group margin_top20 showTypeFields" id="activity_type" style="display: none;">
                <div class="col-xs-12 form-group margin_top20">
                    <label>Activity Type</label>
                    <div class="form-group">
                        <select name="activity_type_id" class="form-control">
                            <option value="">Select Activity Type</option>
                            @if(isset($activity_types) && sizeof($activity_types) > 0)
                            @foreach($activity_types as $activity_type)
                            <option value="{{$activity_type->id}}">{{$activity_type->activity_type}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 form-group margin_top20 showTypeFields" id="transfer_type" style="display: none;">
                <div class="col-xs-12 form-group margin_top20">
                    <label>Transfer Type</label>
                    <div class="form-group">
                        <select name="transfer_type_id" class="form-control">
                            <option value="">Select Transfer Type</option>
                            @if(isset($transfer_types) && sizeof($transfer_types) > 0)
                            @foreach($transfer_types as $transfer_type)
                            <option value="{{$transfer_type->id}}">{{$transfer_type->transfer_type}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 form-group margin_top20 showTypeFields" id="tour_type" style="display: none;">
                <div class="col-xs-12 form-group margin_top20">
                    <label>Tour Type</label>
                    <div class="form-group">
                        <select name="tour_type_id" class="form-control">
                            <option value="">Select Tour Type</option>
                            @if(isset($tour_types) && sizeof($tour_types) > 0)
                            @foreach($tour_types as $tour_type)
                            <option value="{{$tour_type->id}}">{{$tour_type->tour_type}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 form-group margin_top20 showTypeFields" id="wow_type" style="display: none;">
                <div class="col-xs-12 form-group margin_top20">
                    <label>Wow Type</label>
                    <div class="form-group">
                        <select name="wow_type_id" class="form-control">
                            <option value="">Select Wow Type</option>
                            @if(isset($wow_types) && sizeof($wow_types) > 0)
                            @foreach($wow_types as $wow_type)
                            <option value="{{$wow_type->id}}">{{$wow_type->wow_type}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 form-group margin_top20">
                <label>Title of the Listing.</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class=""></i></span>
                    <input type="text" class="form-control"  id="workexp" name="workexp"/>

                </div>
                <div id="workexperr" class="errcls"></div>
            </div> 

            <div class="col-xs-12 form-group margin_top20">
                <label>Describe the service.</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class=""></i></span>
                    <textarea rows="10" class="form-control"  id="description" name="description"></textarea>

                </div>
                <div id="descriptionerr" class="errcls"></div>
            </div>

            <div class="xs-padding-0 col-xs-12 margin_top20">
                <input type="hidden" class="form-control"  id="property_status" name="property_status" value="paid"/>
                <a href="{{URL::to('rentals/add')}}">
                    <button class="airfcfx-panel btn btn_email padding10" type="button">Back</button>
                </a>
                <button class="airfcfx-panel btn btn_email padding10" type="submit" onclick="return savelistPaid()">Continue</button>
                <div id="emailverifyerr" class="errcls"></div>
            </div>


        </div> <!-- col-sm-9 end -->

    </div>
</div>
{!! Form::close() !!}
</div> <!-- row end -->
<script type="text/javascript">
    function showTypeFields(type_name) {
        $(".showTypeFields").hide();
        if (type_name == "bed") {
            $("#bed_type").show();
        } else if(type_name == "a class") {
            $("#class_type").show();
        } else if(type_name == "activity") {
            $("#activity_type").show();
        } else if(type_name == "transfer") {
            $("#transfer_type").show();
        } else if(type_name == "tour") {
            $("#tour_type").show();
        } else if(type_name == "wow") {
            $("#wow_type").show();
        }
    }
</script>