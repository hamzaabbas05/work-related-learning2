
<div class="panel panel-default panelcls">
    <div class="panel-body">
        <div class="row">
<!-- Comment THIS DONE  -->
            <div class="col-xs-12 margin_top10 margin_bottom20  commcls" id="calendardiv">
                <input type="hidden" value="{{$propertyObj->id}}" id="listingid" name="listingid">
                <input type="hidden" id="bookingavailability" name="bookingavailability" value="{{$propertyObj->calendar_availability}}">
                <h3> We can't offer the Service in this period</h3>
                Your listing will not appear in the periods you specify herein. EG: If on Holiday or simply unavailable.								
                <br /><br />
                <!-- <div id="bookavailability">
                        <h4>When are you available?</h4>
                        
                        <div class="alwaysdiv">
                                <center>
                                        <div class="alwaysinnerdiv" onclick="update_booktype('1',this);" <?php if ($propertyObj->calendar_availability == 1) { ?> style = "background: rgb(255, 255, 255);" <?php } ?>>
                                                        <div class="always"></div><br />
                                                        <div>Always</div>
                                                        <div>Any day is bookable.</div>
                                                </div>										</center>
                        </div>
                        <div class="alwaysdiv">
                                <center>
                                        <div class="alwaysinnerdiv" <?php if ($propertyObj->calendar_availability == 2) { ?> style = "background: rgb(255, 255, 255);" <?php } ?>onclick="update_booktype(2 ,this);">
                                                        <div class="always"></div><br />
                                                        <div>One time</div>
                                                        <div>Host for just one period of time.</div>
                                                </div>										</center>
                        </div>	
                </div> -->
                <div id="bookdate" class="">
                    <div class="datediv">
                        <div class="alwaysinnerdiv dateleftdiv" onclick="update_booktype(1, this);">
                            <div class="always"></div><br />
                            <div>Calendar</div>
                            <div>We can't offer this Service in this period.</div>
                        </div>
                        <div class="daterightdiv">
                            <div class="">
                                When can't students book?<br /><br />
                                <div class="alwaysdiv">
                                    <p>Start Date</p>
                                    <div class="form-group field-listing-startdate">

                                        <input type="text" id="startdate" class="form-control cal margin_bottom10 selectsze" name="startdate" value="" placeholder="Start Date">
                                        <div class="sdate" style="clear: both;"></div><br/>
                                        <p class="help-block help-block-error"></p>
                                    </div>												</div>
                                <div class="alwaysdiv">
                                    <p>End Date</p>
                                    <div class="form-group field-listing-enddate">

                                        <input type="text" id="enddate" class="form-control cal margin_bottom10 selectsze" name="enddate" value="" placeholder="End Date">
                                        <div class="edate" style="clear: both;"></div><br/>
                                        <p class="help-block help-block-error"></p>
                                    </div>												</div>
                            </div>
                        </div>										
                    </div>


                </div><input type="button" value="Save" class="btn btn_email nextbtn" onclick="savecalendar();">
                <div id="calendarsettings" class="clear">
                    <br /><hr />

                </div>
                <br />

                <div class="col-sm-6 coldiv">
<!-- Comment  DONE TILL HERE -->

                    <a href="{{URL::to('user/listing/rentals/active')}}" onclick="show_backbooking();" class="btn btn_email backbtn">Finished - go back to my listings</a>
                    <p> </p>
                </div>
                <div class="col-sm-6 coldiv"><?php if ($propertyObj->status == 0) { ?>   <p> Listing will become active after Admin approval</p><?php } ?>
                </div>


            </div>
        </div>
    </div>
    <!--row end -->
</div>