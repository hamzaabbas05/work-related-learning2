@inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<!-- DONE-->
<div class="panel panel-default panelcls">
    <div class="panel-body">
        <div class="row">

            <div class="col-xs-12 margin_top10 margin_bottom20 commcls" id="basicsdiv">
                <h3>LIST YOUR "SERVICE" </h3>
                Here you may specify details of listing and people involved.<hr />
                <br />

                <div class="col-sm-6 coldiv">
                    <h3>SERVICE</h3>
                    {!! Form::open(array('url' => 'rentals/basic_paid_submit', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'id' => 'basicsubmit')) !!}
                    <input type="hidden" value="{{$propertyObj->id}}" name="property_id">
                    <label class="airfcfx-manage-label">Title of the Listing<p class="charleft" style="right: 0;"></p></label>
                    <div class="form-group field-listing-listingname">

                        <input type="text" id="exptitle" class="form-control margin_bottom10" name="exptitle" value="{{$propertyObj->exptitle}}" required>
                        <div class="exptitle errcls" style="clear: both;"></div><br/>
                        <p class="help-block help-block-error"></p>
                    </div>		

                    <label class="airfcfx-manage-label">Type of Listing<p class="charleft" id="charaNum" style="right: 0;"> </p></label>
                    <div class="form-group field-listing-listingname">
                        <?php if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                            // echo "<pre>";
                            //     print_r($propertyTypes);
                            // echo "</pre>";
                        } 
                        ?>
                        <select class="form-control" id="workcategory" name="workcategory" disabled="">
                            <option value="undefined">Select</option>
                            <option value="0" <?php echo ($propertyObj->property_type == 0 || empty($propertyObj->property_type) ? "selected" : "") ?>>First Listing</option>
                            <?php foreach ($propertyTypes as $obj) { ?>
                                <option value="{{$obj->id}}" <?php if ($obj->id == $propertyObj->property_type) { ?> selected <?php } ?> >{{$obj->name}}</option>
                            <?php } ?>
                        </select>
                        <div class="workcategory errcls" style="clear: both;"></div><br/>
                        <p class="help-block help-block-error"></p>
                    </div>	
                    <label class="airfcfx-manage-label">
                        <h3>Person delivering this service (IF NOT YOU)</h3>
                        <p class="charleft"  style="right: 0;">IF NOT YOU as the user listing this service, if you do not specify your details initially provided will be sent to the Student Guest. For example: who provides the lesson, guiding the tour, another host's bedroom and so on</p>
                    </label>

                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-2 text-right">
                            <input type="checkbox"  
                                   <?php if ($propertyObj->work_org_same == 0) { ?> checked <?php } ?> 
                                   id="checktutorid" name="checktutorid" onchange="checktutor(this)">
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <div class="form-group field-profile-firstname required">

                                IF Person delivering the service is NOT YOU

                                <p class="help-block help-block-error"></p>
                            </div>                        
                        </div>
                    </div>


                    <?php
                    $style = 'display:none';
                    $req = "";
                    if ($propertyObj->work_org_same == 0) {
                        $style = 'display:block';
                        $req = "required";
                    }
                    ?>

                    <div id="worktutor" style="{{$style}}">



                        <label class="airfcfx-manage-label">Relationship with the School House<p class="charleft"  style="right: 0;"></p></label>
                        <div class="form-group field-listing-listingname">

                            <select id="tutorrelation" name="tutorrelation" class="form-control" {{$req}}>
                                <option value="undefined">Select</option>
                                <?php foreach ($tutorRelation as $rs) { ?>
                                    <option value="{{$rs->id}}" <?php if ($rs->id == $propertyObj->relation_with_company) { ?> selected <?php } ?>>{{$rs->name}}</option>
                                <?php } ?>
                            </select>
                            <div class="tutorrelationerr errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>
                        <label class="airfcfx-manage-label">Brief description of this person<p class="charleft"  style="right: 0;"></p></label>
                        <div class="form-group field-listing-listingname">

                            <textarea id="work_tutor_fit" class="form-control margin_bottom10" name="work_tutor_fit" {{$req}}>{{$propertyObj->work_tutor_fit}}</textarea>
                            <div class="work_tutor_fiterr errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>


                        <label class="airfcfx-manage-label">Name <p class="charleft"  style="right: 0;"></p></label>
                        <div class="form-group field-listing-listingname">

                            <input type="text" id="rname" class="form-control margin_bottom10" name="rname1" value="{{$propertyObj->represent_name}}" {{$req}}>
                            <div class="rnameerr errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>

                        <label class="airfcfx-manage-label">Surname <p class="charleft"  style="right: 0;"></p></label>
                        <div class="form-group field-listing-listingname">

                            <input type="text" id="rsurname1" class="form-control margin_bottom10" name="rsurname1" value="{{$propertyObj->represent_surname}}" maxlength="35" {{$req}}>
                            <div class="rsurname1err errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>

                        <label class="airfcfx-manage-label">Tax Identification Number<p class="charleft"  style="right: 0;"></p></label>
                        <div class="form-group field-listing-listingname">

                            <input type="text" id="rtax1" class="form-control margin_bottom10" name="rtax1" value="{{$propertyObj->represent_tax_no}}" {{$req}}>
                            <div class="rtax1 errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>


                        <label class="airfcfx-manage-label">Email<p class="charleft"  style="right: 0;"></p></label>
                        <div class="form-group field-listing-listingname">

                            <input type="text" id="remail1" class="form-control margin_bottom10" name="remail1" value="{{$propertyObj->represent_email}}" {{$req}}>
                            <div class="remail1 errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>


                        <label class="airfcfx-manage-label">Tel<p class="charleft"  style="right: 0;"></p></label>
                        <div class="form-group field-listing-listingname">

                            <input type="text" onkeypress="return isNumber(event)" id="rtel1" class="form-control margin_bottom10" name="rtel1" value="{{$propertyObj->represent_phone}}" {{$req}}>
                            <div class="rtel1 errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>



                        <label class="airfcfx-manage-label">Place of Birth<p class="charleft"  style="right: 0;"></p></label>
                        <div class="form-group field-listing-listingname">
                            <input type="text"  id="rborn1" class="form-control margin_bottom10" name="rborn1" value="{{$propertyObj->represent_born}}" {{$req}}>
                            <div class="rborn1 errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>



                    </div>

                    <!-- Human resources starts -->
                    <label class="airfcfx-manage-label">
                        <h3>Person Student should refer to</h3>
                        <p class="charleft"  style="right: 0;"> Person representing the School House for the delivery of this service</p>
                    </label>

                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-2 text-right">
                            <input type="checkbox"  
                                   <?php if (isset($propertyObj->human_resources) && $propertyObj->human_resources == 0) { ?> checked <?php } ?> 
                                   id="checkhumand" name="checkhumanid" onchange="checkhuman(this)">
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <div class="form-group field-profile-firstname required">

                                IF Person Students should refer to is NOT YOU

                                <p class="help-block help-block-error"></p>
                            </div>                        
                        </div>
                    </div>

                    <?php
                    $style = 'display:none';
                    $req = "";
                    if (isset($propertyObj->human_resources) && $propertyObj->human_resources == 0) {
                        $style = 'display:block';
                        $req = "required";
                    }
                    ?>

                    <div id="human_resources" style="{{$style}}">
                        <label class="airfcfx-manage-label">
                            Name 
                            <p class="charleft"  style="right: 0;"></p>
                        </label>
                        <div class="form-group field-listing-listingname">
                            <input type="text" id="hrname" class="form-control margin_bottom10" name="hrname1" value="{{isset($propertyObj->hr_represent_name)?$propertyObj->hr_represent_name:''}}" {{$req}}>
                            <div class="rnameerr errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>

                        <label class="airfcfx-manage-label">
                            Surname 
                            <p class="charleft"  style="right: 0;"></p>
                        </label>
                        <div class="form-group field-listing-listingname">
                            <input type="text" id="hrsurname1" class="form-control margin_bottom10" name="hrsurname1" value="{{isset($propertyObj->hr_represent_surname)?$propertyObj->hr_represent_surname:''}}" maxlength="35" {{$req}}>
                            <div class="rsurname1err errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>

                        <label class="airfcfx-manage-label">
                            Email
                            <p class="charleft"  style="right: 0;"></p>
                        </label>
                        <div class="form-group field-listing-listingname">
                            <input type="email" id="hremail1" class="form-control margin_bottom10" name="hremail1" value="{{isset($propertyObj->hr_represent_email)?$propertyObj->hr_represent_email:''}}" {{$req}}>
                            <div class="remail1 errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>


                        <label class="airfcfx-manage-label">
                            Tel
                            <p class="charleft"  style="right: 0;"></p>
                        </label>
                        <div class="form-group field-listing-listingname">
                            <input type="text" onkeypress="return isNumber(event)" id="hrtel1" class="form-control margin_bottom10" name="hrtel1" value="{{isset($propertyObj->hr_represent_phone)?$propertyObj->hr_represent_phone:''}}" {{$req}}>
                            <div class="rtel1 errcls" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>

                    </div>


                </div>
                <div class="col-sm-6 coldiv">
                <!--  <input type="submit" value="Save" class="btn btn_email nextbtn"  > -->
                    <input type="submit" value="Save" class="btn btn_email nextbtn"  >
                    </form>							</div>
            </div>

        </div>
    </div>
    <!--row end -->
</div>

