<div class="panel panel-default panelcls">
    <div class="panel-body">
        <div class="row">
<!-- DONE -->
            <div class="col-xs-12 margin_top10 margin_bottom20  commcls" id="descriptiondiv">
                <h3>LIST YOUR SERVICE</h3>
                Here you may specify details of listing and people involved.							<hr />
                <br />
                {!! Form::open(array('url' => 'rentals/description_paid_submit', 'class' => 'form-wizard form-horizontal', 'method' => 'POST')) !!}
                <input type="hidden" value="{{$propertyObj->id}}" name="property_id">
                <div class="col-sm-6 coldiv">
                    <label class="airfcfx-manage-label">Describe Service (five lines): </label>
                    <div class="form-group field-listing-description">

                        <textarea id="description" class="form-control margin_bottom10" name="description"   rows="6" placeholder="Eg: You will be welcomed upon arrival at designed meeting Point, Sunday together with me and Extra 30 minutes a day of dedicated time to speak about work or whatever you like. OR Lessons will be held every morning from 9.30 to 12.30 Mon to Fri following a personalised program OR Tour in Cambridge will include a visit to here and there." required>{{$propertyObj->description}}</textarea>

                        <p class="help-block help-block-error"></p>
                    </div>	

                    <label class="airfcfx-manage-label">Further optional description (five lines): </label>
                    <div class="form-group field-listing-description">

                        <textarea id="optional_description" class="form-control margin_bottom10" name="optional_description"   rows="6"  
                                  placeholder="Eg: Do let us know during our first phone call few days before arrival of any dietary preferences and we will do our best to make you feel at home. OR Lessons will be focused maily on speaking exercises typical of a Cambridge exam preparation course.">{{$propertyObj->optional_description}}</textarea>

                        <p class="help-block help-block-error"></p>
                    </div>	

<!--                    <label class="airfcfx-manage-label">Duration in hours of the Service, weekly:</label>
                    <div class="form-group field-listing-listingname">

                        <input type="number" id="work_hours" class="form-control margin_bottom10" name="work_hours" value="{{$propertyObj->work_hours}}" placeholder="Eg: 15 (20 lessons of 45 minutes each for a total of 15 hours OR This field may be left empty">

                        <p class="help-block help-block-error"></p>
                    </div>	


                    <label class="airfcfx-manage-label">Hourly articulation (daily and weekly). Enter daily and weekly scheduled</label>
                    <div class="form-group field-listing-listingname">

                        <input type="text" id="work_schedule" class="form-control margin_bottom10" name="work_schedule" value="{{$propertyObj->work_schedule}}" placeholder="Eg: From Dinner time on the first day to Breakfast time the day you leave OR 20 lessons of 45 minutes each for a total of 15 hours" required>

                    </div>	-->

                    <input type="submit" value="save" class="btn btn_email nextbtn">

                </div>
                </form>
            </div>

        </div>
    </div>
    <!--row end -->
</div>