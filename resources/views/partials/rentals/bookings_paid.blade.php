<div class="panel panel-default panelcls">
    <div class="panel-body">
        <div class="row">
<!-- Comment THIS DONE  -->
            <div class="col-xs-12 margin_top10 margin_bottom20  commcls" id="bookingdiv">
                <input type="hidden" id="bookingstyle" name="bookingstyle" value="{{$propertyObj->booking_style}}">
                <div id="bookingtype">
                    <h3>Rules</h3>
                    Specify any rules Student Guests have to comply with.								<hr />
                    <br />
                    <input type="hidden" value="{{$propertyObj->id}}" id="listingid" name="listingid">
                    <div class="col-sm-6 coldiv">
                        <div class="gridcls bookinggrid">
                            <div class="gridinner">
                                <div class="gridcls">Review each request											</div>
                                <div class="requestcls">
                                </div>
                                <div class="clear"></div>
                                <hr />
                                <div class="clear gridinnercls">
                                    <li class="licls">Students apply for the Service described.</li>
                                    <li class="licls">Approve or decline as soon as possible.</li>
                                </div>
                                <!--<input id="requestbook" type="button" class="btn btn-default width100" value="Selected" <?php if ($propertyObj->booking_style == 1) { ?>style="background-color:#4d4d4d !important;"<?php } ?>   >										</div>-->

                        </div>


                    </div>
                    <br>

                    <!-- code will come -->

                    <div class="col-sm-12 coldiv" style="padding-top: 20px;">
                        <h3>Rules</h3>
                        <div class="form-group field-listing-houserules">
                            <label class="control-label" for="houserules">Describe Rules (Max 250 characters)</label>
                            <textarea id="houserules" class="form-control margin_bottom10" name="houserules" maxlength="250" rows="6" placeholder="Eg. Be tidy, timely, dress appropriately and do your best. Doing your best will be beneficial to you and everybody else.">{{$propertyObj->houserules}}</textarea>
                            <div class="securityerrcls1" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>

                        <data></data>


                    </div>
                    <div class="col-sm-12 coldiv">
                        <h3>Link to .pdf with list of everything else a Student Guest will be happy to know of</h3>
                        <div class="form-group field-listing-houserules">
                            <label class="control-label" for="houserules">Add link to .pdf</label>
                            <input type="text" id="link_to_extra_docs_necessary" class="form-control" name="link_to_extra_docs_necessary" placeholder="Eg: detailed description of your School House OR An example of lesson plan" value="{{$propertyObj->link_to_extra_docs_necessary}}">
                            <div class="securityerrcls1" style="clear: both;"></div><br/>
                            <p class="help-block help-block-error"></p>
                        </div>

                        <data></data>


                    </div>

                    <input type="button" value="Save" class="btn btn_email nextbtn" onclick="save_booking_paid();">

                </div>



            </div>


        </div>



    </div>
</div>
<!--row end -->
</div>