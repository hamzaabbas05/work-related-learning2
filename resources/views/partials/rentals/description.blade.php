	<div class="panel panel-default panelcls">
				<div class="panel-body">
					<div class="row">

						 <div class="col-xs-12 margin_top10 margin_bottom20  commcls" id="descriptiondiv">
							<h3>LIST YOUR "WORK EXPERIENCE" OPPORTUNITY</h3>
							Work-related-learning.org allows Organisations to list Unpaid Job Offers for foreign over 16 high school students.							<hr />
							<br />
							 {!! Form::open(array('url' => 'rentals/description_submit', 'class' => 'form-wizard form-horizontal', 'method' => 'POST')) !!}
								<input type="hidden" value="{{$propertyObj->id}}" name="property_id">
							<div class="col-sm-6 coldiv">
 	 						<label class="airfcfx-manage-label">Describe duties and what you expect student to do ? </label>
								<div class="form-group field-listing-description">

<textarea id="description" class="form-control margin_bottom10" name="description"   rows="6" required>{{$propertyObj->description}}</textarea>

<p class="help-block help-block-error"></p>
</div>	

	<label class="airfcfx-manage-label">Further optional description :</label>
								<div class="form-group field-listing-description">

<textarea id="optional_description" class="form-control margin_bottom10" name="optional_description"   rows="6"  placeholder="Eg:You will be collecting and cataloging goods, warehouse and till management,
 checking shop disposition, price labelling and serving clients." and "Student can take breaks upon
 request, call their school Tutor or Parents but not use the phone for other reasons.">{{$propertyObj->optional_description}}</textarea>

<p class="help-block help-block-error"></p>
</div>	

<label class="airfcfx-manage-label">Duration in (maximum) working hours of the experience, weekly (eg. 40):</label>
								<div class="form-group field-listing-listingname">

<input type="number" id="work_hours" class="form-control margin_bottom10" name="work_hours" value="{{$propertyObj->work_hours}}" required>

<p class="help-block help-block-error"></p>
</div>	


<label class="airfcfx-manage-label">Hourly articulation (daily and weekly) Enter daily and weekly scheduled working hours (eg. from 9am to 12.30pm and from 1pm to 5pm)</label>
								<div class="form-group field-listing-listingname">

<input type="text" id="work_schedule" class="form-control margin_bottom10" name="work_schedule" value="{{$propertyObj->work_schedule}}" required>
 
</div>	

 <input type="submit" value="save" class="btn btn_email nextbtn">
							 
							</div>
							</form>
						</div>
						
						</div>
					</div>
					<!--row end -->
				</div>