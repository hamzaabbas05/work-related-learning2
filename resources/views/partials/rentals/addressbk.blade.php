@inject('country', 'App\Country')
<?php 
$countries = $country->getall();
?>
<div class="panel panel-default panelcls">
				<div class="panel-body">
					<div class="row">
					<div class="col-xs-12 margin_top10 margin_bottom20  commcls" id="locationdiv">
							<h3>Help Students find the workplace</h3>
							Workplace address where students will do their work experience.<hr />
							<br />
							<div class="col-sm-6 coldiv">
						<input type="hidden" value="{{$propertyObj->id}}" id="listingid" name="listingid">		
<h3> Legal Address </h3>
<label>LegalAddress</label>
								<div class="form-group field-listing-legal_address">

<input type="text" id="legal_address" class="form-control margin_bottom10" name="legal_address" value="{{$propertyObj->legal_address}}"  >

<p class="help-block help-block-error"></p>
</div>	

<div class="leftdiv">
								<label>LegalStreetAddressStreetNumber</label>
								<div class="form-group field-listing-lstreet_number">

<input type="text" id="lstreet_number" class="form-control margin_bottom10 divmediumtext" name="lstreet_number" value="{{$propertyObj->lstreet_number}}"> 

<p class="help-block help-block-error"></p>
</div>								</div>
								<div class="leftdiv">
								<label>LegalStreetAddressStreetName</label>
								<div class="form-group field-listing-lroute">

<input type="text" id="lroute" class="form-control margin_bottom10 divmediumtext" name="lroute" value="{{$propertyObj->lroute}}"> 

<p class="help-block help-block-error"></p>
</div>								</div>
								<div style="clear: both;">


<div class="leftdiv">
								<label>LegalAddress-Town/City </label>
								<div class="form-group field-listing-ladministrative_area_level_1">

<input type="text" id="llocality" class="form-control margin_bottom10 divmediumtext" name="llocality" value="{{$propertyObj->llocality}}"> 


<p class="help-block help-block-error"></p>
</div>								</div>
								<div class="leftdiv">
								<label>LegalAddress-State/Region</label>
								<div class="form-group field-listing-llocality">



<input type="text" id="ladministrative_area_level_1" class="form-control margin_bottom10 divmediumtext" name="ladministrative_area_level_1" value="{{$propertyObj->ladministrative_area_level_1}}"> 


<p class="help-block help-block-error"></p>
</div>								</div>
								 

<div class="leftdiv">
								<label>LegalAddress-ZIP/Postal code </label>
								<div class="form-group field-listing-lpostal_code">

<input type="text" id="lpostal_code" class="form-control margin_bottom10 divmediumtext" name="lpostal_code" value="{{$propertyObj->lpostal_code}}"> 

<p class="help-block help-block-error"></p>
</div>								</div>
								<div class="leftdiv">
								<label>LegalAddress-Country</label>
								<div class="form-group field-listing-lcountry">

<input type="text" id="lcountry" class="form-control margin_bottom10 divmediumtext" name="lcountry" value="{{$propertyObj->lcountry}}"> 

<p class="help-block help-block-error"></p>
</div>								</div>
								 



<h3> WorkPlace Address </h3>

 

<label>Workplace Email</label>
								<div class="form-group field-listing-location_email">

<input type="email" id="location_email" class="form-control margin_bottom10" name="location_email" value="{{$propertyObj->location_email}}"  >

<p class="help-block help-block-error"></p>
</div>								

<label>Workplace Telephone</label>
								<div class="form-group field-listing-location_telephone">

<input type="text" id="location_telephone" class="form-control margin_bottom10" name="location_telephone" value="{{$propertyObj->location_telephone}}"  onkeypress="return isNumber(event)"  >

<p class="help-block help-block-error"></p>
</div>	
<label>Workplace Address</label>
								<div class="form-group field-listing-workplace_address">

<input type="text" id="workplace_address" class="form-control margin_bottom10" name="workplace_address" value="{{$propertyObj->workplace_address}}"  >

<p class="help-block help-block-error"></p>
</div>	

<div class="leftdiv">
								<label>WorkplaceAddress-StreetNumber</label>
								<div class="form-group field-listing-wstreet_number">

<input type="text" id="wstreet_number" class="form-control margin_bottom10 divmediumtext" name="wstreet_number" value="{{$propertyObj->wstreet_number}}"> 

<p class="help-block help-block-error"></p>
</div>								</div>
								<div class="leftdiv">
								<label>WorkplaceAddress-StreetName</label>
								<div class="form-group field-listing-wroute">

<input type="text" id="wroute" class="form-control margin_bottom10 divmediumtext" name="wroute" value="{{$propertyObj->wroute}}"> 

<p class="help-block help-block-error"></p>
</div>								</div>
								<div style="clear: both;">


<div class="leftdiv">
								<label>WorkplaceAddress-Town/City </label>
								<div class="form-group field-listing-wadministrative_area_level_1">

<input type="text" id="wlocality" class="form-control margin_bottom10 divmediumtext" name="wlocality" value="{{$propertyObj->wlocality}}"> 

<p class="help-block help-block-error"></p>
</div>								</div>
								<div class="leftdiv">
								<label>WorkplaceAddress-State/Region</label>
								<div class="form-group field-listing-wlocality">
<input type="text" id="wadministrative_area_level_1" class="form-control margin_bottom10 divmediumtext" name="wadministrative_area_level_1" value="{{$propertyObj->wadministrative_area_level_1}}"> 


<p class="help-block help-block-error"></p>
</div>								</div>
								 

<div class="leftdiv">
								<label>WorkplaceAddress-ZIP/Postal code </label>
								<div class="form-group field-listing-wpostal_code">

<input type="text" id="wpostal_code" class="form-control margin_bottom10 divmediumtext" name="wpostal_code" value="{{$propertyObj->wpostal_code}}"> 

<p class="help-block help-block-error"></p>
</div>								</div>
								<div class="leftdiv">
								<label>WorkplaceAddress-Country</label>
								<div class="form-group field-listing-wcountry">

<input type="text" id="wcountry" class="form-control margin_bottom10 divmediumtext" name="wcountry" value="{{$propertyObj->wcountry}}"> 

<p class="help-block help-block-error"></p>
</div>								</div>
  <p style="visibility:hidden">Would students do work at other addresses?</p>
 <div class="form-group field-listing-location_other_address">
<input type="checkbox" <?php if ($propertyObj->work_other_check == 1) { ?> checked <?php } ?> id="work_other_check"><p style="margin-left:30px">Would students do work at other addresses?</p> </div>
<label>Describe other workplaces and eventually addresses</label>
								<div class="form-group field-listing-location_other_address">

<textarea type="text" id="location_other_address" class="form-control location_other_address" name="zipcode" vrows="10">{{$propertyObj->location_other_address}}</textarea>

<p class="help-block help-block-error"></p>
</div>

						<input type="hidden" value="{{$propertyObj->pro_lat}}" id="latbox">
									<input type="hidden" value="{{$propertyObj->pro_lon}}" id="lonbox">
								<div class="errcls" style="clear: both;"></div><br/>
								<input type="button" value="Save Address" class="btn btn_email leftbtn" onclick="showAddress();">
								</div>
							</div>
						</div>
				 	</div>
					</div>
					<!--row end -->
				</div>
				<script type="text/javascript">
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('legal_address'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
              /*  var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();*/

               /* document.getElementById("pro_lat").value = latitude;
                document.getElementById("pro_lon").value = longitude;
*/
                for (var component in componentForm) {
                        var cstr = "l";
 						document.getElementById(cstr+component).value = '';
                        document.getElementById(cstr+component).disabled = false;
                    }

 					for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    var cstr = "l";
                        if (componentForm[addressType]) {
                            
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(cstr+addressType).value = val;
                        }
                   
                  }                
               
            });


            var places1 = new google.maps.places.Autocomplete(document.getElementById('workplace_address'));
            google.maps.event.addListener(places1, 'place_changed', function () {
                var place = places1.getPlace();
                /*var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
*/
                /*document.getElementById("pro_lat").value = latitude;
                document.getElementById("pro_lon").value = longitude;
*/
                for (var component in componentForm) {
                        var cstr = "w";
 						document.getElementById(cstr+component).value = '';
                        document.getElementById(cstr+component).disabled = false;
                    }

 					for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    var cstr = "w";
                        if (componentForm[addressType]) {
                            
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(cstr+addressType).value = val;
                        }
                   
                  }                
               
            });




        });
    </script>