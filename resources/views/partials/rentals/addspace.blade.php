@inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<div class="col-xs-12 col-sm-12 col-md-9 margin_top20 margin_bottom20">
    <div class="listing-create">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="col-xs-12 airfcfx-createlist">
                <label>Work Category</label><br/>
                <?php
                $i = 1;
                $propertyTypeAdded = array();
                foreach ($propertyTypes as $obj) {
                    if ($i <= 3) {
                        array_push($propertyTypeAdded, $obj->id);
                        ?> 
                        <button type="button" class="btn btn_list home" value="2" data-toggle="tooltip" data-placement="bottom" title="">
                            <i class="fa fa-building-o list_icon"></i> {{$obj->name}}
                            <input type="hidden" value="{{$obj->id}}" id="<?php echo "home" . $obj->id ?>">
                        </button>
                        <?php
                    } $i++;
                }
                ?>
                <?php if (count($propertyTypes) > 3) { ?>
                    <select class="select_list  home" id="homeother" style="width:auto;position:relative;">
                        <option value="">Others</option>
                        <?php
                        foreach ($propertyTypes as $obj) {
                            if (!in_array($obj->id, $propertyTypeAdded)) {
                                ?>
                                <option value="{{$obj->id}}">{{$obj->name}}</option>
                                <?php
                            }
                        }
                        ?>
                    </select><?php } ?>
                <div id="homeerr" class="errcls"></div>
            </div> <!-- col-sm-12 end -->
            <!---<div class="col-xs-12 form-group margin_top20">
                <label>Kind of Listing</label>
                <div class="input-group">
                    <input type="radio" class="form-control"  id="property_status_unpaid" name="property_status" value="unpaid"/>Unpaid Work Experience
                    <input type="radio" class="form-control"  id="property_status_paid" name="property_status" value="paid"/>School House

                </div>
                <div id="workexperr" class="errcls"></div>
            </div> <!-- col-sm-12 end -->
            <div class="col-xs-12 form-group margin_top20">
                <label>Work Experience: Title of the Listing</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class=""></i></span>
                    <input type="text" class="form-control"  id="workexp" name="workexp"/>

                </div>
                <div id="workexperr" class="errcls"></div>
            </div> <!-- col-sm-12 end -->

            <div class="col-xs-12 form-group margin_top20">
                <label>Describe duties and what you expect student to do ?</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class=""></i></span>
                    <textarea rows="10" class="form-control"  id="description" name="description"></textarea>

                </div>
                <div id="descriptionerr" class="errcls"></div>
            </div> <!-- col-sm-12 end -->


            <div class="col-xs-12 form-group margin_top20">
                <label>Duration in (maximum) working hours of the experience, weekly (eg. 40):</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class=""></i></span>
                    <input type="number" class="form-control"  id="hour" name="hour"/>

                </div>
                <div id="hourerr" class="errcls"></div>
            </div> <!-- col-sm-12 end -->


            <div class="col-xs-12 form-group margin_top20">
                <label>Hourly articulation (daily and weekly) Enter daily and weekly scheduled working hours (eg. from 9am to 12.30pm and from 1pm to 5pm)</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class=""></i></span>
                    <input type="text" class="form-control"  id="schedule" name="schedule"/>

                </div>
                <div id="scheduleerr" class="errcls"></div>
            </div> <!-- col-sm-12 end -->

            <div class="xs-padding-0 col-xs-12 margin_top20">
                <input type="hidden" class="form-control"  id="property_status" name="property_status" value="unpaid"/>
                <a href="{{URL::to('rentals/add')}}">
                    <button class="airfcfx-panel btn btn_email padding10" type="button">Back</button>
                </a>
                <button class="airfcfx-panel btn btn_email padding10" type="button" onclick="savelist()">Continue</button>
                <div id="emailverifyerr" class="errcls"></div>
            </div>

        </div> <!-- col-sm-9 end -->

    </div>
</div>
</div> <!-- row end -->