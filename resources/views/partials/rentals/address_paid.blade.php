@inject('country', 'App\Country')
<?php
$countries = $country->getall();
?>
<!-- Comment THIS DONE  -->
<div class="panel panel-default panelcls">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 margin_top10 margin_bottom20  commcls" id="locationdiv">
                <h3>Specify location where Service will be provided</h3>
                Specify an address if different from yours (the one you specified before listing).<hr />
                <br />
                <div class="col-sm-6 coldiv">
                    <input type="hidden" value="{{$propertyObj->id}}" id="listingid" name="listingid">		
                    <div class="col-xs-12 margin_top10">
                        <input type="hidden" id="orglat" value="{{\Auth::user()->org_lat}}">
                        <input type="hidden" id="orglan" value="{{\Auth::user()->org_lan}}">
                        <div class="col-xs-12 col-sm-2 text-right">
                            <input type="checkbox" <?php if ($propertyObj->addr_work_org_same == 0) { ?> checked <?php } ?> 
                                   id="checkaddr" name="addr_work_org_same" onchange="checkaddr(this)"> 
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <div class="form-group field-profile-firstname required">
                                Check IF Address is different from YOUR Address

                                <p class="help-block help-block-error"></p>
                            </div>                        </div>
                    </div></div>
                <?php
                $style = 'display:none';
                $req = "";
                if ($propertyObj->addr_work_org_same == 0) {
                    $style = 'display:block';
                    $req = "required";
                }
                ?>
                <div id="workplaceaddrdiv" style="{{$style}}"> 


                    <h3> Service Address </h3>



                    <label>Email</label>
                    <div class="form-group field-listing-location_email">

                        <input type="email" id="location_email" class="form-control margin_bottom10" name="location_email" value="{{$propertyObj->location_email}}"  >

                        <p class="help-block help-block-error"></p>
                    </div>								

                    <label>Telephone</label>
                    <div class="form-group field-listing-location_telephone">

                        <input type="text" id="location_telephone" class="form-control margin_bottom10" name="location_telephone" value="{{$propertyObj->location_telephone}}"  onkeypress="return isNumber(event)"  >

                        <p class="help-block help-block-error"></p>
                    </div>	
                    <label>Address - Start typing, add postal code, <strong>and select from list</strong>, this will automatically add correct values herein</label>
                    <div class="form-group field-listing-workplace_address">

                        <input type="text" id="workplace_address" class="form-control margin_bottom10" name="workplace_address" value="{{$propertyObj->workplace_address}}"  >

                        <p class="help-block help-block-error"></p>
                    </div>	

                    <div class="leftdiv">
                        <label>Address - Street Number</label>
                        <div class="form-group field-listing-wstreet_number">

                            <input type="text" id="wstreet_number" class="form-control margin_bottom10 divmediumtext" name="wstreet_number" value="{{$propertyObj->wstreet_number}}"> 

                            <p class="help-block help-block-error"></p>
                        </div>								</div>
                    <div class="leftdiv">
                        <label>Address - StreetName</label>
                        <div class="form-group field-listing-wroute">

                            <input type="text" id="wroute" class="form-control margin_bottom10 divmediumtext" name="wroute" value="{{$propertyObj->wroute}}"> 

                            <p class="help-block help-block-error"></p>
                        </div>								</div>
                    <div style="clear: both;">


                        <div class="leftdiv">
                            <label>Address - Town/City </label>
                            <div class="form-group field-listing-wadministrative_area_level_1">

                                <input type="text" id="wlocality" class="form-control margin_bottom10 divmediumtext" name="wlocality" value="{{$propertyObj->wlocality}}"> 

                                <p class="help-block help-block-error"></p>
                            </div>								</div>
                        <div class="leftdiv">
                            <label>Address - State/Region</label>
                            <div class="form-group field-listing-wlocality">
                                <input type="text" id="wadministrative_area_level_1" class="form-control margin_bottom10 divmediumtext" name="wadministrative_area_level_1" value="{{$propertyObj->wadministrative_area_level_1}}"> 


                                <p class="help-block help-block-error"></p>
                            </div>								</div>


                        <div class="leftdiv">
                            <label>Address - ZIP/Postal code </label>
                            <div class="form-group field-listing-wpostal_code">

                                <input type="text" id="wpostal_code" class="form-control margin_bottom10 divmediumtext" name="wpostal_code" value="{{$propertyObj->wpostal_code}}"> 

                                <p class="help-block help-block-error"></p>
                            </div>								</div>
                        <div class="leftdiv">
                            <label>Address - Country</label>
                            <div class="form-group field-listing-wcountry">

                                <input type="text" id="wcountry" class="form-control margin_bottom10 divmediumtext" name="wcountry" value="{{$propertyObj->wcountry}}"> 

                                <p class="help-block help-block-error"></p>
                            </div>								</div>


                    </div> </div>

                <p style="visibility:hidden">Would students have to go to other addresses?</p>
                <div class="form-group field-listing-location_other_address">
                    <input type="checkbox" <?php if ($propertyObj->work_other_check == 1) { ?> checked <?php } ?> id="work_other_check"><p style="margin-left:30px">Would students be provided the Service at other addresses?</p> </div>
                <label>Describe reasons why and possibly the complete addresses in plain text</label>
                <div class="form-group field-listing-location_other_address">

                    <textarea type="text" id="location_other_address" class="form-control location_other_address" name="zipcode" vrows="10">{{$propertyObj->location_other_address}}</textarea>

                    <p class="help-block help-block-error"></p>
                </div>

                <input type="hidden" value="{{$propertyObj->pro_lat}}" id="latbox">
                <input type="hidden" value="{{$propertyObj->pro_lon}}" id="lonbox">
                <div class="errcls" style="clear: both;"></div><br/>
                <input type="button" value="Save Address" class="btn btn_email leftbtn" onclick="showAddressPaid();">
            </div>
        </div>
    </div>
</div>
</div>
<!--row end -->
</div>
<script type="text/javascript">

    function checkaddr(event)
    {
        if (event.checked) {
            $('#workplaceaddrdiv').css('display', 'block');
            //$("#location_email").prop('required', true);
            //$("#location_telephone").prop('required', true);
            //$("#workplace_address").prop('required', true);
            //$("#wstreet_number").prop('required', true);
            //$("#wroute").prop('required', true);
            //$("#wlocality").prop('required', true);
            //$("#wadministrative_area_level_1").prop('required', true);
            //$("#wpostal_code").prop('required', true);
            //$("#wcountry").prop('required', true);
        } else {
            $('#workplaceaddrdiv').css('display', 'none');
            //$("#location_email").prop('required', false);
            //$("#location_telephone").prop('required', false);
            //$("#workplace_address").prop('required', false);
            //$("#wstreet_number").prop('required', false);
            //$("#wroute").prop('required', false);
            //$("#wlocality").prop('required', false);
            //$("#wadministrative_area_level_1").prop('required', false);
            //$("#wpostal_code").prop('required', false);
            //$("#wcountry").prop('required', true);
        }
    }


    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    /*google.maps.event.addDomListener(window, 'load', function () {
     var places = new google.maps.places.Autocomplete(document.getElementById('legal_address'));
     google.maps.event.addListener(places, 'place_changed', function () {
     var place = places.getPlace();
     var latitude = place.geometry.location.lat();
     var longitude = place.geometry.location.lng();
     
     document.getElementById("pro_lat").value = latitude;
     document.getElementById("pro_lon").value = longitude;
     
     for (var component in componentForm) {
     var cstr = "l";
     document.getElementById(cstr+component).value = '';
     document.getElementById(cstr+component).disabled = false;
     }
     
     for (var i = 0; i < place.address_components.length; i++) {
     var addressType = place.address_components[i].types[0];
     var cstr = "l";
     if (componentForm[addressType]) {
     
     var val = place.address_components[i][componentForm[addressType]];
     document.getElementById(cstr+addressType).value = val;
     }
     
     }                
     
     });
     */

    var places1 = new google.maps.places.Autocomplete(document.getElementById('workplace_address'));
    google.maps.event.addListener(places1, 'place_changed', function () {
        var place = places1.getPlace();
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();

        document.getElementById("latbox").value = latitude;
        document.getElementById("lonbox").value = longitude;

        for (var component in componentForm) {
            var cstr = "w";
            document.getElementById(cstr + component).value = '';
            document.getElementById(cstr + component).disabled = false;
        }

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var cstr = "w";
            if (componentForm[addressType]) {

                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(cstr + addressType).value = val;
            }

        }

    });





</script>