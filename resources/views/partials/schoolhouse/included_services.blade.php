<hr style="border: 1px solid #DDD;"/>
<div class="col-xs-12 margin_top10">
    <a><h3>General Info</h3></a>

    <p>Allow your Student Guests to know what you normally provide. Students only select one of your beds when sending their request after picking you, the rest of the services will be listed on your Profile page to allow them to pick the right School House, yours.</p> 
        <br>
       <p> Please add as Optional Services what you wish to be paid extra for among your listings.</p>
    <br>
    <p>Beware that any false information, therefore clearly proving you are not providing the services, entitles Students not to pay or to ask for a refund as specified in English UK guidelines.</p>
 
     <br>
       <p>This is free text area for you. First we suggest completing specific areas of interest such as Activities, Classes, Diet, Meals, Tours, Transfers and WOWs with related descriptions you may provide below this section.</p>
    <br>
    <div class="col-xs-12 margin_top10">
        <div class="col-xs-12 col-sm-3 text-right">
            <label class="profile_label">Description:</label> 
           
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="form-group field-profile-emergencyname">
                <input type="text" class="form-control" name="service_included" value="{{isset($userObject->paid_service->service_included)?$userObject->paid_service->service_included:''}}" />
                <p class="help-block help-block-error"></p>
            </div>                                
        </div>
    </div>   
    
    <div class="col-xs-12 margin_top10">
        <div class="col-xs-12 col-sm-3 text-right">
            <label class="profile_label">Further Detailed Description:</label> 
            <br>
       <p>For any further comment you were unable to add elsewhere</p>
    <br>
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="form-group field-profile-emergencyname">
                <textarea class="form-control" rows="10" name="service_desc">{{isset($userObject->paid_service->service_desc)?$userObject->paid_service->service_desc:''}}</textarea>
                <p class="help-block help-block-error"></p>
            </div>                                
        </div>
    </div>

</div>
<hr style="border: 1px solid #DDD;"/>
<div class="col-xs-12 margin_top10">
    <a><h3>Activities</h3></a>

    <p>Allow your Student Guests to know what Activities you normally provide. List here what you are willing to do with students. Extra costs for tickets or fares may be asked directly to Student Guests, please discuss this together with them. If you wish to be paid more than cost of tickets and fares, please add as Optional Services among your listings</p>
    <br>
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">You may select more than one:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <?php
            if (sizeof($activityTypes) > 0) {
                foreach ($activityTypes as $activityObj) {
                    ?>  
                    <label style="display:block; float: left; width: 45%;"  class="airfcfx-search-checkbox-text">
                        <input type="checkbox" id="host_preference" name="school_house_activity[]" value="{{$activityObj->id}}" <?php if (in_array($activityObj->id, $activityTypeArray)) { ?> checked="" <?php } ?>>
                        <span style="margin-left: 20px;">{{$activityObj->activity_type}}</span>
                    </label>
                    <?php
                }
            }
            ?>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end you may include description and meals provided too okay-->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe your Activity:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <textarea id="pet_desc" class="form-control" rows="10" name="activity_desc" >{{isset($userObject->paid_service->activity_desc)?$userObject->paid_service->activity_desc:''}}</textarea>
        </div>                    
    </div>
</div>
<hr style="border: 1px solid #DDD;"/>
<div class="col-xs-12 margin_top10">
    <a><h3>Classes and Conversation sessions</h3></a>

    <p>Allow your Student Guests to know what Classes and Conversation sessions, that is dedicated speaking time, you normally provide. List here what classes you are willing to do. Extra costs for tickets or fares may be asked directly to Student Guests, please discuss this together with them. If you wish to be paid more than cost of tickets and fares, please add as Optional Services among your listings</p>
    <br>
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">You may select more than one:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <?php
            if (sizeof($class_types) > 0) {
                foreach ($class_types as $classObj) {
                    ?>  
                    <label style="display:block; float: left; width: 45%;"  class="airfcfx-search-checkbox-text">
                        <input type="checkbox" id="host_preference" name="school_house_classes[]" value="{{$classObj->id}}" <?php if (in_array($classObj->id, $classTypeArray)) { ?> checked="" <?php } ?>>
                        <span style="margin-left: 20px;">{{$classObj->class_type}}</span>
                    </label>
                    <?php
                }
            }
            ?>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end you may include description and meals provided too okay-->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe your Class:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <textarea id="pet_desc" class="form-control" rows="10" name="class_desc" >{{isset($userObject->paid_service->class_desc)?$userObject->paid_service->class_desc:''}}</textarea>
        </div>                    
    </div>
</div>
<hr style="border: 1px solid #DDD;"/>
<div class="col-xs-12 margin_top10">
    <a><h3>Diet</h3></a>

    <p>Allow your Student Guests to know what diet you normally provide. List here what you are willing to prepare. Extra costs for food may be asked directly to Student Guests, please do this shopping together with them. If you wish to be paid more than cost of food, please add as Optional Services among your listings</p>
    <br>
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">You may select more than one:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <?php
            if (sizeof($dietTypes) > 0) {
                foreach ($dietTypes as $dietObj) {
                    ?>  
                    <label style="display:block; float: left; width: 45%;"  class="airfcfx-search-checkbox-text">
                        <input type="checkbox" id="host_preference" name="school_house_diet[]" value="{{$dietObj->Code}}" <?php if (in_array($dietObj->Code, $dietTypeArray)) { ?> checked="" <?php } ?>>
                        <span style="margin-left: 20px;">{{$dietObj->Code}}</span>
                    </label>
                    <?php
                }
            }
            ?>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end you may include description and meals provided too okay-->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe your Diet:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <textarea id="pet_desc" class="form-control" rows="10" name="diet_desc" >{{isset($userObject->paid_service->diet_desc)?$userObject->paid_service->diet_desc:''}}</textarea>
        </div>                    
    </div>
</div>

<hr style="border: 1px solid #DDD;"/>
<div class="col-xs-12 margin_top10">
    <a><h3>Meals Provided</h3></a>

    <p>Allow your Student Guests to know what meals you normally provide included in the price.</p>
    <br>
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">You may select only one:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <?php
            if (sizeof($dietBoardTypes) > 0) {
                foreach ($dietBoardTypes as $dietBoardObj) {
                    ?>  
                    <label style="display:block; float: left; width: 45%;"  class="airfcfx-search-checkbox-text">
                        <input type="radio" id="host_preference" name="school_house_diet_board" value="{{$dietBoardObj->Code}}" <?php if (isset($userObject->paid_service->school_house_diet_board) && $userObject->paid_service->school_house_diet_board == $dietBoardObj->Code) { ?> checked="" <?php } ?>>
                        <span style="margin-left: 20px;">{{$dietBoardObj->Code}}</span>
                    </label>
                    <?php
                }
            }
            ?>
            <div style="clear: both;"></div>
            <p>*Lunch may be the well known "packed lunch".</p>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end -->

<hr style="border: 1px solid #DDD;"/>
<div class="col-xs-12 margin_top10">
    <a><h3>Transfers</h3></a>

    <p>Allow your Student Guests to know what Transfer you normally provide. List here what you are willing to prepare. Extra costs for food may be asked directly to Student Guests, please do this shopping together with them. If you wish to be paid more than cost of food, please add as Optional Services among your listings</p>
    <br>
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">You may select more than one:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <?php
            if (sizeof($transfer_types) > 0) {
                foreach ($transfer_types as $transferObj) {
                    ?>  
                    <label style="display:block; float: left; width: 45%;"  class="airfcfx-search-checkbox-text">
                        <input type="checkbox" id="host_preference" name="school_house_transfers[]" value="{{$transferObj->id}}" <?php if (in_array($transferObj->id, $transferTypeArray)) { ?> checked="" <?php } ?>>
                        <span style="margin-left: 20px;">{{$transferObj->transfer_type}}</span>
                    </label>
                    <?php
                }
            }
            ?>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end you may include description and meals provided too okay-->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe your Transfer:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <textarea id="pet_desc" class="form-control" rows="10" name="transfer_desc" >{{isset($userObject->paid_service->transfer_desc)?$userObject->paid_service->transfer_desc:''}}</textarea>
        </div>                    
    </div>
</div>

<hr style="border: 1px solid #DDD;"/>
<div class="col-xs-12 margin_top10">
    <a><h3>Tours</h3></a>

    <p>Allow your Student Guests to know what Tours you normally provide. List here what you are willing to prepare. Extra costs for food may be asked directly to Student Guests, please do this shopping together with them. If you wish to be paid more than cost of food, please add as Optional Services among your listings</p>
    <br>
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">You may select more than one:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <?php
            if (sizeof($tour_types) > 0) {
                foreach ($tour_types as $tourObj) {
                    ?>  
                    <label style="display:block; float: left; width: 45%;"  class="airfcfx-search-checkbox-text">
                        <input type="checkbox" id="host_preference" name="school_house_tours[]" value="{{$tourObj->id}}" <?php if (in_array($tourObj->id, $tourTypeArray)) { ?> checked="" <?php } ?>>
                        <span style="margin-left: 20px;">{{$tourObj->tour_type}}</span>
                    </label>
                    <?php
                }
            }
            ?>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end you may include description and meals provided too okay-->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe your Tour:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <textarea id="pet_desc" class="form-control" rows="10" name="tour_desc" >{{isset($userObject->paid_service->tour_desc)?$userObject->paid_service->tour_desc:''}}</textarea>
        </div>                    
    </div>
</div>

<hr style="border: 1px solid #DDD;"/>
<div class="col-xs-12 margin_top10">
    <a><h3>WOW</h3></a>

    <p>Allow your Student Guests to know what WOW you normally provide. List here what you are willing to prepare. Extra costs for food may be asked directly to Student Guests, please do this shopping together with them. If you wish to be paid more than cost of food, please add as Optional Services among your listings</p>
    <br>
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">You may select more than one:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <?php
            if (sizeof($wow_types) > 0) {
                foreach ($wow_types as $wowObj) {
                    ?>  
                    <label style="display:block; float: left; width: 45%;"  class="airfcfx-search-checkbox-text">
                        <input type="checkbox" id="host_preference" name="school_house_wows[]" value="{{$wowObj->id}}" <?php if (in_array($wowObj->id, $wowTypeArray)) { ?> checked="" <?php } ?>>
                        <span style="margin-left: 20px;">{{$wowObj->wow_type}}</span>
                    </label>
                    <?php
                }
            }
            ?>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end you may include description and meals provided too okay-->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe your WOW:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <textarea id="pet_desc" class="form-control" rows="10" name="wow_desc" >{{isset($userObject->paid_service->wow_desc)?$userObject->paid_service->wow_desc:''}}</textarea>
        </div>                    
    </div>
</div>
