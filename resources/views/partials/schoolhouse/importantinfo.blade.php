<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">How to get Home from Airport?</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="airport_way" class="form-control" rows="5" name="airport_way">{{isset($userObject->paid_service->airport_way)?$userObject->paid_service->airport_way:''}}</textarea>

            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> <!--col-xs-12 end -->



<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">How to get Home from Town Centre</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="home_way" class="form-control" rows="5" name="home_way">{{isset($userObject->paid_service->home_way)?$userObject->paid_service->home_way:''}}</textarea>

            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> <!--col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">What mobile phone company to use?</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="mobile_use" class="form-control" rows="5" name="mobile_use">{{isset($userObject->paid_service->mobile_use)?$userObject->paid_service->mobile_use:''}}</textarea>

            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end -->
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Link to .pdf document with all the details of the School House, property, services and area</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <input type="text" name="pdf_document" class="form-control" value="{{isset($userObject->paid_service->pdf_document)?$userObject->paid_service->pdf_document:''}}" />
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end -->