@include('partials.errors')
<div class="panel panel-default panel-faq" style="margin-top:30px; display: none;">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#services" class="collapsed" aria-expanded="false">
            <h4 class="panel-title">Services Included<i class="fa fa-plus text-danger"></i>
                <span class="pull-right"></span>
            </h4>
        </a>
    </div>
    <div id="services" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="panel-body" style="display: block;">
            @include('partials.schoolhouse.service')  
        </div>
    </div>
</div>

<div class="panel panel-default panel-faq" style="margin-top:30px">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#info" class="collapsed" aria-expanded="false">
            <h4 class="panel-title">Important Information<i class="fa fa-plus text-danger"></i>
                <span class="pull-right"></span>
            </h4>
        </a>
    </div>
    <div id="info" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="panel-body" style="display: block;">
            @include('partials.schoolhouse.importantinfo')  
        </div>
    </div>
</div>

<div class="panel panel-default panel-faq" style="margin-top:30px">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#area" class="collapsed" aria-expanded="false">
            <h4 class="panel-title">Area And Amenities<i class="fa fa-plus text-danger"></i>
                <span class="pull-right"></span>
            </h4>
        </a>
    </div>
    <div id="area" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="panel-body" style="display: block;">
            @include('partials.schoolhouse.areaamenities')  
        </div>
    </div>
</div>





<div class="panel panel-default panel-faq" style="margin-top:30px">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#bank_info" class="collapsed" aria-expanded="false">
            <h4 class="panel-title">Bank Info<i class="fa fa-plus text-danger"></i>
                <span class="pull-right"></span>
            </h4>
        </a>
    </div>
    <div id="bank_info" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="panel-body" style="display: block;">
            @include('partials.schoolhouse.bank_info')
        </div>
    </div>
</div>

<div class="errcls" id="submiterr1" style="clear: both;"></div><br/>
<!--<div class="form-group">
    <button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20" onclick="return edit_schoolhouse_basic();">Submit</button>
</div>
</form>-->






















