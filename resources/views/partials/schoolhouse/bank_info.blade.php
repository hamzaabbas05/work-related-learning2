<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Account Holder Name</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="account_holder_name" class="form-control" name="account_holder_name" value="{{isset($userObject->bank_detail->account_holder_name)?$userObject->bank_detail->account_holder_name:''}}">

            <p class="help-block help-block-error"></p>
        </div>  
        <div class="errcls" id="orgnameerr" style="clear: both;"></div><br/>                       

    </div>
</div> <!--col-xs-12 end -->
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Bank Name</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="bank_name" class="form-control" name="bank_name" value="{{isset($userObject->bank_detail->bank_name)?$userObject->bank_detail->bank_name:''}}">

            <p class="help-block help-block-error"></p>
        </div>  
        <div class="errcls" id="orgnameerr" style="clear: both;"></div><br/>                       

    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">IBAN</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">

            <input type="text" id="iban" class="form-control" name="iban" value="{{isset($userObject->bank_detail->iban)?$userObject->bank_detail->iban:''}}">

            <p class="help-block help-block-error"></p>
        </div>                         
        <div class="errcls" id="aboutorgerr" style="clear: both;"></div><br/> 
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Swift Code</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">
            <input type="text" id="swift_code" class="form-control" name="swift_code" value="{{isset($userObject->bank_detail->swift_code)?$userObject->bank_detail->swift_code:''}}">
            <p class="help-block help-block-error"></p>
        </div>  
        <div class="errcls" id="orgnameerr" style="clear: both;"></div><br/>
    </div>
</div>
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Sort Code</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">
            <?php
            $sortCode = isset($userObject->bank_detail->sortCode) ? $userObject->bank_detail->sortCode : "";
            if (!empty($sortCode)) {
                $sortCode = explode("-", $sortCode);
            }
            ?>
            <input type="text" id="sortCode1" maxlength="2" max="2" class="form-control" name="sortCode1" value="{{isset($sortCode[0])?$sortCode[0]:''}}" style="width: 20%;"> - 
            <input type="text" id="sortCode2" maxlength="2" max="2" class="form-control" name="sortCode2" value="{{isset($sortCode[1])?$sortCode[1]:''}}" style="width: 20%;"> - 
            <input type="text" id="sortCode3" maxlength="2" max="2" class="form-control" name="sortCode3" value="{{isset($sortCode[2])?$sortCode[2]:''}}" style="width: 20%;">
            <p class="help-block help-block-error"></p>
        </div>  
        <div class="errcls" id="orgnameerr" style="clear: both;"></div><br/>
    </div>
</div>
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Account Number</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-school">
            <input type="text" id="accountNumber" class="form-control" name="accountNumber" value="{{isset($userObject->bank_detail->accountNumber)?$userObject->bank_detail->accountNumber:''}}">
            <p class="help-block help-block-error"></p>
        </div>  
        <div class="errcls" id="orgnameerr" style="clear: both;"></div><br/>
    </div>
</div>