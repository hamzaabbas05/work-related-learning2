<!-- <div class="row" id="property_info_image_display" style="margin-top:50px;"> -->
    <?php
    /*if (sizeof($propertyimages) > 0) {
        foreach ($propertyimages as $obj) {
            $imgname = $obj->img_name;
            $imgURL = asset('images/schoolhouse/' . $obj->user_id . '/' . $imgname);
            ?> 
            <div class="col-md-3">
                <img style="height:100px" src="{{$imgURL}}" />
            </div>
        <?php }
    }*/ ?>
<!-- </div> -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Note: [Upload only images in png,jpg format], Upload Photo one at a time.</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">

            <input type="file" class="form-control property_image_paid" name="property_photo" >
            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> <!--col-xs-12 end -->
<!--<div class="form-group">
    <button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20">Submit</button>
</div>   

</form>          -->

<script>
$(document).ready(function(){
    // $(".property_image_paid").change(function(){
        $('input[name=property_image_paid]').change(function() {
            // alert("Hi, I mage upload event fire");
            // return false;
            var inp = this;
    uploadedfiles = $("#uploadedfiles").val();

    //check file format.
    var file = inp.files[0];
    var fileType = file["type"];
    // alert("Imae Name : " + file + "Image File Type : " + fileType);
    // return false;

    var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png"];
    
    if ($.inArray(fileType, ValidImageTypes) < 0) {
         alert("Invalid file format uploaded, please use jpg, jpeg and png only.");
        return false;
    }

    // if (uploadedfiles != "")
    // {
    //     uploaded = jQuery.parseJSON(uploadedfiles);
    //     uploadedlen = uploaded.length;
    // } else
    // {
    //     uploadedlen = 0;
    // }
    imagesarr = [];
    var i = 0, len = inp.files.length, img, reader, file;


    /*remainfiles = parseInt(5) - parseInt(uploadedlen);

    if (len > remainfiles)
    {
        $(".photoerrcls").show();
        $(".photoerrcls").html("You can add only 5 images");
        setTimeout(function () {
            $(".photoerrcls").slideUp();
            $('.photoerrcls').html('');
        }, 5000);
        return false;
    }*/


    /*if (len == 0) {
        $(".photoerrcls").show();
        $(".photoerrcls").html("Please Select Image");
        setTimeout(function () {
            $(".photoerrcls").slideUp();
            $('.photoerrcls').html('');
        }, 5000);
        return false;
    }*/
    formdata = new FormData();
    formdata.append('current_page', 'profile_info_image');
    // formdata.append('redirect', redirectUrl);
    var i = 0;
    // for (; i < len; i++) {
        file = inp.files[i];

        if (!!file.type.match(/image.*/)) {
            if (window.FileReader) {
                reader = new FileReader();
                reader.onloadend = function (e) {
                    //showUploadedItem(e.target.result, file.fileName);
                };
                reader.readAsDataURL(file);
            }
            if (formdata) {
                formdata.append("property_image_paid[]", file);
            }
        }
    // }

    $.ajax({
            // url: baseurl + '/rentals/startfileupload', // point to server-side PHP script 
            // url: baseurl + '/user/paidService', // point to server-side PHP script 
            url: baseurl + '/user/update_profile', // point to server-side PHP script 
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#loadingimg").show();
                $("#startuploadbtn").attr("disabled", "true");
            },
            success: function (res) {
                // alert("Response Get : " + res);
                $("#property_info_image_display").append(res);
                $('input[name=property_image_paid]').val('');
                return false;
            }
        });
        });
});
function start_file_upload(redirectUrl = '')
{
    
    

    if (formdata) {
         

        $.ajax({
            url: baseurl + '/rentals/startfileupload', // point to server-side PHP script 
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#loadingimg").show();
                $("#startuploadbtn").attr("disabled", "true");
            },
            success: function (res) {
                $("#imagenames").html("");
                $("#loadingimg").hide();
                // return false;

                if ($.trim(res) == "error") {
                    $(".photoerrcls").show();
                    $(".photoerrcls").html("File size is large");
                    setTimeout(function () {
                        $(".photoerrcls").slideUp();
                        $('.photoerrcls').html('');
                    }, 5000);
                    $("#startuploadbtn").removeAttr("disabled");
                    $("#uploadfile").val("");
                } else
                {
                    result = res.split("***");


                    
                    inputfiles = $("#uploadedfiles").val();
                    if (inputfiles == "")
                        $("#uploadedfiles").val(result[1]);

                    else
                    {

                        existfiles = $("#uploadedfiles").val();
                        if (existfiles == "[]")
                        {
                            $("#uploadedfiles").val('');
                            $("#uploadedfiles").val(result[1]);

                        } else {

                            newfiles = result[1].replace('[', '');
                            existfiles = existfiles.replace(']', '');
                            $("#uploadedfiles").val(existfiles + ',' + newfiles);
                        }

                    }
                    
                    for(var k=0;k<result.length;k++){
                        var result1 = result[k].split("]");
                    
                        if(k==0){

                            $("#imagepreview").append(result[k]);
                        }
                        if($.type(result1[1]) != 'undefined' || $.type(result1[1]) != ''){
                            $("#imagepreview").append(result1[1]);
                        }
                    }
                    // $("#imagepreview").append(result[0]);
                    $("#startuploadbtn").removeAttr("disabled");
                    $("#uploadfile").val("");
                }
            }
        });
    }
}
function remove_paid_image(org, imgname,page_name)//{
    // function remove_image(org, imgname,redirect='',page_name='rentals')
{
   if(confirm("Are you sure? By confirming you will remove this pic.")){
    $(org).hide();
    $(org).prev("img").hide();
    $(org).parent().remove();

    $.ajax({
        type: 'POST',
        url: baseurl + '/rentals/savephotolist',
        async: false,
        data: {
            uploadimg: imgname,
            type:'remove',
            page:page_name
        },
        success: function (data) {
            alert('Photo removed Successfully.');
            return true;
        }
    });

}
}
</script>
<style type="text/css">
    .listclose{right:30px !important;}
</style>