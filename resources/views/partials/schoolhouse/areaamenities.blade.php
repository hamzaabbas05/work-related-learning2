<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Property Urban Location</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <select class="form-control" name="urban_location">
                <option value="">Select Urban Location</option>
                <?php foreach ($urbanLocation as $urbanLocationobj) { ?>
                    <option <?php if (isset($userObject->paid_service->urban_location) && $userObject->paid_service->urban_location == $urbanLocationobj->Code) { ?> selected <?php } ?> value="{{$urbanLocationobj->Code}}">{{$urbanLocationobj->Code}}</option> <?php } ?>

            </select>


            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
    </div>
</div> <!--col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Property Region</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <select class="form-control" name="property_region">
                <option value="">Select Region</option>
                <?php foreach ($propertyRegion as $propertyRegionobj) { ?>
                    <option <?php if (isset($userObject->paid_service->property_region) && $userObject->paid_service->property_region == $propertyRegionobj->Code) { ?> selected <?php } ?> value="{{$propertyRegionobj->Code}}">{{$propertyRegionobj->Code}}</option> <?php } ?>

            </select>

            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
    </div>
</div> <!--col-xs-12 end -->



<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Local amenities</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <?php foreach ($houseLocalAmenities as $houseLocalAmenitiesobj) { ?>
                <input type="checkbox" id="local_amenity" name="local_amenity[]" value="{{$houseLocalAmenitiesobj->Code}}" <?php if (in_array($houseLocalAmenitiesobj->Code, $amenityArray)) { ?> checked="" <?php } ?>>
                <label style="display:block"  class="airfcfx-search-checkbox-text">{{$houseLocalAmenitiesobj->Code}}</label><br />
            <?php } ?>

            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Local amenities Description</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="amenity_desc" class="form-control" rows="8" name="amenity_desc">{{isset($userObject->paid_service->amenity_desc)?$userObject->paid_service->amenity_desc:''}}</textarea>

            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> <!--col-xs-12 end -->




