<div class="row" id="family_info_image_display" style="margin-top:50px;">

    <?php
    if (sizeof($familyimages) > 0) {
        foreach ($familyimages as $obj) {
            $imgname = $obj->img_name;
            $imgURL = asset('images/family/' . $obj->user_id . '/' . $imgname);
            ?> 
            <div class="col-md-3 listimgdiv">
                <img style="height:100px" src="{{$imgURL}}">
                  <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$obj->id" onclick="remove_paid_image(this, '{{$imgname}}','paidService_family_photo')"></i>
            </div>
            <?php
        }
    }
    ?>
</div>
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Note: [Upload only images in png,jpg format], Upload Photo one at a time.</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <input type="file" class="form-control" name="family_photo" >
            <p class="help-block help-block-error"></p>
        </div>                               
    </div>
</div>
<br>
<div class="col-xs-12 margin_top10">
    <a><h3>Family Members</h3></a>

    <div class="col-xs-12 col-sm-9">
        @include('partials.schoolhouse.members')  
    </div>
    <!--
    <div class="panel panel-default panel-faq" style="margin-top:30px">
    <!--col-xs-12 end 
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#members" class="collapsed" aria-expanded="false">
            <h4 class="panel-title">Members<i class="fa fa-plus text-danger"></i>
                <span class="pull-right"></span>
            </h4>
        </a>
    </div>
    <div id="members" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="panel-body" style="display: block;">
            
        </div>
    </div>
</div>
    -->
</div>

<hr style="border: 1px solid #DDD;"/>
<div class="col-xs-12 margin_top10">
    <a><h3>Pets</h3></a>
    <p>Allow your Student Guests to know if you have pets and they are</p>
    <br>
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">You may select more than one:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <?php foreach ($petsType as $petTypeobj) { ?>  
                <label style="display:block; float: left; width: 30%;"  class="airfcfx-search-checkbox-text">
                    <input type="checkbox" id="host_preference" name="pet_types[]" value="{{$petTypeobj->Code}}" <?php if (in_array($petTypeobj->Code, $petTypeArray)) { ?> checked="" <?php } ?>>
                    <span style="margin-left: 20px;">{{$petTypeobj->Code}}</span>
                </label>
            <?php } ?>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe your pet(s):</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <textarea id="pet_desc" class="form-control" rows="10" name="pet_desc" >{{isset($userObject->paid_service->pet_desc)?$userObject->paid_service->pet_desc:''}}</textarea>
        </div>                    
    </div>
</div>
<hr style="border: 1px solid #DDD;"/>






<!--col-xs-12 end -->
<!--<div class="form-group">
    <button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20">Submit</button>
</div>             
</form>         -->
<script>
$(document).ready(function(){
    // $(".property_image_paid").change(function(){
        $('input[name=family_photo]').change(function() {
            // alert("Hi, I mage upload event fire");
            // return false;
            var inp = this;
    uploadedfiles = $("#uploadedfiles").val();

    //check file format.
    var file = inp.files[0];
    var fileType = file["type"];
    // alert("Imae Name : " + file + "Image File Type : " + fileType);
    // return false;

    var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png"];
    
    if ($.inArray(fileType, ValidImageTypes) < 0) {
         alert("Invalid file format uploaded, please use jpg, jpeg and png only.");
        return false;
    }

    // if (uploadedfiles != "")
    // {
    //     uploaded = jQuery.parseJSON(uploadedfiles);
    //     uploadedlen = uploaded.length;
    // } else
    // {
    //     uploadedlen = 0;
    // }
    imagesarr = [];
    var i = 0, len = inp.files.length, img, reader, file;


    /*remainfiles = parseInt(5) - parseInt(uploadedlen);

    if (len > remainfiles)
    {
        $(".photoerrcls").show();
        $(".photoerrcls").html("You can add only 5 images");
        setTimeout(function () {
            $(".photoerrcls").slideUp();
            $('.photoerrcls').html('');
        }, 5000);
        return false;
    }*/


    /*if (len == 0) {
        $(".photoerrcls").show();
        $(".photoerrcls").html("Please Select Image");
        setTimeout(function () {
            $(".photoerrcls").slideUp();
            $('.photoerrcls').html('');
        }, 5000);
        return false;
    }*/
    formdata = new FormData();
    formdata.append('current_page', 'family_info_image');
    // formdata.append('redirect', redirectUrl);
    var i = 0;
    // for (; i < len; i++) {
        file = inp.files[i];

        if (!!file.type.match(/image.*/)) {
            if (window.FileReader) {
                reader = new FileReader();
                reader.onloadend = function (e) {
                    //showUploadedItem(e.target.result, file.fileName);
                };
                reader.readAsDataURL(file);
            }
            if (formdata) {
                formdata.append("family_photo[]", file);
            }
        }
    // }

    $.ajax({
            // url: baseurl + '/rentals/startfileupload', // point to server-side PHP script 
            // url: baseurl + '/user/paidService', // point to server-side PHP script 
            url: baseurl + '/user/update_profile', // point to server-side PHP script 
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#loadingimg").show();
                $("#startuploadbtn").attr("disabled", "true");
            },
            success: function (res) {
                // alert("Response Get : " + res);
                $("#family_info_image_display").append(res);
                $('input[name=family_photo]').val('');
                
                return false;
            }
        });
        });
});
</script>
<style type="text/css">
    .listclose{right:20px !important;}
</style>