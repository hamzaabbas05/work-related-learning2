<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Facilities</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <?php foreach ($facilities as $facilitiesobj) { ?>

            <input type="checkbox" id="facilities" name="facilities[]" value="{{$facilitiesobj->Code}}" <?php if (in_array($facilitiesobj->Code, $facilitiesArray)) { ?> checked="" <?php } ?>>
            <label style="display:block"  class="airfcfx-search-checkbox-text">{{$facilitiesobj->Code}}</label><br />
            <?php } ?>

            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> <!--col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Facility - Description</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="facility_desc" class="form-control" rows="8" name="facility_desc">{{isset($userObject->paid_service->facility_desc)?$userObject->paid_service->facility_desc:''}}</textarea>

            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> <!--col-xs-12 end -->




