<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Home Type</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <select class="form-control" name="home_type">
                <option value="">Select Home Type</option>
                <?php foreach ($homeType as $homeTypeobj) { ?>
                    <option <?php if (isset($userObject->paid_service->home_type) && $userObject->paid_service->home_type == $homeTypeobj->Code) { ?> selected <?php } ?>  value="{{$homeTypeobj->Code}}">{{$homeTypeobj->Code}}</option> <?php } ?>

            </select>

            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
    </div>
</div> <!--col-xs-12 end -->
<!--
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Property description[Slogan]</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="property_slogan" class="form-control" rows="5" name="property_slogan">{{isset($userObject->paid_service->property_slogan)?$userObject->paid_service->property_slogan:''}}</textarea>


            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> col-xs-12 end -->



<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Description:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="property_desc" class="form-control" rows="5" name="property_desc">{{isset($userObject->paid_service->property_desc)?$userObject->paid_service->property_desc:''}}</textarea>


            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> <!--col-xs-12 end -->




