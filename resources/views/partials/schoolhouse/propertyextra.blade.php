<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Number of Rooms</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <select class="form-control" name="room_no">
                <option value="">Select Rooms</option>
                <?php for ($i = 1; $i <= 100; $i++) { ?> 
                    <option value="{{$i}}"  <?php if (isset($userObject->paid_service->room_no) && $userObject->paid_service->room_no == $i) { ?> selected <?php } ?>>{{$i}}</option>


                <?php } ?>

            </select> 

            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Number of Guests room</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <select class="form-control" name="guest_room_no">
                <option value="">Select GuestRooms</option>
                <?php for ($i = 1; $i <= 100; $i++) { ?> 
                    <option value="{{$i}}"  <?php if (isset($userObject->paid_service->guest_room_no) && $userObject->paid_service->guest_room_no == $i) { ?> selected <?php } ?>>{{$i}}</option>


                <?php } ?>

            </select> 

            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Number of toilets</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <select class="form-control" name="toilet_no">
                <option value="">Select  Toilets</option>
                <?php for ($i = 1; $i <= 100; $i++) { ?> 
                    <option value="{{$i}}"  <?php if (isset($userObject->paid_service->toilet_no) && $userObject->paid_service->toilet_no == $i) { ?> selected <?php } ?>>{{$i}}</option>


                <?php } ?>

            </select> 

            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Property Number of showers</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <select class="form-control" name="shower_no">
                <option value="">Select Showers</option>
                <?php for ($i = 1; $i <= 100; $i++) { ?> 
                    <option value="{{$i}}"  <?php if (isset($userObject->paid_service->shower_no) && $userObject->paid_service->shower_no == $i) { ?> selected <?php } ?>>{{$i}}</option>


                <?php } ?>

            </select> 

            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Number of Baths</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <select class="form-control" name="bath_no">
                <option value="">Select Bathrooms</option>
                <?php for ($i = 1; $i <= 100; $i++) { ?> 
                    <option value="{{$i}}"  <?php if (isset($userObject->paid_service->bath_no) && $userObject->paid_service->bath_no == $i) { ?> selected <?php } ?>>{{$i}}</option>


                <?php } ?>

            </select>
            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
    </div>
</div> <!--col-xs-12 end -->




