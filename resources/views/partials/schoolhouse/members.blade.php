<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Family Name</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname required">
            <input type="text" id="family_name" class="form-control" name="family_name" value="{{isset($userObject->paid_service->family_name)?$userObject->paid_service->family_name:''}}">
        </div>                    
    </div>
</div>

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Is the Language at Home the Country's official?<i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <input type="radio" <?php if (isset($userObject->paid_service->is_language) && $userObject->paid_service->is_language == 1) { ?> checked="" <?php } ?> value="1" name="is_language"><label style="display:block"  class="airfcfx-search-checkbox-text">Yes</label><br />
        <input type="radio" <?php if (isset($userObject->paid_service->is_language) && $userObject->paid_service->is_language == 0) { ?> checked="" <?php } ?> value="0" name="is_language"><label style="display:block"  class="airfcfx-search-checkbox-text">No</label><br />

    </div>
</div> <!--col-xs-12 end -->



<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">All Members General Description</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="member_desc" class="form-control" rows="10" name="member_desc">{{isset($userObject->paid_service->member_desc)?$userObject->paid_service->member_desc:''}}</textarea>

            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> <!--col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Family situation at Home<i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <select class="form-control" name="family_situation">
            <option value="">If you like share your Family Situation</option>
            <?php foreach ($familySituation as $familySituationobj) { ?>
                <option <?php if (isset($userObject->paid_service->family_situation) && $userObject->paid_service->family_situation == $familySituationobj->Code) { ?> selected <?php } ?> value="{{$familySituationobj->Code}}">{{$familySituationobj->Code}}</option> <?php } ?>
        </select>
    </div>
</div> <!--col-xs-12 end -->
<div class="col-xs-12 margin_top10" style="margin-bottom: 10px;">
    <div class="col-xs-12 col-sm-12">
        <label class="profile_label" style="margin-right: 15px;">
            Family Members/Children (staying at home)
            <i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> 
        </label> 
        <a href="javascript:;" id="add_more_members" style="text-align: right; float: right;">
            <img src="{{Asset('images/plus.png')}}" alt="Add More Members" style="max-width: 32px;" />
        </a>
    </div>
</div> <!--col-xs-12 end -->
<div id="insert_more_fields">
    @if(isset($userObject->paidServiceFamilyRelation) && sizeof($userObject->paidServiceFamilyRelation) > 0)
    @foreach($userObject->paidServiceFamilyRelation as $f_relation)
    <div>
        <hr style="border: 1px solid #ddd; width: 100%;"/>
        <div class="col-xs-12 margin_top10" style="margin-bottom: 10px;">
            <div class="col-xs-12 col-sm-12">
                <a href="javascript:;" onclick="removeMember(this)" style="text-align: right; float: right;">
                    <img src="{{Asset('images/minus.png')}}" alt="Remove Member" style="max-width: 32px;" />
                </a>
            </div>
        </div>
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Name</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-firstname required">
                    <input type="text" id="family_name" class="form-control" name="family_member_name[]" value="{{$f_relation->name}}"/>
                </div>                    
            </div>
        </div>
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Birth Year</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-firstname required">
                    <select name="family_member_birth_year[]" class="form-control">
                        <option value="">Select Year</option>
                        <?php
                        $currentYear = date("Y");
                        $startYear = $currentYear - 15;
                        $baseYear = 1900;
                        for ($i = $currentYear; $i >= $baseYear; $i--) {
                            ?>
                            <option value="{{$i}}" @if($f_relation->birth_year == $i) selected="" @endif>{{$i}}</option>
                        <?php } ?>
                    </select>
                </div>                    
            </div>
        </div>
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Relation</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-firstname required">
                    <select name="family_member_relation[]" class="form-control">
                        <option value="">Select Relation</option>
                        <option value="Son" @if($f_relation->relationship == "Son") selected="" @endif>Son</option>
                        <option value="Daughter" @if($f_relation->relationship == "Daughter") selected="" @endif>Daughter</option>
                        <option value="Host Father" @if($f_relation->relationship == "Host Father") selected="" @endif>Host Father</option>
                        <option value="Host Mother" @if($f_relation->relationship == "Host Mother") selected="" @endif>Host Mother</option>
                        <option value="Nephew" @if($f_relation->relationship == "Nephew") selected="" @endif>Nephew</option>
                        <option value="Niece" @if($f_relation->relationship == "Niece") selected="" @endif>Niece</option>
                        <option value="In-laws" @if($f_relation->relationship == "In-laws") selected="" @endif>In-laws</option>
                        <option value="Lodger" @if($f_relation->relationship == "Lodger") selected="" @endif>Lodger</option>
                        <option value="Other" @if($f_relation->relationship == "Other") selected="" @endif>Other</option>
                    </select>
                </div>                    
            </div>
        </div>
    </div>
    @endforeach
    @endif
</div>

