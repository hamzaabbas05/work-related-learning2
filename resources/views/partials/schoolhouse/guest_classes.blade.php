<!--- Start Add new Room modal--->
<div class="modal fade " id="add_classroom_modal" tabindex="-1" role="dialog" aria-labelledby="add_classroom_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_classroom_modal">Add New Class Room </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="dropzone-form-classs-rooms" action="" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <!--<div class="col-md-4 col-xs-12">
                            <div class="form-group"> 

                                <label for="edit_classroom_type" class="col-form-label">Select Class Room Type:</label>
                                <select name="edit_classroom_type" class="form-control add_classroom_type" required>
                                    <option value="">Select Class Room Type</option>
                                    @if(isset($class_types) && sizeof($class_types) > 0) @foreach($class_types as $class_type)
                                    <option value="{{$class_type->id}}" >{{$class_type->class_type}}</option>
                                    @endforeach @endif
                                </select>
                            </div>

                        </div>--->
						<div class="col-md-6 colx-xs-12">
                            <div class="form-group">
								 <label for="classroom_title" class="col-form-label">Class Room Title</label>
                                <input type="text" class="form-control" id="new_add_classroom_title" />
                            </div>
                        </div>
                       
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="classroom_title" class="col-form-label">Number of places
									<span class="general_tooltip" >	
										<img src="{{Asset('images/tooltip.png')}}" class="general_tool_tip_icon" />
										<span class="custom_tooltiptext">how many students you can facilitate</span>												
									</span>
								</label>
                                <input type="number" class="form-control add_class_room_number_places" value="" /> 
                            </div>

                        </div>
						
                    </div>
					<div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group"> 

                                <label for="class_room_currency" class="col-form-label">Select Basic Price Currency</label>
                                
								{!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'add_class_room_currency']) !!}
                            </div>

                        </div>
						 <div class="col-md-4 col-xs-12">
                            <div class="form-group"> 

                                <label for="add_class_room_payment_type" class="col-form-label">Select Payment Type</label>
                                <select name="add_class_room_payment_type" class="form-control add_class_room_payment_type" required>
                                    <option value="">Select Payment Type</option>
									<option value="weekly">Weekly</option>
									<option value="fixed">Fixed</option>
                                </select>
                            </div>

                        </div>
						<div class="col-md-4 colx-xs-12">
                            <div class="form-group">
								 <label for="class_room_amount" class="col-form-label">Amount</label>
                                <input type="number" class="form-control" id="class_room_amount" />
                            </div>
                        </div>
                       
                       
						
                    </div>
                    <div class="row">
                        <div class="col-md-2 colx-xs-12">
                            <h5>Facilities</h5>
                        </div>
                        <div class="col-md-10 colx-xs-12">
                            <div class="form-group field-profile-emergencyname classroom_facility">

                                <ul class="unstyled centered">
                                    <?php
										$facility_array = array();

										foreach ($room_facilities as $room_facility) {
															?>
                                        <li>
                                            <input class="styled-checkbox" name="classroom_facilities[]" id="classroom-styled-checkbox-{{$room_facility->id}}" type="checkbox" value="{{$room_facility->id}}">
                                            <label for="classroom-styled-checkbox-{{$room_facility->id}}">{{$room_facility->facility_type}}</label>
                                        </li>

                                        <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    
					 <div class="row"> 
                        <div class="col-md-2 colx-xs-12">
                            <h5>Description</h5>
                        </div>
                        <div class="col-md-10 colx-xs-12">
                            <div class="form-group field-profile-emergencyname room_facility">
                                <input type="text" class="form-control" id="new_add_classroom_description" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="classroomdropzone" class="dropzone"></div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 70%;">adding classroom...</span>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="add_new_classroom_modal_btn">Add Class Room</button>
            </div>
        </div>
    </div>
</div>

<!--- end Add new room modal--->
<!--- Start edit room modal--->
<div class="modal fade " id="edit_classroom_modal" tabindex="-1" role="dialog" aria-labelledby="edit_classroom_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Update Room Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="update-dropzone-form-classrooms" action="" method="POST" enctype="multipart/form-data">
                    <div class="row">
						<input type="hidden" id="update_classroom_id" value="" />
                        <!--<div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                

                                <label for="update_classroom_type_id" class="col-form-label">Select Room Type:</label>
                                <select name="update_classroom_type_id" class="form-control update_classroom_type_id" required>
                                    <option value="">Select Room Type</option>
                                    @if(isset($class_types) && sizeof($class_types) > 0) @foreach($class_types as $class_type)
                                    <option value="{{$class_type->id}}" >{{$class_type->class_type}}</option>
                                    @endforeach @endif
                                </select>
                            </div>

                        </div>-->
						<div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="classroom_title" class="col-form-label">Class Room Title</label>
                                <input type="text" class="form-control " id="update_classroom_title" value="" /> 
                            </div>

                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="classroom_title" class="col-form-label">Number of places
									<span class="general_tooltip" >	
										<img src="{{Asset('images/tooltip.png')}}" class="general_tool_tip_icon" />
										<span class="custom_tooltiptext">how many students you can facilitate</span>												
									</span>
								</label> 
                                <input type="number" class="form-control " id="update_class_room_number_places" value="" /> 
                            </div>

                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group"> 

                                <label for="class_room_currency" class="col-form-label">Select Basic Price Currency</label>
                                
								{!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'update_class_room_currency']) !!}
                            </div>

                        </div>
						 <div class="col-md-4 col-xs-12">
                            <div class="form-group"> 

                                <label for="add_class_room_payment_type" class="col-form-label">Select Payment Type</label>
                                <select name="add_class_room_payment_type" class="form-control update_class_room_payment_type" required>
                                    <option value="">Select Payment Type</option>
									<option value="weekly">Weekly</option>
									<option value="fixed">Fixed</option>
                                </select>
                            </div>

                        </div>
						<div class="col-md-4 colx-xs-12">
                            <div class="form-group">
								 <label for="class_room_amount" class="col-form-label">Amount</label>
                                <input type="number" class="form-control" id="update_class_room_amount" />
                            </div>
                        </div>
                       
                       
						
                    </div>
					
                    <div class="row">
                        <div class="col-md-2 colx-xs-12">
                            <h5>Facilities</h5>
                        </div>
                        <div class="col-md-10 colx-xs-12">
                            <div class="form-group field-profile-emergencyname classroom_facility">

                                <ul class="unstyled centered">
                                    <?php
									$facility_array = array();

									foreach ($room_facilities as $room_facility) {
															?>
                                        <li>
                                            <input class="styled-checkbox update_classroom_facility" name="update_classroom_facilities[]" id="class-update_styled-checkbox-{{$room_facility->id}}" type="checkbox" value="{{$room_facility->id}}">
                                            <label for="class-update_styled-checkbox-{{$room_facility->id}}">{{$room_facility->facility_type}}</label>
                                        </li>

                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                   
					<div class="row">
                        <div class="col-md-2 colx-xs-12">
                            <h5>Description</h5>
                        </div>
                        <div class="col-md-10 colx-xs-12">
                            <div class="form-group field-profile-emergencyname ">
                                <input type="text" class="form-control " id="update_classroom_desc" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="edit_classroom_dropzone" class="dropzone"></div>
                        </div>
                    </div>
 
                </form>
            </div>
            <div class="modal-footer">
                <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">deleting...</span>
                <button class="btn btn-primary delete_classroom" title="Delete This Room">Delete Class Room</button>
                <button type="button" class="btn btn-primary" id="update_classroom_modal_btn">Update Class Room</button>
            </div>
        </div>
    </div>
</div>

<!--- end edit room modal--->
<!-- Edit Bed Modal--->
<div class="modal fade" id="edit_classplace_modal" tabindex="-1" role="dialog" aria-labelledby="edit_classplace_modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="edit_classplace_modal">Edit Class Place</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="hidden" id="update_place_id" />
                        <input type="hidden" id="update_place_room_id" />
                        <label for="edit_bed_type" class="col-form-label">Select Place Type:</label>
                        <select name="bed_type_id" class="form-control edit_bed_type_id" required>
                            <option value="">Select Place Type</option>
                            @if(isset($bed_types) && sizeof($bed_types) > 0) @foreach($bed_types as $bed_type)
                            <option value="{{$bed_type->id}}">{{$bed_type->bed_type}}</option>
                            @endforeach @endif
                        </select>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">deleting...</span>
                <button class="btn btn-primary delete_place" title="Delete This place">Delete Place</button>
                <button type="button" class="btn btn-primary" id="update_place_btn">Update Place</button>
            </div>
        </div>
    </div>
</div>
<!--END UPDATE BED Modal--->
<!--- Start Add new bed modal--->
<div class="modal fade" id="add_classplace_modal" tabindex="-1" role="dialog" aria-labelledby="add_classplace_modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_classplace_modal">Add New Place To </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="hidden" id="room_id_for_new_place" />
                        <label for="edit_bed_type" class="col-form-label">Select Place Type:</label>
                        <select name="bed_type_id" class="form-control add_new_bed_type_id" required>
                            <option value="">Select Place Type</option>
                            @if(isset($bed_types) && sizeof($bed_types) > 0) @foreach($bed_types as $bed_type)
                            <option value="{{$bed_type->id}}">{{$bed_type->bed_type}}</option>
                            @endforeach @endif
                        </select>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 70%;">Updating...</span>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="add_new_place_modal_btn">Add Place</button>
            </div>
        </div>
    </div>
</div>

<!--- end Add new bed modal--->

<div class="margin_top10 class_rooms_sec">
    <div class="sec_head">
        <h3 class="">Classes</h3>
    </div>

</div>
<?php


	if(isset($userObj->user_guest_rooms) && sizeof($userObj->user_guest_rooms) > 0){
	// echo '<pre>';print_r($userObj);echo '</pre>';
		foreach($userObj->user_guest_rooms as $gr_key => $guest_room){ 
			$class_room_places = \App\Property::where(["user_id" => $guest_room->user_id, "bed_guest_room_id" => $guest_room->id])
									->where(["property_type" => "24", "status" => "1", "property_status" => "paid"])->get();
			
			$newDateStart = date("Y-m-d 11:00:00", strtotime($checkinsearch));
			$newDateEnd = date("Y-m-d  10:00:00", strtotime($checkoutsearch));
			$places_available = false;
			foreach($class_room_places as $place){
				$place_id = $place->id;
											
				$availableCheck = \App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $place_id);

				//return $availableCheck;
				if (count($availableCheck) > 0) {                   
					   continue;                  
				}else{
					// Bed room available so add to cart now 
					$places_available = true;
					break;
				}

			}
			
			$total_places = count($class_room_places);
			if($guest_room->service_type != 'classroom'){
				continue;
			}
			$guest_room_type = '';
			foreach($class_types as $class_type_value){
				if($guest_room->room_type_id == $class_type_value->id){
					$guest_room_type = $class_type_value->class_type;
					break;
				}
			}
		?>
			<div class="classroom_item " id="classroom_index_{{$guest_room->id}}" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
				<div class="row" id="classroom_title">
					<div class="col-xs-8 col-md-7">
						<h4>
							<?php  

							if(isset($guest_room->room_type_id)){
								if($guest_room->room_type_id == 8 or $guest_room->room_type_id == 9){ 
									$bed_room_type_icon = 'Quadruple.png';
								}else if($guest_room->room_type_id == 7){
									$bed_room_type_icon = 'triple.png';
								}else if($guest_room->room_type_id == 4 or $guest_room->room_type_id == 5 or $guest_room->room_type_id == 6){
									$bed_room_type_icon = 'twin-share.png';
								}else if($guest_room->room_type_id == 3){
									$bed_room_type_icon = 'double.png';
								}else{
									$bed_room_type_icon = 'single-room.png';
								}
							}else{
								$bed_room_type_icon = 'single-room.png';
							}
							?>
							<img src="{{Asset('frontendassets/images/bed-room-type/'.$bed_room_type_icon)}}" class="class_room_type_icon" />
							{{$guest_room->room_name}} 
						</h4>
					</div>
					<!--col-sm-8 end-->
					<div class="col-xs-4 col-md-5">
						<div class="classroom_action_icons">
							<?php   if($loged_in_user_id == $propertyObj->user->id){
										$payment_currency = 0;
										$payment_amount = 0;
										$payment_type = '';
										if (sizeof($class_room_places) > 0) {
						
											$place_id = $class_room_places->first()->id;
											$pricing = \App\PropertyPricing::where("property_id", $place_id)->first();
											
											if(isset($pricing->payment_type)){
												$payment_type = $pricing->payment_type;
												
												$payment_currency = $pricing->currency_id;
												$payment_amount = $pricing->amount;
											}
										}
							?> 
										<button class="edit_classroom" title="Edit This Class Room" payment_currency="{{$payment_currency}}" payment_type="{{$payment_type}}" payment_amount="{{$payment_amount}}" room_facilities="{{$guest_room->room_facility}}" data-toggle="modal" data-target="#edit_classroom_modal" room_id="{{$guest_room->id}}"  room_desc="{{isset($guest_room->room_description)?$guest_room->room_description:''}}" room_title="{{$guest_room->room_name}}" number_places="{{$total_places}}">
											<img src="{{asset('frontendassets/images/edit.png')}}" />
										</button>
							<?php   }else{
										if (sizeof($class_room_places) > 0) {
											if (Auth::guest()) {
?>
												<a href="{{url('signin')}}" type="button" class="guest_room_login_btn">Login</a>
<?php 
											}else{
												//if (!Auth::user()->hasRole('Student') && !Auth::user()->hasRole('Parent')) { 
?>
													<!--<a href="{{url('user/edit')}}" id="requestbtn" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20">Complete your Profile to send request</a>-->
<?php 
												//} else {
													if($places_available){
									?>
														<button class="send_bed_request add_classsroom_to_basket" data-room_id="{{$guest_room->id}}" title="Send Request or Add to basket">Add To Basket </button>
							<?php 
													}
												//}
											}
										}
									} 
							?>
						</div>
					</div>
				</div>

				<div class="classroom_description">
					<ul class="classroom_facilities">
						<li>
							Facilities
						</li>
						<?php

							$facilities = "";
							if (isset($guest_room->room_facility) && !empty($guest_room->room_facility)) {

								$facilities = App\UserPaidService::getRoomFacilityNames($guest_room->room_facility);
							}

							// if (sizeof($facilities) > 0) {
							if (!empty($facilities)) {
								foreach ($facilities as $facility) {
									echo '<li><span class="facilities facility-'.$facility->id.'">'.$facility->id . '</span></li>';
								}
							}

						?>
					</ul>
					<p class="room_desp" style="word-wrap:break-word;">
						{{isset($guest_room->room_description)?$guest_room->room_description:''}}
					</p>
            
            <?php 
					$guest_room_images = \App\GuestRoomImages::where(["room_id" => $guest_room->id])->get();
					if(count($guest_room_images) > 0){ 
			?>
						<ul class="class_room_images">
							<?php  
							//var_dump($guest_room_images);
							foreach($guest_room_images as $room_image){
								$room_image_name = $room_image->img_name;

								$img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
								$image_size = filesize($img_path);
								//$image_size = 78000;
								//var_dump($image_size);die;  
								?>
								<li>
									<a href="<?php echo Asset('images/rentals/'.$property_id.'/rooms/'.$room_image_name); ?>" data-fancybox="images"><img src="<?php echo Asset('images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name); ?>" data-size="<?php  echo $image_size; ?>" /></a>
								</li>
								<?php 
							}
							?>
						</ul>
            <?php 	}else{
						echo '<ul class="class_room_images"></ul>';
					} 
					
					$disabledDates = array();

					if (sizeof($class_room_places) > 0) {
						
						$place_id = $class_room_places->first()->id;
						$pricing = \App\PropertyPricing::where("property_id", $place_id)->first();
						//var_dump($currencies);die;
						
				?>
                        <div class="classroom_places_sec  classroom_{{$guest_room->id}}_places">
                            <div class="margin_top10" style="position:relative;    border-bottom: 1px solid #ddd;">
                                <h3 class="sub_sec_head">Places available in this classroom </h3>
								<?php 
								if(isset($pricing->payment_type)){
									if($pricing->payment_type == 'weekly'){
										$payment_type = 'per week';
									}else{
										$payment_type = '';
									}
									
									$payment_currency = $currencies[$pricing->currency_id];
									$payment_amount = $pricing->amount;
									
								
								?>
								<span class="place_pricing">{{$payment_currency}} {{$payment_amount}}  {{$payment_type}}</span>
								<?php 
								}
								?>
							</div>

                            <div class="places_box" style="width:100%">
                               
                                    <?php

								$disabledDates= array();
								if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
									$fetchDisabledDates = App\Booking::disabledDatesForProperty($propertyObj->id);
									$disabledDates= array();
									if(sizeof($fetchDisabledDates) > 0){
										foreach($fetchDisabledDates as $dDate){
											array_push($disabledDates,$dDate->the_date);
										}
									}

								}
								
								foreach ($class_room_places as $place) {

									$bed_rental_booked = App\Property::getBookedPropertyByDates($place->id, $checkinsearch, $checkoutsearch);

									$calendarObj = App\Booking::bookingForProperty($place->id);

									list($disable_checkinDates, $disable_CheckOutDates) = App\Property::prepareRestrictedDays($calendarObj);
									if (!empty($disable_checkinDates) && !empty($disable_CheckOutDates)) {
									   $checkin_disable_dates = $checkin_disable_dates . $disable_checkinDates; //here values are in this var. but not in rentaldetail view page seen? yes, so for unpaid it has been deleted_ dont hknow because I am checking the script and logic in rental but couldn't found . So we have it here but not in unpaid, let-s add in unpaid again, then, correct_ yes okay I'll add it.good
									   $checkout_disable_dates = $checkout_disable_dates . $disable_CheckOutDates; 
									}
								
									

									?> 
                                        <div class="class_room_place">
                                            <img src="{{Asset('images/class-icon.png')}}" class="class_room_type_icon" />
											
												<div class="custom_tooltip" >	
													<img src="{{Asset('images/available-icon.png')}}" class="tool_tip_icon" />
													<span class="custom_tooltiptext">Place is available</span>												
												</div>
												
										</div>
                                            <?php  
										
											if(isset($bed_rental_booked->id) && $bed_rental_booked->id != ''){
												if (isset($bed_rental_booked->user->sname16) && !empty($bed_rental_booked->user->sname16)) {
													$dob = isset($bed_rental_booked->user->dob16) ? $bed_rental_booked->user->dob16 : "";
													$gender = isset($bed_rental_booked->user->gender16) ? $bed_rental_booked->user->gender16 : "";
													$gender16 = true;
													//sname16
													$name = isset($bed_rental_booked->user->sname16) ? $bed_rental_booked->user->sname16 : "";
													// about_yorself is correctly "Student Description"
												} else {
													$dob = isset($bed_rental_booked->user->dob) ? $bed_rental_booked->user->dob : "";
													$gender = isset($bed_rental_booked->user->gender) ? $bed_rental_booked->user->gender : "";
													$gender16 = false;
													//name
													$name = isset($bed_rental_booked->user->name) ? $bed_rental_booked->user->name : "";
												}
												if($gender16 == false){
													if($gender == "1"){
														$gender = 'Male';
													}elseif($gender == "2"){
														$gender = 'Female';
													}elseif($gender == "3"){
														$gender = 'Other';
													}else{
														$gender = $gender;
													}

												}

												$new_dob = "";
												if (isset($dob) && !empty($dob)) {
													$exp_dob = explode("/", $dob);
													if (is_array($exp_dob)) {
														$new_dob = $exp_dob[2] . "-" . $exp_dob[1] . "-" . $exp_dob[0];
													}
												}
												if (!empty($new_dob)) {
													//echo date("Y-m-d", strtotime($new_dob)) . "<br />";
													if (isset($checkinsearch) && !empty($checkinsearch)) {
														$booked_checkin_date = date("Y-m-d", strtotime($checkinsearch));
													} else {
														$booked_checkin_date = isset($bed_rental_booked->checkin) ? date("Y-m-d", strtotime($bed_rental_booked->checkin)) : "";
													}
													//echo $booked_checkin_date."<br />";
													$birthdate = new DateTime($new_dob);
													$today = new DateTime($booked_checkin_date);
													$age = $birthdate->diff($today)->y;
													echo $age." Years";
//                                                                          
												}
												$language = "";
												if (isset($bed_rental_booked->user->user_extrainfo->native_language_speaker) && !empty($bed_rental_booked->user->user_extrainfo->native_language_speaker)) {
													$language = App\Language::where("id", $bed_rental_booked->user->user_extrainfo->native_language_speaker)->first();
													$user_language = ($language->language_name)?$language->language_name:'';
												}

												echo $name.' : '.$gender.' : '.$age.' Years old'; 
											}else{
												
												if (Auth::guest()) {
												?>
                                                    <!--<a href="{{url('signin')}}" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20">Login</a>-->
                                                    <?php 
												}else{
													//if (!Auth::user()->hasRole('Student') && !Auth::user()->hasRole('Parent')) { 
												?>
														<!--<a href="{{url('user/edit')}}" id="requestbtn" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20">Complete your Profile to send request</a>-->
                                                <?php 
												//	}else{
														//if(isset($place->booking_style) && $place->booking_style == 1) {
															
														//}
													//}
												}

											}
										?>
                                           

                                <?php }  
											?>
                                
                            
							</div>
						</div>
			<?php   }else{

						if($loged_in_user_id == $propertyObj->user->id){
			?>
							<div class="room_beds_sec room_{{$guest_room->id}}_beds">
								<div class="margin_top10">
									<h3 class="sub_sec_head">Places available in this classroom</h3>
								</div>
								<table class="beds_table" style="width:100%">
									<tbody>
									</tbody>
								</table>
								<button class="add_new_place_to_classrooms" title="Add Places" data-toggle="modal" data-target="#add_classplace_modal" data-type="{{$guest_room_type}}" data-room_id="{{$guest_room->id}}">Add Places</button>

							</div>
            <?php 
						}else{
			?>
							<div>
								<div class="margin_top10">
									<h3 class="sub_sec_head">Place available in {{isset($guest_room->room_name)?$guest_room->room_name:''}} </h3>
								</div>
								<div class="add_new_bed">
									<p>No  places Availble in this room yet</p>

								</div>
							</div>
            <?php 

						}
					} ?>
				</div>
			</div>
					
   
		<?php 
		}
			if($loged_in_user_id == $propertyObj->user->id){
		?>
				<button class="add_new_classroom" title="Add New Class Room" data-toggle="modal" data-target="#add_classroom_modal">Add new class room</button>
		<?php 
			}		
	}else{
		if($loged_in_user_id == $propertyObj->user->id){
	?>
			<button class="add_new_classroom" title="Add New Class Room" data-toggle="modal" data-target="#add_classroom_modal">Add new class room</button>
	<?php 
		}
	}
	
	
	?>     