<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Smoking<i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <input type="radio" value="1" name="smoking" @if(isset($userObject->paid_service->smoking) && $userObject->paid_service->smoking == "1") checked="" @endif><label style="display:block"  class="airfcfx-search-checkbox-text"> Yes</label><br />
        <input type="radio" value="0" name="smoking" @if(isset($userObject->paid_service->smoking) && $userObject->paid_service->smoking == "0") checked="" @endif><label style="display:block"  class="airfcfx-search-checkbox-text"> No</label><br />
        <div class="errcls" id="" style="clear: both;"></div><br/>
    </div>
</div> <!--col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Time Limits (Curfew, meal and other time limits)</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="time_limits" class="form-control" rows="5" name="time_limits">{{isset($userObject->paid_service->time_limits)?$userObject->paid_service->time_limits:''}}</textarea>

            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> <!--col-xs-12 end -->
 <!-- FOR now we are using listing rules
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Rules in the Home</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="house_rules" class="form-control" rows="10" name="house_rules">{{isset($userObject->paid_service->house_rules)?$userObject->paid_service->house_rules:''}}</textarea>

            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div>col-xs-12 end -->


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Will you give Student Guests a key?<i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <input type="radio" value="1" name="key_given" @if(isset($userObject->paid_service->key_given) && $userObject->paid_service->key_given == "1") checked="" @endif><label style="display:block"  class="airfcfx-search-checkbox-text"> Yes</label><br />
        <input type="radio" value="0" name="key_given" @if(isset($userObject->paid_service->key_given) && $userObject->paid_service->key_given == "0") checked="" @endif><label style="display:block"  class="airfcfx-search-checkbox-text">No</label><br />

    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">At the Table with Student Guests?</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <select class="form-control" name="guest_table">
                <option value="">Select Option</option>
                <?php foreach ($tableGuest as $tableGuestobj) { ?>
                    <option value="{{$tableGuestobj->Code}}" @if(isset($userObject->paid_service->guest_table) && $userObject->paid_service->guest_table == $tableGuestobj->Code) selected="" @endif>{{$tableGuestobj->Code}}</option> <?php } ?>

            </select>


            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
    </div>
</div> <!--col-xs-12 end -->







<!-- FOR now we are using listing rules
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Other Rules in the House</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <textarea id="other_house_rules" class="form-control" rows="10" name="other_house_rules">{{isset($userObject->paid_service->other_house_rules)?$userObject->paid_service->other_house_rules:''}}</textarea>

            <p class="help-block help-block-error"></p>
        </div>                                </div>
</div> col-xs-12 end -->
