@include('partials.errors')

<div class="row" id="property_info_image_display" style="margin-top:50px;">
    <?php
    if (isset($userObject->paidServicePropertyImage) && sizeof($userObject->paidServicePropertyImage) > 0) {
        foreach ($userObject->paidServicePropertyImage as $obj) {
            $imgname = $obj->img_name;
            $imgURL = asset('images/schoolhouse/' . $obj->user_id . '/' . $imgname);
            ?> 
            <div class="col-md-3 listimgdiv">
                <img style="height:100px" src="{{$imgURL}}" />
                <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$obj->id" onclick="remove_paid_image(this, '{{$imgname}}','paidService')"></i>
            </div>
            <?php
        }
    }
    ?>
</div>


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Note: [Upload only images in png,jpg format], Upload Photo one at a time.</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname">
            <input type="file" class="form-control" name="property_image_paid" />
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end -->
<div class="clear"></div>
<div class="panel panel-default panel-faq" style="margin-top:30px">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#pbasics" class="collapsed" aria-expanded="false">
            <h4 class="panel-title">Property Basic Information<i class="fa fa-plus text-danger"></i>
                <span class="pull-right"></span>
            </h4>
        </a>
    </div>
    <div id="pbasics" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="panel-body" style="display: block;">
            @include('partials.schoolhouse.propertybasics')
        </div>
    </div>
</div>

<div class="panel panel-default panel-faq" style="margin-top:30px">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#facilities" class="collapsed" aria-expanded="false">
            <h4 class="panel-title">Facilities Included<i class="fa fa-plus text-danger"></i>
                <span class="pull-right"></span>
            </h4>
        </a>
    </div>
    <div id="facilities" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="panel-body" style="display: block;">
            @include('partials.schoolhouse.facilities')  
        </div>
    </div>
</div>

<div class="panel panel-default panel-faq" style="margin-top:30px">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#rules" class="collapsed" aria-expanded="false">
            <h4 class="panel-title">House Rules<i class="fa fa-plus text-danger"></i>
                <span class="pull-right"></span>
            </h4>
        </a>
    </div>
    <div id="rules" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="panel-body" style="display: block;">
            @include('partials.schoolhouse.rules')  
        </div>
    </div>
</div>
<div class="panel panel-default panel-faq" style="margin-top:30px">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#guestRooms" class="collapsed" aria-expanded="false">
            <h4 class="panel-title">Guest Rooms<i class="fa fa-plus text-danger"></i>
                <span class="pull-right"></span>
            </h4>
        </a>
    </div>
    <div id="guestRooms" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">

        <div class="col-xs-12 margin_top10" style="margin-bottom: 10px;">
            <div class="col-xs-12 col-sm-12">
                <label class="profile_label" style="margin-right: 15px;">
                    Guest Rooms that are available at your place.
                    <p>
                        Here you will add your Guest Rooms, later you will have to add all the beds in each room. Students book beds, not rooms.
                    </p>
                    <p>
                        After adding Guest Rooms you may add Guest Beds <a href="{{URL::to('rentals/add/school-house-bed')}}">by clicking here</a>.
                    </p>
                </label> 
                <a href="javascript:;" id="add_more_rooms" style="text-align: right; float: right;">
                    <img src="{{Asset('images/plus.png')}}" alt="Add More Rooms" style="max-width: 32px;" />
                </a>
            </div>
        </div>
        <div id="all_guest_rooms">
            @if(isset($userObject->user_guest_rooms) && sizeof($userObject->user_guest_rooms) > 0)
            @foreach($userObject->user_guest_rooms as $gr_key => $guest_room)
            <div>
                <hr style="border: 1px solid #ddd; width: 100%;"/>
                <div class="col-xs-12 margin_top10" style="margin-bottom: 10px;">
                    <div class="col-xs-12 col-sm-12">
                        <a href="javascript:;" onclick="removeGuestRoom(this)" style="text-align: right; float: right;">
                            <img src="{{Asset('images/minus.png')}}" alt="Remove Guest Room" style="max-width: 32px;" />
                        </a>
                    </div>
                </div>
                <div class="panel-body" >
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Room Name</label> 
                            <p>
                                Choose a name which will allow you to identify it when adding beds to it.
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-firstname required">
                                <input type="text" id="family_name" class="form-control" placeholder="Tavern with indipendent entrance." name="room_name[]" value="{{isset($guest_room->room_name)?$guest_room->room_name:''}}">
                            </div>                    
                        </div>
                    </div>
                </div>
                <div class="panel-body" >
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Room Type</label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-state required">
                                <select class="form-control" name="room_type_id[]">
                                    <option value="">Select Room Type</option>
                                    @if(isset($real_room_type) && sizeof($real_room_type) > 0)
                                    @foreach($real_room_type as $room_type)
                                    <option value="{{$room_type->id}}"  <?php if (isset($guest_room->room_type_id) && $guest_room->room_type_id == $room_type->id) { ?> selected <?php } ?>>{{$room_type->room_type}}</option>
                                    @endforeach
                                    @endif
                                </select> 
                                <p class="help-block help-block-error"></p>
                            </div>                   
                        </div>
                    </div>
                </div>
                <div class="panel-body" >
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Room Description</label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-firstname required">
                                <textarea class="form-control" name="room_description[]">{{isset($guest_room->room_description)?$guest_room->room_description:''}}</textarea>
                            </div>                    
                        </div>
                    </div>
                </div>
                <div class="panel-body" >
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Room Facilities</label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-emergencyname room_facility">
                                <?php
                                $facility_array = array();
                                if (isset($guest_room->room_facility)) {
                                    $facility_array = explode(",", $guest_room->room_facility);
                                }
                                foreach ($room_facilities as $room_facility) {
                                    ?>  
                                    <label style="display:block; float: left; width: 45%;"  class="airfcfx-search-checkbox-text">
                                        <input type="checkbox" id="host_preference" class="facility_checkbox" name="room_facility[{{$gr_key}}][]" value="{{$room_facility->id}}" <?php if (in_array($room_facility->id, $facility_array)) { ?> checked="" <?php } ?>>
                                        <span style="margin-left: 20px;">{{$room_facility->facility_type}}</span>
                                    </label>
<?php } ?>
                                <p class="help-block help-block-error"></p>
                            </div>                                
                        </div>
                    </div>
                </div>
                <div class="panel-body" >
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Student Guest(s) Toilet</label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-state required">
                                <select class="form-control" name="room_guest_toilet[]">
                                    <option value="">Select Room Toilet Facility</option>
                                    <option value="En suite - private for the room Guest(s)"  <?php if (isset($guest_room->room_guest_toilet) && $guest_room->room_guest_toilet == "En suite - private for the room Guest(s)") { ?> selected <?php } ?>>En suite - private for the room Guest(s)</option>
                                    <option value="Shared with other Guest(s) but outside the Room"  <?php if (isset($guest_room->room_guest_toilet) && $guest_room->room_guest_toilet == "Shared with other Guest(s) but outside the Room") { ?> selected <?php } ?>>Shared with other Guest(s) but outside the Room</option>
                                    <option value="Shared with the rest of the family"  <?php if (isset($guest_room->room_guest_toilet) && $guest_room->room_guest_toilet == "Shared with the rest of the family") { ?> selected <?php } ?>>Shared with the rest of the family</option>
                                </select> 
                                <p class="help-block help-block-error"></p>
                            </div>                   
                        </div>
                    </div>
                </div>
                <div class="panel-body" >
                    <div class="col-xs-12 margin_top10">
                        <div class="col-xs-12 col-sm-3 text-right">
                            <label class="profile_label">Student Guest(s) Shower/Bathroom(s)</label> 
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="form-group field-profile-state required">
                                <select class="form-control" name="room_guest_shower[]">
                                    <option value="">Select Room Shower/Bathroom Facility</option>
                                    <option value="En suite - private for the room Guest(s)"  <?php if (isset($guest_room->room_guest_shower) && $guest_room->room_guest_shower == "En suite - private for the room Guest(s)") { ?> selected <?php } ?>>En suite - private for the room Guest(s)</option>
                                    <option value="Shared with other Guest(s) but outside the Room"  <?php if (isset($guest_room->room_guest_shower) && $guest_room->room_guest_shower == "Shared with other Guest(s) but outside the Room") { ?> selected <?php } ?>>Shared with other Guest(s) but outside the Room</option>
                                    <option value="Shared with the rest of the family"  <?php if (isset($guest_room->room_guest_shower) && $guest_room->room_guest_shower == "Shared with the rest of the family") { ?> selected <?php } ?>>Shared with the rest of the family</option>
                                </select> 
                                <p class="help-block help-block-error"></p>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>

    </div>
</div>

<div class="panel panel-default panel-faq" style="margin-top:30px; display: none;">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#pextra" class="collapsed" aria-expanded="false">
            <h4 class="panel-title">Property Extra Information<i class="fa fa-plus text-danger"></i>
                <span class="pull-right"></span>
            </h4>
        </a>
    </div>
    <div id="pextra" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="panel-body" style="display: block;">
            @include('partials.schoolhouse.propertyextra')  
        </div>
    </div>
</div>
<div class="errcls" id="submiterr2" style="clear: both;"></div><br/>
<!--<div class="form-group">
    <button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20">Submit</button>
</div>
</form>-->
<div id="more_guest_rooms" style="display: none;">
    <div>
        <hr style="border: 1px solid #ddd; width: 100%;"/>
        <div class="col-xs-12 margin_top10" style="margin-bottom: 10px;">
            <div class="col-xs-12 col-sm-12">
                <a href="javascript:;" onclick="removeGuestRoom(this)" style="text-align: right; float: right;">
                    <img src="{{Asset('images/minus.png')}}" alt="Remove Guest Room" style="max-width: 32px;" />
                </a>
            </div>
        </div>
        <div class="panel-body" >
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Room Name</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-firstname required">
                        <input type="text" id="family_name" class="form-control" name="room_name[]" />
                    </div>                    
                </div>
            </div>
        </div>
        <div class="panel-body" >
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Room Type</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-state required">
                        <select class="form-control" name="room_type_id[]">
                            <option value="">Select Room Type</option>
                            @if(isset($real_room_type) && sizeof($real_room_type) > 0)
                            @foreach($real_room_type as $room_type)
                            <option value="{{$room_type->id}}"  >{{$room_type->room_type}}</option>
                            @endforeach
                            @endif
                        </select> 
                        <p class="help-block help-block-error"></p>
                    </div>                   
                </div>
            </div>
        </div>
        <div class="panel-body" >
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Room Description</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-firstname required">
                        <textarea class="form-control" name="room_description[]"></textarea>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="panel-body" >
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Room Facilities</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-emergencyname room_facility">
<?php foreach ($room_facilities as $room_facility) { ?>  
                            <label style="display:block; float: left; width: 45%;"  class="airfcfx-search-checkbox-text">
                                <input type="checkbox" id="host_preference" class="facility_checkbox" name="room_facility[][]" value="{{$room_facility->id}}" />
                                <span style="margin-left: 20px;">{{$room_facility->facility_type}}</span>
                            </label>
<?php } ?>
                        <p class="help-block help-block-error"></p>
                    </div>                                
                </div>
            </div>
        </div>
        <div class="panel-body" >
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Student Guest(s) Toilet</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-state required">
                        <select class="form-control" name="room_guest_toilet[]">
                            <option value="">Select Room Toilet Facility</option>
                            <option value="En suite - private for the room Guest(s)"  >En suite - private for the room Guest(s)</option>
                            <option value="Shared with other Guest(s) but outside the Room"  >Shared with other Guest(s) but outside the Room</option>
                            <option value="Shared with the rest of the family"  >Shared with the rest of the family</option>
                        </select> 
                        <p class="help-block help-block-error"></p>
                    </div>                   
                </div>
            </div>
        </div>
        <div class="panel-body" >
            <div class="col-xs-12 margin_top10">
                <div class="col-xs-12 col-sm-3 text-right">
                    <label class="profile_label">Student Guest(s) Shower/Bathroom(s)</label> 
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="form-group field-profile-state required">
                        <select class="form-control" name="room_guest_shower[]">
                            <option value="">Select Room Shower/Bathroom Facility</option>
                            <option value="En suite - private for the room Guest(s)"  >En suite - private for the room Guest(s)</option>
                            <option value="Shared with other Guest(s) but outside the Room"  >Shared with other Guest(s) but outside the Room</option>
                            <option value="Shared with the rest of the family"  >Shared with the rest of the family</option>
                        </select> 
                        <p class="help-block help-block-error"></p>
                    </div>                   
                </div>
            </div>
        </div>
    </div>
</div>