<!--- Start Add new Room modal--->
<div class="modal fade " id="add_room_modal" tabindex="-1" role="dialog" aria-labelledby="add_room_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_room_modal">Add New Room </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="dropzone-form" action="" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">

                                <label for="edit_bed_type" class="col-form-label">Select Room Type:</label>
                                <select name="bed_type_id" class="form-control add_new_room_type_id" required>
                                    <option value="">Select Room Type</option>
                                    @if(isset($real_room_type) && sizeof($real_room_type) > 0) @foreach($real_room_type as $room_type)
                                    <option value="{{$room_type->id}}" <?php if (isset($guest_room->room_type_id) && $guest_room->room_type_id == $room_type->id) { ?> selected
                                        <?php } ?>>{{$room_type->room_type}}</option>
                                    @endforeach @endif
                                </select>
                            </div>

                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">

                                <label for="edit_bed_type" class="col-form-label">Toilet</label>
                                <select name="bed_type_id" class="form-control add_new_room_toilet" required>
                                    <option value="">Toilet</option>
                                    <option value="En suite - private for the room Guest(s)">En suite - private for the room Guest(s)</option>
                                    <option value="Shared with other Guest(s) but outside the Room">Shared with other Guest(s) but outside the Room</option>
                                    <option value="Shared with the rest of the family">Shared with the rest of the family</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">

                                <label for="edit_bed_type" class="col-form-label">Shower</label>
                                <select name="bed_type_id" class="form-control add_new_room_shower" required>
                                    <option value="">Shower</option>
                                    <option value="En suite - private for the room Guest(s)">En suite - private for the room Guest(s)</option>
                                    <option value="Shared with other Guest(s) but outside the Room">Shared with other Guest(s) but outside the Room</option>
                                    <option value="Shared with the rest of the family">Shared with the rest of the family</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 colx-xs-12">
                            <h5>Facilities</h5>
                        </div>
                        <div class="col-md-10 colx-xs-12">
                            <div class="form-group field-profile-emergencyname room_facility">

                                <ul class="unstyled centered">
                                    <?php
														$facility_array = array();

														foreach ($room_facilities as $room_facility) {
															?>
                                        <li>
                                            <input class="styled-checkbox" name="room_facilities[]" id="styled-checkbox-{{$room_facility->id}}" type="checkbox" value="{{$room_facility->id}}">
                                            <label for="styled-checkbox-{{$room_facility->id}}">{{$room_facility->facility_type}}</label>
                                        </li>

                                        <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2 colx-xs-12">
                            <h5>Description</h5>
                        </div>
                        <div class="col-md-10 colx-xs-12">
                            <div class="form-group field-profile-emergencyname room_facility">
                                <input type="text" class="form-control" id="new_add_room_description" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="dropzone" class="dropzone"></div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 70%;">Updating...</span>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="add_new_room_modal_btn">Add Room</button>
            </div>
        </div>
    </div>
</div>

<!--- end Add new room modal--->
<!--- Start edit room modal--->
<div class="modal fade " id="edit_room_modal" tabindex="-1" role="dialog" aria-labelledby="add_room_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_room_modal">Update Room Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="update-dropzone-form" action="" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <input type="hidden" id="update_room_id" value="" />

                                <label for="edit_bed_type" class="col-form-label">Select Room Type:</label>
                                <select name="update_room_type_id" class="form-control update_room_type_id" required>
                                    <option value="">Select Room Type</option>
                                    @if(isset($real_room_type) && sizeof($real_room_type) > 0) @foreach($real_room_type as $room_type)
                                    <option value="{{$room_type->id}}" <?php if (isset($guest_room->room_type_id) && $guest_room->room_type_id == $room_type->id) { ?> selected
                                        <?php } ?>>{{$room_type->room_type}}</option>
                                    @endforeach @endif
                                </select>
                            </div>

                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">

                                <label for="edit_bed_type" class="col-form-label">Toilet</label>
                                <select name="update_room_toilet" class="form-control update_room_toilet" required>
                                    <option value="">Toilet</option>
                                    <option value="En suite - private for the room Guest(s)">En suite - private for the room Guest(s)</option>
                                    <option value="Shared with other Guest(s) but outside the Room">Shared with other Guest(s) but outside the Room</option>
                                    <option value="Shared with the rest of the family">Shared with the rest of the family</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">

                                <label for="edit_bed_type" class="col-form-label">Shower</label>
                                <select name="update_room_shower" class="form-control update_room_shower" required>
                                    <option value="">Shower</option>
                                    <option value="En suite - private for the room Guest(s)">En suite - private for the room Guest(s)</option>
                                    <option value="Shared with other Guest(s) but outside the Room">Shared with other Guest(s) but outside the Room</option>
                                    <option value="Shared with the rest of the family">Shared with the rest of the family</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 colx-xs-12">
                            <h5>Facilities</h5>
                        </div>
                        <div class="col-md-10 colx-xs-12">
                            <div class="form-group field-profile-emergencyname room_facility">

                                <ul class="unstyled centered">
                                    <?php
														$facility_array = array();

														foreach ($room_facilities as $room_facility) {
															?>
                                        <li>
                                            <input class="styled-checkbox update_room_facility" name="update_room_facilities[]" id="update_styled-checkbox-{{$room_facility->id}}" type="checkbox" value="{{$room_facility->id}}">
                                            <label for="update_styled-checkbox-{{$room_facility->id}}">{{$room_facility->facility_type}}</label>
                                        </li>

                                        <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2 colx-xs-12">
                            <h5>Description</h5>
                        </div>
                        <div class="col-md-10 colx-xs-12">
                            <div class="form-group field-profile-emergencyname room_facility">
                                <input type="text" class="form-control " id="update_room_desc" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="edit_room_dropzone" class="dropzone"></div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-12">
							<div class="room_beds_for_update">
                           
							</div>
                        </div>
                    </div>
					

                </form>
            </div>
            <div class="modal-footer">
                <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">deleting...</span>
                <button class="btn btn-primary delete_room" title="Delete This Room">Delete Room</button>
                <button type="button" class="btn btn-primary" id="update_room_modal_btn">Update Room</button>
            </div>
        </div>
    </div>
</div>

<!--- end edit room modal--->
<!-- Edit Bed Modal--->
<div class="modal fade" id="edit_bed_modal" tabindex="-1" role="dialog" aria-labelledby="edit_bed_modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="edit_bed_modal">Edit Bed</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="hidden" id="update_bed_id" />
                        <input type="hidden" id="update_bed_room_id" />
                        <label for="edit_bed_type" class="col-form-label">Select Bed Type:</label>
                        <select name="bed_type_id" class="form-control edit_bed_type_id" required>
                            <option value="">Select Bed Type</option>
                            @if(isset($bed_types) && sizeof($bed_types) > 0) @foreach($bed_types as $bed_type)
                            <option value="{{$bed_type->id}}">{{$bed_type->bed_type}}</option>
                            @endforeach @endif
                        </select>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">deleting...</span>
                <button class="btn btn-primary delete_bed" title="Delete This Bed">Delete Bed</button>
                <button type="button" class="btn btn-primary" id="update_bed_btn">Update Bed</button>
            </div>
        </div>
    </div>
</div>
<!--END UPDATE BED Modal--->
<!--- Start Add new bed modal--->
<div class="modal fade" id="add_bed_modal" tabindex="-1" role="dialog" aria-labelledby="add_bed_modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_bed_modal">Add New Bed To </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="hidden" id="room_id_for_new_bed" />
                        <label for="edit_bed_type" class="col-form-label">Select Bed Type:</label>
                        <select name="bed_type_id" class="form-control add_new_bed_type_id" required>
                            <option value="">Select Bed Type</option>
                            @if(isset($bed_types) && sizeof($bed_types) > 0) @foreach($bed_types as $bed_type)
                            <option value="{{$bed_type->id}}">{{$bed_type->bed_type}}</option>
                            @endforeach @endif
                        </select>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 70%;">Updating...</span>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="add_new_bed_modal_btn">Add Bed</button>
            </div>
        </div>
    </div>
</div>

<!--- end Add new bed modal--->

<div class="margin_top10 guest_rooms_sec">
    <div class="sec_head">
        <h3 class="">Guest Rooms</h3>
    </div>

</div>
<?php 
if(isset($userObj->user_guest_rooms) && sizeof($userObj->user_guest_rooms) > 0){
								
	foreach($userObj->user_guest_rooms as $gr_key => $guest_room){ 
		//only getting service_type = bedroom data for guestrooms these are actually bedrooms
		if($guest_room->service_type != 'bedroom'){
			continue;
		}
		$guest_room_type = '';
		foreach($real_room_type as $room_type_value){
			if($guest_room->room_type_id == $room_type_value->id){
				$guest_room_type = $room_type_value->room_type;
				break;
			}
		}
		
		$room_beds = \App\Property::where(["user_id" => $guest_room->user_id, "bed_guest_room_id" => $guest_room->id])
							->where(["property_type" => "26", "status" => "1", "property_status" => "paid"])->get();
		
		$newDateStart = date("Y-m-d 11:00:00", strtotime($checkinsearch));
		$newDateEnd = date("Y-m-d  10:00:00", strtotime($checkoutsearch));
		$beds_available = false;
		foreach($room_beds as $room_bed){
			$room_bed_id = $room_bed->id;
										
			$availableCheck = \App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $room_bed_id);

			//return $availableCheck;
			if (count($availableCheck) > 0) {                   
				   continue;                  
			}else{
				// Bed room available so add to cart now 
				$beds_available = true;
				break;
			}

		}

		$disabledDates = array();
?>
    <div class="room_item " id="room_index_{{$guest_room->id}}" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
        <div class="row" id="room_title">
            <div class="col-xs-8 col-md-7">
                <h4>
<?php  

					if(isset($guest_room->realRoomType->id)){
						if($guest_room->realRoomType->id == 8 or $guest_room->realRoomType->id == 9){ 
							$bed_room_type_icon = 'Quadruple.png';
						}else if($guest_room->realRoomType->id == 7){
							$bed_room_type_icon = 'triple.png';
						}else if($guest_room->realRoomType->id == 4 or $guest_room->realRoomType->id == 5 or $guest_room->realRoomType->id == 6){
							$bed_room_type_icon = 'twin-share.png';
						}else if($guest_room->realRoomType->id == 3){
							$bed_room_type_icon = 'double.png';
						}else{
							$bed_room_type_icon = 'single-room.png';
						}
					}else{
						$bed_room_type_icon = 'single-room.png';
					}
?>
					<img src="{{Asset('frontendassets/images/bed-room-type/'.$bed_room_type_icon)}}" class="bed_room_type_icon" />
					{{$guest_room_type}} 
				</h4>
            </div>
            <!--col-sm-8 end-->
            <div class="col-xs-4 col-md-5">
                <div class="room_action_icons">
<?php
					if($loged_in_user_id == $propertyObj->user->id){
?>
                        <button class="edit_room" title="Edit This Room" room_facilities="{{$guest_room->room_facility}}" data-toggle="modal" data-target="#edit_room_modal" room_id="{{$guest_room->id}}" room_type_id="{{$guest_room->realRoomType->id}}" room_desc="{{isset($guest_room->room_description)?$guest_room->room_description:''}}" room_toilet="{{$guest_room->room_guest_toilet}}" room_shower="{{$guest_room->room_guest_shower}}">
                            <img src="{{asset('frontendassets/images/edit.png')}}" />
                        </button>
<?php				}else{
						if (sizeof($room_beds) > 0) {
							if (Auth::guest()) {
?>
								<a href="{{url('signin')}}" type="button" class="guest_room_login_btn">Login</a>
<?php 
							}else{
                                //here we are checking this already if not student user role and ntot parent then we are asking to complete profile and we will not show add to basket button any more if user role is not stuendt or parent. This for any listing_ this is for just paid type listings because that page is only for paid listings. Strange, this should be only for unpaid unpaid is on other page let me chekc if that page is also has this ehck or not ok. this check here is unnecessary. ok i can remove now ok wait let me comment this
								//if (!Auth::user()->hasRole('Student') && !Auth::user()->hasRole('Parent')) { 
?>
								<!--<a href="{{url('user/edit')}}" id="requestbtn" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20">Complete your Profile to send request</a>-->
<?php 
								//} else {
									if($beds_available){
?>
										<button class="send_bed_request add_room_to_basket" data-room_id="{{$guest_room->id}}" title="Add to basket">Add To Basket </button>
<?php 					
									}
								//}
							}
						}
					}
?>
                </div>
            </div>
        </div>

        <div class="room_description">
            <ul class="room_facilities">
                <li>
                    Facilities
                </li>
<?php

				$facilities = "";
				if (isset($guest_room->room_facility) && !empty($guest_room->room_facility)) {

					$facilities = App\UserPaidService::getRoomFacilityNames($guest_room->room_facility);
				}

				
				if (!empty($facilities)) {
					foreach ($facilities as $facility) {
						echo '<li><span class="facilities facility-'.$facility->id.'">'.$facility->id . '</span></li>';
					}
				}

?>
            </ul>
            <p class="room_desp" style="word-wrap:break-word;">
                {{isset($guest_room->room_description)?$guest_room->room_description:''}}
            </p>
            <ul class="shower_toilets_icons">
                <li>
<?php 
					if($guest_room->room_guest_toilet =='En suite - private for the room Guest(s)'){
						$bath_icon = 'ensuite.png';
					}else{
						$bath_icon = 'ensuite-guests-only.png';
					}													
?>					
					<img src="{{Asset('frontendassets/images/bed-room-type/'.$bath_icon)}}" class="bed_room_type_icon" />
                </li>
                <li>
<?php 
					if($guest_room->room_guest_shower == 'Shared with other Guest(s) but outside the Room'){
						$shower_icon = 'ensuite-guests-only.png';
					}else{
						$shower_icon = 'ensuite-guests-only.png';
					}

?>
                    <img src="{{Asset('frontendassets/images/bed-room-type/'.$shower_icon)}}" class="bed_room_type_icon" />
                </li>
            </ul>
<?php 
			$guest_room_images = \App\GuestRoomImages::where(["room_id" => $guest_room->id])->get();
			if(count($guest_room_images) > 0){
?>
				<ul class="guest_room_images">
<?php  
				foreach($guest_room_images as $room_image){
					$room_image_name = $room_image->img_name;

					$img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
					$image_size = filesize($img_path);
					//$image_size = 10000;
					//var_dump($image_size);die;  
?>
                        <li>
                            <a href="<?php echo Asset('images/rentals/'.$property_id.'/rooms/'.$room_image_name); ?>" data-fancybox="images"><img src="<?php echo Asset('images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name); ?>" data-size="<?php  echo $image_size; ?>" /></a>
                        </li>
<?php 
				}
?>
                </ul>
<?php 		}else{
				echo '<ul class="guest_room_images"></ul>';
			}
?>
<?php
			if (sizeof($room_beds) > 0) {
?>
				<div class="room_beds_sec  room_{{$guest_room->id}}_beds">
					<div class="margin_top10" style="position:relative;    border-bottom: 1px solid #ddd;">
						<h3 class="sub_sec_head">Beds available in this room </h3>

					</div>

					<table class="beds_table" style="width:100%">
						<tbody>
<?php

						$disabledDates= array();
						if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
							$fetchDisabledDates = App\Booking::disabledDatesForProperty($propertyObj->id);
							$disabledDates= array();
							if(sizeof($fetchDisabledDates) > 0){
								foreach($fetchDisabledDates as $dDate){
									array_push($disabledDates,$dDate->the_date);
								}
							}

						}

						foreach ($room_beds as $bed) {

							$bed_rental_booked = App\Property::getBookedPropertyByDates($bed->id, $checkinsearch, $checkoutsearch);

							$calendarObj = App\Booking::bookingForProperty($bed->id);
							
							list($disable_checkinDates, $disable_CheckOutDates) = App\Property::prepareRestrictedDays($calendarObj);
							
							if (!empty($disable_checkinDates) && !empty($disable_CheckOutDates)) {
							   $checkin_disable_dates = $checkin_disable_dates . $disable_checkinDates; //here values are in this var. but not in rentaldetail view page seen? yes, so for unpaid it has been deleted_ dont hknow because I am checking the script and logic in rental but couldn't found . So we have it here but not in unpaid, let-s add in unpaid again, then, correct_ yes okay I'll add it.good
							   $checkout_disable_dates = $checkout_disable_dates . $disable_CheckOutDates;
							}
							  
							if(isset($bed->bedtype->id)){
								if($bed->bedtype->id == 8 or $bed->bedtype->id == 6 or $bed->bedtype->id == 5){ 
									$bed_icon = 'bunk-bed.png';
								}else if($bed->bedtype->id == 4){
									$bed_icon = 'futon.png';
								}else if($bed->bedtype->id == 3){
									$bed_icon = 'futon.png';
								}else if($bed->bedtype->id == 2){
									$bed_icon = 'double.png';
								}else{
									$bed_icon = 'single-bed.png';
								}
							}else{
								$bed_icon = 'single-bed.png';
							}

?>
							<tr class="bed_{{$bed->id}}">
								<td>
									<img src="{{Asset('frontendassets/images/bed-type/'.$bed_icon)}}" class="bed_room_type_icon" />
								</td>
								<td>
									<h5>{{isset($bed->bedtype->bed_type)?$bed->bedtype->bed_type:''}} Bed</h5>
								</td>
								<td style="text-align: right;">
<?php  
									if(isset($bed_rental_booked->id) && $bed_rental_booked->id != ''){
										if (isset($bed_rental_booked->user->sname16) && !empty($bed_rental_booked->user->sname16)) {
											$dob = isset($bed_rental_booked->user->dob16) ? $bed_rental_booked->user->dob16 : "";
											$gender = isset($bed_rental_booked->user->gender16) ? $bed_rental_booked->user->gender16 : "";
											$gender16 = true;
											//sname16
											$name = isset($bed_rental_booked->user->sname16) ? $bed_rental_booked->user->sname16 : "";
											// about_yorself is correctly "Student Description"
										} else {
											$dob = isset($bed_rental_booked->user->dob) ? $bed_rental_booked->user->dob : "";
											$gender = isset($bed_rental_booked->user->gender) ? $bed_rental_booked->user->gender : "";
											$gender16 = false;
											//name
											$name = isset($bed_rental_booked->user->name) ? $bed_rental_booked->user->name : "";
										}
										if($gender16 == false){
											if($gender == "1"){
												$gender = 'Male';
											}elseif($gender == "2"){
												$gender = 'Female';
											}elseif($gender == "3"){
												$gender = 'Other';
											}else{
												$gender = $gender;
											}

										}

										$new_dob = "";
										if (isset($dob) && !empty($dob)) {
											$exp_dob = explode("/", $dob);
											if (is_array($exp_dob)) {
												$new_dob = $exp_dob[2] . "-" . $exp_dob[1] . "-" . $exp_dob[0];
											}
										}
										if (!empty($new_dob)) {
											//echo date("Y-m-d", strtotime($new_dob)) . "<br />";
											if (isset($checkinsearch) && !empty($checkinsearch)) {
												$booked_checkin_date = date("Y-m-d", strtotime($checkinsearch));
											} else {
												$booked_checkin_date = isset($bed_rental_booked->checkin) ? date("Y-m-d", strtotime($bed_rental_booked->checkin)) : "";
											}
											//echo $booked_checkin_date."<br />";
											$birthdate = new DateTime($new_dob);
											$today = new DateTime($booked_checkin_date);
											$age = $birthdate->diff($today)->y;
											//echo $age." Years";
//                                                                          
										}
										$language = "";
										if (isset($bed_rental_booked->user->user_extrainfo->native_language_speaker) && !empty($bed_rental_booked->user->user_extrainfo->native_language_speaker)) {
											$language = App\Language::where("id", $bed_rental_booked->user->user_extrainfo->native_language_speaker)->first();
											$user_language = ($language->language_name)?$language->language_name:'';
										}

										
										//echo 'Already Booked';
										echo $name.' : '.$gender.' : '.$age.' Years old'; 
									}else{
										
										$newDateStart = date("Y-m-d 11:00:00", strtotime($checkinsearch));
										$newDateEnd = date("Y-m-d  10:00:00", strtotime($checkoutsearch));
										$available_dates_check = \App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $bed->id);
										if(count($available_dates_check) > 0){
											echo 'Not Availble in these dates';
										}else{
											
										
										
											if (Auth::guest()) {
?>
												<a href="{{url('signin')}}" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20">Login</a>
<?php 
											}else{
												if (!Auth::user()->hasRole('Student') && !Auth::user()->hasRole('Parent')) { 
?>
													<a href="{{url('user/edit')}}" id="requestbtn" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20">Complete your Profile to send request</a>
<?php 
												} else {
													if (isset($bed->booking_style) && $bed->booking_style == 1) {
														if($loged_in_user_id == $propertyObj->user->id){
?>
															<?php /*<button class="edit_bed" data-bed-id="{{$bed->id}}" title="Edit This Bed" data-toggle="modal" data-target="#edit_bed_modal" data-bed_id="{{$bed->id}}" data-room_id="{{$guest_room->id}}" data-bed_type_id="{{$bed->bedtype->id}}"><img src="{{asset('frontendassets/images/edit.png')}}" /></button> */ ?>
<?php 													}else{
	
															$cart_data = \App\Cartlists::select('property_id')->
															where('user_id',$loged_in_user_id)->
															where(function($whr) use ($today){
																$whr->where("start_date","<=",$today)->where("end_date",">=",$today)
																->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
															})->get();
									
															//echo '<pre>';
															$cart_ids = array();
															foreach($cart_data as $cart_item){
																$cart_ids[] = $cart_item->property_id;
															}  
															//print_r($cart_ids);
															if(!in_array($bed->id,$cart_ids)){
?>															
															<button  data-bed-id="{{$bed->id}}" id="requestbtn" class="add_to_basket btn-pad btn btn-danger full_width" title="Send Request or Add to basket">Add To Basket </button>
<?php 												        }else{
?>
															<span class="already_added">Already Added</span>
<?php
	
															}
														} 
													} else {
?>
														<button type="button" onclick="instant_book('{{$bed->id}}');" id="requestbtn" class="btn-pad btn btn-danger full_width"><b>Book Now</b></button>
<?php
													}
												}
											}
										}
									}
?>
								</td>
							</tr>

<?php 
						}  
?>
						</tbody>
					</table>
<?php 				if($loged_in_user_id == $propertyObj->user->id){
?>
                        <button class="add_new_bed_to_rooms" title="Add New Bed" data-toggle="modal" data-target="#add_bed_modal" data-type="{{$guest_room_type}}" data-room_id="{{$guest_room->id}}">Add another bed</button>
<?php 				}
?>
                </div>
<?php 		}else{
				if($loged_in_user_id == $propertyObj->user->id){
?>
					<div class="room_beds_sec room_{{$guest_room->id}}_beds">
						<div class="margin_top10">
							<h3 class="sub_sec_head">Beds available in this room</h3>
						</div>
						<table class="beds_table" style="width:100%">
							<tbody>
							</tbody>
						</table>
						<button class="add_new_bed_to_rooms" title="Add New Bed" data-toggle="modal" data-target="#add_bed_modal" data-type="{{$guest_room_type}}" data-room_id="{{$guest_room->id}}">Add new bed</button>

					</div>
<?php 
				}else{
?>
					<div>
						<div class="margin_top10">
							<h3 class="sub_sec_head">Beds available in {{isset($guest_room->room_name)?$guest_room->room_name:''}} </h3>
						</div>
						<div class="add_new_bed">
							<p>No Bed Availble in this room yet</p>

						</div>
					</div>
<?php 

				}
			}
?>
        </div>
    </div>
<?php 
	}
	if($loged_in_user_id == $propertyObj->user->id){
?>
        <button class="add_new_room" title="Add another Guest Room" data-toggle="modal" data-target="#add_room_modal">Add another room</button>
<?php 
	}
}else{
	if($loged_in_user_id == $propertyObj->user->id){
?>
        <button class="add_new_room" title="Add New Guest Room" data-toggle="modal" data-target="#add_room_modal">Add new room</button>
<?php 
	}

}
?>