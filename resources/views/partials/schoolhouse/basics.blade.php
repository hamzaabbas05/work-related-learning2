<br>
<a><h3>School House - Listing Information</h3></a>

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">School House Name</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <input type="text" id="house_name" class="form-control" name="house_name" value="{{isset($userObject->paid_service->house_name)?$userObject->paid_service->house_name:''}}" placeholder="Not your family name. Make up a commercial name for your newly added school house. It does not mean you need to be a registered company." title="Make up a commercial name for your newly added school house, it must not be your family name. It does not mean you need to be a registered company" >
        </div>                    
    </div>
</div>


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">School House Slogan - (Title of Listing)</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <input type="text" id="house_slogan" class="form-control" name="house_slogan" value="{{isset($userObject->paid_service->house_slogan)?$userObject->paid_service->house_slogan:''}}" >
        </div>                    
    </div>
</div>


<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">School House Description</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <textarea id="house_desc" class="form-control" rows="10" name="house_desc" >{{isset($userObject->paid_service->house_desc)?$userObject->paid_service->house_desc:""}}</textarea>
        </div>                    
    </div>
</div>
<hr style="border: 1px solid #DDD;"/>
<a><h3>Referral system - Member who Referred or is Assisting you</h3></a>
<p>Insert details of the person who referred you to the system or who is assisting you in the completion of your Profile and listings.</p>
@if(Session::has('referral_error'))
<div class="alert alert-danger" style="z-index: 100;">
    {{Session::get('referral_error')}}
    <a style="float: right; border: 1px solid; border-radius: 2px; padding: 0px 4px; text-decoration: none; cursor: pointer;" onclick="closeErrorMsg(this)">X</a>
</div>
@endif
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Name:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <input type="text" id="referral_name" class="form-control" name="referral_name" value="{{isset($userObject->user_referral->name)?$userObject->user_referral->name:''}}"/>
        </div>                    
    </div>
</div>
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Surname:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <input type="text" id="referral_surname" class="form-control" name="referral_surname" value="{{isset($userObject->user_referral->surname)?$userObject->user_referral->surname:''}}"/>
        </div>                    
    </div>
</div>
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Email:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <input type="email" id="referral_email" class="form-control" name="referral_email" value="{{isset($userObject->user_referral->email)?$userObject->user_referral->email:''}}"/>
        </div>                    
    </div>
</div>
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <input type="checkbox" value="1" <?php if (isset($userObject->user_referral->referral_referr_me) && $userObject->user_referral->referral_referr_me == 1) { ?> checked="" <?php } ?> name="referral_referr_me" />
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname required">
            Referred me to the system
            <p class="help-block help-block-error"></p>
        </div>                        
    </div>
</div> 
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <input type="checkbox" value="1" <?php if (isset($userObject->user_referral->is_assisting_me) && $userObject->user_referral->is_assisting_me == 1) { ?> checked="" <?php } ?> name="is_assisting_me" />
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname required">
            Is Assisting me
            <p class="help-block help-block-error"></p>
        </div>                        
    </div>
</div> 
<hr style="border: 1px solid #DDD;"/>
<a><h3>Hosting and Hospitality</h3></a>
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe yourself as a host:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            <textarea id="house_desc" class="form-control" rows="10" name="house_host_desc" >{{isset($userObject->paid_service->house_host_desc)?$userObject->paid_service->house_host_desc:""}}</textarea>
        </div>                    
    </div>
</div>
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Number of years hosting experience</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <select class="form-control" name="host_experience">
                <option value="">Select Host Experience</option>
                <?php for ($i = 1; $i <= 100; $i++) { ?> 
                    <option <?php if (isset($userObject->paid_service->host_experience) && $userObject->paid_service->host_experience == $i) { ?> selected <?php } ?>   value="{{$i}}">{{$i}}</option>


                <?php } ?>

            </select>


            <p class="help-block help-block-error"></p>
        </div>                            <!--p class="margin_top_5 text_gray1">We won't share your private email address with other Holidaysiles users.</p-->
    </div>
</div> <!--col-xs-12 end -->



<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Will Host</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <?php foreach ($hostPreference as $hostPreferenceobj) { ?>  
                <input type="checkbox" id="host_preference" name="host_preference[]" value="{{$hostPreferenceobj->Code}}" <?php if (in_array($hostPreferenceobj->Code, $hostPreferArray)) { ?> checked="" <?php } ?>>
                <label style="display:block"  class="airfcfx-search-checkbox-text">{{$hostPreferenceobj->Code}}</label>
                <br />
            <?php } ?>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end -->

<a><h3>Teaching and Conversation skills</h3></a>
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">You may select more than one:</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <?php foreach ($teachingType as $teachingTypeobj) { ?>  
                <input type="checkbox" id="host_preference" name="teaching_skills[]" value="{{$teachingTypeobj->Code}}" <?php if (in_array($teachingTypeobj->Code, $teachingSkillArray)) { ?> checked="" <?php } ?>>
                <label style="display:block"  class="airfcfx-search-checkbox-text">{{$teachingTypeobj->Code}}</label>
                <br />
            <?php } ?>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end -->
<!--
<a><h3>Academic qualifications</h3></a> 

RADIOUS OPTIONS NOT CHECKBOXES

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Highest level achieved</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-emergencyname ">
            <php foreach ($teachingType as $teachingTypeobj) { ?>  
                <input type="checkbox" id="host_preference" name="host_preference[]" value="{{$teachingTypeobj->Code}}" 
                    
                    <php if (in_array($teachingTypeobj->Code, $teachingTypeArray)) { ?> checked="" </php } ?>>

                <label style="display:block"  class="airfcfx-search-checkbox-text">{{$teachingTypeobj->Code}}</label>
                <br />
            </php>
            <p class="help-block help-block-error"></p>
        </div>                                
    </div>
</div> <!--col-xs-12 end -->


<a><h3>School House - Legal and Property Address</h3></a>

<div class="col-xs-12 margin_top10" id="" style="">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="padding0 profile_label">School House Address 
        </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class=" margin_top10 ">
            <div class="col-xs-12 col-sm-12" style="padding:0px;">
                <label class="profile_label">Legal Address - Start typing, add postal code, and select from list</label>       
                <div class="form-group field-shippingaddress-address1">
                    <input type="text" id="houseaddress" class="form-control margin_bottom10" required="" name="houseaddress" value="{{isset($userObject->paid_service->houseaddress)?$userObject->paid_service->houseaddress:''}}" >

                    <p class="help-block help-block-error"></p>
                </div>    <div class="errcls" id="houseaddresserr" style="clear: both;"></div><br/>                       
            </div>

        </div>


        <div class="margin_top10">                                    
            <label class="profile_label">Street Address Number</label>
            <div class="form-group field-shippingaddress-city">

                <input type="text" id="street_numberh" class="form-control margin_bottom10"  required=""  name="street_numberh" value="{{isset($userObject->paid_service->street_numberh)?$userObject->paid_service->street_numberh:''}}" >

                <p class="help-block help-block-error"></p><div class="errcls" id="street_numberherr" style="clear: both;"></div><br/> 
            </div>                                     
        </div>

        <div class=" margin_top10">                                    
            <label class="profile_label">Street Address Name</label>  
            <div class="form-group field-shippingaddress-state">

                <input type="text" id="routeh" class="form-control margin_bottom10" name="routeh" required=""  readonly="" value="{{isset($userObject->paid_service->routeh)?$userObject->paid_service->routeh:''}}" >

                <p class="help-block help-block-error"></p><div class="errcls" id="routeherr" style="clear: both;"></div><br/> 
            </div>                                   
        </div> <!--col-xs-12 end -->

        <div class=" margin_top10">                                    
            <label class="profile_label">Town/City</label>
            <div class="form-group field-shippingaddress-zipcode">

                <input type="text" id="localityh" readonly="" class="form-control margin_bottom10" name="localityh" value="{{isset($userObject->paid_service->localityh)?$userObject->paid_service->localityh:''}}"  >

                <p class="help-block help-block-error"></p><div class="errcls" id="localityherr" style="clear: both;"></div><br/> 
            </div>                                     
        </div>

        <div class=" margin_top10">                                    
            <label class="profile_label">State/Region</label>
            <div class="form-group field-shippingaddress-zipcode">

                <input type="text" id="administrative_area_level_1h"  readonly="" class="form-control margin_bottom10" name="administrative_area_level_1h" value="{{isset($userObject->paid_service->administrative_area_level_1h)?$userObject->paid_service->administrative_area_level_1h:''}}"  >

                <p class="help-block help-block-error"></p><div class="errcls" id="administrative_area_level_1herr" style="clear: both;"></div><br/> 
            </div>                                     
        </div>


        <div class=" margin_top10">                                    
            <label class="profile_label">ZIP / Postal Code</label>
            <div class="form-group field-shippingaddress-zipcode">

                <input type="text"  required="" id="postal_codeh" class="form-control margin_bottom10" name="postal_codeh" value="{{isset($userObject->paid_service->postal_codeh)?$userObject->paid_service->postal_codeh:''}}"  >

                <p class="help-block help-block-error"></p><div class="errcls" id="postal_codeherr" style="clear: both;"></div><br/> 
            </div>                                     
        </div>
        <div class=" margin_top10">                                    
            <label class="profile_label">Country</label>
            <div class="form-group field-shippingaddress-zipcode">

                <input type="text" id="countryh" readonly=""  required="" class="form-control margin_bottom10" name="countryh" value="{{isset($userObject->paid_service->countryh)?$userObject->paid_service->countryh:''}}"  >

                <p class="help-block help-block-error"></p><div class="errcls" id="countryherr" style="clear: both;"></div><br/> 
            </div>                                     
        </div>                            
    </div>
</div> <!-- Address End -->

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">

        <input type="checkbox" <?php if (isset($userObject->paid_service->house_property_differ) && $userObject->paid_service->house_property_differ == 1) { ?> Checked <?php } ?> id="house_property_differ" name="house_property_differ" onchange="showpropertyaddress(this)">
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname ">
            Check If SchoolHouse and Property Address are different.
        </div>                        
    </div>
</div> <!--col-xs-12 end -->

<div class="col-xs-12 margin_top10" id="property_address" style="<?php if (isset($userObject->paid_service->house_property_differ) && $userObject->paid_service->house_property_differ == 1) { ?> display:block; <?php } else { ?> display:none; <?php } ?>">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="padding0 profile_label">Property Address 
        </label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class=" margin_top10 ">
            <div class="col-xs-12 col-sm-12" style="padding:0px;">
                <label class="profile_label">Property Address - Start typing, add postal code, and select from list</label>       
                <div class="form-group field-shippingaddress-address1">
                    <input type="text"  required="" id="propertyaddress" class="form-control margin_bottom10" name="propertyaddress" value="{{isset($userObject->paid_service->propertyaddress)?$userObject->paid_service->propertyaddress:''}}">

                    <p class="help-block help-block-error"></p>
                </div>    <div class="errcls" id="propertyaddresserr" style="clear: both;"></div><br/>                       
            </div>

        </div>


        <div class="margin_top10">                                    
            <label class="profile_label">Street Address Number</label>
            <div class="form-group field-shippingaddress-city">

                <input type="text" required="" id="street_numberprop" class="form-control margin_bottom10" name="street_numberp" value="{{isset($userObject->paid_service->street_numberp)?$userObject->paid_service->street_numberp:''}}">

                <p class="help-block help-block-error"></p><div class="errcls" id="street_numberperr" style="clear: both;"></div><br/> 
            </div>                                     
        </div>

        <div class=" margin_top10">                                    
            <label class="profile_label">Street Address Name</label>  
            <div class="form-group field-shippingaddress-state">

                <input type="text" readonly="" id="routeprop" class="form-control margin_bottom10" name="routep" value="{{isset($userObject->paid_service->routep)?$userObject->paid_service->routep:''}}">

                <p class="help-block help-block-error"></p><div class="errcls" id="routeperr" style="clear: both;"></div><br/> 
            </div>                                   
        </div> <!--col-xs-12 end -->

        <div class=" margin_top10">                                    
            <label class="profile_label">Town/City</label>
            <div class="form-group field-shippingaddress-zipcode">

                <input type="text" readonly="" id="localityprop" class="form-control margin_bottom10" name="localityp" value="{{isset($userObject->paid_service->localityp)?$userObject->paid_service->localityp:''}}"  >

                <p class="help-block help-block-error"></p><div class="errcls" id="localityperr" style="clear: both;"></div><br/> 
            </div>                                     
        </div>

        <div class=" margin_top10">                                    
            <label class="profile_label">State/Region</label>
            <div class="form-group field-shippingaddress-zipcode">

                <input type="text" readonly="" id="administrative_area_level_1prop" class="form-control margin_bottom10" name="administrative_area_level_1p" value="{{isset($userObject->paid_service->administrative_area_level_1p)?$userObject->paid_service->administrative_area_level_1p:''}}"  >

                <p class="help-block help-block-error"></p><div class="errcls" id="administrative_area_level_1perr" style="clear: both;"></div><br/> 
            </div>                                     
        </div>


        <div class=" margin_top10">                                    
            <label class="profile_label">ZIP / Postal Code</label>
            <div class="form-group field-shippingaddress-zipcode">

                <input type="text" required=""  id="postal_codeprop" class="form-control margin_bottom10" name="postal_codep" value="{{isset($userObject->paid_service->postal_codep)?$userObject->paid_service->postal_codep:''}}"  >

                <p class="help-block help-block-error"></p><div class="errcls" id="postal_codeperr" style="clear: both;"></div><br/> 
            </div>                                     
        </div>
        <div class=" margin_top10">                                    
            <label class="profile_label">Country</label>
            <div class="form-group field-shippingaddress-zipcode">

                <input type="text" readonly=""  required=""  id="countryprop" class="form-control margin_bottom10" name="countryp" value="{{isset($userObject->paid_service->countryp)?$userObject->paid_service->countryp:''}}"  >

                <p class="help-block help-block-error"></p><div class="errcls" id="countryperr" style="clear: both;"></div><br/> 
            </div>                                     
        </div>                            
    </div>
</div> <!-- Address End -->

<input type="hidden" name="houselatbox" id="houselatbox" value="{{isset($userObject->paid_service->houselatbox)?$userObject->paid_service->houselatbox:''}}"> 
<input type="hidden" name="houselonbox" id="houselonbox" value="{{isset($userObject->paid_service->houselonbox)?$userObject->paid_service->houselonbox:''}}"> 
<input type="hidden" name="proplatbox" id="proplatbox" value="{{isset($userObject->paid_service->proplatbox)?$userObject->paid_service->proplatbox:''}}"> 
<input type="hidden" name="proplonbox" id="proplonbox" value="{{isset($userObject->paid_service->proplonbox)?$userObject->paid_service->proplonbox:''}}"> 
<input type="hidden" name="paid_services_id" id="paid_services_id" value="{{isset($userObject->paid_service->id)?$userObject->paid_service->id:''}}"> 




<div class="errcls" id="submiterr1" style="clear: both;"></div><br/>
<!--<div class="form-group">
    <button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20" onclick="return edit_schoolhouse_basic();">Submit</button>
</div>-->


