<div class="row">

    <?php
    $s = 0;
    foreach ($searchPropertyObj as $property) {
        $id = "carousel-example-generic" . $s;
        if ($property->property_status == "paid") {
            $rental_url = "rentals_paid";
        } else {
            $rental_url = "rentals";
        }
        ?>
        <div class="col-xs-12 col-sm-6 margin_top10">
            <div id="{{$id}}" class="carousel slide" data-ride="carousel">
                @if (sizeof($property->propertyimages) > 0)
                <div class="carousel-inner" role="listbox" onmouseover="showme({{$s}})" onmouseout="hideme({{$s}})" >    
                    <?php
                    $i = 1;
                    if (sizeof($property->propertyimages) > 0) {
                        foreach ($property->propertyimages as $img) {
                            $class = "item bg_img";
                            if ($i == 1) {
                                $class = "item bg_img active";
                            }
                            ?>
                            <a href='{{url("view/".$rental_url."/$property->id")}}' target="_blank" title="{{$img->img_name}}"
                               class="{{$class}}" style="height:250px;width:370px;background-image:url('{{asset('images/rentals/'.$img->property_id.'/'.$img->img_name)}}');">
                            </a>
                            <?php
                            $i++;
                        }
                    } else {
                        ?>
                        <a href='{{url("view/".$rental_url."/$property->id")}}' target="_blank" title=""
                           class="item bg_img active" style="height:250px;width:370px;background-image:url('{{asset('images/no_prop_image.png')}}');">
                        </a>
                    <?php } ?>
                </div>
                @else
                <div class="carousel-inner" role="listbox" onmouseover="showme(0)" onmouseout="hideme(0)" >
                    <a href='{{url("view/".$rental_url."/$property->id")}}' target="_blank" title="no image found"
                       class="item bg_img active" style="height:250px;width:370px;background-image:url('{{asset('images/no_prop_image.png')}}');">
                    </a>
                </div>
                @endif
                <!-- Controls -->
                <?php
                if ($property->propertyimages->count() > 1) {
                    $href = "#carousel-example-generic" . $s;
                    ?> 
                    <a class="left carousel-control" href="{{$href}}" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="{{$href}}" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a><?php } ?>
                <?php
                $imgName = asset('images/noimage/' . $profile_noimage);
                if ($property->user->profile_img_name != "" && !is_null($property->user->profile_img_name)) {
                    $imgName = asset('images/profile/' . $property->user->profile_img_name);
                }
                ?>

                <a href='{{url("view/".$rental_url."/$property->id")}}' target="_blank" title="{{$property->user->name}}">
                    <div class="bg_img1" style="margin-right:50px;height:56;width:56;background-image:url('{{$imgName}}');"></div>
                </a> 
                <!-- Wish List -->
                @if (Auth::guest())
                <a href="{{url('signup')}}">
                    <div class="favorite" style="margin-right:55px;" ><i class="fa fa-heart-o"></i><i class="fa fa-heart fav_bg"></i></div>
                </a>
                @else
                <div class="favorite" style="margin-right:55px;" data-toggle="modal" data-target="#myModal" onclick="show_list_popup(event,<?php echo $property->id; ?>)">
                    <?php $st = 'style="background-color: #990029"'; ?>
                    <i class="fa fa-heart-o" <?php
                    if (in_array($property->id, $wishProperty)) {
                        echo $st;
                    }
                    ?> ></i><i class="fa fa-heart fav_bg"></i>
                </div>
                @endif
                <div class="favorite" style="margin-right:0px; left: 30px; width: 10%;">
                    @if($property->property_status == "paid")
                    <img src="{{URL::Asset('images/School_icon.png')}}" style="width: 35px;" alt="" />
                    @else
                    <img src="{{URL::Asset('images/work.png')}}" style="width: 35px;" alt="" />
                    @endif
                </div>
                <!-- Wish List -->
            </div><!--carousel-example-generic end-->

            <!-- Property Name -->
            <a href='{{url("view/".$rental_url."/$property->id")}}' target="_blank" title="{{$property->exptitle}}"><p class="airfcfx-search-listing-name margin_top10 margin_left10 fa-1x text_gray1">{{$property->exptitle}}</p>
            </a>

            <a href='{{url("view/".$rental_url."/$property->id")}}' target="_blank" title="{{$property->exptitle}}">
                <p class="small margin_left10 text_gray1"><b>{{$property->description}}</b>

                    <?php
                    $maxNumberOfStars = 5; // Define the maximum number of stars possible.
                    $totalRating = 0;
                    $total_count = 0;
                    if ($property->reviews->count() > 0) {
                        $totalRating = $property->reviews->sum('rateVal');
                        $total_count = $property->reviews->count();
                    }
                    $total_rating = 0;
                    //if ($propertObj->reviews->count() > 0) {
                    if ($total_count > 0) {
                        $total_rating = $totalRating / $total_count;
                        $total_rating = ceil($total_rating);
                    }
//                    $maxNumberOfStars = 5; // Define the maximum number of stars possible.
//                    $totalRating = $property->reviews->sum('rateVal'); // Calculate the total number of ratings.
//                    $avg = 0;
//                    if ($property->reviews->count() > 0) {
//                        $avg = (int) ($totalRating / $property->reviews->count()) * $maxNumberOfStars;
//                    }
//
//                    $nonstar = $maxNumberOfStars - $avg;
                    ?>

                    <span class="text-warning">
                        <?php for ($k = 1; $k <= $total_rating; $k++) { ?> 
                            <i class="fa fa-star"></i>
                        <?php } ?>
                    </span>
                    {{$property->reviews->count()}} review
                </p>
                @if ($property->property_status == "paid")
                <p>{{isset($property->pricing->amount)?"EUR ".$property->pricing->amount:''}}</p>
                @endif
            </a>
        </div><!--col-sm-6 end-->

        <?php
        $s++;
    }
    ?>

</div><!--row end-->