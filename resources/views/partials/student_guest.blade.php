<div class="airfcfx-panel panel panel-default">
    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">Student Guest (Over 18)</h3>
    </div>
    <div class="airfcfx-panel-padding panel-body">
        <div class="row ">
            <div class="airfcfx-panel-padding panel-body">

                @include('partials.student-profile-common18')
                @include('partials.address.student18_residence')
                <div class="col-xs-12 margin_top10">
                    <div class="col-xs-12 col-sm-3 text-right">

                        <input type="checkbox" <?php if ($userObject->s_res_dom_same == 0) { ?> checked <?php } ?> id="s_res_dom_same" name="s_res_dom_same" onchange="showstudentDomicile(this)">
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="form-group field-profile-firstname required">

                            Check If Residency and Domicile Addresses are different

                            <p class="help-block help-block-error"></p>
                        </div>                        </div>
                </div> <!--col-xs-12 end -->
                <?php
                $dstyle = 'display:none';
                if ($userObject->s_res_dom_same == 0) {
                    $dstyle = 'display:block';
                }
                ?>
                <div class="col-xs-12 margin_top10" id="s18domicilediv" style="{{$dstyle}}">
                    <div class="col-xs-12 col-sm-3 text-right">
                        <label class="padding0 profile_label">Domicile Address 
                        </label> 
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        @include('partials.address.student18_domicile')
                        
                    </div>
                </div> <!-- Address End -->
            </div>
        </div>
    </div>
</div>
<div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">School Info</h3>
        <br>
        <p>Select from the list provided herein, if your school is not in the list, contact us via the email on the footer.</P>
        <p>If you don't know who your School Tutor is, ask your School.</P>
        <p>Your School Tutor may temporarily be the Person travelling with you.</P>
    </div>
    <div class="airfcfx-panel-padding panel-body">
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">School Name</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">



                    <select id="school_name" name="school_name" class="form-control"  > 
                        <option value="">Select school</option>
                        <?php foreach ($schools as $sc) { ?> <option <?php if ($sc->id == $userObject->school_name) { ?> selected <?php } ?> value="{{$sc->id}}">{{$sc->name}}</option><?php } ?>
                    </select>




                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="school_nameerr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->


        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">class letter</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_letter" class="form-control" name="class_letter" value="{{$userObject->class_letter}}"  >

                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="class_lettererr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->


        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">class number</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_number" class="form-control" name="class_number" value="{{$userObject->class_number}}"  >

                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->

        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">matricule/student number</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="student_number18" class="form-control" name="student_number18" value="{{$userObject->student_number}}"  placeholder='"204858" or "we do not have one at our school"'>

                    <p class="help-block help-block-error"></p>
                </div>            <div class="errcls" id="student_number18err" style="clear: both;"></div><br/>                 </div>
        </div>

        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">School Tutor - Name</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_number" class="form-control" name="tutor_name" value="{{isset($userObject->tutor_name)?$userObject->tutor_name:''}}"  >

                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">School Tutor - Place of Birth</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_number" class="form-control" name="tutor_pob" value="{{isset($userObject->tutor_pob)?$userObject->tutor_pob:''}}"  >

                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">School Tutor - Surname</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_number" class="form-control" name="tutor_surname" value="{{isset($userObject->tutor_surname)?$userObject->tutor_surname:''}}"  >

                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">School Tutor - Personal Tax identification number</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_number" class="form-control" name="tutor_pan" value="{{isset($userObject->tutor_pan)?$userObject->tutor_pan:''}}"  >

                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">School Tutor - e-mail</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_number" class="form-control" name="tutor_email" value="{{isset($userObject->tutor_email)?$userObject->tutor_email:''}}"  >

                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">School Tutor - Telephone Number</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_number" class="form-control" name="tutor_phone" value="{{isset($userObject->tutor_phone)?$userObject->tutor_phone:''}}"  >

                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->
        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">School Tutor - Emergency telephone Number</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="class_number" class="form-control" name="tutor_emergency_phone" value="{{isset($userObject->tutor_emergency_phone)?$userObject->tutor_emergency_phone:''}}"  >

                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->


    </div>
</div>
<div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
        <h3 class="airfcfx-panel-title panel-title">In case of group departure</h3>
    </div>
    <div class="airfcfx-panel-padding panel-body">

        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Select your Group</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">
                    <select id="secondary_group_id" class="form-control" name="secondary_group_id">
                        <option value="">-- Select Group --</option>
                        @if(isset($groups) && sizeof($groups) > 0)
                        @foreach($groups as $group)
                        <?php
                        $group_in = isset($group->group_date_in) ? date("Y-m-d", strtotime($group->group_date_in)) : "";
                        $current_date = date("Y-m-d");
                        if ($group_in > $current_date) {
                            ?>
                            <option value="{{$group->id}}" @if(isset($userObject->secondary_group_id) && $userObject->secondary_group_id == $group->id) selected="" @endif>{{$group->group_name}}</option>
                        <?php } ?>
                        @endforeach
                        @endif
                    </select>
                    <p class="help-block help-block-error"></p>
                </div>         
                <div class="errcls" id="secondary_group_iderr" style="clear: both;"></div><br/>
            </div>
        </div>


        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Travelling with you - Name and Surname</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="text" id="school_tutor_name" class="form-control" name="school_tutor_name" value="{{$userObject->school_tutor_name}}"  >

                    <p class="help-block help-block-error"></p>
                </div>         <div class="errcls" id="school_tutor_nameerr" style="clear: both;"></div><br/>                    </div>
        </div> <!--col-xs-12 end -->

        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Travelling with you - Email</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="email" id="school_tutor_email" class="form-control" name="school_tutor_email" value="{{$userObject->school_tutor_email}}"  >

                    <p class="help-block help-block-error"></p>
                </div>                   <div class="errcls" id="school_tutor_emailerr" style="clear: both;"></div><br/>         </div>
        </div> <!--col-xs-12 end -->

        <div class="col-xs-12 margin_top10">
            <div class="col-xs-12 col-sm-3 text-right">
                <label class="profile_label">Travelling with you - Phone Number</label> 
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="form-group field-profile-school">

                    <input type="number" id="school_tutor_number" class="form-control" name="school_tutor_number" value="{{$userObject->school_tutor_number}}"  >

                    <p class="help-block help-block-error"></p>
                </div>     <div class="errcls" id="school_tutor_numbererr" style="clear: both;"></div><br/>                        </div>
        </div> <!--col-xs-12 end -->
    </div>
</div>