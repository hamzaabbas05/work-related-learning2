<div class="profile_head">
	<div class="container">    
    	<ul class="profile_head_menu list-unstyled">
		<li {!! (Request::is('user/dashboard') ? 'class="active"' : '') !!}><a href="{{URL::to('user/dashboard')}}">Dashboard</a></li>
        <li {!! (Request::is('user/messages') || Request::is('user/messages/*')? 'class="active"' : '') !!}><a href="{{URL::to('user/messages')}}">Inbox</a></li>
        <li {!! (Request::is('user/listing/rentals/*')  || Request::is('user/reservations') || Request::is('user/reservations/*') ? 'class="active"' : '') !!}><a href="{{URL::to('user/listing/rentals/active')}}">Your Listing</a></li>
        <li {!! (Request::is('user/trips') ? 'class="active"' : '') !!}><a href="{{URL::to('user/trips')}}">Your Trips</a></li>
        <li {!! (Request::is('user/edit') || Request::is('user/trust')|| Request::is('user/reviews') ? 'class="active"' : '') !!}><a href="{{URL::to('user/edit')}}">Profile</a></li>
        <li {!! (Request::is('user/changepassword') || Request::is('request/booking/*') || Request::is('request/booking-multiple/*') || Request::is('user/transactions/*')? 'class="active"' : '') !!}><a href="{{URL::to('user/changepassword')}}">Account</a></li>   
         <li {!! (Request::is('user/wishlist') || Request::is('user/wishlist/*')? 'class="active"' : '') !!}><a href="{{URL::to('user/wishlist')}}">Wishlists</a></li>
<li {!! (Request::is('user/addreview') || Request::is('user/reviews/*')? 'class="active"' : '') !!}><a href="{{URL::to('user/reviews/aboutyou')}}">Reviews</a></li>
              </ul>
    </div> <!--container end -->
</div> <!--profile_head end -->