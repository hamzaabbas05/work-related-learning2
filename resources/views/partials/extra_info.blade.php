@inject('country', 'App\Country')
<?php
$countries = $country->getall();
?>
<div class="col-xs-12 margin_top10" style="display: none;">
    <div class="col-xs-12 col-sm-3 text-right">

        <label class="profile_label">Add Information for </label> 
    </div>

    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname required">

            <button  style="{{$orgbtnstyle}}" id="orgbtn" class="btn btn-success" type="button" onclick="showrelevantdiv(4)">Organization</button>
            <button style="{{$stubtnstyle}}" id="stubtn" class="btn btn-info" type="button" onclick="showrelevantdiv(6)">Over 18 High school Student</button>
            <button style="{{$parbtnstyle}}"  id="parbtn" class="btn btn-warning" type="button" onclick="showrelevantdiv(5)">Parent</button>

   <!-- <select onchange="showrelevantdiv(this.value)" class="form-control" style="width:auto;" id="userrole" name="userrole" required  >
                                  <option value="" >Select</option>
                                  <option <?php if ($roleId == 4) { ?> selected <?php } ?> value="4">Organization</option>
                                  <option <?php if ($roleId == 5) { ?> selected <?php } ?> value="5">Parent</option>
                                  <option <?php if ($roleId == 6) { ?> selected <?php } ?> value="6">Student</option>
                        </select> -->

        </div>         <!--  <div class="errcls" id="userroleerr" style="clear: both;"></div><br/> -->


    </div>
</div> <!--col-xs-12 end -->                

<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">NIN - National Insurance Number (SSN in the USA, Codice Fiscale in Italy)</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-firstname required">
            <input type="text" id="nin18" class="form-control" name="NIN18" value="{{$userObject->NIN18}}" maxlength="30" placeholder="National Insurance Number">
            <p class="help-block help-block-error"></p>
        </div>                    
        <div class="errcls" id="nin18err" style="clear: both;"></div><br/>
    </div>
</div>
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Second Citizenship</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">

            {!! Form::select('second_citizenship', $countries, isset($user_extrainfo->second_citizenship)?$user_extrainfo->second_citizenship:'', ['class'=>'form-control', 'name' => 'second_citizenship', 'id' => 'second_citizenship']) !!}

            <p class="help-block help-block-error"></p>
        </div>
        <div class="errcls" id="second_countryerr" style="clear: both;"></div><br/>
    </div>
</div> 
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">I am Native speaker of</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">

            <select name="native_language_speaker" class="form-control">
                <option value="">Select Language</option>
                @if(sizeof($languages) > 0)
                @foreach($languages as $language)
                <option value="{{$language->id}}" @if(isset($user_extrainfo->native_language_speaker) && $user_extrainfo->native_language_speaker == $language->id) selected="" @endif>{{$language->language_name}}</option>
                @endforeach
                @endif
            </select>

            <p class="help-block help-block-error"></p>
        </div>
        <div class="errcls" id="native_error" style="clear: both;"></div><br/>
    </div>
</div> 
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">My second language</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">

            <select name="second_language_speaker" class="form-control">
                <option value="">Select Language</option>
                @if(sizeof($languages) > 0)
                @foreach($languages as $language)
                <option value="{{$language->id}}" @if(isset($user_extrainfo->second_language_speaker) && $user_extrainfo->second_language_speaker == $language->id) selected="" @endif>{{$language->language_name}}</option>
                @endforeach
                @endif
            </select>

            <p class="help-block help-block-error"></p>
        </div>
        <div class="errcls" id="second_language_error" style="clear: both;"></div><br/>
    </div>
</div> 
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">My third language</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">

            <select name="third_language_speaker" class="form-control">
                <option value="">Select Language</option>
                @if(sizeof($languages) > 0)
                @foreach($languages as $language)
                <option value="{{$language->id}}" @if(isset($user_extrainfo->third_language_speaker) && $user_extrainfo->third_language_speaker == $language->id) selected="" @endif>{{$language->language_name}}</option>
                @endforeach
                @endif
            </select>

            <p class="help-block help-block-error"></p>
        </div>
        <div class="errcls" id="third_language_error" style="clear: both;"></div><br/>
    </div>
</div> 
<div class="col-xs-12 margin_top10">
    <div class="col-xs-12 col-sm-3 text-right">
        <label class="profile_label">Describe how well you speak other Languages</label> 
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="form-group field-profile-state required">
            <textarea name="description_more_languages" class="form-control">{!! isset($user_extrainfo->description_more_languages)?$user_extrainfo->description_more_languages:'' !!}</textarea>
            <p class="help-block help-block-error"></p>
        </div>
        <div class="errcls" id="more_language_error" style="clear: both;"></div><br/>
    </div>
</div> 
<br>
<div style="display: none;">
    <h3>Member Assisting You</h3>
    <br>
    <p>The Person assist you in registering and adding your listings.</p>
    <br>
    <div class="col-xs-12 margin_top10">
        <div class="col-xs-12 col-sm-3 text-right">
            <label class="profile_label">Member Assisting You Name</label> 
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="form-group field-profile-state required">
                <input type="text" name="member_assisting_you_name" class="form-control" value="{{isset($user_extrainfo->member_assisting_you_name)?$user_extrainfo->member_assisting_you_name:''}}"/>
                <p class="help-block help-block-error"></p>
            </div>
            <div class="errcls" id="agent_name_error" style="clear: both;"></div><br/>
        </div>
    </div> 
    <div class="col-xs-12 margin_top10">
        <div class="col-xs-12 col-sm-3 text-right">
            <label class="profile_label">Member Assisting You Surname</label> 
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="form-group field-profile-state required">
                <input type="text" name="member_assisting_you_surname" class="form-control" value="{{isset($user_extrainfo->member_assisting_you_surname)?$user_extrainfo->member_assisting_you_surname:''}}"/>
                <p class="help-block help-block-error"></p>
            </div>
            <div class="errcls" id="agent_surname_error" style="clear: both;"></div><br/>
        </div>
    </div> 
    <div class="col-xs-12 margin_top10">
        <div class="col-xs-12 col-sm-3 text-right">
            <label class="profile_label">Member Assisting You Email</label> 
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="form-group field-profile-state required">
                <input type="email" name="member_assisting_you_email" class="form-control" value="{{isset($user_extrainfo->member_assisting_you_email)?$user_extrainfo->member_assisting_you_email:''}}"/>
                <p class="help-block help-block-error"></p>
            </div>
            <div class="errcls" id="agent_email_error" style="clear: both;"></div><br/>
        </div>
    </div>
</div>