<?php
if($_SERVER['REMOTE_ADDR'] == '2.43.240.216'){
    // echo "Count :".count($searchPropertyObj);

    // echo "Ajax Search Result <pre>";
    // print_r($searchPropertyObj);die;

}

?>
<p class="margin_top20 text-center">
    <b>
        <span>
            {{$unpaid_count}}
        </span> 
        Work Expereinces - 
        <span>
            {{$paid_count}}
        </span> 
        School Houses
        <span class="airfcfx-searchval"></span>
    </b>
</p>
<div class="row">

    <?php
     function load_images($url,$width,$height){
        //header("Content-type: image/jpeg");
        list($width_orignal,$height_orignal) = getimagesize($url);
        $ratio_orignal = $width_orignal / $height_orignal;
        if($width/$height > $ratio_orignal){
            $width = $ratio_orignal * $height;
        }else{
            $height = $width/$ratio_orignal;
        }

        // Re-sample image
        $image_pixels = imagecreatetruecolor($width,$height);
        $image = imagecreatefromjpeg($url);
        imagecopyresampled($image_pixels, $image, 0, 0, 0, 0, $width, $height, $width_orignal, $height_orignal);
        return imagejpeg($image_pixels,null,100);

    }
    function get_radius_lat_long($lat,$long){
         //Position, decimal degrees
         $lat = $lat;//51.0
         $lon = $long;//0.0
         $Pi = "3.14";
         //Earth’s radius, sphere
         $R="6378137";

         //offsets in meters
         // $dn = 100;
         // $de = 100;
         $dn = 96;
         $de = 96;

         //Coordinate offsets in radians
         $dLat = $dn/$R;
         $dLon = $de/($R*Cos($Pi*$lat/180));

         //OffsetPosition, decimal degrees
         $latO = $lat + $dLat * 180/$Pi;
         $lonO = $lon + $dLon * 180/$Pi;
         return $latO.",".$lonO;
    }

    $s = 0;
    foreach ($searchPropertyObj as $property) { // echo '<pre>'; print_r($property);die; //echo $property->id.' == ';continue;
        if ($property->property_status == "paid" && isset($property->user->paid_service->house_slogan) && $property->user->paid_service->house_slogan == '' && isset($property->user->paid_service->house_desc) && $property->user->paid_service->house_desc == '') {
            //continue;
        } // checking if null

        $id = "carousel-example-generic" . $s;
        $lat = '';
        $lng = '';
        $org_lat = '';
        $org_lng = '';
        $propTitle = '';
        if ($property->property_status == "paid" && !empty($property->house_slogan)) {
            $propTitle = $property->house_slogan;
        } else {
            $propTitle = $property->exptitle;
        }
        if ($property->property_status == "paid") {

            // To check if the status paid, but the title and description (slogan, desc) both null/blank then ignore and not show.
            //if($property->house_slogan == '' && $property->house_desc == ''){ continue; } // checking if null
            //$rental_url = "rentals_paid";// if null then THIS
            $rental_url = "school_house";
            $clicklink = url("view/" . $rental_url . "/" . $property->user_id);
            $itemtype = 'school';

            /*
              //$query->whereRaw('IF (addr_work_org_same = 0, '
              //. '(property.pro_lat BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (property.pro_lon BETWEEN ' . $minLong . ' AND ' . $maxLong . '),'
              /*if the house property have different location means */
            //. 'IF (user_paid_services.house_property_differ = 1, ' 
            //. '(user_paid_services.proplatbox BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (user_paid_services.proplonbox BETWEEN ' . $minLong . ' AND ' . $maxLong . '), '
            //. 'IF (user_paid_services.house_property_differ = 0, '
            // . '(user_paid_services.houselatbox BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (user_paid_services.houselonbox BETWEEN ' . $minLong . ' AND ' . $maxLong . '), '
            //. '(users.org_lat BETWEEN ' . $minLat . ' AND ' . $maxLat . ') AND (users.org_lan BETWEEN ' . $minLong . ' AND ' . $maxLong . ')'
            //. ' )'
            // . ' )'
            //. ' )');
            //*/
            /* if ($property->addr_work_org_same = 0) {
              $lat = $property->pro_lat;
              $lng = $property->pro_lon;
              } elseif (isset($property->user->paid_service->house_property_differ) && $property->user->paid_service->house_property_differ == 1) {
              $lat = isset($property->user->paid_service->proplatbox) ? $property->user->paid_service->proplatbox : "";
              $lng = isset($property->user->paid_service->proplonbox) ? $property->user->paid_service->proplonbox : "";
              } elseif (isset($property->user->paid_service->house_property_differ) && $property->user->paid_service->house_property_differ == 0) {
              $lat = isset($property->user->paid_service->houselatbox) ? $property->user->paid_service->houselatbox : "";
              $lng = isset($property->user->paid_service->houselonbox) ? $property->user->paid_service->houselonbox : "";
              } else {
              $lat = $property->org_lat;
              $lng = $property->org_lan;
              } */
            $prop_latlng = $property;
            if ($prop_latlng->addr_work_org_same == 0) {
                $lat = $prop_latlng->pro_lat;
                $lng = $prop_latlng->pro_lon;
            } elseif (isset($prop_latlng->user->paid_service->house_property_differ) && $prop_latlng->user->paid_service->house_property_differ == 1) {
                $lat = isset($prop_latlng->user->paid_service->proplatbox) ? $prop_latlng->user->paid_service->proplatbox : "";
                $lng = isset($prop_latlng->user->paid_service->proplonbox) ? $prop_latlng->user->paid_service->proplonbox : "";
            } elseif (isset($prop_latlng->user->paid_service->house_property_differ) && $prop_latlng->user->paid_service->house_property_differ == 0) {
                $lat = isset($prop_latlng->user->paid_service->houselatbox) ? $prop_latlng->user->paid_service->houselatbox : "";
                $lng = isset($prop_latlng->user->paid_service->houselonbox) ? $prop_latlng->user->paid_service->houselonbox : "";
            } else {
                $lat = $prop_latlng->org_lat;
                $lng = $prop_latlng->org_lan;
            }
        } else {
            $rental_url = "rentals"; // else ... this is for job listing detail. K.
            $clicklink = url("view/" . $rental_url . "/$property->id");
            $itemtype = 'work';
            $lat = $property->pro_lat;
            $lng = $property->pro_lon;
        }
        // to vary latlogs.
        if ($lat != '' && $lng != '') {
            $org_lat = $lat;
            $org_lng = $lng;
            //  $lat = $lat + 0.00089832; //add some tested decimal numbers,  this for approximation $lat is correct latitude, 0,00089832 is approx 100 metres in the UK
            //  $lng = $lng + 0.001427437; //add some tested decimal numbers,  this for approximation, 0,001427437 is 100 metres 
            $lat = $lat + 0.00045678; //add some tested decimal numbers,  this for approximation $lat is correct latitude, 0,00089832 is approx 100 metres in the UK
            $lng = $lng + 0.00034567; //add some tested decimal numbers,  this for approximation, 0,001427437 is 100 metres 
        }
        if ($lat == '' && $lng == '') {
            continue; //do not show the property which not hvae latlongs.
        }

        $lat_long   = get_radius_lat_long($lat,$lng);
        // echo "<br> Lat New :".$lat."<br> Long :".$lng."<br> LatLong :".$lat_long."<br>";
        if(!empty($lat_long)){
            $explode = explode(",",$lat_long);
            $lat = $explode[0];
            $lng = $explode[1];
        }
        ?>
        <div class="col-xs-12 col-sm-6 margin_top10 detailsContainer" data-proptitle="<?php echo $propTitle; ?>" data-propid="<?php echo $property->id; ?>" data-itemtype="<?php echo $itemtype; ?>" data-lat = "<?php echo $lat; ?>" data-lng = "<?php echo $lng; ?>">
            <div id="{{$id}}" class="carousel slide" data-ride="carousel">
                @if (isset($property->propertyimages) && sizeof($property->propertyimages) > 0)
                <div class="carousel-inner" role="listbox" onmouseover="showme({{$s}})" onmouseout="hideme({{$s}})" >    
                    <?php
                    $i = 1;
                    if (sizeof($property->propertyimages) > 0) {
                        foreach ($property->propertyimages as $img) {
                            if(empty($img->img_name)){
                                ?>
                                 <a href='<?php echo $clicklink; ?>' target="_blank" title=""
                           class="item bg_img active" style="height:250px;width:370px;background-image:url('{{asset('images/no_prop_image.png')}}');">
                        </a>
                                <?php 
                            }else{
                                $class = "item bg_img";
                                if ($i == 1) {
                                    $class = "item bg_img active";
                                }
                                $thumbnail_path = public_path() . '/images/rentals/' . $img->property_id . '/thumbnail/' . $img->img_name;
                                 $img_path_orignal = public_path() . '/images/rentals/' . $img->property_id . '/' . $img->img_name;
    //                            echo $thumbnail_path . "<br />";
                                if (\File::exists($thumbnail_path)) {
                                    $img_path = Asset('images/rentals/' . $img->property_id . '/thumbnail/' . $img->img_name);
                                } else {
                                    $img_path = Asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
                                }

                                if (!\File::exists($img_path_orignal)) {
                                    $img_path = Asset('images/no_prop_image.png');
                                }
                                //$image_url = load_images($img_path,370,250);
                                $image_url = $img_path;
                                ?>
                                <a href='<?php echo $clicklink; ?>' target="_blank" title="{{$img->img_name}}"
                                   class="{{$class}}" style="height:250px;width:100%;background-image:url('{{$image_url}}');">
                                </a>
                            <?php
                            }
                            $i++;
                        }
                    } else {
                        ?>
                        <a href='<?php echo $clicklink; ?>' target="_blank" title=""
                           class="item bg_img active" style="height:250px;width:100%;background-image:url('{{asset('images/no_prop_image.png')}}');">
                        </a>
                    <?php } ?>
                </div>
                @else
                <div class="carousel-inner" role="listbox" onmouseover="showme(0)" onmouseout="hideme(0)" >
                    <a href='<?php echo $clicklink; ?>' target="_blank" title="no image found"
                       class="item bg_img active" style="height:250px;width:100%;background-image:url('{{asset('images/no_prop_image.png')}}');">
                    </a>
                </div>
                @endif
                <!-- Controls -->
                <?php
                if ($property->propertyimages->count() > 1) {
                    $href = "#carousel-example-generic" . $s;
                    ?> 
                    <a class="left carousel-control" href="{{$href}}" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="{{$href}}" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a><?php } ?>
                <?php
                $imgName = asset('images/noimage/profile.png');
                if (isset($property->user->profile_img_name) && $property->user->profile_img_name != "" && !is_null($property->user->profile_img_name)) {
                    $user_profile_orignal = public_path() . '/images/profile/' . $property->user->profile_img_name;
    //                            echo $thumbnail_path . "<br />";
                    if (\File::exists($user_profile_orignal)) {
                        $imgName = asset('images/profile/' . $property->user->profile_img_name);
                    }
                }
                ?>

                <a href='<?php echo $clicklink; ?>' target="_blank" title="">
                    <div class="bg_img1" style="margin-right:50px;height:56;width:56;background-image:url('{{$imgName}}');"></div>
                </a> 
                <!-- Wish List -->
                @if (Auth::guest())
                <a href="{{url('signup')}}">
                    <div class="favorite" style="margin-right:55px;" ><i class="fa fa-heart-o"></i><i class="fa fa-heart fav_bg"></i></div>
                </a>
                @else
                <div class="favorite" style="margin-right:55px;" data-toggle="modal" data-target="#myModal" onclick="show_list_popup(event,<?php echo $property->id; ?>)">
                    <?php $st = 'style="background-color: #990029"'; ?>
                    <i class="fa fa-heart-o" <?php
                    if (in_array($property->id, $wishProperty)) {
                        echo $st;
                    }
                    ?> ></i><i class="fa fa-heart fav_bg"></i>
                </div>
                @endif
                <div class="favorite" style="margin-right:0px; left: 30px; width: 10%;">
                    @if($property->property_status == "paid")
                    <img src="{{URL::Asset('images/School_icon.png')}}" style="width: 35px;" alt="" />
                    @else
                    <img src="{{URL::Asset('images/work.png')}}" style="width: 35px;" alt="" />
                    @endif
                </div>
                <!-- Wish List -->
            </div><!--carousel-example-generic end-->

            <!-- Property Name -->
            <a href='<?php echo $clicklink; ?>' target="_blank" title="{{$property->exptitle}}">
                <p class="airfcfx-search-listing-name margin_top10 margin_left10 fa-1x text_gray1">

                    <?php /*@if($property->property_status == "paid" && !empty($property->house_slogan))
                    {{$property->house_slogan}}
                    @else
                    {{$property->exptitle}}
                    @endif */ 
                    if($property->property_status == "paid" && !empty($property->house_slogan)){
                        if($_SERVER['REMOTE_ADDR'] == '31.159.143.22'){
                            echo $property->house_slogan."(".$property->id.")";
                        }else{
                            echo $property->house_slogan;                            
                        }
                    }else{
                        if($_SERVER['REMOTE_ADDR'] == '31.159.143.22'){
                            echo $property->exptitle."(". $property->id.")";
                        }else{
                            echo $property->exptitle;                            
                        }
                    }
                    ?>
                </p>
            </a>


            <a href='<?php echo $clicklink; ?>' target="_blank" title="{{$property->exptitle}}">
                <?php
                if ($property->addr_work_org_same == 0) {
                    $address = $property->workplace_address; // workplace_address is our property, listing, address. Case paid, if checkbox checked, then use this address:workplace_address
                }
                // this is where from frontend we store school house legal address, 
                // check if school house property address is not same
                else if (isset($property->user->paid_service->house_property_differ) && !empty($property->user->paid_service->propertyaddress) && $property->user->paid_service->house_property_differ == "1") {
                    $address = isset($property->user->paid_service->propertyaddress) ? $property->user->paid_service->propertyaddress : '';
                }
                // to get the houseaddress from user_paid_services table for normal address, not from property table
                else if (isset($property->user->paid_service->house_property_differ) && !empty($property->user->paid_service->houseaddress) && $property->user->paid_service->house_property_differ == "0") {
                    $address = isset($property->user->paid_service->houseaddress) ? $property->user->paid_service->houseaddress : '';
                } else if (isset($property->user->paid_service->house_property_differ) && !empty($property->user->paid_service->houseaddress) && $property->user->paid_service->house_property_differ == "0") {
                    $address = isset($property->user->paid_service->houseaddress) ? $property->user->paid_service->houseaddress : '';
                }
                // else get the user address 
                else {
                    $address = isset($property->user->raddress18) ? $property->user->raddress18 : "";
                }
                //echo $property->id . "<br />";
                //echo $address . "<br />";
               // echo "Property id ".$property->property_status."<br>";
                if ($property->property_status == "paid" && isset($getUserAcceptedJob->prd_id)) {
                    $get_school_house_time = App\RentalsEnquiry::getSchoolHouseTime($property->id, $getUserAcceptedJob->prd_id);
                  // var_dump($get_school_house_time);
                    if (isset($get_school_house_time->id) && isset($get_school_house_time->time)  ) {
                        //echo "paid address = ".$get_school_house_time->paidAddress."<br />unpaid address = ".$get_school_house_time->unpaidAddress."<br />";
                        $time = $get_school_house_time->time;

                        if($time != 'N/A'){
                            if($time>=60){
                                $hours = floor($time/60);
                                $minutes = ($time % 60);
                                $distance_time = $hours." Hours and ".$minutes." Minutes";
                            }else{
                                $distance_time = $time." Minutes";
                            }
                        }else{
                            $distance_time = 'N/A';
                        }
                        
                        //if($get_school_house_time->time != 'N/A'){
                             echo "Time to reach Workplace: " . $distance_time . "  by Bus, Google Approx. Time"; 
                        //}
                       
                    }
                }
                if($_SERVER['REMOTE_ADDR'] == '31.159.143.22'){ 
                    if($timeFromPaidToWork && isset($timeFromPaidToWork[$property->id])){
                      //  echo 'Time to reach Workplace: '.$timeFromPaidToWork[$property->id].' minutes';
                    }
                }
                /*?>
                @if($timeFromPaidToWork && isset($timeFromPaidToWork[$property->id]))
                <!--Time to reach Workplace: {{$timeFromPaidToWork[$property->id]}} minutes-->
                @endif
                */?>
                <p class="small margin_left10 text_gray1">
                    <b>
                        @if($property->property_status == "paid" && !empty($property->house_desc))
                        {{$property->house_desc}}
                        @else
                        {{$property->description}}
                        @endif
                    </b>

                    <?php
                    $maxNumberOfStars = 5; // Define the maximum number of stars possible.
                    $totalRating = 0;
                    $total_count = 0;
                    if ($property->reviews->count() > 0) {
                        $totalRating = $property->reviews->sum('rateVal');
                        $total_count = $property->reviews->count();
                    }
                    $total_rating = 0;
                    //if ($propertObj->reviews->count() > 0) {
                    if ($total_count > 0) {
                        $total_rating = $totalRating / $total_count;
                        $total_rating = ceil($total_rating);
                    }
//                    $maxNumberOfStars = 5; // Define the maximum number of stars possible.
//                    $totalRating = $property->reviews->sum('rateVal'); // Calculate the total number of ratings.
//                    $avg = 0;
//                    if ($property->reviews->count() > 0) {
//                        $avg = (int) ($totalRating / $property->reviews->count()) * $maxNumberOfStars;
//                    }
//
//                    $nonstar = $maxNumberOfStars - $avg;
                    ?>

                    <span class="text-warning">
                        <?php for ($k = 1; $k <= $total_rating; $k++) { ?> 
                            <i class="fa fa-star"></i>
                        <?php } ?>
                    </span>
                    {{$property->reviews->count()}} review
                </p>
                @if ($property->property_status == "paid")
                <!--<p>{{isset($property->pricing->amount)?"EUR ".$property->pricing->amount:''}}</p>-->
                @endif
            </a>
        </div><!--col-sm-6 end-->

        <?php
        $s++;
    }
    ?>

</div><!--row end-->