@extends('layouts.searchdefault')
@section('nav_bar')
<nav class="navbar navbar-default norm_nav ">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="airfcfx-mobile-navbar navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" class="navbar-brand">
                                    <img src="/admin/images/logoblack.png" alt="Airfinch" class="img-responsive margin_top10" />
                </a>
                <div class="pos_rel pull-left search_sec">
                <input id="where-to-go" type="text" class="form-control" placeholder="Search" 
                  value="">
                <i class="fa fa-search over_input"></i>
                
                <input id="place-lng" type="hidden" value="0">
        </div>                
            </div><input id="place-lat" type="hidden" value="0">

            <div class="navbar-collapse collapse" id="navbar">

        <ul class="nav navbar-nav navbar-right">
          <!--  <li class="dropdown">
              <form action="/language" method="GET">
                <select name="language">
<option value="fr">French</option>
<option value="en" selected>English</option>countryid
</select>               <button type="submit">Change</button>             </form>       
            </li>  -->    
                        <li class="dropdown"><a href="/user/help/index" 
                class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-question-icon"></span><span class="airfcfx-menu">Help</span></a>
              </li>
              <li class="dropdown"><a href="/register" aria-expanded="false"
                aria-haspopup="true" role="button" data-toggle="modal"
                class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-signup-icon"></span><span class="airfcfx-menu">Sign Up</span></a>
              </li>
              <li class="dropdown"><a href="/signin" aria-expanded="false"
                aria-haspopup="true" role="button" data-toggle="modal"
                class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-login-icon"></span><span class="airfcfx-menu">Login</span></a>
              </li>
                
            
          </li>
            
        </ul>
      </div>
      <!--/.nav-collapse -->
            <!--/.nav-collapse -->
        
    </nav>
  
@stop
@inject('propertySettings', 'App\PropertySettings')
<?php 
$PropertySettingsObj = $propertySettings->propertySettings();
?>
{{-- content --}}
@section('content')
    

                        <div class="split_cell_1 bg_white pos_rel">

<div class="filter_menu2">

<div class="padd_lf_rg_20" >

<div class="row">
<div class="col-xs-12 col-sm-3 margin_top30">
<label class="margin_top10 one">Dates</label>
</div><!--col-sm-3 end-->
<div class="col-xs-6 col-sm-3 margin_top30">
<input id="check-in" type="text" class="airfcfx-right-border form-control" placeholder="Check In" value="11/04/2016" onchange="updateSearchList('#check-in', 'indate');">
</div><!--col-sm-3 end-->

<div class="col-xs-6 col-sm-3 margin_top30">
<input id="check-out" type="text" class="airfcfx-right-border form-control" placeholder="Check Out" value="11/07/2016" onchange="updateSearchList('#check-out', 'outdate');">
</div><!--col-sm-3 end-->
 
<div class="col-xs-12 col-sm-3 margin_top30 ">
<select class="form-control" id="guest-count" onchange="updateSearchList('#guest-count', 'guest');" > <?php for($i=1; $i<=$PropertySettingsObj->accomodates_no; $i++) { ?>
                              <option value="{{$i}}"><?php echo "Guest".$i;?></option>
                              <?php  } ?>
        </select>
</div><!--col-sm-3 end-->
</div><!--row end-->

<div class="border_bottom2 margin_top20"></div>

<div class="row airfcfx-mobile-search-check-cnt">
<div class="col-xs-12 col-sm-3 margin_top20">
<label class="margin_top10">Room Type</label>
</div><!--col-sm-3 end-->

<?php $i=0;foreach($roomTypes as $obj) { 

if ($i == 0) {
  $id = "home";
} else if($i == 1) {
$id = "home1";
} else {
  $j = $i - 1;
  $id = "home".$j."-".$i;
}

?>

<div class="col-xs-12 col-sm-3 margin_top20">
<div class="pos_rel">
  <input id="{{$id}}" type="checkbox"  class="airfcfx-brwn-btn brwn_btn" onchange="updateSearchList('<?php echo "#".$id?>', 'roomtype-checkbox');">
<label for="home">  <i class="fa fa-home"></i> {{$obj->name}} </label>
</div>
</div><!--col-sm-3 end-->



<?php $i++;} ?>






</div><!--col-sm-3 end-->
 
</div><!--row end-->

<!-- <div class="row airfcfx-mobile-search-custcheck-cnt">
  <div class="airfcfx-mobile-cal-roomtype-cnt">
    <div class="airfcfx-mobile-cal-roomtype" onclick="updateSearchList('#home-1', 'roomtype-checkbox');">
      <div class="airfcfx-mobile-cal-room-details">
        <div class="airfcfx-mobile-entire-home"></div>
        <div class="airfcfx-mobile-cal-room-txt">Event Space</div>
      </div>
    </div>
    <div class="airfcfx-mobile-cal-roomtype" onclick="updateSearchList('#home1-2', 'roomtype-checkbox');">
      <div class="airfcfx-mobile-cal-room-details">
        <div class="airfcfx-mobile-private-room"></div>
        <div class="airfcfx-mobile-cal-room-txt">Pop Shop</div>
      </div>  
    </div>
    <div class="airfcfx-mobile-cal-roomtype" onclick="updateSearchList('#home2-3', 'roomtype-checkbox');">
      <div class="airfcfx-mobile-cal-room-details">
        <div class="airfcfx-mobile-shared-room"></div>
        <div class="airfcfx-mobile-cal-room-txt">Office Space</div>
      </div>  
    </div>
  </div>
</div> -->

  
  <div class="border_bottom2 margin_top20"></div>
<input type="hidden" id="countryid" value="">
<div class="row">
<div class="col-xs-12 col-sm-3 margin_top20">
<label class="margin_top10"> Price Range  </label>
</div><!--col-sm-3 end-->


<div class="col-xs-12 col-sm-9 margin_top20">

<div class="range-slider">
  <input id="price_range" type="text" name="price_range" value="260;800">
</div>

</div><!--col-sm-9 end-->


</div><!--row end-->   

 

<div class="filter_menu">  

<div class="border_bottom2 margin_top50"></div>

<div class="row">
<div class="col-xs-12 col-sm-3 margin_top30">
<label class="margin_top10">Size</label>
</div><!--col-sm-3 end-->

<div class="col-xs-12 col-sm-3 margin_top30">
<select id="bedroom-count" class="form-control" >
<option value="">Bedrooms</option>
  <?php for($i=1; $i<=$PropertySettingsObj->bed_room_no; $i++) { ?>
                              <option value="{{$i}}"   >{{$i}}</option>
                              <?php  } ?></select>
</div><!--col-sm-3 end-->

<div class="col-xs-12 col-sm-3 margin_top30">
<select id="bathroom-count" class="form-control">
<option value="">Bathrooms</option>
  <?php for($i=1; $i<=$PropertySettingsObj->bath_no; $i++) { ?>
                              <option value="{{$i}}"   >{{$i}}</option>
                              <?php  } ?></select>
</div><!--col-sm-3 end-->
 
<div class="col-xs-12 col-sm-3 margin_top30 ">
<select id="beds-count" class="form-control">
<option value="">Beds</option>
  <?php for($i=1; $i<=$PropertySettingsObj->bed_no; $i++) { ?>
                              <option value="{{$i}}"   >{{$i}}</option>
                              <?php  } ?></select>
</div><!--col-sm-3 end-->
</div><!--row end-->

<div class="border_bottom2 margin_top20"></div>
<div class="panel-group accor_adj" id="accordion" role="tablist" aria-multiselectable="true">

 
  
  <div class="border_bottom2 margin_top10"></div>


  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <div class="row">
<div class="col-xs-12 col-sm-3 margin_top20  margin_bottom10">
<label class="margin_top_5">Property Type</label>
</div>
<?php 
                foreach($propertyTypes as $obj) { ?>
<!--col-sm-3 end-->
<div class="col-xs-12 col-sm-3 margin_top20  margin_bottom10">
<div class="pos_rel">
<label class="font_norm">  <input type="checkbox"  class="" onchange="updateHomeType({{$obj->id}})">
<div class="airfcfx-search-checkbox-text">{{$obj->name}}</div></label>
</div>
</div> 


<?php } ?>
</div>
    </div>
   
  

   <br />
    <br />
    <br />
    <br />

  
  
  
  
</div><!--panel-group end-->



  

<!--/div--><!--padd_lf_rg_20 end-->

  </div><!--filter_menu end-->

</div>
</div>

<div class="filter_menu4 margin_bottom30">
<div class="bg_gray1 margin_top50">


<div class="airfcfx-mobile-morefilter-hide bg_gray1 padd_lf_rg_20 fix_pls">

<div class="row">
<div class="col-xs-12 col-sm-6">
<button type="button" class="btn btn-default margin_top10 filter_menu_btn"><b>More Filter</b></button>

<span class="fix_pls_span"></span>

</div><!--col-sm-6 end-->
<div class="col-xs-12 col-sm-6 text-right">
<p class="margin_top20"><b><span class="total-count-detail">6</span> Rentals · 
  <span class="airfcfx-searchval">United States</span></b></p>
</div><!--col-sm-6 end-->
</div><!--row end-->
</div><!--bg_gray1 end-->

<!--filter mobile popup-->
<div class="modal fade" id="airfcfx-mobile-filt" role="dialog"> 
        <div class="modal-dialog mobile-cal-cnt">
          <div class="mobile-cal-header">
            Filters
          </div>
          <div class="mobile-cal-body">
            <div class="mobile-cal-divider"></div>
            <div class="mobile-cal-size-cnt">
            <select id="bedroom-count-mobile" class="form-control" >
            <option value="">Bedrooms</option>
            <option value='1'>1 Bedrooms</option><option value='2'>2 Bedrooms</option><option value='3'>3 Bedrooms</option><option value='4'>4 Bedrooms</option><option value='5'>5 Bedrooms</option><option value='6'>6 Bedrooms</option><option value='7'>7 Bedrooms</option><option value='8'>8 Bedrooms</option><option value='9'>9 Bedrooms</option><option value='10'>10 Bedrooms</option>            </select>
            <select id="bathroom-count-mobile" class="form-control">
            <option value="">Bathrooms</option>
            <option value='1'>1 Bathrooms</option><option value='2'>2 Bathrooms</option><option value='3'>3 Bathrooms</option><option value='4'>4 Bathrooms</option><option value='5'>5 Bathrooms</option><option value='6'>6 Bathrooms</option><option value='7'>7 Bathrooms</option><option value='8'>8 Bathrooms</option><option value='9'>9 Bathrooms</option><option value='10'>10 Bathrooms</option>            </select>
            <select id="beds-count-mobile" class="form-control">
            <option value="">Beds</option>
            <option value='1'>1 Beds</option><option value='2'>2 Beds</option><option value='3'>3 Beds</option><option value='4'>4 Beds</option><option value='5'>5 Beds</option><option value='6'>6 Beds</option><option value='7'>7 Beds</option><option value='8'>8 Beds</option><option value='9'>9 Beds</option><option value='10'>10 Beds</option>            </select>
            </div>
            <div class="mobile-cal-divider"></div>
           
                        
  </div>
  
  <div class="border_bottom2 margin_top10"></div>
            <div class="mobile-cal-divider"></div>
            <div class="mobile-cal-section-heading txt-left-align">Property Type</div>
            <div class="mobile-cal-check-cnt">
                            <div class="pos_rel">
                <label class="font_norm">
                <input type="checkbox"  class="" onchange="updateHomeType('1')">
                <div class="airfcfx-search-checkbox-text">Office Space</div>
                </label>
              </div>
                             
                            <div class="pos_rel">
                <label class="font_norm">
                <input type="checkbox"  class="" onchange="updateHomeType('2')">
                <div class="airfcfx-search-checkbox-text">Event Space</div>
                </label>
              </div>
                             
                            <div class="pos_rel">
                <label class="font_norm">
                <input type="checkbox"  class="" onchange="updateHomeType('3')">
                <div class="airfcfx-search-checkbox-text">Storage Space</div>
                </label>
              </div>
              <div class="airfcfx-more-div">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThreemobile" aria-expanded="false" 
                  aria-controls="collapseThree" onclick="hide_more_txt(this)" id="propertymore">+ More </a></div>               
                            
            </div>
                        <div id="collapseThreemobile" class="panel-collapse collapse clear" role="tabpanel" aria-labelledby="headingTwo">
                  <div class="panel-body">

                       
 

                       

                      
                        
                       

                         
            <div class="airfcfx-less-div">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThreemobile" aria-expanded="false" 
                  aria-controls="collapseThree" onclick="show_more_txt1()">- Less </a></div>                </div>
              </div>
              
            <div class="mobile-cal-divider"></div>
            <button class="airfcfx-white-btn airfcfx-mobile-cal-2btn airfcfx-slider-btn btn btn_search"  data-dismiss="modal" type="button">Cancel</button>
            <button class="airfcfx-mobile-cal-2btn airfcfx-slider-btn btn btn_search pull-right" onclick="updateSearchListmobile('home1', 'more');">Apply Filters</button>
          </div>
        </div>
      </div>

<div class="airfcfx-mobile-float-icon-cnt"><button type="button" class="airfcfx-mobile-morefilter-btn btn btn-default margin_top10" data-toggle="modal" data-target="#airfcfx-mobile-filt"><b>More Filter</b></button></div>

<div id="search-data" class="bg_gray1 padd_lf_rg_20 padd_bottom30">

<div class="row">
      <div class="col-xs-12 col-sm-6 margin_top10">


<div id="carousel-example-generic0" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox" onmouseover="showme(0)" onmouseout="hideme(0)">    

  <a href="http://airfinchscript.com/user/listing/view/MTYwXzkxNDE=" target="_blank" title="xvcxxzczx"
     class="item bg_img active" style="height:250px;width:370px;background-image:url('http://airfinchscript.com/albums/images/listings/1465812324_50_0.jpg');">
    
  </a>
     </div>

  <!-- Controls -->
    
<a href="http://airfinchscript.com/profile/NTAtMjY3" target="_blank" title="kiran raj">
  <div class="bg_img1" style="height:56;width:56;background-image:url('http://airfinchscript.com/albums/images/users/50_profilepic.jpg');"></div>
</a>
<div class="price"><h3>  € 127</h3></div>

<!--  
<a href="http://airfinchscript.com/signin">
<div class="favorite"><i class="fa fa-heart-o"></i><i class="fa fa-heart fav_bg"></i></div>
</a> -->

<div class="favorite" data-toggle="modal" data-target="#myModal" onclick="show_list_popup(event,404);"><i class="fa fa-heart-o"></i><i class="fa fa-heart fav_bg"></i></div>

  
</div><!--carousel-example-generic end-->

<a href="http://airfinchscript.com/user/listing/view/MTYwXzkxNDE=" target="_blank" title="xvcxxzczx">
  <p class="airfcfx-search-listing-name margin_top10 margin_left10 fa-1x text_gray1">xvcxxzczx </p>
</a>

<a href="http://airfinchscript.com/user/listing/view/MTYwXzkxNDE=" target="_blank" title="xvcxxzczx">
  <p class="small margin_left10 text_gray1"><b>  Event Space </b><!-- <span class="text-warning">
<i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i>. 
</span> 1 review  </b> --></p>
</a>

</div><!--col-sm-6 end-->
 
<div class="col-xs-12 col-sm-6 margin_top10">


<div id="carousel-example-generic0" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox" onmouseover="showme(0)" onmouseout="hideme(0)">    

  <a href="http://airfinchscript.com/user/listing/view/MTYwXzkxNDE=" target="_blank" title="xvcxxzczx"
     class="item bg_img active" style="height:250px;width:370px;background-image:url('http://airfinchscript.com/albums/images/listings/1465812324_50_0.jpg');">
    
  </a>
     </div>

  <!-- Controls -->
    
<a href="http://airfinchscript.com/profile/NTAtMjY3" target="_blank" title="kiran raj">
  <div class="bg_img1" style="height:56;width:56;background-image:url('http://airfinchscript.com/albums/images/users/50_profilepic.jpg');"></div>
</a>
<div class="price"><h3>  € 127</h3></div>

 
<!-- <a href="http://airfinchscript.com/signin"><div class="favorite"><i class="fa fa-heart-o"></i><i class="fa fa-heart fav_bg"></i></div></a> -->


<div class="favorite" data-toggle="modal" data-target="#myModal" onclick="show_list_popup(event,404);"><i class="fa fa-heart-o"></i><i class="fa fa-heart fav_bg"></i></div>

  
</div><!--carousel-example-generic end-->

<a href="http://airfinchscript.com/user/listing/view/MTYwXzkxNDE=" target="_blank" title="xvcxxzczx">
  <p class="airfcfx-search-listing-name margin_top10 margin_left10 fa-1x text_gray1">xvcxxzczx </p>
</a>

<a href="http://airfinchscript.com/user/listing/view/MTYwXzkxNDE=" target="_blank" title="xvcxxzczx">
  <p class="small margin_left10 text_gray1"><b>  Event Space </b><!-- <span class="text-warning">
<i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i>. 
</span> 1 review  </b> --></p>
</a>

</div><!--col-sm-6 end-->





</div><!--row end-->
 
 



<input type="hidden" value="1" class="current-page">
<input type="hidden" value="10" class="offset">
<input type="hidden" value="10" class="limit">
<div class="border_bottom"></div>


</div><!--padd_lf_rg_20 end-->
</div> <!--bg_gray1 end-->
</div><!--filter_menu4 end-->


<div class="bottom_filter">
<button type="button" class="btn btn-default filter_menu_btn">Close</button>
<button type="button" class="btn btn-danger  filter_menu_btn margin_left10" onclick="updateSearchList('home1', 'more');">Apply Filters</button>
</div><!--bottom_filter end-->

</div>  <!--cell_1 end-->
</div>
<div class="split_cell_2">
 
<div class="airfcfx-map-check-div"> 
  <input type="checkbox" id="searchmove" name="searchmove">
  <label class="airfcfx-search-checkbox-text" for="commonamenities1" style="display:block">Search as I move the map</label>
</div>  
<div id="map" style="border:0; height:90vh; width:100%;">

</div>


</div>  <!--cell_2 end-->

<script>

//more filter
$(document).ready(function() {
   var project3 = $('.fix_pls_span').offset();
  var $window = $('.split_cell_1');
$window.scroll(function() {
//start fix
    if ( $window.scrollTop() >= project3.top) {
          $(".fix_pls").addClass("fixed1");
    }
    else {
          $(".fix_pls").removeClass("fixed1");
      } 
    
  }); 
  });


//slideer stop
$('.carousel').carousel({
  interval: false
})
//Range slider
  jQuery(document).ready(function($) {
    var priceto = '';
          priceto = $.trim(priceto);
    // Price Range Input
    $("#price_range").rangeslider({
      from: 10,
      to: 10000,
      limits: false,
      //scale: ['1', '10000+'],
      //heterogeneity: ['50/500'],
      step: 10,
      smooth: true,
      dimension: '',
      /*onstatechange : function(event)
      {
      value = $(".jslider-value.jslider-value-to > span").html();//alert(value);
      if (value>9999) {//alert("hello");
         $(".jslider-value.jslider-value-to > span").html('10000+');
      }
      else
      {
        $(".jslider-value.jslider-value-to > span").html(value);
      }  
      //$(".jslider-value.jslider-value-to > span").html((ui.value == '10000') ? (ui.value + '+') : ui.value);
      },*/

    });
  });

//Range slider  
$(document).ready(function(){
    $(".toggle_foot, .close_x").click(function(){
        $(".toggle_foot1").toggleClass("foot_ads");
    });
});


//Range slider  
$(document).ready(function(){
    $(".filter_menu_btn").click(function(){
        $(".filter_menu").toggleClass("filter_menu1");
    $(".filter_menu2").toggleClass("filter_menu3");
    $(".filter_menu4").toggleClass("filter_menu5");
    $(".bottom_filter").toggleClass("filter_menu1");    
    });
});



$(function () {
  $("#check-in").datepicker({
    minDate:new Date(),
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#check-out").datepicker("option", "minDate", dt);
      updateSearchList('#check-in', 'indate');
        },
    onClose: function (selectedDate) {
    $("#check-out").datepicker('show');
    }   
    });
edate = new Date();
edate.setDate(edate.getDate()+1); 
    $("#check-out").datepicker({
    minDate:edate,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#check-in").datepicker("option", "maxDate", dt);
      updateSearchList('#check-out', 'outdate');
        }
    });
});

</script>
    
<script async defer type="text/javascript" src="{{ asset('frontendassets/js/map_actions.js')}}"></script>
 
<script async defer type="text/javascript">

  var markerPoints = new Array();
  var infoMarker = new Array();
  var baseLat = 37.09024; var baselng = -95.712891;
  
            var locations = {lat: 0, lng: 0};
        //addMarker(locations); 
        markerPoints.push(locations);
        infoMarker.push('<p>xvcxxzczx</p><span>Event Space</span><br><b>€127</b>');
          //pausecontent.push('');
              var locations = {lat: 37.807487, lng: -122.420464};
        //addMarker(locations); 
        markerPoints.push(locations);
        infoMarker.push('<p>bright cozy room amazing view</p><span>Pop Shop</span><br><b>€500</b>');
          //pausecontent.push('');
              var locations = {lat: 37.797565, lng: -122.403397};
        //addMarker(locations); 
        markerPoints.push(locations);
        infoMarker.push('<p>Luxury Studio-Hotel Like Amenities</p><span>Pop Shop</span><br><b>€422.37</b>');
          //pausecontent.push('');
              var locations = {lat: 37.175346, lng: -84.11953};
        //addMarker(locations); 
        markerPoints.push(locations);
        infoMarker.push('<p>London Listing</p><span>Office Space</span><br><b>€359.72</b>');
          //pausecontent.push('');
              var locations = {lat: 39.886448, lng: -83.44825};
        //addMarker(locations); 
        markerPoints.push(locations);
        infoMarker.push('<p>ikol uol</p><span>Event Space</span><br><b>€800</b>');
          //pausecontent.push('');
              var locations = {lat: 37.780972, lng: -122.279114};
        //addMarker(locations); 
        markerPoints.push(locations);
        infoMarker.push('<p>SPACIOUS DOWNTOWN APARTMENT MODERN</p><span>Event Space</span><br><b>€449.65</b>');
          //pausecontent.push('');
        var latLng = new google.maps.LatLng(baseLat, baselng);
    var geocoder= new google.maps.Geocoder();
    var searchval = $("#where-to-go").val();
    var zoomval;
    geocoder.geocode( {'latLng': latLng},
      function(results, status) {
        if(status == google.maps.GeocoderStatus.OK) {
          if(results[0]) {
                    var add= results[0].formatted_address ;
                    var  value=add.split(",");

                    count=value.length;
                    country=value[count-1];
                    state=value[count-2];
                    //city=value[count-3];
                    if (typeof results[0]['address_components'][4] != 'undefined') {
                    city=results[0]['address_components'][4].long_name;
                  }
                  else
                    city = "";
                    /*if (typeof state != 'undefined') {
                    state=value[count-2];
                  }
                  else
                    state = "";  
                  stateexist = searchval.indexOf(state);*/
                    cityexist = searchval.indexOf(city);
                    
                    /*if(stateexist>=0)
                      zoomvalue = "8";
                    else */if(cityexist>=0)
                      zoomvalue = "10";
                    else
                      zoomvalue = "5";
                zoomval = 5;
          }
          else {
            //alert("No results");
          }
        }
        else {
          //alert("Status: " + status);
        }
      }
    );
    google.maps.event.addListener(map,'click',function(event) { alert("dfsd");

});
    //showMarkers();
</script>
  
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog listpopupwidth" role="document">
    <div class="modal-content">

      <div class="modal-body padding0">
        <div class="toplistdiv" style="display:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>   
        <h3>Save to Wish List</h3>          
        <hr />
  </div>
          <div class="airfcfx-leftlistdiv leftlistdiv">
        <div class="banner2 banner2hgt" id="listimage" ></div>
          </div>
      <input type="hidden" value="" id="listingid">
  <div class="airfcfx-rightlistdiv-cnt">
   <div class="airfcfx-rightlistdiv rightlistdiv padding20 wishlisthgt">
   
   <div class="airfcfx-topfullviewdiv topfullviewdiv">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
        <h3>Save to Wish List</h3><hr />
         </div>
     
   <div class="airfcfx-wishlist-contianer" id="listsdiv"> 
<div id="listnames"></div>
          </div>
      </div>
      <div class="airfcfx-wish-createlist-cnt">
        <input type="text" id="newlistname" class="airfcfx-listtxt listtxt" value="" placeholder="Create New List" maxlength="20">
        <input type="button" value="Create" class="airfcfx-createbtn btn btn-danger createbtn" onclick="create_new_list();">
      </div> 
          <div class="airfcfx-wishlist-btn-cnt">
          <input type="button" value="Cancel" class="airfcfx-cancelsze btn btn_email cancelsze cancelbtn " data-dismiss="modal">
      <input type="button" value="Save" data-dismiss="modal" class="airfcfx-savebtn btn btn-primary savebtn pull-right" onclick="save_lists();">
      <div class="errcls listerr"></div>
         </div>
    </div> 
          
    </div>
      <div class="clear">

      </div>            
  </div>
</div>
</div>  
  
<!--div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog listpopupwidth" role="document">
    <div class="modal-content">

      <div class="modal-body padding0">
      <div class="toplistdiv" style="display:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
        <h3>Save to Wish List</h3>
          
        <hr />
        </div>
          <div class="leftlistdiv">

            <div class="banner2 banner2hgt" id="listimage" ></div>
          </div>
          <input type="hidden" value="" id="listingid">
        <div class="rightlistdiv padding20 wishlisthgt" id="listsdiv">
        <div class="topfullviewdiv">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
        <h3>Save to Wish List</h3>
         <hr />
         </div>

      <div id="listnames"></div>
          </div>
          <input type="text" id="newlistname" class="listtxt" value="" placeholder="Create New List">
          <input type="button" value="Create" class="btn btn-danger createbtn" onclick="create_new_list();">
          <br />
         
          <br />
        <input type="button" value="Cancel" class="btn btn_email cancelsze cancelbtn" data-dismiss="modal">
    <input type="button" value="Save" data-dismiss="modal" class="btn btn-primary savebtn" onclick="save_lists();">
     <div class="errcls listerr"></div>
          
    </div>
      <div class="clear">

      </div>            
  </div>
</div>
</div-->
  <script>
$(document).ready(function(){ 
  $("body").css("overflow","hidden");
  $(".alert-success").addClass("flashcss");

  $("#closebutton").click(function(){
    $(".alert-success").removeClass("flashcss");
  });
});
</script>



 <script>
/*$(document).ready(function(){
  loginSession = readCookie('PHPSESSID');
  function readCookie(name) {
      var nameEQ = escape(name) + "=";
      var ca = document.cookie.split(';');//console.log(document.cookie);
      for (var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) === ' ') c = c.substring(1, c.length);
          if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
      }
      return null;
  }
  if (typeof timerId != 'undefined'){
    clearInterval(timerId);
  }
  var timerId = setInterval(function() {
    var currentSession = readCookie('PHPSESSID');
      if(loginSession != currentSession) {
        console.log('in reload '+loginSession+" "+currentSession);
        window.location = 'http://airfinchscript.com/';
        clearInterval(timerId);
          //Or whatever else you want!
      }
      
  },1000);
});*/


$(document).ready(function(){
$("#check-in").keydown(function(event){
if (event.which == 13) {
$("#check-in").readonlyDatepicker(true);
}
});
$("#check-out").keydown(function(event){
if (event.which == 13) {
$("#check-out").readonlyDatepicker(true);
}
}); 
  $("#check-in").datepicker({
    minDate:new Date(),
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#check-out").datepicker("option", "minDate", dt);
        },
    onClose: function (selectedDate) {
    $("#check-out").datepicker('show');
    }   
    });
edate = new Date();
edate.setDate(edate.getDate()+1); 
    $("#check-out").datepicker({
    minDate:edate,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#check-in").datepicker("option", "maxDate", dt);
        }
    });
  
  $("#check-in-main").datepicker({
    minDate:new Date(),
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#check-out-main").datepicker("option", "minDate", dt);
        },
    onClose: function (selectedDate) {
    $("#check-out-main").datepicker('show');
    }   
    });
edate = new Date();
edate.setDate(edate.getDate()+1); 
    $("#check-out-main").datepicker({
    minDate:edate,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#check-in-main").datepicker("option", "maxDate", dt);
        }
    });   
});
</script>
 
</body>
</html>

<script type="text/javascript" src="{{ asset('frontendassets/js/owl.carousel.js')}}"></script>
<script type="text/javascript" src="{{ asset('frontendassets/js/jquery-ui.min.js')}}"></script>

@stop