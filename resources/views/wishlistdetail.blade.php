@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">
        <div class="col-sm-12 no-hor-padding">

            <div class="col-xs-12 margin_top20 margin_bottom20">
                <div class="col-sm-1 text-center imgleft">
                    <span class="airfcfx-user-icon profile_pict inlinedisplay" style="width:60px;background-image:url('{{asset('images/profile/'.$wishlistobj->user->profile_img_name)}}');"></span>                    </div>
                <div class="col-sm-9">
                    <a href="#" class="text-danger"><h4 class="margin_top0"><b>{{$wishlistobj->user->name}}'s Wish Lists</b></h4></a>

                    <p> Wishlists: <b>{{$wishlistobj->properties->count()}} listings</b></p>

                </div> 
                <!--div class="col-sm-2">
            <button class="btn btn_google pull-right " data-toggle="modal" data-target="#myModal">Create a New Lists</button>
            </div-->
            </div>
        </div> <!-- row end -->
        <div class="col-sm-12">  
            <?php
            foreach ($wishlistobj->properties as $prop) {
                $id = "wish" . $prop->id;
                ?>
                <div id="{{$id}}">



                    <div class="col-xs-12 col-sm-8 leftdiv no-hor-padding">

                        <div id="carousel-example-generic1591" class="carousel slide" data-ride="carousel">

                            <div class="carousel-inner" role="listbox">  

                                <?php
                                foreach ($prop->propertyimages as $img) {
                                    $imgURL = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
                                    ?>
                                    <a href='{{url("view/rentals/$prop->id")}}' target="_blank" title="{{$prop->title}}" class="banner1 item active" style="background-image:url({{$imgURL}});width: 100%; height: 315px;">

                                    </a>
                                <?php } ?>

                            </div>

                            <!-- Controls -->






                        </div><!--carousel-example-generic end--></div>


                    <div class="col-xs-12 col-sm-4 leftdiv wishrightalgn"><h3 class="airfcfx-listing-name"><a href='{{url("view/rentals/$prop->id")}}'>{{$prop->title}}</a></h3>{{$prop->auto_complete}}</div>


                    <div class="clear"></div>
                    <div style="cursor:pointer;padding-top:10px;" class="col-xs-12 col-sm-2" onclick="remove_wish_list({{$wishlistobj->id}}, {{$prop->id}})">
                        <div class="airfcfx-listing-remove"><i class="fa fa-remove"></i><span style="padding-left:5px;">Remove</span></div>
                    </div>



                    <br></div>

                <hr class="wishlistborder">



            <?php } ?> </div> 
        <!-- row end -->

    </div> <!-- container end -->
</div>
<script type="text/javascript">
    $('.carousel').carousel({
    interval: false
    })
</script>

@stop