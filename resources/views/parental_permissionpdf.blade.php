<html><head><style> 
            .cust_p{
                line-height:1.38;margin-top:0pt;margin-bottom:0pt;margin-left: -36.75pt;margin-right: -29.25pt;text-align: center;
            }
            .cust_title{
                font-size: 12pt; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }
            .cust_address{
                font-size: 11pt; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }
            .cust_h1{
                line-height:1.38;margin-top:0pt;margin-bottom:0pt;margin-left: -36.75pt;margin-right: -29.25pt;text-align: center;
            }
            .cust_main_title {
                font-size: 14pt; font-family: Arial; color: rgb(7, 55, 99); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }
            .cust_span1 {
                font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }
            .main_div {
                margin: 0 auto;width: 100%;position: relative;;
            }
            table {
                border-width: initial; border-style: none; border-color: initial; border-collapse: collapse;margin: 0 auto;position: relative; width: 100%;
            }
            table th {
                border-left:solid #000000 1pt;border-right:solid #000000 1pt;border-bottom:solid #000000 1pt;border-top:solid #000000 1pt;vertical-align:top;padding: 2px /*5pt 5pt 5pt 5pt*/;
                border:none !important;
            }
            table td {
                border-left:solid #000000 1pt;border-right:solid #000000 1pt;border-bottom:solid #000000 1pt;border-top:solid #000000 1pt;vertical-align:top;padding:2px /*5pt 5pt 5pt 5pt*/;   
                border :none !important;
            }
            table td strong{
                font-size: 12px;
            }
            table .part_1{
                width: 95%;
                font-size: 12px;
            }
            .cust_footer_div {
                width: 100%;margin:0 auto;
            }
            .cust_footer_p {
                line-height:1.30;
                font-size: 12px;
                /*border: solid 1px red;*/
                /*margin-left: 60px;*/
            }
            .cust_footer_p span {
                font-size: 12px;
                /*border: solid 1px red;*/
                /*margin-left: 60px;*/
            }
            .cust_footer_span {
                font-size: 10pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }
            .div_square {
                            width: 25%;
                            height: 2%;
                            border: solid 1px black;
                            padding: 5px;
                            margin: 3px 0 4px 5px;
                        }

</style></head><body>
    <h1 dir="ltr" class="cust_h1">
        <span id="docs-internal-guid-0af5eb38-7fff-dcf3-8c8e-c9bc1f21cb64">
            <span class="cust_main_title">Consenso genitori per minori di 18 anni - Programma FLASH - Alternanza all’Estero </span>
        </span>
    </h1>

<?php if (is_null($enquiryObj->user->student_relationship)) {
    $studentName = $enquiryObj->user->name." ".$enquiryObj->user->lastname.";";
}else{
    $studentName = $enquiryObj->user->sname16." ".$enquiryObj->user->ssurname16.";";
}
?>
<p dir="ltr" class="cust_footer_p">
    <!-- <span id="docs-internal-guid-0af5eb38-7fff-dcf3-8c8e-c9bc1f21cb64"> -->
    <span>
        Gli studenti sotto i 18 anni sono minorenni per la legge del Regno Unito. Si avvisa che lui/lei non sarà sorvegliato durante le pause, prima e dopo il lavoro fino al rientro alle school houses dove tengono lezioni e alloggiano). 
    </span>
</p>
<div dir="ltr" class="main_div">
    <table border="0" align="center">
        <colgroup>
            <col width="60"/>
            <col width="119"/>
            <col width="119"/>
            <col width="70"/>
            <col width="202"/>
            <col width="143"/>
        </colgroup>
        <tbody>
            <tr style="height:16pt">
                <td colspan=2> <strong> Luoghi con maggiorenni - obbligatorio </strong> </td>
            </tr>
            <tr style="height:16pt">
                <td class="part_1"> Questo consenso è obbligatorio per poter partecipare alle attività descritte.
Confermo di essere disposta/o a lasciare che mio figlio/a di 16 /17 anni venga inserita/o in un ambiente con altri soggetti maggiori di 18 anni. </td>
                <td> 
                    <div class="div_square">&nbsp;</div> 
                </td>
            </tr>
            <tr style="height:16pt">
                <td colspan=2> 
                    <strong> Emergenze mediche </strong> 
                </td>
            </tr>
            <tr style="height:16pt">
                <td class="part_1">
                    L’autorità di un genitore/tutore è richiesta per preventivamente per eventuali emergenze mediche, chirurgiche o trattamenti dentali che potrebbero incorrere in caso di gravi incidenti o malattie. Per evitare ritardi nell’organizzazione del trattamento durante le emergenze, se il permesso non può, per ogni ragione, essere ottenuto, si raccomanda di dare il permesso in anticipo all’agente di zona, dichiarando l’autorità su questo modulo di permesso agli agenti del prof. Avanzi Matthew.
Do il permesso ad agente senior incaricato dal Prof. Avanzi Matthew ad organizzare trattamenti di emergenza medica, chirurgica e dentale per mio figlio se necessario. 
                </td>
                <td> 
                    <div class="div_square">&nbsp;</div>        
                </td>
            </tr>
            <tr style="height:16pt">
                <td colspan=2> 
                    <strong> Attività - obbligatorio </strong>
                </td>
            </tr>
            <tr style="height:16pt">
                <td class="part_1">
                    Do il permesso a mio figlio di partecipare nel programma attività per minori che ha luogo sia in sede che utilizzando mezzi di trasporto organizzati dal Prof. Avanzi Matthew e dal suo team quando necessario. 
                </td>
                <td> 
                    <div class="div_square">&nbsp;</div>        
                </td>
            </tr>

            <tr style="height:16pt">
                <td colspan=2> 
                    <strong> Media </strong>
                </td>
            </tr>
            <tr style="height:16pt">
                <td class="part_1">
                    Fotografie o video possono essere fatti agli studenti durante la permanenza e presso le strutture ospitanti. Il Prof. Avanzi Matthew si riserva il diritto di usare queste foto o video clips come parte di un futuro materiale promozionale, se non altrimenti dichiarato dal genitore/tutore. Si spunti questa casella se si acconsente che il proprio figlio faccia parte del futuro materiale pubblicitario.
                </td>
                <td> 
                    <div class="div_square">&nbsp;</div>        
                </td>
            </tr>
             <tr style="height:16pt">
                <td colspan=2> 
                    <strong> Spese per urgenze (oppure convenuti per email) </strong>
                </td>
            </tr>
            <tr style="height:16pt">
                <td class="part_1">
                   Convengo che eventuali supplementi per cibo o benessere/salute di mio figlio saranno inclusi automaticamente in fattura alla fine della permanenza., quali ad esempio e non limitatamente a spese incorse per permettere il rimpatrio del figlio in caso di perdita del documento di viaggio.
                </td>
                <td> 
                    <div class="div_square">&nbsp;</div>        
                </td>
            </tr>
           <tr style="height:16pt">
                <td colspan=2> 
                   <strong> Trasporti aeroportuali e locali - obbligatorio </strong>
                </td>
            </tr>
            <tr style="height:16pt">
                <td class="part_1">
                   I trasporti da e all’aeroporto, come pure gli spostamenti interni tra school house e posto di lavoro e lezioni fossero necessari mezzi, può essere pagato direttamente sul posto in quanto il Prof. Avanzi ha solo intermediato e non avrà ricavi dagli stessi. Confermo di aver letto e capito che queste spese sono eventuali ed accessorie.
                </td>
                <td> 
                    <div class="div_square">&nbsp;</div>        
                </td>
            </tr> 
            <tr style="height:16pt">
                <td colspan=2> 
                    <strong> Permesso uscite senza supervisione (possibile solo per studenti di 16 e 17 anni) - obbligatorio </strong>
                </td>
            </tr>
            <tr style="height:16pt">
                <td class="part_1">
                    Do il permesso a mio figlio di lasciare il posto di lavoro/la famiglia ospitante senza supervisione durante il tempo libero. Ho spiegato a mio figlio che deve partecipare a tutti gli eventi in programma e deve rientrare presso la famiglia ospitante nell’orario stabilito in base all’età: 16 anni e 17 anni: 10:00pm
                </td>
                <td> 
                    <div class="div_square">&nbsp;</div>        
                </td>
            </tr> 
            <tr style="height:16pt">
                <td colspan=2> 
                    <strong> Accettazione Termini e Condizioni sul sito www-work-related-learning.com - obbligatorio </strong>
                </td>
            </tr>
            <tr style="height:16pt">
                <td class="part_1">
                    Dichiaro di aver letto, compreso ed accettato integralmente termini e condizioni (Terms and Conditions” sul sito ove genitore e studente hanno immesso i loro dati per poter partecipare al progetto. Accetto le condizioni di assicurazione ed il costo di interventi non inclusi (non imputabili alle school houses ed ai servizi offerti dagli agenti) in euro di equivalenti £20 costi fissi e £30 all’ora con massimale di £150 al giorno; esempi pratici: assistenza recupero documento di viaggio temporaneo in caso di perdita dello stesso, assistenza da remoto e in loco in caso di arresto o altro.
                </td>
                <td> 
                    <div class="div_square">&nbsp;</div>        
                </td>
            </tr> 

        </tbody>
    </table>
<p dir="ltr" class="cust_footer_p">
    <span id="docs-internal-guid-0af5eb38-7fff-dcf3-8c8e-c9bc1f21cb64">
    Se desidera cambiare le decisioni riportate su questo documento, si necessita di un nuovo documento via lettera registrata (raccomandata). Sono obbligatori quelli indicati come tali.
    </span>
</p>
<p dir="ltr" class="cust_footer_p">
    <span id="docs-internal-guid-0af5eb38-7fff-dcf3-8c8e-c9bc1f21cb64">
        <strong> Dettagli Genitore o Tutore Legale</strong> di: <?php echo $studentName;?> 
    </span>
        Lei deve essere la persona che ha la responsabilità genitoriale per il periodo: <?php echo ($enquiryObj->checkin != '' ? date("Y-m-d",strtotime($enquiryObj->checkin)) : "");?>-<?php echo ($enquiryObj->checkout != '' ? date("Y-m-d",strtotime($enquiryObj->checkout)).";" : "");?> ed essere maggiore di 18 anni, accompagnatori ed agenti non possono completare questo modulo
    </span>
</p>
<p dir="ltr" class="cust_footer_p">
    <span>
        <!-- <span class="cust_footer_span"> -->
            Nome e Cognome per esteso ( del genitore/tutore ):_____________________; Telefono:_____________________;
        <!-- </span> -->
    </span>
</p>
<p dir="ltr" class="cust_footer_p">
    <span>
        <!-- <span class="cust_footer_span"> -->
            Relazione con lo studente:_____________________; Indirizzo completo:_____________________;
        <!-- </span> -->
    </span>
</p>
<p dir="ltr" class="cust_footer_p">
    <span>
        <!-- <span class="cust_footer_span"> -->
            Luogo e data:_____________________; Firma del genitore:_____________________; 
        <!-- </span> -->
    </span>
</p>
<p dir="ltr" class="cust_footer_p">
    <span>
        <span class="cust_footer_span" style="color:blue;font-size: 9px;">
            <strong>Privacy: </strong> Where Learn in a Flash ltd receives any personal data (as defined by the Data Protection Act 1998 and following relevant), it shall ensure that it fully complies with the provisions of the Act and only deals with the data to fulfil its obligations. In fulfilment of its obligations under the Act, Learn in a Flash ltd as the party providing data should too, shall have such systems in place to ensure: a) Full compliance with the Act b) In particular, compliance with the Seventh Data Protection Principle which deals with the security of personal data c) The reliability of all its employees who may be involved in processing the personal data. Parties involved shall take all reasonable steps o ensure that all its partners contractors and agents comply with this clause where they are processing any of personal data. Other parties involved shall allow Learn in a Flash ltd reasonable access to such information as is necessary to ensure that it is complying with the agreed and legal provisions and the Act as a whole. 
        </span>
    </span>
</p>
</div>
</body></html>
<?php //echo "test";exit;?>