<html>
    <head>
        <style>
            .divbottom 
            {
                margin-bottom:10px;
            }

            .margintop
            {
                margin-top:50px;
            }


        </style>
    </head>
    <body>
        <div style="margin-left:60px;">
            <div>
                {!! html_entity_decode($cmsObj->page_desc) !!}
                <h2>1) INSTITUTE OF HIGHER EDUCATION - PROMOTER</h2></div>
            <div class="divbottom">Institute (hereinafter referred to as Promoter): {{isset($rentalObj->user->school->name)?$rentalObj->user->school->name:""}}</div>


            <div><h2>2) ORGANISATION - THE HOSTING PARTY</h2></div>
            <div class="divbottom">Organisation Name: {{$rentalObj->host->orgname}}</div>
            <!-- Legal Address -->
            <div class="divbottom" ><h3>Registered office/Legal Address</h3></div></tr>
        <div class="divbottom">Address street name: {{$rentalObj->host->router18}}</div>
        <div class="divbottom">Address street number: {{$rentalObj->host->street_numberr18}}</div>
        <div class="divbottom">Town/City: {{$rentalObj->host->localityr18}}</div>
        <div class="divbottom">State/Region: {{$rentalObj->host->administrative_area_level_1r18 }}</div>
        <div  class="divbottom">Provincia:</div>
        <div class="divbottom">Postal code: {{$rentalObj->host->postal_coder18}}</div>
        <div class="divbottom">Country: {{$rentalObj->host->countryr18}}</div>
        <!-- End Legal Address -->
        <?php if ($rentalObj->property->addr_work_org_same == 0) { ?>  
            <!-- Operative Address -->
            <div class="divbottom" ><h3>Operative Address where the work related learning traineeship takes place:</h3></div>
            <div class="divbottom">Address street name: {{$rentalObj->property->wroute}}</div>
            <div class="divbottom">Address street number: {{$rentalObj->property->wstreet_number}}</div>
            <div class="divbottom">Town/City: {{$rentalObj->property->wlocality}}</div>
            <div class="divbottom">State/Region: {{$rentalObj->property->wadministrative_area_level_1 }}</div>
            <div  class="divbottom">Provincia:</div>
            <div class="divbottom">Postal code: {{$rentalObj->property->wpostal_code}}</div>
            <div class="divbottom">Country: {{$rentalObj->property->wcountry}}</div>
            <div class="divbottom">Telephone: {{$rentalObj->property->location_email}}</div>
            <div class="divbottom">Email: {{$rentalObj->property->location_telephone}}</div>
            <!-- End Legal Address -->

        <?php } else { ?>
            <div class="divbottom" ><h3>Operative Address where the work related learning traineeship takes place:</h3></div>
            <div class="divbottom">Address street name: {{$rentalObj->host->router18}}</div>
            <div class="divbottom">Address street number: {{$rentalObj->host->street_numberr18}}</div>
            <div class="divbottom">Town/City: {{$rentalObj->host->localityr18}}</div>
            <div class="divbottom">State/Region: {{$rentalObj->host->administrative_area_level_1r18 }}</div>
            <div  class="divbottom">Provincia: </div>
            <div class="divbottom">Postal code: {{$rentalObj->host->postal_coder18}}</div>
            <div class="divbottom">Country: {{$rentalObj->host->countryr18}}</div>
            <div class="divbottom">Telephone: {{$rentalObj->host->orgphone}}</div>
            <div class="divbottom">Email: {{$rentalObj->host->orgemail}}</div>
        <?php } ?>

        <div class="divbottom">Further (possible) operating office to conduct the Internship: 
            <?php
            if ($rentalObj->property->work_other_check == 1) {
                echo "Yes";
            } else {
                echo "no";
            }
            ?>
        </div>
        <div class="divbottom">Other operative Address where the work related learning traineeship takes place: </div> 
        <div class="divbottom">{!!$rentalObj->property->location_other_address!!}</div> 


    </div> 


    <!-- Work Tutor Infomation  -->
    <div class="divbottom" ><h3>Work Tutor:</h3></div> 

    @if(isset($rentalObj->property->work_org_same) and $rentalObj->property->work_org_same == 0)

    <div class="divbottom">Name of Work Tutor: {{$rentalObj->property->represent_name}}</div> 
    <div class="divbottom">Surname of Work Tutor: {{$rentalObj->property->represent_surname}}</div> 
    <div class="divbottom">Place of birth: {{$rentalObj->property->represent_born}}</div>
    <div class="divbottom">Tax Code - Tax identification number:{{$rentalObj->property->represent_tax_no}}</div> 
    <div class="divbottom">Email: {{$rentalObj->property->represent_email}}</div> 
    <div class="divbottom">Telephone: {{$rentalObj->property->represent_phone}} </div> 
    @else
    <div class="divbottom">Name of Work Tutor: {{isset($rentalObj->property->user->name)?$rentalObj->property->user->name:''}}</div> 
    <div class="divbottom">Surname of Work Tutor: {{isset($rentalObj->property->user->lastname)?$rentalObj->property->user->lastname:''}}</div>        
    <div class="divbottom">Place of birth: {{isset($rentalObj->property->user->basicpob)?$rentalObj->property->user->basicpob:''}}</div>
    <div class="divbottom">Tax Code - Tax identification number: {{isset($rentalObj->property->user->NIN18)?$rentalObj->property->user->NIN18:''}}</div> 
    <div class="divbottom">Email: {{isset($rentalObj->property->user->email)?$rentalObj->property->user->email:''}}</div> 
    <div class="divbottom">Telephone: {{isset($rentalObj->property->user->phonenumber)?$rentalObj->property->user->phonenumber:''}} </div> 
    @endif

    <div>Type of traineeship: Internship Training Curricula</div>

    <div><h2>3) STUDENT INTERN / TRAINEE</h2></div>
    <div class="divbottom">Name of Student: {{$rentalObj->user->name}}</div>
    <div class="divbottom">Surname of Student: {{$rentalObj->user->lastname}}</div>


    <div class="divbottom">Date of Agreement: {{$rentalObj->dateAdded}}</div>
    <div class="divbottom">Reference number of Agreement: {{$rentalObj->Bookingno}}</div>



    <!--DURATION, OBJECTIVES AND METHODS OF THE INTERNSHIP -->
    <div style="text-align:center"><h3>DURATION, OBJECTIVES AND METHODS OF THE INTERNSHIP</h3></div>
    <div class="divbottom">DExpected hours: {{$rentalObj->property->work_hours}}</div>
    Hours done: 
    <div class="divbottom">Start Date: {{$rentalObj->checkin}}
    </div>
    <div class="divbottom">End Date: {{$rentalObj->checkout}} 
    </div>

</body>
</html>