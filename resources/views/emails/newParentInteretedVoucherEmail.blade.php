<html>
    <head>
        <title>Work-Related-Learning - Posti di Lavoro</title>
    </head>
    <body>
        <!-- TonewParentInteretedVoucherEmail-->

        <p> Gentili genitori e studenti, 
        </p>
        <p>con la presente iscrizione state esprimendo interesse nella proposta, i vostri dati verranno conservati e trasmessi all'Istituto di appartenenza.</p>
        <br />

        <p>
            Vi ricordiamo che se lo studente fosse minore di 18 anni saranno i genitori a registrarsi al portale, 
            poi completerete il profilo utente cliccando: log-in, sul nome in alto a destra, 
            poi su <a href="http://work-related-learning.com/user/edit" target="_blank"><strong>Edit Profile</strong> 
                infine sul pulsante <a href="http://work-related-learning.com/user/parent" target="_blank"><strong>Parent of Under 18</strong>; 
                    se ivece foste già maggiorenni sul tasto <a href="http://work-related-learning.com/user/studentGuest" target="_blank"><strong>Student Guest (over 18)</strong>.
        </p>
        <br />
        <h4>REGISTRAZIONE AL PORTALE - dati necessari</h4>
        <p>Prego procedere al completamento di quanto richiesto dal vostro istituto nella sezione <a href="http://work-related-learning.com/user/edit" target="_blank"><strong>Profile</strong></a></p>
        
        <p>In genere viene richiesto nome e cognome dello studente, Istituto Superiore e classe di appartenenza, infine gruppo di appartenenza se sono state proposte diverse date di partenza.</p>
<br />
<p>Tutti gli altri dati sarano necessari affinchè il sistema possa generare la documentazione necessaria precompilata, pronta per le firme, ovvero necessarimente da compilare solo prima dell'invio delle domande di lavoro.</p>
<br />
<p>I dati sono richiesti dal sistema, quindi per salvare dovrete mettere un valore, ad esempio uno zero (0) o un trattino (-) negli altri campi.</p>
<br />

<h4>Prossimo passo?</h4>
        <p>Una volta verificate le pre-iscrizioni, riceverete email di conferma e modalità di ricerca lavoro e School House (famiglie ospitanti che offrono anche altri servizi).</p>
        
        <p><strong>Consigliamo</strong> di anticipare i tempi e selezionare i posti di lavoro di interesse utilizzando la funzione <a href="http://work-related-learning.com/user/wishlist" target="_blank">Add to Wishlist</a>, per ogni posto di interesse si cliacca sul cuore (anzichè su invia richiesta).</p>
        
<br />

<h4>Quando potremo fare domanda di lavoro?</h4>
        <p>Riceverete email di conferma e modalità di ricerca lavoro e School House.</p>
        
        <p>Una volta acquistato il credito necessario (pagando l'Isituto o l'Agenzia) potrete fare domanda.</p>
        
<br />

<h4>Posso comprare i voli?</h4>
        <p>E' in genere vero che prima si prenota e meno si paga, occorre però avere il via libero da parte dell'Istituto, altrimenti rischiate di comprare invano. La certezza arriva nel momento in cui si riceve l'email con l'invito a ricercare ed inviare domande di lavoro, o meglio, se si vuole sindacare sul lavoro trovato, solo dopo essere stati accettati da Work Tutor di lavoro soddisfacente.</p>
        
        <p><strong>Consigliamo</strong> di non controllare più volte il prezzo del volo, di usare "navigazione in incognito", perchè tramite i Cookies il prezzo a voi esposto potrebbe aumentare ogni volta che semplicemente guardiate il prezzo.</p>
<br />

<h4>Avete altri consigli?</h4>
        <p>Nelle email che riceverete, ad ogni passo daremo i consigli necessari.</p>
        
        <p><strong>Consigliamo</strong> di leggere <a href="http://work-related-learning.com/faq/work" target="_blank"><strong>le FAQ</strong></a> (Frequently Asked Questions), sia per studenti ma anche per Work Tutors e School Houses. Le informazioni sono generiche in quanto possono riferirsi a partenze individuali o solo per servizi prestati dalle School Houses anche a minori di 16 anni.</p>
<br />

        <h4>Se il vostro Istituto Superiore non è nell'elenco</h4><strong></strong>
        <p>Ovvero quando dentro <a href="http://work-related-learning.com/user/edit" target="_blank"><strong>"Edit Profile"</strong></a> non trovate il vostro Istituto Superiore al punto "School Name"</p>
            <p>
            1) contattateci, scrivento a hello@work-related-learning.com;
            </p>
            <p>
            2) organizzeremo un incontro con il Tutor Interno (chiedete alla segreteria o al Responsabile di alternanza se non sapete ancora chi è) ed il Responsabile di Alternanza.
            </p>
            <p>Dovreste innanzitutto manifestare il vostro interesse.</p>
            <p>Fornirci contatto diretto con queste due persone affinchè possiamo spiegare come funziona a livello tecnico e la validità ai fini normativi.</p>    
            
         <br />
        
        <p>Vi chiediamo di aggiungere queste email: notification@work-related-learning.com, 
            hello@work-related-learning.com ai vostri contatti per assicurarvi che non vadano a finire nostri messaggi nella vostra casella di spam.</p>


        <br />
        <p>Vi auguriamo buona esperienza e buon lavoro!</p>
           <br />


        <div style="color: blue;">
            <p>--</p>
            <p>Best wishes</p>
            <p>Dr. Avanzi Matthew</p>
            <br />
            
            
            
            
            <p>IMAGE</p>
            
            
            
            
            
            
            <p>Learn in a FLASH Ltd</p>
            <p>20-22 Wenlock Road </p>
            <p>London</p>
            <p>N1 7GU</p>
            <p>United Kingdom</p>
            <br />
            <p><strong>  
                    Avoid missing our emails:</strong>
Add notification@work-related-learning.com and hello@work-related-learning.com to your list of desired contacts if you want to be sure that our emails are not classified as spam by your systems; this not to run the risk of missing out on important news or messages. You may also reply to this email to increase likelihood of getting all our emails but we will not reply to these. Contact your agents for any assistance or us for any matter concerning the Website.
</p>
<p><strong>  
                    Disclaimer:</strong>
This email and any files transmitted with it may be confidential, privileged or otherwise protected by work product immunity or legal rules and supposedly intended for the use of the individual or entity to whom they are addressed. If you have received this email in error, please notify the system manager; you may filter messages (marking as spam could be ineffective and illegal as we do not send spam) from our site in case somebody is using yours inappropriately, or you could register to our website to make sure to get only the messages for your experiences. The integrity and security of this message cannot be guaranteed as transmitted over the internet. The recipient should check this email and any attachments for the presence of viruses and malaware. Our emails may contain links to other websites of interest, once you have used these links, we cannot be held responsible for protection and privacy whilst visiting such sites.
            </p>

        </div>
    </body>
</html>



