<html>
    <head>
        <title>Work-Related-Learning - Application</title>
    </head>
    <body>
        <p> Hi 
            <?php
            $new_name = isset($propertyObject->hr_represent_name) ? $propertyObject->hr_represent_name . " " : "";
            $new_name = $new_name . isset($propertyObject->hr_represent_surname) ? $propertyObject->hr_represent_surname : "";
            if (isset($sent_to) && $sent_to == "notification") {
                $new_name = "Matthew Avanzi";
            }
            ?>
            {{$new_name}}, 
        </p>
        <br />
        <p>Herein Work Tutor and Student details, and links to accept or message student.</p>
        <br />

        Job will be for  period from 
        {{isset($rentalObj->checkin)?date("d-m-Y", strtotime($rentalObj->checkin)):""}} to 
        {{isset($rentalObj->checkout)?date("d-m-Y", strtotime($rentalObj->checkout)):""}}
        <br />
        
        <p><strong>Work Tutor Details:</strong></p>
        <div class="divbottom">
            @if(isset($propertyObject->work_org_same) and $propertyObject->work_org_same == 0)
            <div class="divbottom"><b>Name</b>: {{isset($propertyObject->represent_name) ?$propertyObject->represent_name:""}}</div> 
            <div class="divbottom"><b>Surname</b>: {{isset($propertyObject->represent_surname) ?$propertyObject->represent_surname:""}}</div> 
            <div class="divbottom"><b>e- mail</b>: {{isset($propertyObject->represent_email) ?$propertyObject->represent_email:""}}</div> 
            <div class="divbottom"><b>Telephone</b>: {{isset($propertyObject->represent_phone) ?$propertyObject->represent_phone:""}} </div> 
            @else
            <div class="divbottom"><b>Name</b>: {{isset($propertyObject->user->name)?$propertyObject->user->name:""}}</div> 
            <div class="divbottom"><b>Surname</b>: {{isset($propertyObject->user->lastname)?$propertyObject->user->lastname:""}}</div> 
            <div class="divbottom"><b>e- mail</b>: {{isset($propertyObject->user->email)?$propertyObject->user->email:""}}</div> 
            <div class="divbottom"><b>Telephone</b>: {{isset($propertyObject->user->phonenumber)?$propertyObject->user->phonenumber:""}} </div>
            @endif
        </div>
        
        <p><strong>Student details:</strong></p>
        <div>
            <ul style="width: 100%; border: 1px solid; list-style-type: none;">
                <li>
                    Student Surname and Name : 
                    <?php
                    if (isset($rentalObj->user->ssurname16) && !empty($rentalObj->user->ssurname16)) {
                        ?>
                        {!! isset($rentalObj->user->ssurname16)?$rentalObj->user->ssurname16:"" !!} {!! isset($rentalObj->user->sname16)?$rentalObj->user->sname16:"" !!}
                    <?php } else { ?>
                        {!! isset($rentalObj->user->lastname)?$rentalObj->user->lastname:"" !!} {!! isset($rentalObj->user->name)?$rentalObj->user->name:"" !!}
                    <?php } ?>
                </li>
                <li>
                    Date of Birth : 
                    <?php if (isset($rentalObj->user->dob16) && !empty($rentalObj->user->dob16)) { ?>
                        {{isset($rentalObj->user->dob16)?$rentalObj->user->dob16:""}}
                    <?php } else { ?>
                        {{isset($rentalObj->user->dob)?$rentalObj->user->dob:""}}
                    <?php } ?>
                </li>
                <li>
                    their Secondary School : {{isset($rentalObj->user->school_name)?$rentalObj->user->school_name:""}}
                </li>
                <!--                profile-about16 -> about_yourself
                medical_learning16 -> medical_learning-->
                <li>
                    Student Description : {{isset($rentalObj->user->about_yourself)?$rentalObj->user->about_yourself:""}}
                </li>
                <li>
                    Student Medical or Any Description : {{isset($rentalObj->user->medical_learning)?$rentalObj->user->medical_learning:""}}
                </li>
                <li>
                    Message for Work Tutor : {{isset($rentalObj->user->work_tutor_notes)?$rentalObj->user->work_tutor_notes:""}}
                </li>
            </ul>
        </div>
        <br />
        
           <strong>Kindly view request to:
               <a href="{{url('user/reservations')}}">Accept or Decline</a></strong>
        
        <br />
        <br />
        
        or further any requests and if you wish ask for an interview through our internal messaging system:
        <a href="{{url('user/messages')}}">Messaging</a>
        <br />
        <br />
        Best wishes,
        <br />
        Dr. Avanzi Matthew
        
        @include('emails.disclaimer')
    </body>
</html>
