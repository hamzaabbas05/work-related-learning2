<html>
<head>
<title>Work-Related-Learning</title>
</head>
<body>
<h2> Hi {{$name}} </h2>
<p>thank you for accepting our student.</p>

<h2>The student will bring the documentation</h2>
<p>You don't need to do anything as it will be the student to bring the Documentation necessary:</p>

<p>1) the &quot;Learning Agreement&quot; - which will be signed: by the student and, in case of underage students, by the parent or legal guardian; the student&#39;s high school Headmaster and School Tutor;<br />

<p>2) the &quot;Attendance Register&quot; - which the student will have to complete (time they start and finish every day with brief description of what done) and Work Tutor sign if truthful;</p>

<p>3) the &quot;Evaluation Form&quot; - which the Work Tutor will have to fill in before student leaves (four questions to be marked 1 to 4 or fail);</p>

<p>4) and any document you have ask for.</p>

<p>Best wishes<br />
Matthew Avanzi</p>

@include('emails.disclaimer')
</body>
</html>
 
 
 
