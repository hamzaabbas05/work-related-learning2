<html>
<head>
<title>Work-Related-Learning</title>
</head>
<body>

<h2> Hi {{$name}} </h2>

<p>You request {{$property_title}} {{$check_in}} {{$check_out}} has not received any reply.</p><br>
<p>This usually happens when a Work Tutor is unavailable or has already accepted another student and has not completed bureaucracy by clicking on "decline" to your application.</p><br>
<p>Please send new requests: www.work-related-learning.com</p><br>
<p>Best wishes</p><br>
<p>Matthew Avanzi</p><br>
</br>

</br>
 @include('emails.disclaimer')
</body>
</html>
 