<html>
    <head>
        <title>Work-Related-Learning Job Review Paid System</title>
    </head>
    <body>
        <p> 
            Hi {{isset($user_detail->name)?$user_detail->name:''}} ,
        </p>
        <br />
        <p>
            Thank you for your recent visit to our location. We want to provide you with the best experience possible!
        </p>
        
        <br />
        <p>
            Would you consider posting and online review for us? In addition to providing feedback to our team, online reviews can help other customers learn about who we are and about the services we offer. They are also a great way to give referrals to your family and friends.
        </p>
        <br />
        <p>Please take a moment to leave your feedback, we truly appreciate it!</p>
        <br />
        <p>
            Click here to review 
            <a href="{{URL::to('user/addreview/'.base64_encode($property->rental_id))}}">Review Job</a>
        </p>
        

       <br />
       <p>Thank you for being our customer and for your feedback.</p>

        <br />
        <br />
        Best wishes,
        <br />
        Prof. Avanzi Matthew

        @include('emails.disclaimer')
    </body>
</html>
