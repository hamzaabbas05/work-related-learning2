<html>
    <head>
        <title>Work-Related-Learning Work Experience Cancellation</title>
    </head>
    <body>
        <p> 
            Hi {{$name}},
        </p>
        <br />
        <p>Herein Work Tutor and Student details for the job which has been cancelled.</p>
        <br />
        <p>If student has not yet started his experience you don't need to do anything.</p>
        <br />
        <p>If student has cancelled half way though the experience, you should anyway provide updated Register and final Evaluation.</p>
        <!-- only this extra in FUTURE We could add check IF date()>Date_IN then send first line ELSE send second
        -->
        <br />

        Job was for period from 
        {{isset($rentalObj->checkin)?date("d-m-Y", strtotime($rentalObj->checkin)):""}} to 
        {{isset($rentalObj->checkout)?date("d-m-Y", strtotime($rentalObj->checkout)):""}}

        <br />
        <p><strong>Work Tutor Details:</strong></p>
        <div class="divbottom">
            @if(isset($rentalObj->property->work_org_same) && $rentalObj->property->work_org_same == 0)
            <div class="divbottom"><b>Name</b>: {{isset($rentalObj->property->represent_name) ? $rentalObj->property->represent_name:"No Name"}}</div> 
            <div class="divbottom"><b>Surname</b>: {{isset($rentalObj->property->represent_surname) ? $rentalObj->property->represent_surname:"No Surname"}}</div> 
            <div class="divbottom"><b>e- mail</b>: {{isset($rentalObj->property->represent_email) ? $rentalObj->property->represent_email:"No Email"}}</div> 
            <div class="divbottom"><b>Telephone</b>: {{isset($rentalObj->property->represent_phone) ? $rentalObj->property->represent_phone:"N/A"}} </div> 
            @else
            <div class="divbottom"><b>Name</b>: {{isset($rentalObj->property->user->name)?$rentalObj->property->user->name:"no user"}}</div> 
            <div class="divbottom"><b>Surname</b>: {{isset($rentalObj->property->user->lastname)?$rentalObj->property->user->lastname:"no user"}}</div> 
            <div class="divbottom"><b>e- mail</b>: {{isset($rentalObj->property->user->email)?$rentalObj->property->user->email:"no user"}}</div> 
            <div class="divbottom"><b>Telephone</b>: {{isset($rentalObj->property->user->phonenumber)?$rentalObj->property->user->phonenumber:"no user"}} </div>
            @endif
        </div>

        <br />

        <p><strong>Student details:</strong></p>
        <div>
            <ul style="width: 100%; border: 1px solid; list-style-type: none;">
                <li>
                    Student Surname and Name : 
                    <?php
                    if (isset($rentalObj->user->ssurname16) && !empty($rentalObj->user->ssurname16)) {
                        ?>
                        {!! isset($rentalObj->user->ssurname16)?$rentalObj->user->ssurname16:"" !!} {!! isset($rentalObj->user->sname16)?$rentalObj->user->sname16:"" !!}
                    <?php } else { ?>
                        {!! isset($rentalObj->user->lastname)?$rentalObj->user->lastname:"" !!} {!! isset($rentalObj->user->name)?$rentalObj->user->name:"" !!}
                    <?php } ?>
                </li>
                <li>
                    Date of Birth : 
                    <?php if (isset($rentalObj->user->dob16) && !empty($rentalObj->user->dob16)) { ?>
                        {{isset($rentalObj->user->dob16)?$rentalObj->user->dob16:""}}
                    <?php } else { ?>
                        {{isset($rentalObj->user->dob)?$rentalObj->user->dob:""}}
                    <?php } ?>
                </li>
                <li>
                    Secondary School : {{isset($rentalObj->user->school_info->name)?$rentalObj->user->school_info->name:""}}
                </li>
                <!--                profile-about16 -> about_yourself
                medical_learning16 -> medical_learning-->
                <li>
                    Student Description : {{isset($rentalObj->user->about_yourself)?$rentalObj->user->about_yourself:""}}
                </li>
                <li>
                    Student Medical or Any Description : {{isset($rentalObj->user->medical_learning)?$rentalObj->user->medical_learning:""}}
                </li>
                <li>
                    Message for Work Tutor : {{isset($rentalObj->user->work_tutor_notes)?$rentalObj->user->work_tutor_notes:""}}
                </li>
            </ul>
        </div>
        <br/>
        @include('emails.disclaimer')
    </body>
</html>
