<html>
<head>
  <title>Buying Nights</title>
</head>
    <body style="margin:0px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#4BBEFF" data-bgcolor="body-bg-dark" data-module="1" class="ui-sortable-handle currentTable" onload="window.print()">  
    <tbody><tr>
     <td>
     <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="devicewidth" style="background-color:#ffffff;" data-bgcolor="light-gray-bg"> 
     <tbody><tr>
     <td height="30" bgcolor="#4BBEFF" >&nbsp;</td> 
     </tr>  
     <tr>  
    <td align="center">          
     <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">              
     <tbody><tr style="padding: 10px 10px 0px 10px; float: left">          
               
     <td align="center" valign="top">
                    <table width="650" border="0" cellpadding="5" cellspacing="1" >
                            <tbody style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;">
                            <tr>

                             <th width="70" bgcolor="#4BBEFF"style="color:#fff; font-size:15px;"><img src="{{(url('images/favicon/2017-01-18-16-40-34-LOGO - WRL - work-related-learning.com - largest-bolt.png'))}}" alt="Receipt" width=50 height=50></th>
                              <th width="75" ></th>
                              <th width="75"></th>
                              <th width="75"></th>
                              <th align="right" width="75" style="color:#f3402e; text-align:right"><a onClick="window.print()" TARGET="_blank" style="cursor: pointer; cursor: hand;text-decoration:underline;">Print Receipt</a></th>
                            </tr>
                </tbody></table>
         <table width="650" border="0" cellpadding="5" cellspacing="1" >
                            <tbody style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;">
                            <tr>                              
                              <td width="120" colspan=2>To : {{$userNameInfo}}</td>
                              <td width="100" colspan=2>{{$userNameInfo1}}</td>
                              <!-- <td width="60"></td> -->
                              <td align="right" width="120" style="color:#f3402e; text-align:left">RECEIPT #{{$billerId}} <br>Date: {{date("d/m/Y")}}</td>
                            </tr>                            
                </tbody></table>
          <table width="650" border="0" cellpadding="5" cellspacing="1" >
              <tbody style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;">
                  <tr>                              
                      <td width="80">Tax number {{$userTaxNo}}</td>
                      <!-- <td width="60"></td> -->
                      <td width="80">Tax number {{$userTaxNo1}}</td>
                      <td width="50"></td>
                      <td width="60"></td>
                      <td align="right" width="120" style="color:#f3402e; text-align:left"></td>
                  </tr>                            
              </tbody>
         </table>
                </td>       
     </tr>          
     </tr>
     <tr><td align="left" style="color:#4c4c4c;font-size:13px;font-family:Open Sans, Arial, Helvetica, sans-serif;margin:10px; padding: 10px">Booking Number : {{$billerId}}</td></tr>
     <tr><td align="left" style="color:#4c4c4c;font-size:13px;font-family:Open Sans, Arial, Helvetica, sans-serif;margin:10px; padding: 10px">Nights paid for : {{$title}}</td></tr>';
     <!-- <tr><td align="left" style="color:#4c4c4c;font-size:13px;font-family:Open Sans, Arial, Helvetica, sans-serif;margin:10px; padding: 10px">Address : '.$streetaddress.'</td></tr> -->
    <!--  // $messageview .= '<tr>
     // <td style="border-top:1px solid #808080" bgcolor="#fff">&nbsp;</td>       
     // </tr>        
     // <tr>         
     // <td>
     // <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">              
     // <tbody><tr style="padding: 10px; float: left">          
               
     // <td align="center" valign="top">
     //                <table width="650" border="0" cellpadding="5" cellspacing="1" >
     //                        <tbody style="font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;" ><tr>
     //                          <th width="75" bgcolor="#EFEFEF">Check In</th>
     //                         <th width="5"></th>
     //                          <th width="75" bgcolor="#EFEFEF">Check Out</th>
     //                          <th width="75" ></th>
     //                          <th width="75" bgcolor="#EFEFEF">'.$Night.'</th>
                              
     //                        </tr>';
                            // <tr align="center">
                            //     <td >'.$checkindate.'</td>
                            //     <td ></td>
                            //     <td >'.$checkoutdate.'</td>
                            //     <td ></td>
                            //     <td >'.$Night.'</td>
                            //   </tr>
     //            </tbody></table>
     //            </td>       
     // </tr>          
     // </tbody>
     // </table>  
     // </td>
     // </tr>'; -->
     
     <!-- <tr style="pointer-events:none;">
//      <td align="center" valign="top" style="color:#000; font-weight: 700; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;" data-size="body-text" data-min="10" data-max="25" data-color="footer-text">
// <img id="map-image" border="0" alt="'.$streetaddress.'" src="https://maps.googleapis.com/maps/api/staticmap?center='.$streetaddress.'&zoom=13&size=600x300&maptype=roadmap&sensor=false&format=png&visual_refresh=true&markers=size:mid%7Ccolor:red%7C">  
//      </td> 
//     </tr>  -->
    <tr>      
     <td>&nbsp;</td>      
     </tr>       
          
     <tr>   
      <tr>         
 <td align="center" >          
 <table width="100%" border="0" cellspacing="1" cellpadding="0" align="center" style="padding:0px 10px;">              
 <tbody>
   <!--  <tr>          
            
 <td align="left" width="300px" valign="top" style="color:#4f595b; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px; line-height:20px;" data-size="body-text" data-min="10" data-max="25" data-color="footer-text">
<h4 style="float: left; width:100%;">Cancellation Policy  -    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </h4>For More details of the cancellation policy, please refer <a href="/pages/cancellation-policy" target="_blank">cancellation policy</a>.
 <td>

 </tr>       -->


<tr>          
      
 <td align="left" width="300px" valign="top" style="color:#4f595b; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px; line-height:20px;" data-size="body-text" data-min="10" data-max="25" data-color="footer-text">
<h4 style="float: left; width:100%; margin: 10px 0px;">Billing</h4>
<table style="width:100%; font-size:13px;">
  <tr>
    <td style="border-bottom: 1px solid #bbb;">Nights paid for  &nbsp;{{$Night}}</td>
    <td style="border-bottom: 1px solid #bbb;"></td>        
    <td style="border-bottom: 1px solid #bbb; padding: 5px 0px;">€ {{$TotalwithoutService}}</td>
  </tr>
  <tr>
<td style="border-bottom: 1px solid #bbb;">Service Fee</td>
    <td style="border-bottom: 1px solid #bbb;"></td>        
    <td style="border-bottom: 1px solid #bbb; padding: 5px 0px;">€ {{$servicefee}}</td>
  </tr>
  <tr>
<td style="border-bottom: 1px solid #bbb;  padding: 10px 0px;">Total</td>
    <td style="border-bottom: 1px solid #bbb;padding: 10px 0px;"></td>      
    <td style="border-bottom: 1px solid #bbb;padding: 10px 0px;">€ {{$TotalAmt}}</td>
  </tr>
</table>

<td>
 </tr> 
 </tbody>
 </table>      
 </td>        
 </tr> 
     </tr>
            
     <tr>      
     <td>&nbsp;</td>     
     </tr>       
     <!-- <tr>    
     <td align="center" valign="middle" style="color:#444444; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px;"><a href="javascript:void(0);" style="color:#0094aa; text-decoration:none;" data-size="body-text" data-min="10" data-max="25" data-link-color="plain-url-color" data-link-size="plain-url-text">(Remember: Not responding to this booking will result in your listing being ranked lower.)</a></td>       
     </tr>         -->
     <tr>        
     <td>&nbsp;</td>   
     </tr>              
     <tr>               
     <td align="center" valign="middle" style="color:#444444; font-family:Open Sans, Arial, Helvetica, sans-serif; font-size:13px; padding:0 20px;" data-size="body-text" data-min="10" data-max="25" data-color="body-text">If you need help or have any questions, please contact <a href="mailto:esperienzainglese@gmail.com"   style="color:#0094aa;" data-link-color="plain-url-color"> esperienzainglese@gmail.com </a></td>     
     </tr>       
     <tr>       
     <td height="50">&nbsp;</td>      
     </tr>         
     <tr>       
     <td height="30" bgcolor="#fff">&nbsp;</td>     
     </tr>      
     <tr>         
     <td align="center" bgcolor="#fff">          
     <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="padding:0px 10px;">              
     <tbody>
     <tr>
        <td width="20"> &nbsp; </td>
        <td width="100" style="text-align: center;"> Receipt by: Learn in a FLASH ltd - #11601088 SIC 85600 
20/22 Wenlock Road London N1 7GU United Kingdom
Tel: (+44) 020-3432 2230 Web: <a href="https://www.work-related-learning.com" target="_blank">www.work-related-learning.com</a> E-mail: <a href="mailto:hello@work-related-learning.com">hello@work-related-learning.com</a>
 </td>
        <td width="20"> &nbsp; </td>
     </tr>
     <tr>
        <td width="20"> &nbsp; </td>
    </tr>
     </tbody>
     </table>      
     </td>        
     </tr>         
     <tr>
     <td height="30" bgcolor="#4BBEFF" >&nbsp;</td> 
     </tr> 
     </tbody></table> 
     </td>      </tr>  
     </tbody></table>
     </body>    
</html>
<?php //exit; ?>
