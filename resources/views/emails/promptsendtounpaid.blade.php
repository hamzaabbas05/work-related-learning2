<html>
<head>
<title>Work-Related-Learning</title>
</head>
<body> 
<p> Hi {{$name}} </p>
<p>con la presente informiamo che non ci risulta ancora inviata alcuna richiesta per trovare un posto di lavoro (il controllo è settimanale).</p>

<p>Vi invitiamo pertanto a procedere tramite il portale <a href="https://www.work-related-learning.com"">www.work-related-learning.com</a></p>

<p>Potete controllare nelle vostre email la ricezione di una contenente le istruzioni su come procedere, nell'oggetto dell'email troverete il testo "Istruzioni completamento iscrizione al portale ed invio domande di lavoro".</p>

<p>Consigliamo di inviare le richieste il prima possibile, possono occorrere settimane per gestire tutte le richieste, ma comunque una alla volta, ovvero date il tempo alla prima di rispondere prima di inviare alla seconda e terza scelta,</p> 

<p>Potete usare la funzione "Wishlist" cliccando sul cuore nell'angolo in alto a destra per salvare quelle preferite.</p>

<p>Best Wishes </p></br>
<p>Matthew Avanzi</p></br>

</br>
 @include('emails.disclaimer')
</body>
</html>
 