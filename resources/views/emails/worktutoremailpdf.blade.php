<html>
    <head>
        <title>Work-Related-Learning</title>
    </head>
    <body>
        <p>Dear Work Tutor,</p>
        <p>hereunder details for student arriving:</p>
        <br />

        <p>Student will be taking "Learning Agreement", register to confirm real amount of hours worked and evaluation form.</p>

        <br />

        <p>Keep in mind you should only sign the first two as the first will be precompiled by the system and you should ask student to fill in the register. 
            Last the evaluation form requires 4 circles which count towards a school mark for the student.</p>
        <br />

        <h2><strong>Student details</strong></h2>
        <div>
            <table style="width: 100%; border: 1px solid;">
                <tr>
                    <td>Date of Birth</td>
                    <td>Place of Birth</td>
                    <td>Gender</td>
                    <td>Class</td>
                    <td>Surname and Name</td>
                    <td>Mobile (0039) for Italy</td>
                    <td>Email</td>
                    <td>Booking No #</td>
                </tr>
                <tr>
                    <?php
                    if (isset($rentalObj->user->sname16) && !empty($rentalObj->user->sname16)) {
                        $dob = isset($rentalObj->user->dob16) ? $rentalObj->user->dob16 : "";
                        $pob = isset($rentalObj->user->pob16) ? $rentalObj->user->pob16 : "";
                        $gender = isset($rentalObj->user->gender16) ? $rentalObj->user->gender16 : "";
                        $gender16 = true;
                        //sname16
                        $lastname = isset($rentalObj->user->ssurname16) ? $rentalObj->user->ssurname16 . " " : "";
                        $firstname = isset($rentalObj->user->sname16) ? $rentalObj->user->sname16 : "";
                        $name = $lastname . $firstname;
                        $contact = isset($rentalObj->user->sphone16) ? $rentalObj->user->sphone16 : "";
                        $email = isset($rentalObj->user->semail16) ? $rentalObj->user->semail16 : "";
                        $description = isset($rentalObj->user->about_yourself) ? $rentalObj->user->about_yourself : "";
                        $med_condition = isset($rentalObj->user->medical_learning) ? $rentalObj->user->medical_learning : "";

                        // about_yorself is correctly "Student Description"
                    } else {
                        $dob = isset($rentalObj->user->dob) ? $rentalObj->user->dob : "";
                        $pob = isset($rentalObj->user->basicpob) ? $rentalObj->user->basicpob : "";
                        $gender = isset($rentalObj->user->gender) ? $rentalObj->user->gender : "";
                        $gender16 = false;
                        //name
                        $lastname = isset($rentalObj->user->lastname) ? $rentalObj->user->lastname . " " : "";
                        $firstname = isset($rentalObj->user->name) ? $rentalObj->user->name : "";
                        $name = $lastname . $firstname;
                        $contact = isset($rentalObj->user->phonenumber) ? $rentalObj->user->phonenumber : "";
                        $email = isset($rentalObj->user->email) ? $rentalObj->user->email : "";
                        $description = isset($rentalObj->user->about_yourself) ? $rentalObj->user->about_yourself : "";
                    }
                    ?>
                    <td>{{$dob}}</td>
                    <td>{{$pob}}</td>
                    <td>
                        @if($gender16 == false)
                        @if($gender == "1")
                        Male
                        @elseif($gender == "2")
                        Female
                        @elseif($gender == "3")
                        Other
                        @endif
                        @else
                        {{$gender}}
                        @endif
                    </td>
                    <td>{!! isset($rentalObj->user->class_letter)?$rentalObj->user->class_letter:"" !!} {!! isset($rentalObj->user->class_number)?$rentalObj->user->class_number:"" !!}</td>
                    <td>{!! $name !!}</td>
                    <td>{!! $contact !!}</td>
                    <td>{!! $email !!}</td>
                    <td>{!! $rentalObj->Bookingno !!}</td>
                </tr>
            </table>
        </div>
        <br />

        <h4><strong>Student Description</strong></h4>
        <p>{!! isset($rentalObj->user->about_yourself)?$rentalObj->user->about_yourself:"" !!}</p>
        <br />
        <h4><strong>Medical (or any) Condition</strong></h4>
        <p>{!! isset($rentalObj->user->medical_learning)?$rentalObj->user->medical_learning:"" !!}</p>
        <br />
        <h4><strong>Notes for work tutor</strong></h4>
        <p>{!! isset($rentalObj->user->work_tutor_notes)?$rentalObj->user->work_tutor_notes:"" !!}</p>
        <br />


        <h2><strong>School Tutor</strong></h2>
        <p>
            {{isset($rentalObj->user->school_tutor_name)?$rentalObj->user->school_tutor_name:""}} 
            {{isset($rentalObj->user->school_tutor_sur_name)?$rentalObj->user->school_tutor_sur_name:""}} - 
            Mobile : {{isset($rentalObj->user->school_tutor_number)?$rentalObj->user->school_tutor_number:""}} - 
            Email : {{isset($rentalObj->user->school_tutor_email)?$rentalObj->user->school_tutor_email:""}}
        </p>
        <br />


        <p>
            Please first refer to us for any issue:
        </p>
        <p>
            Dr. Matthew Avanzi - flash@madrelingua-inglese.it - landline: 00442034322230 (we call back) - in case of emergency: 00393348498788 (also on Whatsapp).
        </p>
        <br />
        <p>
        <h2><strong>
                Please click here to confirm receipt of this email, thanks
                <a href="{{URL::to('jobEmailConfirm/'.$token)}}">
                    Click to Confirm
                </a>
            </strong></h2>
    </p>
    <p>
        I am sure the student will be of great help to your organisation.</p>
    <br />

    <br />
    <p>Best wishes,</p>
    <p>Matthew Avanzi</p>   
    <br />
    <div style="color: blue;">
        <p>--</p>
        <p>Dr. Matthew Avanzi</p>
        <br />
        <p>Avoid missing our emails:</p>
        <br />
        @include('emails.disclaimer')
    </div>
</body>
</html>



