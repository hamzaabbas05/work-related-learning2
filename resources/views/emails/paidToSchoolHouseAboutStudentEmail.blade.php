<html>
    <head>
        <title>Work-Related-Learning paidToSchoolHouseAboutStudentEmail</title>
    </head>
    <body>
        <p>Dear developing School House,</p>
        <p>
            hereunder the details of the student/s.
        </p>
        <br />

        <h4><strong>Pick up</strong></h4>
        <p>
            <strong>
                {{isset($rentalObj->user->group_info->pick_up_where)?$rentalObj->user->group_info->pick_up_where:''}} 
            </strong> at <strong> {{isset($rentalObj->user->group_info->pick_up_time)?$rentalObj->user->group_info->pick_up_time:''}} 
            </strong> Local time.
        </p>
        <p>(Students will be getting there 
            the {{isset($rentalObj->user->group_info->catching_the_what_time_bus_to)?$rentalObj->user->group_info->catching_the_what_time_bus_to:''}} 
            {{isset($rentalObj->user->group_info->catching_the_what_bus_to)?$rentalObj->user->group_info->catching_the_what_bus_to:''}}
        </p>
        <br />

        <h4><strong>Drop Off</strong></h4>
        <p>
            <strong>
                {{isset($rentalObj->user->group_info->drop_off_where)?$rentalObj->user->group_info->drop_off_where:''}} 
            </strong> at <strong> {{isset($rentalObj->user->group_info->drop_off_time)?$rentalObj->user->group_info->drop_off_time:''}} 
            </strong> Local time.
        </p>
        <p>(so to catch the 
            {{isset($rentalObj->user->group_info->catching_the_what_time_bus_back)?$rentalObj->user->group_info->catching_the_what_time_bus_back:''}} 
            {{isset($rentalObj->user->group_info->catching_the_what_bus_back)?$rentalObj->user->group_info->catching_the_what_bus_back:''}} 
            )
        </p>
        <br />

        <h4><strong>Student Details:</strong></h4>
        <ul style="width: 100%; border: none; list-style-type: none; padding: 0px 10px 0px 10px;">
            <?php
            if (isset($rentalObj->user->sname16) && !empty($rentalObj->user->sname16)) {
                $dob = isset($rentalObj->user->dob16) ? $rentalObj->user->dob16 : "";
                $gender = isset($rentalObj->user->gender16) ? $rentalObj->user->gender16 : "";
                $gender16 = true;
                //sname16
                $lastname = isset($rentalObj->user->ssurname16) ? $rentalObj->user->ssurname16 . " " : "";
                $firstname = isset($rentalObj->user->sname16) ? $rentalObj->user->sname16 : "";
                $name = $lastname . $firstname;
                $contact = isset($rentalObj->user->sphone16) ? $rentalObj->user->sphone16 : "";
                $email = isset($rentalObj->user->semail16) ? $rentalObj->user->semail16 : "";
                $description = isset($rentalObj->user->about_yourself) ? $rentalObj->user->about_yourself : "";
                $med_condition = isset($rentalObj->user->medical_learning) ? $rentalObj->user->medical_learning : "";

                // about_yorself is correctly "Student Description"
            } else {
                $dob = isset($rentalObj->user->dob) ? $rentalObj->user->dob : "";
                $gender = isset($rentalObj->user->gender) ? $rentalObj->user->gender : "";
                $gender16 = false;
                //name
                $lastname = isset($rentalObj->user->lastname) ? $rentalObj->user->lastname . " " : "";
                $firstname = isset($rentalObj->user->name) ? $rentalObj->user->name : "";
                $name = $lastname . $firstname;
                $contact = isset($rentalObj->user->phonenumber) ? $rentalObj->user->phonenumber : "";
                $email = isset($rentalObj->user->email) ? $rentalObj->user->email : "";
                $description = isset($rentalObj->user->about_yourself) ? $rentalObj->user->about_yourself : "";
            }
            ?>
            <li><b>Date of Birth</b>: {{$dob}}</li>
            <li><b>Gender</b>: 
                @if($gender16 == false)
                @if($gender == "1")
                Male
                @elseif($gender == "2")
                Female
                @elseif($gender == "3")
                Other
                @endif
                @else
                {{$gender}}
                @endif
            </li>
            <li><b>Surname and Name</b>: {{$name}}</li>
            <li><b>Mobile (0039) for Italy</b>: {{$contact}}</li>
            <li><b>Email</b>: {{$email}}</li>
            <li><b>Student Description</b>: {{$description}}</li>
            <li><b>Medical (or any) Condition</b>: {{isset($rentalObj->user->medical_learning) ? $rentalObj->user->medical_learning: ""}}</li>
            <li><b>Notes for Host</b>: {{isset($rentalObj->user->host_family_notes)?$rentalObj->user->host_family_notes:""}}</li>
            <li><b>Booking No # </b>: {{isset($rentalObj->Bookingno)?$rentalObj->Bookingno:""}}</li> <!-- as its a paid email and the job which is booekd is paid than automatically here the rental details will refer to paid job. and if paid than this paidToSchookHouse email will send else not -->
        </ul>
        <br />
        <p><strong>Emergencies</strong> </p>
        <p>Please refer to us first to solve any emergency:
            <br />
            Dr. Matthew Avanzi: email: flash@madrelingua-inglese.it UK landline: 00442034322230 (here we call back, no text) - in case of emergency 00393348498788 (also on Whatsapp).
            <br />
            We have a local agent who at a cost will assist students, for example in case of loss of a travel document (i.e. valid I.D.) student will be taken to London for necessary paperwork, or in case student needs to be assisted at hospital, police station and so on.
        </p>
        <br />


        <p><strong> Travelling with the students </strong></p> 
        <p>Name and Surname: {{isset($rentalObj->user->school_tutor_name)?$rentalObj->user->school_tutor_name:""}};</p>
        <p>Contact: {{isset($rentalObj->user->school_tutor_number)?$rentalObj->user->school_tutor_number:""}}; </p>
        <p>email: {{isset($rentalObj->user->school_tutor_email)?$rentalObj->user->school_tutor_email:""}}.</p>
        <p>Keep in mind these people are representative of our clients, not part of our organisation, they evaluate the quality of the services we provide.
        </p>
        <p>
            They are there in case of need at work, not anywhere else, 
            but thanks to the daily interaction with students we are sure you will be able to solve most issues, 
            as usually they arise just due to communication barriers, between Work Tutors and Students.
        </p>
        <br />



        <p><strong>Emergency Contacts provided by Student (probably their Parents): </strong></p>    
        <p>Relationship: {{isset($rentalObj->user->emergency_contact_relationship)?$rentalObj->user->emergency_contact_relationship:""}}</p>
        <p>Name and Surname: {{isset($rentalObj->user->emergency_contact_name)?$rentalObj->user->emergency_contact_name:""}}</p>
        <p>Contact: {{isset($rentalObj->user->emergency_contact_phone)?$rentalObj->user->emergency_contact_phone:""}}</p>
        <p>email: {{isset($rentalObj->user->emergency_contact_email)?$rentalObj->user->emergency_contact_email:""}}</p>
        
        <p>Contact in case the student breaches your School House rules, i.e. if they do not respect curfew time.
        </p>
        <br />
            <!-- <p>These the details of the person accompanying the student on his trip: </p>
        
        
               //Accompanying
        {{isset($rentalObj->user->school_tutor_name)?$rentalObj->user->school_tutor_name:""}}
        <br />
        {{isset($rentalObj->user->school_tutor_sur_name)?$rentalObj->user->school_tutor_sur_name:""}}
        <br />
        {{isset($rentalObj->user->school_tutor_email)?$rentalObj->user->school_tutor_email:""}}
        <br />
        {{isset($rentalObj->user->school_tutor_number)?$rentalObj->user->school_tutor_number:""}}
        <br />-->

        <p>The students parents allow him or her to leave the School House in the evening till 10.00 pm: </p>
        @if (isset($rentalObj->user->student_relationship) && empty($rentalObj->user->student_relationship) ) 
        <p>
            The student <strong>is over 18</strong> so he or she does not need Parental consent to be allowed to go out without adult supervision. 
            <br /> 
            Students must comply with your School House rules but you may agree directly on different curfew times.
        </p>
        <p>The student <strong>is allowed</strong> to go out without adult supervision</p>
        @elseif(isset($rentalObj->user->check_allow) && $rentalObj->user->check_allow == 1) 
        <!--        we need other check, if student over 18 it ill erroneously conclude in the else
        didnt get the point if student is over 18 than ?
        he is legally by definition allowed to go out but point is logic, student over 18 has not seen this option
        so why else him into NOT allowed_ 
        clear_
        okay so i will add check if user is over 18 he will not see this check or is allowed to go out right ?
inthe past we have used this check student_relationship-->
        <p>The student <strong>is allowed</strong> to go out without adult supervision. Curfew time is set at 10.00 pm for students who are under 18 (and over 16)</p>
        @else 
        <p>The student <strong>is NOT allowed</strong> to go out without adult supervision.</p>
        @endif
        <br />

        <h4><strong>We advise students to make a call few days before arriving, please make sure they do and not only contact via text or email (prompt via email for them to call you)</strong></h4>
        <br />

        <!-- Work Tutor Infomation  -->
        <p><strong></strong></p> 


        <div class="divbottom" ><h3>Students will be working at:</h3></div> 
       <!-- if he has cancelled it should nto show_ means if cancelled by listing owner then it should not sent email?? not show in email if canelled see logic here it is only showing when accept  -->
        <?php
        if (isset($rentalObj->user_id)) {
            $user_booking = \App\RentalsEnquiry::where(["user_id" => $rentalObj->user_id, "approval" => "Accept","cancelled"=>"No"])->where("checkin", ">=", date("Y-m-d"))->orderBy('checkin', 'ASC')->get();
             $user_has_booking = \App\RentalsEnquiry::where(["user_id" => $rentalObj->user_id, "approval" => "Accept","cancelled"=>"No"])->where("checkin", ">=", date("Y-m-d"))->orderBy('checkin', 'ASC')->count();
            // HOW we can test this? generate accept for student with multilple accept and after cancelling
            if ($user_has_booking > 0) { 
                foreach ($user_booking as $booking) {
                    if (isset($booking->property->property_status) && ($booking->property->property_status == "unpaid" || $booking->property->property_status == "unpaidflash")) {
                        ?>
                        <b>Organisation details:</b>
                        <div class="divbottom"><b>Name</b>: {{isset($booking->property->user->orgname)?$booking->property->user->orgname:""}}</div>
                        @if(isset($booking->property->addr_work_org_same) and $booking->property->addr_work_org_same == 0) 
                        <div class="divbottom"><b>Address</b>: {{isset($booking->property->workplace_address)?$booking->property->workplace_address:""}}</div> 
                        <div class="divbottom"><b>Email</b>: {{isset($booking->property->location_email)?$booking->property->location_email:""}}</div> 
                        <div class="divbottom"><b>Telephone</b>: {{isset($booking->property->location_telephone)?$booking->property->location_telephone:""}}</div>
                        @else
                        <div class="divbottom"><b>Address</b>: {{isset($booking->property->user->raddress18)?$booking->property->user->raddress18:""}}</div> 
                        <div class="divbottom"><b>Email</b>: {{isset($booking->property->user->email)?$booking->property->user->email:""}}</div> 
                        <div class="divbottom"><b>Telephone</b>: {{isset($booking->property->user->orgphone)?$booking->property->user->orgphone:""}}</div>
                        @endif

                        <b>Represented by (as Work Tutor):</b>
                        @if(isset($booking->property->work_org_same) and $booking->property->work_org_same == 0)

                        <div class="divbottom"><b>Name</b>: {{isset($booking->property->represent_name) ?$booking->property->represent_name:""}}</div> 
                        <div class="divbottom"><b>Surname</b>: {{isset($booking->property->represent_surname) ?$booking->property->represent_surname:""}}</div> 
                        <div class="divbottom"><b>e- mail</b>: {{isset($booking->property->represent_email) ?$booking->property->represent_email:""}}</div> 
                        <div class="divbottom"><b>Telephone</b>: {{isset($booking->property->represent_phone) ?$booking->property->represent_phone:""}} </div> 
                        @else
                        <div class="divbottom"><b>Name of Work Tutor</b>: {{isset($booking->property->user->name)?$booking->property->user->name:""}}</div> 
                        <div class="divbottom"><b>Surname of Work Tutor</b>: {{isset($booking->property->user->lastname)?$booking->property->user->lastname:""}}</div> 
                        <div class="divbottom"><b>e- mail</b>: {{isset($booking->property->user->email)?$booking->property->user->email:""}}</div> 
                        <div class="divbottom"><b>Telephone</b>: {{isset($booking->property->user->phonenumber)?$booking->property->user->phonenumber:""}} </div> 
                        @endif
                        <b>Placement dates:</b>                  
                        <div class="divbottom"><b>Starting date (unless closed for Holiday/day off, see Learning Agreement with Student in case)</b>: {{date("F j, Y",strtotime($booking->checkin))}}</div> 
                        <div class="divbottom"><b>Finishing date</b>: {{date("F j, Y",strtotime($booking->checkout))}}</div> 
                        <b>Reference number:</b> 
                        <div class="divbottom"><b>Booking Number</b>: {{$booking->Bookingno}}</div>

                        <br />
                        <hr />
                        <?php
                    }
                }
            }
        }
        ?>
        <h4><strong>Please remember to make a call to the Work Tutor few days before placement stating date</strong> to make sure everything is fine (Work Tutors remembers/is still working/is not on sick leave and so on) as application may have been accepted some time ago.</h4>
        <br />

        <br />
        <h2><strong>
                Please click here to confirm receipt of this email, thanks
                <a href="{{URL::to('jobEmailConfirm/'.$token)}}">
                    Click to Confirm
                </a>
            </strong>
        </h2>

        <br />

        <br />
        <p>Best wishes,</p>
        <p>Matthew Avanzi</p>   
        <br />
        <div style="color: blue;">
            <p>--</p>
            <p>Dr. Matthew Avanzi</p>
            <br />
            <p>Avoid missing our emails:</p>
            <br />
            @include('emails.disclaimer')
        </div>
    </body>
</html>



