<html>
    <head>
        <title>School House service request - to be accepted or declined</title>
    </head>
    <body>
        <h2> 
            Hi 
            <?php
            $new_name = $name;
            if (isset($sent_to) && $sent_to == "worktutor") {
                $new_name = isset($propertyObject->represent_name) ? $propertyObject->represent_name : $name;
            } else if (isset($sent_to) && $sent_to == "userlisting") {
                $new_name = isset($renter_user->name) ? $renter_user->name : $name;
            }
            ?>
            {{$new_name}} </h2>
        <p>your School House has received a request.</p>
        <br />

        Student requested for this period from {{isset($rentalObj->checkin)?$rentalObj->checkin:""}} to {{isset($rentalObj->checkout)?$rentalObj->checkout:""}}
        <br />

        
        <p>Student details:</p>
        
        <div>
            <ul style="width: 100%; border: 1px solid; list-style-type: none;">
                <li>
                    Student Name: 
                    <?php
                    if (isset($rentalObj->user->ssurname16) && !empty($rentalObj->user->ssurname16)) {
                        ?>
                        <!--{!! isset($rentalObj->user->ssurname16)?$rentalObj->user->ssurname16:"" !!}-->
                        {!! isset($rentalObj->user->sname16)?$rentalObj->user->sname16:"" !!}
                    <?php } else { ?>
                        <!--{!! isset($rentalObj->user->lastname)?$rentalObj->user->lastname:"" !!}-->
                        {!! isset($rentalObj->user->name)?$rentalObj->user->name:"" !!}
                    <?php } ?>
                </li>
                
                <li>
                    Student Gender: 
                    <?php
                    if (isset($rentalObj->user->ssurname16) && !empty($rentalObj->user->ssurname16)) {
                        ?>
                        {{isset($rentalObj->user->gender16) ? $rentalObj->user->gender16 : ""}}                 
                    <?php } else { ?>
                        @if(isset($rentalObj->user->gender) && $rentalObj->user->gender == "1")
                        Male
                        @elseif(isset($rentalObj->user->gender) && $rentalObj->user->gender == "2")
                        Female
                        @elseif(isset($rentalObj->user->gender) && $rentalObj->user->gender == "3")
                        Other
                        @endif

                    <?php } ?>
                </li>
                
                <li>
                    Date of Birth : 
                    <?php if (isset($rentalObj->user->dob16) && !empty($rentalObj->user->dob16)) { ?>
                        {{isset($rentalObj->user->dob16)?$rentalObj->user->dob16:""}}
                    <?php } else { ?>
                        {{isset($rentalObj->user->dob)?$rentalObj->user->dob:""}}
                    <?php } ?>
                </li>
                <li>
                    School : {{isset($rentalObj->user->school->name)?$rentalObj->user->school->name:""}}
                </li>
                <!--                profile-about16 -> about_yourself
                medical_learning16 -> medical_learning-->
                <li>
                    Student Description : {{isset($rentalObj->user->about_yourself)?$rentalObj->user->about_yourself:""}}
                </li>
                <li>
                    Student Medical or Any Description : {{isset($rentalObj->user->medical_learning)?$rentalObj->user->medical_learning:""}}
                </li>
                <li>
                    Message for Host : {{isset($rentalObj->user->host_family_notes)?$rentalObj->user->host_family_notes:""}}
                </li>
            </ul>
        </div>


        Kindly reply by accepting or refusing:
        <a href="{{url('user/reservations')}}">View Request</a>
        
        <br />
        
        or further any requests and if you wish ask for an interview through our internal messaging system:
        <a href="{{url('user/messages')}}">Messaging</a>
        
        <br />
        Best wishes,
        <br />
        Prof. Avanzi Matthew

        @include('emails.disclaimer')
    </body>
</html>
