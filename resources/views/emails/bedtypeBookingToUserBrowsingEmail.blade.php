<html>
    <head>
        <title>Work-Related-Learning - School House requested</title>
    </head>
    <body> 
        <p> Gentili genitori e studenti, </p>
        <p>confermiamo invio della richiesta per il seguente studente:</p>

        <br />
        <div>
            <table style="width: 100%; border: 1px solid;">
                <tr>
                    <td>Surname and Name</td>
                    <td>Mobile (0039) for Italy</td>
                    <td>Email</td>
                    <td>Booking No#</td>
                </tr>
                <tr>
                    <?php
                    if (isset($rentalObj->user->sname16) && !empty($rentalObj->user->sname16)) {
                        $lastname = isset($rentalObj->user->ssurname16) ? $rentalObj->user->ssurname16 . " " : "";
                        $firstname = isset($rentalObj->user->sname16) ? $rentalObj->user->sname16 : "";
                        $name = $lastname . $firstname;
                        $contact = isset($rentalObj->user->sphone16) ? $rentalObj->user->sphone16 : "";
                        $email = isset($rentalObj->user->semail16) ? $rentalObj->user->semail16 : "";
                    } else {
                        $lastname = isset($rentalObj->user->lastname) ? $rentalObj->user->lastname . " " : "";
                        $firstname = isset($rentalObj->user->name) ? $rentalObj->user->name : "";
                        $name = $lastname . $firstname;
                        $contact = isset($rentalObj->user->phonenumber) ? $rentalObj->user->phonenumber : "";
                        $email = isset($rentalObj->user->email) ? $rentalObj->user->email : "";
                    }
                    ?>
                    <td>{!! $name !!}</td>
                    <td>{!! $contact !!}</td>
                    <td>{!! $email !!}</td>
                    <td>{!! $rentalObj->Bookingno !!}</td>
                </tr>
            </table>
        </div>
        <br />
        <p>
            Avete inviato richiesta per queste date:
            {{isset($rentalObj->checkin)?date("d-m-Y", strtotime($rentalObj->checkin)):""}} al 
            {{isset($rentalObj->checkout)?date("d-m-Y", strtotime($rentalObj->checkout)):""}}
            <br />        
            a questa School House {{isset($propertyObject->user->orgname)?$propertyObject->user->orgname:""}} con titolo: {{$propertyObject->exptitle}}
        </p>

        <h4><strong>Dati School House</strong></h4>
        <p>
            I dati della School House verranno inviati se la stessa accetterà la vostra richiesta. Altrimenti verrete notificati e vi verrà proposto di cercare nuova School House.
        </p>
        <p>
        <h3><strong>
                Confermate la ricezione di questa email: 
                <a href="{{URL::to('jobEmailConfirm/'.$token)}}">
                    Cliccare qua
                </a>
            </strong></h3>
    </p>
    <br />
    <p>Vi auguriamo buona esperienza e buon lavoro!</p>
    <p>Staff Madrelingua</p>

    <br />

    <br />
    <br />
    <div style="color: blue;">
        <p>--</p>
        <p>Dr. Matthew Avanzi</p>
        <br />
        <p>Evitate di mancare nostre email:</p>
        <br />
        <p>
            Aggiungete info@madrelingua-inglese.it, ed altri nostri account che usate per servizi specifici, alla vostra lista contatti. 
            Ciò per evitare di perdere nostre comunicazioni che potrebbero finire nello spam (inviamo email commerciali solo 2/3 volte l'anno).
        </p>
        <br />
        <p>Avoid missing our emails:</p>
        <br />
        @include('emails.disclaimer')
    </div>
</body>
</html>
