<html>
    <head>
        <title>Notification to Admin for listing creation or update</title>
    </head>
    <body>
        <p> Hi 
            <?php
            $new_name = isset($propertyObject->hr_represent_name) ? $propertyObject->hr_represent_name . " " : "";
            $new_name = $new_name . isset($propertyObject->hr_represent_surname) ? $propertyObject->hr_represent_surname : "";
            if (isset($sent_to) && $sent_to == "notification") {
                $new_name = "Matthew Avanzi";
            }
            ?>
            {{$new_name}}, 
        </p>
        <br />
        <p>
            @if(isset($propertyObject->user->paid_service->house_name) && !empty($propertyObject->user->paid_service->house_name))
            This School House:  {{$propertyObject->user->paid_service->house_name}}
            @else
            This company {{isset($propertyObject->user->orgname)?$propertyObject->user->orgname:""}}, 
            @endif
            has changed this listing with title: {{$propertyObject->exptitle}}
        </p>
        <br />

        <br />
        <p><strong>User Listing Details:</strong></p>
        <div class="divbottom">
            @if(isset($propertyObject->work_org_same) and $propertyObject->work_org_same == 0)
            Work Tutor is:
            <div class="divbottom"><b>Name</b>: {{isset($propertyObject->represent_name) ?$propertyObject->represent_name:""}}</div> 
            <div class="divbottom"><b>Surname</b>: {{isset($propertyObject->represent_surname) ?$propertyObject->represent_surname:""}}</div> 
            <div class="divbottom"><b>e- mail</b>: {{isset($propertyObject->represent_email) ?$propertyObject->represent_email:""}}</div> 
            <div class="divbottom"><b>Telephone</b>: {{isset($propertyObject->represent_phone) ?$propertyObject->represent_phone:""}} </div> 
            User Listing School House or Work Experience
            <div class="divbottom"><b>Name</b>: {{isset($propertyObject->user->name)?$propertyObject->user->name:""}}</div> 
            <div class="divbottom"><b>Surname</b>: {{isset($propertyObject->user->lastname)?$propertyObject->user->lastname:""}}</div> 
            <div class="divbottom"><b>e- mail</b>: {{isset($propertyObject->user->email)?$propertyObject->user->email:""}}</div> 
            <div class="divbottom"><b>Telephone</b>: {{isset($propertyObject->user->phonenumber)?$propertyObject->user->phonenumber:""}} </div>
            @else
            User Listing School House or Work Experience
            <div class="divbottom"><b>Name</b>: {{isset($propertyObject->user->name)?$propertyObject->user->name:""}}</div> 
            <div class="divbottom"><b>Surname</b>: {{isset($propertyObject->user->lastname)?$propertyObject->user->lastname:""}}</div> 
            <div class="divbottom"><b>e- mail</b>: {{isset($propertyObject->user->email)?$propertyObject->user->email:""}}</div> 
            <div class="divbottom"><b>Telephone</b>: {{isset($propertyObject->user->phonenumber)?$propertyObject->user->phonenumber:""}} </div>
            @endif
        </div>
        <br />

        Kindly reply by accepting or refusing Listing or Changes to, advise User Listing/Work Tutor on any changes to be made if breaching our terms and conditions, policy and netiquette.
        
        <br />
        <br />
        Best wishes,
        <br />
        Prof. Avanzi Matthew

        @include('emails.disclaimer')
    </body>
</html>
