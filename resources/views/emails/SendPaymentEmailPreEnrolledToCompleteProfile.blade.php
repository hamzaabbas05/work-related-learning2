
<html>
    <head>
        <title>Work-Related-Learning - Posti di Lavoro</title>
    </head>
    <body>
        <!-- ToParentAboutWorkTutor-->

        <p> Gentili genitori e studenti, 
        </p>
        <p>si prega di leggere tutto con attenzione prima di procedere e dopo aver provveduto.</p>
        
        <br />
        <h4>COMPLETARE IL PROFILO</h4>
        
        <p>Prego procedere cliccando su: <a href="http://work-related-learning.com/user/parent" target="_blank"><strong>Complete - Parent of Under 18</strong></a>,; se siete studenti maggiorenni cliccate sul tasto <a href="http://work-related-learning.com/user/studentGuest" target="_blank"><strong>Complete - Stuent guest (over 18)</strong> </a>. Se non siete loggati vi chiederà user e password.</p>

        <p>
            Qui un video che mostra
            <a href="https://drive.google.com/file/d/1XbvVX6FsazOeHelQBmIP6ds99btehwzB/view" target="_blank">come completare il profilo.</a>
        </p>
        <p>Mettete un trattino (-) o uno zero (0) ovunque non abbiate i dati necessari al momento 
            in quanto se i campi sono obbligatori dovete inserire qualcosa per poter salvare le modifiche.
</p>
<p>Dovrete inserire dati del Vs. Tutor interno e se vi è un docente che viaggia con voi i dati dello stesso (dati recuperabili alla segreteria del Vs. Istituto), se la stessa persona i dati vanno inseriti due volte nelle due sezioni. Nel caso il Vs. Tutor Interno fosse il Responsabile di Alternanza di riferimento qui per comodità mettiamo i loro dati.
<br />
Responsabile Alternanza:<br />
    nome: {{$tutor_name}}<br />
    cognome: {{$tutor_surname}}<br />
    luogo di nascita: {{$tutor_pob}}<br />
    Codice Fiscale: {{$tutor_pan}}<br />
    email: {{$tutor_email}}<br />
    numero: {{$tutor_phone}}.<br />

</p>

<p>Se invece il Vs. Tutor Interno e l'eventuale docente che viaggia con voi fossero altre persone e non aveste questi dati, vi invitiamo a chiedere i dati degli stessi alla segreteria del Vs. Istituto. 

</p>

<p>Questi i recapiti del Vs. Isituto:
<br />
email: {{$school_email}} <br />
telefono: {{$phonenumber}}.

</p>

        <br />
         <!--        <div>
                    <p>Docente che viaggia con gli Studenti:</p>
                    <table style="width: 100%; border: 1px solid;">
                        <tr>
                            <td>Date In</td>
                            <td>Date Out</td>
                            <td>DOB - Date of Birth</td>
                            <td>Surname and Name</td>
                            <td>Gender (Male or Female)</td>
                            <td>Mobile (0039) for Italy</td>
                            <td>Email</td>
                        </tr>
                    </table>
                </div>-->
      

        <br />

        <center<h4><strong>SCELTA POSTI DI LAVORO - Invio domande</strong></h4>
        
       <p> Dati di Work Tutor e School Houses:</p>

 <p>I dati completi dei work tutor verranno inviati una volta che il Work Tutor accetti la domanda di lavoro inviata.</p>

<p>Ogni studente ha lo possibilità di fare domanda a tre posti di lavoro.</p>

<p>Se la vostra prima scelta è indirizzata ad un datore che gestisce direttamente le richieste, 
questi potrà chiedervi un’intervista ed accettare direttamente la vostra candidatura.</p>

<p>Se l'organizzazione richiede l’intermediazione del Dott. Avanzi, 
    successivamente ad una verifica dello stesso delle disponibilità del datore di lavoro si riceveranno i dati del Work Tutor</p>

<p>La seconda e la terza scelta non inviano notifiche ai Work Tutor, 
servono per uso interno nel caso la vostra prima scelta non fosse disponibile o presa da un'altro studente.</p>

<p>Tutti gli altri studenti riceveranno i dati del Work Tutor a circa un mese dalla partenza</p>

<p>Qui un video che mostra <a href="https://drive.google.com/file/d/1GUWJmjNOxZEHr5IZEfgeKk20k-xHiqGV/view" target="_blank">come selezionare ed inviare una domanda di lavoro</a></p> 

<p>Qui un video che mostra alcune <a href="https://drive.google.com/file/d/1GUWJmjNOxZEHr5IZEfgeKk20k-xHiqGV/view" target="_blank">funzionalità aggiuntive e l’email di notifica</a> che riceverete con i dati del Work Tutor</p>



<br />
        <h4><strong>SCELTA DELLA SCHOOL HOUSE</strong></h4>
        <p>Le School Houses potranno accettare una vostra richiesta solo dopo che avrete ricevuto conferma del lavoro. </p>        
        <p>Invieremo una email con unteriori informazioni per il caso
        </p>
        <br />
        <h4><strong>NOTIFICHE</strong></h4>
        

<p>Vi chiediamo di aggiungere questa email: notification@work-related-learning.com ai vostri contatti per assicurarvi che non vadano a finire nostri messaggi nella vostra casella di spam (in genere basta scivere una email).</p>

<p>Il sito è completo di Termini e condizioni, Policy, Netiquette, FAQ e altro. Per altri quesiti non esitate a chiedere.</p>
        
        
        <br />
        <p>Vi auguriamo buona esperienza e buon lavoro!</p>
        <p>Dott. Matthew Avanzi</p>

        <br />


        <br />
        <br />
        <div style="color: blue;">
            <p>--</p>
            <p>Dr. Matthew Avanzi</p>
            <br />
            <p>Evitate di mancare nostre email:</p>
            <br />
            <p>
                Aggiungete notification@work-related-learning.com, ed altri nostri account che usate per servizi specifici, alla vostra lista contatti. 
                Ciò per evitare di perdere nostre comunicazioni che potrebbero finire nello spam (inviamo email commerciali solo 2/3 volte l'anno).
            </p>
        </div>
        @include('emails.disclaimer')
    </body>
</html>
<?php //echo "TesT"; exit;?>