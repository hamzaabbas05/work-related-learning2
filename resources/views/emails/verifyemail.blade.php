<html>
<head>
<title>Work-Related-Learning</title>
</head>
<body>
<h2> Hi {{$username}} </h2>
<p>Thank you for joining www.work-related-learning.com.</p></br>
<p>you have registered with email {{$email}}.</p><br>
<p> Click this Link to confirm your email</p>
<p>
  <a href="{{URL::to('confirm/'.$confirmCode)}}">Click this Link</a>
</p>
@include('emails.disclaimer')
</body>
</html>
 