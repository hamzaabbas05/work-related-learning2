<html>
    <head>
        <title>Work-Related-Learning - Posti di Lavoro</title>
    </head>
    <body>
        <!-- ToNewWorkTutorVoucherEmail-->
        Dear Work Tutor,
        if you have not read it yet, here is  <a href="https://docs.google.com/document/d/1j3IFRgL76zLPpCJEOy5k7ipVviN5K5m6PzfbMVoRziM/edit?usp=sharing" target="_blank"><strong>our introductory email</strong></a>
 <br />
        <h4>Getting down to business:</h4>
        <p>
To add a job listing on our emulated work market, for unpaid curricular traineeships, you should:
        </p>
        <p>
1) complete your profile with required info: which is under <a href="http://work-related-learning.com/user/edit" target="_blank">"Basic"</a> and what required under <a href="http://work-related-learning.com/user/workExperience" target="_blank">"Unpaid Work Experience"</a>
</p>
<p>
2) add listing clicking on <a href="http://work-related-learning.com/rentals/add" target="_blank">"Unpaid Work Experience"</a> and simply fill in. Last wait for admin approval.  
</p>
 <br />

<p><strong>We strongly recommend:</strong> to specify what you expect the student to know, 
    i.e. the use of such and such software/machine, and work you will ask which may be daunting, to put off the wrong students and to attract the right ones.
    
     <br />
    Under "Workplace Rules" please specify: "no mobile phones allowed during working hours". 
     <br />
         and to specify </p>
    
    </h4>Getting requests:</h4>

<p>you may see your active listings under <a href="http://work-related-learning.com/user/listing/rentals/active" target="_blank">"Your listings"</a></p>
<p>and in the same page click on <a href="http://work-related-learning.com/user/reservations" target="_blank">"Student/'s job applications"</a> to see of any request: student profile, message them (for example asking for a phone interview), accept or decline.</p>
<p>No need to check, you will receive an email notification for every request, not to miss out on any please add notification@work-related-learning.com to your contact (usually simply sending an email).</p>
<p>If you accept a students the system will generate the documentation in compliance with EU and local laws, here a <a href="http://work-related-learning.com/" target="_blank">sample of the "Learning Agreement"</a>.</p>
 <p>No need to do anything else, the students will bring the hard copies of the 3 documents required, plus any other you request.</p>
      
</h4>Upon student arrival:</h4>

<p>Sign documentation, to validate insurance coverage by the school, do your risk and safety job (fire exits, warn about risks and so on) and make them work!
       </p>
       
        <p>Ask the student to complete the daily register, it is among their duties.</p>

        </h4>Before the student leaves:</h4>
<p>Complete the evaluation form, 
<a href="https://docs.google.com/document/d/1-5ZVhj9vgtGnM6ZU3AeFZm063TJkL-pK5o5Vizokhpo/edit?usp=sharing" target="_blank">"here a sample"</a>.</p>


               <br />


        <div style="color: blue;">
            <p>--</p>
            <p>Best wishes</p>
            <p>Dr. Avanzi Matthew</p>
            <br />
            
            
            
            
            <p>IMAGE</p>
            
            
            
            
            
            
            <p>Learn in a FLASH Ltd</p>
            <p>20-22 Wenlock Road </p>
            <p>London</p>
            <p>N1 7GU</p>
            <p>United Kingdom</p>
            <br />
            <p><strong>  
                    Avoid missing our emails:</strong>
Add notification@work-related-learning.com and hello@work-related-learning.com to your list of desired contacts if you want to be sure that our emails are not classified as spam by your systems; this not to run the risk of missing out on important news or messages. You may also reply to this email to increase likelihood of getting all our emails but we will not reply to these. Contact your agents for any assistance or us for any matter concerning the Website.
</p>
<p><strong>  
                    Disclaimer:</strong>
This email and any files transmitted with it may be confidential, privileged or otherwise protected by work product immunity or legal rules and supposedly intended for the use of the individual or entity to whom they are addressed. If you have received this email in error, please notify the system manager; you may filter messages (marking as spam could be ineffective and illegal as we do not send spam) from our site in case somebody is using yours inappropriately, or you could register to our website to make sure to get only the messages for your experiences. The integrity and security of this message cannot be guaranteed as transmitted over the internet. The recipient should check this email and any attachments for the presence of viruses and malaware. Our emails may contain links to other websites of interest, once you have used these links, we cannot be held responsible for protection and privacy whilst visiting such sites.
            </p>

        </div>
    </body>
</html>



