<html>
    <head>
        <title>Work-Related-Learning parentofPaidEmail</title>
    </head>
    <body>
        <p>Gentili genitori e studenti,</p>
        <p>Vi preghiamo di leggere con attenzione in vista dell'esperienza di</p>
        <br />

        <div>
            <table style="width: 100%; border: 1px solid;">
                <tr>
                    <td>Surname and Name</td>
                    <td>Mobile (0039) for Italy</td>
                    <td>Email</td>
                    <td>Booking No#</td>
                </tr>
                <tr>
                    <?php
                    if (isset($rentalObj->user->sname16) && !empty($rentalObj->user->sname16)) {
                        $lastname = isset($rentalObj->user->ssurname16) ? $rentalObj->user->ssurname16 . " " : "";
                        $firstname = isset($rentalObj->user->sname16) ? $rentalObj->user->sname16 : "";
                        $name = $lastname . $firstname;
                        $contact = isset($rentalObj->user->sphone16) ? $rentalObj->user->sphone16 : "";
                        $email = isset($rentalObj->user->semail16) ? $rentalObj->user->semail16 : "";
                    } else {
                        $lastname = isset($rentalObj->user->lastname) ? $rentalObj->user->lastname . " " : "";
                        $firstname = isset($rentalObj->user->name) ? $rentalObj->user->name : "";
                        $name = $lastname . $firstname;
                        $contact = isset($rentalObj->user->phonenumber) ? $rentalObj->user->phonenumber : "";
                        $email = isset($rentalObj->user->email) ? $rentalObj->user->email : "";
                    }
                    ?>
                    <td>{!! $name !!}</td>
                    <td>{!! $contact !!}</td>
                    <td>{!! $email !!}</td>
                    <td>{!! $rentalObj->Bookingno !!}</td>
                </tr>
            </table>
        </div>
       
        <br />
        <p>Di seguito i dati della SCHOOL HOUSE dove alloggerete:</p>

        <div>
            <table style="width: 100%; border: 1px solid;">
                <tr>
                    <td>Name</td>
                    <td>Surname</td>
                    <td>Phone</td>
                    <td>email</td>
                    <td>Address</td>
                    <td>Rules</td>
                    <td>Gmaps route to work</td>
                </tr>
                <tr>
                    <td>{{isset($rentalObj->property->user->name)?$rentalObj->property->user->name:""}}</td>
                    <td>{{isset($rentalObj->property->user->lastname)?$rentalObj->property->user->lastname:""}}</td>
                    <td>{!! isset($rentalObj->property->user->phonenumber)?$rentalObj->property->user->phonenumber:"" !!}</td>
                    <td>{!! isset($rentalObj->property->user->email)?$rentalObj->property->user->email:"" !!}</td>
                    <td>
                        <?php
if(isset($rentalObj->property->addr_work_org_same) && $rentalObj->property->addr_work_org_same == "0"){
    echo (isset($rentalObj->property->workplace_address) ? $rentalObj->property->workplace_address : "");
}else if(isset($rentalObj->property->user->user_paid_services->propertyaddress)){
    // echo (isset($rentalObj->property->user->user_paid_services->propertyaddress) ? $rentalObj->property->user->user_paid_services->propertyaddress: "");
    if($rentalObj->property->property_status == 'paid' ){
        echo (isset($rentalObj->property->user->user_paid_services->propertyaddress) ? $rentalObj->property->user->user_paid_services->propertyaddress: "");
    }else{        
        echo (isset($rentalObj->property->user->user_paid_services->houseaddress) ? $rentalObj->property->user->user_paid_services->houseaddress: "");
    }    
}else{
    echo (isset($rentalObj->property->user->raddress18) ? $rentalObj->property->user->raddress18: "");
}
                        ?>
                    </td>
                    <td>{!! isset($rentalObj->property->houserules)?$rentalObj->property->houserules:"" !!}</td>
                    <td>
                        @if(isset($rentalObj->property->workplace_address) && !empty($rentalObj->property->workplace_address))
                        <a href="https://maps.google.com/?q={{$rentalObj->property->workplace_address}}" target="_blank">Click to View on Map</a>
                        @elseif(isset($rentalObj->property->user->user_paid_services->propertyaddress) && !empty($rentalObj->property->user->user_paid_services->propertyaddress))
                        <a href="https://maps.google.com/?q={{$rentalObj->property->user->user_paid_services->propertyaddress}}" target="_blank">Click to View on Map</a>
                        @else(isset($rentalObj->property->user->raddress18) && !empty($rentalObj->property->user->raddress18))
                        <a href="https://maps.google.com/?q={{$rentalObj->property->user->raddress18}}" target="_blank">Click to View on Map</a>
                        @endif
                    </td>
                </tr>
            </table>
        </div>

        <br />
        Le indicazioni fornite da Google Maps sono molto accurate, indicano l'orario di percorrenza incluso traffico, tratte a piedi e cambi di mezzi. <br />
        <?php
        $link_docs = "";
        $user_booking = \App\RentalsEnquiry::select("fc_rentalsenquiry.*", "property.id as p_id", "property.property_status", "property.link_to_extra_docs_necessary", "property.user_id as p_userid", "property.workplace_address")
                ->join('property', 'fc_rentalsenquiry.prd_id', '=', 'property.id')
                ->where(["fc_rentalsenquiry.user_id" => $rentalObj->user_id, "fc_rentalsenquiry.approval" => "Accept"])
                ->where("fc_rentalsenquiry.checkin", ">=", date("Y-m-d", strtotime($rentalObj->checkin)))
                ->where("fc_rentalsenquiry.checkout", "<=", date("Y-m-d", strtotime($rentalObj->checkout)))
                //->where("fc_rentalsenquiry.is_hr", "0")
                ->where("property.property_status", "!=", "paid")
                ->orderBy('fc_rentalsenquiry.checkin', 'ASC')
                ->first();
        if (isset($user_booking->id)) {
            if (isset($user_booking->link_to_extra_docs_necessary) && !empty($user_booking->link_to_extra_docs_necessary)) {
                $link_docs = $user_booking->link_to_extra_docs_necessary;
            }
            ?>
            <a href="{{URL('gmap/'.$rentalObj->id.'/'.$user_booking->p_id.'/'.$user_booking->id)}}">Qui troverete precaricati gli indirizzi di partenza e destinazione</a>, che userete i mezzi (o andrete a piedi) e l'arrivo inserito è per lunedì alle 9am. <br />
        <?php } ?>
        Verificate effettivo orario e giorno di inizio nel Learning Agreement, ovvero nei dati forniti dall'Organizzazione come "Working Hours". Inoltre se Gmaps non fornisce indicazioni, potrebbe essere che manca troppo tempo alla partenza, basta tornare a questo link qualche giorno/settimana prima della partenza e vi indicherà itinerario, fermate e compagnia di bus da prendere. 

<!-- <h4><strong>L'indirizzo per arrivare sul luogo di lavoro lo avete all'interno del Learning Agreement, usate Google Maps per trovare i collegamenti con i mezzi pubblici indicando orario di arrivo 
 <a href="https://www.google.it/maps/dir/52.918007,-1.102882/52.9538297,-1.1536412/@52.9477175,-1.1785875,14z/data=!4m6!4m5!2m3!6e1!7e2!8j1521450000!3e3?hl=it">
 come in questo esempio.
 </a>
</strong></h4>-->
        <br />
        <h4><strong>Si consiglia un contatto telefonico da parte degli studenti qualche giorno dalla partenza per una prima introduzione (of course, in English).</strong></h4>
        <br />
        <br />
        <p><strong>DETTAGLI WORK TUTOR:</strong></p>
        <br />
        <p>Avete già ricevuto tutta la documentazione nella email ricevuta quando la vostra esperienza lavorativa è stata accettata ed avete ricevuto email di conferma.</p>
        <?php if (!empty($link_docs)) { ?>
            <p>Chiediamo solo di controllare una seconda volta di aver effettivamente provveduto ad eventuale documentazione extra richiesta dal Work Tutor: 
                <a href="{!! isset($link_docs)?$link_docs:'#' !!}" target="_blank">Link alle indicazioni Google Maps "®"</a>
            </p>
        <?php } ?>
        <br />
        <p><strong>VIAGGIO:</strong></p>
        <br />
        <h4><strong>Vi ricordiamo di portare tutti i documenti necessari per viaggiare:</strong></h4>
        <p>- documento valido per l'espatrio: eg. passaporto o carta d'identità</p>
        <p>- tessera sanitaria in corso di validità (Carta Nazionale/Regionale dei Servizi - per assistenza sanitaria gratuita);</p>
        <br />
        <p>Se avete preferito usare l'App (application) della compagnia aerea e fare il check-in e quindi il boarding pass in autonomia, basta che ricordiate di prendere il cellulare carico.
            Per gli altri: portare boarding pass cartaceo.</p>
        <br />


        <h4><strong>Bagaglio:</strong></h4>
        <p>
            Riassumiamo che occorre mettere liquidi e cose taglienti/pericolose (es. forbicine) nei bagagli da imbarcare, o nel bagaglio a mano solo ill liquidi in contenitori da max 100ml dentro la busta trasparente sigillabile standard max 1lt.
        </p>
        <p>Fare riferimento ai Termini e condizioni del vettore.</p>
        <br />
        <br />

        <p>
            <strong>COSA PORTARE:</strong>
        </p>
        <p>
            - vestiti: a nostro parere è indispensabile avere in valigia una giacchetta leggera anti-vento (stile k-way) per far fronte al mutevole clima Britannico comunque non troppo freddo;
        </p>
        <p> - beni personali: considerate che i ragazzi sono ospiti e quindi di portare solo ciò che è personale e che le cose che fornireste anche voi ad un ospite saranno disponibili (es. lenzuola, asciugamani) come pure saranno disponibili al lavaggio degli indumenti (una lavatrice piena a settimana, anche più se sarete gentili);</p>
        <p> -  un dono: se voleste portare un dono consigliamo cibo italiano molto apprezzato o un pensiero (non liquidi nel bagaglio a mano!). Magari quei giorni prima della partenza in cui consigliamo un contatto telefonico con le famiglie, intrattenendo una conversazione più lunga, si riesce a capire cosa piace dell'Italia o altro e comprare un dono di conseguenza, questo ancor più apprezzabile.</p>
        <p>Vi chiediamo di mostrare cordialità e buone maniere.</p>
        <br />
        <br />
        <p><strong>NUMERI DI EMERGENZA:</strong></p>
        <p>Consigliamo di salvarli nella rubrica del telefono assieme ai numeri ed indirizzi di School Houses, Work Tutors, Accompagnatori e altri forniti, come pure su un foglio di carta da tenere in tasca</p>
        <p><strong>Per problemi con i luoghi di Lavoro,</strong>inclusi infortuni nei viaggi da e verso i luoghi di lavoro, contattare il proprio Tutor Interno (dell’Istituto Superiore, gruppo o della classe, il quale potrebbe essere chi vi accompagna a seconda dell'Istituto). Chiedete o comunque riferite alle procedure dettate dal vostro Istituto Superiore.</p>
        <p><strong>Per problemi con le School Houses,</strong>relative all'organizzazione ed urgenze risolvibili dall’Italia:</p>
        <p>Madrelingua 0039035755009 </p>
        <p>Matthew Avanzi 00393348498788</p>
        <br />
        <p>
            <strong>Per urgenze diverse risolvibili in the UK:</strong>
        </p>
        <p>Rosa ambler 00447780747833</p>
        <p>Disponibili per emergenze non imputabili al viaggio organizzato da Madrelingua o alle famiglie ospitanti (ad esempio perdita di un documento, danni causati dai ragazzi, arresti, licenziamenti, violazione termini e condizioni ecc.), anche di notte, come specificato <a href="{{URL::to('home')}}">sul sito</a>, ad un costo di £30 orarie e massimale giornaliero di £150 con costo di chiamata notturna (19.00 - 6.00).</p>
        <br /> <br />
        <p>
            <strong>TENERSI IN CONTATTO CON I RAGAZZI:</strong>
        </p>
        <p>Le tariffe di Roaming dati sono state regolamentate in Europa, quindi usare il proprio vettore non dovrebbe costare molto.</p>
        <p>Il Wi-Fi è gratuito e diffuso in tutta la città, quasi ovunque vi sia un locale pubblico, sui pullman della città (basta stare alle pensiline) come pure presso tutte le School Houses (il servizio non è garantito).</p>
        <p>Tramite wi-fi si può inviare messaggi, effettuare chiamate (ecc.) gratuitamente con software tipo Skype, Viber, Whatsapp ecc.</p>
        <br /> <br />
        <p><strong>COPRIFUOCO:</strong></p>
        <p>Ricordiamo a tutti i genitori che in ottemperanza alla linee guida del British Council inglese si richiede per i minori il rispetto dei seguenti orari di rientro nelle famiglie: ore 22.00. Rispettare eventuali regole aggiuntive della casa.</p>
        <br /> <br />
        <p><strong>SOLDI:</strong></p>
        <p>Consigliamo ai ragazzi di portarsi dall'Italia alcune Sterline in contanti (50-100 £) e di tenere sempre 20 Sterline in tasca per ogni evenienza. </p>
        <p>Potrebbe essere necessario pagare il trasferimento dall'aeroporto (circa £8 a tratta), eventuale abbonamento al bus ( stima £ 15); questo a dipendere dagli accordi con l’Istituto Superiore ed il luogo di lavoro, alcuni a distanza pedonabile.</p>
        <p>
            Una carta prepagata con limite di prelievo è un buon modo per assicurare ai propri figli accesso a liquidità in qualsiasi momento senza il rischio di portarsi tanti contanti. Con le comodissime Carte “Contacless” potranno effettuare pagamenti semplicemente apponendo la carta sui sensori preposti (ad esempio sui pullman, nei negozi ecc.) fino a un massimale tipico per operazione di £25. Si può prelevare anche ai bancomat (cash-machines) presenti presso tutte le vie di negozi.
        </p>
        <p>Ricordiamo che se il cambio avviene presso una banca italiana potrà essere necessario prenotare le Sterline, dunque potrà occorrere qualche giorno per averle.</p>
        <br /> <br />
        <p><strong>ASSICURAZIONE</strong></p>
        <br />
        <p><strong>Assicurazione lavorativa:</strong></p>
        <p>Vi è una copertura come in Italia, chiedete al vostro Istituto Superiore per dettagli in quanto sono loro ad avere stipulato polizza (infortuni, assistenza legale, danni ed altro).</p>
        <br />
        <p><strong>Assicurazione viaggio:</strong></p>
        <p>Abbiamo una copertura, integrativa a quella obbligatoria di legge, con assicurazione dedicata ai Viaggi di Studio ed in Alternanza per 2.000.000 di euro per responsabilità civile danni a terzi (anche per docenti che viaggiano con i ragazzi) per coprire la nostra organizzazione, forniamo indicazione di agenzie assicurative di riferimento per coperture desiderate dai genitori/ragazzi.</p>
        <p>Vi ricordiamo che il servizio sanitario inglese è gratuito come in Italia (ticket, malattie e farmaci esclusi) basta avere con sé il tesserino sanitario (Carta Regionale/Nazionale dei servizi).</p>
        <br />
        <p><strong>Extra:</strong></p>
        <p>Nel caso vorreste ed altre coperture (eg. bagagli, perdita volo, documenti ecc.) siete pregati di prendere opportuna copertura assicurativa presso compagnia di fiducia.</p>
        <br /> <br />
        <p><strong>CONFERMARE LETTURA</strong>:</p>
        <p> 
            <a href="{{URL::to('jobEmailConfirm/'.$token)}}">
                Cliccare sul seguente link per confermare ricezione della presente email
            </a> per non incorrere in extra eventuali costi.
        </p>
        <br />
        <p>Vi auguriamo buon viaggio, buon soggiorno e buon lavoro!</p>
        <p>Staff Madrelingua</p>
        <p>Monia Zambaiti</p>

        <br />

        <div style="color: blue;">
            <p>--</p>
            <p>Dr. Matthew Avanzi</p>
            <br />
            <p>Avoid missing our emails:</p>
            <br />
            @include('emails.disclaimer')
        </div>
    </body>
</html>



