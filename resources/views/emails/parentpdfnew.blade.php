<html>
    <head>
        <title>Work-Related-Learning</title>
    </head>
    <body>
        <!-- ToParentAboutWorkTutor-->

        <p> Gentili genitori e studenti {{isset($rentalObj->user->name)?$rentalObj->user->name:""}} {{isset($rentalObj->user->lastname)?$rentalObj->user->lastname:""}} 
            {{isset($rentalObj->user->sname16)?$rentalObj->user->sname16:""}} {{isset($rentalObj->user->ssurname16)?$rentalObj->user->ssurname16:""}}, 
        </p>
        <p>Vi preghiamo di leggere con attenzione in vista del viaggio in Inghilterra dei vostri figli.</p>

        <br />
        <div>
            <table style="width: 100%; border: 1px solid;">
                <tr>
                    <td>Surname and Name</td>
                    <td>Mobile (0039) for Italy</td>
                    <td>Email</td>
                    <td>Booking No#</td>
                </tr>
                <tr>
                    <?php
                    if (isset($rentalObj->user->sname16) && !empty($rentalObj->user->sname16)) {
                        $lastname = isset($rentalObj->user->ssurname16) ? $rentalObj->user->ssurname16 . " " : "";
                        $firstname = isset($rentalObj->user->sname16) ? $rentalObj->user->sname16 : "";
                        $name = $lastname . $firstname;
                        $contact = isset($rentalObj->user->sphone16) ? $rentalObj->user->sphone16 : "";
                        $email = isset($rentalObj->user->semail16) ? $rentalObj->user->semail16 : "";
                    } else {
                        $lastname = isset($rentalObj->user->lastname) ? $rentalObj->user->lastname . " " : "";
                        $firstname = isset($rentalObj->user->name) ? $rentalObj->user->name : "";
                        $name = $lastname . $firstname;
                        $contact = isset($rentalObj->user->phonenumber) ? $rentalObj->user->phonenumber : "";
                        $email = isset($rentalObj->user->email) ? $rentalObj->user->email : "";
                    }
                    ?>
                    <td>{!! $name !!}</td>
                    <td>{!! $contact !!}</td>
                    <td>{!! $email !!}</td>
                    <td>{!! $rentalObj->Bookingno !!}</td>
                </tr>
            </table>
        </div>
       
        <br />
        <br />

        <h3> <strong>DOCUMENTI DA PRODURRE: 4 (eventualmente 5)</strong></h3>
        <p>1) Dovrete scaricare, stampare, verificare e completare in duplice copia: il "Learning Agreement" in Inglese <strong>IN ALLEGATO</strong></p>
        <p>
            (sempre dal sito potete scaricare <a href="http://work-related-learning.com/assets/le/LEitaly.pdf" target="_blank">versione</a> 
            tradotta in italiano fosse necessario per capirne i contenuti).
        </p>
        <p>I dati del datore di lavoro sono presenti nel Learning Agreement.</p>
        <br />
        <p>
            Il Learning Agreement  andrà poi firmato da studente e genitore, in duplice copia, e portato a scuola per l'apposizione della 
            firma da parte del Dirigente Scolastico e del Tutor interno (dello studente) per questo progetto ed infine ritirato per poterlo 
            portare in the UK ove si chiederà la firma del Work Tutor, una copia si lascia a quest’ultima figura.
        </p>

        <br />

        <h4><strong>Altri documenti da stampare e portare in the UK:</strong></h4>
        <p>
                2) Registro presenze - vedasi allegati
        </p>
        <p>
                3) Evaluation form - vedasi allegati
        </p>
        <p>
            Questi documenti andranno completati con i vostri dati, portati in Inghilterra e fatti firmare (il registro va compilato dallo studente) dal Work Tutor in Inghilterra e riportati in Italia.
        </p>
        <p>
            <strong>
                La consegna della documentazione completa è responsabilità di studenti o tutori legali dei minorenni.
            </strong>
        </p>
        <p>
            E’ <strong>tassativo</strong> che tutta la documentazione sia completa di dati e firme alla fine dell’esperienza e questa responsabilità è degli studenti o dei tutori legali di studenti minorenni. Eventuali integrazioni, quale un learning agreement senza firma, verranno supportate dei nostri agenti di zona in the UK al tariffario indicato sul portale.
        </p>
        <br />

        <h4><strong>Per l’organizzazione di viaggio, alloggio, servizi e per alcune emergenze </strong></h4>
        <p>
                4) Documento da compilare e firmare - vedasi allegati
        </p>
        <p>
            Per permettere alla scuola Madrelingua, e suoi agenti ed altre parti coinvolte, di gestire le situazioni di emergenza per i minori di 18 anni ma anche di gestire l'organizzazione (quali i trasferimenti). 
        </p>
        <br />
        <p>
            Questo documento andrà portato in Aeroporto e consegnato a chi viaggia con gli studenti (oppure se in viaggio senza: alla nostra local agent/School House all'arrivo a Nottingham).
        </p>
        <br />
        <?php if (isset($rentalObj->property->link_to_extra_docs_necessary) && !empty($rentalObj->property->link_to_extra_docs_necessary)) { ?>
            <h4><strong>Altri EVENTUALI documenti da PRODURRE e portare in the UK:</strong></h4>
            <p>
                Verificare se informative, da leggere, o altri documenti richiesti, da stampare e compilare, quali referenze o altri moduli dedicati relativi all'organizzazione ospitante.
            </p>
            <p>  
                <a href="{{$rentalObj->property->link_to_extra_docs_necessary}}" target="_blank">
                    5) Eventuale documentazione da Produrre
                </a>
            </p>
            <br />
            <p>
                Questi documenti andranno completati con i vostri dati, portati in Inghilterra e fatti firmare (il registro va compilato dallo studente) dal Work Tutor in Inghilterra e riportati in Italia.
            </p>
            <br />
        <?php } ?>

        <h4><strong>Come arrivare sul posto di lavoro</strong></h4>
        <p>
            Collegamenti con i mezzi pubblici dalla famiglia al posto di lavoro (indicazioni Google Maps) verranno inseriti tra i dati della School House (famiglia ospitante) nella prossima email. Se non diversamente specificato l'orario di inizio si intende alle 9am di lunedì.
        </p>
        <br />
        <p>
            Consigliamo di salvare in 
            <a href="https://support.google.com/maps/answer/6291838?co=GENIE.Platform%3DAndroid&hl=it" target="_blank">modalità off-line</a> 
            la mappa dell’area cittadina di Nottingham (200MB di memoria) sullo smartphone dei ragazzi.
        </p>
        <br />
        <h4><strong>Esperienza Lavorativa</strong></h4>
        <p>
            Si ricorda che i posti di lavoro sono vere organizzazioni che vi considereranno come risorsa da far fruttare e che non sono pagati, come voi, 
            per prendervi a lavorare nonostante il vostro handicap linguistico e la carenza di esperienza lavorativa, ci si aspetta quindi massima 
            serietà e collaborazione nonché un atteggiamento proattivo per rendere l’esperienza il più fruttifera per gli studenti, ovvero più si mostrano 
            partecipi e più i Work Tutors li renderanno tali.
        </p>
        <br />
        <h4><strong>Dati School House</strong></h4>
        <p>
            I dati della School House verranno inviati la settimana prima della partenza con tutte le istruzioni del caso, quali ad esempio le domande 
            frequenti su cosa portare, informative, tragitti ed altro ancora.

        </p>
        <p>
        <h3><strong>
                Confermate la ricezione di questa email: 
                <a href="{{URL::to('jobEmailConfirm/'.$token)}}">
                    Cliccare qua
                </a>
            </strong></h3>
    </p>
    <br />
    <p>Vi auguriamo buona esperienza e buon lavoro!</p>
    <p>Staff Madrelingua</p>

    <br />


    <br />
    <br />
    <div style="color: blue;">
        <p>--</p>
        <p>Dr. Matthew Avanzi</p>
        <br />
        <p>Evitate di mancare nostre email:</p>
        <br />
        <p>
            Aggiungete info@madrelingua-inglese.it, ed altri nostri account che usate per servizi specifici, alla vostra lista contatti. 
            Ciò per evitare di perdere nostre comunicazioni che potrebbero finire nello spam (inviamo email commerciali solo 2/3 volte l'anno).
        </p>
    </div>
</body>
</html>



