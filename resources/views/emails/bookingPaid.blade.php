<html>
    <head>
        <title>Work-Related-Learning - Your School House has received a request</title>
    </head>
    <body>
        <p> 
            Hi 
            <?php
            $new_name = $name;
            if (isset($sent_to) && $sent_to == "worktutor") {
                $new_name = isset($propertyObject->represent_name) ? $propertyObject->represent_name : $name;
            } else if (isset($sent_to) && $sent_to == "userlisting") {
                $new_name = isset($renter_user->name) ? $renter_user->name : $name;
            }
            ?>
            {{$new_name}} 
            </p>
        <br />
        <p>
            Your School House {{isset($propertyObject->user->orgname)?$propertyObject->user->orgname:""}} has received a 
            request for the listing with title: {{$propertyObject->exptitle}}
        </p>
        <br />

        Student requested for this period from 
        {{isset($rentalObj->checkin)?date("d-m-Y", strtotime($rentalObj->checkin)):""}} to 
        {{isset($rentalObj->checkout)?date("d-m-Y", strtotime($rentalObj->checkout)):""}}
        
        <br />
        <p><strong>Details of person delivering this service:</strong></p>
        <div class="divbottom">
            @if(isset($propertyObject->work_org_same) and $propertyObject->work_org_same == 0)
            <div class="divbottom"><b>Name</b>: {{isset($propertyObject->represent_name) ?$propertyObject->represent_name:""}}</div> 
            <div class="divbottom"><b>Surname</b>: {{isset($propertyObject->represent_surname) ?$propertyObject->represent_surname:""}}</div> 
            <div class="divbottom"><b>e- mail</b>: {{isset($propertyObject->represent_email) ?$propertyObject->represent_email:""}}</div> 
            <div class="divbottom"><b>Telephone</b>: {{isset($propertyObject->represent_phone) ?$propertyObject->represent_phone:""}} </div> 
            @else
            <div class="divbottom"><b>Name</b>: {{isset($propertyObject->user->name)?$propertyObject->user->name:""}}</div> 
            <div class="divbottom"><b>Surname</b>: {{isset($propertyObject->user->lastname)?$propertyObject->user->lastname:""}}</div> 
            <div class="divbottom"><b>e- mail</b>: {{isset($propertyObject->user->email)?$propertyObject->user->email:""}}</div> 
            <div class="divbottom"><b>Telephone</b>: {{isset($propertyObject->user->phonenumber)?$propertyObject->user->phonenumber:""}} </div>
            @endif
        </div>
        <br />

        <p><strong>Student details:</strong></p>
        <div>
            <ul style="width: 100%; border: 1px solid; list-style-type: none;">
                <li>
                    Student Surname and Name : 
                    <?php
                    if (isset($rentalObj->user->ssurname16) && !empty($rentalObj->user->ssurname16)) {
                        ?>
                        {!! isset($rentalObj->user->ssurname16)?$rentalObj->user->ssurname16:"" !!} {!! isset($rentalObj->user->sname16)?$rentalObj->user->sname16:"" !!}
                    <?php } else { ?>
                        {!! isset($rentalObj->user->lastname)?$rentalObj->user->lastname:"" !!} {!! isset($rentalObj->user->name)?$rentalObj->user->name:"" !!}
                    <?php } ?>
                </li>
                <li>
                    Date of Birth : 
                    <?php if (isset($rentalObj->user->dob16) && !empty($rentalObj->user->dob16)) { ?>
                        {{isset($rentalObj->user->dob16)?$rentalObj->user->dob16:""}}
                    <?php } else { ?>
                        {{isset($rentalObj->user->dob)?$rentalObj->user->dob:""}}
                    <?php } ?>
                </li>
                <li>
                    <!--School : {{isset($rentalObj->user->school_name)?$rentalObj->user->school_name:""}}-->
                    School : {{isset($rentalObj->user->school->name)?$rentalObj->user->school->name:""}}
                </li>
                <!--                profile-about16 -> about_yourself
                medical_learning16 -> medical_learning-->
                <li>
                    Student Description : {{isset($rentalObj->user->about_yourself)?$rentalObj->user->about_yourself:""}}
                </li>
                <li>
                    Student Medical or Any Description : {{isset($rentalObj->user->medical_learning)?$rentalObj->user->medical_learning:""}}
                </li>
                <li>
                    Message for Work Tutor : {{isset($rentalObj->user->work_tutor_notes)?$rentalObj->user->work_tutor_notes:""}}
                </li>
                <li>
                    Booking No # : {{isset($rentalObj->Bookingno) ? $rentalObj->Bookingno:""}}
                </li>
            </ul>
        </div>


        <strong>Kindly view request to:
               <a href="{{url('user/schoolHouses')}}">Accept or Decline</a></strong>
        
        <br />
        <br />
        or further any requests and ask any question through our internal messaging system:
        <a href="{{url('user/messages')}}">Messaging</a>
        <br />
        <br />
        Best wishes,
        <br />
        Prof. Avanzi Matthew

        @include('emails.disclaimer')
    </body>
</html>
