<html>
<head>
<title>Work-Related-Learning</title>
</head>
<body>
<h2> Hi {{$name}} </h2>
<p>your Work Experience request has been accepted</p>
<p>1) Download the &quot;Learning Agreement&quot; (Convenzione e Patto) and print two hard copies which:</p>

<p>1.1) have to be signed by the student and, in case of undersage students, by the parent or legal guardian.<br />
1.2) have to be taken to the student&#39;s high school, handed to the School Tutor (responsabile di alternanza) who will have to sign it together with the headmaster of the high school<br />
1.3) then,this extremely important document, has to be taken to the student&#39;s Work Tutor who will have to sign it to make it valid and the hours of work experience count as work related learning (alternanza).</p>

<p>2) Furthermore the student has to take to the Work Tutor a &nbsp;hard copy of the attendance register (registro presenze) which your school tutor will give you</p>

<p>3) and any document he or she may have asked you.</p>

<p>Regards<br />
Prof. Avanzi Matthew</p>

<p>PS: for further assistance you may refer to your school tutor.&nbsp;<br />
In case you require assistance by Prof. Avanzi and his team you may book an appointment which will be charged Euro 30 (one hour max).</p>
@include('emails.disclaimer')
</body>
</html>
 
 
 
