<html>
    <head>
        <title>Work-Related-Learning - Posti di Lavoro</title>
    </head>
    <body>
        <!-- ToParentAboutWorkTutor-->

        <p> Gentili genitori e studenti, 
        </p>
        <p>si prega di leggere tutto con attenzione prima di procedere e dopo aver provveduto.</p>
        <br />

        <p>
            Vi ricordiamo che se lo studente fosse minore di 18 anni saranno i genitori a registrarsi al portale, 
            poi completerete il profilo utente cliccando: log-in, sul nome in alto a destra, poi su <strong>edit profile</strong> infine sul pulsante <strong>parent of</strong>.
        </p>
        <br />
        <h4>REGISTRAZIONE AL PORTALE - dati necessari</h4>
        
        <p>Prego procedere alla registrazione sul sito cliccando su <strong>join now</strong> in alto a destra: <a href="http://work-related-learning.com/" target="_blank">www.work-related-learning.com</a></p>
        
        
        <p>Occorerà inserire questo <strong>Voucher code: {{isset($voucher->voucher_code)?$voucher->voucher_code:""}}</strong></p>

        <p>
            Qui un video che mostra 
            <a href="https://drive.google.com/file/d/1Rw_sVSjpqU3bto66GNBxalygnZtpy9V9/view" target="_blank">come registrarsi</a> e uno su 
            <a href="https://drive.google.com/file/d/1XbvVX6FsazOeHelQBmIP6ds99btehwzB/view" target="_blank">come completare il profilo.</a>
        </p>
        <p>Mettete un trattino (-) o uno zero (0) ovunque non abbiate i dati necessari al momento 
            in quanto se i campi sono obbligatori dovete inserire qualcosa per poter salvare le modifiche.
</p>
        <br />
         <!--        <div>
                    <p>Docente che viaggia con gli Studenti:</p>
                    <table style="width: 100%; border: 1px solid;">
                        <tr>
                            <td>Date In</td>
                            <td>Date Out</td>
                            <td>DOB - Date of Birth</td>
                            <td>Surname and Name</td>
                            <td>Gender (Male or Female)</td>
                            <td>Mobile (0039) for Italy</td>
                            <td>Email</td>
                        </tr>
                    </table>
                </div>-->
      

        <br />

        <center<h4><strong>SCELTA POSTI DI LAVORO - Invio domande</strong></h4>
        
       <p> Dati di Work Tutor e School Houses:</p>

 <p>I dati completi dei work tutor verranno inviati una volta che il Work Tutor accetti la domanda di lavoro inviata.</p>

<p>Ogni studente ha lo possibilità di fare domanda a tre posti di lavoro.</p>

<p>Se la vostra prima scelta è indirizzata ad un datore che gestisce direttamente le richieste, 
questi potrà chiedervi un’intervista ed accettare direttamente la vostra candidatura.</p>

<p>Se l'organizzazione richiede l’intermediazione del Dott. Avanzi, 
    successivamente ad una verifica dello stesso delle disponibilità del datore di lavoro si riceveranno i dati del Work Tutor</p>

<p>La seconda e la terza scelta non inviano notifiche ai Work Tutor, 
servono per uso interno nel caso la vostra prima scelta non fosse disponibile o presa da un'altro studente.</p>

<p>Tutti gli altri studenti riceveranno i dati del Work Tutor a circa un mese dalla partenza</p>

<p>Qui un video che mostra <a href="https://drive.google.com/file/d/1GUWJmjNOxZEHr5IZEfgeKk20k-xHiqGV/view" target="_blank">come selezionare ed inviare una domanda di lavoro</a></p> 

<p>Qui un video che mostra alcune <a href="https://drive.google.com/file/d/1GUWJmjNOxZEHr5IZEfgeKk20k-xHiqGV/view" target="_blank">funzionalità aggiuntive e l’email di notifica</a> che riceverete con i dati del Work Tutor</p>



<br />
        <h4><strong>SCELTA DELLA SCHOOL HOUSE</strong></h4>
        <p>Le School Houses potranno accettare una vostra richiesta solo dopo che avrete ricevuto conferma del lavoro. </p>        
        <p>Invieremo una email con unteriori informazioni per il caso
        </p>
        <br />
        <h4><strong>NOTIFICHE</strong></h4>
        

<p>Vi chiediamo di aggiungere queste email: notification@work-related-learning.com, 
    esperienzainglese@gmail.com ai vostri contatti per assicurarvi che non vadano a finire nostri messaggi nella vostra casella di spam.</p>

<p>Il sito è completo di Termini e condizioni, Policy, Netiquette, FAQ e altro. Per altri quesiti non esitate a chiedere.</p>
        
        
        <br />
        <p>Vi auguriamo buona esperienza e buon lavoro!</p>
        <p>Dott. Matthew Avanzi</p>
        <br />
        <br />
        <br />
        <div style="color: blue;">
            <p>--</p>
            <p>Dr. Matthew Avanzi</p>
            <br />
            <p>Evitate di mancare nostre email:</p>
            <br />
            <p>
                Aggiungete notification@work-related-learning.com, ed altri nostri account che usate per servizi specifici, alla vostra lista contatti. 
                Ciò per evitare di perdere nostre comunicazioni che potrebbero finire nello spam (inviamo email commerciali solo 2/3 volte l'anno).
            </p>
        </div>
        @include('emails.disclaimer')
    </body>
</html>