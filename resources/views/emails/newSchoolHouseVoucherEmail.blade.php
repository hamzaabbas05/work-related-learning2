<html>
    <head>
        <title>Work-Related-Learning - Welcome - School House</title>
    </head>
    <body>
        <!-- ToNewSchooHouseVoucherEmail-->

        <p>Dear developing School House, 
        </p>
        <p>please complete your profile, add services you provide and wait for admin approval.</p>
       
        <p>First you ought to know the users select your School House according to your profile, the services included in the price, which we have set at £200 a week (£30 a night for shorter stays).
        </p>
        <p>Then you may add optional services paid extra, such as: lessons, activities, tours, arranging pick up and drop off, special diets and so on.
        </p>
<h4>Complete profile</h4>
        <p>
Here 3 videos to show how this can easily be done (on a computer): 
<br />
<br />
1) complete your School House PROFILE: <a href="https://www.youtube.com/watch?v=fkcXx0-vXfU" target="_blank">video tutorial</a> and <a href="http://work-related-learning.com/user/paidServices" target="_blank">link to the page</a>;
<br />
2) be sure you have added School House and Property info, including rooms, at point 1 before proceeding to point 3;
<br />
3) add BEDS (to the rooms you specify under “Property info”) <a href="https://www.youtube.com/watch?v=OmXo4DoTi7A" target="_blank">video tutorial</a> and <a href="http://work-related-learning.com/rentals/add/school-house-bed" target="_blank">link to the page</a> (you must be logged in), and further services you provide at an extra cost in <a href="http://work-related-learning.com/user/listing/rentals/active" target="_blank">"your listings"</a> (you see both paid services and unpaid work you offer if any).
<br />         
        
        </p>
        
        <p>
If you don’t know/can’t remember your password: <a href="https://www.youtube.com/watch?v=fkcXx0-vXfU" target="_blank">video tutorial</a> and link to the homepage <a href="http://work-related-learning.com/" target="_blank">www.work-related-learning.com</a>; 
        </p>
        

<h4>What is expected</h4>
<p>
<strong>You should before arrival:</strong>
</p>
<p>
respond to the email in which you are asked for availability in the appropriate section <a href="http://work-related-learning.com/user/schoolHouses" target="_blank">"Student requests"</a> (you must be logged in);



</p>
<p>
phone work tutor few days before arrival to verify all is in place (eg. not home sick/change of management), this info is sent together with student info;
</p>
<p>
receive a phone call from students few days before their arrival (that is please prompt for a call if they only send an email or message).
</p>

<p><strong>
You should for their stay:
    </strong></p>
<p>
pick up (usually on a Sat afternoon) and drop off (usually on a Sat morning) students in town, select <a href="http://work-related-learning.com/user/paidServices" target="_blank">"Town Centre/Main public transport spot Pick up ad Drop off"</a>";
</p>
<p>
allow, convince, the student to spend a weekend day with you and your family, doing anything, something possibly nice, with them;
</p>
<p>
provide full board: breakfast, lunch or packed lunch and dinner (good food, they are mostly Italian), select <a href="http://work-related-learning.com/user/paidServices" target="_blank">"Full board (Breakfast, lunch and Dinner)*lunch may be packed"</a>;
</p>
<p>
assist students, i.e. with dedicated five 30 minute “conversation sessions” (eg. speaking about work, on first day check they have made the Work Tutor sign the “Learning Agreement”), it will help you better understand the scheme too, select <a href="http://work-related-learning.com/user/paidServices" target="_blank">in section "Classes and Conversation sessions"</a> individual, and plan for few, and groups up to 4, and so on depending on how many students you may fit in your "conversation room"; writing "dedicated five 30 minute conversation sessions" in the description.
</p>

<p>
    <strong>You should in general:</strong>
</p>
follow the specifications on the portal: <a href="http://work-related-learning.com/cms/terms" target="_blank">"T&Cs"</a>, very useful <a href="http://work-related-learning.com/user/paidServices" target="_blank">"FAQs"</a>, Netiquette, Privacy policy and the rest of the info provided;
<p>
bear in mind the “Teacher accompanying the students” is representative of our clients, not a member of our organisation, so they should be assisted and see everything is fine.
</p>

<h4>Work placements</h4>
<p>
Students will make their choices also according to the distance to workplaces, that is you may consider helping us find places near your School House.
</p>
<p>
To have an idea you may here see <a href="https://docs.google.com/document/d/1j3IFRgL76zLPpCJEOy5k7ipVviN5K5m6PzfbMVoRziM/edit?usp=sharing" target="_blank">"our introductory email"</a>.
</p>

               <br />


        <div style="color: blue;">
            <p>--</p>
            <p>Best wishes</p>
            <p>Dr. Avanzi Matthew</p>
            <br />
            
            
            
            
            <p>IMAGE</p>
            
            
            
            
            
            
            <p>Learn in a FLASH Ltd</p>
            <p>20-22 Wenlock Road </p>
            <p>London</p>
            <p>N1 7GU</p>
            <p>United Kingdom</p>
            <br />
            <p><strong>  
                    Avoid missing our emails:</strong>
Add notification@work-related-learning.com and hello@work-related-learning.com to your list of desired contacts if you want to be sure that our emails are not classified as spam by your systems; this not to run the risk of missing out on important news or messages. You may also reply to this email to increase likelihood of getting all our emails but we will not reply to these. Contact your agents for any assistance or us for any matter concerning the Website.
</p>
<p><strong>  
                    Disclaimer:</strong>
This email and any files transmitted with it may be confidential, privileged or otherwise protected by work product immunity or legal rules and supposedly intended for the use of the individual or entity to whom they are addressed. If you have received this email in error, please notify the system manager; you may filter messages (marking as spam could be ineffective and illegal as we do not send spam) from our site in case somebody is using yours inappropriately, or you could register to our website to make sure to get only the messages for your experiences. The integrity and security of this message cannot be guaranteed as transmitted over the internet. The recipient should check this email and any attachments for the presence of viruses and malaware. Our emails may contain links to other websites of interest, once you have used these links, we cannot be held responsible for protection and privacy whilst visiting such sites.
            </p>
        </div>
    </body>
</html>



