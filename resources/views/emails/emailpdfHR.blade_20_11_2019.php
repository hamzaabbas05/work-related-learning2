<html>
<head>
<title>Work-Related-Learning</title>
</head>
<body>
<p>Dear Human Resources, </p>

<p> we thank your company for accepting the student. </p>
<p> You don't need to do anything else until the student arrives.</p>
<p> The student will bring hard copies of (all attached for your convenience): </p><br> 
<p> 1) two copies of the “Learning Agreement”, one for you, one to take back to his/her school signed by the Work Tutor;</p>
<p> 2) an attendance register, ask him/her to complete it (add working day and time and brief description of what done that day), to be signed as well if truthful, to certify the real amount of hours worked which may differ from what on the Learning Agreement;</p>
<p> 3) Evaluation form to be given to the student last day of work. </p> <Br>
<p> Best wishes </p> <br>
<p> Matthew Avanzi</p><br>
 
<p>PS: 
Do not hesitate to contact me in case of any queries
For any issue contact in writing the School tutor. 
</p>
@include('emails.disclaimer')
</body>
</html>