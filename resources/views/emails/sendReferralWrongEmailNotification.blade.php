<html>
    <head>
        <title>Work-Related-Learning wrong referral Email</title>
    </head>
    <body>
        <p>Dear Admin office,</p>
        <p>This user has indicated a person-s email but this is not present in our system.</p>
        <br />
        <p>We have already asked the user to check if the email is correct.</p>
        <br />
        <p>The Memebr who Referred or is Assisting the user may have another email, it is user who should contact this person to double/check the Member has not joined using another email.</p>
        <br />
        <p>Else try to trace back to the Member who referred them to complete procedure.</p>

        <div>
            <br />
            <p><strong>Table with Name, Surname, Mobile and email of the User who added the email which is not present in our system</strong></p>
            <br />
            <table style="width: 100%; border: 1px solid;">
                <tr>
                    <td>Surname and Name</td>
                    <td>Mobile (0039) for Italy</td>
                    <td>Email</td>
                  
                </tr>
                <tr>
                    <?php
                    if (isset($user->sname16) && !empty($user->sname16)) {
                        $lastname = isset($user->ssurname16) ? $user->ssurname16 . " " : "";
                        $firstname = isset($user->sname16) ? $user->sname16 : "";
                        $name = $lastname . $firstname;
                        $contact = isset($user->sphone16) ? $user->sphone16 : "";
                        $email = isset($user->semail16) ? $user->semail16 : "";
                    } else {
                        $lastname = isset($user->lastname) ? $user->lastname . " " : "";
                        $firstname = isset($user->name) ? $user->name : "";
                        $name = $lastname . $firstname;
                        $contact = isset($user->phonenumber) ? $user->phonenumber : "";
                        $email = isset($user->email) ? $user->email : "";
                    }
                    ?>
                    <td>{!! $name !!}</td>
                    <td>{!! $contact !!}</td>
                    <td>{!! $email !!}</td>
                   
                </tr>
            </table>
            <br />
            <p><strong>Table with Name, Surname and email of Person User has identified as Member who Referred or is Assisting him or her.</strong></p>
            <br />
            <table style="width: 100%; border: 1px solid;">
                <tr>
                    <td>Name</td>
                    <td>Surname</td>
                    <td>Email</td>                  
                </tr>
                <tr>
                    <td>{!! isset($referral_email_wrong->name)?$referral_email_wrong->name:'' !!}</td>
                    <td>{!! isset($referral_email_wrong->surname)?$referral_email_wrong->surname:'' !!}</td>
                    <td>{!! isset($referral_email_wrong->email)?$referral_email_wrong->email:'' !!}</td>                   
                </tr>
            </table>
        </div>
       
     
        <p>Best wishes</p>
        <p>Matthew</p>

        <br />

        <div style="color: blue;">
            <p>--</p>
            <p>Dr. Matthew Avanzi</p>
            <br />
            <p>Avoid missing our emails:</p>
            <br />
            @include('emails.disclaimer')
        </div>
    </body>
</html>



