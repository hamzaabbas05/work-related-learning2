<html>
    <head>
        <title>Work-Related-Learning - Scelta della School House</title>
    </head>
    <body>
        <p> Gentilie {{$rentalObj->user->name}}  {{$rentalObj->user->lastname}},</p>
        <p>con la presente informiamo che potete ora selezionare la School House di destinazione. </p>   <br>

        <p>Vi invitiamo pertanto a scegliere la vostra preferita tramite il portale www.work-related-learning.com</p> 
        <p>C'è la possibilità all'interno della pagina dei risultati di limitare la ricerca alle sole school houses.</p> 
        <p>Consigliamo di inviare le richieste il prima possibile, possono occorrere settimane per gestire tutte le richieste, ma comunque una alla volta, ovvero date il tempo alla prima di rispondere prima di inviare alla seconda e terza scelta, potete usare la funzione "Wishlist" cliccando sul cuore nell'angolo in alto a destra per salvare quelle preferite.</p><br>     

        <p>Vi auguriamo buon viaggio, buon soggiorno e buon lavoro!</p>
       <p>Matthew Avanzi</p>        
        <div style="color: blue;">
            <p>--</p>
            <p>Dr. Matthew Avanzi</p>           
            <p>Avoid missing our emails:</p>
            
            @include('emails.disclaimer')
        </div>
    </body>
</html>