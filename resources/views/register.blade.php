@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('country', 'App\Country')
<?php
$countries = $country->getall();
?>
<div class="pos_rel bg_gray1">
    <div class="container">
        @include('layouts.errorsuccess')
        <div class="modal-dialog login_width" role="document">

            <div class="modal-content">
                <div class="modal-body text-center">
                    <p>Signup with </p><br />
                    <!--<a href="#" class="text-danger" style="display: none;">
                        <div id="w0" class="auth-clients">
                            <ul class="auth-clients clear">
                                <li class="auth-client"><a class="auth-link facebook" href="{{URL::to('social/login/redirect/facebook')}}" data-popup-width="800" data-popup-height="500"><span class="auth-icon facebook"></span></a></li>
                                <li class="auth-client"><a class="auth-link google" href="{{URL::to('social/login/redirect/google')}}"><span class="auth-icon google"></span></a></li>
                            </ul>
                        </div>
                    </a>
                    <div class="login_or border_bottom margin_top10"><span>or</span></div>
                    -->
                    <form id="form-signup" action="<?php echo url('user_register') ?>" method="post" role="form">
                        
                    {!! Form::token() !!}

                    <div class="form-group field-signupform-firstname required">

                        <input type="text" id="firstname" value="{{old('firstname')}}" class="form-control margin_top20 margin_bottom10" name="firstname" maxlength="40" placeholder="First Name(s)" onkeypress="return isAlpha(event)">

                        <p class="help-block help-block-error"></p>
                    </div>                <div class="form-group field-signupform-lastname required">

                        <input type="text" id="lastname" class="form-control margin_bottom10" value="{{old('lastname')}}" name="lastname" maxlength="40" placeholder="Family Name(s) / Surname(s)" onkeypress="return isAlpha(event)">

                        <p class="help-block help-block-error"></p>
                    </div>
                    <div class="form-group field-signupform-phonenumber">

                        <input type="text" id="phonenumber" value="{{old('phonenumber')}}" class="form-control margin_top20 margin_bottom10" name="phonenumber" maxlength="40" placeholder="Phone with Country code (i.e. 0044)">

                        <p class="help-block help-block-error"></p>
                    </div> 
                    <div class="form-group field-signupform-email required">

                        <input type="text" id="email" class="form-control margin_bottom10" value="{{old('email')}}" name="email" placeholder="Email Address" autocomplete="off">

                        <p class="help-block help-block-error"></p>
                    </div>                <!--?= $form->errorSummary($model); ?-->
                    
                    <div class="form-group field-signupform-password required">

                        <input type="password" id="password" class="form-control margin_bottom10" name="password" placeholder="Make up a Password" title="Yes, make one up now. We suggest to save it in your browser / device." autocomplete="off">

                        <p class="help-block help-block-error"></p>
                    </div>                 <div class="bdaycls"> <label>Birthday ?</label><br />

                        <select name="bmonth" class="bdayselcls" id="bmonth">
                            <option value="">Month</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <select name="bday" class="bdayselcls" id="bday">
                            <option value="0">Day</option>
                            <?php for ($i = 1; $i <= 31; $i++) { ?>
                                <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                        </select>
                        <select name="byear" class="bdayselcls" id="byear">
                            <option value="">Year</option>
                            <?php
                            $currentYear = date("Y");
                            $startYear = $currentYear - 15;
                            $baseYear = 1900;
                            for ($i = $startYear; $i >= $baseYear; $i--) {
                                ?>
                                <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                        </select>
                    </div>
                    <br/>
<div class="bdaycls" style="margin-top: 20px;">
                        <select class="form-control" style="width:100%!important;" id="profile-gender" name="gender" required>
                            <option value="" >Gender</option>
                            <option value="1" >Male</option>
                            <option value="2" >Female</option>
                            <option value="3" >Other</option>
                        </select>
                    </div>
                    <div class="bdaycls" style="margin-top: 20px;">
                        <label>I am Native speaker of</label><br />
                        <select name="native_language_speaker" class="form-control">
                            <!-- <option value="">I am Native speaker of</option> -->
                            @if(sizeof($languages) > 0)
                            @foreach($languages as $language)
                            <option value="{{$language->id}}" <?php echo ($language->id == 1 ? "selected=selected" :'') ?>>{{$language->language_name}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="bdaycls" style="margin-top: 20px;">
                        <label> Country of Citizenship</label> <br>
                        {!! Form::select('country_id', $countries, '226', ['class'=>'form-control', 'name' => 'country_id', 'id' => 'country_id', 'required']) !!}
                    </div>
                    <br/>
                    <!--                    <div class="bdaycls" style="margin-top: 20px;">
                                            <input type="text" id="voucher_code" class="form-control margin_bottom10 @if(!empty(old('voucher_code'))) error_class @endif" value="{{old('voucher_code')}}" name="voucher_code" placeholder="Voucher Code" autocomplete="off" required="" />
                                            <p class="help-block help-block-error"></p>
                                        </div>-->
                    <!-- <div id="tabs">
                        <ul>
                            <li><a href="#tabs-1">Basic Info</a></li>
                            <li><a href="#tabs-1">Extra Info</a></li>
                        </ul>
                    </div>
                    <div id="tabs-1">Basic Info goes here</div>
                    <div id="tabs-2">Extra Info goes here</div> -->
                    <div class="margin_top10 text-left margin_bottom10 required">
                        <label>Do you have a Voucher Code?</label>
                        <span name="yes" id="yes_voucher_code" class="btn btn_email width50" style="width:49%"> Yes </span>
                        <span name="no" id="no_voucher_code" class="btn btn_email width50" style="width:49%"> No </span>
                        <input type="hidden" name="no_voucher_code" id="no_voucher_code_hidden" class="no_voucher_code_hidden" required>
                    </div>

                    <div class="bdaycls" style="margin-top: 1px; display:none; @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") display:none; @endif" id="showVoucher">
                         <input type="text" id="voucher_code" class="form-control margin_bottom10 @if(!empty(old('voucher_code'))) error_class @endif" value="{{old('voucher_code')}}" name="voucher_code" placeholder="Voucher Code" title="If you don't have one, click on checkbox here under." autocomplete="off" />
                        <p class="help-block help-block-error"></p>
                    </div>

                    <!--                    <div class="bdaycls" style="margin-top: 20px;">
                                            <input type="text" id="voucher_code" class="form-control margin_bottom10 @if(!empty(old('voucher_code'))) error_class @endif" value="{{old('voucher_code')}}" name="voucher_code" placeholder="Voucher Code" autocomplete="off" required="" />
                                            <p class="help-block help-block-error"></p>
                                        </div>-->
                   <!--  <div class="bdaycls" style="margin-top: 1px; @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") display:none; @endif" id="showVoucher">
                         <input type="text" id="voucher_code" class="form-control margin_bottom10 @if(!empty(old('voucher_code'))) error_class @endif" value="{{old('voucher_code')}}" name="voucher_code" placeholder="Voucher Code" title="If you don't have one, click on checkbox here under." autocomplete="off" />
                        <p class="help-block help-block-error"></p>
                    </div> -->
                    <!-- <div class="margin_top10 text-left font_size12 margin_bottom10">
                        <input type="checkbox" value="1" id="no_voucher_code" name="no_voucher_code" @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") checked @endif/>
                               <p style="margin-left:20px;">I have no Voucher Code</p>
                    </div> -->
                    <div class="bdaycls" style="margin-top: 1px; @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") display:block; @else display:none; @endif" id="showNoVoucher">
                         <label>Select Interest in </label><br />
                        <select name="voucher_interest" class="" style="width: 100%;height: 35px;;padding: 6px;">
                            <option value="">Select</option>
                            <option value="NewSchoolHouse" title="Offering paid services to Student Guests" @if(old('voucher_interest') == "NewSchoolHouse") selected="" @endif>New School House</option>
                            <option value="NewWorkTutor" title="Offering unpaid Work Experience to Secondary School Students" @if(old('voucher_interest') == "NewWorkTutor") selected="" @endif>New Organiation / Work Tutor</option>
                            <option value="NewParentInterested" title="Requesting paid services and unpaid Work Experience" @if(old('voucher_interest') == "NewParentInterested") selected="" @endif>New User as Parent / Student Guest</option>
                        </select>
                        <p class="help-block help-block-error"></p>
                    </div>
                    
                    
                    
                    <div class="bdaycls" style="margin-top: 20px;"> 
                        {!! Recaptcha::render() !!}
                    </div>
                    <div class="has-error"><p id="bdayerr" class="help-block help-block-error"></p><p id="hiddenvouchererror" class="help-block help-block-error"></p></div>
                    <div class="margin_top10 text-left font_size12 margin_bottom10"><input id="agree" type="checkbox"><p style="margin-left:20px;">By signing up, I agree to<a href="{{url('cms/terms')}}" target="_blank" class="text-danger">Terms and Conditions.</a> </p></div>
                    <div class="has-error"><p id="agreeerr" class="help-block help-block-error"></p></div>
                    <div class="form-group">
                        <button type="submit" class="btn btn_email margin_top10 width100" name="signup-button">Sign up</button>                </div>
                    <img alt="loading" id="signuploadimg" src="{{ asset('frontendassets/images/load.gif')}}" class="loading" style="margin-top:-1px;">            
                
                    </form>  
                </div>  

            </div>
        </div>
    </div> <!-- container end -->
</div> <!-- list_bg end -->

<style type="text/css">
    .error_class {
        border: 1px solid red;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        // $("#no_voucher_code").click(function(){
        //     if($(this).is(":checked")) {
        //         $("#showVoucher").hide();
        //         $("#showNoVoucher").show();
        //     } else {
        //         $("#showVoucher").show();
        //         $("#showNoVoucher").hide();
        //     }
        // });
         $("#yes_voucher_code").click(function(){
            $("#showVoucher").show();
            $("#showNoVoucher").hide();
            $("#no_voucher_code_hidden").val(0);
            $("#voucher_code").attr("required",true);
        });

         $("#no_voucher_code").click(function(){
            $("#showVoucher").hide();
            $("#showNoVoucher").show();
            $("#no_voucher_code_hidden").val(1);
            $("#voucher_interest").attr("required",true);
        });
    });
</script>
@stop