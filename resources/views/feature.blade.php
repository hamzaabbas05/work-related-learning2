<div class="airfcfx-home-cnt-width container pos_rel">
<div class="text-center margin_top50 margin_bottom30">
            <h1 class="airfcfx-txt-capitalize text-uppercase">Recently Viewed</h1>
			<h4>Find the most recent listings you discovered</h4>
    	</div>	
        <div id="owl-demos" class="owl-carousel owl-theme">
            <div class="owl-wrapper-outer">
                <div class="owl-wrapper" style="left: 0px; display: block; transition: all 1000ms ease 0s; transform: translate3d(0px, 0px, 0px);"><div class="airfcfx-homerecent-pad-10 col-xs-12 col-sm-4"><a class="rm_text_deco" href="http://airfinchscript.com/user/listing/view/MTZfODAzMA==">
                        <div class="item">
                            <div class=" bg_img " style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/listings/1458557152_1_0.jpg&amp;w=465&amp;h=330);">
                                <div class="bg_img1" style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/users/1_2016022610.JPG&amp;w=56&amp;h=56);">
                                </div>
                                <div class="price"><h3> $ 700</h3>
                                </div></div>
                            <p class="margin_right60 margin_top10 margin_left10 fa-1x pointer">Rue Daval Bastille </p>
                            <p class="small margin_left10 text_gray1"><b>  Pop Shop  </b></p>
                        </div></a>
                    </div><div class="airfcfx-homerecent-pad-10 col-xs-12 col-sm-4"><a class="rm_text_deco" href="http://airfinchscript.com/user/listing/view/MzU5Xzc5Nw==">
                        <div class="item">
                            <div class=" bg_img " style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/listings/1466764386_4_0.jpg&amp;w=465&amp;h=330);">
                                <div class="bg_img1" style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/users/4_adframe546.jpg&amp;w=56&amp;h=56);">
                                </div>
                                <div class="price"><h3> zł 500</h3>
                                </div></div>
                            <p class="margin_right60 margin_top10 margin_left10 fa-1x pointer">vdfsgsdg </p>
                            <p class="small margin_left10 text_gray1"><b>  Event Space  </b></p>
                        </div></a>
                    </div><div class="airfcfx-homerecent-pad-10 col-xs-12 col-sm-4"><a class="rm_text_deco" href="http://airfinchscript.com/user/listing/view/OTg0Xzc4Njg=">
                        <div class="item">
                            <div class=" bg_img " style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/listings/1478865896_4_0.jpg&amp;w=465&amp;h=330);">
                                <div class="bg_img1" style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/users/4_adframe546.jpg&amp;w=56&amp;h=56);">
                                </div>
                                <div class="price"><h3> $ 500</h3>
                                </div></div>
                            <p class="margin_right60 margin_top10 margin_left10 fa-1x pointer">Turkey list </p>
                            <p class="small margin_left10 text_gray1"><b>  Pop Shop  </b></p>
                        </div></a>
                    </div></div>
            </div>
        </div>
    </div>