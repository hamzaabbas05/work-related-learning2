@extends('layouts.frontdefault')
@section('header_styles')
<link href="{{ asset('frontendassets/css/jslider.css')}}" rel="stylesheet">
@stop
@section('header_js')
<script src="{{ asset('frontendassets/js/jquery.slider.bundle.js')}}"></script>
<script src="{{ asset('frontendassets/js/jquery.slider.js')}}"></script>
@stop
@section('nav_bar')
@include('partials.indexnavbar')
@stop
{{-- content --}}
@section('content')
@inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>@inject('settings', 'App\Settings')
<?php
$settingsObj = $settings->settingsInfo();
$logoname = $settingsObj->logo;
$profile_noimage = $settingsObj->no_image_profile;
?>
@inject('country', 'App\Country')
<?php
$countries = $country->getall();
?>
<div class="banner" style="background-image:url({{asset('images/banners/'.$homeBanner->banner_img)}});">
    <div class="airfcfx-home-cnt-width container">
        <div class="pos_rel banner_text text_white"> 
            @include('layouts.errorsuccess')
            <h1>{{$homeBanner->title}}</h1>
            <h4>{{$homeBanner->text}}</h4>

            <div class="airfcfx-home-website-search banner_form margin_top40">  
                <div class="banner_form_left">   
                     <?php  
                    //if($_SERVER['REMOTE_ADDR'] == '111.119.187.57'){
                    //    $custom_width = 'width:20% !important';
                    //}else{
                         $custom_width = 'width:20% !important';
                    //}
                    ?>                   
                    <input id="where-to-go-main" type="text" value="" placeholder="Where do you want to go?" class="form-control form_text1 where-to-go" /> 
                    <input type="hidden" id="latitude" value="">
                    <input type="hidden" id="longitude" value="">
                    <input id="check-in-main" value="" readonly class="airfcfx-check-cal form-control cal form_text2 check-in" style="<?php echo $custom_width; ?>" placeholder="Start Date">
                    <input id="check-out-main" value="" readonly class="airfcfx-check-cal form-control cal form_text2 check-out" style="<?php echo $custom_width; ?>"  placeholder="End Date">
                    <?php  
					//echo $_SERVER['REMOTE_ADDR'];
                   // if($_SERVER['REMOTE_ADDR'] == '111.119.187.57'){
                    ?>
                        <select id="guest-count" style="<?php echo $custom_width; ?>"  class="airfcfx-guest-count form-control form_text2 guest-count" name="numofstudents">
                    <?php for ($i = 1; $i <= $PropertySettingsObj->accomodates_no; $i++) { ?>
                             <option value="{{$i}}"><?php echo $i." students"; ?></option>
                    <?php } ?>
                            </select> 
                    <?php //} ?>                                  
                </div>
                <div class="banner_form_right">
                    <button class="airfcfx-btn_search btn btn_search width100" type="button" onclick="searchlistmain();" />
                    <div class="airfcfx-search-icon"></div><div class="airfcfx-mobile-search-txt"> Search</div>
                    </button>

                </div>
                <div class="error-search" style="float:right;"></div>
            </div> <!--banner_form end-->

            <div class="airfcfx-home-mobile-cal col-xs-12 no-hor-padding">
                <a href="#" class="airfcfx-input-model" data-toggle="modal" data-target="#airfcfx-mobile-cal">Where to ?</a>
                <a href="#" class="airfcfx-mobile-cal-search" data-toggle="modal" data-target="#airfcfx-mobile-cal"><div class="airfcfx-search-icon"></div></a>
            </div>

            <!-- Mobile Responsive-->

            <div class="modal fade" id="airfcfx-mobile-cal" role="dialog"> 
                <div class="modal-dialog mobile-cal-cnt">
                    <div class="mobile-cal-header">
                        <button class="close airfcfx-mobile-cal-close" type="button" data-dismiss="modal">×</button>
                        Search          </div>
                    <div class="mobile-cal-body">
                        <input id="where-to-go-mobile" type="text" value="" placeholder="Where do u want to go?" class="mobile-cal-input-100 form-control form_text1 where-to-go" /> 
                        <input type="hidden" id="latitudemobile" value="">
                        <input type="hidden" id="longitudemobile" value="">
                        <input id="check-in-mobile" value="" readonly class="mobile-cal-input-50 airfcfx-check-cal form-control cal form_text2 check-in" placeholder="Check in">
                        <input id="check-out-mobile" value="" readonly class="pull-right mobile-cal-input-50 airfcfx-check-cal form-control cal form_text2 check-out" placeholder="Check Out ">
                        <div class="error-searchmobile" style="float:right;"></div>
                        <button class="airfcfx-mobile-cal-btn airfcfx-slider-btn btn btn_search" onclick="searchlistmobile();">Find a place</button>

                    </div>
                </div>
            </div>
            <!-- Mobile Responsive End-->      




        </div>  <!--banner_text end-->

    </div>
    <!--container end-->
    <!--Section for 4 menus Start-->
    <div class="container marginTop100 sectionSearchLinks">
        <div class="row">
            <div class="col-sm-3 col-xs-6">
                <a href="{{ url('search_WorkAndUsersListingPaid/learn') }}" class="page-links" style="background-image:url({{url('/images/homepageicons/learnbg.png')}})">
                    <img src="{{asset('images/homepageicons/learn.png')}}" alt="learn">
                    Learn
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="{{ url('search_WorkAndUsersListingPaid/live') }}" class="page-links" style="background-image:url({{asset('images/homepageicons/livebg.png')}})">
                    <img src="{{asset('images/homepageicons/live.png')}}" alt="live">
                    Live
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="{{ url('search_WorkAndUsersListingPaid/work') }}" class="page-links" style="background-image:url({{asset('images/homepageicons/workbg.png')}})">
                    <img src="{{asset('images/homepageicons/work.png')}}" alt="work">
                    Work
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="{{ url('search_WorkAndUsersListingPaid/enjoy') }}" class="page-links" style="background-image:url({{asset('images/homepageicons/enjoybg.png')}})">
                    <img src="{{asset('images/homepageicons/enjoy.png')}}" alt="enjoy">
                    Enjoy
                </a>
            </div>
        </div>


    </div>
    <!--Section 4 menus End-->
</div>
<!--banner end-->

<div class="airfcfx-home-cnt-width container"> 
    <div id="carousel-example-generic" class="airfcfx-carousel carousel slide margin_top30" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php
            $i = 1;
            foreach ($homeCenterBanner as $obj) {
                ?>
                <div class="item <?php if ($i == 1) { ?> active <?php } ?>"><div class="airfcfx-slide_one slide_one" style="background-image:url({{asset('images/banners/'.$obj->banner_img)}});"></div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-hor-padding">
                        <div class="airfcfx-carousel-caption carousel-caption slide_caption1">
                            <h1>{{$obj->title}}</h1>
                            <h4>{{$obj->text}}</h4>

                        </div> 
                    </div>   <!-- col-sm-4 end --> 
                </div> 

                <?php
                $i++;
            }
            ?>
        </div>



    </div> <!-- slide end -->

</div> <!-- container end -->
<div class="airfcfx-home-cnt-width container"> 
    <div class="text-center margin_top50 margin_bottom30">
        <h1 class="airfcfx-txt-capitalize text-uppercase">Popular Work experiences</h1>
        <h4>List of Popular Work experiences. </h4>
    </div>
    <div id="carousel-example-generic1" class="airfcfx-carousel carousel slide margin_top30" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php
            $i = 1;
            foreach ($propobj->chunk(3) as $obj) {
                ?>
                <div class="item <?php if ($i == 1) { ?> active <?php } ?>">


                    <div id="owl-demos" class="owl-carousel owl-theme">
                        <div class="owl-wrapper-outer">
                            <div class="owl-wrapper" style="left: 0px; display: block; transition: all 1000ms ease 0s; transform: translate3d(0px, 0px, 0px);">
                                <?php
                                foreach ($obj as $pa) {
                                    foreach ($pa->propertyimages as $img) {
                                        $imgu = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
                                    }
                                    $imgName = asset('images/noimage/' . $profile_noimage);
                                    if ($pa->user->profile_img_name != "" && !is_null($pa->user->profile_img_name)) {
                                        $imgName = asset('images/profile/' . $pa->user->profile_img_name);
                                    }
                                    ?> 
                                    <div class="airfcfx-homerecent-pad-10 col-xs-12 col-sm-4">
                                        <a class="rm_text_deco" href='{{url("view/rentals/$pa->id")}}'>
                                            <div class="item">
                                                <div class=" bg_img " style="background-image:url({{$imgu}});">
                                                    <div class="bg_img1" style="background-image:url({{$imgName}});">
                                                    </div>
                                                    <div class="price"><h3> € {{$pa->price_night}}</h3>
                                                    </div></div>
                                                <p class="margin_right60 margin_top10 margin_left10 fa-1x pointer">
                                                    {{$pa->title}} </p>
                                                <p class="small margin_left10 text_gray1"><b>{{$pa->propertytype->name}}</b></p>
                                            </div></a>
                                    </div>


                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div> 

                <?php
                $i++;
            }
            ?>


        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#carousel-example-generic1" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic1" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div> <!-- slide end -->

</div> <!-- container end -->






</div>

<div class="airfcfx-home-cnt-width container margin_bottom30">

    <div class="text-center margin_top50 margin_bottom30">
        <h1 class="airfcfx-txt-capitalize text-uppercase">How Do I set up my Account and Listings</h1>
        <h4>Steps to follow</h4>
    </div>

    <div class="airfcfx-reason_one-cnt col-xs-12 col-sm-3">
        <div class="airfcfx-places airfcfx-reason_one reason_one">
            <div class="text-center">
                <h1 class="text_white">Create an Account</h1>
                <p class="reason_text">Free of charge, minimum info to start, Students are responsibe for the paperwork.</p>
                <div class="airfcfx-support-block-line"></div>
            </div>
        </div>  <!-- reason_one end -->
    </div> <!-- col-sm-4 end -->

    <div class="airfcfx-reason_one-cnt col-xs-12 col-sm-3">
        <div class="airfcfx-customers airfcfx-reason_one reason_one">
            <div class="text-center">
                <h1 class="text_white">List Experience</h1>
                <p class="reason_text">Describe in detail, set extra rules and conditions, duties, so students knows what to expect/</p>
                <div class="airfcfx-support-block-line"></div>
            </div>
        </div>  <!-- reason_one end -->
    </div> <!-- col-sm-4 end -->

    <div class="airfcfx-reason_one-cnt col-xs-12 col-sm-3">
        <div class="airfcfx-support  airfcfx-reason_one reason_one">
            <div class="text-center">
                <h1 class="text_white">Get Guidance</h1>
                <p class="reason_text">Your member assistant will help you, just ask for any details.</p>
                <div class="airfcfx-support-block-line"></div>
            </div>
        </div>  <!-- reason_one end -->
    </div> <!-- col-sm-4 end -->

    <div class="airfcfx-reason_one-cnt col-xs-12 col-sm-3">
        <div class="airfcfx-support  airfcfx-reason_one reason_one">
            <div class="text-center">
                <h1 class="text_white">Get Students</h1>
                <p class="reason_text">Students may choose your listing and you may confirm.</p>
                <div class="airfcfx-support-block-line"></div>
            </div>
        </div>  <!-- reason_one end -->
    </div> <!-- col-sm-4 end -->

</div> <!-- Container end -->



<div class="airfcfx-home-cnt-width container  margin_bottom30 ">
    <div class="text-center margin_top50 margin_bottom30">
        <h1 class="text-uppercase">How it works?</h1>
    </div>
    <div class="col-xs-12 col-sm-3">
        <div class="community1 text-center">
            <a href="#">
                <button class="btn btn_travel">Browse Listings</button>
                <img src="<?php echo url('frontendassets/images/create.png') ?>" alt="create"  />
                <br/>

                <p class="text_white">Students choose their work experience according to the description, type, rules and duties.</p>
            </a>
        </div> <!-- community1 end -->
    </div> <!-- col-sm-3 end -->

    <div class="col-xs-12 col-sm-3">
        <a href="#">
            <div class="community2" style="background-image:url(frontendassets/images/create1.jpg);">
                <button class="btn btn_travel">Job Interview</button>
                <div class="community_text text-center">

                    <p class="text_white ">Eventually using messaging service before approval and refusal by tutor.</p>
                </div> <!-- community2_text end -->
            </div> <!-- community2 end -->
        </a>
    </div> <!-- col-sm-3 end -->

    <div class="col-xs-12 col-sm-3">
        <a href="#">
            <div class="community2" style="background-image:url(frontendassets/images/create2.jpg);">
                <button class="btn btn_business">Confirmation</button>
                <div class="community_text text-center">

                    <p class="text_white ">Upon Cofirmation by Work Tutor, work-related-learning.org provides details and paperwork.</p>
                </div> <!-- community2_text end -->
            </div> <!-- community2 end -->
        </a>
    </div> <!-- col-sm-3 end -->

    <div class="col-xs-12 col-sm-3">
        <a href="#">
            <div class="community2" style="background-image:url(frontendassets/images/create3.jpg);">
                <button class="btn btn_host">Feedback</button>
                <div class="community_text text-center">

                    <p class="text_white ">Evaluation will count as a school mark, Work tutor will fill in online form via email.</p>
                </div> <!-- community2_text end -->
            </div> <!-- community2 end -->
        </a>
    </div> <!-- col-sm-3 end -->

</div> <!-- Container end -->


<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog login_width" role="document">
        <div class="modal-content">
            <div class="modal-header no_border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
            </div>
            <div class="modal-body text-center">
                <p>Signup with </p><br />
                <!--
                <a href="#" class="text-danger">
                    <div id="w0" class="auth-clients"><ul class="auth-clients clear">
                            <li class="auth-client"><a class="auth-link facebook" href="{{URL::to('social/login/redirect/facebook')}}" data-popup-width="800" data-popup-height="500"><span class="auth-icon facebook"></span></a></li>
                            <li class="auth-client"><a class="auth-link google" href="{{URL::to('social/login/redirect/google')}}"><span class="auth-icon google"></span></a></li>



                        </ul></div>       
                </a>
                <div class="login_or border_bottom margin_top10"><span>or</span></div>
                -->
                <form id="form-signup" action="<?php echo url('user_register') ?>" method="post" role="form">
                    {!! Form::token() !!}

                    <div class="form-group field-signupform-firstname required">

                        <input type="text" id="firstname" value="{{old('firstname')}}" class="form-control margin_top20 margin_bottom10" name="firstname" maxlength="40" placeholder="First Name(s)" onkeypress="return isAlpha(event)">

                        <p class="help-block help-block-error"></p>
                    </div>                <div class="form-group field-signupform-lastname required">

                        <input type="text" id="lastname" class="form-control margin_bottom10" value="{{old('lastname')}}" name="lastname" maxlength="40" placeholder="Family Name(s) / Surname(s)" onkeypress="return isAlpha(event)">

                        <p class="help-block help-block-error"></p>
                    </div>
                    <div class="form-group field-signupform-phonenumber">

                        <input type="text" id="phonenumber" value="{{old('phonenumber')}}" class="form-control margin_top20 margin_bottom10" name="phonenumber" maxlength="40" placeholder="Phone with Country code (i.e. 0044)">

                        <p class="help-block help-block-error"></p>
                    </div> 
                    <div class="form-group field-signupform-email required">

                        <input type="text" id="email" class="form-control margin_bottom10" value="{{old('email')}}" name="email" placeholder="Email Address" autocomplete="off">

                        <p class="help-block help-block-error"></p>
                    </div>                <!--?= $form->errorSummary($model); ?-->
                    
                    <div class="form-group field-signupform-password required">

                        <input type="password" id="password" class="form-control margin_bottom10" name="password" placeholder="Make up a Password" title="Yes, make one up now. We suggest to save it in your browser / device." autocomplete="off">

                        <p class="help-block help-block-error"></p>
                    </div>                 <div class="bdaycls"> <label>Birthday ?</label><br />

                        <select name="bmonth" class="bdayselcls" id="bmonth">
                            <option value="">Month</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <select name="bday" class="bdayselcls" id="bday">
                            <option value="0">Day</option>
                            <?php for ($i = 1; $i <= 31; $i++) { ?>
                                <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                        </select>
                        <select name="byear" class="bdayselcls" id="byear">
                            <option value="">Year</option>
                            <?php
                            $currentYear = date("Y");
                            $startYear = $currentYear - 15;
                            $baseYear = 1900;
                            for ($i = $startYear; $i >= $baseYear; $i--) {
                                ?>
                                <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                        </select>
                    </div>
                    <br/>
<div class="bdaycls" style="margin-top: 20px;">
                        <select class="form-control" style="width:100%!important;" id="profile-gender" name="gender" required>
                            <option value="" >Gender</option>
                            <option value="1" >Male</option>
                            <option value="2" >Female</option>
                            <option value="3" >Other</option>
                        </select>
                    </div>
                    <div class="bdaycls" style="margin-top: 20px;">
                        <label>I am Native speaker of</label><br />
                        <select name="native_language_speaker" class="form-control">
                            <!-- <option value="">I am Native speaker of</option> -->
                            @if(sizeof($languages) > 0)
                            @foreach($languages as $language)
                            <option value="{{$language->id}}" <?php echo ($language->id == 1 ? "selected=selected" :'') ?>>{{$language->language_name}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="bdaycls" style="margin-top: 20px;">
                        <label> Country of Citizenship</label> <br>
                        {!! Form::select('country_id', $countries, '226', ['class'=>'form-control', 'name' => 'country_id', 'id' => 'country_id', 'required']) !!}
                    </div>
                    <br/>
                    <!--                    <div class="bdaycls" style="margin-top: 20px;">
                                            <input type="text" id="voucher_code" class="form-control margin_bottom10 @if(!empty(old('voucher_code'))) error_class @endif" value="{{old('voucher_code')}}" name="voucher_code" placeholder="Voucher Code" autocomplete="off" required="" />
                                            <p class="help-block help-block-error"></p>
                                        </div>-->
                    <!-- <div id="tabs">
                        <ul>
                            <li><a href="#tabs-1">Basic Info</a></li>
                            <li><a href="#tabs-1">Extra Info</a></li>
                        </ul>
                    </div>
                    <div id="tabs-1">Basic Info goes here</div>
                    <div id="tabs-2">Extra Info goes here</div> -->
                    <div class="margin_top10 text-left margin_bottom10 required">
                        <label>Do you have a Voucher Code?</label>
                        <span name="yes" id="yes_voucher_code" class="btn btn_email width50" style="width:49%"> Yes </span>
                        <span name="no" id="no_voucher_code" class="btn btn_email width50" style="width:49%"> No </span>
                        <input type="hidden" name="no_voucher_code" id="no_voucher_code_hidden" class="no_voucher_code_hidden" required>
                    </div>

                    <div class="bdaycls" style="margin-top: 1px; display:none; @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") display:none; @endif" id="showVoucher">
                         <input type="text" id="voucher_code" class="form-control margin_bottom10 @if(!empty(old('voucher_code'))) error_class @endif" value="{{old('voucher_code')}}" name="voucher_code" placeholder="Voucher Code" title="If you don't have one, click on checkbox here under." autocomplete="off" />
                        <p class="help-block help-block-error"></p>
                    </div>

                    <!--                    <div class="bdaycls" style="margin-top: 20px;">
                                            <input type="text" id="voucher_code" class="form-control margin_bottom10 @if(!empty(old('voucher_code'))) error_class @endif" value="{{old('voucher_code')}}" name="voucher_code" placeholder="Voucher Code" autocomplete="off" required="" />
                                            <p class="help-block help-block-error"></p>
                                        </div>-->
                   <!--  <div class="bdaycls" style="margin-top: 1px; @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") display:none; @endif" id="showVoucher">
                         <input type="text" id="voucher_code" class="form-control margin_bottom10 @if(!empty(old('voucher_code'))) error_class @endif" value="{{old('voucher_code')}}" name="voucher_code" placeholder="Voucher Code" title="If you don't have one, click on checkbox here under." autocomplete="off" />
                        <p class="help-block help-block-error"></p>
                    </div> -->
                    <!-- <div class="margin_top10 text-left font_size12 margin_bottom10">
                        <input type="checkbox" value="1" id="no_voucher_code" name="no_voucher_code" @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") checked @endif/>
                               <p style="margin-left:20px;">I have no Voucher Code</p>
                    </div> -->
                    <div class="bdaycls" style="margin-top: 1px; @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") display:block; @else display:none; @endif" id="showNoVoucher">
                         <label>Select Interest in </label><br />
                        <select name="voucher_interest" class="" style="width: 100%;height: 35px;;padding: 6px;">
                            <option value="">Select</option>
                            <option value="NewSchoolHouse" title="Offering paid services to Student Guests" @if(old('voucher_interest') == "NewSchoolHouse") selected="" @endif>New School House</option>
                            <option value="NewWorkTutor" title="Offering unpaid Work Experience to Secondary School Students" @if(old('voucher_interest') == "NewWorkTutor") selected="" @endif>New Organiation / Work Tutor</option>
                            <option value="NewParentInterested" title="Requesting paid services and unpaid Work Experience" @if(old('voucher_interest') == "NewParentInterested") selected="" @endif>New User as Parent / Student Guest</option>
                        </select>
                        <p class="help-block help-block-error"></p>
                    </div>
                    
                    
                    
                    <div class="bdaycls" style="margin-top: 20px;"> 
                        {!! Recaptcha::render() !!}
                    </div>
                    <div class="has-error"><p id="bdayerr" class="help-block help-block-error"></p><p id="hiddenvouchererror" class="help-block help-block-error"></p></div>
                    <div class="margin_top10 text-left font_size12 margin_bottom10"><input id="agree" type="checkbox"><p style="margin-left:20px;">By signing up, I agree to<a href="{{url('cms/terms')}}" target="_blank" class="text-danger">Terms and Conditions.</a> </p></div>
                    <div class="has-error"><p id="agreeerr" class="help-block help-block-error"></p></div>
                    <div class="form-group">
                        <button type="submit" class="btn btn_email margin_top10 width100" name="signup-button">Sign up</button>                </div>
                    <img alt="loading" id="signuploadimg" src="{{ asset('frontendassets/images/load.gif')}}" class="loading" style="margin-top:-1px;">            
                </form>  
            </div>  

        </div>
    </div>
</div>



<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog login_width" role="document">
        <div class="modal-content">
            <div class="modal-header no_border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
            </div>
            <div class="modal-body text-center">
                <p>Signin</p><br />
                <!--
                <a href="#" class="text-danger">
                    <div id="w1" class="auth-clients"><ul class="auth-clients clear">
                            <li class="auth-client"><a class="auth-link facebook" href="{{URL::to('social/login/redirect/facebook')}}" data-popup-width="800" data-popup-height="500"><span class="auth-icon facebook"></span></a></li>

                            <li class="auth-client"><a class="auth-link google" href="{{URL::to('social/login/redirect/google')}}"><span class="auth-icon google"></span></a></li></ul></div>       </a>
                <div class="login_or border_bottom margin_top10"><span>or</span></div>  
                -->
                <form id="login-form" action="<?php echo url('user_login') ?>"" method="post" role="form">

                    <div class="form-group field-signupform-email required">

                        <input type="text" id="login-email" class="form-control margin_top30 margin_bottom10" name="email" placeholder="Email">

                        <p class="help-block help-block-error"></p>
                    </div>
                    <div class="form-group field-signupform-password required">

                        <input type="password" id="login-password" class="form-control margin_bottom10" name="password" placeholder="Password">

                        <p class="help-block help-block-error"></p>
                    </div>        <div class="pull-left margin_bottom10">
                        <input type="hidden" value="0" name="SignupForm[rememberMe]">
                        <input id="login-rememberMe" type="checkbox" name="rememberMe">
                        <div class="airfcfx-search-checkbox-text">Remember me</div>
                    </div>
                    <p class="text-right text-danger margin_bottom10">
                        <a href="#" data-toggle="modal" data-target="#myModalpass" id="forgotpass">
                            Forgot Password?        </a>
                    </p>

                    <div class="form-group">
                        <button type="submit" class="btn btn_email margin_top10 width100" name="login-button">Login</button>                </div>
                    <img alt="loading" id="loginloadimg" src="<?php echo url('images/load.gif') ?>" class="loading" style="margin-top:-1px;">            </form>            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="myModalpass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog login_width" role="document">
        <div class="modal-content">
            <div class="modal-header no_border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
            </div>
            <div class="modal-body text-center">
                Enter the email address associated with your account, and we'll email you a link to reset your password.   
                <form id="" action="{{url('/password/email')}}" method="post" role="form">

                    <div class="form-group field-passwordresetrequestform-email">

                        <input type="text" id="passwordresetrequestform-email" class="form-control margin_top30" name="email" placeholder="Email">

                        <p class="help-block help-block-error"></p>
                    </div>
                    <div class="login_or border_bottom margin_top10"></div>      
                    <div class="form-group">
                        <button type="submit" class="btn btn_email margin_top10 width100" name="reset-button">Send Reset Link</button>                </div>

                </form>            </div>

        </div>
    </div>
</div>
<style type="text/css">
    .error_class {
        border: 1px solid red;
    }
</style>
<script>
    $(document).ready(function () {
        // $("#no_voucher_code").click(function(){
        //     if($(this).is(":checked")) {
        //         $("#showVoucher").hide();
        //         $("#showNoVoucher").show();
        //     } else {
        //         $("#showVoucher").show();
        //         $("#showNoVoucher").hide();
        //     }
        // });
        $("#yes_voucher_code").click(function(){
            $("#showVoucher").show();
            $("#showNoVoucher").hide();
            $("#no_voucher_code_hidden").val(0);
            $("#voucher_code").attr("required",true);
        });

         $("#no_voucher_code").click(function(){
            $("#showVoucher").hide();
            $("#showNoVoucher").show();
            $("#no_voucher_code_hidden").val(1);
            $("#voucher_interest").attr("required",true);
        });
    });
    $('.carousel').carousel({
    interval: 4000
    });
    $(document).ready(function() {
    $("#where-to-go-main").val("");
    $("#check-in-main").val("");
    $("#check-out-main").val("");
    $('#forgotpass').click(function () {
    $("#forgotpass").attr('data-dismiss', 'modal');
    /*document.cookie = "dismiss=true";
     // Load up a new modal...
     $('#loginModal').hide();
     $("#loginModal").removeClass("in");
     $(".modal-backdrop").remove();*/
    $("body").css("padding-right", "0px");
    });
    //$(document).on('keyup',"", function (event) {
    $("#check-in-main").keydown(function(event){
    if (event.which == 13) {
    $("#check-in-main").readonlyDatepicker(true);
    }
    });
    $("#check-out-main").keydown(function(event){
    if (event.which == 13) {
    $("#check-out-main").readonlyDatepicker(true);
    }
    });
    });
    function close_model_popup()
    {
    $("#welcomepopup").removeClass("in");
    $("#welcomepopup").css("display", "none");
    }


</script>
<script>
    $(document).ready(function() {

    $(".modal").on("hidden.bs.modal", function(){
    $(".help-block").html("");
    $(".form-group").removeClass("has-error");
    });
    $("#w2-success-0").css("display", "block");
    $("#w2-success-0").css("right", "0");
    });
    setTimeout(function() {
    $("#w2-success-0").css("right", "-845px");
    }, 4000);
</script>
@stop
