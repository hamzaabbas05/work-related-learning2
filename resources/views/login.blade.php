@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')


<div class="pos_rel bg_gray1">
    @if (session()->has('flash_notification.message'))
    <div class="alert alert-{{ session('flash_notification.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        {!! session('flash_notification.message') !!}
    </div>
    @endif
    <div class="container">
        <div class="modal-dialog login_width" role="document">
            <div class="modal-content" style="box-shadow: none;">
                <div class="modal-body text-center">
                    <p>Sign in</p><br />
                    <!--
                          <a href="#" class="text-danger">
                          <div id="w0" class="auth-clients"><ul class="auth-clients clear"><li class="auth-client"><a class="auth-link facebook" href="" data-popup-width="800" data-popup-height="500"><span class="auth-icon facebook"></span></a></li><li class="auth-client"><a class="auth-link google" href=""><span class="auth-icon google"></span></a></li></ul></div>		
                  </a>
                  <div class="login_or border_bottom margin_top10"><span>or</span></div>
                  
                    -->
                    <form id="login-form" action="<?php echo url('user_login') ?>" method="post" role="form">

                        <div class="form-group field-signupform-email required">

                            <input type="text" id="login-email" class="form-control margin_top30 margin_bottom10" name="email" placeholder="Email">

                            <p class="help-block help-block-error"></p>
                        </div>
                        <div class="form-group field-signupform-password required">

                            <input type="password" id="login-password" class="form-control margin_bottom10" name="password" placeholder="Password">

                            <p class="help-block help-block-error"></p>
                        </div>        <div class="pull-left margin_bottom10">
                            <input type="hidden" value="0" name="SignupForm[rememberMe]">
                            <input id="login-rememberMe" type="checkbox" name="rememberMe">
                            <div class="airfcfx-search-checkbox-text">Remember me</div>
                        </div>
                        <p class="text-right text-danger margin_bottom10">
                            <a href="#" data-toggle="modal" data-target="#myModalpass" id="forgotpass">
                                Forgot Password?        </a>
                        </p>

                        <div class="form-group">
                            <input type="hidden" name="previous_url" value="{{isset($previous_url)?$previous_url:''}}" />
                            <button type="submit" class="btn btn_email margin_top10 width100" name="login-button">Login</button>                
                        </div>
                        <img alt="loading" id="loginloadimg" src="<?php echo url('images/load.gif') ?>" class="loading" style="margin-top:-1px;">  
                    </form>        







                </div>  

            </div>
        </div>
    </div> <!-- container end -->
</div> <!-- list_bg end -->

<div class="modal fade" id="myModalpass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog login_width" role="document">
        <div class="modal-content">
            <div class="modal-header no_border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
            </div>
            <div class="modal-body text-center">
                Enter the email address associated with your account, and we'll email you a link to reset your password.      
                <form id="" action="{{url('/password/email')}}" method="post" role="form">

                    <div class="form-group field-passwordresetrequestform-email">

                        <input type="text" id="passwordresetrequestform-email" class="form-control margin_top30" name="email" placeholder="Email">

                        <p class="help-block help-block-error"></p>
                    </div>
                    <div class="login_or border_bottom margin_top10"></div>      
                    <div class="form-group">
                        <button type="submit" class="btn btn_email margin_top10 width100" name="reset-button">Send Reset Link</button>                </div>

                </form>          </div>

        </div>
    </div>
</div>
@stop