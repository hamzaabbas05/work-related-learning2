@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">  


        <div class="col-xs-12 col-sm-12 margin_top20">        
			@include('layouts.errorsuccess')
            <div class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel-padding airfcfx-panel panel-heading profile_menu1">
                    <h3 class="panel-title">Your Trips</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="trips-cnt margin_top10 margin_bottom20 paddingleft table-responsive col-sm-12 col-lg-12"><table class="table table_no_border tripdesign">
                                <thead>
                                    <tr class="review_tab">
                                        <th>Sno</th>
                                        <th>Dates and Location</th>
                                        <th>Booking No</th>
                                        <!--<th>Booking</th>-->
                                        <th>Approval</th>
                                        <th>Message / Host Profile</th>
                                        <th>Send Feedback</th>
                                    </tr>
                                </thead><tbody>
                                    <?php
                                    $i = 1;

                                    foreach ($tripsObj as $trips) {
                                         
                                        // exit;
                                        $class = "successtxt";
                                        if ($trips->booking_status == 'Pending') {
                                            $class = "text-danger";
                                        } if ($trips->booking_status == 'Cancelled') {
                                            $class = "text-medium";
                                        }

                                        $class1 = "successtxt";
                                        if ($trips->approval == 'Pending') {
                                            $class1 = "text-danger";
                                        } if ($trips->approval == 'Decline') {
                                            $class1 = "text-medium";
                                        }


                                        $id = "reserve" . $trips->id;
                                        ?> 
                                        <tr id="{{$id}}">
                                            <td>{{$i}}</td>
                                            <td class="airfcfx-breakword"><p class="airfcfx-td-dtnloc-trp">{{date("Y-m-d", strtotime($trips->checkin))}}- {{date("Y-m-d",strtotime($trips->checkout))}}</p><p class="airfcfx-td-dtnloc-trp"><a class="text-danger" href="">{{isset($trips->property->auto_complete)?$trips->property->auto_complete:""}}</a></p>
                                                <p class="airfcfx-td-dtnloc-trp"></p>
                                                <div style="">
                                                    {{$trips->user->aboutorg}}<br>
                                                    {{$trips->property->exptitle}}<br>
                                                    {{$trips->property->work_schedule}}<br>
                                                    {{$trips->property->description}}<br>
                                                    {{$trips->property->houserules}}<br>
                                                    Host : {{$trips->property->user->name}}</div><p></p></td>
                                            <td align="center">{{$trips->Bookingno}}</td>
<!--                                            <td class="airfcfx-min-width-80px">
                                                <p class="{{$class}}"><b>{{$trips->booking_status}}</b></p>
                                            </td>-->
                                            <td class="airfcfx-min-width-80px">
                                                <p class="{{$class1}}"><b>
                                                
                                                    <?php 
                                                    $newDateStart = $trips->checkin;
                                                    $newDateEnd = $trips->checkout;
                                                    $availableCheck = App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $trips->prd_id);

                                                    if($trips->booking_status == 'Pending' && $trips->approval == 'Pending'){
                                                         $userAllreadyAccpeted = App\RentalsEnquiry::userAllreadyAccpetedForUnpaid($newDateStart, $newDateEnd, $trips->user_id); 
                                                        if($userAllreadyAccpeted){
                                                            //this is for overlaping dates_ as pending will change be ause student will not be acceptable there anyway as already taken eslewhere for ovelpapping dates, agree_ yes
                                                             echo "You have already been accepted somewhere else.";
                                                         }else{
                                                            if(count($availableCheck) > 0){
                                                                //this is for overlaping dates
                                                                echo "Already taken by another student.";
                                                            }else{
                                                                echo $trips->approval;
                                                            }
                                                        }
                                                        
                                                    }else{
                                                        if ($trips->approval == "Accept") {
                                                            echo "Accepted <br> ";
                                                            //echo "Student Guest already taken."; is this for trips page specifically_ yes . on overlapping dates correct_ so considerign what listing for this user_ this means when this request is not accepted and not decline but clicked in im not availble thse date ok so, agree_ ok if paid then wh
                                                        } elseif($trips->approval == "Unavailable"){
                                                            if($trips->property->property_status == 'unpaid' || $trips->property->property_status == 'unpaidflash'){
                                                                echo "Work Tutor not available these dates.";
                                                            }
                                                            if($trips->property->property_status == 'paid'){
                                                                 echo "Host not available these dates.";
                                                            }
                                                              
                                                        }else{
                                                            echo $trips->approval;
                                                        }               
                                                    }
                                                 ?>
                                            </b></p>
                                            </td>
                                            <td>
                                                <?php $ssid = $trips->renter_id; ?>
                                                <a target="_blank" href='{{url("user/conversation/$trips->id")}}'>Send Message</a> | 
                                                <a target="_blank" href='{{url("viewuser/$ssid")}}'>Host Profile</a> 
                                            </td>
                                            <td align="center">
                                                @if($trips->approval == "Accept")
                                                @if(isset($trips->single_review->id))
                                                Already Reviewed
                                                @else
                                                <a href="{{URL::to('user/addreview/'.base64_encode($trips->id))}}">Add Review</a>
                                                @endif
                                                @endif 
                                            </td>
                                        </tr>
                                        <?php $i++;
                                    }
                                    ?>

                                </tbody></table></div>                          

                    </div> <!--row end -->

                </div>

            </div> <!--Panel end -->





        </div>


    </div>
</div>@stop