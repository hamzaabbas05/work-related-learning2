@extends('layouts.frontdefault')
@section('header_styles')
<link href="{{ asset('frontendassets/css/jslider.css')}}" rel="stylesheet">
@stop
@section('header_js')
<script src="{{ asset('frontendassets/js/jquery.slider.bundle.js')}}"></script>
<script src="{{ asset('frontendassets/js/jquery.slider.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{Asset('fancybox/jquery.fancybox.min.css')}}">
@stop
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('settings', 'App\Settings')
@inject('propertySettings', 'App\PropertySettings')
<?php
$settingsObj = $settings->settingsInfo();
$profile_noimage = $settingsObj->no_image_profile;
?>
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
//$propertyObj = isset($userObj->user_paid_property) ? $userObj->user_paid_property : "";
$property_id = isset($propertyObj->id) ? $propertyObj->id : "";

// $forbiddenCheckIn = "";
// $forbiddenCheckOut = "";
//echo 'in test for new view working';die();

?>

<div class="navbar-fixed-top header_top_invidual">
    <div class="container">

        <div class="row">
            <div class="padding-top10 col-xs-12 col-sm-8">
                <ul class="list-inline">
                    <li><a href="#about" class="text_white rm_text_deco1 text_focus_color highlightcls" id="aboutcls"> About this listing</a></li>
                    <li><a href="#photo" class="text_white rm_text_deco1 text_focus_color highlightcls" id="photocls">Pictures</a></li>
                    <!--li><a href="#review" class="text_white rm_text_deco1"> Reviews</a></li-->
                    <li><a href="#host" class="text_white rm_text_deco1 text_focus_color highlightcls" id="hostcls">The Host</a></li>
                    <li><a href="#location" class="text_white rm_text_deco1 text_focus_color highlightcls" id="locationcls">Location</a></li>
                </ul>
            </div><!--col-sm-8 end-->

            <div class="col-xs-12 col-sm-4 padding-right0">

                <div class="bg_black header_adj">

                    <div class="row">

                        <div class="margin_topr10 col-xs-12 col-sm-12">
                            <h3 class="margin_bottom0">{{isset($propertyObj->exptitle)?$propertyObj->exptitle:""}}</h3>
                        </div><!--col-sm-8 end--> 
                        <button type="button" class="close mobileviewreq" onclick="closereqpopup();" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    </div><!--row end-->


                </div><!--header_adj end-->

            </div><!--col-sm-4 end-->

        </div><!--row end-->

    </div><!--container end-->
</div><!--row end-->

<?php
$imgURL = "";

if (isset($propertyObj->propertyimages) && sizeof($propertyObj->propertyimages) > 0) {
    foreach ($propertyObj->propertyimages as $img) {
        $imgURL = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
    }
} else {
    $imgURL = asset('images/no_prop_image.png');
}

?>

<div class="banner2" style='background-image:url(<?php echo $imgURL; ?>);'></div>  
<?php
$imgName = asset('images/noimage/' . $profile_noimage);
if (isset($propertyObj->user->profile_img_name) && $propertyObj->user->profile_img_name != "" && !is_null($propertyObj->user->profile_img_name)) {
    $imgName = asset('images/profile/' . $propertyObj->user->profile_img_name);
}

?>

<div class="bg_white border1 ">

    <div class="container tpp">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-2 text-center">
                <div class="profile margin_top30 center-block" style="background-image:url({{$imgName}});"> </div> 
                <!--<a href="" class="text_gray1 margin_top20 center-block">-->
                <?php
                $ptitle = "";
                if (isset($propertyObj->title) && !empty($propertyObj->title)) {
                    $ptitle = $propertyObj->title;
                } elseif (isset($propertyObj->user->orgname)) {
                    $ptitle = $propertyObj->user->orgname;
                }
                ?>

                <!--</a>-->
            </div><!--col-sm-2 end-->
            <input type="hidden" id="listingid" value="{{$property_id}}">
            <div class="col-xs-12 col-sm-5 col-md-6 margin_top30 margin_bottom20 listtxtalgn">
                <h3 class="font-size22">
                    <b>{{isset($propertyObj->exptitle)?$propertyObj->exptitle:''}}</b>
                </h3>
            </div><!--col-sm-6 end-->

            <div class="col-xs-12 col-sm-4 col-md-4 pos_rel wid">

                <div class="pos_abs make_fix">

                    <div class="border1  bg_white">

                        <div class="bg_black padding10 margin_adj clearfix ">

                            <div class="col-xs-12 col-sm-12">
                                <h3 class="margin_bottom0">
                                    {{isset($propertyObj->exptitle)?$propertyObj->exptitle:''}} 
                                    @if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid")
                                    <!--                                    <br />
                                                                        <span>{{isset($propertyObj->pricing->amount)?"EUR ".$propertyObj->pricing->amount:'0'}}</span>-->
                                    @endif
                                </h3>
                            </div><!--col-sm-8 end--> 
                            <button type="button" class="close mobileviewreq" onclick="closereqpopup();" aria-label="Close"><span aria-hidden="true">&times;</span></button>


                        </div><!--row end-->

                        <?php
                        $checkinsearch = "";
                        $checkoutsearch = "";
                        if (\Session::has('checkin')) {
                            $checkinsearch = \Session::get('checkin');
                            //\Session::forget('checkin');
                        }
                        if (\Session::has('checkout')) {
                            $checkoutsearch = \Session::get('checkout');
                            //\Session::forget('checkout');
                        }
                        ?>
                        {!! Form::open(array('url' => 'rentals/searchBeds', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                        <div class="table1 margin_top10 margin_left5 margin_bottom10 padd_5_10 checkinalgn caltxt">

                            <div class="checkindivcls" style="width:50%">
                               <label class="desktop_lable">Start Date</label>

                                <label class="mobile_lable">In </label>
                                <input type="text" id="startdate" name="checkinDate" value="{{$checkinsearch}}" readonly class="font-size12 form-control margin10 cal" placeholder="DD-MM-YYYY">
                            </div>
                            <div class="checkindivcls" style="width:50%">
                                <label class="desktop_lable">End Date </label>
                                <label class="mobile_lable">Out</label>
                                <input type="text" id="enddate" name="checkoutDate" value="{{$checkoutsearch}}" readonly class="font-size12 form-control margin10 cal" placeholder="DD-MM-YYYY">
                            </div>

                        </div><!--table1 end-->
                        <div class="padding10 margin_left5 margin_right5 margin_bottom10 margin_top10">
                            <button type="submit" id="requestbtn" class="btn-pad btn btn-danger full_width"><b>Refresh Dates </b></button>
                        </div>
                        {!! Form::close() !!}
                        <?php $class = "hiddencls"; ?>
                        <?php
                        if ($checkinsearch != "" && $checkoutsearch != "") {
                            $class = "";
                        }
                        ?>


                        <div id="pricediv" class="{{$class}}">
                            <input type="hidden" id="commissionprice">

                            <div class="errcls centertxt" id="maxstayerr"></div>

                            <div class="padding10 margin_left5 margin_right5 margin_bottom10">

                            </div>



                            <div class="payment-form hiddencls"></div>
                        </div><!--border1 end-->
                        <!--bg_white end-->

                    </div><!--pos_end-->

                </div><!--col-sm-4 end-->



            </div><!--row end-->

        </div><!--container end-->
    </div><!--bg_white end-->
    <div class="bg_gray1">
        <div class="container">


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <span id="about" class="scrollhighlight"> </span>
                    <h3 class="margin_top30 "><b>About Service</b></h3>
                    <div id="jRatedetail"></div>


                    <div class="row">
                        <style type="text/css">
                            .menumargin{
                                margin-right:4px;
                            }
                            /*                            .col-sm-4 p {
                                                            font-weight: 600;
                                                            color: #fe5771;
                                                        }*/
                        </style>

                        <div class="col-xs-12 col-sm-12 margin_top20">
                            <div class="btn-group" id="school_house_nav">
                                <a id="basicmenu" href="#school_house_info" class="btn btn-primary menumargin">School House</a>
                                <a id="propertymenu" href="#school_house_property_info" style="background-color: #4d4d4d !important;" class="btn btn-primary menumargin">Property</a>
                                <a id="familyphotomenu" href="#school_house_family_info" style="background-color: #4d4d4d !important;" class="btn btn-primary menumargin">Family</a>
                                <a id="extramenu" href="#school_house_extra_info" style="background-color: #4d4d4d !important;" class="btn btn-primary menumargin">Extra</a>
                                <a id="included_services" href="#school_house_included_services" style="background-color: #4d4d4d !important;" class="btn btn-primary menumargin">Included Services</a>
                                <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){ 
                                    // echo "Count :".$isOtherPropertyAvailable."<br>";
                                    if($isOtherPropertyAvailable > 0){
                                        $bgColor = '#4d4d4d';
                                        $colorCss = '';
                                        $hrefLink = url('rentals/optionalpaidservicelist/'.$userObj->id);
                                    }else{
                                        $bgColor = '#dce0e0';
                                        $hrefLink = '#';
                                        $colorCss = 'color:#333 !important;';
                                    }

        //             $propertyObj = \App\Property::with(array("propertytype"=>function($query){
        //     $query->where('listing_type','paid');

        //     $query->where('status',1);
        // },"propertyimages"))->where('user_id',$userObj->id)->whereIn('property_type',array('24','25','27','28','29','30'))->get();
        //             echo "<pre>";
        //                 print_r($propertyObj);
        //             echo "</pre>";
                                    ?>
                                <a id="included_services" href="<?php echo $hrefLink;?>" style="background-color: <?php echo $bgColor;?> !important; <?php echo $colorCss;?>" class="btn btn-primary menumargin">View Other Paid Services</a>
                                <?php } ?>
                            </div>
                        </div>
                        <div id="school_house_info" class="col-md-12">
                            <h3 class="margin_top30 "><b>School House</b></h3>
                            <div class="col-md-12" style="margin-left: 10px;">
                                <div class="margin_top10">
                                    <a><h3>School House - Listing Information</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4 ">
                                        <p class="margin_top20 headtxt text_gray1">School House Name</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{ isset($userObj->paid_service->house_name)?$userObj->paid_service->house_name:''}}
                                        </p>

                                        <!--                    To be added the functionality / just show property images for now okay let me check it.. actually ife
                                        If from user we get properties, that gives >1 results..
                                        property pic is among user details in paidschool/house/ not from property table..
                                        but I get your point
                                        we could use script as it is, and in property have School house images
                                        we would need to create completely different code for managinf user listings
                                        which is again fine with me
                                        okay if you like we can use no image img as welland
                                        the same as for ohter listings, do take from paroperty table, we will call them school house images
                                        but what if we get many property records for a user, should we use first propertylessand its image?
                                        We will for now allow only one listing paid per user andcall it School House, so to gain time for development's
                                        okay let me get 1 record for user from id from link
                                        there should be one for each real user /not the testing users(
                                        for exa mple for userid : 27 there is properti, so we will get first property and will get the image from tehre.
                                        for now..
                                        -->
                                    </div>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">School House Slogan</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{ isset($userObj->paid_service->house_slogan)?$userObj->paid_service->house_slogan:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div><!--row end-->
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">School House Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            @if(isset($userObj->paid_service->house_desc) && !empty($userObj->paid_service->house_desc))
                                            {{ isset($userObj->paid_service->house_desc)?$userObj->paid_service->house_desc:''}}
                                            @else
                                            {{ isset($userObj->user_paid_property->exptitle)?$userObj->user_paid_property->exptitle:''}}
                                            @endif
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div><!--row end-->

                                <div class="margin_top10">
                                    <a><h3>Hosting and Hospitality</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Description as a Host</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{ isset($userObj->paid_service->house_host_desc)?$userObj->paid_service->house_host_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div><!--row end-->
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Number of years hosting experience</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{ isset($userObj->paid_service->host_experience)?$userObj->paid_service->host_experience:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div><!--row end-->
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Will Host</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">

                                            {{isset($userObj->paid_service->host_preference)?str_replace(",", ", ", $userObj->paid_service->host_preference):''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div><!--row end-->
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>Teaching and Conversation skills</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Teacher has following skills</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->teaching_skills)?str_replace(",", ", ", $userObj->paid_service->teaching_skills):''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                            </div>
                        </div>

                        <div id="school_house_property_info" class="col-md-12">
                            <h3 class="margin_top30 "><b>Property</b></h3>
                            <div class="col-md-12" style="margin-left: 10px;">
                                <div class="margin_top10">
                                    <a><h3>Property Images</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-12">
                                        <?php
                                        if (isset($userObj->paidServicePropertyImage) && sizeof($userObj->paidServicePropertyImage) > 0) {
                                            foreach ($userObj->paidServicePropertyImage as $obj) {
                                                $imgname = $obj->img_name;
                                                $imgURL = asset('images/schoolhouse/' . $obj->user_id . '/' . $imgname);
                                                ?> 
                                                <div class="col-md-3">
                                                    <a href="{{$imgURL}}" data-fancybox="images">
                                                        <img style="max-height:100px" src="{{$imgURL}}" />
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                        }


                                        ?>
                                    </div><!--col-sm-8 end-->
                                </div>
                                
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>Basic Information</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Home Type</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->home_type)?$userObj->paid_service->home_type:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->property_desc)?$userObj->paid_service->property_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>Facilities Included</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Facilities</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->facilities)?str_replace(",", ", ", $userObj->paid_service->facilities):''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->facility_desc)?$userObj->paid_service->facility_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>House Rules</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Smoking</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            @if(isset($userObj->paid_service->smoking) && $userObj->paid_service->smoking == "1")
                                            Yes
                                            @else
                                            No
                                            @endif
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>

                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Time Limits (Curfew, meal and other time limits)</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            @if(isset($userObj->paid_service->time_limits) && !empty($userObj->paid_service->time_limits))
                                            {{isset($userObj->paid_service->time_limits)?$userObj->paid_service->time_limits:''}}
                                            @else
                                        <p>No Limits have been specified</p>
                                        @endif
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <?php

                                    // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                                    //     echo " User Property Obj <pre>";
                                    //         print_r($userObj);
                                    //     echo "</pre>";
                                    //     exit;
                                    // }
                                    //@if(isset($userObj->user_guest_rooms) && sizeof($userObj->user_guest_rooms) > 0)
                                ?> 
                                @if(isset($userObj->user_guest_rooms) && sizeof($userObj->user_guest_rooms) > 0)
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>Guest Rooms <?php if($_SERVER['REMOTE_ADDR'] == '79.106.209.91'){ echo "testing";} ?></h3></a>
                                </div>   
                                <?php
                                    // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                                        // echo " User Property Obj <pre>";
                                        //     print_r($userObj);
                                        // echo "</pre>";
                                        // exit;
                                    // }

                                ?> 
                                @foreach($userObj->user_guest_rooms as $gr_key => $guest_room)
                                <div style="border: 1px solid #dce0e0; padding: 10px; margin-top: 10px;">
                                    <div class="row" id="description">
                                        <div class="col-xs-12 col-sm-4">
                                            <p class="margin_top20 headtxt text_gray1">Room Name</p>
                                        </div><!--col-sm-4 end-->
                                        <div class="col-xs-12 col-sm-8">
                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                {{isset($guest_room->room_name)?$guest_room->room_name:''}}
                                            </p>
                                        </div><!--col-sm-8 end-->
                                    </div>

                                    <div class="row" id="description">
                                        <div class="col-xs-12 col-sm-4">
                                            <p class="margin_top20 headtxt text_gray1">Room Type</p>
                                        </div><!--col-sm-4 end-->
                                        <div class="col-xs-12 col-sm-8">
                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                {{isset($guest_room->realRoomType->room_type)?$guest_room->realRoomType->room_type:''}}
                                            </p>
                                        </div><!--col-sm-8 end-->
                                    </div>
                                    <div class="row" id="description">
                                        <div class="col-xs-12 col-sm-4">
                                            <p class="margin_top20 headtxt text_gray1">Room Description</p>
                                        </div><!--col-sm-4 end-->
                                        <div class="col-xs-12 col-sm-8">
                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                {{isset($guest_room->room_description)?$guest_room->room_description:''}}
                                            </p>
                                        </div><!--col-sm-8 end-->
                                    </div>

                                    <div class="row" id="description">
                                        <div class="col-xs-12 col-sm-4">
                                            <p class="margin_top20 headtxt text_gray1">Room Facilities</p>
                                        </div><!--col-sm-4 end-->
                                        <div class="col-xs-12 col-sm-8">
                                            
                                            <p class="margin_top20" style="word-wrap:break-word;">

                                                <?php
                                                
                                                $facilities = "";
                                                if (isset($guest_room->room_facility) && !empty($guest_room->room_facility)) {
                                                

                                                    $facilities = App\UserPaidService::getRoomFacilityNames($guest_room->room_facility);
                                                }
                                                
                                                // if (sizeof($facilities) > 0) {
                                                if (!empty($facilities)) {
                                                    foreach ($facilities as $facility) {
                                                        echo $facility->facility_type . ", ";
                                                    }
                                                }

                                                ?>
                                            </p>
                                        </div><!--col-sm-8 end-->

                                    </div>
                                    
                                    <div class="row" id="description">
                                        <div class="col-xs-12 col-sm-4">
                                            <p class="margin_top20 headtxt text_gray1">Student Guest(s) Toilet</p>
                                        </div><!--col-sm-4 end-->
                                        <div class="col-xs-12 col-sm-8">
                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                {{isset($guest_room->room_guest_toilet)?$guest_room->room_guest_toilet:''}}
                                            </p>
                                        </div><!--col-sm-8 end-->
                                    </div>
                                    <div class="row" id="description">
                                        <div class="col-xs-12 col-sm-4">
                                            <p class="margin_top20 headtxt text_gray1">Student Guest(s) Shower/Bathroom(s)</p>
                                        </div><!--col-sm-4 end-->
                                        <div class="col-xs-12 col-sm-8">
                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                {{isset($guest_room->room_guest_shower)?$guest_room->room_guest_shower:''}}
                                            </p>
                                        </div><!--col-sm-8 end-->
                                    </div>

                                    <?php
                                    $room_beds = \App\Property::where(["user_id" => $guest_room->user_id, "bed_guest_room_id" => $guest_room->id])
                                                    ->where(["property_type" => "26", "status" => "1", "property_status" => "paid"])->get();
                                    // if($_SERVER['REMOTE_ADDR'] == '93.32.247.99'){
                                    //     echo "Gues Room Id : ".$guest_room->id."<br>";
                                    //     echo"Count Room Beds :".count($room_beds);exit;
                                    // }

                                    // if($_SERVER['REMOTE_ADDR'] == '192.168.43.46'){
                                    //     echo "User ID : ".$guest_room->user_id."<br> Bed Guest Room Id : ".$guest_room->id."<br>";
                                    //     echo "<pre>";
                                    //         print_r($room_beds);
                                    //     echo "</pre>";
                                    // }
                                    
                                    $disabledDates = array();

                                    if (sizeof($room_beds) > 0) {
                                        ?>
                                        <div style="margin-left: 10px;">
                                            <div class="margin_top10">
                                                <a><h3>Beds available in this room</h3></a>
                                            </div>
                                            <?php
 
$disabledDates= array();
 if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){

// $data = \DB::table('bookings')
//     // ->select(* )
//     ->where('bookings.PropId', '=', $propertyObj->id)
//     ->whereNotExists( function ($query) use ($propertyObj) {
//     // ->whereNotExists( function ($query) {
//         // echo "<br> Property Obj<pre>";
//         //     print_r($propertyObj);
//         // echo "</pre><br>";
//         $query->select(DB::raw(1))
//         ->from('fc_rentalsenquiry')
//         ->whereRaw('bookings.PropId = fc_rentalsenquiry.prd_id ')
//        ->where('fc_rentalsenquiry.prd_id', '=', $propertyObj->id);
//        // echo "<br> Property Obj<pre>";
//        //      print_r($query);
//        //  echo "</pre><br>";

//     })->orderBy('id','DESC')->get();  
    // echo "<pre>";
    //     print_r($data);
    // echo "</pre>";
// if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){echo "tesing <pre>";exit;}
// echo "<br> Prop Id :".$propertyObj->id."<br>";
    $fetchDisabledDates = App\Booking::disabledDatesForProperty($propertyObj->id);
    /*if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                                        echo "User ID : ".$guest_room->user_id."<br> Bed Guest Room Id : ".$guest_room->id."<br>";
                                        echo "<pre>";
                                        echo "Size of :".sizeof($room_beds)."<br>";
                                        echo "Count :".sizeof($fetchDisabledDates);
                                            // print_r($fetchDisabledDates);
                                        echo "</pre>";
                                        // exit;
                                    }else{*/

    $disabledDates= array();
    if(sizeof($fetchDisabledDates) > 0){
        foreach($fetchDisabledDates as $dDate){
            array_push($disabledDates,$dDate->the_date);
        }
    }
                                    //}

    // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){echo "test ";exit;   }
    // echo "Disabled Date<pre>";
    //     print_r($disabledDates);
    //     print_r($fetchDisabledDates);
    // echo "</pre>";

    }


    if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                                        // echo "User ID : ".$guest_room->user_id."<br> Bed Guest Room Id : ".$guest_room->id."<br>";
                                        // echo "<pre>";
                                        // echo "Size of :".sizeof($room_beds)."<br>";
                                        // // echo "Count :".var_dump($fetchDisabledDates);
                                        //     print_r($room_beds);
                                        // echo "</pre>";
                                        // exit;
                                    }//else{

// if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){echo "test <pre>";var_dump($disabledDates);exit;}
                                            foreach ($room_beds as $bed) {
//                                                $bed_rental_booked = isset($bed->getBookedProperty) ? $bed->getBookedProperty : "";
                                                //echo $bed->id . "---" . $checkinsearch . "----" . $checkoutsearch;
                                                $bed_rental_booked = App\Property::getBookedPropertyByDates($bed->id, $checkinsearch, $checkoutsearch);
                                                
                                                $calendarObj = App\Booking::bookingForProperty($bed->id);
                                                if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
                                                    // echo "<br> Count Booked :".count($bed_rental_booked)."<br>";
                                                    // echo "<br> Checkin search :".$checkinsearch."<br> checkout Search:".$checkoutsearch."<br>";
                                                    // $propertyObj

                                                    // echo "<br> Bed Id :".$bed->id."<br>Calendar Obj Data<pre>";
                                                    // print_r($data);
                                                    // print_r($bed_rental_booked);
                                                    // echo "</pre>";
                                                }
                                                list($disable_checkinDates, $disable_CheckOutDates) = App\Property::prepareRestrictedDays($calendarObj);
                                                if (!empty($disable_checkinDates) && !empty($disable_CheckOutDates)) {
                                                   $forbiddenCheckIn1 = $forbiddenCheckIn . $disable_checkinDates; //here values are in this var. but not in rentaldetail view page seen? yes, so for unpaid it has been deleted_ dont hknow because I am checking the script and logic in rental but couldn't found . So we have it here but not in unpaid, let-s add in unpaid again, then, correct_ yes okay I'll add it.good
                                                   $forbiddenCheckOut1 = $forbiddenCheckOut . $disable_CheckOutDates;
                                                }
                                                //print_r($forbiddenCheckIn);
//                                                echo "<br />";
//                                                echo $bed->id;
//                                                echo "<br />";
//                                                print_r($forbiddenCheckOut);
                               
                                                ?>
                                                <!--<div class="border_bottom margin_top10"></div>-->
                                                <?php /*<div style="border: 1px solid #dce0e0; padding: 10px; margin-top: 10px; @if(isset($bed_rental_booked->id) && count($bed_rental_booked) > 0) background-color: #dce0e0; @endif"> */
                                                if($_SERVER['REMOTE_ADDR'] == '93.36.86.74'){ 
                                                    // echo 'Booked Dat a<pre>';
                                                    //     print_r($bed_rental_booked);
                                                    // echo '</pre>';
                                                }
                                                // <div style="border: 1px solid #dce0e0; padding: 10px; margin-top: 10px; @if(isset($bed_rental_booked->id) && $bed_rental_booked->id != '') background-color: #dce0e0; @endif">
                                                ?>
                                                <div style="border: 1px solid #dce0e0; padding: 10px; margin-top: 10px; @if(isset($bed_rental_booked->id)) background-color: #dce0e0; @endif">
                                                    <div class="row" id="description">
                                                        <div class="col-xs-12 col-sm-4">
                                                            <p class="margin_top20 headtxt text_gray1">Title</p>
                                                        </div><!--col-sm-4 end-->
                                                        <div class="col-xs-12 col-sm-8">
                                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                                {{isset($bed->exptitle)?$bed->exptitle:''}}
                                                            </p>
                                                        </div><!--col-sm-8 end-->
                                                    </div>
                                                    <div class="row" id="description">
                                                        <div class="col-xs-12 col-sm-4">
                                                            <p class="margin_top20 headtxt text_gray1">Bed Description</p>
                                                        </div><!--col-sm-4 end-->
                                                        <div class="col-xs-12 col-sm-8">
                                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                                {{isset($bed->description)?$bed->description:''}}
                                                            </p>
                                                        </div><!--col-sm-8 end-->
                                                    </div>

                                                    @if(isset($bed->optional_description) && !empty($bed->optional_description))
                                                    <div class="row" id="description">
                                                        <div class="col-xs-12 col-sm-4">
                                                            <p class="margin_top20 headtxt text_gray1">Bed Optional Description</p>
                                                        </div><!--col-sm-4 end-->
                                                        <div class="col-xs-12 col-sm-8">
                                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                                {{isset($bed->optional_description)?$bed->optional_description:''}}
                                                            </p>
                                                        </div><!--col-sm-8 end-->
                                                    </div>
                                                    @endif
                                                    <div class="row" id="description">
                                                        <div class="col-xs-12 col-sm-4">
                                                            <p class="margin_top20 headtxt text_gray1">Bed Type</p>
                                                        </div><!--col-sm-4 end-->
                                                        <div class="col-xs-12 col-sm-8">
                                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                                {{isset($bed->bedtype->bed_type)?$bed->bedtype->bed_type:''}}
                                                            </p>
                                                        </div><!--col-sm-8 end. This is American spelling so Images ? Good idea-->
                                                    </div>
                                                    <div class="row" id="description">
                                                        <!--<div class="col-xs-12 col-sm-4">
                                                            <p class="margin_top20 headtxt text_gray1">Pictures</p>
                                                        </div>col-sm-4 end, no heading_ heading of ?-->
                                                        <div class="col-xs-12 col-sm-8">
                                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                                <?php
                                                                 //if($_SERVER['REMOTE_ADDR'] == '93.33.23.89'){echo "tesing1 Size Of beds 321 :<pre> ";print_r($bed->propertyimages[0]);exit;}
                                                                // if (isset($bed->propertyimages) && $bed->propertyimages[0]->idsizeof($bed->propertyimages) > 0) {
                                                                if (isset($bed->propertyimages[0]->id) && $bed->propertyimages[0]->id !='') {
                                                                    foreach ($bed->propertyimages as $img) {
                                                                        ?>
                                                                        <a href="{{Asset('images/rentals/'.$img->property_id.'/'.$img->img_name)}}" data-fancybox="images">
                                                                            <span style="margin-left: 10px;">
                                                                                <img src="{{asset('images/rentals/'.$img->property_id.'/'.$img->img_name)}}" style="width:70px;height:70px;" alt="{{isset($bed->exptitle)?$bed->exptitle:''}}"/>
                                                                            </span>
                                                                        </a>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </p>
                                                        </div><!--col-sm-8 end-->
                                                    </div>
                                                    <?php
                                                   // if($_SERVER['REMOTE_ADDR'] == '93.33.23.89'){echo "tesing1 Size Of beds 321 :<pre> ";print_r($bed_rental_booked);exit;}

                                                    // @if(isset($bed_rental_booked->id) && sizeof($bed_rental_booked) > 0)
                                                    ?>
                                                    @if(isset($bed_rental_booked->id) && $bed_rental_booked->id != '')
                                                    <div style="margin-top: 5%;">
                                                        <div class="margin_top10">
                                                            <a><h3 style="text-align: center;">This Beds is Booked By</h3></a>
                                                        </div>
                                                        <?php
                                                        if (isset($bed_rental_booked->user->sname16) && !empty($bed_rental_booked->user->sname16)) {
                                                            $dob = isset($bed_rental_booked->user->dob16) ? $bed_rental_booked->user->dob16 : "";
                                                            $gender = isset($bed_rental_booked->user->gender16) ? $bed_rental_booked->user->gender16 : "";
                                                            $gender16 = true;
                                                            //sname16
                                                            $name = isset($bed_rental_booked->user->sname16) ? $bed_rental_booked->user->sname16 : "";
                                                            // about_yorself is correctly "Student Description"
                                                        } else {
                                                            $dob = isset($bed_rental_booked->user->dob) ? $bed_rental_booked->user->dob : "";
                                                            $gender = isset($bed_rental_booked->user->gender) ? $bed_rental_booked->user->gender : "";
                                                            $gender16 = false;
                                                            //name
                                                            $name = isset($bed_rental_booked->user->name) ? $bed_rental_booked->user->name : "";
                                                        }
                                                        ?>
                                                        <div style="margin-left: 10px;">
                                                            <div class="row" id="description">
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <p class="margin_top20 headtxt text_gray1">Name</p>
                                                                </div><!--col-sm-4 end-->
                                                                <div class="col-xs-12 col-sm-8">
                                                                    <p class="margin_top20" style="word-wrap:break-word;"> 
                                                                        {{isset($name)?$name:''}} 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="description">
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <p class="margin_top20 headtxt text_gray1">Gender</p>
                                                                </div><!--col-sm-4 end-->
                                                                <div class="col-xs-12 col-sm-8">
                                                                    <p class="margin_top20" style="word-wrap:break-word;">
                                                                        @if($gender16 == false)
                                                                        @if($gender == "1")
                                                                        Male
                                                                        @elseif($gender == "2")
                                                                        Female
                                                                        @elseif($gender == "3")
                                                                        Other
                                                                        @endif
                                                                        @else
                                                                        {{$gender}}
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="description">
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <p class="margin_top20 headtxt text_gray1">Age</p>
                                                                </div><!--col-sm-4 end-->
                                                                <div class="col-xs-12 col-sm-8">
                                                                    <p class="margin_top20" style="word-wrap:break-word;">
                                                                        <?php
                                                                        //date in mm/dd/yyyy format; or it can be in other formats as well
                                                                        //$dob = isset($bed_rental_booked->user->dob) ? $bed_rental_booked->user->dob : "";
                                                                        $new_dob = "";
                                                                        if (isset($dob) && !empty($dob)) {
                                                                            $exp_dob = explode("/", $dob);
                                                                            if (is_array($exp_dob)) {
                                                                                $new_dob = $exp_dob[2] . "-" . $exp_dob[1] . "-" . $exp_dob[0];
                                                                            }
                                                                        }
                                                                        if (!empty($new_dob)) {
                                                                            //echo date("Y-m-d", strtotime($new_dob)) . "<br />";
                                                                            if (isset($checkinsearch) && !empty($checkinsearch)) {
                                                                                $booked_checkin_date = date("Y-m-d", strtotime($checkinsearch));
                                                                            } else {
                                                                                $booked_checkin_date = isset($bed_rental_booked->checkin) ? date("Y-m-d", strtotime($bed_rental_booked->checkin)) : "";
                                                                            }
                                                                            //echo $booked_checkin_date."<br />";
                                                                            $birthdate = new DateTime($new_dob);
                                                                            $today = new DateTime($booked_checkin_date);
                                                                            $age = $birthdate->diff($today)->y;
                                                                            echo $age." Years";
//                                                                            $birthDate = isset($dob) ? $dob : "";
//                                                                            $birthDate = explode("/", $birthDate);
//                                                                            //get age from date or birthdate / subtract chack in date
//                                                                            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[0], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
                                                                            //echo "<br />original age " . $age;
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="description">
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <p class="margin_top20 headtxt text_gray1">Language</p>
                                                                </div><!--col-sm-4 end-->
                                                                <div class="col-xs-12 col-sm-8">
                                                                    <p class="margin_top20" style="word-wrap:break-word;">
                                                                        <?php
                                                                        $language = "";
                                                                        if (isset($bed_rental_booked->user->user_extrainfo->native_language_speaker) && !empty($bed_rental_booked->user->user_extrainfo->native_language_speaker)) {
                                                                            $language = App\Language::where("id", $bed_rental_booked->user->user_extrainfo->native_language_speaker)->first();
                                                                        }
                                                                        ?>
                                                                        {{isset($language->language_name)?$language->language_name:''}}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <div class="row {{$class}} 321" id="description">
                                                    <?php if($_SERVER['REMOTE_ADDR'] == '79.106.209.91'){
                                                            $display_request = "display: block;";
                                                        }else{ 
                                                        //  @if(isset($bed_rental_booked->id) && sizeof($bed_rental_booked) > 0)
                                                        // @if(isset($bed_rental_booked->id) && $bed_rental_booked->id != '')  //comment on 14_11_2019
                                                          

                                                            if(isset($bed_rental_booked->id) && $bed_rental_booked != NULL ){
                                                                $display_request = "display: none;";
                                                            }else{
                                                                $display_request = "display: block;"; 
                                                            }
                                                             


                                                         ?>
                                                      

                                                    <?php }  ?>
                                                        <div class="col-xs-12 col-sm-12" style="{{$display_request}}">
                                                            <p class="margin_top10 headtxt text_gray1" style="text-align: center;">
                                                                @if (Auth::guest())
                                                                <a href="{{url('signin')}}" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" >Login</a>
                                                                @else
                                                                <?php if (!Auth::user()->hasRole('Student') && !Auth::user()->hasRole('Parent')) { ?>
                                                                    <!-- Comment - Compelte Profile needs to be reviewed  -->
                                                                    <a href="{{url('user/edit')}}" id="requestbtn" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" >Complete your Profile to send request</a>
                                                                    <?php
                                                                } else {
                                                                    if (isset($bed->booking_style) && $bed->booking_style == 1) {
                                                                        ?> 
                                                                        <button type="button" onclick="send_reserve_request('{{$bed->id}}');" id="requestbtn" class="btn-pad btn btn-danger full_width"><b>Send request</b></button>
                                                                    <?php } else { ?> 
                                                                        <button type="button" onclick="instant_book('{{$bed->id}}');" id="requestbtn" class="btn-pad btn btn-danger full_width"><b>Book Now</b></button>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                                @endif
                                                            <center><img id="paypalloadingimg" src="{{asset('frontendassets/images/load.gif')}}" class="loading" style="margin-top:5px;"></center>
                                                            <center><b>This may take a long time, please be patient</b></center>
                                                            <div class="errcls centertxt" id="emailverifyerr"></div>
                                                            </p>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12">
                                                            <p class="margin_top10 headtxt text_gray1" style="text-align: center;">
                                                                @if (Auth::guest())
                                                                <a href="{{url('signin')}}" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" >Login</a>
                                                                @else
                                                                <?php
                                                                $text = "Save To Wishlist";
                                                                if (isset($wishProperty) && in_array($property_id, $wishProperty)) {
                                                                    $text = "Added in Wishlist";
                                                                }
                                                                ?>
                                                                <button type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" data-toggle="modal" data-target="#myModal" onclick="show_list_popup(event,<?php echo $bed->id; ?>);">
                                                                    <i class="fa fa-heart-o"></i>{{$text}}
                                                                </button>
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } 
                                        //} ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                @endforeach
                                @endif
                            </div>
                            
                        </div>
                        <div id="school_house_family_info" class="col-md-12">
                            <h3 class="margin_top30 "><b>Family</b></h3>
                            <div class="col-md-12" style="margin-left: 10px;">
                                <div class="margin_top10">
                                    <a><h3>Family Images</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-12">
                                        <?php

                                        if (isset($userObj->paidServiceFamilyImage) && sizeof($userObj->paidServiceFamilyImage) > 0) {
                                            foreach ($userObj->paidServiceFamilyImage as $obj) {
                                                $imgname = $obj->img_name;
                                                $imgURL = asset('images/family/' . $obj->user_id . '/' . $imgname);
                                                ?> 
                                                <div class="col-md-3">
                                                    <a href="{{$imgURL}}" data-fancybox="images">
                                                        <img style="max-height:100px" src="{{$imgURL}}" />
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                        }
                                         
                                        ?>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>Family Members</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Language at Home the Country's official</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            @if(isset($userObj->paid_service->is_language) && $userObj->paid_service->is_language == "1")
                                            Yes
                                            @else
                                            No
                                            @endif
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">All Members General Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->member_desc)?$userObj->paid_service->member_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Family situation at Home</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->family_situation)?$userObj->paid_service->family_situation:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @if(isset($userObj->paidServiceFamilyRelation) && sizeof($userObj->paidServiceFamilyRelation) > 0)
                                <div class="border_bottom margin_top10"></div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Family Members/Children (staying at home)</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        @foreach($userObj->paidServiceFamilyRelation as $f_relation)
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            <label style="float: left; margin-right: 10px;">Name : </label>
                                            <span style="float: left;">{{isset($f_relation->name)?$f_relation->name:''}}</span>
                                        </p>
                                        <p style="clear: both;"></p>
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            <label style="float: left; margin-right: 10px;">Birth Year : </label>
                                            <span style="float: left;">{{isset($f_relation->birth_year)?$f_relation->birth_year:''}}</span>
                                        </p>
                                        <p style="clear: both;"></p>
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            <label style="float: left; margin-right: 10px;">Relation : </label>
                                            <span style="float: left;">{{isset($f_relation->relationship)?$f_relation->relationship:''}}</span>
                                        </p>
                                        <p style="clear: both;"></p>
                                        <hr style="border: 1px solid #ddd;"/>
                                        @endforeach
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="border_bottom margin_top10"></div>
                                @else
                                <div class="border_bottom margin_top10"></div>
                                @endif

                                <div class="margin_top10">
                                    <a><h3>Pets</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Pets at Home</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            @if(isset($userObj->paid_service->pet_types) && !empty($userObj->paid_service->pet_types))
                                            {{isset($userObj->paid_service->pet_types)?str_replace(",", ", ", $userObj->paid_service->pet_types):''}}
                                            @else <p>No Pets have been specified</p>
                                        @endif
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @if(isset($userObj->paid_service->pet_types) && !empty($userObj->paid_service->pet_types))
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Pets Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->pet_desc)?$userObj->paid_service->pet_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif


                            </div>
                        </div>
                        <div id="school_house_extra_info" class="col-md-12">
                            <h3 class="margin_top30 "><b>Extra Info</b></h3>
                            @if( (isset($userObj->paid_service->urban_location) && !empty($userObj->paid_service->urban_location)) 
                            || (isset($userObj->paid_service->property_region) && !empty($userObj->paid_service->property_region))
                            || (isset($userObj->paid_service->local_amenity) && !empty($userObj->paid_service->local_amenity))
                            || (isset($userObj->paid_service->amenity_desc) && !empty($userObj->paid_service->amenity_desc))
                            )
                            <div class="col-md-12" style="margin-left: 10px;">
                                <div class="margin_top10">
                                    <a><h3>Area And Amenities</h3></a>
                                </div>
                                @if(isset($userObj->paid_service->urban_location) && !empty($userObj->paid_service->urban_location))
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Urban Location</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->urban_location)?$userObj->paid_service->urban_location:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->property_region) && !empty($userObj->paid_service->property_region))
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Region</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->property_region)?$userObj->paid_service->property_region:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->local_amenity) && !empty($userObj->paid_service->local_amenity))
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Local Amenities</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->local_amenity)?str_replace(",", ", ", $userObj->paid_service->local_amenity):''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->amenity_desc) && !empty($userObj->paid_service->amenity_desc))
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Local Amenities Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->amenity_desc)?$userObj->paid_service->amenity_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                            </div>
                            @else <p>No extra information has been added</p>
                            @endif
                        </div>

                        <div id="school_house_included_services" class="col-md-12">
                            <h3 class="margin_top30 "><b>Included Services</b></h3>
                            <div class="col-md-12" style="margin-left: 10px;">
                                @if(isset($userObj->paid_service->school_house_activity) && !empty($userObj->paid_service->school_house_activity))
                                <div class="margin_top10">
                                    <a><h3>Activities</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Activities</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            <?php
                                            $activities = "";
                                            if (isset($userObj->paid_service->school_house_activity) && !empty($userObj->paid_service->school_house_activity)) {
                                                $activities = App\UserPaidService::getActivityNames($userObj->paid_service->school_house_activity);
                                            }
                                            if (sizeof($activities) > 0) {
                                                foreach ($activities as $activity) {
                                                    echo $activity->activity_type . ", ";
                                                }
                                            }
                                            ?>
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Activity Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->activity_desc)?$userObj->paid_service->activity_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->school_house_classes) && !empty($userObj->paid_service->school_house_classes))
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>Classes</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Classes</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            <?php
                                            $classes = "";
                                            if (isset($userObj->paid_service->school_house_classes) && !empty($userObj->paid_service->school_house_classes)) {
                                                $classes = App\UserPaidService::getClassesNames($userObj->paid_service->school_house_classes);
                                            }
                                            if (sizeof($classes) > 0) {
                                                foreach ($classes as $class) {
                                                    echo $class->class_type . ", ";
                                                }
                                            }
                                            ?>
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Class Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->class_desc)?$userObj->paid_service->class_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->school_house_diet) && !empty($userObj->paid_service->school_house_diet))
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>Diet</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Diet at Home</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->school_house_diet)?str_replace(",", ", ", $userObj->paid_service->school_house_diet):''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Diet Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->diet_desc)?$userObj->paid_service->diet_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->school_house_diet_board) && !empty($userObj->paid_service->school_house_diet_board))
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>Meals Provided</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Meals at Home</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->school_house_diet_board)?$userObj->paid_service->school_house_diet_board:''}}
                                        </p>
                                        <p>*Lunch may be packed lunch</p>
                                    </div><!--col-sm-8 end fine with me okay-->
                                </div>
                                @endif

                                @if(isset($userObj->paid_service->school_house_transfers) && !empty($userObj->paid_service->school_house_transfers))
                                <div class="border_bottom margin_top10"></div>

                                <div class="margin_top10">
                                    <a><h3>Transfers</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Transfers</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            <?php
                                            $transfers = "";
                                            if (isset($userObj->paid_service->school_house_transfers) && !empty($userObj->paid_service->school_house_transfers)) {
                                                $transfers = App\UserPaidService::getTransferNames($userObj->paid_service->school_house_transfers);
                                            }
                                            if (sizeof($transfers) > 0) {
                                                foreach ($transfers as $transfer) {
                                                    echo $transfer->transfer_type . ", ";
                                                }
                                            }
                                            ?>
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Transfer Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->transfer_desc)?$userObj->paid_service->transfer_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->school_house_tours) && !empty($userObj->paid_service->school_house_tours))
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>Tours</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Tours</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            <?php
                                            $tours = "";
                                            if (isset($userObj->paid_service->school_house_tours) && !empty($userObj->paid_service->school_house_tours)) {
                                                $tours = App\UserPaidService::getTourNames($userObj->paid_service->school_house_tours);
                                            }
                                            if (sizeof($tours) > 0) {
                                                foreach ($tours as $tour) {
                                                    echo $tour->tour_type . ", ";
                                                }
                                            }
                                            ?>
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Tour Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->tour_desc)?$userObj->paid_service->tour_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->school_house_wows) && !empty($userObj->paid_service->school_house_wows))
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <a><h3>WOW</h3></a>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">WOW</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            <?php
                                            $wows = "";
                                            if (isset($userObj->paid_service->school_house_wows) && !empty($userObj->paid_service->school_house_wows)) {
                                                $wows = App\UserPaidService::getWowNames($userObj->paid_service->school_house_wows);
                                            }
                                            if (sizeof($wows) > 0) {
                                                foreach ($wows as $wow) {
                                                    echo $wow->wow_type . ", ";
                                                }
                                            }
                                            ?>
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class="margin_top20 headtxt text_gray1">Wow Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="margin_top20" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->wow_desc)?$userObj->paid_service->wow_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                            </div>
                        </div>
                        <!--col-sm-4 end-->
                    </div><!--row end-->
                    <div class="border_bottom margin_top10"></div>



                    <!--row end--><span class=" scrollhighlight" id="photo">



                        <link rel="stylesheet" href="{{ asset('frontendassets/css/supersized.css')}}" type="text/css" media="screen" />
                        <link rel="stylesheet" href="{{ asset('frontendassets/css/supersized.shutter.css')}}" type="text/css" media="screen" />
                        <script type="text/javascript" src="{{ asset('frontendassets/js/jquery.easing.min.js')}}"></script>
                        <script type="text/javascript" src="{{ asset('frontendassets/js/supersized.3.2.7.min.js')}}"></script>
                        <script type="text/javascript" src="{{ asset('frontendassets/js/supersized.shutter.min.js')}}"></script>




                </div><!--col-sm-8 end-->
                <!--<div class="col-xs-12 col-sm-12 col-md-4">Right listing section</div>-->
                <div class="col-xs-12 col-sm-12 col-md-4">&nbsp;</div>
            </div><!--row end-->

        </div><!--container end-->
    </div>

    <!-- Review Start -->
    <?php if (isset($propertyObj->reviews) && $propertyObj->reviews->count() > 0) { ?> 
        <div class="bg_white margin_top30 border1">
            }
            <div class="container">
                <h3 class="margin_top20">{{$propertyObj->reviews->count()}} Reviews <span id="review"></span></h3>


                <div class="row">
                    <div class="col-xs-12 col-sm-8">

                        <?php
                        foreach ($propertyObj->reviews as $review) {


                            $rimgName = asset('images/noimage/' . $profile_noimage);
                            if ($review->reviewer->profile_img_name != "" && !is_null($review->reviewer->profile_img_name)) {
                                $rimgName = asset('images/profile/' . $review->reviewer->profile_img_name);
                            }
                            ?> 

                            <div class="row">
                                <div class="col-xs-12 col-sm-4 text-center padd_bottom30">
                                    <div class="profile margin_top20 center-block" style="background-image:url({{$rimgName}});"> </div>
                                    <a class="text_gray1 margin_top10 center-block" href="#">{{$review->reviewer->name}}</a>
                                </div> 

                                <div class="col-xs-12 col-sm-8">
                                    <p>{{$review->description}}</p>
                                    <div class="row margin_top20">
                                        <div class="col-xs-12 col-sm-6"><p class="text-muted margin_top10">{{$review->dateAdded}}</p></div>
                                         <!--<div class="col-xs-12 col-sm-6 text-right"><button type="button" class="btn btn-default"> <i class="fa fa-thumbs-o-up"></i> Helpful</button></div>-->
                                    </div> 
                                </div> 
                            </div> 
                        <?php } ?> 

                    </div> 
                </div> 


            </div> 
        </div>  <?php } else { ?>
        <!-- Review End -->

        <div class="bg_white margin_top30 border1">
            <div class="container">
                <h3 class="margin_top20 margin_bottom10"><b>No reviews yet</b></h3>
                <div class="margin_bottom20">You could give this Service its first review!</div>
            </div>
        </div>

    <?php } ?>


    <div class="container margin_top30 stopfixer">
        <h3><b>About the Member: {{isset($propertyObj->title)?$propertyObj->title:''}} <span id="host" class="scrollhighlight"></span></b></h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12  col-md-8">


                <div class="row">

                    <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="profile margin_top20 center-block" style="background-image:url({{$imgName}});"> </div>
                    </div><!--col-sm-4 end-->

                    <div class="col-xs-12 col-sm-8 col-md-8 xs-center-txt">
                        <p class="margin_top40">{{isset($propertyObj->auto_complete)?$propertyObj->auto_complete:''}}
                            <br />Member since {{isset($propertyObj->user->created_at)?date_format($propertyObj->user->created_at, 'jS F Y'):''}}</p>

                        <?php
                        $ln = "Private";
                        if (isset($propertyObj->legal_nature) && $propertyObj->legal_nature == 1) {
                            $ln = "Public";
                        }
                        ?>

                    </div>

                    <!--col-sm-4 end-->

                </div><!--row end-->



                <div class="row hiddencls">

                    <div class="col-xs-12 col-sm-4">
                        <p class="margin_top30">Trust</p>
                    </div><!--col-sm-4 end-->

                    <div class="col-xs-12 col-sm-8">

                        <button type="button" class="btn btn-warning margin_top20"> 1</button>
                        <p>Review</p>
                    </div><!--col-sm-4 end-->

                </div><!--row end-->

            </div><!--col-sm-8 end--></div><!--row end-->


        <!--iframe src="https://www.google.com/maps/@11.0136259,76.8978671,14.25z?hl=en-US"  width="100%" 
        height="450" frameborder="0" style="border:0; " class="margin_top30 map"  ></iframe-->
        <span id="location" class="scrollhighlight"></span>
        <div class="map margin_bottom30">
            <span class="map1"></span>
            <!--span id="location"></span-->
            <div id="map_canvas" frameborder="0" style="border:0; " class="margin_top30 map">



            </div>



        </div>


        <h3 class="margin_top30 hiddencls">Explore other options in and around</h3>

        <p class="margin_bottom30 hiddencls">More Services to see<br /><br />
        </p>



    </div><!--container end-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog listpopupwidth" role="document">
            <div class="modal-content">

                <div class="modal-body padding0">
                    <div class="toplistdiv" style="display:none;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>   
                        <h3>Save to Wish List</h3>          
                        <hr />
                    </div>

                    <div class="airfcfx-leftlistdiv leftlistdiv">
                        <div class="banner2 banner2hgt"  id="listimage"></div>
                    </div>
                    <div class="airfcfx-rightlistdiv-cnt">
                        <div class="airfcfx-rightlistdiv rightlistdiv padding20 wishlisthgt">

                            <div class="airfcfx-topfullviewdiv topfullviewdiv">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
                                <h3>Save to Wish List</h3><hr />
                            </div>

                            <div class="airfcfx-wishlist-contianer" id="listnames"> 

                            </div>
                        </div>
                        <div class="airfcfx-wish-createlist-cnt">
                            <input type="text" id="newlistname1" class="airfcfx-listtxt listtxt" value="" placeholder="Create New List" maxlength="20">
                            <input type="button" value="create" class="airfcfx-createbtn btn btn-danger createbtn" onclick="create_new_list();">
                        </div> 
                        <div class="airfcfx-wishlist-btn-cnt">
                            <input type="button" value="Cancel" class="airfcfx-cancelsze btn btn_email cancelsze cancelbtn " data-dismiss="modal">
                            <input type="button" value="Save" data-dismiss="modal" class="airfcfx-savebtn btn btn-primary savebtn pull-right" onclick="save_lists();">
                            <div class="errcls listerr"></div>
                        </div>
                    </div> 

                </div>
                <div class="clear">

                </div>            
            </div>
        </div>
    </div>

    <div class="modal" id="contactform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no_border">
                    <button type="button" class="close" data-dismiss="modal" onclick="checkitclear();" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
                </div>
                <div class="modal-body">

                    <div class="col-xs-12 contact-txtbox-align">
                        <h3>Contact Provider</h3>
                        <p>Write your message:</p>
                    </div>
                    <div class="col-xs-12 contact-txtbox-align margin_top20">
                        <textarea id="contactmessage" style="resize:none" cols="50" rows="5" class="form-control" maxlength="250"></textarea>
                        <div id="charNum"></div>
                    </div>
                </div>
                <input type="hidden" id="userid" value="4">
                <input type="hidden" id="hostid" value="4">
                <input type="hidden" id="listingid" value="893">
                <div class="clear"></div><br />
                <div class="modal-footer">
                    <input type="button" id="send_msg" value="Send Message" class="btn btn-danger" onclick="send_contact_message();">
                    <img id="loadingimg" src="/images/load.gif" class="loading" style="margin-top:-1px;">        <div id="succmsg" class="successtxt hiddencls">Message Sent Successfully</div><br/>
                    <div class="msgerrcls"></div>

                </div>
            </div>
            <div class="clear">
            </div>            
        </div>
    </div>  
	hell

    <script src="{{Asset('fancybox/jquery.fancybox.min.js')}}"></script>
    <script type="text/javascript">
                        function checkitclear(){
                        //$('#contactmessage').val('');
                        $('.msgerrcls').val('');
                        $('.msgerrcls').hide();
                        }

    </script>		  
    <!--script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script-->
    <?php
    $prop_latlng = $propertyObj;
    if (isset($prop_latlng->addr_work_org_same) && $prop_latlng->addr_work_org_same == 0) {
        $lat = $prop_latlng->pro_lat;
        $lng = $prop_latlng->pro_lon;
    } elseif (isset($prop_latlng->user->paid_service->house_property_differ) && $prop_latlng->user->paid_service->house_property_differ == 1) {
        $lat = isset($prop_latlng->user->paid_service->proplatbox) ? $prop_latlng->user->paid_service->proplatbox : "";
        $lng = isset($prop_latlng->user->paid_service->proplonbox) ? $prop_latlng->user->paid_service->proplonbox : "";
    } elseif (isset($prop_latlng->user->paid_service->house_property_differ) && $prop_latlng->user->paid_service->house_property_differ == 0) {
        $lat = isset($prop_latlng->user->paid_service->houselatbox) ? $prop_latlng->user->paid_service->houselatbox : "";
        $lng = isset($prop_latlng->user->paid_service->houselonbox) ? $prop_latlng->user->paid_service->houselonbox : "";
    } else {
        $lat = isset($prop_latlng->org_lat) ? $prop_latlng->org_lat : "";
        $lng = isset($prop_latlng->org_lan) ? $prop_latlng->org_lan : '';
    }
    ?>
    <script>


        /*  var mapOptions = {
         zoom: 10,
         mapTypeId: google.maps.MapTypeId.SATELLITE,
         scrollwheel: false      
         }
         map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);*/
        var map;
        var geocoder;
        var centerChangedLast;
        var reverseGeocodedLast;
        var currentReverseGeocodeResponse;
        function initialize() {
        lat = <?php echo isset($lat) ? $lat : ''; ?>;
        long = <?php echo isset($lng) ? $lng : ''; ?>;
        var latlng = new google.maps.LatLng(lat, long);
        var myOptions = {
        zoom: 10,
                center: latlng,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        var myCity = new google.maps.Circle({
        center:latlng,
                radius:7000,
                strokeColor:"#8fe3de",
                strokeOpacity:0.8,
                strokeWeight:2,
                fillColor:"#8cdfd9",
                fillOpacity:0.4
        });
        myCity.setMap(map);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
        // google.maps.event.addDomListener(window, 'load', initialize);    

        //slideer stop
        $('.carousel').carousel({
        interval: false
        })


        //Nav Fixed
        $(document).ready(function(){ 
			var a = $(".wid").outerWidth();
			$(".make_fix").css('width', a - 15);
			var project2 = $('.tpp').offset();
			var $window = $(window);
			$window.scroll(function(){
				//start fix
				if($window.scrollTop() >= project2.top){
					$(".make_fix").addClass("fixed");
					jQuery('.header_top_invidual').addClass('filter_menu1');
				}else{
					$(".make_fix").removeClass("fixed");
					jQuery('.header_top_invidual').removeClass('filter_menu1');
				}
			});
        });
        //clouse fix
        $(document).ready(function(){
			var project1 = $('.stopfixer').offset();
			var $window = $(window);
			var valueofmap = project1.top - '100';
			$window.scroll(function() {
				if($window.scrollTop() >= valueofmap){
					// alert(project1.top);
					$(".make_fix").addClass("fixed_rm");
				}else{
					$(".make_fix").removeClass("fixed_rm");
				}
			});
        });
		
        //Owl Carasel
        $(document).ready(function() {

			//var owl = $("#owl-demo");

			/*owl.owlCarousel({
			 items : 3, //10 items above 1000px browser width
			 itemsDesktop : [3,5], //5 items between 1000px and 901px
			 itemsDesktopSmall : [3,3], // betweem 900px and 601px
			 itemsTablet: [3,2], //2 items between 600 and 0
			 itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
			 });*/

			// Custom Navigation Events
			$(".next").click(function(){
				owl.trigger('owl.next');
			});
			$(".prev").click(function(){
				owl.trigger('owl.prev');
			});

        });
		
		
        //width of filter
        $(document).ready(function(){
			$(window).scroll(function(){
				$("#startdate").datepicker("hide");
				$("#enddate").datepicker("hide");
				$("#startdate").blur();
				$("#enddate").blur();
				$("#startdatemobile").datepicker("hide");
				$("#enddatemobile").datepicker("hide");
				$("#startdatemobile").blur();
				$("#enddatemobile").blur();
			});
			
			$("#startdate").keydown(function(event){
				if (event.which == 13) {
					$("#startdate").readonlyDatepicker(true);
				}
			});
			$("#enddate").keydown(function(event){
				if (event.which == 13) {
					$("#enddate").readonlyDatepicker(true);
				}
			});
			$("#startdatemobile").keydown(function(event){
				if (event.which == 13) {
					$("#startdatemobile").readonlyDatepicker(true);
				}
			});
			$("#enddatemobile").keydown(function(event){
				if (event.which == 13) {
					$("#enddatemobile").readonlyDatepicker(true);
				}
			});
        });
		
		</script>

    <?php
	 
   if (!empty($forbiddenCheckIn)) {
        $forbiddenCheckIn = str_replace('"',"",$forbiddenCheckIn);
         if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){

            // echo "CheckINd <pre>";
                // print_r($forbiddenCheckIn);
                $forbiddenCheckIn1 = trim($forbiddenCheckIn,",");
                // print_r(explode(",",$forbiddenCheckIn));
            // echo "</pre>";
         }

         $forbiddenCheckIn = '[' . (count(explode(",",$forbiddenCheckIn)) > 1 ? explode(",",$forbiddenCheckIn)[0] : $forbiddenCheckIn) . ']';
       
    if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
       // $forbiddenCheckIn = '[' . (count(explode(",",$forbiddenCheckIn)) > 1 ? implode(",", $forbiddenCheckIn) : $forbiddenCheckIn) . ']';
        // var_dump($forbiddenCheckIn1);
        // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){echo "test <pre>";var_dump($disabledDates);exit;   } 
        if(isset($disabledDates) && count($disabledDates) > 0){
            $disabledDates_checkin = implode(",", $disabledDates);
            $forbiddenCheckIn1 .= ",".$disabledDates_checkin;
        }
       $forbiddenCheckIn = '[' . (count(explode(",",$forbiddenCheckIn1)) > 1 ? $forbiddenCheckIn1 : $forbiddenCheckIn1) . ']';
       // $forbiddenCheckIn = "['2019-08-15 11:00:00','2019-08-15 11:00:00','2019-08-16 10:00:00','2019-08-16 10:00:00','2019-08-14 11:00:00','2019-08-15 10:00:00','2019-08-19 11:00:00','2019-08-20 11:00:00','2019-08-21 11:00:00','2019-08-22 10:00:00','2019-08-31 11:00:00','2019-08-31 11:00:00','2019-09-01 10:00:00','2019-09-01 10:00:00','2019-08-27 11:00:00','2019-08-27 11:00:00','2019-08-28 11:00:00','2019-08-28 11:00:00','2019-09-28 10:00:00','2019-09-29 10:00:00']";

        // echo "CheckINd <pre>";
        //         $forbiddenCheckIn = trim($forbiddenCheckIn,",");
        //         print_r(explode(",",$forbiddenCheckIn));
        //     echo "</pre>";
   }else{
    $disabledDates= array();

    $forbiddenCheckIn = '[' . (count(explode(",",$forbiddenCheckIn)) > 1 ? explode(",",$forbiddenCheckIn)[0] : $forbiddenCheckIn) . ']';
       // $forbiddenCheckIn = (count(explode(" ",$forbiddenCheckIn)) > 1 ? explode(" ",$forbiddenCheckIn)[0] : $forbiddenCheckIn);
   }

   }
   if (!empty($forbiddenCheckOut)) {
    // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){echo "test <pre>";var_dump($disabledDates);exit;}
        $forbiddenCheckOut = str_replace('"',"",$forbiddenCheckOut);
        if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
            // echo "CheckINd <pre>";
            //     print_r($forbiddenCheckOut);
                $forbiddenCheckOut1 = trim($forbiddenCheckOut,",");
            //     print_r(explode(",",$forbiddenCheckOut));
            // echo "</pre>";
             $forbiddenCheckOut = (count(explode(",",$forbiddenCheckOut1)) > 1 ? $forbiddenCheckOut1 : $forbiddenCheckOut1);
             if(count($disabledDates) > 0){
                $disabledDates_checkout = implode(",", $disabledDates);
                $forbiddenCheckOut .= ",".$disabledDates_checkout;
            }
            $forbiddenCheckOut = '[' .$forbiddenCheckOut. ']';
         }else{
            $disabledDates= array();
            $forbiddenCheckOut = '[' . (count(explode(" ", $forbiddenCheckOut)) > 1 ? explode(" ", $forbiddenCheckOut)[0] :$forbiddenCheckOut). ']';            
         }
       // $forbiddenCheckOut = (count(explode(" ", $forbiddenCheckOut)) > 1 ? explode(" ", $forbiddenCheckOut)[0] :$forbiddenCheckOut);
   }

   //  if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
   //      // echo "<bR> DB Count :".count(explode(" ","2019-08-15 11:00:00"))."<br>";
   //  echo "<pre>";
   //  echo "Checkin CUSTOM DATE :<br> ";
   //      print_r($forbiddenCheckIn);
   //      echo "<br> Checkin :";
   //      print_r($forbiddenCheckOut);
   //  echo "</pre>";
   //  // exit;
   // }
    ?>

    <script>

        var dateRange = [];
        var beforedate = [];
        var afterdate = [];
        $(function () {
        startdate = '';
        enddate = '';
        listid = <?php echo $property_id ?>;
        startdates = "";
        enddates = "";
        for (i = 0; i < startdates.length; i++)
        {
        fromdate = new Date(startdates[i] * 1000);
        fromdate.setDate(fromdate.getDate() - 1);
        beforedate.push($.datepicker.formatDate('mm/dd/yy', fromdate));
        todate = new Date(enddates[i] * 1000);
        todate.setDate(todate.getDate());
        afterdate.push($.datepicker.formatDate('mm/dd/yy', todate));
        for (var d = new Date((startdates[i] * 1000));
        d < new Date(enddates[i] * 1000);
        d.setDate(d.getDate() + 1)) {
        dateRange.push($.datepicker.formatDate('yy-mm-dd', d));
        }
        }//alert(beforedate);

        var sdate = new Date(startdate * 1000);
        var edate = new Date(enddate * 1000);
        var sedate = new Date(enddate * 1000);
        sedate = new Date(sedate.setDate(sedate.getDate() - 1));
        if (startdate == "") {
        sdate = "";
        }
        if (enddate == "") {
        edate = "";
        sedate = "";
        }
        if (sdate != "") {
        todaydate = new Date();
        if (sdate < todaydate) {
        minimumdate = new Date();
        endminimumdate = new Date();
        endminimumdate.setDate(endminimumdate.getDate() + 1);
        }
        else
        {
        minimumdate = new Date(sdate.setDate(sdate.getDate()));
        endminimumdate = new Date(sdate.setDate(sdate.getDate() + 1));
        }
        }
        else
        {
        minimumdate = new Date();
        endminimumdate = new Date();
        endminimumdate.setDate(endminimumdate.getDate() + 1);
        }
        var array1 = "<?php echo isset($forbiddenCheckIn) ? $forbiddenCheckIn : ''; ?>";
        // var array12 = <?php //var_dump($forbiddenCheckIn);?>

        <?php if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){ ?>
            console.log("Array1 : " + array1);

            // console.log("Array1 : " + array1);            
        <?php } ?>
        var array2 = "<?php echo isset($forbiddenCheckOut) ? $forbiddenCheckOut : ''; ?>";
        <?php if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){ ?>
            console.log("\n Array2 : " + array2);
            // console.log("Array1 : " + array1);            
        <?php } ?>
		console.log(disableDates);  
<?php if (isset($propertyObj->calendar_availability) && $propertyObj->calendar_availability == 1) { ?>
            $("#startdate").datepicker({
            changeMonth: true,
                    numberOfMonths: 1,
                    beforeShowDay: disableDates,
                    minDate:minimumdate,
                    maxDate:sedate,
                    beforeShowDay: function(date){
                    var string = $.datepicker.formatDate('yy-mm-dd', date);
                    var check = array1.indexOf(string) == - 1;
                    // console.log("Check : " + array1 + " String : " + string + "Date :" + date);
                    if (typeof (check) != 'undefined')
                    {
                    if (check)
                    {
                    return [true, '', ''];
                    }
                    else
                    {
                    return [false, "bookedDate", "check"];
                    }
                    }
                    },
                    onSelect: function (selectedDate) {
                    $("#requestbtn").attr("disabled", false);
                    var orginalDate = new Date(selectedDate);
                    orginalDate.setDate(orginalDate.getDate() + 1);
                    //var monthsAddedDate = new Date(new Date(orginalDate).setMonth(orginalDate.getMonth() + 3));
                    $("#enddate").datepicker("option", 'minDate', orginalDate);
                    $("#enddate").datepicker("option", 'maxDate', edate);
                    stdates = $("#startdate").val();
                    stdate = new Date(stdates);
                    eddates = $("#enddate").val();
                    eddate = new Date(eddates);
                    var seldateRange = [];
                    if (stdates != "" && eddates != "") {
                    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    selstartdate = stdate.getTime();
                    selenddate = eddate.getTime();
                    for (var dates = new Date(selstartdate);
                    dates < new Date(selenddate);
                    dates.setDate(dates.getDate() + 1)) {
                    seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                    }
                    var scnt = 0;
                    for (a = 0; a < seldateRange.length; a++)
                    {
                    if ($.inArray(seldateRange[a], dateRange) >= 0) {
                    scnt++;
                    }
                    }
                    if (scnt > 0) {

                    $("#requestbtn").attr("disabled", true);
                    $("#maxstayerr").html("Can not book those days");
                    }
                    else
                    {
                    scnt = 0;
                    $("#requestbtn").attr("disabled", true);
                    $("#maxstayerr").html("");
                    $.ajax({
                    url: baseurl + '/showpricedetail', // point to server-side PHP script 
                            type: "POST",
                            async: false,
                            data: {
                            listid : listid,
                                    noofdays : diffDays
                            },
                            success: function (res) {
                            $("#pricediv").show();
                            $("#requestbtn").attr("disabled", false);
                            }
                    });
                    }
                    }
                    },
                    onClose: function (selectedDate) {
                    $("#enddate").datepicker('show');
                    }
            });
            $("#enddate").datepicker({
            beforeShowDay: disableDates,
                    minDate:endminimumdate,
                    maxDate:edate,
                    changeMonth: true,
                    numberOfMonths: 1,
                    beforeShowDay: function(date){
                        <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){ ?>
                    var string = $.datepicker.formatDate('yy-mm-dd', date);
                    string = string + " 11:00:00";
                    // console.log("\n selected Dates :" + string + "after date convert :" + date);
                    // console.log("\n Date Hours : " + date.getHours()+": Date Minutes "+date.getMinutes() + "\n ");

                <?php }else{ ?>
                    var string = $.datepicker.formatDate('yy-mm-dd', date);
                <?php }?>
                    var check = array2.indexOf(string) == - 1;
                    if (typeof (check) != 'undefined')
                    {
                    if (check)
                    {
                    return [true, '', ''];
                    }
                    else
                    {
                    return [false, "bookedDate", "check"];
                    }
                    }
                    },
                    onSelect: function (selectedDate) {
                    $("#requestbtn").attr("disabled", false);
                    var orginalDate = new Date(selectedDate);
                    orginalDate.setDate(orginalDate.getDate() - 1);
                    //var monthsAddedDate = new Date(new Date(orginalDate).setMonth(orginalDate.getMonth() - 3));
                    //$("#startdate").datepicker("option", 'maxDate', orginalDate);
                    stdates = $("#startdate").val();
                    stdate = new Date(stdates);
                    eddates = $("#enddate").val();
                    eddate = new Date(eddates);
                    var seldateRange = [];
                    if (stdates != "" && eddates != "") {
                    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    selstartdate = stdate.getTime();
                    selenddate = eddate.getTime();
                    for (var dates = new Date(selstartdate);
                    dates < new Date(selenddate);
                    dates.setDate(dates.getDate() + 1)) {
                        <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){ ?>
                            var formatDate = $.datepicker.formatDate('yy-mm-dd', dates) + " 11:00:00";
                            // formatDate += formatDate+" 11:00:00";
                            // console.log("Formated Dates : " + $.datepicker.formatDate('yy-mm-dd', dates) + " >>>> " + formatDate);
                    seldateRange.push(formatDate);
                    // seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                <?php }else{ ?>
                    seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                    <?php } ?>
                    }
                    var ecnt = 0;
                     var isCheckInDateBooked = [];
                    var isCheckOutDateBooked = [];
                    console.log("Before Loop Seldate Range : " + seldateRange);
                    for (b = 0; b < seldateRange.length; b++)
                    {
                       console.log("\n Date Range : " + seldateRange);
                        console.log("\n Single Date : " + seldateRange[b]);
                        console.log("\n Type of A1 > " + typeof array1);
                        console.log("\n Type of A2 > " + typeof array2);
                        // console.log("\n storedDate  : " + array2);
                        // console.log("\n storedDate  : " + array2);
                        console.log("\n SelecDate Rage Array1  : " + array1.indexOf(seldateRange[b]));
                        console.log("\n  SelecDate Rage Array2 : " + array2.indexOf(seldateRange[b]));
                        // array1.indexOf(string) == - 1
                        // console.log("\n Is available for Array 1 > " + $.inArray(seldateRange[b], array1));
                        // console.log("\n Is available for Array 2 > " + $.inArray(seldateRange[b], array2));
                        console.log( "\n " + seldateRange[b]);
                        // if($.inArray(seldateRange[b], array1) >= 0){
                            var check1 = array1.indexOf(seldateRange[b]) != -1;
                            var check2 = array2.indexOf(seldateRange[b]) != -1;
                        if(check1 > 0){
                            isCheckInDateBooked.push(seldateRange[b]);
                        }
                        // if($.inArray(seldateRange[b], array2) >= 0){
                        if(check2 > 0){
                            isCheckOutDateBooked.push(seldateRange[b]);
                        }
                        <?php //if($_SERVER['REMOTE_ADDR']){ ?>

                        console.log("\n CheckIn Date : " + isCheckInDateBooked);
                        console.log("\n Checkout Date : " + isCheckOutDateBooked);
                        console.log("\n SelectDateRange : " + seldateRange[b]);

                        <?php //} ?>

                    if ($.inArray(seldateRange[b], seldateRange) >= 0) {
                    ecnt++;
                    }
                    }
                    // if (ecnt > 0) {
                   if((isCheckInDateBooked.length > 0 || isCheckOutDateBooked.length > 0) && ecnt > 0){

                    $("#requestbtn").attr("disabled", true);
                    $("#pricediv").show();
                    $("#maxstayerr").html("Can not book those days");
                    }
                    else
                    {
                    ecnt = 0;
                    $("#requestbtn").attr("disabled", true);
                    $("#maxstayerr").html("");
                    $.ajax({
                    url: baseurl + '/showpricedetail', // point to server-side PHP script 
                            type: "POST",
                            async: false,
                            data: {
                            listid : listid,
                                    noofdays : diffDays
                            },
                            success: function (res) {
                            $("#pricediv").show();
                            $("#requestbtn").attr("disabled", false);
                            }
                    });
                    }
                    }
                    }
            });
<?php } ?>


        });
        var disableDates = function(dt) {
        var dateString = jQuery.datepicker.formatDate('yy-mm-dd', dt);
        return [dateRange.indexOf(dateString) == - 1];
        }

        function focus_checkin() {
        var visible = $("#startdate").datepicker("widget").is(":visible");
        $("#startdate").datepicker(visible ? "hide" : "show");
        //$("#startdate").datepicker('show');
        }
    </script>

    <style>
        #map_canvas {
            width: 100%;
            height: 450px;
        }
        .owl-item
        {
            float: left;
        }
    </style>


    <script src="{{ asset('frontendassets/js/jRate.js')}}"></script> 
    <script>

        $(document).ready(function(){

        $("#jRatedetail").jRate({
        rating: <?php echo isset($rating) ? $rating : 0; ?>,
                readOnly: true
        });
        $("#school_house_nav a[href^='#']").on('click', function(e) {

        // prevent default anchor click behavior
        e.preventDefault();
        // store hash
        var hash = this.hash;
        // animate
        $('html, body').animate({
        scrollTop: $(hash).offset().top
        }, 300, function(){

        // when done, add hash to url
        // (default click behaviour)
        window.location.hash = hash;
        });
        });
        });
    </script>  
    @stop
