@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
@inject('settings', 'App\Settings')
<?php
$settingsObj = $settings->settingsInfo();
$logoname = $settingsObj->logo;
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php
            $imgpath = asset('images/profile/' . Auth::user()->profile_img_name);
            if (is_null(Auth::user()->profile_img_name)) {
                $imgpath = asset('images/noimage/' . $settingsObj->no_image_profile);
            }
            ?>
            <div class="col-xs-12 col-sm-4 col-lg-3 margin_top20 margin_bottom20">
                <div class="airfcfx-profile-img profile_img pos_rel" style="background-image:url({{$imgpath}})" >			 <div class="airfcfx-profile_photo profile_photo text-center">
                <!-- a href="#"><i class="fa fa-camera"></i> Add Profile Photo</a -->
                        {!! Form::open(array('url' => 'user/image_submit', 'id' => "fileupload",   'method' => 'POST', 'files'=>true)) !!}
                        @include('partials.errors')
                        <a href="" style="top: 0;position: absolute;color:#fff;text-decoration:none;cursor:pointer;width: 100%;left: 0;padding: 10px 0;"><i class="fa fa-camera"></i>  Add Profile Photo</a>
                        <input type="file" id="userfile" name="profile_img_name" accept=".png, .jpg, .jpeg" style="opacity:0;width:100%;cursor:pointer;position:absolute;top:0;height:40px;" onchange="return on_submit();">
                        <div class="col-xs-4" style="display:none;"><button type="submit" class="btn btn-danger">Save</button></div>
                        </form>
                    </div>	


                </div> <!--profile_img end-->

                <br />
                <div class="border1 margin_bottom20 clear">
                    <div class="row padd_10_15">
                        <div class="text-center margin_top10 margin_bottom10">
                            <h1 class="airfcfx-wordwrap">{{Auth::user()->name}}</h1>
                            <a href="{{URL::to('viewuser')}}" class="text-danger">View Profile</a>
                            <br/>
                            <a href="{{URL::to('user/edit')}}">
                                <button class="airfcfx-panel btn btn_email margin_top10">Complete Profile</button>
                            </a>
                        </div>
                    </div> <!--row end -->                    
                </div> <!--border1 end -->

                <div class="airfcfx-panel panel panel-default">
                    <div class="airfcfx-panel panel-heading profile_menu1">
                        <h3 class="airfcfx-panel-title panel-title">Verifications 
                            <i class="fa fa-question-circle hover_tool pos_rel pull-right">
                                <div class="tooltip_text">
                                    <p class="font_size12">Verifications help build trust between guests and hosts and can make relations easier.  </p>
                                </div><!--tooltip_text end-->
                            </i>
                        </h3>
                    </div>
                    <div class="airfcfx-dashbd-verify-panel-body panel-body">
                        <div class="row"> 
                            <div class="airfcfx-verifications-row table1">
                                <div class="airfcfx-verified-symbol-width tab_cel text-center ">
                                    <?php
                                    $verify = "Not Verified";
                                    $classAdd = "fa fa-times-circle-o fa-1x text-danger";
                                    if (Auth::user()->email_confirmed == 1) {
                                        $verify = "Verified";
                                        $classAdd = "fa fa-check-circle-o fa-1x text-success";
                                    }
                                    ?> 
                                    <i class="{{$classAdd}}"></i>
                                </div>	
                                <p>Email</p>
                                <p class="text_gray1">{{$verify}} </p>


                            </div>
                            <div class="airfcfx-verifications-row table1">                                
                                <div class="airfcfx-verified-symbol-width tab_cel text-center ">
                                    <?php
                                    $phverify = "Not Verified";
                                    $classAdd = "fa fa-times-circle-o fa-1x text-danger";
                                    if (Auth::user()->phone_confirmed == 1) {
                                        $phverify = "Verified";
                                        $classAdd = "fa fa-check-circle-o fa-1x text-success";
                                    }
                                    ?>   
                                    <i class="{{$classAdd}}"></i>
                                </div>	
                                <p>Mobile</p>
                                <p class="text_gray1">{{$phverify}}</p>
                            </div> 						
                            <div class="airfcfx-verifications-row table1">                                
                                <div class="airfcfx-verified-symbol-width tab_cel text-center ">
                                    <?php
                                    $id_verify = "Not Verified";
                                    $classAdd = "fa fa-times-circle-o fa-1x text-danger";
                                    $user = \Auth::user();
                                    if (isset($user->user_verification->is_active) && !empty($user->user_verification->personal_id_picture_front) && $user->user_verification->is_active == 1) {
                                        $id_verify = "Verified";
                                        $classAdd = "fa fa-check-circle-o fa-1x text-success";
                                    }
                                    ?>   
                                    <i class="{{$classAdd}}"></i>
                                </div>	
                                <p>Personal ID</p>
                                <p class="text_gray1">{{$id_verify}}</p>
                            </div> 						
                            <div class="airfcfx-verifications-row table1">                                
                                <div class="airfcfx-verified-symbol-width tab_cel text-center ">
                                    <?php
                                    $cr_verify = "Not Verified";
                                    $classAdd = "fa fa-times-circle-o fa-1x text-danger";
                                    $user = \Auth::user();
                                    if (isset($user->user_verification->is_active) && !empty($user->user_verification->criminal_check_image) && $user->user_verification->is_active == 1) {
                                        $cr_verify = "Verified";
                                        $classAdd = "fa fa-check-circle-o fa-1x text-success";
                                    }
                                    ?>   
                                    <i class="{{$classAdd}}"></i>
                                </div>	
                                <p>Criminal Check</p>
                                <p class="text_gray1">{{$cr_verify}}</p>
                            </div>
                             <div class="airfcfx-verifications-row table1">                                
                            <div class="airfcfx-verified-symbol-width tab_cel text-center ">
                                <?php
                                $byflash_verify = "Not Verified";
                                $classAdd = "fa fa-times-circle-o fa-1x text-danger";
                                if (isset($user->user_verification->is_verfied_by_flash) && $user->user_verification->is_verfied_by_flash == 'yes') {
                                    $byflash_verify = "Verified";
                                    $classAdd = "fa fa-check-circle-o fa-1x text-success";
                                }
                                ?>   
                                <i class="{{$classAdd}}"></i>
                            </div>  
                            <p>Verified by Flash</p>
                            <p class="text_gray1">{{$byflash_verify}}</p>
                        </div>                         						
                            <br />
                            <a href="{{URL::to('user/trust')}}" class="text-danger margin_left20">Edit Verifications -> </a>
                        </div> <!--row end --> 
                    </div>
                </div> <!--Panel end -->
                <div class="panel panel-default">
                    <div class="panel-heading profile_menu1">
                        <h3 class=" panel-title">
                            Quick Links                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row padd_10_15">
                            <ul class="list-unstyled quick_link">                                 
                                <li><a href="{{URL::to('user/listing/rentals/active')}}" class="text-danger margin_left20">View/Manage Listing </a></li>
                                <!-- <li><a href="{{URL::to('user/listings/reservations')}}" class="text-danger margin_left20">Reservations </a></li>
                                <li><a href="{{URL::to('user/reviews')}}" class="text-danger margin_left20">Reviews & References </a></li> -->
                            </ul>
                        </div> 
                    </div>
                </div> <!--Panel end -->                   

            </div> <!--col-sm-3 end -->


            <div class="col-xs-12 col-sm-8 col-lg-9 margin_top20 margin_bottom20">
                @include('layouts.errorsuccess')
                <div class="airfcfx-panel panel panel-default">
                    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                        <h3 class="airfcfx-panel-title panel-title ">Welcome to work-related-learning, {{Auth::user()->name}}</h3>
                    </div>
                    <div class="airfcfx-panel-padding panel-body">
                        <div class="row ">                
                            <div class="">
                                <div class="col-xs-12 ">                                                       
                                    <p><p>This is your Dashboard, the place to manage your Listings, check messages in your Inbox, respond to Requests, view upcoming Work Experience and Student Guest Information, and much more!</p>
                                    <!-- http://work-related-learning.com/user/edit                                     -->
                                    <br/>
                                    <a href="{{URL::to('user/edit')}}" class="airfcfx-red-heading text-danger">Edit Your Profile</a>
                                    <p>Update your info, add basic and mandatory information, you may add all the details, complete info for becoming any of the users: listing unpaid work experience, paid services and for travelling as Secondary School Student or any Student Guest.</p>
                                    <br/>
<!-- http://work-related-learning.com/viewuser just added -->
                                    <a href="{{URL::to('viewuser')}}" class="airfcfx-red-heading text-danger">View Your Profile</a>
                                    <p>View what others see of you while surfing this website</p>
                                    <br/>
                                    <a href="{{URL::to('user/listing/rentals/active')}}" class="airfcfx-red-heading text-danger">Manage Your Listings</a>
                                    <p>Update availability and any info for all of your listings.</p>
                                    <br/>

                                    <a href="{{URL::to('faq/work')}}" class="airfcfx-red-heading text-danger">Get Help!</a>
                                    <p>View our help section and FAQs to get started on work-related-learning</p>

                                </div>  
                            </div> <!--col-xs-12 end -->                            
                        </div> <!--row end -->
                    </div>
                </div> <!--Panel end -->




            </div> <!-- col-sm-9 end -->
        </div>
    </div> <!-- row end -->

</div> <!-- container end -->

<style type="text/css">
    .col-lg-7
    {
        z-index : 1000;
    }
    .cancel,.delete,.start,.toggle
    {
        display: none;
    }
    .fileinput-button
    {
        background: none !important;
        border: none !important;
        text-align: left !important;
    }
    .fileupload-buttonbar
    {
        width:23% !important;
    }
    input[type="file"]
    {
        width: 145%;
        opacity: 0;
    }
    .profile_img
    {
        border: 1px solid #dce0e0;
    }
</style>
<script>
    function on_submit() {
        if (document.getElementById("userfile").files[0].size > 4194304)
        {
            alert("File is too big.");
            return false;
        }
        $("#fileupload").submit();
        return true;
    }
</script>@stop