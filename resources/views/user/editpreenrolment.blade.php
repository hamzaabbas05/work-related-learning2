@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
@section('content')
@include('partials.user_bredcrum')

<div class="bg_gray1">
    <div class="container">
        @inject('country', 'App\Country')
        <?php
        $countries = $country->getall();

        $roleId = 0;
        $orgdiv = 'display:none';
        $studiv = 'display:none';
        $pardiv = 'display:none';
        $orgbtnstyle = "background-color:#ccc !important";
        $stubtnstyle = "background-color:#ccc !important";
        $parbtnstyle = "background-color:#fe5771 !important";
        $disableStyle = '';
//        if (\Auth::user()->hasRole('Organization')) {
//            $orgbtnstyle = "background-color:#fe5771 !important";
//            $roleId = 4;
//            $orgdiv = 'display:block';
//            $disableStyle = 'disabled = "true"';
//        } elseif (\Auth::user()->hasRole('Parent')) {
//            $parbtnstyle = "background-color:#fe5771 !important";
//            $roleId = 5;
//            $pardiv = 'display:block';
//            $disableStyle = 'disabled = "true"';
//        } elseif (\Auth::user()->hasRole('Student')) {
//            $stubtnstyle = "background-color:#fe5771 !important";
//            $roleId = 6;
//            $studiv = 'display:block';
//            $disableStyle = 'disabled = "true"';
//        }
        ?>
        @include('partials.profile_sidebar')

        <div class="col-xs-12 col-sm-9 margin_top20">
            @include('layouts.errorsuccess')
            {!! Form::open(array('url' => 'user/update_profile', 'id' => "form-edit", 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
            @include('partials.errors')
            <input type="hidden" id="role" name="role" value="{{$roleId}}">
            <input type="hidden" id="current_page" name="current_page" value="{{isset($current_page)?$current_page:'basic_info'}}" />
            <input type="hidden" id="field_changed" name="field_changed" value="0" />
            <div class="airfcfx-panel panel panel-default" id="basic_info">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">By Pre-Enrolling you communicate the intention to take part.</h3>
                    <br>
                    <p>According to your school or other conditions places may be limited, for example your school may have to select you according to your language skills, or, as an individual, accept candidation, or, if not a school student, you may prefer a phone appointment or meeting us in person.</p><br>

                    <p>You will receive an email to confirm your enrolment with payment method options (moment in which you may confidently buy your flight ticket). </P>

                </div>

                <div class="airfcfx-panel-padding panel-body">
                    <div class="row ">
                        @include('partials.parent_of_pre_enrol_under')
                    </div> <!--Basic Info row end -->

                </div>
            </div> <!--Panel end -->


            <div  id="emergency_contact" class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">Emergency Contact Informations</h3>
                </div>
                <div class="airfcfx-panel-padding panel-body">
                    @include('partials.emergency-contact')
                    <script>
                        $(document).ready(function () {
                            $(".show_ph").click(function () {
                                $(".add_phone").show();
                                $(".show_ph").hide();
                            });
                            $(".add_cont").click(function () {
                                $(".add_contact").toggle();
                            });
                            $(".add_ship").click(function () {
                                $(".add_shipping").toggle();
                            });
                        });
                    </script> 
                </div>
            </div>
            <div class="errcls" id="submiterr" style="clear: both;"></div><br/>
            <div class="form-group">
                <button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20" onclick="return edit_pre_enroll_profile_parent();">Submit</button> 
            </div>
            <!--button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20" onclick="return edit_profile();">Submit</button> </div> -->

            </form>
        </div> <!--col-sm-9 end -->

    </div> <!--container end -->
</div>

<script>
    $(document).ready(function () {
        $('#form-edit').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(".show_ph").click(function () {
            $(".add_phone").show();
            $(".show_ph").hide();
        });
        $(".add_cont").click(function () {

            // $("#add_contact").toggle();
        });
        $(".add_ship").click(function () {
            $(".add_shipping").toggle();
        });

        $(".add_shipd").click(function () {
            $(".add_shippingd").toggle();
        });

        $(".add_shipd16").click(function () {
            $(".add_shippingd16").toggle();
        });
    });
    function showContact(obj) {
        var data_id = $(obj).data("id");
        alert(data_id);
        if (data_id == "1") {
            $(".add_contact").show();
            $(obj).attr("data-id", "0");
        } else {
            $(".add_contact").hide();
            $(obj).attr("data-id", "1");
        }
    }
</script>
<style type="text/css">
    .field-profile-phoneno
    {
        display:inline;
    }
    .help-block-error{
        clear: both;
    }
</style>

<script>

    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    google.maps.event.addDomListener(window, 'load', function () {

        /* Organization Address */

        var places6 = new google.maps.places.Autocomplete(document.getElementById('orgaddress'));
        google.maps.event.addListener(places6, 'place_changed', function () {
            var place = places6.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            document.getElementById("orglatbox").value = latitude;
            document.getElementById("orglonbox").value = longitude;
            for (var component in componentForm) {
                var cstr = "org";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "org";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        /* Student Over 18 Residence Address*/
        var places2 = new google.maps.places.Autocomplete(document.getElementById('over18raddress'));
        google.maps.event.addListener(places2, 'place_changed', function () {
            var place = places2.getPlace();

            for (var component in componentForm) {
                var cstr = "sr18";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "sr18";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        /* Student Over 18 Domicile Address*/
        var places = new google.maps.places.Autocomplete(document.getElementById('s18domicileaddress'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();

            for (var component in componentForm) {
                var cstr = "sd18";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "sd18";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });



        /* Student Under16 Domiciliary Address*/
        var places1 = new google.maps.places.Autocomplete(document.getElementById('daddress16'));
        google.maps.event.addListener(places1, 'place_changed', function () {
            var place = places1.getPlace();

            for (var component in componentForm) {
                var cstr = "d16";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "d16";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });


        /* Student Under16 Residence Address*/
        var places5 = new google.maps.places.Autocomplete(document.getElementById('sraddress16'));
        google.maps.event.addListener(places5, 'place_changed', function () {
            var place = places5.getPlace();

            for (var component in componentForm) {
                var cstr = "sr16";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "sr16";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        /* Parent Address */

        var places7 = new google.maps.places.Autocomplete(document.getElementById('parentaddress'));
        google.maps.event.addListener(places7, 'place_changed', function () {
            var place = places7.getPlace();

            for (var component in componentForm) {
                var cstr = "p";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "p";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        var places8 = new google.maps.places.Autocomplete(document.getElementById('pdomicileaddress'));
        google.maps.event.addListener(places8, 'place_changed', function () {
            var place = places8.getPlace();

            for (var component in componentForm) {
                var cstr = "dp";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "dp";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });
        // paid services basic address
        var places6 = new google.maps.places.Autocomplete(document.getElementById('houseaddress'));
        google.maps.event.addListener(places6, 'place_changed', function () {
            var place = places6.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            document.getElementById("houselatbox").value = latitude;
            document.getElementById("houselonbox").value = longitude;
            for (var component in componentForm) {
                var cstr = "h";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "h";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        var places9 = new google.maps.places.Autocomplete(document.getElementById('propertyaddress'));
        google.maps.event.addListener(places9, 'place_changed', function () {
            var place = places9.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            document.getElementById("proplatbox").value = latitude;
            document.getElementById("proplonbox").value = longitude;
            for (var component in componentForm) {
                var cstr = "prop";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }
            //alert(place.address_components.length);

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "prop";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });



    });



</script>

@stop