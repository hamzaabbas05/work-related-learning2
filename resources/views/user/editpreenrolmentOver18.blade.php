@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
@section('content')
@include('partials.user_bredcrum')

<div class="bg_gray1">
    <div class="container">
        @inject('country', 'App\Country')
        <?php
        $countries = $country->getall();

        $roleId = 0;
        $orgdiv = 'display:none';
        $studiv = 'display:none';
        $pardiv = 'display:none';
        $orgbtnstyle = "background-color:#ccc !important";
        $stubtnstyle = "background-color:#ccc !important";
        $parbtnstyle = "background-color:#fe5771 !important";
        $disableStyle = '';
//        if (\Auth::user()->hasRole('Organization')) {
//            $orgbtnstyle = "background-color:#fe5771 !important";
//            $roleId = 4;
//            $orgdiv = 'display:block';
//            $disableStyle = 'disabled = "true"';
//        } elseif (\Auth::user()->hasRole('Parent')) {
//            $parbtnstyle = "background-color:#fe5771 !important";
//            $roleId = 5;
//            $pardiv = 'display:block';
//            $disableStyle = 'disabled = "true"';
//        } elseif (\Auth::user()->hasRole('Student')) {
//            $stubtnstyle = "background-color:#fe5771 !important";
//            $roleId = 6;
//            $studiv = 'display:block';
//            $disableStyle = 'disabled = "true"';
//        }
        ?>
        @include('partials.profile_sidebar')

        <div class="col-xs-12 col-sm-9 margin_top20">
            @include('layouts.errorsuccess')
            {!! Form::open(array('url' => 'user/update_profile', 'id' => "form-edit", 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
            @include('partials.errors')
            <input type="hidden" id="role" name="role" value="{{$roleId}}">
            <input type="hidden" id="current_page" name="current_page" value="{{isset($current_page)?$current_page:'basic_info'}}" />
            <input type="hidden" id="field_changed" name="field_changed" value="0" />
            <div class="airfcfx-panel panel panel-default" id="basic_info">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">By Pre-Enrolling you communicate the intention to take part.</h3>
                    <br>
                    <p>According to your school or other conditions places may be limited, for example your school may have to select you according to your language skills, or, as an individual, accept candidation, or, if not a school student, you may prefer a phone appointment or meeting us in person.</p><br>

                    <p>You will receive an email to confirm your enrolment with payment method options (moment in which you may confidently buy your flight ticket). </P>

                </div>

                <div class="airfcfx-panel-padding panel-body">
                    <div class="row ">
                        @include('partials.parent_of_pre_enrol_over')
                    </div> <!--Basic Info row end -->

                </div>
            </div> <!--Panel end -->


            <div  id="emergency_contact" class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">Emergency Contact Informations</h3>
                </div>
                <div class="airfcfx-panel-padding panel-body">
                    @include('partials.emergency-contact')
                    <script>
                        $(document).ready(function () {
                            $(".show_ph").click(function () {
                                $(".add_phone").show();
                                $(".show_ph").hide();
                            });
                            $(".add_cont").click(function () {
                                $(".add_contact").toggle();
                            });
                            $(".add_ship").click(function () {
                                $(".add_shipping").toggle();
                            });
                        });
                    </script> 
                </div>
            </div>
            <div class="errcls" id="submiterr" style="clear: both;"></div><br/>
            <div class="form-group">
                <button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20" onclick="return edit_pre_enroll_over18_profile_parent();">Submit</button> 
            </div>
            <!--button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20" onclick="return edit_profile();">Submit</button> </div> -->

            </form>
        </div> <!--col-sm-9 end -->

    </div> <!--container end -->
</div>

<script>
    $(document).ready(function () {
        $('#form-edit').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(".show_ph").click(function () {
            $(".add_phone").show();
            $(".show_ph").hide();
        });
        $(".add_cont").click(function () {

            // $("#add_contact").toggle();
        });
        $(".add_ship").click(function () {
            $(".add_shipping").toggle();
        });

        $(".add_shipd").click(function () {
            $(".add_shippingd").toggle();
        });

        $(".add_shipd16").click(function () {
            $(".add_shippingd16").toggle();
        });
    });
    function showContact(obj) {
        var data_id = $(obj).data("id");
        alert(data_id);
        if (data_id == "1") {
            $(".add_contact").show();
            $(obj).attr("data-id", "0");
        } else {
            $(".add_contact").hide();
            $(obj).attr("data-id", "1");
        }
    }
</script>
<style type="text/css">
    .field-profile-phoneno
    {
        display:inline;
    }
    .help-block-error{
        clear: both;
    }
</style>

@stop