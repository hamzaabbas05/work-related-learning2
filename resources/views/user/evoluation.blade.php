@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
 
<div class="bg_gray1">
      <div class="container">    
         
        <div class="col-xs-12 col-sm-12 margin_top20">
		<div class="airfcfx-panel panel panel-default">
		 
    {!! Form::open(array('url' => 'user/evaluatedone', 'id' => "evaluatedone",   'method' => 'POST', 'files'=>true)) !!}
    <input type="hidden" id="enquiryId" name="enquiryId" value="{{$enquiryObj->id}}">
  @include('partials.errors')
          <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
            <h3 class="airfcfx-panel-title panel-title">Evaluation</h3>
          </div>
          <div style="text-align:center">
          <h2>Compulsory Evaluation</h2>
          <h4>In compliance with Italian law on Work Related Learning 107/2015</h4></div>
          <div style="margin-left:20px;">
        <p>Evaluation of the Student - introduction </p>
<p>At the end of the Work Related Learning program you have to express an evaluation about the competences developed by the scholar.  This judgement will be considered when making an overall evaluation of the student by the Italian school he or she is attending. </p></div>


          <div class="airfcfx-panel-padding panel-body">
            	<div class="row">
        
                          
                          <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Name of the Organisation</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-confirmpassword required">

<input type="text" disabled="true" value="{{$enquiryObj->property->title}}" id="title" class="form-control" name="title" style="width:50%;" required>

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>
 <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Workplace Address</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-confirmpassword required">

<textarea disabled="true" id="workplace_address" class="form-control" name="workplace_address" style="width:50%;" rows="6" required >{{$enquiryObj->property->workplace_address}} </textarea>

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>


                            <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">
Type of work experience
</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">

<input type="text"  id="intern" class="form-control" name="intern" value="Curricular traineeship" style="width:50%;" required>

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>
  <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Starting Date</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">

<input type="text" disabled="true" id="checkin" class="form-control" name="checkin" value="{{$enquiryObj->checkin}}" style="width:50%;" required>

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>



                          <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Ending Date</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">

<input type="text" disabled="true" id="checkout" class="form-control" value="{{$enquiryObj->checkout}}" name="checkout" style="width:50%;" required>

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>


                          <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Expected Hours</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">

<input type="text" disabled="true" id="expected_hours" class="form-control" value="{{$enquiryObj->property->work_hours}}" name="expected_hours" style="width:50%;" required>

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>

                             <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Hours done as specified on the signed register</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">

<input type="text" id="hours_done" class="form-control" value="" name="hours_done" style="width:50%;" required>

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>
  <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Evaluated Period * </label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">
  <select class="form-control" style=" " id="period" name="period" required>
                      <option value="" >Select Period</option>
                  <option value="1" >period 1 (September to May)</option>
                  <option value="2"  >period 2 (June to August)</option>
                
                        </select>
 
 
</div>                            </div>
                          </div>
                		 <p><span style="color:#ff0055">Evaluation 1</span>. The Student: is responsible of the work done: decides  the activities to do with the tutor,  managing properly working times and places; respects the rules and the working environment, does daily  tasks in a responsible way, always paying attention to his/her duties; knows the guideline of the place where he/she works ( including norms on safety ) and the rules of the Work Related Learning project. </p><br><br>
                       <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Evaluation 1 - MARK: * </label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">
  <select class="form-control" style="" id="mark_1" name="mark_1" required>
                      <option value="" >Evaluation 1</option>
                  <option value="0" >Not Reached</option>
                  <option value="6"  >6</option>
                  <option value="7" >7</option>
                   <option value="8" >8</option>
                    <option value="9" >9</option>
                     <option value="10" >Not Observed</option>
                        </select>
 
 
</div>                            </div>
                          </div>

<p><span style="color:#ff0055">Evaluation 2.</span> The student: finds the structure of the business and acknowledgement system, using applications, software and communication tools to realize simple business observation; is able to read and interpret  the main original documents in order to identify information and make considerations; supports staff in the examination of administrative facts, adopting methods, tools and software used by the company/organisation;  independently does some company surveying; being able to make himself/herself understood, knowing and using specific language (in foreign languages too) adopted in that working context; recognises the roles, the hierarchies and the skills/competences of the different people in the same working environment; knows the structure and the organogram of the company/organisation, the main original documents, the elements of the accounting system, with a particular reference to the bookkeping of purchase and sales, and to the double-entry booking method (if applicable); knows the laws the company must compèly with, the contracts, the typical terms (in foreign languages too) that are used in administrative work. 
</p><br><br>
            <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Evaluation 2   - MARK: * </label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">
  <select class="form-control" style=" " id="mark_2" name="mark_2" required>
                      <option value="" >Evaluation 2</option>
                  <option value="0" >Not Reached</option>
                  <option value="6"  >6</option>
                  <option value="7" >7</option>
                   <option value="8" >8</option>
                    <option value="9" >9</option>
                     <option value="10" >Not Observed</option>
                        </select>
 
 
</div>                            </div>
                          </div>

<p><span style="color:#ff0055">Evaluation 3.</span> The student: knows the English language taking care about the oral exposition and adapting it to the different situations; uses the specific language and a range of vocabulary suitable to communicate in that work context, recognises and figures out  the communicative style considering the interlocutor and the context; knows the best attitude to adopt in  interpersonal relationships and finally is aware of structure, specific lexicon and typical words of the expositive text (the final report).
</p><br><br>
 <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Evaluation 3   - MARK: * </label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">
  <select class="form-control" style=" " id="mark_3" name="mark_3" required>
                      <option value="" >Evaluation 3</option>
                  <option value="0" >Not Reached</option>
                  <option value="6"  >6</option>
                  <option value="7" >7</option>
                   <option value="8" >8</option>
                    <option value="9" >9</option>
                     <option value="10" >Not Observed</option>
                        </select>
 
 
</div>                            </div>
                          </div>
                   

<p><span style="color:#ff0055">Evaluation 4.</span>The student: studies, does research, communicates using information and communication technologies, applies operational methodologies shared by other operators of the company, supports operators in carrying out activities, correctly understands the data and documents relating to the hosting structure, knows the organization of activities and operational methods shared by the host structure operators.
</p><br><br>
 <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Evaluation 4   - MARK: * </label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">
  <select class="form-control" style=" " id="mark_4" name="mark_4" required>
                      <option value="" >Evaluation 4</option>
                  <option value="0" >Not Reached</option>
                  <option value="6"  >6</option>
                  <option value="7" >7</option>
                   <option value="8" >8</option>
                    <option value="9" >9</option>
                     <option value="10" >Not Observed</option>
                        </select>
 
 
</div>                            </div>
                          </div>
                      <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Further observations and comment on skills acquired during the experience:</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-confirmpassword required">

<textarea id="further_observe" class="form-control" name="further_observe"   cols="10"    rows="10"  >  </textarea>

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>  

  <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Date</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">

<input type="text" id="date" class="form-control" value="<?php echo date('Y/m/d')?>" name="date" style="width:50%;"  required>

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>

                           <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Tutor Name</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">

<input type="text" id="represent_name" class="form-control" disabled="true" value="{{$enquiryObj->user->school_tutor_name}}" name="represent_name" style="width:50%;" required >

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>
      <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Tutor SurName</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">

<input type="text" id="represent_surname" value="{{$enquiryObj->user->school_tutor_sur_name}}" class="form-control" name="represent_surname" style="width:50%;" required disabled="true">

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>
                    </div> <!--row end -->
          </div>
          <div class="airfcfx-panel-footer panel-footer">
          	<div class="text-right"><button class="airfcfx-panel btn btn_email margin_bottom10">Submit</button></div>
          </div>
          </form>      </div>  <!--Panel end -->       
       
       
      <!--div class="panel panel-default">
          <div class="panel-heading profile_menu1">
            <h3 class="panel-title">Login Notifications  </h3>
          </div>
          
          <div class="panel-body">
            	<div class="row margin_top10">
                		<div class="col-xs-12">
                        <div class="checkbox margin_bottom20">
                                <label>
                                  <input type="checkbox">   Turn on login notifications  
                                </label>
                              </div>
                        
                        <p>Login notifications are an extra security feature. When you turn them on, we’ll let you know when anyone logs into your Airbnb account from a new browser. This helps keep your account safe. </p>
                        </div>
                    </div> 
          </div>
          <div class="panel-footer">
          	<div class="text-right"><button class="btn btn_email  ">Save</button></div>
          </div>
          
      </div-->  <!--Panel end -->  
        
       
       
        
      </div> <!--col-sm-9 end -->
        
    </div> <!--container end -->
</div>

  
<script>
$(document).ready(function(){    
    $(".show_ph").click(function(){
        $(".add_phone").show();
		$(".show_ph").hide();
    });
	$(".add_cont").click(function(){
        $(".add_contact").toggle();		
    });
	$(".add_ship").click(function(){
        $(".add_shipping").toggle();		
    });
});
</script>  
@stop