@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">    
        @include('partials.account_sidebar')
        <div class="col-xs-12 col-sm-9 margin_top20">
            <div class="airfcfx-panel panel panel-default">

                {!! Form::open(array('url' => 'user/resetpassword', 'id' => "changepassword-form",   'method' => 'POST', 'files'=>true)) !!}
                @include('partials.errors')
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">Your Wallet Credits  </h3>
                </div>
                <div class="airfcfx-panel-padding panel-body">
                    <div class="row">
                        <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Amount</label>
                            <div class="col-sm-9">
                                <div class="form-group field-resetpasswordform-newpassword required">
                                    <?php
                                    
                                     $get_credits_amount_all = App\UserCredit::where("user_id",\Auth::user()->id)->where("credit_amount", ">", "0")->get();
                                    //calculating nights herer
                                    $total_credit_amount_sent = 0;
                                    foreach ($get_credits_amount_all as $key => $credit_amount) {
                                        $total_credit_amount_sent += $credit_amount->credit_amount;
                                    }
                                   // echo "<pre>";

                                    $wallet_amount = (isset($user_wallet->amount)?$user_wallet->amount:0);
                                    $credit_amount = $wallet_amount - $total_credit_amount_sent;
                                    $credit_amount = ($credit_amount > 0 ? $credit_amount : 0);
                                    ?>
                                    {{$credit_amount}}
                                </div>                            
                            </div>
                        </div>
                        <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Send to Unpaid</label>
                            <div class="col-sm-9">
                                <div class="form-group field-resetpasswordform-newpassword required">
                                    <?php
                                    $get_credits_sendtounpaid_all = App\UserCredit::where("user_id", \Auth::user()->id)->where("send_to_unpaid", ">", "0")->get();
                                    //calculating nights herer
                                    $total_send_to_unpaid_sent = 0;
                                    foreach ($get_credits_sendtounpaid_all as $key => $send_to_unpaid) {
                                        $total_send_to_unpaid_sent += $send_to_unpaid->send_to_unpaid;
                                    }
                                   // echo "<pre>";
                                    $get_credits_sendtounpaid = $total_send_to_unpaid_sent;

                                    $wallet_sendtounpaid = (isset($user_wallet->send_to_unpaid)?$user_wallet->send_to_unpaid:0);
                                    $credit_sendtounpaid = $wallet_sendtounpaid - $get_credits_sendtounpaid;
                                    $credit_sendtounpaid = ($credit_sendtounpaid > 0 ? $credit_sendtounpaid : 0);
                                    ?>
                                    {{$credit_sendtounpaid}}
                                </div>                            
                            </div>
                        </div>
                        <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Book Unpaid</label>
                            <div class="col-sm-9">
                                <div class="form-group field-resetpasswordform-newpassword required">
                                    <?php
                                   $get_credits_bookunpaid_all = App\UserCredit::where("user_id", \Auth::user()->id)->where("book_unpaid", ">", "0")->get();
                                    //calculating nights herer
                                    $total_book_unpaid_sent = 0;
                                    foreach ($get_credits_bookunpaid_all as $key => $book_unpaid) {
                                        $total_book_unpaid_sent += $book_unpaid->book_unpaid;
                                    }
                                   // echo "<pre>";
                                    $get_credits_bookunpaid = $total_book_unpaid_sent;

                                    $wallet_bookunpaid = (isset($user_wallet->book_unpaid)?$user_wallet->book_unpaid:0);
                                    $credit_bookunpaid = $wallet_bookunpaid - $get_credits_bookunpaid;
                                    $credit_bookunpaid = ($credit_bookunpaid > 0 ? $credit_bookunpaid : 0);
                                    ?>
                                    {{$credit_bookunpaid}}
                                </div>                            
                            </div>
                        </div>
                        <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Send to Schoolhouse</label>
                            <div class="col-sm-9">
                                <div class="form-group field-resetpasswordform-newpassword required">
                                    <?php
                                   $get_credits_sendschoolhouse_all = App\UserCredit::where("user_id", \Auth::user()->id)->where("send_to_schoolhouse", ">", "0")->get();
                                    //calculating nights herer
                                    $total_schoolhouse_sent = 0;
                                    foreach ($get_credits_sendschoolhouse_all as $key => $send_to_schoolhouse) {
                                        $total_schoolhouse_sent += $send_to_schoolhouse->send_to_schoolhouse;
                                    }
                                   // echo "<pre>";
                                    $get_credits_sendschoolhouse = $total_schoolhouse_sent;
                                    $wallet_sendschoolhouse = (isset($user_wallet->send_to_schoolhouse)?$user_wallet->send_to_schoolhouse:0);
                                    $credit_sendschoolhouse = $wallet_sendschoolhouse - $get_credits_sendschoolhouse;
                                    $credit_sendschoolhouse = ($credit_sendschoolhouse > 0 ? $credit_sendschoolhouse : 0);
                                        ?>
                                    {{$credit_sendschoolhouse}}
                                </div>                            
                            </div>
                        </div>
                        <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">Book Paid</label>
                            <div class="col-sm-9">
                                <div class="form-group field-resetpasswordform-newpassword required">
                                    <?php
                                   $get_credits_bookpaid_all = App\UserCredit::where("user_id", \Auth::user()->id)->where("book_paid", ">", "0")->get();
                                //calculating nights herer
                                $total_book_paid_sent = 0;
                                foreach ($get_credits_bookpaid_all as $key => $book_paid) {
                                    $total_book_paid_sent += $book_paid->book_paid;
                                }
                               // echo "<pre>";
                                $get_credits_bookpaid = $total_book_paid_sent;

                                $wallet_bookpaid = (isset($user_wallet->book_paid)?$user_wallet->book_paid:0);
                                $credit_bookpaid = $wallet_bookpaid - $get_credits_bookpaid;
                               // $credit_bookpaid = ($credit_bookpaid > 0 ? $credit_bookpaid : 0);
                                    ?>
                                    {{$credit_bookpaid}}
                                </div>                            
                            </div>
                        </div>
                    </div> <!-- row end -->
                </div>
                </form>      
            </div>  <!--Panel end -->       
        </div> <!--col-sm-9 end -->

    </div> <!--container end -->
</div>


<script>
    $(document).ready(function () {
        $(".show_ph").click(function () {
            $(".add_phone").show();
            $(".show_ph").hide();
        });
        $(".add_cont").click(function () {
            $(".add_contact").toggle();
        });
        $(".add_ship").click(function () {
            $(".add_shipping").toggle();
        });
    });
</script>  
@stop