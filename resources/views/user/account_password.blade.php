@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
      <div class="container">    
        @include('partials.account_sidebar')
        <div class="col-xs-12 col-sm-9 margin_top20">
		<div class="airfcfx-panel panel panel-default">
		 
    {!! Form::open(array('url' => 'user/resetpassword', 'id' => "changepassword-form",   'method' => 'POST', 'files'=>true)) !!}
  @include('partials.errors')
          <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
            <h3 class="airfcfx-panel-title panel-title">Change Your Password  </h3>
          </div>
          
          <div class="airfcfx-panel-padding panel-body">
            	<div class="row">
          <div class="col-xs-12 margin_bottom10">
                            <label for="" class="col-sm-3 control-label text-right">New Password</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-newpassword required">

<input type="password" id="newpassword" class="form-control" name="password" style="width:50%;">

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>
                          
                          <div class="col-xs-12">
                            <label for="" class="col-sm-3 control-label text-right">Confirm Password</label>
                            <div class="col-sm-9">
                              <div class="form-group field-resetpasswordform-confirmpassword required">

<input type="password" id="confirmpassword" class="form-control" name="password_confirm" style="width:50%;">

<p class="help-block help-block-error"></p>
</div>                            </div>
                          </div>
  
                		
                        
                    </div> <!--row end -->
          </div>
          <div class="airfcfx-panel-footer panel-footer">
          	<div class="text-right"><button class="airfcfx-panel btn btn_email margin_bottom10">Update Password</button></div>
          </div>
          </form>      </div>  <!--Panel end -->       
       
       
      <!--div class="panel panel-default">
          <div class="panel-heading profile_menu1">
            <h3 class="panel-title">Login Notifications  </h3>
          </div>
          
          <div class="panel-body">
            	<div class="row margin_top10">
                		<div class="col-xs-12">                        
                        <div class="checkbox margin_bottom20">
                                <label>
                                  <input type="checkbox">   Turn on login notifications  
                                </label>
                              </div>
                        
                        <p>Login notifications are an extra security feature. When you turn them on, we’ll let you know when anyone logs into your Airbnb account from a new browser. This helps keep your account safe. </p>
                        </div>
                    </div> 
          </div>
          <div class="panel-footer">
          	<div class="text-right"><button class="btn btn_email  ">Save</button></div>
          </div>
          
      </div-->  <!--Panel end -->  
        
       
       
        
      </div> <!--col-sm-9 end -->
        
    </div> <!--container end -->
</div>

  
<script>
$(document).ready(function(){    
    $(".show_ph").click(function(){
        $(".add_phone").show();
		$(".show_ph").hide();
    });
	$(".add_cont").click(function(){
        $(".add_contact").toggle();		
    });
	$(".add_ship").click(function(){
        $(".add_shipping").toggle();		
    });
});
</script>  
@stop