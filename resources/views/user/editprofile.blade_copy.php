@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">
        @inject('country', 'App\Country')
        <?php
        $countries = $country->getall();

        $roleId = 0;
        $orgdiv = 'display:none';
        $studiv = 'display:none';
        $pardiv = 'display:none';
        $orgbtnstyle = "background-color:#ccc !important";
        $stubtnstyle = "background-color:#ccc !important";
        $parbtnstyle = "background-color:#ccc !important";
        $disableStyle = '';
        if (\Auth::user()->hasRole('Organization')) {
            $orgbtnstyle = "background-color:#fe5771 !important";
            $roleId = 4;
            $orgdiv = 'display:block';
            $disableStyle = 'disabled = "true"';
        } elseif (\Auth::user()->hasRole('Parent')) {
            $parbtnstyle = "background-color:#fe5771 !important";
            $roleId = 5;
            $pardiv = 'display:block';
            $disableStyle = 'disabled = "true"';
        } elseif (\Auth::user()->hasRole('Student')) {
            $stubtnstyle = "background-color:#fe5771 !important";
            $roleId = 6;
            $studiv = 'display:block';
            $disableStyle = 'disabled = "true"';
        }
        ?>
        @include('partials.profile_sidebar')
        
        <div class="col-xs-12 col-sm-9 margin_top20">

            {!! Form::open(array('url' => 'user/profile_submit', 'id' => "form-edit", 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
            @include('partials.errors')
            <input type="hidden" id="role" name="role" value="{{$roleId}}">
            <div class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">Over 18 user's basic info (Offering unpaid work; Offering paid services; Over 18 student guest; 
                        Under 18 student's parent)</h3>
                </div>

                <div class="airfcfx-panel-padding panel-body">
                    <div class="row ">
                        @include('partials.basicinfo')
                    </div> <!--Basic Info row end -->

                </div>
            </div> <!--Panel end -->

            <div id="orgdiv" style="{{$orgdiv}}" class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">Organization</h3>
                </div>
                <div class="airfcfx-panel-padding panel-body">
                    <div class="row">
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Organisation Name</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="orgname" class="form-control" name="orgname" value="{{$userObject->orgname}}">

                                    <p class="help-block help-block-error"></p>
                                </div>  <div class="errcls" id="orgnameerr" style="clear: both;"></div><br/>                       

                            </div>
                        </div> <!--col-xs-12 end -->

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">About Organization</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <textarea id="aboutorg" class="form-control" name="aboutorg"   >{{$userObject->aboutorg}}</textarea>

                                    <p class="help-block help-block-error"></p>
                                </div>                         
                                <div class="errcls" id="aboutorgerr" style="clear: both;"></div><br/> 



                            </div>
                        </div> <!--col-xs-12 end -->


                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Legal Nature</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">




                                    <select id="legal_nature" class="form-control listselect selectsze" name="legal_nature"  >


                                        <option value=""> Select </option>
                                        <option <?php if ($userObject->legal_nature == 1) { ?> selected <?php } ?> value="1">Public</option>
                                        <option <?php if ($userObject->legal_nature === 0) { ?> selected <?php } ?> value="0">Private</option>
                                    </select>

                                    <p class="help-block help-block-error"></p>
                                </div>         
                                <div class="errcls" id="legal_natureerr" style="clear: both;"></div><br/> 


                            </div>
                        </div> <!--col-xs-12 end -->

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Human Resources</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">
                                    <select id="orghr" class="form-control listselect selectsze" name="orghr">
                                        <option value="">Select</option>
                                        <?php foreach ($roomTypes as $obj) { ?>
                                            <option value="{{$obj->id}}" <?php if ($obj->id == $userObject->orghr) { ?> selected <?php } ?> >{{$obj->name}}</option>
                                        <?php } ?>
                                    </select>

                                    <p class="help-block help-block-error"></p>
                                </div>             

                                <div class="errcls" id="orghrerr" style="clear: both;"></div><br/> 



                            </div>
                        </div> <!--col-xs-12 end -->
                        <input type="hidden" id="orglatbox" name="orglatbox" value="{{$userObject->org_lat}}">
                        <input type="hidden" id="orglonbox" name="orglonbox" value="{{$userObject->org_lan}}">
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Organisation Email</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="email" id="orgemail" class="form-control" name="orgemail" value="{{$userObject->orgemail}}" >
                                    <p class="help-block help-block-error"></p>
                                </div>                          
                                <div class="errcls" id="orgemailerr" style="clear: both;"></div><br/>


                            </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Organisation Phone</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="number"  id="orgphone" class="form-control" name="orgphone" value="{{$userObject->orgphone}}">

                                    <p class="help-block help-block-error"></p>
                                </div>                           
                                <div class="errcls" id="orgphoneerr" style="clear: both;"></div><br/> 


                            </div>
                        </div> <!--col-xs-12 end -->








                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="padding0 profile_label">Legal Address 
                                </label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                @include('partials.address.org_residence')
                            </div>
                        </div> <!-- Address End -->
                        <h3>As Work Tutor</h3>
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Relationship in the Company</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <select id="tutorrelation" name="tutorrelation" class="form-control" >
                                        <option value="undefined">Select</option>
                                        <?php foreach ($tutorRelation as $rs) { ?>
                                            <option value="{{$rs->id}}" <?php if ($rs->id == $userObject->tutorrelation) { ?> selected <?php } ?>>{{$rs->name}}</option>
                                        <?php } ?>
                                    </select>

                                    <p class="help-block help-block-error"></p>
                                </div>                           
                                <div class="errcls" id="tutorrelationerr" style="clear: both;"></div><br/> 


                            </div>
                        </div>

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Why are you fit for being a Work Tutor?</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <textarea  id="why_fit" class="form-control" name="why_fit" > {{$userObject->why_fit}}</textarea>

                                    <p class="help-block help-block-error"></p>
                                </div>                           
                                <div class="errcls" id="why_fiterr" style="clear: both;"></div><br/> 

                            </div>
                        </div>
                    </div>
                </div></div>
            <div  id="studentdiv" style="{{$studiv}}">
                <div class="airfcfx-panel panel panel-default">
                    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                        <h3 class="airfcfx-panel-title panel-title">Over 18 High school Student</h3>
                    </div>
                    <div class="airfcfx-panel-padding panel-body">

                        @include('partials.student-profile-common18')
                        @include('partials.address.student18_residence')
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">

                                <input type="checkbox" <?php if ($userObject->s_res_dom_same == 0) { ?> checked <?php } ?> id="s_res_dom_same" name="s_res_dom_same" onchange="showstudentDomicile(this)">
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-firstname required">

                                    Check If Residency and Domicile Addresses are different

                                    <p class="help-block help-block-error"></p>
                                </div>                        </div>
                        </div> <!--col-xs-12 end -->
                        <?php
                        $dstyle = 'display:none';
                        if ($userObject->s_res_dom_same == 0) {
                            $dstyle = 'display:block';
                        }
                        ?>
                        <div class="col-xs-12 margin_top10" id="s18domicilediv" style="{{$dstyle}}">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="padding0 profile_label">Domicile Address 
                                </label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                @include('partials.address.student18_domicile')
                            </div>
                        </div> <!-- Address End -->
                    </div>

                </div>
                <div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
                    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                        <h3 class="airfcfx-panel-title panel-title">School Info</h3>
                    </div>
                    <div class="airfcfx-panel-padding panel-body">
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Name</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">



                                    <select id="school_name" name="school_name" class="form-control"  > 
                                        <option value="">Select school</option>
                                        <?php foreach ($schools as $sc) { ?> <option <?php if ($sc->id == $userObject->school_name) { ?> selected <?php } ?> value="{{$sc->id}}">{{$sc->name}}</option><?php } ?>
                                    </select>




                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="school_nameerr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->


                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">class letter</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_letter" class="form-control" name="class_letter" value="{{$userObject->class_letter}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_lettererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->


                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">class number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="class_number" value="{{$userObject->class_number}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Name</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_name" value="{{isset($userObject->tutor_name)?$userObject->tutor_name:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Place of Birth</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_pob" value="{{isset($userObject->tutor_pob)?$userObject->tutor_pob:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Surname</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_surname" value="{{isset($userObject->tutor_surname)?$userObject->tutor_surname:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Personal Tax identification number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_pan" value="{{isset($userObject->tutor_pan)?$userObject->tutor_pan:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - e-mail</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_email" value="{{isset($userObject->tutor_email)?$userObject->tutor_email:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Telephone Number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_phone" value="{{isset($userObject->tutor_phone)?$userObject->tutor_phone:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Emergency telephone Number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_emergency_phone" value="{{isset($userObject->tutor_emergency_phone)?$userObject->tutor_emergency_phone:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->


                    </div>
                </div>
                <div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
                    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                        <h3 class="airfcfx-panel-title panel-title">Accompanying you (in case of group departure)</h3>
                    </div>
                    <div class="airfcfx-panel-padding panel-body">

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Accompanying you  - Name and Surname</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="school_tutor_name" class="form-control" name="school_tutor_name" value="{{$userObject->school_tutor_name}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>         <div class="errcls" id="school_tutor_nameerr" style="clear: both;"></div><br/>                    </div>
                        </div> <!--col-xs-12 end -->

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Accompanying you - Email</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="email" id="school_tutor_email" class="form-control" name="school_tutor_email" value="{{$userObject->school_tutor_email}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>                   <div class="errcls" id="school_tutor_emailerr" style="clear: both;"></div><br/>         </div>
                        </div> <!--col-xs-12 end -->

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Accompanying you - Number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="number" id="school_tutor_number" class="form-control" name="school_tutor_number" value="{{$userObject->school_tutor_number}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="school_tutor_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                    </div>
                </div>
            </div>

            <div id="parentdiv" style="{{$pardiv}}">
                <div class="airfcfx-panel panel panel-default">
                    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                        <h3 class="airfcfx-panel-title panel-title">Parent - Under-18 - high school student must be over 16 on departure</h3>
                    </div>
                    <div class="airfcfx-panel-padding panel-body">
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Relationship with Student hereunder<i class="fa fa-lock profile_icon" data-toggle="tooltip" data-placement="top" title="Private"></i> </label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <select class="form-control" style="width:auto;" id="student_relationship" name="student_relationship">
                                    <option value="" >Select</option>
                                    <option value="1" <?php if ($userObject->student_relationship == 1) { ?> selected <?php } ?>>Mother</option>
                                    <option value="2" <?php if ($userObject->student_relationship == 2) { ?> selected <?php } ?>>Father</option>
                                    <option value="3" <?php if ($userObject->student_relationship == 3) { ?> selected <?php } ?>>Legal Guardian
                                    </option>
                                </select>

                                <div class="errcls" id="student_relationshiperr" style="clear: both;"></div><br/>
                            </div>
                        </div> 

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">

                                <input type="checkbox" <?php if ($userObject->check_allow == 1) { ?> checked="" <?php } ?> id="allowcheck" name="check_allow">
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-firstname required">

                                    I allow my son/daughter to leave the Host family after dinner until 10pm

                                    <p class="help-block help-block-error"></p>
                                </div>                        </div>
                        </div> <!--col-xs-12 end -->

                        @include('partials.address.parent_residence')
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">

                                <input type="checkbox" <?php if ($userObject->s_res_dom_same == 0) { ?> checked <?php } ?> id="p_res_dom_same" name="p_res_dom_same" onchange="showparentdomicile(this)">
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-firstname required">

                                    Check If Residency and Domicile Address are different

                                    <p class="help-block help-block-error"></p>
                                </div>                        </div>
                        </div> <!--col-xs-12 end -->
                        <?php
                        $dstyle = 'display:none';
                        if ($userObject->s_res_dom_same == 0) {
                            $dstyle = 'display:block';
                        }
                        ?>
                        <div class="col-xs-12 margin_top10" id="pdomicilediv" style="{{$dstyle}}">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="padding0 profile_label">Domicile Address 
                                </label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                @include('partials.address.parent_domicile')
                            </div>
                        </div> <!-- Address End -->

                    </div><!-- Panel Body -->
                </div>

                <div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
                    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                        <h3 class="airfcfx-panel-title panel-title">Over 16 Student info</h3>
                    </div>
                    <div class="airfcfx-panel-padding panel-body">
                        @include('partials.studentunder16')
                        @include('partials.address.student16_residence')
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">

                                <input type="checkbox" <?php if ($userObject->s16_res_dom_same == 0) { ?> checked <?php } ?> id="s16_res_dom_same" name="s16_res_dom_same" onchange="showstudent16domicilediv(this)">
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-firstname required">

                                    Check If Residency and Domicile Address are different

                                    <p class="help-block help-block-error"></p>
                                </div>                        </div>
                        </div> <!--col-xs-12 end -->

                        <?php
                        $dstyle = 'display:none';
                        if ($userObject->s16_res_dom_same == 0) {
                            $dstyle = 'display:block';
                        }
                        ?>
                        <div class="col-xs-12 margin_top10" id="s16domicilediv" style="{{$dstyle}}">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="padding0 profile_label">Domicile Address 
                                </label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                @include('partials.address.student16_domicile')
                            </div>
                        </div> <!-- Address End -->
                    </div>
                </div>

                <div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
                    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                        <h3 class="airfcfx-panel-title panel-title">School Info</h3>
                    </div>
                    <div class="airfcfx-panel-padding panel-body">
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Name</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">



                                    <select id="school_name16" name="school_name16" class="form-control"  > 
                                        <option value="">Select school</option>
                                        <?php foreach ($schools as $sc) { ?> <option <?php if ($sc->id == $userObject->school_name) { ?> selected <?php } ?> value="{{$sc->id}}">{{$sc->name}}</option><?php } ?>
                                    </select>




                                    <p class="help-block help-block-error"></p>
                                </div>                   <div class="errcls" id="school_name16err" style="clear: both;"></div><br/>          </div>
                        </div> <!--col-xs-12 end -->


                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">class letter</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_letter16" class="form-control" name="class_letter16" value="{{$userObject->class_letter}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>           <div class="errcls" id="class_letter16err" style="clear: both;"></div><br/>                  </div>
                        </div> <!--col-xs-12 end -->


                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">class number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number16" class="form-control" name="class_number16" value="{{$userObject->class_number}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>          <div class="errcls" id="class_number16err" style="clear: both;"></div><br/>                   </div>
                        </div> <!--col-xs-12 end -->

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">matricule/student number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="student_number16" class="form-control" name="student_number16" value="{{$userObject->student_number}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>            <div class="errcls" id="student_number16err" style="clear: both;"></div><br/>                 </div>
                        </div> <!--col-xs-12 end -->


                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Name</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_name16" value="{{isset($userObject->tutor_name)?$userObject->tutor_name:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Place of Birth</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_pob16" value="{{isset($userObject->tutor_pob)?$userObject->tutor_pob:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Surname</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_surname16" value="{{isset($userObject->tutor_surname)?$userObject->tutor_surname:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Personal Tax identification number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_pan16" value="{{isset($userObject->tutor_pan)?$userObject->tutor_pan:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - e-mail</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_email16" value="{{isset($userObject->tutor_email)?$userObject->tutor_email:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Telephone Number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_phone16" value="{{isset($userObject->tutor_phone)?$userObject->tutor_phone:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">School Tutor - Emergency telephone Number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="class_number" class="form-control" name="tutor_emergency_phone16" value="{{isset($userObject->tutor_emergency_phone)?$userObject->tutor_emergency_phone:''}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>     <div class="errcls" id="class_numbererr" style="clear: both;"></div><br/>                        </div>
                        </div> <!--col-xs-12 end -->


                    </div>
                </div>
                <div class="airfcfx-panel panel panel-default" style="margin-top: 20px;">
                    <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                        <h3 class="airfcfx-panel-title panel-title">Accompanying you (in case of group departure)</h3>
                    </div>
                    <div class="airfcfx-panel-padding panel-body">
                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Accompanying you  - Name and Surname</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="text" id="school_tutor_name16" class="form-control" name="school_tutor_name16" value="{{$userObject->school_tutor_name}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>               <div class="errcls" id="school_tutor_name16err" style="clear: both;"></div><br/>              </div>
                        </div> <!--col-xs-12 end -->

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Accompanying you - Email</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="email" id="school_tutor_email16" class="form-control" name="school_tutor_email16" value="{{$userObject->school_tutor_email}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>                   <div class="errcls" id="school_tutor_email16err" style="clear: both;"></div><br/>         </div>
                        </div> <!--col-xs-12 end -->

                        <div class="col-xs-12 margin_top10">
                            <div class="col-xs-12 col-sm-3 text-right">
                                <label class="profile_label">Accompanying you - Number</label> 
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="form-group field-profile-school">

                                    <input type="number" id="school_tutor_number16" class="form-control" name="school_tutor_number16" value="{{$userObject->school_tutor_number}}"  >

                                    <p class="help-block help-block-error"></p>
                                </div>             <div class="errcls" id="school_tutor_number16err" style="clear: both;"></div><br/>                </div>
                        </div> <!--col-xs-12 end -->
                    </div>
                </div>
            </div>

            <div   class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">Emergency Contact Informations</h3>
                </div>
                <div class="airfcfx-panel-padding panel-body">
                    @include('partials.emergency-contact')

                </div>
            </div>
            <div class="errcls" id="submiterr" style="clear: both;"></div><br/>
            <div class="form-group">
                <button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20" onclick="return edit_profile();">Submit</button> </div>

            </form>
        </div> <!--col-sm-9 end -->

    </div> <!--container end -->
</div>


</form><script>
    $(document).ready(function () {
        $('#form-edit').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(".show_ph").click(function () {
            $(".add_phone").show();
            $(".show_ph").hide();
        });
        $(".add_cont").click(function () {
            $(".add_contact").toggle();
        });
        $(".add_ship").click(function () {
            $(".add_shipping").toggle();
        });

        $(".add_shipd").click(function () {
            $(".add_shippingd").toggle();
        });

        $(".add_shipd16").click(function () {
            $(".add_shippingd16").toggle();
        });
    });
</script>
<style type="text/css">
    .field-profile-phoneno
    {
        display:inline;
    }
    .help-block-error{
        clear: both;
    }
</style>

<script>

    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    google.maps.event.addDomListener(window, 'load', function () {

        /* Organization Address */

        var places6 = new google.maps.places.Autocomplete(document.getElementById('orgaddress'));
        google.maps.event.addListener(places6, 'place_changed', function () {
            var place = places6.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            document.getElementById("orglatbox").value = latitude;
            document.getElementById("orglonbox").value = longitude;
            for (var component in componentForm) {
                var cstr = "org";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "org";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        /* Student Over 18 Residence Address*/
        var places2 = new google.maps.places.Autocomplete(document.getElementById('over18raddress'));
        google.maps.event.addListener(places2, 'place_changed', function () {
            var place = places2.getPlace();

            for (var component in componentForm) {
                var cstr = "sr18";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "sr18";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        /* Student Over 18 Domicile Address*/
        var places = new google.maps.places.Autocomplete(document.getElementById('s18domicileaddress'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();

            for (var component in componentForm) {
                var cstr = "sd18";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "sd18";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });



        /* Student Under16 Domiciliary Address*/
        var places1 = new google.maps.places.Autocomplete(document.getElementById('daddress16'));
        google.maps.event.addListener(places1, 'place_changed', function () {
            var place = places1.getPlace();

            for (var component in componentForm) {
                var cstr = "d16";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "d16";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });


        /* Student Under16 Residence Address*/
        var places5 = new google.maps.places.Autocomplete(document.getElementById('sraddress16'));
        google.maps.event.addListener(places5, 'place_changed', function () {
            var place = places5.getPlace();

            for (var component in componentForm) {
                var cstr = "sr16";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "sr16";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        /* Parent Address */

        var places7 = new google.maps.places.Autocomplete(document.getElementById('parentaddress'));
        google.maps.event.addListener(places7, 'place_changed', function () {
            var place = places7.getPlace();

            for (var component in componentForm) {
                var cstr = "p";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "p";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        var places8 = new google.maps.places.Autocomplete(document.getElementById('pdomicileaddress'));
        google.maps.event.addListener(places8, 'place_changed', function () {
            var place = places8.getPlace();

            for (var component in componentForm) {
                var cstr = "dp";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "dp";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });



    });



</script>
@stop