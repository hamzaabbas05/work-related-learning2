@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
@section('content')
@include('partials.user_bredcrum')

<div class="bg_gray1">
    <div class="container">
        @inject('country', 'App\Country')
        <?php
        $countries = $country->getall();

        $roleId = 0;
        $orgdiv = 'display:none';
        $studiv = 'display:none';
        $pardiv = 'display:none';
        $orgbtnstyle = "background-color:#ccc !important";
        $stubtnstyle = "background-color:#ccc !important";
        $parbtnstyle = "background-color:#fe5771 !important";
        $disableStyle = '';
//        if (\Auth::user()->hasRole('Organization')) {
//            $orgbtnstyle = "background-color:#fe5771 !important";
//            $roleId = 4;
//            $orgdiv = 'display:block';
//            $disableStyle = 'disabled = "true"';
//        } elseif (\Auth::user()->hasRole('Parent')) {
//            $parbtnstyle = "background-color:#fe5771 !important";
//            $roleId = 5;
//            $pardiv = 'display:block';
//            $disableStyle = 'disabled = "true"';
//        } elseif (\Auth::user()->hasRole('Student')) {
//            $stubtnstyle = "background-color:#fe5771 !important";
//            $roleId = 6;
//            $studiv = 'display:block';
//            $disableStyle = 'disabled = "true"';
//        }
        ?>
        @include('partials.profile_sidebar')

        <div class="col-xs-12 col-sm-9 margin_top20">
            @include('layouts.errorsuccess')
            {!! Form::open(array('url' => 'user/update_profile', 'id' => "form-edit", 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
            @include('partials.errors')
            <input type="hidden" id="role" name="role" value="{{$roleId}}">
            <input type="hidden" id="current_page" name="current_page" value="{{isset($current_page)?$current_page:'basic_info'}}" />
            <input type="hidden" id="field_changed" name="field_changed" value="0" />
            <div class="airfcfx-panel panel panel-default" id="trust_info" style="display: block;">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">Trust and Verification</h3>
                    <p class="font_size13">Trust is fundamental when relating to people. To reduce risks we allow users to provide and verify.</p> 

                </div>

                <div class="airfcfx-panel-padding panel-body">
                    <div class="row">        
                        <?php
                        $verify = "Your Email is Not Verified";
                        if (Auth::user()->email_confirmed == 1) {
                            $verify = "Your email address has been verified successfully.";
                        }
                        $phverify = "Your Phone is Not Verified";
                        if (Auth::user()->phone_confirmed == 1) {
                            $phverify = "Your Phone Number has been verified successfully";
                        }
                        $is_active = isset($user_verification->is_active) ? $user_verification->is_active : "";
                        if($is_active == "0") {
                            $disabled = "";
                        } else {
                            $disabled = "";
                        }
                        ?>           
                        <div class="">

                            <div class="col-xs-12 trust">
                                <h4>Email Address</h4>
                                {{$verify}}             
                            </div>  
                            <div id="succmsg"></div>

                            <div class="col-xs-12 trust margin_top20">
                                <h4>Phone Number - To be implemented</h4>  
                                <p>{{$phverify}}</p>
                                <p class="font_size13">Make it easier to communicate with a verified phone number. We’ll send you a code by SMS or read it to you over the phone. Enter the code below to confirm that you’re the person on the other end. </p> 
                                <br>
                            </div>
                            <div id="succmsg"></div>

                            <div class="col-xs-12 trust margin_top20">
                                <h4>Personal ID</h4>  
                                <p>
                                    @if(isset($user_verification->is_active) && !empty($user_verification->personal_id_picture_front) && $user_verification->is_active == 1)
                                    Your ID is Verified 
                                    @else
                                    Your ID is Not Verified 
                                    @endif
                                </p>
                                <p class="font_size13">It is important to identify people involved who often deal with minors or Vulnerable Adults. The details within, like all data which could identify you, will not be visible on public profile.</p> 
                                <br>
                            </div>
                            <div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                    <label class="profile_label">Personal ID kind</label> 
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="form-group field-profile-state required">
                                        <select name="personal_id_kind" class="form-control" {{$disabled}}>
                                            <option value="">Select Personal ID</option>
                                            <option value="Passport" @if(isset($user_verification->personal_id_kind) && $user_verification->personal_id_kind == "Passport") selected="" @endif>Passport</option>
                                            <option value="ID Card" @if(isset($user_verification->personal_id_kind) && $user_verification->personal_id_kind == "ID Card") selected="" @endif>ID Card</option>
                                            <option value="Driving Licence" @if(isset($user_verification->personal_id_kind) && $user_verification->personal_id_kind == "Driving Licence") selected="" @endif>Driving Licence</option>
                                            <option value="Other" @if(isset($user_verification->personal_id_kind) && $user_verification->personal_id_kind == "Other") selected="" @endif>Other</option>
                                        </select>
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                    <div class="errcls" id="agent_email_error" style="clear: both;"></div><br/>
                                </div>
                            </div>
                            <div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                    <label class="profile_label">Personal ID Expiry date</label> 
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="form-group field-profile-state required">
                                        <input type="text" name="personal_id_expiry_date" {{$disabled}} class="form-control datepicker" value="{{isset($user_verification->personal_id_expiry_date)?$user_verification->personal_id_expiry_date:''}}" />
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                    <div class="errcls" id="agent_email_error" style="clear: both;"></div><br/>
                                </div>
                            </div>
                            <div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                    <label class="profile_label">Personal ID upload Image - Front</label> 
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="form-group field-profile-state required">
                                        <div id="personal_id_picture_front_image">
                                        @if(isset($user_verification->personal_id_picture_front) && !empty($user_verification->personal_id_picture_front))
                                            <div class="col-md-3 listimgdiv">
                                            <img src="{{Asset('images/extra_info/'.$user_verification->personal_id_picture_front)}}" alt="" style="width: 50%;"/>
                                            <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$obj->id" onclick="remove_trust_image(this, '<?php echo $user_verification->personal_id_picture_front;?>','personal_id_picture_front')"></i>
                                            </div>
                                        @endif
                                        </div>
                                        <input type="file" name="personal_id_picture_front" class="form-control" {{$disabled}} />
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                    <div class="errcls" id="agent_email_error" style="clear: both;"></div><br/>
                                </div>
                            </div>
                            <div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                    <label class="profile_label">Personal ID upload Image - Back</label> 
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="form-group field-profile-state required">
                                        <div id="personal_id_picture_back_image">
                                        @if(isset($user_verification->personal_id_picture_back) && !empty($user_verification->personal_id_picture_back))
                                            <div class="col-md-3 listimgdiv">
                                            <img src="{{Asset('images/extra_info/'.$user_verification->personal_id_picture_back)}}" alt="" style="width: 50%;"/>
                                            <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$obj->id" onclick="remove_trust_image(this, '<?php echo $user_verification->personal_id_picture_back;?>','personal_id_picture_back')"></i>
                                        </div>
                                        @endif
                                        </div>
                                        <input type="file" name="personal_id_picture_back" class="form-control" {{$disabled}} />
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                    <div class="errcls" id="agent_email_error" style="clear: both;"></div><br/>
                                </div>
                            </div>
                            <div class="col-xs-12 trust margin_top20">
                                <h4>Criminal Record Check</h4>  
                                <p>
                                    @if(isset($user_verification->is_active) && !empty($user_verification->criminal_check_image) && $user_verification->is_active == 1)
                                    Your Criminal Record check is Verified
                                    @else
                                    Your Criminal Record check is Not Verified
                                    @endif
                                </p>
                                <p class="font_size13">When working or assisting Minors it is important to provide a Criminal Record Check for people involved. Law does not require this so we ask for latest, Student Guests and their parents may consider this important in their choices. The details within, like all data which could identify you, will not be visible on public profile </p> 
                                <br>
                            </div>
                            <div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                    <label class="profile_label">Criminal Record Check</label> 
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="form-group field-profile-state required">
                                        <label class="radio-inline" style="width: 20%;">
                                            <span style="margin-right: 5px;">Done</span>
                                            <input type="radio" name="criminal_check_done" value="1" {{$disabled}} @if(isset($user_verification->criminal_check_done) && $user_verification->criminal_check_done == 1) checked="" @endif />
                                        </label>
                                        <label class="radio-inline" style="width: 30%;">
                                            <span style="margin-right: 5px;">Not Done</span>
                                            <input type="radio" name="criminal_check_done" value="0" {{$disabled}} @if(isset($user_verification->criminal_check_done) && $user_verification->criminal_check_done == 0) checked="" @endif/>
                                        </label>
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                    <div class="errcls" id="agent_email_error" style="clear: both;"></div><br/>
                                </div>
                            </div>
                            <div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                    <label class="profile_label">Criminal Record Check - Last Done (year)</label> 
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="form-group field-profile-state required">
                                        <input type="number" maxlength="4" name="criminal_check_last_done_year" {{$disabled}} class="form-control" value="{{isset($user_verification->criminal_check_last_done_year)?$user_verification->criminal_check_last_done_year:''}}" />
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                    <div class="errcls" id="agent_email_error" style="clear: both;"></div><br/>
                                </div>
                            </div>
                            <div class="col-xs-12 margin_top10">
                                <div class="col-xs-12 col-sm-3 text-right">
                                    <label class="profile_label">Criminal Record Check upload Image</label> 
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="form-group field-profile-state required">
                                        <div id="criminal_check_image">
                                        @if(isset($user_verification->criminal_check_image) && !empty($user_verification->criminal_check_image))
                                        <div class="col-md-3 listimgdiv">
                                            <img src="{{Asset('images/extra_info/'.$user_verification->criminal_check_image)}}" alt="" style="width: 50%;"/>
                                             <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$obj->id" onclick="remove_trust_image(this, '<?php echo $user_verification->criminal_check_image;?>','criminal_check_image')"></i>
                                        </div>
                                        @endif
                                    </div>
                                        <input type="file" name="criminal_check_image" class="form-control" {{$disabled}}/>
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                    <div class="errcls" id="agent_email_error" style="clear: both;"></div><br/>
                                </div>
                            </div>
                        </div> <!--col-xs-12 end -->

                    </div>

                </div>
                <script>
                    $(document).ready(function () {
                        $(".show_ph").click(function () {
                            $(".add_phone").show();
                            $(".show_ph").hide();
                        });
                        $(".add_cont").click(function () {
                            $(".add_contact").toggle();
                        });
                        $(".add_ship").click(function () {
                            $(".add_shipping").toggle();
                        });
                    });
                </script>  
            </div> <!--Panel end -->

            <div  id="emergency_contact" class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="airfcfx-panel-title panel-title">Emergency Contact Informations</h3>
                </div>
                <div class="airfcfx-panel-padding panel-body">
                    @include('partials.emergency-contact')

                </div>
            </div>
            <div class="errcls" id="submiterr" style="clear: both;"></div><br/>
            <div class="form-group">
                <button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20" {{$disabled}}>Submit</button> 
            </div>
            <!--button type="submit" class="pull-right airfcfx-panel btn btn_email margin_bottom20" onclick="return edit_profile();">Submit</button> </div> -->

            </form>
        </div> <!--col-sm-9 end -->

    </div> <!--container end -->
</div>

<script>
    $(document).ready(function () {
        $('#form-edit').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(".show_ph").click(function () {
            $(".add_phone").show();
            $(".show_ph").hide();
        });
        $(".add_cont").click(function () {

            // $("#add_contact").toggle();
        });
        $(".add_ship").click(function () {
            $(".add_shipping").toggle();
        });

        $(".add_shipd").click(function () {
            $(".add_shippingd").toggle();
        });

        $(".add_shipd16").click(function () {
            $(".add_shippingd16").toggle();
        });
    });
    function showContact(obj) {
        var data_id = $(obj).data("id");
        alert(data_id);
        if (data_id == "1") {
            $(".add_contact").show();
            $(obj).attr("data-id", "0");
        } else {
            $(".add_contact").hide();
            $(obj).attr("data-id", "1");
        }
    }
</script>
<script>
$(document).ready(function(){
    // $(".property_image_paid").change(function(){
        $('input[name=personal_id_picture_front]').change(function() {
            // alert("Hi, I mage upload event fire");

            // return false;
            var inp = this;
    uploadedfiles = $("#uploadedfiles").val();

    //check file format.
    var file = inp.files[0];
    var fileType = file["type"];

    var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png"];
    
    if ($.inArray(fileType, ValidImageTypes) < 0) {
         alert("Invalid file format uploaded, please use jpg, jpeg and png only.");
        return false;
    }

    imagesarr = [];
    var i = 0, len = inp.files.length, img, reader, file;

    formdata = new FormData();
    formdata.append('current_page', 'user_editverifiation');
    formdata.append('type', 'personal_id_picture_front');
    // formdata.append('redirect', redirectUrl);
    var i = 0;
    // for (; i < len; i++) {
        file = inp.files[i];

        if (!!file.type.match(/image.*/)) {
            if (window.FileReader) {
                reader = new FileReader();
                reader.onloadend = function (e) {
                    //showUploadedItem(e.target.result, file.fileName);
                };
                reader.readAsDataURL(file);
            }
            if (formdata) {
                formdata.append("personal_id_picture_front", file);
            }
        }
    // }

    $.ajax({
            url: baseurl + '/user/update_profile', // point to server-side PHP script 
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#loadingimg").show();
                $("#startuploadbtn").attr("disabled", "true");
            },
            success: function (res) {
                // alert("Response Get : " + res);
                $("#personal_id_picture_front_image").html(res);
                
                $('input[name=personal_id_picture_front]').val('');
                return false;
            }
        });
        });
        $('input[name=personal_id_picture_back]').change(function() {
            // alert("Hi, I mage upload event fire");
            // return false;
            var inp = this;
    uploadedfiles = $("#uploadedfiles").val();

    //check file format.
    var file = inp.files[0];
    var fileType = file["type"];

    var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png"];
    
    if ($.inArray(fileType, ValidImageTypes) < 0) {
         alert("Invalid file format uploaded, please use jpg, jpeg and png only.");
        return false;
    }

    imagesarr = [];
    var i = 0, len = inp.files.length, img, reader, file;

    formdata = new FormData();
    formdata.append('current_page', 'user_editverifiation');
    formdata.append('type', 'personal_id_picture_back');
    // formdata.append('redirect', redirectUrl);
    var i = 0;
    // for (; i < len; i++) {
        file = inp.files[i];

        if (!!file.type.match(/image.*/)) {
            if (window.FileReader) {
                reader = new FileReader();
                reader.onloadend = function (e) {
                    //showUploadedItem(e.target.result, file.fileName);
                };
                reader.readAsDataURL(file);
            }
            if (formdata) {
                formdata.append("personal_id_picture_back", file);
            }
        }
    // }

    $.ajax({
            url: baseurl + '/user/update_profile', // point to server-side PHP script 
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#loadingimg").show();
                $("#startuploadbtn").attr("disabled", "true");
            },
            success: function (res) {
                // alert("Response Get : " + res);
                $("#personal_id_picture_back_image").html(res);
                $('input[name=personal_id_picture_back]').val('');
                return false;
            }
        });
        });
        $('input[name=criminal_check_image]').change(function() {
            // alert("Hi, I mage upload event fire");
            // return false;
            var inp = this;
    uploadedfiles = $("#uploadedfiles").val();

    //check file format.
    var file = inp.files[0];
    var fileType = file["type"];

    var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png"];
    
    if ($.inArray(fileType, ValidImageTypes) < 0) {
         alert("Invalid file format uploaded, please use jpg, jpeg and png only.");
        return false;
    }

    imagesarr = [];
    var i = 0, len = inp.files.length, img, reader, file;

    formdata = new FormData();
    formdata.append('current_page', 'user_editverifiation');
    formdata.append('type', 'criminal_check_image');
    // formdata.append('redirect', redirectUrl);
    var i = 0;
    // for (; i < len; i++) {
        file = inp.files[i];

        if (!!file.type.match(/image.*/)) {
            if (window.FileReader) {
                reader = new FileReader();
                reader.onloadend = function (e) {
                    //showUploadedItem(e.target.result, file.fileName);
                };
                reader.readAsDataURL(file);
            }
            if (formdata) {
                formdata.append("criminal_check_image", file);
            }
        }
    // }

    $.ajax({
            url: baseurl + '/user/update_profile', // point to server-side PHP script 
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#loadingimg").show();
                $("#startuploadbtn").attr("disabled", "true");
            },
            success: function (res) {
                // alert("Response Get : " + res);
                $("#criminal_check_image").html(res);

                $('input[name=criminal_check_image]').val('');
                return false;
            }
        });
        });
});

function remove_trust_image(org, imgname,page_name){
   if(confirm("Are you sure? By confirming you will remove this pic.")){
    $(org).hide();
    $(org).prev("img").hide();
    $(org).parent().remove();

    $.ajax({
        type: 'POST',
        url: baseurl + '/user/update_profile',
        async: false,
        data: {
            uploadimg: imgname,
            action:'remove',
            type:page_name,
            current_page:'user_editverifiation'
        },
        success: function (data) {
            alert('Photo removed Successfully.');
            return true;
        }
    });

}
}
</script>
<style type="text/css">
    .field-profile-phoneno
    {
        display:inline;
    }
    .help-block-error{
        clear: both;
    }
    .listclose{right:20px !important;}
</style>

<script>

    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    google.maps.event.addDomListener(window, 'load', function () {

        /* Organization Address */

        var places6 = new google.maps.places.Autocomplete(document.getElementById('orgaddress'));
        google.maps.event.addListener(places6, 'place_changed', function () {
            var place = places6.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            document.getElementById("orglatbox").value = latitude;
            document.getElementById("orglonbox").value = longitude;
            for (var component in componentForm) {
                var cstr = "org";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "org";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        /* Student Over 18 Residence Address*/
        var places2 = new google.maps.places.Autocomplete(document.getElementById('over18raddress'));
        google.maps.event.addListener(places2, 'place_changed', function () {
            var place = places2.getPlace();

            for (var component in componentForm) {
                var cstr = "sr18";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "sr18";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        /* Student Over 18 Domicile Address*/
        var places = new google.maps.places.Autocomplete(document.getElementById('s18domicileaddress'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();

            for (var component in componentForm) {
                var cstr = "sd18";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "sd18";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });



        /* Student Under16 Domiciliary Address*/
        var places1 = new google.maps.places.Autocomplete(document.getElementById('daddress16'));
        google.maps.event.addListener(places1, 'place_changed', function () {
            var place = places1.getPlace();

            for (var component in componentForm) {
                var cstr = "d16";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "d16";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });


        /* Student Under16 Residence Address*/
        var places5 = new google.maps.places.Autocomplete(document.getElementById('sraddress16'));
        google.maps.event.addListener(places5, 'place_changed', function () {
            var place = places5.getPlace();

            for (var component in componentForm) {
                var cstr = "sr16";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "sr16";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        /* Parent Address */

        var places7 = new google.maps.places.Autocomplete(document.getElementById('parentaddress'));
        google.maps.event.addListener(places7, 'place_changed', function () {
            var place = places7.getPlace();

            for (var component in componentForm) {
                var cstr = "p";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "p";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        var places8 = new google.maps.places.Autocomplete(document.getElementById('pdomicileaddress'));
        google.maps.event.addListener(places8, 'place_changed', function () {
            var place = places8.getPlace();

            for (var component in componentForm) {
                var cstr = "dp";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "dp";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });
        // paid services basic address
        var places6 = new google.maps.places.Autocomplete(document.getElementById('houseaddress'));
        google.maps.event.addListener(places6, 'place_changed', function () {
            var place = places6.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            document.getElementById("houselatbox").value = latitude;
            document.getElementById("houselonbox").value = longitude;
            for (var component in componentForm) {
                var cstr = "h";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "h";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });

        var places9 = new google.maps.places.Autocomplete(document.getElementById('propertyaddress'));
        google.maps.event.addListener(places9, 'place_changed', function () {
            var place = places9.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            document.getElementById("proplatbox").value = latitude;
            document.getElementById("proplonbox").value = longitude;
            for (var component in componentForm) {
                var cstr = "prop";
                document.getElementById(component + cstr).value = '';
                document.getElementById(component + cstr).disabled = false;
            }
            //alert(place.address_components.length);

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var cstr = "prop";
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + cstr).value = val;
                }
            }
        });



    });



</script>

@stop