@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
@inject('settings', 'App\Settings')
<?php
$settingsObj = $settings->settingsInfo();
$logoname = $settingsObj->logo;
?>
<style type="text/css">
    .detail_student {
        margin-top: 10px;
    }
</style>
<div class="bg_gray1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3 margin_top20 margin_bottom20">
                <?php
                $imgpath = asset('images/profile/' . $userobj->profile_img_name);
                if (is_null($userobj->profile_img_name)) {
                    $imgpath = asset('images/noimage/' . $settingsObj->no_image_profile);
                }
                ?>
                <div class="profile_img" style="background-image:url({{$imgpath}});" ></div>        	
                <div class="border1 margin_top20 margin_bottom20">
                    <h4 class="profile_menu1 padding10 margin_top0 border_bottom padding-left-15" style="padding:0px;margin-bottom:0px;">
                        Verifications	
                        <i class="fa fa-question-circle hover_tool pos_rel pull-right">
                            <div class="tooltip_text">
                                <p class="font_size12">Verifications help build trust between guests and hosts and can make relations easier. </p>
                            </div><!--tooltip_text end-->
                        </i>
                    </h4>

                    <div class="bg_white"> 
                        <div class="airfcfx-verifications-row table1">
                            <div class="airfcfx-verified-symbol-width tab_cel text-center ">
                                <?php
                                $verify = "Not Verified";
                                $classAdd = "fa fa-times-circle-o fa-1x text-danger";
                                if ($userobj->email_confirmed == 1) {
                                    $verify = "Verified";
                                    $classAdd = "fa fa-check-circle-o fa-1x text-success";
                                }
                                ?>   
                                <i class="{{$classAdd}}"></i>
                            </div>	
                            <p>Email</p>
                            <p class="text_gray1">{{$verify}} </p>
                        </div>
                        <?php
                        $phverify = "Not Verified";
                        $classAdd = "fa fa-times-circle-o fa-1x text-danger";
                        if ($userobj->phone_confirmed == 1) {
                            $phverify = "Verified";
                            $classAdd = "fa fa-check-circle-o fa-1x text-success";
                        }
                        ?>   
                        <div class="airfcfx-verifications-row table1">                               
                            <div class="airfcfx-verified-symbol-width tab_cel text-center ">
                                <i class="{{$classAdd}}"></i>
                            </div>	 
                            <p>Mobile</p>
                            <p class="text_gray1">{{$phverify}}</p>										

                        </div>   
                        <div class="airfcfx-verifications-row table1">                                
                            <div class="airfcfx-verified-symbol-width tab_cel text-center ">
                                <?php
                                $id_verify = "Not Verified";
                                $classAdd = "fa fa-times-circle-o fa-1x text-danger";
                                if (isset($userobj->user_verification->is_active) && !empty($userobj->user_verification->personal_id_picture_front) && $userobj->user_verification->is_active == 1) {
                                    $id_verify = "Verified";
                                    $classAdd = "fa fa-check-circle-o fa-1x text-success";
                                }
                                ?>   
                                <i class="{{$classAdd}}"></i>
                            </div>	
                            <p>Personal ID</p>
                            <p class="text_gray1">{{$id_verify}}</p>
                        </div> 						
                        <div class="airfcfx-verifications-row table1">                                
                            <div class="airfcfx-verified-symbol-width tab_cel text-center ">
                                <?php
                                $cr_verify = "Not Verified";
                                $classAdd = "fa fa-times-circle-o fa-1x text-danger";
                                if (isset($userobj->user_verification->is_criminal_verified) && !empty($userobj->user_verification->criminal_check_image) && $userobj->user_verification->is_criminal_verified == 'yes') {
                                    $cr_verify = "Verified";
                                    $classAdd = "fa fa-check-circle-o fa-1x text-success";
                                }
                                ?>   
                                <i class="{{$classAdd}}"></i>
                            </div>	
                            <p>Criminal Check</p>
                            <p class="text_gray1">{{$cr_verify}}</p>
                        </div>
                        <div class="airfcfx-verifications-row table1">                                
                            <div class="airfcfx-verified-symbol-width tab_cel text-center ">
                                <?php
                                $byflash_verify = "Not Verified";
                                $classAdd = "fa fa-times-circle-o fa-1x text-danger";
                                if (isset($userobj->user_verification->is_verfied_by_flash) && $userobj->user_verification->is_verfied_by_flash == 'yes') {
                                    $byflash_verify = "Verified";
                                    $classAdd = "fa fa-check-circle-o fa-1x text-success";
                                }
                                ?>   
                                <i class="{{$classAdd}}"></i>
                            </div>  
                            <p>Verified by Flash</p>
                            <p class="text_gray1">{{$byflash_verify}}</p>
                        </div>                        
                    </div> <!--row end -->                    
                </div> <!--border1 end -->
            </div>

            <div class="col-xs-12 col-sm-9 margin_top20 margin_bottom20">
                <h1>Hey, I’m {{$userobj->name}}!</h1>
                @if(isset($userobj->student_relationship))
                <h4>
                    @if($userobj->student_relationship == 1)
                    Mother of 
                    @elseif($userobj->student_relationship == 2)
                    Father of 
                    @elseif($userobj->student_relationship == 3)
                    Legal Guardian of 
                    @endif
                    {{$userobj->sname16}}, 
                    {{$userobj->gender16}}, 
                    <?php
                    //date in mm/dd/yyyy format; or it can be in other formats as well
                    $birthDate = (isset($userobj->dob16) && !empty($userobj->dob16)) ? date("m/d/Y", strtotime($userobj->dob16)) : "";
                    //explode the date to get month, day and year
                    $birthDate = explode("/", $birthDate);
                    //get age from date or birthdate
                    $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
                    ?>
                    Age {{$age}}, 
                </h4>
                @endif
                <h4>IN · Member since {{date_format($userobj->created_at, 'jS F Y')}}</h4> 
                <div style="margin-top: 20px;">
                    <fieldset>
                        <legend>Student Details:</legend>
                        <p class="detail_student">
                            <b>Description :</b> {{isset($userobj->about_yourself)?$userobj->about_yourself:""}}
                        </p>
                        <p class="detail_student">
                            <b>Medical or learning Condition :</b> {{isset($userobj->medical_learning)?$userobj->medical_learning:""}}
                        </p>
                        <p class="detail_student">
                            <b>Notes for Host family :</b> {{isset($userobj->host_family_notes)?$userobj->host_family_notes:""}}
                        </p>
                        <p class="detail_student">
                            <b>Notes for Work Tutor :</b> {{isset($userobj->work_tutor_notes)?$userobj->work_tutor_notes:""}}
                        </p>
                        <p class="detail_student">
                            <b>Academic Qualifications :</b> 
                            {{isset($userobj->academic_qualifications)?$userobj->academic_qualifications:""}}
                        </p>
                        <p class="detail_student">
                            <b>Date of attainment :</b> 
                            {{isset($userobj->date_of_attainment)?$userobj->date_of_attainment:""}}
                        </p>
                        <p class="detail_student">
                            <b>Attending school :</b> 
                            {{isset($userobj->school->name)?$userobj->school->name:""}}
                        </p>
                        <p class="detail_student">
                            <b>School Tutor - Name (all details are provided once request is accepted) :</b> 
                            {{isset($userobj->tutor_name)?$userobj->tutor_name:""}}
                        </p>
                        <!-- 
                        @if(isset($userobj->group_info->id))
                        <p class="detail_student">
                            <b>Travelling with student Name (if any) :</b> 
                            {{isset($userobj->school_tutor_name)?$userobj->school_tutor_name:""}}
                        </p> 
                        @endif
                        -->
                    </fieldset>

                </div>
                <!-- <a class="text-danger" href="{{URL::to('user/edit')}}">Edit Profile</a> -->

            </div>
        </div> <!-- row end -->

    </div> <!-- container end -->


</div>

<script>
    $(document).ready(function () {
        $("#w0-success-0").css("display", "block");
        $("#w0-success-0").css("right", "0");
    });
    setTimeout(function () {
        $("#w0-success-0").css("right", "-845px");
    }, 4000);
</script>
@stop