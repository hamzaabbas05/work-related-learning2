@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')

<div class="bg_gray1">
    <div class="container">  
		<div class="row">
			<div class="col-md-12 margin_top30">
				<h4>Services</h4>
				<?php 
						
				$html = '';
				
					$html .= '<table class="booking_services_table" style="width:100%" border="1">
								<thead>
									<th>Booking No</th>
									<th>Schoolhouse Title</th>
									<th>Checkin Date</th>
									<th>Checkout Date</th>
									<th>Your Credits</th>
									<th>Required Credits</th>
									<th>Total Price</th>
									<th>Pending Payment</th>
								</thead>';
					
						$html .= '<tbody><tr>';
						
						
						$html .= "<td>".$booking_request->booking_number."</td>";
						$html .= "<td>".$booking_request->service_title."</td>";
						$html .= "<td>".$booking_request->checkin_date."</td>";
						$html .= "<td>".$booking_request->checkout_date."</td>";
						$html .= "<td>".$booking_request->your_credits."</td>";
						$html .= "<td>".$booking_request->required_credits."</td>";
						$html .= "<td>".$booking_request->currency_symbol." ".$booking_request->required_credits * $booking_request->per_day_price."</td>";
						$html .= "<td>".$booking_request->currency_symbol." ".$booking_request->to_be_paid."</td>";
						$html .= "</tr></tbody>";
						
					
					$html .= "</table>";
					
				
				echo $html;

				
				?>
			</div>
			<div class="col-md-6 col-md-offset-3 margin_top30">
				<div class="panel panel-default credit-card-box">
					<div class="panel-heading display-table" style="background: #fe5771; color: #fff;" >
						<div class="row display-tr" >
							<h3 class="panel-title display-td" style="line-height: 45px; padding: 0px 20px;">Payment Details <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png"></h3>
						</div>                    
					</div>
					<div class="panel-body"> 
						<form role="form" action="{{ route('stripe.post') }}" method="post" class="require-validation"
														 data-cc-on-file="false"
														data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
														id="payment-form">
						
						<?php  echo '<input type="hidden" name="booking_number" value="'.$booking_number.'" />'; ?>
						@if (Session::has('success'))
							<div class="alert alert-success text-center">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
								<p>{{ Session::get('success') }}</p>
							</div>
						@endif
	  
						
							 
							<div class='form-row row'>
								<div class='col-xs-12 form-group required'>
									<label class='control-label'>Name on Card</label> <input
										class='form-control' size='4' type='text'>
								</div>
							</div>
	  
							<div class='form-row row'>
								<div class='col-xs-12 form-group card required'>
									<label class='control-label'>Card Number</label> <input
										autocomplete='off' class='form-control card-number' placeholder="XXXX-XXXX-XXXX-XXXX"
										type='text'>
								</div>
							</div>
	  
							<div class='form-row row'>
								<div class='col-xs-12 col-md-4 form-group cvc required'>
									<label class='control-label'>CVC</label> <input autocomplete='off'
										class='form-control card-cvc' placeholder='ex. 311' size='4'
										type='password'>
								</div>
								<div class='col-xs-12 col-md-4 form-group expiration required'>
									<label class='control-label'>Expiration Month</label> <input
										class='form-control card-expiry-month' placeholder='MM' size='2'
										type='text'>
								</div>
								<div class='col-xs-12 col-md-4 form-group expiration required'>
									<label class='control-label'>Expiration Year</label> <input
										class='form-control card-expiry-year' placeholder='YYYY' size='4'
										type='text'>
								</div>
							</div>
	  
							<div class='form-row row'>
								<div class='col-md-12 error form-group hide'>
									<div class='alert-danger alert'>Please correct the errors and try
										again.</div>
								</div>
							</div>
	  
							<div class="row">
								<div class="col-xs-12">
									<button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now ({{$booking_request->currency_symbol." ".$booking_request->to_be_paid}})</button>
								</div>
							</div>
							  
						</form>
						
					</div>
				</div>        
			</div>
		</div> 
	</div>
</div>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$(function() {
    var $form         = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
 
        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });
  
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
  
});

String.prototype.toCardFormat = function() {
  return this.replace(/[^0-9]/g, "").substr(0, 16).split("").reduce(cardFormat, "");

  function cardFormat(str, l, i) {
    return str + ((!i || (i % 4)) ? "" : "-") + l;
  }
};

$(document).ready(function() {
  $(".card-number").keyup(function() {
    $(this).val($(this).val().toCardFormat());
  });
});

</script>@stop