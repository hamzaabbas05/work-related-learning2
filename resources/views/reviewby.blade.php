@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">    
        <div class="col-xs-12 col-sm-12 margin_top20">
            <div class="airfcfx-panel panel panel-default">
                <div class="airfcfx-xs-heading-tab-cnt aifcfx-hor-padding airfcfx-padd-top-20 airfcfx-panel panel-heading profile_menu1" style="padding-bottom:0px;">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs review_tab" role="tablist">

                        <li role="presentation"  {!! (Request::is('user/reviews/aboutyou') ? 'class="active"' : '') !!}><a class="airfcfx-tab-heading-btpadding" href="{{url('user/reviews/aboutyou')}}">About You</a></li>
                        <li {!! (Request::is('user/reviews/byyou') ? 'class="active"' : '') !!} role="presentation"><a class="airfcfx-tab-heading-btpadding" href="{{url('user/reviews/byyou')}}">By You</a></li> 
                    </ul>
                </div>



                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="profile">

                        <div class="airfcfx-panel-padding panel-body">
                            <div class="row">                
                                <div class="col-xs-12">
                                    <table class="airfcfx-xs-table margin_bottom0 table table_no_border">
                                        <thead>
                                            <tr class="review_tab">
                                                <th>Sno</th>
                                                <th>Review About</th>
                                                <th>Reviewed Date</th> 
                                                <th>Booking No</th> 
                                                <th>Property Name</th> 
                                                <th>Description</th> 
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php $i = 1;
                                            foreach ($reviewObj as $obj) {
                                                ?> 
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <!--<td class="xs-table-heading">Order Date</td>-->
                                                    <td>{!! $obj->revieweduser->name !!}</td>
                                                    <!--<td class="xs-table-heading">Booking Date</td>-->
                                                    <td>{!! $obj->dateAdded !!}</td>
                                                    <!--<td class="xs-table-heading">Booking No</td>-->
                                                    <td>{!! $obj->enquiry->Bookingno !!}</td>			
                                                    <!--<td class="xs-table-heading">Listings</td>-->
                                                    <td >{!! $obj->property->title !!}</td>
                                                    <!--<td class="xs-table-heading">Listings</td>-->
                                                    <td class="airfcfx-td-transc">{!! $obj->description !!}</td>

                                                </tr>
                                                <?php $i++;
                                            }
                                            ?>
                                        </tbody></table>



                                </div>                           
                            </div> <!--row end --> 
                        </div>





                    </div> <!--#profile end -->



                </div> <!-- tab end -->  

            </div> <!--Panel end -->

        </div></div></div>
@stop