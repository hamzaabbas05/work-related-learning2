@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
<div class="container">
	<div class="row">
    	
        <div class="col-xs-12 margin_top20 margin_bottom20">
        	<div class="col-sm-12">
                <div class="airfcfx-panel panel panel-default">
                  <div class="panel-heading profile_menu1">                  	
                    <select class="airfcfx-message-select form-control" style="width:195px;" onchange="change_message_type();" id="selmessage">
                    <option value="inbox"  >Inbox</option><option value="sent">Sent</option><option value="admin">Admin Messages</option>                    
                    
                    
					
                    </select>                    
                  </div>
                  <div class="airfcfx-panel-body panel-body padding10" id="inboxdiv">
                     @include('messageajax')

 				</div>
                </div>
                    
            </div> <!--col-sm-12 end-->
        </div> <!--col-xs-12 end-->
    	
      
        
    </div> <!-- row end -->

</div> <!-- container end -->
</div>
@stop