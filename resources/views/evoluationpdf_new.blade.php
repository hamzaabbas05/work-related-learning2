<html><head>
	<style type="text/css">
		.cust_main_title {
                font-size: 14pt; font-family: Arial; color: rgb(7, 55, 99); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;
            }
	</style>
</head><body>
<div class="bg_gray1">
	<div class="container">    
		<div class="col-xs-12 col-sm-12 margin_top20">
			<div class="airfcfx-panel panel panel-default">
  			
				<div style="text-align:center">
					<h1 class="cust_main_title">Compulsory Evaluation of the Student - In compliance with Italian law on Work Related Learning</h1>
					
				</div>
          		<div style="margin-left:20px;">
        			
					<p>At the end of the Work Related Learning program you have to express an evaluation about the competences developed by the scholar.  This judgement will be considered when making an overall evaluation of the student by the Italian school he or she is attending. </p> <!-- this is good with no double interline -->
				</div>
          		<div class="airfcfx-panel-padding panel-body" style="margin-left:20px;">
            		<div class="row">
         				<!-- <h2> Student Details </h2> -->
          				<div class="col-xs-12 margin_bottom10">
            				<div class="col-sm-9">
              					<div class="form-group field-resetpasswordform-confirmpassword required">
                            		Name of the Organisation: 
                            		<?php echo ($enquiryObj->property->title != '' ? trim($enquiryObj->property->title).";" : trim($enquiryObj->property->exptitle).";"); ?>
								</div> <!-- here you start addind double interline -->
							</div>
                        </div><br> 
 						<div class="col-xs-12 margin_bottom10">
                        	<div class="col-sm-9">
                             	<div class="form-group field-resetpasswordform-confirmpassword required">
                            		Workplace Address:
									<?php if ($enquiryObj->property->addr_work_org_same == 0) { ?>  
										            <!-- Operative Address -->
										<!-- <div class="divbottom"> -->
											{{$enquiryObj->property->wroute}}
											{{$enquiryObj->property->wstreet_number}}
											{{$enquiryObj->property->wlocality}}
											{{$enquiryObj->property->wadministrative_area_level_1 }}
											{{$enquiryObj->property->wpostal_code}}
											{{$enquiryObj->property->wcountry}}
											{{$enquiryObj->property->location_email}}
											{{$enquiryObj->property->location_telephone}};
										<!-- </div> -->
										<!-- End Legal Address -->

										<?php } else { ?>
										<!-- <div class="divbottom"> -->
											{{$enquiryObj->host->router18}}
											{{$enquiryObj->host->street_numberr18}}
											{{$enquiryObj->host->localityr18}}
											{{$enquiryObj->host->administrative_area_level_1r18 }}
											{{$enquiryObj->host->postal_coder18}}
											{{$enquiryObj->host->countryr18}}
											{{$enquiryObj->host->orgphone}}
											{{$enquiryObj->host->orgemail}};
										<!-- </div> -->
										<?php } ?>
								</div>
							</div>
                        </div> <br> 
                        <div class="col-xs-12 margin_bottom10">
                            <div class="col-sm-9">
                            	<div class="form-group field-resetpasswordform-confirmpassword required">
                        			<span style="float:left">Type of traineeship: </span>
                        			<div style="float:left;width:25%;"> Curricular Traineeship;</div> <br>

								</div> 
							</div>
                        </div><br>
                        <div class="" style="margin: 0px 0px 3px 0px"> 
						<div class="col-xs-3 margin_bottom10 " style="width:28%; float: left;">
		                	<div class="">
		                    	<div class="form-group field-resetpasswordform-newpassword required">
		                    		Starting Date: <?php echo ($enquiryObj->checkin != '' ? date("Y-m-d",strtotime($enquiryObj->checkin)).";" : "");?>
								</div>      
							</div>
						</div> 
                        <div class="col-xs-3 margin_bottom10" style="width:25%; float: left;">
                            <div class="">
                            	<div class="form-group field-resetpasswordform-newpassword required">
                            		Ending Date: <?php echo ($enquiryObj->checkout != '' ? date("Y-m-d",strtotime($enquiryObj->checkout)).";" : "");?>
								</div>
							</div>
                        </div> 
                        <div class="col-xs-3 margin_bottom10" style="width:20%; float: left;">
                            <div class="">
                            	<div class="form-group field-resetpasswordform-newpassword required">
                            		Expected Hours: <?php echo $enquiryObj->property->work_hours.";";?>
								</div>
							</div>
                        </div>

                        <div class="col-xs-3 margin_bottom10" style="width:28%; float: left;">
                            <div class="" style="width: 100%;float: left;">
                            	<div class="form-group field-resetpasswordform-newpassword required" style="float:left;width: 100%;">
                            		<span style="float:left;"> Hours done: _____ ; </span>
								</div>
							</div>
                        </div><br>                         
                    </div><br>
                        <!-- <div class="col-xs-12 margin_bottom10">
                        	<div class="col-sm-9">
                            	<div class="form-group field-resetpasswordform-newpassword required">
                            		<span style="float:left;"> Hours done: </span><div style="float:left;width:25%;border-bottom: solid 1px black;">&nbsp;&nbsp;</div> 
                				</div>
                			</div>
                        </div><br> -->
                        <div class="col-xs-12 margin_bottom10" style="margin-top: 10px;clear:both;">
                        	<div class="col-sm-9">
                            	<div class="form-group field-resetpasswordform-confirmpassword required" style="">
	                            	Name and Surname of Student: 
			                        <?php if (is_null($enquiryObj->user->student_relationship)) {
	                            		$studentName = $enquiryObj->user->name." ".$enquiryObj->user->lastname;
	                            	}else{
	                            		$studentName = $enquiryObj->user->sname16." ".$enquiryObj->user->ssurname16;
	                            	}
	                        		echo $studentName.";";
	                        		?>
	                        	</div>
	                        </div>
                        </div> <br>
                        <div class="col-xs-12 margin_bottom10">
                        	<div class="col-sm-9">
                            	<div class="form-group field-resetpasswordform-confirmpassword required">
	                            	<span style="float:left;">School attended:</span> <?php echo $schoolName.";";?><br>
	                        	</div>
	                        </div>
                        </div>
                        <!--  double interline s as far as this point-->
                      	<ul style="margin: 5px;">
                  			<li style="font-size: 10px;">
                		 		<span style="color:#ff0055;font-size: 11px;">Evaluation 1 - Completion of tasks and behaviour:</span>. The Student: is responsible of the work done: decides  the activities to do with the tutor,  managing properly working times and places; respects the rules and the working environment, does daily  tasks in a responsible way, always paying attention to his/her duties; knows the guideline of the place where he/she works ( including norms on safety ) and the rules of the Work Related Learning project.
		                    	<strong><p style="font-size: 10px;margin:4px">MARK: * not reached / 1 / 2 / 3 / 4  / not observed  - please circle.
		 						</p></strong>
	                       	</li>
	                       	<li style="font-size: 10px;">
								<span style="color:#ff0055;font-size: 11px;">Evaluation 2 - Working skills:</span> The student: finds the structure of the business and acknowledgement system, using applications, software and communication tools to realize simple business observation; is able to read and interpret  the main original documents in order to identify information and make considerations; supports staff in the examination of administrative facts, adopting methods, tools and software used by the company/organisation;  independently does some company surveying; being able to make himself/herself understood, knowing and using specific language (in foreign languages too) adopted in that working context; recognises the roles, the hierarchies and the skills/competences of the different people in the same working environment; knows the structure and the organogram of the company/organisation, the main original documents, the elements of the accounting system, with a particular reference to the bookkeping of purchase and sales, and to the double-entry booking method (if applicable); knows the laws the company must compèly with, the contracts, the typical terms (in foreign languages too) that are used in administrative work. 
								<strong><p style="font-size: 10px;margin:4px">MARK: * not reached / 1 / 2 / 3 / 4  / not observed  - please circle.</p></strong>
							</li>
							<li style="font-size: 10px;">
								<span style="color:#ff0055;font-size: 11px;">Evaluation 3 - Communication skills:</span> The student: knows the English language taking care about the oral exposition and adapting it to the different situations; uses the specific language and a range of vocabulary suitable to communicate in that work context, recognises and figures out  the communicative style considering the interlocutor and the context; knows the best attitude to adopt in  interpersonal relationships and finally is aware of structure, specific lexicon and typical words of the expositive text (the final report).								
								<strong><p style="font-size: 10px;margin:4px">MARK: * not reached / 1 / 2 / 3 / 4  / not observed  - please circle.
 	  								</p></strong>
 	  						</li>
 	  						<li style="font-size: 10px;">
 	  							<span style="color:#ff0055;font-size: 11px;">Evaluation 4 - Proactivity and adaptability:</span>The student: studies, does research, communicates using information and communication technologies, applies operational methodologies shared by other operators of the company, supports operators in carrying out activities, correctly understands the data and documents relating to the hosting structure, knows the organization of activities and operational methods shared by the host structure operators.
 	  							<strong><p style="font-size: 10px;margin:4px">MARK: * not reached / 1 / 2 / 3 / 4  / not observed  - please circle.</p></strong>
							</li>
						</ul>
						<div class="col-xs-12 margin_bottom10">
    						<div class="col-sm-9">
        						<div class="form-group field-resetpasswordform-confirmpassword required">
    								<span style="float:left">Further observations and comment on skills acquired during the experience:</span><br>______________________________________________________________________________________;
			<?php /* <textarea id="further_observe" class="form-control" name="further_observe"   cols="10"    rows="10"  > {{$evaluateError['further_observe'] }}</textarea> 
									<div style="width: 60%;float: right;">&nbsp;&nbsp; </div>*/ ?>
								</div>
							</div>
						</div><br>
						<div class="row" style="width: 100%;">
	  						<div class="col-xs-6 margin_bottom10" style="width: 20%; float: left;">
	                            <div class="">
	                         		<div class="form-group field-resetpasswordform-newpassword required" style="float: left;">
	                            		<div style="float:left;"> Date: <?php echo ($enquiryObj->checkout != '' ? date("Y-m-d",strtotime($enquiryObj->checkout)).";" : "");?></div>
	                            			
									</div>
								</div>
	                        </div>
	                        <div class="col-xs-6 margin_bottom10" style="width:80%; float: right;">
	                            <div class="">
	                         		<div class="form-group field-resetpasswordform-newpassword required">
	                            		Reference Number: <?php echo $enquiryObj->Bookingno.";";?>
									</div>
								</div>
	                        </div>
                    </div>
                        <div class="col-xs-12 margin_bottom10" style="clear:both;" >
                            <div class="col-sm-9">
                         		<div class="form-group field-resetpasswordform-newpassword required" style="float: left;">
                            		<span style="float:left;">Name and surname of the tutor in the company/organisation: </span> <br> ______________________________________________________________________________________;
                            		<?php //echo $enquiryObj->user->school_tutor_name." ".$enquiryObj->user->school_tutor_sur_name;?>
								</div>
							</div>
                        </div>
                    </div> <!--row end -->
          		</div>
            </div>  <!--Panel end -->       
       
       
      <!--div class="panel panel-default">
          <div class="panel-heading profile_menu1">
            <h3 class="panel-title">Login Notifications  </h3>
          </div>
          
          <div class="panel-body">
            	<div class="row margin_top10">
                		<div class="col-xs-12">
                        <div class="checkbox margin_bottom20">
                                <label>
                                  <input type="checkbox">   Turn on login notifications  
                                </label>
                              </div>
                        
                        <p>Login notifications are an extra security feature. When you turn them on, we’ll let you know when anyone logs into your Airbnb account from a new browser. This helps keep your account safe. </p>
                        </div>
                    </div> 
          </div>
          <div class="panel-footer">
          	<div class="text-right"><button class="btn btn_email  ">Save</button></div>
          </div>
          
      </div-->  <!--Panel end -->  
        
      </div> <!--col-sm-9 end -->
        
    </div> <!--container end -->
</div>
</body></html>
<?php //echo "END HERE";exit; ?>