@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
      <div class="container">    
      @include('partials.account_sidebar')
        
        <div class="col-xs-12 col-sm-9 margin_top20">
   		 <div class="airfcfx-panel panel panel-default">
                      <div class="airfcfx-xs-heading-tab-cnt aifcfx-hor-padding airfcfx-padd-top-20 airfcfx-panel panel-heading profile_menu1" style="padding-bottom:0px;">
                        <!-- Nav tabs -->
              <ul class="nav nav-tabs review_tab" role="tablist">
				
                <li role="presentation"  {!! (Request::is('user/transactions/completed') ? 'class="active"' : '') !!}><a class="airfcfx-tab-heading-btpadding" href="{{url('user/transactions/completed')}}">Completed Transaction</a></li>
                <li {!! (Request::is('user/transactions/pending') ? 'class="active"' : '') !!} role="presentation"><a class="airfcfx-tab-heading-btpadding" href="{{url('user/transactions/pending')}}">Pending Transaction</a></li> 
                         </ul>
                      </div>
    
              
            
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="profile">
				
                      <div class="airfcfx-panel-padding panel-body">
                       <div class="row">                
                                <div class="col-xs-12">
                                <table class="airfcfx-xs-table margin_bottom0 table table_no_border">
                                	<thead>
                                      <tr class="review_tab">
                                        <th>Order Date</th>
                                        <th>Booking Date</th> 
                                        <th>Booking No </th>                                       
                                        <th>Listings</th>                                        
                                        <th>Amount</th>
                                        <th>Status</th>
										 
                                      </tr>
                                    </thead>
										<tbody>

<?php foreach($paymentObj as $obj) { ?> 
										<tr><td class="xs-table-heading">Order Date</td><td>{!! $obj->modified !!}</td>
												<td class="xs-table-heading">Booking Date</td><td>{!! $obj->rentals->dateAdded !!}</td>
<td class="xs-table-heading">Booking No</td><td>{!! $obj->rentals->Bookingno !!}</td>			
 											<td class="xs-table-heading">Listings</td><td class="airfcfx-td-transc">{!! $obj->property->title !!}</td>
												<td class="xs-table-heading">Amount</td><td>{!! $obj->price !!}</td>
												<td class="xs-table-heading">Status</td><td>{!! $obj->status !!}</td>
												<!--<td class="xs-table-heading">View</td><td align="center"><a href=""><i class="airfcfx-red-icon fa fa-search"></i></a></td>-->
												</tr>
												<?php } ?>
 							 </tbody></table>
                                
                                                            
						                           
                                </div>                           
                             </div> <!--row end --> 
                      </div>
                      
                    
                    
                     
                   
                </div> <!--#profile end -->
                

                
                 </div> <!-- tab end -->  
                	
                </div> <!--Panel end -->
       
      </div></div></div>
      @stop