@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<style type="text/css">
    .tripdesign td {
        border-right: 1px solid #ddd !important;
    }

</style>
<div class="bg_gray1">
    <div class="container" style="width: 92%;">  

        @include('partials.listing_sidebar')
        <div class="col-xs-12 col-sm-10 margin_top20">        
            <div class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel-padding airfcfx-panel panel-heading profile_menu1">
                    <h3 class="panel-title">Your Reservations</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="trips-cnt margin_top10 margin_bottom20 paddingleft table-responsive col-sm-12 col-lg-12">
                            <img  id="statuschange" src="{{asset('frontendassets/images/load.gif')}}" class="loading" style="display:none;margin-top:5px;margin-left:600px;"> 
                            <table class="table table_no_border tripdesign">
                                <thead>
                                    <tr class="review_tab">
                                        <th style="width: 6%;">Sno</th>
                                        <th style="width: 14%;">Dates</th>
                                        <th style="width: 30%;">Listing Details</th>
                                        <th style="width: 10%;">Booking No</th>
                                        <th style="width: 10%;">First Choice</th>
                                        <th style="width: 10%;">Approval Status</th>
                                        <th style="width: 10%;">Approve/Decline</th>
                                        <th style="width: 10%;">Message / See Profile</th>
                                    </tr>
                                </thead>
                                <tbody> 

                                    <?php
                                    $i = 1;
                                    foreach ($tripsObj as $trips) {


                                        $class = "successtxt";

                                        if ($trips->booking_status == 'Pending') {
                                            $class = "text-danger";
                                        } if ($trips->booking_status == 'Cancelled') {
                                            $class = "text-medium";
                                        }
                                        $class1 = "successtxt";
                                        if ($trips->approval == 'Pending') {
                                            $class1 = "text-danger";
                                        } if ($trips->approval == 'Decline') {
                                            $class1 = "text-medium";
                                        }

                                        $id = "reserve" . $trips->id;
                                        if (isset($trips->property->property_status) && $trips->property->property_status != "paid") {
                                            ?> 
                                            <tr id="{{$id}}">
                                                <td style="border-left: 1px solid #ddd !important;">{{$i}}</td>
                                                <td class="airfcfx-breakword">
                                                    <p class="airfcfx-td-dtnloc-trp" style="width: 100%">
                                                        {{date("F jS, Y", strtotime($trips->checkin))}} <br /> 
                                                        TO <br />
                                                        {{date("F jS, Y", strtotime($trips->checkout))}}
                                                    </p> 
                                                </td>

                                                <td class="airfcfx-breakword">
                                                    <div><span style="color:#330099">Title:</span>{{$trips->property->exptitle}}</div>
                                                    <div><span style="color:#330099">{{!empty($trips->property->represent_name)?"WorkTutor":"User Listing"}}:</span> {{!empty($trips->property->represent_name)?$trips->property->represent_name:$trips->property->user->name}}</div>
                                                    <?php $ssid = $trips->user->id; ?>
                                                    <div><span style="color:#330099">Student:</span><a href='{{url("viewuser/$ssid")}}'>
                                                            <?php echo (!is_null($trips->user->student_relationship)) ? $trips->user->sname16 : $trips->user->name; ?></a></div>
                                                </td>

                                                <td align="center">{{$trips->Bookingno}}</td>
                                                <td align="center">
                                                    <?php
                                                    //$user_rentalenq = App\RentalsEnquiry::where('id', '!=', $trips->id)->where('user_id', $trips->user_id)->get();
                                                    //$availableCheck = App\RentalsEnquiry::getEnquiryDates($user_rentalenq, $trips->checkin, $trips->checkout);
                                                    //echo "user id = " . $trips->user_id . " - allow =" . $availableCheck . " - ";
                                                    //if ($availableCheck == 0) {
                                                    //echo "First Choice";
                                                    //} else {
                                                    //echo "2nd or 3rd Choice";
                                                    //}
                                                    //echo "user_id = ".$trips->user_id;
                                                    echo App\RentalsEnquiry::checkFirstChoice($trips->user_id, $trips->id, "unpaid");
                                                    ?>

                                                </td>
                                                <td class="airfcfx-min-width-80px">
                                                    <p class="{{$class1}}">
                                                        <b>
                                                            <?php

                                                            $newDateStart = $trips->checkin;
                                                            $newDateEnd = $trips->checkout;
                                                            $availableCheck = App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $trips->prd_id);
                                                             $DisableCalDates = App\Schedule::where('id', $trips->property->id)->get();

                                                            $disable_dates = array();
                                                            if(count($DisableCalDates) > 0){
                                                                foreach (json_decode($DisableCalDates[0]->data) as $disable_date=>$schedule) {
                                                                    $exploaded_str = explode(" ", $disable_date);
                                                                    $disable_dates[] = $exploaded_str[0]; 
                                                                } 
                                                            }
                                                             
                                                          
                                                           $exploaded_start_date = explode(" ", $newDateStart);
                                                           $exploaded_end_date = explode(" ", $newDateEnd);
                                                            // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                                                            //  echo "<pre>";
                                                             //    echo "Start Date : ".$newDateStart."<br> End Date : ".$newDateEnd."<br>";
                                                               //     print_r($disable_dates);die;
                                                            //     echo "</pre>";
                                                            // }
                                                            if($trips->booking_status == 'Pending' && $trips->approval == 'Pending'){
                                                                 $userAllreadyAccpeted = App\RentalsEnquiry::userAllreadyAccpetedForUnpaid($newDateStart, $newDateEnd, $trips->user_id); 
                                                                if($userAllreadyAccpeted){
                                                                     echo "User already taken somewhere else.";
                                                                 }else{
                                                                    if(count($availableCheck) > 0){
                                                                        echo "You have already you Approved one Request with overlapping dates.";
                                                                    }else{
                                                                        echo $trips->approval;
                                                                    }
                                                                }
                                                                
                                                            }else{
                                                                if ($trips->approval == "Accept") {
                                                                    echo "Accepted <br> ";
                                                                    //echo "Student Guest already taken.";
                                                                } elseif($trips->approval == "Unavailable"){
                                                                          echo "You are not available these dates.";
                                                                }else{
                                                                    echo $trips->approval;
                                                                }               
                                                            }
                                                            ?>
                                                        </b>

                                                    </p>
                                                </td>
                                                <td>
                                                    <?php
                                                    $check_in_date = date("Y-m-d", strtotime($trips->checkin));
                                                    $current_date = date("Y-m-d");

                                                    if ($check_in_date >= $current_date) {
                                                        ?>
                                                        <?php if ($trips->approval == "Pending") { ?> 
                                                            <?php
                                                    // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                                                        if($trips->booking_status == 'Pending' && $trips->approval == 'Pending'){

                                                        $newDateStart = $trips->checkin;
                                                        $newDateEnd = $trips->checkout;
                                                        $availableCheck = App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $trips->prd_id);

                                                        // echo "<br> Is available :".count($availableCheck)."<br>";
                                                        if(count($availableCheck) > 0){
                                                            echo "";
                                                            // echo "You have already you Approved one Request with overlapping dates.";
                                                        }else{ ?>

                                                            <button type="button" class="btn btn-responsive btn-success" onclick="approvedecline('Accept', '{{$trips->id}}')" style="background-color: #5cb85c !important;">Accept</button>
                                                            <br />
                                                            <button type="button" class="btn btn-responsive btn-primary" onclick="approvedecline('Decline', '{{$trips->id}}')" style="margin-top: 10px;">Decline</button>
                                                             <button type="button" class="btn btn-responsive btn-primary" onclick="notavailable('{{$newDateStart}}','{{$newDateEnd}}', '{{$trips->property->id}}','{{$trips->id}}')" style="margin-top: 10px;">I'm not available these date</button>                                                            
                                                        <?php }
                                                        // echo "<pre>";
                                                        //     // print_r($availableCheck);
                                                        //     print_r($trips);
                                                        // echo "</pre>";
                                                        }

                                                    /*}else{                                                        
                                                        ?>
                                                            <button type="button" class="btn btn-responsive btn-success" onclick="approvedecline('Accept', '{{$trips->id}}')" style="background-color: #5cb85c !important;">Accept</button>
                                                            <br />
                                                            <button type="button" class="btn btn-responsive btn-primary" onclick="approvedecline('Decline', '{{$trips->id}}')" style="margin-top: 10px;">Decline</button>

                                                        <?php }*/ ?>
                <!--                                                        <select onchange="approvedecline(this.value, {{$trips->id}})">
                                                                <option value="">Select Status</option>
                                                                <option <?php //if ($trips->approval == "Accept") {         ?> selected <?php //}         ?> value="Accept">Accept</option>
                                                                <option <?php //if ($trips->approval == "Decline") {         ?> selected <?php //}         ?>  value="Decline">Decline</option>
                                                            </select> -->
                                                        <?php } else { ?> 
                                                            <p class="{{$class1}}">
                                                                <b>
                                                                    <?php
                                                                    if ($trips->approval == "Accept") {
                                                                        echo "Accepted";
                                                                    } else {
                                                                        echo $trips->approval." -- ";
                                                                    }
                                                                    ?>
                                                                </b>
                                                            </p>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <p class="{{$class1}}"><b>this is an old request</b></p>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <a target="_blank" href='{{url("user/conversation/$trips->id")}}'>Send Message</a> | 
                                                    <a target="_blank" href='{{url("viewuser/$ssid")}}'>Student Profile</a> 
                                                </td>
                                            </tr>

                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>

                                </tbody></table></div>                          

                    </div> <!--row end -->

                </div>

            </div> <!--Panel end -->





        </div>


    </div>
</div>@stop