@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">  

        @include('partials.listing_sidebar')
        <div class="col-xs-12 col-sm-10 margin_top20">        
            <div class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel-padding airfcfx-panel panel-heading profile_menu1">
                    <h3 class="panel-title">Your Reservations</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="trips-cnt margin_top10 margin_bottom20 paddingleft table-responsive col-sm-12 col-lg-12">
                            <img  id="statuschange" src="{{asset('frontendassets/images/load.gif')}}" class="loading" style="display:none;margin-top:5px;margin-left:600px;"> 
                            <table class="table table_no_border tripdesign">
                                <thead>
                                    <tr class="review_tab">
                                        <th>Sno</th>
                                        <th>Dates</th>
                                        <th>Listing Details</th>
                                        <th>Booking No</th>
                                        <th>Approval Status</th>
                                        <th>Approve/Decline</th>
                                        <th>Messaging</th>
                                    </tr>
                                </thead>
                                <tbody> 

                                    <?php
                                    $i = 1;
                                    foreach ($tripsObj as $trips) {
                                        $class = "successtxt";

                                        if ($trips->booking_status == 'Pending') {
                                            $class = "text-danger";
                                        } if ($trips->booking_status == 'Cancelled') {
                                            $class = "text-medium";
                                        }
                                        $class1 = "successtxt";
                                        if ($trips->approval == 'Pending') {
                                            $class1 = "text-danger";
                                        } if ($trips->approval == 'Decline') {
                                            $class1 = "text-medium";
                                        }

                                        $id = "reserve" . $trips->id;
                                        ?> 
                                        <tr id="{{$id}}">
                                            <td>{{$i}}</td>
                                            <td class="airfcfx-breakword"><p class="airfcfx-td-dtnloc-trp">
                                                    {{date("F jS, Y", strtotime($trips->checkin))}} TO {{date("F jS, Y", strtotime($trips->checkout))}}</p> 
                                            </td>

                                            <td class="airfcfx-breakword">
                                                <div><span style="color:#330099">Listing Title: </span>{{$trips->property->exptitle}}</div>
                                                <div><span style="color:#330099">WorkTutor :</span> {{$trips->property->represent_name}}</div><?php $ssid = $trips->user->id; ?>
                                                <div><span style="color:#330099">Name of Student : </span><a href='{{url("viewuser/$ssid")}}'>
                                                        <?php echo (!is_null($trips->user->student_relationship)) ? $trips->user->sname16 : $trips->user->name; ?></a></div>
                                            </td>

                                            <td align="center">{{$trips->Bookingno}}</td>

                                            <td class="airfcfx-min-width-80px">
                                                <p class="{{$class1}}"><b>{{$trips->approval}}</b></p>
                                            </td>

                                            <td>
                                                {!! Form::model('', ['action' => 'RentalsController@updatebookingstatus']) !!}
                                                <?php if ($trips->approval == "Pending") { ?> 
                                                    <select name="status">
                                                        <option value="">Select Status</option>
                                                        <option <?php if ($trips->approval == "Accept") { ?> selected <?php } ?> value="Accept">Accept</option>
                                                        <option <?php if ($trips->approval == "Decline") { ?> selected <?php } ?>  value="Decline">Decline</option>
                                                    </select> <?php } else { ?> 
                                                    <p class="{{$class1}}"><b>{{$trips->approval}}</b></p>
                                                <?php } ?>
                                                <input type="hidden" name="enquiryid" value="{{$trips->id}}" />
                                                <input type="submit" name="submit" value="Test" />
                                                {!! Form::close() !!}
                                                <?php if ($trips->approval == "Pending") { ?> 
                                                    <select onchange="approvedecline(this.value, {{$trips->id}})">
                                                        <option value="">Select Status</option>
                                                        <option <?php if ($trips->approval == "Accept") { ?> selected <?php } ?> value="Accept">Accept</option>
                                                        <option <?php if ($trips->approval == "Decline") { ?> selected <?php } ?>  value="Decline">Decline</option>
                                                    </select> <?php } else { ?> 
                                                    <p class="{{$class1}}"><b>{{$trips->approval}}</b></p>
                                                <?php } ?>
                                            </td>
                                            <td><a target="_blank" href='{{url("user/conversation/$trips->id")}}'>Send Message</a> </td>
                                        </tr>

                                        <?php $i++;
                                    } ?>

                                </tbody></table></div>                          

                    </div> <!--row end -->

                </div>

            </div> <!--Panel end -->





        </div>


    </div>
</div>@stop