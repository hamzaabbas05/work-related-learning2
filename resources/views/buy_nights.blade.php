 <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- <title>Laravel</title> -->
    </head>
    
@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<style type="text/css">
    .stripe-button-el{display: none;}
</style>
<div class="bg_gray1">
    <div class="container">  
        <div class="col-xs-12 col-sm-12 margin_top20">        

            <div class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel-padding airfcfx-panel panel-heading profile_menu1">
                    <h3 class="panel-title">Buy Nights</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="trips-cnt margin_top10 margin_bottom20 paddingleft table-responsive col-sm-12 col-lg-12">
                            <table class="table table_no_border tripdesign">
                                <tbody>
                                    <tr>
                                    <td>
                                        <form action="{{url('user/nightCheckout')}}" method="POST">
                                        {{ csrf_field() }}
                                            <!-- data-key="pk_test_pIaGoPD69OsOWmh1FIE8Hl4J" -->
                                            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="{{env('STRIPE_KEY')}}"
                                            data-amount="17650"
                                            data-name="Stripe Demo"
                                            data-description="Online course about integrating Stripe"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-locale="auto"
                                            data-currency="{{env('STRIPE_CURRENCY')}}">
                                            </script>
                                            <input type="hidden" name="amount" value="17650" id="7amounts" class="amount">
                                            <input type="hidden" name="nights" value="7" id="7nights" class="nights">
                                            <input type="submit" class="btn" value="Buy 7 Nights" name="buy_nights">
                                        </form>
                                    </td>
                                    <td>
                                        <form action="{{url('user/nightCheckout')}}" method="POST">
                                        {{ csrf_field() }}            <!-- data-key="pk_test_pIaGoPD69OsOWmh1FIE8Hl4J" -->
                                            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="{{env('STRIPE_KEY')}}"
                                            data-amount="35300"
                                            data-name="Stripe Demo"
                                            data-description="Online course about integrating Stripe"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-locale="auto"
                                            data-currency="{{env('STRIPE_CURRENCY')}}">
                                            </script>
                                            <input type="hidden" name="amount" value="35300" id="14amount" class="amount">
                                            <input type="hidden" name="nights" value="14" id="14nights" class="nights">
                                            <input type="submit" class="btn" value="Buy 14 Nights" name="buy_nights">
                                        </form>
                                    </td>
                                    <td>
                                        <form action="{{url('user/nightCheckout')}}" method="POST">
                                        {{ csrf_field() }}            <!-- data-key="pk_test_pIaGoPD69OsOWmh1FIE8Hl4J" -->
                                            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="{{env('STRIPE_KEY')}}"
                                            data-amount="40300"
                                            data-name="Stripe Demo"
                                            data-description="Online course about integrating Stripe"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-locale="auto"
                                            data-currency="{{env('STRIPE_CURRENCY')}}">
                                            </script>
                                            <input type="hidden" name="amount" value="40300" id="21amount" class="amount">
                                            <input type="hidden" name="nights" value="21" id="21nights" class="nights">
                                            <input type="submit" class="btn" value="Buy 21 Nights" name="buy_nights">
                                        </form>                                    
                                    <td>
                                        <!-- <form name="frm_to_buy" method="POST" action=""> -->
                                            <button class="btn" onClick="alert('You can send details on this email esperienzainglese@gmail.com');">More Nights</button>
                                        <!-- </form> -->
                                        </td>
                                    </tr>
                                    <?php
                                    
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div> <!--row end -->
                </div>
            </div> <!--Panel end -->
        </div>
    </div>
</div>@stop