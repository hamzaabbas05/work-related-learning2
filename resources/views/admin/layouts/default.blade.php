<!DOCTYPE html>
<html>

<head>

@inject('settings', 'App\Settings')
<?php $settingsObj = $settings->settingsInfo();
$logoname = $settingsObj->logo; ?>
    <meta charset="UTF-8">
    <title>
        @section('title')
            Admin Panel
        @show
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyAz0HqeAgq5xNt9DSUSMPIVlmLK_uuAoDc"></script> -->
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyA0BQbSzrX8W4conO6nmF9-LzpH4R-y1jU"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- font Awesome -->
    <link href="{{ asset('assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/styles/black.css') }}" rel="stylesheet" type="text/css" id="colorscheme"/>
    <link href="{{ asset('assets/css/panel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/metisMenu.css') }}" rel="stylesheet" type="text/css"/>
<script>
var baseurl='<?php echo url();?>';
</script> 

    <!-- end of global css -->
    <!--page level css-->
    @yield('header_styles')
            <!--end of page level css-->
</head>

<body class="skin-josh">
<header class="header">
    <a href="" class="logo">
        <img src="{{ asset('images/logo/'.$logoname) }}" style="height:40px;" alt="logo">
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <div>
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <div class="responsive_nav"></div>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
           <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        
                            <?php /*<img style="height:40px;"  src="{{asset('images/profile/'.Auth::user()->profile_img_name)}} " width="35"
                                 class="img-circle img-responsive pull-left" height="35" alt="riot"> */?>
                        
                        <div class="riot">
                            <div>
                               {{Auth::user()->name}}
                                <span>
                                        <i class="caret"></i>
                                    </span>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                           
                                <?php /*<img   src="{{asset('images/profile/'.Auth::user()->profile_img_name)}}"
                                     class="img-responsive img-circle" alt="User Image">*/ ?>
                           
                            <p class="topprofiletext"></p>
                        </li>
                        <!-- Menu Body -->
                         
                        <li>
                            <a href="{{ URL::to('admin/profile') }}">
                                <i class="livicon" data-name="gears" data-s="18"></i>
                                Profile
                            </a>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="{{ URL::to('logout') }}">
                                    <i class="livicon" data-name="sign-out" data-s="18"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <section class="sidebar ">
            <div class="page-sidebar  sidebar-nav">
                <!-- BEGIN SIDEBAR MENU -->
                <ul id="menu" class="page-sidebar-menu">
                    <li {!! (Request::is('admin/dashboard') ? 'class="active"' : '') !!}>
                        <a href="{{url('admin/dashboard')}}">
                            <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                               data-loop="true"></i>
                            <span class="title">Dashboard</span>
                        </a>
                    </li>
                     <li {!! (Request::is('admin/profile') || Request::is('admin/profile/password') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="medal" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                               data-loop="true"></i>
                            <span class="title">Profile Management</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/profile') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/profile')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Profile Management
                                </a>
                            </li>
                            <li {!! (Request::is('admin/profile/password') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/profile/password')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Passwords
                                </a>
                            </li>
                         </ul>
                    </li> 
                    <!-- <li {!! (Request::is('admin/roles') || Request::is('admin/permissions') || Request::is('admin/subadmin') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="medal" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                               data-loop="true"></i>
                            <span class="title">SubAdmin Management</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/roles') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/roles')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Roles
                                </a>
                            </li>
                            <li {!! (Request::is('admin/permissions') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/permissions')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Permissions
                                </a>
                            </li>
                             <li {!! (Request::is('admin/subadmin') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/subadmin')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Subadmins
                                </a>
                            </li>
                         </ul>
                    </li>  -->
                  <li {!! (Request::is('admin/settings/general') || Request::is('admin/settings/email') || Request::is('admin/settings/socialmedia') || Request::is('admin/settings/noimage') || Request::is('admin/settings/app') || Request::is('admin/settings/payment') || Request::is('admin/settings/propertyattributes') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="medal" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                               data-loop="true"></i>
                            <span class="title">Site Management</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/settings/general') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/settings/general')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    General Settings
                                </a>
                            </li>
                            <li {!! (Request::is('admin/settings/email') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/settings/email')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Email & Contact Settings
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/settings/socialmedia')) && !(Request::is('admin/users/create')) ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/settings/socialmedia')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Social Media settings
                                </a>
                            </li>
                            <li {!! (Request::is('admin/settings/app') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/settings/app')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    API Credentials
                                </a>
                            </li>
                             <li {!! (Request::is('admin/settings/payment') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/settings/payment')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Payment Settings
                                </a>
                            </li>
                             <li {!! (Request::is('admin/settings/noimage') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{url('admin/settings/noimage')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    No_Image Settings
                                </a>
                            </li>
                             <li {!! (Request::is('admin/settings/propertyattributes') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/settings/propertyattributes') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Work-Experience Settings
                                </a>
                            </li>
                        </ul>
                    </li>

                      <li {!! (Request::is('admin/user/manage') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">User  Management<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/user/manage') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/user/manage') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Users
                                </a>
                            </li>
                      </ul>
                    </li>
                      <li {!! (Request::is('admin/user/trust') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">User  Verifications<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/user/trust') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/user/trust') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Users Verifications
                                </a>
                            </li>
                      </ul>
                    </li>
                      
                     <li {!! (Request::is('admin/property/*') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">WorkExperience  Management<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/property/manage') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/property/manage') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage WorkExperience
                                </a>
                            </li>
                            <li {!! (Request::is('admin/property/managePaid') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/property/managePaid') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage WorkExperience Paid
                                </a>
                            </li>
                            <li {!! (Request::is('admin/property/manageDistanceHomeWork') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/property/manageDistanceHomeWork') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Distances Home->Work
                                </a>
                            </li>
                            <!-- <li {!! (Request::is('admin/property/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/property/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                   Add New
                                </a>
                            </li> -->
                     
                           
                        </ul>
                    </li>

                     <li {!! (Request::is('admin/school') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">School  Management<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/school') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/school') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Schools
                                </a>
                            </li>
                            <li {!! (Request::is('admin/school/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/school/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                   Add New School
                                </a>
                            </li>
                     
                           
                        </ul>
                    </li>
                     <li  {!! (Request::is('admin/voucherCredit') ? 'class="active"' : '') !!} {!! (Request::is('admin/voucherCredit/*') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Voucher Credit Management<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/voucherCredit') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/voucherCredit') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage VoucherCredit
                                </a>
                            </li>
                            <li {!! (Request::is('admin/voucherCredit/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/voucherCredit/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                   Add VoucherCredit for NEW User Not PreEnrolled
                                </a>
                            </li>
                             <li {!! (Request::is('admin/voucherCredit/createforpreenroll') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/voucherCredit/createforpreenroll') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                   Add VoucherCredit for NEW Pre-Enrolled
                                </a>
                            </li>
                            <li {!! (Request::is('admin/voucherCredit/assignUser') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/voucherCredit/assignUser') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                   Assign User
                                </a>
                            </li>
                     
                           
                        </ul>
                    </li>
                     <li  {!! (Request::is('admin/voucher') ? 'class="active"' : '') !!} {!! (Request::is('admin/voucher/*') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Voucher  Management<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/voucher') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/voucher') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Vouchers
                                </a>
                            </li>
                            <li {!! (Request::is('admin/voucher/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/voucher/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                   Add New Voucher
                                </a>
                            </li>
                     
                           
                        </ul>
                    </li>
                    
                     <li {!! (Request::is('admin/groups/management') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Groups  Management<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/groups/management') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/groups/management') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Groups
                                </a>
                            </li>                           
                        </ul>
                    </li>
                     <li {!! (Request::is('admin/bookedJobs/*') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Manage Bookings<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/bookedJobs/accept') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/bookedJobs/accept') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Accept Booking
                                </a>
                            </li>                        
                            <li {!! (Request::is('admin/bookedJobs/pending') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/bookedJobs/pending') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Pending Booking
                                </a>
                            </li>                        
                        </ul>
                    </li>
                    <li @if (Request::is('admin/schoolGroups') or Request::is('admin/schoolGroups/*')) class="active" @endif>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Secondary School Groups<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li @if (Request::is('admin/schoolGroups') or Request::is('admin/schoolGroups/*')) class="active" @endif>
                                <a href="{{ URL::to('admin/schoolGroups/') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage School Groups
                                </a>
                            </li>
                            <li {!! (Request::is('admin/schoolGroups/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/schoolGroups/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                   Add New School Group
                                </a>
                            </li>
                            <li {!! (Request::is('admin/schoolGroups/studentToGroup') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/schoolGroups/studentToGroup') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                   Add Student to Group
                                </a>
                            </li>
                      </ul>
                    </li>
                     <li {!! (Request::is('admin/booking/generator') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Booking Generator<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/booking/generator') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/booking/generator') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Generator Booking - Pending
                                </a>
                            </li>
                            <li {!! (Request::is('admin/booking/paidgenerator') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/booking/paidgenerator') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Generator Booking Paid - Pending
                                </a>
                            </li>
                            <li {!! (Request::is('admin/booking/generator') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/booking/generator/accept') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Generator Booking - Accept
                                </a>
                            </li>                           
                        </ul>
                    </li>
                    <li {!! (Request::is('admin/paidlisting') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Transferwise List<span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/paidlisting') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/paidlisting') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    View Transferwise List
                                </a>
                            </li>                           
                        </ul>
                    </li>

                   <!--  <li {!! (Request::is('admin/payment') || Request::is('admin/payment/Paid')  || Request::is('admin/payment/Pending')  ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="image" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                               data-loop="true"></i>
                            <span class="title">Evaluation Management</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/payment') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/payment') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Task Completed
                                </a>
                            </li>
                            <li {!! (Request::is('admin/payment/Paid') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/payment/Paid') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Paid Payments
                                </a>
                            </li>

                             <li {!! (Request::is('admin/payment/Pending') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/payment/Pending') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Pending Payments
                                </a>
                            </li>
                            
                             
                        </ul>
                    </li> -->

                    <li {!! (Request::is('admin/reservation') ||  Request::is('admin/reservation/Pending') || Request::is('admin/reservation/New')  || Request::is('admin/reservation/Accept')  ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="image" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                               data-loop="true"></i>
                            <span class="title">Reservation Manage</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/reservation') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/reservation/all') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                 All Reservations
                                </a>
                            </li>
                             <li {!! (Request::is('admin/reservation/New') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/reservation/New') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    New Reservations
                                </a>
                            </li> 
                            <li {!! (Request::is('admin/reservation/Pending') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/reservation/Pending') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Pending Reservations
                                </a>
                            </li>
                            

                             <li {!! (Request::is('admin/reservation/Accept') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/reservation/Accept') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Approved Reservations
                                </a>
                            </li> 

                            
                             
                        </ul>
                    </li>


                    <li {!! (Request::is('admin/review')  ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="image" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                               data-loop="true"></i>
                            <span class="title">Review Management</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/review') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/review') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Reviews
                                </a>
                            </li>
                           
 
                             
                        </ul>
                    </li>




                     <!--  <li {!! (Request::is('admin/commission') || Request::is('admin/commission/create') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="image" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                               data-loop="true"></i>
                            <span class="title">Commission Manage</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/commission') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/commission') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Commission
                                </a>
                            </li>
                            <li {!! (Request::is('admin/commission/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/commission/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Add Commission
                                </a>
                            </li>
                            
                             
                        </ul>
                    </li> -->

                    <!-- <li {!! (Request::is('admin/commissiontrack') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="image" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                               data-loop="true"></i>
                            <span class="title">Commission Tracking</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/commissiontrack') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/commissiontrack') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Commission Tracking
                                </a>
                            </li>
                            
                            
                             
                        </ul>
                    </li>
 -->

                <!--  <li {!! (Request::is('admin/propertytype') || Request::is('admin/roomtype') || Request::is('admin/cancellation') || Request::is('admin/propertygeneral') || Request::is('admin/amenities') || Request::is('admin/specialattributes') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Property Attributes</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/settings/propertyattributes') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/settings/propertyattributes') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    General Attributes
                                </a>
                            </li>
                            <li {!! (Request::is('admin/propertytype') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/propertytype') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Property Types
                                </a>
                            </li>
                            <li {!! (Request::is('admin/roomtype') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/roomtype') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Room Types
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/cancellation')) && !(Request::is('admin/users/create')) ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/cancellation') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Cancellation
                                </a>
                            </li>
                            <li {!! (Request::is('admin/amenities') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/amenities') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Amenities
                                </a>
                            </li>
                            <li {!! (Request::is('admin/specialattributes') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/specialattributes') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Special Attributes
                                </a>
                            </li>
                        </ul>
                    </li> -->
                    <li {!! (Request::is('admin/banner') || Request::is('admin/banner/create') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="image" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                               data-loop="true"></i>
                            <span class="title">Banner Management</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/banner') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/banner') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Banners
                                </a>
                            </li>
                            <li {!! (Request::is('admin/banner/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/banner/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Add Banner
                                </a>
                            </li>
                            
                             
                        </ul>
                    </li>
                     <!--  <li {!! (Request::is('admin/destination') || Request::is('admin/destination/create') ? 'class="active"' : '') !!}>
                        <a href="#">
                           <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Destination Control</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/destination') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/destination') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Destinations
                                </a>
                            </li>
                            <li {!! (Request::is('admin/destination/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/destination/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Add Destination
                                </a>
                            </li>
                            
                             
                        </ul>
                    </li> -->
                   <!--  <li {!! (Request::is('admin/homedestination') || Request::is('admin/homedestination/create') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="image" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                               data-loop="true"></i>
                            <span class="title">Destination Banners</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/homedestination') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/homedestination') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage HomeBanners
                                </a>
                            </li>
                            <li {!! (Request::is('admin/homedestination/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/homedestination/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Add Destination Banner
                                </a>
                            </li>
                            
                             
                        </ul>
                    </li> -->
                     <li {!! (Request::is('admin/faq') || Request::is('admin/faq/create') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="flag" data-c="#418bca" data-hc="#418bca" data-size="18" data-loop="true"></i>
                               
                            <span class="title">FAQ Management</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/faq') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/faq') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage FAQ
                                </a>
                            </li>
                            <li {!! (Request::is('admin/faq/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/faq/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Add FAQ
                                </a>
                            </li>
                            
                             
                        </ul>
                    </li>
                     <li {!! (Request::is('admin/language') || Request::is('admin/language/create') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Language Management</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/language') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/language') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Languages
                                </a>
                            </li>
                            <li {!! (Request::is('admin/language/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/language/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Add Language
                                </a>
                            </li>
                            
                             
                        </ul>
                    </li>
                    <!-- <li {!! (Request::is('admin/currency') || Request::is('admin/currency/create') ? 'class="active"' : '') !!}>
                        <a href="#">
                           <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#5bc0de" data-hc="#5bc0de"
                               data-loop="true"></i>
                            <span class="title">Currency Management</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/currency') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/currency') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage Currencies
                                </a>
                            </li>
                            <li {!! (Request::is('admin/currency/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/currency/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Add Currency
                                </a>
                            </li>
                            
                             
                        </ul>
                    </li> -->

                     <li {!! (Request::is('admin/cms') || Request::is('admin/cms/create') ? 'class="active"' : '') !!}>
                        <a href="#">
                              <i class="livicon" data-name="flag" data-c="#418bca" data-hc="#418bca" data-size="18" data-loop="true"></i>
                           
                            <span class="title">CMS Management</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/cms') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/cms') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manage CMS
                                </a>
                            </li>
                            <li {!! (Request::is('admin/cms/create') ? 'class="active" id="active"' : '') !!}>
                                <a href="{{ URL::to('admin/cms/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Add Page
                                </a>
                            </li>
                            
                             
                        </ul>
                    </li>




                  <!-- Menus generated by CRUD generator -->
                    @include('admin/layouts/menu')
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
        </section>
    </aside>
    <aside class="right-side">

        <!-- Notifications -->
        @include('partials.flashnotification')

                <!-- Content -->
        @yield('content')

    </aside>
    <!-- right-side -->
</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<!-- global js -->



<script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<link href="{{ asset('frontendassets/css/jquery-ui.min.css')}}" rel="stylesheet">
<script  type="text/javascript" src="{{asset('frontendassets/js/jquery-ui.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        var dateFormat = "mm/dd/yy";
        $("#startdate").datepicker({minDate: 0}).on( "change", function() {
          $("#enddate").datepicker( "option", "minDate", getDate( this ) );
        });
        $("#enddate").datepicker({minDate: 0}).on( "change", function() {
            $("#startdate").datepicker( "option", "maxDate", getDate( this ) );
          });
         function getDate( element ) {
            var date;
            try {
              date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
              date = null;
            }

            return date;
          }
    });
</script>
@if (Request::is('admin/form_builder2') || Request::is('admin/gridmanager') || Request::is('admin/portlet_draggable') || Request::is('admin/property/calendar/*'))
    <script src="{{ asset('assets/vendors/form_builder1/js/jquery.ui.min.js') }}"></script>
@endif
<script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!--livicons-->
<script src="{{ asset('assets/vendors/livicons/minified/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/livicons/minified/livicons-1.4.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/josh.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/metisMenu.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/holder-master/holder.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontendassets/js/admin.js')}}"></script>

<!-- end of global js -->
<!-- begin page level js -->
@yield('footer_scripts')
        <!-- end page level js -->
</body>
</html>