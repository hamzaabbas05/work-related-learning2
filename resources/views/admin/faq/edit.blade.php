@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    
    <link href="{{ asset('assets/vendors/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.min.css') }}"  rel="stylesheet" media="screen"/>
    <link href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/summernote/dist/summernote-bs3.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
    
@stop


{{-- Page content --}}
@section('content')
    @include('admin.partials.bredcrum')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                            {{$bredCrum}}
                        </h3>
                                <span class="pull-right clickable">
                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                </span>
                    </div>
                    <div class="panel-body">

                        <!-- errors -->
                        <div class="has-error">
                            @include('partials.errors')
                        </div>

                        <!--main content-->
                        <div class="col-md-12">

                            <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                            {!! Form::open(array('url' => 'admin/faq/'.$editObj->id, 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                             <input type="hidden" name="_method" value="put" />
                                <!-- CSRF Token -->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <section>
                                        <div class="form-group">
                                        <label class="col-sm-2 control-label">Question</label>
                                        <div class="col-sm-10">
                                           <textarea class="form-control required" name="question" rows="3">{{$editObj->question}}</textarea>
                                        </div>
                                    </div>
                                      <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="text-muted bootstrap-admin-box-title editor-clr">
                                <i class="livicon" data-name="tag" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                {{$bredCrum}}
                            </div>
                        </div>
                        <div class="bootstrap-admin-panel-content">
                            <textarea id="ckeditor_standard" name="answer">{{$editObj->answer}}</textarea>
                        </div>
                    </div> <div class="form-group">
                                        <label class="col-sm-2 control-label">Type</label>
                                        <div class="col-sm-10">
                                        <label class="radio-inline">
                                            <input name="ftype" id="optionsRadiosInline1" value="1"  <?php if ($editObj->ftype == 1) { ?> checked="checked" <?php }?>="checked" type="radio">Unpaid work experiences - FAQ</label>
                                        <label class="radio-inline">
                                            <input name="ftype" id="optionsRadiosInline2" value="2" type="radio"  <?php if ($editObj->ftype == 2) { ?> checked="checked" <?php }?>>Listing Unpaid Work Experience - FAQ</label>
 <label class="radio-inline">
                                            <input name="ftype" id="optionsRadiosInline2" value="3" type="radio"  <?php if ($editObj->ftype == 3) { ?> checked="checked" <?php }?>>Listing About School Houses - FAQ</label>




                                            </div>
                                       
                                    </div>
                                            <div class="form-group">
                                        <label>Status</label>
                                        <label class="radio-inline">
                                            <input name="status" id="status" value="1" 
                                            <?php if ($editObj->status == 1) { ?> checked="checked" <?php }?> type="radio">Active</label>
                                        <label class="radio-inline">
                                            <input name="status" id="status" value="2" type="radio" <?php if ($editObj->status == 2) { ?> checked="checked" <?php }?> >DeActive</label>
                                       
                                    </div>
                                <div class="form-group">
                                    <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                    </div>
                                    </div>

                                 </section>
                                

                            </form>
                            <!-- END FORM WIZARD WITH VALIDATION -->
                        </div>
                        <!--main content end-->
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    
    <!-- Bootstrap WYSIHTML5 -->
    <script  src="{{ asset('assets/vendors/ckeditor/ckeditor.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/ckeditor/adapters/jquery.js') }}"  type="text/javascript" ></script>
    <script  src="{{ asset('assets/vendors/tinymce/js/tinymce/tinymce.min.js') }}"  type="text/javascript" ></script>
    <script  src="{{ asset('assets/vendors/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.all.min.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.min.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript"></script>
    
@stop