<html>
    <table>
        <thead>
            <tr >
                <th>recipientId</th>
                <th>name</th>
                <th>account</th>
                <th>sourceCurrency</th>
                <th>targetCurrency</th>
                <th>amountCurrency</th>
                <th>amount</th>
                <th>paymentReference</th>
            </tr>
        </thead>
        <tbody>
            @if(sizeof($rentalPaidJobs) > 0)
            @foreach ($rentalPaidJobs as $job)
            <tr>
                <td>{!! isset($job->Bookingno)?$job->Bookingno:"" !!}</td>
                <td>{!! isset($job->host->name)?$job->host->name:"" !!}</td>
                <td>{!! isset($job->host->bank_detail->accountNumber)?$job->host->bank_detail->accountNumber:"" !!}</td>
                <td>{!! isset($job->host->bank_detail->sourceCurrency)?$job->host->bank_detail->sourceCurrency:"" !!}</td>
                <td>{!! isset($job->host->bank_detail->targetCurrency)?$job->host->bank_detail->targetCurrency:"" !!}</td>
                <td>{!! isset($job->host->bank_detail->amountCurrency)?$job->host->bank_detail->amountCurrency:"" !!}</td>
                <td>200</td>
                <td>{{date('FjY')}}</td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>

</html>