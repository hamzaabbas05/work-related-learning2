<html>
    <table>
        <thead>
            <tr >
                <th>ID</th>
                <th>Name</th>
                <th>Property Type</th>
                <th>Room Type</th>
                <th>status</th>
                <th>name</th>
                <th>surname</th>
                <th>mobile</th>
                <th>landline</th>
                <th>country</th>
                <th>address</th>
                <th>email</th>


            </tr>
        </thead>
        <tbody>
            @foreach ($paymentObj as $obj)
            <tr>
                <td>{!! $obj->id !!}</td>
                <td>{!! $obj->title !!}</td>
                <td>{{ isset($obj->propertytype->name)?$obj->propertytype->name:"" }}</td>
                <td>{{ isset($obj->roomtype->name)?$obj->roomtype->name:'' }}</td>
                <td >
                    @if($obj->status == 1)
                    Active
                    @elseif($obj->status == 0)
                    Pending Approval
                    @elseif($obj->status == 2)
                    DeActive
                    @endif

                </td>
                <td>{!! $obj->user->name !!}</td>
                <td>{!! $obj->user->lastname !!}</td>
                <td>{!! $obj->user->phonenumber !!}</td>
                <td>{!! $obj->user->orgphone !!}</td>
                <td>{!! $obj->user->country_id !!}</td>
                <td>{!! $obj->user->raddress18 !!}</td>
                <td>{!! $obj->user->email !!}</td>
                
            </tr>
            @endforeach

        </tbody>
    </table>

</html>