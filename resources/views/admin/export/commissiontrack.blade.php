<html>
  <table>
                    <thead>
                        <tr >
                           <th>ID</th>
                            <th>HostEmail</th>
                            <th>Booking No</th>
                            <th>Total Amount</th>
                            <th>Guest Fee</th>
                            <th>Host Fee</th>
                            <th>Payable Amount</th>
                            <th>Paid Status</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                   @foreach ($trackObj as $obj)
                        <tr>
                            <td>{!! $obj->id !!}</td>
                            <td>{!! $obj->host_email !!}</td>
                            <td>{!! $obj->booking_no !!}</td>
                          <td>{!! $obj->total_amount !!}</td>
                          <td>{!! $obj->guest_fee !!}</td>
                          
                              <td>{!! $obj->host_fee !!}</td>
                              <td>{!! $obj->payable_amount !!}</td>
                             <td>{!! $obj->paid_status !!}</td>
                             <td>{!! $obj->dateAdded !!}</td>
                        </tr>
                    @endforeach
                        
                    </tbody>
                </table>

</html>