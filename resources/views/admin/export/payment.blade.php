<html>
  <table>
                    <thead>
                        <tr >
                            <th>ID</th>
                            <th>Guest Email</th>
                            <th>HostEmail</th>
                            <th>Booking No</th>
                            <th>Total Amount</th>
                            <th>Payment Type</th>
                            <th>DealCodeNumber</th>
                            <th>PayerEmail</th>
                            <th>Transaction Id</th>
                            <th>Paid Status</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($paymentObj as $obj)
                    	<tr>
                            <td>{!! $obj->id !!}</td>
                            <td>{!! $obj->user->email !!}</td>
                            <td>{!! $obj->host->email !!}</td>
                            <td>{!! $obj->rentals->Bookingno !!}</td>
                            <td>{!! $obj->total !!}</td>
                            <td>{!! $obj->payment_type !!}</td>
                            <td>{!! $obj->dealCodeNumber !!}</td>
                            <td>{!! $obj->payer_email !!}</td>
                            <td>{!! $obj->paypal_transaction_id !!}</td>
                            <td>{!! $obj->status !!}</td>
                            <td>{!! $obj->modified !!}</td>
            			</tr>
                    @endforeach
                        
                    </tbody>
                </table>

</html>