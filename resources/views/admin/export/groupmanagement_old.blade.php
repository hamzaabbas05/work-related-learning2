<html>
    <table>
        <thead>
            <tr >
                <th>Date In</th>
                <th>Date Out</th>
                <th>Group Name</th>
                <th>School Name</th>
                <th>List Type</th>
                <th>Status</th>
                <th>Is Sent (user listing / work tutor)</th>
                <th>Is Confirmed (user listing / work tutor)</th>
                <th>P_Is Sent (user booking / Parent and Students)</th>
                <th>P_Is Confirmed (user booking / Parent and Students)</th>
                <th>S_Is Sent (user listing / School House)</th>
                <th>S_Is Confirmed (user listing / School House)</th>
                <th>First Choice</th>
                <th>First Booking No.</th>
                <th>First Org Name</th>
                <th>2nd Choice</th>
                <th>2nd Booking No.</th>
                <th>2nd Org Name</th>
                <th>3rd Choice</th>
                <th>3rd Booking No.</th>
                <th>3rd Org Name</th>
                <th>Admin 4th Choice</th>
                <th>Admin Booking No.</th>
                <th>Admin 4th Org Name</th>
                <th>Commenti Insegnante - eg. Adatto a "Realizza Evento presso Biblioteca", Adatto Primary school assistant teacher, Adatto grafica, Inglese scarso, inglese buono</th>
                <th>DOB</th>
                <th>Name</th>
                <th>Gender (Over 18)</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>DOB16</th>
                <th>Name (Over 16)</th>
                <th>Gender (Over 16)</th>
                <th>Class</th>
                <th>Mobile (Over 16)</th>
                <th>Email (Over 16)</th>                
                <th>Student Desp.</th>
                <th>Medical condition</th>
                <th>Notes for Home</th>
                <th>Notes for Work</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users_info as $user)
            @if(sizeof($user->user_booking) > 0)
            @foreach($user->user_booking as $booking)
            <tr>
                <td>{!! isset($booking->checkin)?date("F j, Y", strtotime($booking->checkin)):"" !!}</td>
                <td>{!! isset($booking->checkout)?date("F j, Y", strtotime($booking->checkout)):"" !!}</td>
                <td>{!! isset($user->group_info->group_name)?$user->group_info->group_name:"" !!}</td>
                <td>{!! isset($user->school_info->name)?$user->school_info->name:"" !!}</td>
                <td>{!! isset($booking->property_detail->property_status)?$booking->property_detail->property_status:"" !!}</td>
                <td>{!! isset($booking->approval)?$booking->approval:"" !!}</td>
                <?php
                $is_sent = "not found";
                $is_confirm = "not found";
                if (isset($booking->property_detail->id)) {
                    $email_detail = \App\EmailSent::where(["user_id" => $user->id, "property_id" => $booking->property_detail->id, "sent_to" => "Work Tutor Email"])->first();
                    if (isset($email_detail->id)) {
                        $is_sent = $email_detail->is_sent;
                        if ($email_detail->is_confirm == "1") {
                            $is_confirm = "Confirmed";
                        }
                    }
                }
                $parent_is_sent = "not found";
                $parent_is_confirm = "not found";
                if (isset($booking->property_detail->id)) {
                    $email_detail = \App\EmailSent::where(["user_id" => $user->id, "property_id" => $booking->property_detail->id, "sent_to" => "Parent Email"])->first();
                    if (isset($email_detail->id)) {
                        $parent_is_sent = $email_detail->is_sent;
                        if ($email_detail->is_confirm == "1") {
                            $parent_is_confirm = "Confirmed";
                        }
                    }
                }
                $paid_is_sent = "not found";
                $paid_is_confirm = "not found";
                if (isset($booking->property_detail->id)) {
                    $email_detail = \App\EmailSent::where(["user_id" => $user->id, "property_id" => $booking->property_detail->id, "sent_to" => "Paid Service school houses Email"])->first();
                    if (isset($email_detail->id)) {
                        $paid_is_sent = $email_detail->is_sent;
                        if ($email_detail->is_confirm == "1") {
                            $paid_is_confirm = "Confirmed";
                        }
                    }
                }
                ?>
                <td>{{$is_sent}}</td>
                <td>{{$is_confirm}}</td>
                <td>{{$parent_is_sent}}</td>
                <td>{{$parent_is_confirm}}</td>
                <td>{{$paid_is_sent}}</td>
                <td>{{$paid_is_confirm}}</td>
                <?php $choices = 0; ?>
                @for($i=0;$i<3;$i++)
                @if(isset($user->user_booking[$i]))
                <td>
                    <a href='{{url("view/rentals/".$user->user_booking[$i]->property_detail->id)}}' target="_blank">
                        {!! isset($user->user_booking[$i]->property_detail->exptitle)?$user->user_booking[$i]->property_detail->exptitle:"" !!}
                    </a>
                </td>
                <td>{{$user->user_booking[$i]->Bookingno}}</td>
                <td>
                    <?php
                    $ptitle = isset($user->user_booking[$i]->property_detail->user->orgname) ? $user->user_booking[$i]->property_detail->user->orgname : "No Title / Org Name added";
                    if (isset($user->user_booking[$i]->property_detail->title) && !empty($user->user_booking[$i]->property_detail->title)) {
                        $ptitle = $user->user_booking[$i]->property_detail->title;
                    }
                    ?>
                    {!! $ptitle !!}
                </td>
                @else
                <td>&nbsp; no choice</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                @endif
                @endfor

                <?php
                $check_admin = 0;
                $user_index = 0;
                for ($i = 0; $i <= 3; $i++) {
                    if (isset($user->user_booking[$i])) {
                        if (isset($user->user_booking[$i]->is_admin_booked) && $user->user_booking[$i]->is_admin_booked == "1") {
                            $check_admin = 1;
                            $user_index = $i;
                        }
                    }
                }
                ?>
                @if(isset($check_admin) && $check_admin == "1" )
                <td>
                    <a href='{{url("view/rentals/".$user->user_booking[$user_index]->property_detail->id)}}' target="_blank">
                        {!! isset($user->user_booking[$user_index]->property_detail->exptitle)?$user->user_booking[$user_index]->property_detail->exptitle:"" !!}
                    </a>
                </td>
                <td>{{$user->user_booking[$user_index]->Bookingno}}</td>
                <td>
                    <?php
                    $ptitle = isset($user->user_booking[$user_index]->property_detail->user->orgname) ? $user->user_booking[$user_index]->property_detail->user->orgname : "No Title / Org Name added";
                    if (isset($user->user_booking[$user_index]->property_detail->title) && !empty($user->user_booking[$user_index]->property_detail->title)) {
                        $ptitle = $user->user_booking[$user_index]->property_detail->title;
                    }
                    ?>
                    {!! $ptitle !!}
                </td>
                @else
                <td>&nbsp; no admin choice</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                @endif

                <td>&nbsp;</td>
                <!--td>{!! isset($user->dob)?date("F j, Y", strtotime($user->dob)):"" !!}</td-->
                <td>{!! isset($user->dob)?$user->dob:"" !!}</td>
                <td>{!! isset($user->lastname)?$user->lastname:"" !!} {!! isset($user->name)?$user->name:"" !!}</td>
                <td>
                    @if(isset($user->gender) && $user->gender == "1")
                    Male
                    @elseif(isset($user->gender) && $user->gender == "2")
                    Female
                    @elseif(isset($user->gender) && $user->gender == "3")
                    Other
                    @endif
                </td>
                <td>{!! isset($user->phonenumber)?$user->phonenumber:"" !!}</td>
                <td>{!! isset($user->email)?$user->email:"" !!}</td>
                <!--td>{!! isset($user->dob16)?date("F j, Y", strtotime($user->dob16)):"" !!}</td-->
                <td>{!! isset($user->dob16)?$user->dob16:"" !!}</td>
                <td>{!! isset($user->ssurname16)?$user->ssurname16:"" !!} {!! isset($user->sname16)?$user->sname16:"" !!}</td>
                <td>{!! isset($user->gender16)?$user->gender16:"" !!}</td>
                <td>{!! isset($user->class_letter)?$user->class_letter:"" !!} {!! isset($user->class_number)?$user->class_number:"" !!}</td>
                <td>{!! isset($user->sphone16)?$user->sphone16:"" !!}</td>
                <td>{!! isset($user->semail16)?$user->semail16:"" !!}</td>                
                <td>{!! isset($user->about_yourself)?$user->about_yourself:"" !!}</td>
                <td>{!! isset($user->medical_learning)?$user->medical_learning:"" !!}</td>
                <td>{!! isset($user->host_family_notes)?$user->host_family_notes:"" !!}</td>
                <td>{!! isset($user->work_tutor_notes)?$user->work_tutor_notes:"" !!}</td>
            </tr>
            @endforeach
            @endif                        
            @endforeach

        </tbody>
    </table>

</html>