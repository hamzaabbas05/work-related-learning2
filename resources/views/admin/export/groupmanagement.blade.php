<html><table><thead><tr>
                <th>Date In</th>
                <th>Date Out</th>
                <th>Group Name</th>
                <!--<th>User ID</th>-->
                <th>Address</th>
                <th>Rules / Specifications</th>
                <th>Email</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Mobile</th>
                <!--here landaline other paid service infol other job listing user info -->
                <th>Org Phone</th>
                <th>Registration No</th>
                <th>Org Name</th>
                <th>Role in Company</th>
                <th>Why Fit</th>
                <th>School Name</th>
                <th>List Type</th>
                <th>Status</th>
                <th>Is Sent (to user listing / work tutor)</th>
                <th>Is Confirmed (by user listing / work tutor)</th>
                <th>P_Is Sent (to user booking (type of booking is defined in column "List Type") / Parent and Students)</th>
                <th>P_Is Confirmed (by user booking / Parent and Students)</th>
                <th>S_Is Sent (user listing / School House)</th>
                <th>S_Is Confirmed (user listing / School House)</th>
                <th>Student_Is Sent</th>
                <th>Student_Is Confirmed</th>
                <th>First Choice</th>
                <th>First Booking No.</th>
                <th>First Org Name</th>
                <th>2nd Choice</th>
                <th>2nd Booking No.</th>
                <th>2nd Org Name</th>
                <th>3rd Choice</th>
                <th>3rd Booking No.</th>
                <th>3rd Org Name</th>
                <th>Admin 4th Choice</th>
                <th>Admin Booking No.</th>
                <th>Admin 4th Org Name</th>
                <th>Commenti Insegnante - eg. Adatto a Biblioteca, Primary school grafica, Inglese scarso, buono</th>
                <th>DOB</th>
                <th>Name</th>
                <th>Gender (Over 18)</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>DOB16</th>
                <th>Name (Over 16)</th>
                <th>Gender (Over 16)</th>
                <th>Class</th>
                <th>Mobile (Over 16)</th>
                <th>Email (Over 16)</th>                
                <th>Student Desp.</th>
                <th>Medical condition</th>
                <th>Notes for Home</th>
                <th>Notes for Work</th>
                <!--Work Tutor Detials (IF NOT YOU)-->
                <th>Relationship in the Company</th>
                <th>Why are you fit for being a Work Tutor?</th>
                <th>Work Tutor's Name</th>
                <th>Work Tutor's SurName</th>
                <th>Work Tutor's Tax Identification Number</th>
                <th>Work Tutor's Email</th>
                <th>Work Tutor's Tel</th>
                <th>Work Tutor's Place of Birth</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $rental_array = array();

            // echo "Count :".count($users_info)."<pre>";
            //     print_r($users_info);
            // echo "</pre>";
            // exit;
            ?>
            @foreach ($users_info as $booking)
            <?php
            $rental_array[] = $booking->id;
            ?>
            <tr>
                <td>{!! isset($booking->checkin)?date("F j, Y", strtotime($booking->checkin)):"" !!}</td>
                <td>{!! isset($booking->checkout)?date("F j, Y", strtotime($booking->checkout)):"" !!}</td>
                <td>{!! isset($booking->user_detail->group_info->group_name)?$booking->user_detail->group_info->group_name:"" !!}</td>
                <?php /*<!--<td>{!! isset($booking->property_detail->user_detail->id)?$booking->property_detail->user->id:"" !!}</td>-->*/?>
                <td>
                   @if($booking->property_detail->property_status == "paid")
                    {!! isset($booking->property_detail->user->paid_service->houseaddress)?$booking->property_detail->user->paid_service->houseaddress:"" !!}
                    @elseif(isset($booking->property_detail->legal_address) && !empty($booking->property_detail->legal_address) )
                    {!! $booking->property_detail->legal_address !!}
                    @elseif(isset($booking->property_detail->user->raddress18) && !empty($booking->property_detail->user->raddress18) )
                    {{$booking->property_detail->user->raddress18}}
                    @endif
                </td>
                <td>{!! isset($booking->property_detail->houserules)?$booking->property_detail->houserules:"" !!}</td>
                <td>{!! isset($booking->property_detail->user->email)?$booking->property_detail->user->email:"" !!}</td>
                <td>{!! isset($booking->property_detail->user->name)?$booking->property_detail->user->name:"" !!}</td>
                <td>{!! isset($booking->property_detail->user->lastname)?$booking->property_detail->user->lastname:"" !!}</td>
                <td>{!! isset($booking->property_detail->user->phonenumber)?$booking->property_detail->user->phonenumber:"" !!}</td>

                <td>{!! isset($booking->property_detail->user->orgphone)?$booking->property_detail->user->orgphone:"" !!}</td>
                <td>{!! isset($booking->property_detail->user->organisation_tax_registration_number)?$booking->property_detail->user->organisation_tax_registration_number:"" !!}</td>
                <td>{!! isset($booking->property_detail->user->orgname)?$booking->property_detail->user->orgname:"" !!}</td>
                <td>{!! isset($booking->property_detail->user->user_tutorrelation->name)?$booking->property_detail->user->user_tutorrelation->name:"" !!}</td>
                <td>{!! isset($booking->property_detail->user->why_fit)?$booking->property_detail->user->why_fit:"" !!}</td>

                <td>{!! isset($booking->user_detail->school_info->name)?$booking->user_detail->school_info->name:"" !!}</td>
                <td>
                    <?php $property_type = ""; ?>
                    @if(isset($booking->property_detail->property_status) && $booking->property_detail->property_status == "unpaid")
                    <?php $property_type = "Work Placement"; ?>
                    @elseif(isset($booking->property_detail->property_status) && $booking->property_detail->property_status == "unpaidflash")
                    <?php $property_type = "Work Placement"; ?>
                    @elseif(isset($booking->property_detail->property_status) && $booking->property_detail->property_status == "paid")
                    <?php $property_type = "School House"; ?>
                    @endif
                    {{$property_type}}
                </td>
                <td>{!! isset($booking->approval)?$booking->approval:"" !!}</td>
                
                <?php

                $is_sent = "not found";
                $is_confirm = "not found";
                if (isset($booking->property_detail->id)) {
                    $email_detail = \App\EmailSent::where(["user_id" => $booking->user_id, "property_id" => $booking->prd_id, "sent_to" => "Work Tutor Email"])->first();
                    if (isset($email_detail->id)) {
                        $is_sent = $email_detail->is_sent;
                        if ($email_detail->is_confirm == "1") {
                            $is_confirm = "Confirmed";
                        }
                    }
                }
                $parent_is_sent = "not found";
                $parent_is_confirm = "not found";
                if (isset($booking->property_detail->id)) {
                    $email_detail = \App\EmailSent::where(["user_id" => $booking->user_detail->id, "property_id" => $booking->property_detail->id, "sent_to" => "Parent Email"])->first();
                    if (isset($email_detail->id)) {
                        $parent_is_sent = $email_detail->is_sent;
                        if ($email_detail->is_confirm == "1") {
                            $parent_is_confirm = "Confirmed";
                        }
                    }
                }
                $paid_is_sent = "not found";
                $paid_is_confirm = "not found";
                if (isset($booking->property_detail->id)) {
                    $email_detail = \App\EmailSent::where(["user_id" => $booking->user_detail->id, "property_id" => $booking->property_detail->id, "sent_to" => "Paid Service school houses Email"])->first();
                    if (isset($email_detail->id)) {
                        $paid_is_sent = $email_detail->is_sent;
                        if ($email_detail->is_confirm == "1") {
                            $paid_is_confirm = "Confirmed";
                        }
                    }
                }
                $student_is_sent = "not found";
                $student_is_confirm = "not found";
                if (isset($booking->property_detail->id)) {
                    $email_detail = \App\EmailSent::where(["user_id" => $booking->user_detail->id, "property_id" => $booking->property_detail->id, "sent_to" => "Student Email"])->first();
                    if (isset($email_detail->id)) {
                        $student_is_sent = $email_detail->is_sent;
                        if ($email_detail->is_confirm == "1") {
                            $student_is_confirm = "Confirmed";
                        }
                    }
                }
                ?>
                <td>{{$is_sent}}</td>
                <td>{{$is_confirm}}</td>
                <td>{{$parent_is_sent}}</td>
                <td>{{$parent_is_confirm}}</td>
                <td>{{$paid_is_sent}}</td>
                <td>{{$paid_is_confirm}}</td>
                <td>{{$student_is_sent}}</td>
                <td>{{$student_is_confirm}}</td>
                <!--user 1st choice starts-->
                <td>
                    <a href='{{url("view/rentals/".$booking->property_detail->id)}}' target="_blank">
                        {!! isset($booking->property_detail->exptitle)?$booking->property_detail->exptitle:"" !!}
                    </a>
                </td>
                <td>{{$booking->Bookingno}}</td>
                <td>
                    <?php
                    $ptitle = isset($booking->host->orgname) ? $booking->host->orgname : "No Title / Org Name added";
                    if (isset($booking->property_detail->title) && !empty($booking->property_detail->title)) {
                        $ptitle = $booking->property_detail->title;
                    }
                    ?>
                    {!! $ptitle !!}
                </td>
                <!--user 1st choice ends-->
                <!--user 2nd choice starts-->
                
                <?php
                $second_choice_checkin = '';
                $second_choice = App\RentalsEnquiry::where('dateAdded', '>', $booking->dateAdded)->where('user_id', $booking->user_id)->orderBy('dateAdded', 'ASC')->first();
                if (isset($second_choice->id)) {
                    $second_choice_checkin = isset($second_choice->dateAdded) ? $second_choice->dateAdded : "";
                    $rental_array[] = $second_choice->id;
                    ?>
                    <td>
                        <a href='{{url("view/rentals/".$second_choice->property_detail->id)}}' target="_blank">
                            {!! isset($second_choice->property_detail->exptitle)?$second_choice->property_detail->exptitle:"" !!}
                        </a>
                    </td>
                    <td>{{$second_choice->Bookingno}}</td>
                    <td>
                        <?php
                        $ptitle = isset($second_choice->host->orgname) ? $second_choice->host->orgname : "No Title / Org Name added";
                        if (isset($second_choice->property_detail->title) && !empty($second_choice->property_detail->title)) {
                            $ptitle = $second_choice->property_detail->title;
                        }
                        ?>
                        {!! $ptitle !!}
                    </td>
                <?php } else { ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php } ?> 
                <!--user 2nd choice ends-->
                <!--user 3rd choice starts-->
               <?php
                if (!empty($second_choice_checkin)) {
                    $third_choice = App\RentalsEnquiry::where('dateAdded', '>', $second_choice_checkin)->where('user_id', $booking->user_id)->orderBy('dateAdded', 'ASC')->first();
                    if (isset($third_choice->id)) {
                        $rental_array[] = $third_choice->id;
                        ?> 
                        <td>
                            <a href='{{url("view/rentals/".$third_choice->property_detail->id)}}' target="_blank">
                                {!! isset($third_choice->property_detail->exptitle)?$third_choice->property_detail->exptitle:"" !!}
                            </a>
                        </td>
                        <td>{{$third_choice->Bookingno}}</td>
                        <td>
                           <?php
                            $ptitle = isset($third_choice->host->orgname) ? $third_choice->host->orgname : "No Title / Org Name added";
                            if (isset($third_choice->property_detail->title) && !empty($third_choice->property_detail->title)) {
                                $ptitle = $third_choice->property_detail->title;
                            }
                            ?>
                            {!! $ptitle !!}
                        </td>
                    <?php } else { ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <?php
                    }
                } else {
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php } ?>
                <!--user 3rd choice ends-->
                <!--user Admin choice starts-->
                <?php
                $admin_choice = App\RentalsEnquiry::select("fc_rentalsenquiry.*")
                                ->join("property", "fc_rentalsenquiry.prd_id", "=", "property.id")
                                ->where('fc_rentalsenquiry.user_id', $booking->user_id)
                                ->where('fc_rentalsenquiry.is_admin_booked', '1')
                                ->where('property.property_status', 'paid')
                                ->orderBy('fc_rentalsenquiry.id', 'DESC')->first();
                if (isset($admin_choice->id)) {
                    ?>
                    <td>
                        <a href='{{url("view/rentals/".$admin_choice->property_detail->id)}}' target="_blank">
                            {!! isset($admin_choice->property_detail->exptitle)?$admin_choice->property_detail->exptitle:"" !!}
                        </a> 
                    </td>
                    <td>{{$admin_choice->Bookingno}}</td>
                    <td>
                       <?php
                        $ptitle = isset($admin_choice->host->orgname) ? $admin_choice->host->orgname : "No Title / Org Name added";
                        if (isset($admin_choice->property_detail->title) && !empty($admin_choice->property_detail->title)) {
                            $ptitle = $admin_choice->property_detail->title;
                        }
                        ?>
                        {!! $ptitle !!}
                    </td>
                <?php } else { ?>
                    <td>&nbsp; no admin choice</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php } ?> 
                <!--user Admin choice ends-->
                <td>&nbsp;</td>
                <?php /* <!--td>{!! isset($booking->user_detail->dob)?date("F j, Y", strtotime($booking->user_detail->dob)):"" !!}</td--> */?>

                <td>{!! isset($booking->user_detail->dob)?$booking->user_detail->dob:"" !!}</td>
                <td>{!! isset($booking->user_detail->lastname)?$booking->user_detail->lastname:"" !!} {!! isset($booking->user_detail->name)?$booking->user_detail->name:"" !!}</td>
                <td>
                    
                    @if(isset($booking->user_detail->gender) && $booking->user_detail->gender == "1")
                    Male
                    @elseif(isset($booking->user_detail->gender) && $booking->user_detail->gender == "2")
                    Female
                    @elseif(isset($booking->user_detail->gender) && $booking->user_detail->gender == "3")
                    Other
                    @endif
                </td>
                <td>{!! isset($booking->user_detail->phonenumber)?$booking->user_detail->phonenumber:"" !!}</td>
                <td>{!! isset($booking->user_detail->email)?$booking->user_detail->email:"" !!}</td>
                <?php /*<!--td>{!! isset($booking->user_detail->dob16)?date("F j, Y", strtotime($booking->user_detail->dob16)):"" !!}</td--> */?>
                <td>{!! isset($booking->user_detail->dob16)?$booking->user_detail->dob16:"" !!}</td>
                <td>{!! isset($booking->user_detail->ssurname16)?$booking->user_detail->ssurname16:"" !!} {!! isset($booking->user_detail->sname16)?$booking->user_detail->sname16:"" !!}</td>
                <td>{!! isset($booking->user_detail->gender16)?$booking->user_detail->gender16:"" !!}</td>
                <td>{!! isset($booking->user_detail->class_letter)?$booking->user_detail->class_letter:"" !!} {!! isset($booking->user_detail->class_number)?$booking->user_detail->class_number:"" !!}</td>
                <td>{!! isset($booking->user_detail->sphone16)?$booking->user_detail->sphone16:"" !!}</td>
                <td>{!! isset($booking->user_detail->semail16)?$booking->user_detail->semail16:"" !!}</td>
                <td>{!! isset($booking->user_detail->about_yourself)?$booking->user_detail->about_yourself:"" !!}</td>
                <td>{!! isset($booking->user_detail->medical_learning)?$booking->user_detail->medical_learning:"" !!}</td>
                <td>{!! isset($booking->user_detail->host_family_notes)?$booking->user_detail->host_family_notes:"" !!}</td>
                <td>{!! isset($booking->user_detail->work_tutor_notes)?$booking->user_detail->work_tutor_notes:"" !!}</td>
                <!--Work Tutor Detials (IF NOT YOU)-->
                @if(isset($booking->property_detail->work_org_same) && $booking->property_detail->work_org_same == 0)
                <td>{!! isset($booking->property_detail->tutorrelation->name)?$booking->property_detail->tutorrelation->name:"" !!}</td>
                <td>{!! isset($booking->property_detail->work_tutor_fit)?$booking->property_detail->work_tutor_fit:"" !!}</td>
                <td>{!! isset($booking->property_detail->represent_name)?$booking->property_detail->represent_name:"" !!}</td>
                <td>{!! isset($booking->property_detail->represent_surname)?$booking->property_detail->represent_surname:"" !!}</td>
                <td>{!! isset($booking->property_detail->represent_tax_no)?$booking->property_detail->represent_tax_no:"" !!}</td>
                <td>{!! isset($booking->property_detail->represent_email)?$booking->property_detail->represent_email:"" !!}</td>
                <td>{!! isset($booking->property_detail->represent_phone)?$booking->property_detail->represent_phone:"" !!}</td>
                <td>{!! isset($booking->property_detail->represent_born)?$booking->property_detail->represent_born:"" !!}</td>
                @else
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                @endif
            </tr>  
            @endforeach
</tbody></table></html>