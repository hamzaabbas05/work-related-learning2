<html>
  <table>
                    <thead>
                        <tr >
                            <th>ID</th>
                            <th>BookingNo</th>
                            <th>HostEmail</th>
                            <th>PropertyName</th>
                            <th>CheckIn</th>
                            <th>Checkout</th>
                            <th>Amount</th>
                            <th>Booking Status</th>
                            <th>Host Approval</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($reserveObj as $obj)
                    	<tr>
                             <td>{!! $obj->id !!}</td>
                            <td>{!! $obj->Bookingno !!}</td>
                            <td>{!! $obj->host->email !!}</td>
                            <td>{!! $obj->property->title !!}</td>
                            <td>{!! $obj->checkin !!}</td>
                            <td>{!! $obj->checkout !!}</td>
                            <td>{!! $obj->totalAmt !!}</td>
                            <td>{!! $obj->booking_status !!}</td>
                            <td>{!! $obj->approval !!}</td>
                            <td>{!! $obj->dateAdded !!}</td>
            			</tr>
                    @endforeach
                        
                    </tbody>
                </table>

</html>