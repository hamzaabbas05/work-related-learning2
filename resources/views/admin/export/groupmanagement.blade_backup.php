<html>
    <table>
        <thead>
            <tr >
                <th>Date In</th>
                <th>Date Out</th>
                <th>Group Name</th>
                <th>Address</th>
                <th>Rules / Specifications</th>
                <th>Email</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Mobile</th>
                <th>School Name</th>
                <th>List Type</th>
                <th>Status</th>
                <th>Is Sent (user listing / work tutor)</th>
                <th>Is Confirmed (user listing / work tutor)</th>
                <th>P_Is Sent (user booking / Parent and Students)</th>
                <th>P_Is Confirmed (user booking / Parent and Students)</th>
                <th>S_Is Sent (user listing / School House)</th>
                <th>S_Is Confirmed (user listing / School House)</th>
                <th>First Choice</th>
                <th>First Booking No.</th>
                <th>First Org Name</th>
                <th>2nd Choice</th>
                <th>2nd Booking No.</th>
                <th>2nd Org Name</th>
                <th>3rd Choice</th>
                <th>3rd Booking No.</th>
                <th>3rd Org Name</th>
                <th>Admin 4th Choice</th>
                <th>Admin Booking No.</th>
                <th>Admin 4th Org Name</th>
                <th>Commenti Insegnante - eg. Adatto a Biblioteca, Primary school grafica, Inglese scarso, buono</th>
                <th>DOB</th>
                <th>Name</th>
                <th>Gender (Over 18)</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>DOB16</th>
                <th>Name (Over 16)</th>
                <th>Gender (Over 16)</th>
                <th>Class</th>
                <th>Mobile (Over 16)</th>
                <th>Email (Over 16)</th>                
                <th>Student Desp.</th>
                <th>Medical condition</th>
                <th>Notes for Home</th>
                <th>Notes for Work</th>
            </tr>
        </thead>
        <tbody>
            
            @foreach ($users_info as $booking)
            <tr>
                <td>{!! isset($booking->checkin)?date("F j, Y", strtotime($booking->checkin)):"" !!}</td>
                <td>{!! isset($booking->checkout)?date("F j, Y", strtotime($booking->checkout)):"" !!}</td>
                <td>{!! isset($booking->user_detail->group_info->group_name)?$booking->user_detail->group_info->group_name:"" !!}</td>
                @if($booking->approval == "Accept")
                    <td>{!! isset($booking->property->user->paid_service->houseaddress)?$booking->property->user->paid_service->houseaddress:"" !!}</td>
                    @if($booking->property->property_status == "paid")
                        <td>{!! isset($booking->property->houserules)?$booking->property->houserules:"" !!}</td>
                    @else
                        <td>&nbsp;</td>
                    @endif
                    <td>{!! isset($booking->property->user->email)?$booking->property->user->email:"" !!}</td>
                    <td>{!! isset($booking->property->user->name)?$booking->property->user->name:"" !!}</td>
                    <td>{!! isset($booking->property->user->lastname)?$booking->property->user->lastname:"" !!}</td>
                    <td>{!! isset($booking->property->user->phonenumber)?$booking->property->user->phonenumber:"" !!}</td>
                @else
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                @endif
                <td>{!! isset($booking->user_detail->school_info->name)?$booking->user_detail->school_info->name:"" !!}</td>
                <td>{!! isset($booking->property_detail->property_status)?$booking->property_detail->property_status:"" !!}</td>
                <td>{!! isset($booking->approval)?$booking->approval:"" !!}</td>
                <?php
                $is_sent = "not found";
                $is_confirm = "not found";
                if (isset($booking->property_detail->id)) {
                    $email_detail = \App\EmailSent::where(["user_id" => $booking->user_detail->id, "property_id" => $booking->property_detail->id, "sent_to" => "Work Tutor Email"])->first();
                    if (isset($email_detail->id)) {
                        $is_sent = $email_detail->is_sent;
                        if ($email_detail->is_confirm == "1") {
                            $is_confirm = "Confirmed";
                        }
                    }
                }
                $parent_is_sent = "not found";
                $parent_is_confirm = "not found";
                if (isset($booking->property_detail->id)) {
                    $email_detail = \App\EmailSent::where(["user_id" => $booking->user_detail->id, "property_id" => $booking->property_detail->id, "sent_to" => "Parent Email"])->first();
                    if (isset($email_detail->id)) {
                        $parent_is_sent = $email_detail->is_sent;
                        if ($email_detail->is_confirm == "1") {
                            $parent_is_confirm = "Confirmed";
                        }
                    }
                }
                $paid_is_sent = "not found";
                $paid_is_confirm = "not found";
                if (isset($booking->property_detail->id)) {
                    $email_detail = \App\EmailSent::where(["user_id" => $booking->user_detail->id, "property_id" => $booking->property_detail->id, "sent_to" => "Paid Service school houses Email"])->first();
                    if (isset($email_detail->id)) {
                        $paid_is_sent = $email_detail->is_sent;
                        if ($email_detail->is_confirm == "1") {
                            $paid_is_confirm = "Confirmed";
                        }
                    }
                }
                ?>
                <td>{{$is_sent}}</td>
                <td>{{$is_confirm}}</td>
                <td>{{$parent_is_sent}}</td>
                <td>{{$parent_is_confirm}}</td>
                <td>{{$paid_is_sent}}</td>
                <td>{{$paid_is_confirm}}</td>
                <?php $choices = 0; ?>
                @for($i=0;$i<3;$i++)
                @if(isset($booking->user_detail->user_booking[$i]))
                <td>
                    <a href='{{url("view/rentals/".$booking->user_detail->user_booking[$i]->property_detail->id)}}' target="_blank">
                        {!! isset($booking->user_detail->user_booking[$i]->property_detail->exptitle)?$booking->user_detail->user_booking[$i]->property_detail->exptitle:"" !!}
                    </a>
                </td>
                <td>{{$booking->user_detail->user_booking[$i]->Bookingno}}</td>
                <td>
                    <?php
                    $ptitle = isset($booking->user_detail->user_booking[$i]->property_detail->user->orgname) ? $booking->user_detail->user_booking[$i]->property_detail->user->orgname : "No Title / Org Name added";
                    if (isset($booking->user_detail->user_booking[$i]->property_detail->title) && !empty($booking->user_detail->user_booking[$i]->property_detail->title)) {
                        $ptitle = $booking->user_detail->user_booking[$i]->property_detail->title;
                    }
                    ?>
                    {!! $ptitle !!}
                </td>
                @else
                <td>&nbsp; no choice</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                @endif
                @endfor

                <?php
                $check_admin = 0;
                $user_index = 0;
                for ($i = 0; $i <= 3; $i++) {
                    if (isset($booking->user_detail->user_booking[$i])) {
                        if (isset($booking->user_detail->user_booking[$i]->is_admin_booked) && $booking->user_detail->user_booking[$i]->is_admin_booked == "1") {
                            $check_admin = 1;
                            $user_index = $i;
                        }
                    }
                }
                ?>
                @if(isset($check_admin) && $check_admin == "1" )
                <td>
                    <a href='{{url("view/rentals/".$booking->user_detail->user_booking[$user_index]->property_detail->id)}}' target="_blank">
                        {!! isset($booking->user_detail->user_booking[$user_index]->property_detail->exptitle)?$booking->user_detail->user_booking[$user_index]->property_detail->exptitle:"" !!}
                    </a>
                </td>
                <td>{{$booking->user_detail->user_booking[$user_index]->Bookingno}}</td>
                <td>
                    <?php
                    $ptitle = isset($booking->user_detail->user_booking[$user_index]->property_detail->user->orgname) ? $booking->user_detail->user_booking[$user_index]->property_detail->user->orgname : "No Title / Org Name added";
                    if (isset($booking->user_detail->user_booking[$user_index]->property_detail->title) && !empty($booking->user_detail->user_booking[$user_index]->property_detail->title)) {
                        $ptitle = $booking->user_detail->user_booking[$user_index]->property_detail->title;
                    }
                    ?>
                    {!! $ptitle !!}
                </td>
                @else
                <td>&nbsp; no admin choice</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                @endif

                <td>&nbsp;</td>
                <!--td>{!! isset($booking->user_detail->dob)?date("F j, Y", strtotime($booking->user_detail->dob)):"" !!}</td-->
                <td>{!! isset($booking->user_detail->dob)?$booking->user_detail->dob:"" !!}</td>
                <td>{!! isset($booking->user_detail->lastname)?$booking->user_detail->lastname:"" !!} {!! isset($booking->user_detail->name)?$booking->user_detail->name:"" !!}</td>
                <td>
                    @if(isset($booking->user_detail->gender) && $booking->user_detail->gender == "1")
                    Male
                    @elseif(isset($booking->user_detail->gender) && $booking->user_detail->gender == "2")
                    Female
                    @elseif(isset($booking->user_detail->gender) && $booking->user_detail->gender == "3")
                    Other
                    @endif
                </td>
                <td>{!! isset($booking->user_detail->phonenumber)?$booking->user_detail->phonenumber:"" !!}</td>
                <td>{!! isset($booking->user_detail->email)?$booking->user_detail->email:"" !!}</td>
                <!--td>{!! isset($booking->user_detail->dob16)?date("F j, Y", strtotime($booking->user_detail->dob16)):"" !!}</td-->
                <td>{!! isset($booking->user_detail->dob16)?$booking->user_detail->dob16:"" !!}</td>
                <td>{!! isset($booking->user_detail->ssurname16)?$booking->user_detail->ssurname16:"" !!} {!! isset($booking->user_detail->sname16)?$booking->user_detail->sname16:"" !!}</td>
                <td>{!! isset($booking->user_detail->gender16)?$booking->user_detail->gender16:"" !!}</td>
                <td>{!! isset($booking->user_detail->class_letter)?$booking->user_detail->class_letter:"" !!} {!! isset($booking->user_detail->class_number)?$booking->user_detail->class_number:"" !!}</td>
                <td>{!! isset($booking->user_detail->sphone16)?$booking->user_detail->sphone16:"" !!}</td>
                <td>{!! isset($booking->user_detail->semail16)?$booking->user_detail->semail16:"" !!}</td>                
                <td>{!! isset($booking->user_detail->about_yourself)?$booking->user_detail->about_yourself:"" !!}</td>
                <td>{!! isset($booking->user_detail->medical_learning)?$booking->user_detail->medical_learning:"" !!}</td>
                <td>{!! isset($booking->user_detail->host_family_notes)?$booking->user_detail->host_family_notes:"" !!}</td>
                <td>{!! isset($booking->user_detail->work_tutor_notes)?$booking->user_detail->work_tutor_notes:"" !!}</td>
            </tr>                   
            @endforeach

        </tbody>
    </table>

</html>