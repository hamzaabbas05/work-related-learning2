@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Form Editors
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    
    <link href="{{ asset('assets/vendors/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.min.css') }}"  rel="stylesheet" media="screen"/>
    <link href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/summernote/dist/summernote-bs3.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
    
@stop

{{-- Page content --}}
@section('content')

  @include('admin.partials.bredcrum')
            <!--section ends-->
            <section class="content paddingleft_right15">
            {!! Form::open(array('url' => 'admin/cms/'.$editObj->id, 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                <!--main content-->
                <input type="hidden" name="_method" value="put" />
                <div class="row">
                            <div class="form-group">
                                        <label class="col-sm-2 control-label">Name</label>
                                        <div class="col-sm-10">
                                        <input id="name" name="page_name" value="{{$editObj->page_name}}" type="text" class="form-control">
                                        </div>
                                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="text-muted bootstrap-admin-box-title editor-clr">
                                <i class="livicon" data-name="tag" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                {{$bredCrum}}
                            </div>
                        </div>
                        <div class="bootstrap-admin-panel-content">
                            <textarea id="ckeditor_full" name="page_desc">{{$editObj->page_desc}}</textarea>
                        </div>
                    </div>
                               <div class="form-group">
                                        <label>Status</label>
                                        <label class="radio-inline">
                                            <input name="status" id="status" value="1" 
                                            <?php if ($editObj->status == 1) { ?> checked="checked" <?php }?> type="radio">Active</label>
                                        <label class="radio-inline">
                                            <input name="status" id="status" value="2" type="radio" <?php if ($editObj->status == 2) { ?> checked="checked" <?php }?> >DeActive</label>
                                       
                                    </div>
                </div>
                    <div class="form-group">
                                    <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                    </div>
                                    </div> </form>

              <!--   <div class="row">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="text-muted bootstrap-admin-box-title editor-clr">
                                <i class="livicon" data-name="thermo-down" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                CKEditor Full
                            </div>
                        </div>
                        <div class="bootstrap-admin-panel-content">
                            <textarea id="ckeditor_full"></textarea>
                        </div>
                    </div>
                </div> -->
              <!--   <div class="row">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="text-muted bootstrap-admin-box-title editor-clr">
                                <i class="livicon" data-name="thermo-up" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                TinyMCE Basic
                            </div>
                        </div>
                        <div class="bootstrap-admin-panel-content">
                            <textarea id="tinymce_basic"></textarea>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="row">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="text-muted bootstrap-admin-box-title editor-clr">
                                <i class="livicon" data-name="umbrella" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                TinyMCE Full
                            </div>
                        </div>
                        <div class="bootstrap-admin-panel-content">
                            <textarea id="tinymce_full"></textarea>
                        </div>
                    </div>
                </div> -->
                
               <!--  <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="livicon" data-name="search" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                    Bootstrap WYSIHTML5
                                </h3>
                            </div>
                            <div class="panel-body">
                                <form>
                                    <textarea class="textarea editor-cls" placeholder="Place some text here" ></textarea>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> -->
                 
                <!--main content ends--> </section>
            <!-- content --> 
    @stop

{{-- page level scripts --}}
@section('footer_scripts')
    
    <!-- Bootstrap WYSIHTML5 -->
    <script  src="{{ asset('assets/vendors/ckeditor/ckeditor.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/ckeditor/adapters/jquery.js') }}"  type="text/javascript" ></script>
    <script  src="{{ asset('assets/vendors/tinymce/js/tinymce/tinymce.min.js') }}"  type="text/javascript" ></script>
    <script  src="{{ asset('assets/vendors/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.all.min.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.min.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript"></script>
    
@stop