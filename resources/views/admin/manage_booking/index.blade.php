@extends('admin/layouts/default')
<?php 
// ini_set("display_errors","-1");
// error_reporting(E_ALL);
?>
{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Page content --}}
@section('content')
<?php //if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){

                                    // echo "User Info<pre>";
                                    //     print_r($users_info);
                                    // echo "</pre>";
                                    // exit;
                                //} echo "test";exit; ?>
@include('admin.partials.bredcrum')
<?php // ini_set('memory_limit', '128M');?>
<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    {{$bredCrum}}
                </h4>
            </div>
            <br />
 

            @include('layouts.errorsuccess')
           
            <div class="panel-body">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th style="width: 5%;">ID</th>
                            <th style="width: 20%;">Student</th>
                            <th style="width: 30%;">Job</th>
                            <th style="width: 10%;">Booking No#</th>
                            <th style="width: 15%;">Check In</th>
                            <th style="width: 15%;">Check Out</th>
                            <th>Status</th>
                            <th>Is Cancelled</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php //if($_SERVER['REMOTE_ADDR'] != '93.42.44.112'){ ?>

                        @foreach ($users_info as $booking)
                        <tr>

                            <td>{!! $booking->id !!}</td>
                            <td> 
                                <a href='{{url("viewuser/".$booking->user_detail->id)}}' target="_blank">
                                    <?php echo isset($booking['user_detail']->lastname)?$booking['user_detail']->lastname:"";?> <?php echo isset($booking['user_detail']->name)?$booking['user_detail']->name:"" ;?>
                                </a>

                                    
                            </td>

                            <td>
                                <?php 
                                $job_id = isset($booking->property->id) ? $booking->property->id : 0;
                                ?>
                                <a href='{{url("view/rentals/".$job_id)}}' target="_blank">
                                    {!! isset($booking->property->exptitle)?$booking->property->exptitle:"" !!}
                                </a> 

                            </td>
                           

                            <td>{!! isset($booking->Bookingno)?$booking->Bookingno:"" !!}</td>
                            <td>{!! isset($booking->checkin)?date("F j, Y", strtotime($booking->checkin)):"" !!}</td>
                            <td>{!! isset($booking->checkout)?date("F j, Y", strtotime($booking->checkout)):"" !!}</td>
                            <td>

                                
                                <?php $property_type = ""; ?>
                                @if(isset($booking->property->property_status) && $booking->property->property_status == "unpaid")
                                <?php $property_type = "Work Placement"; ?>
                                @elseif(isset($booking->property->property_status) && $booking->property->property_status == "unpaidflash")
                                <?php $property_type = "Work Placement"; ?>
                                @elseif(isset($booking->property->property_status) && $booking->property->property_status == "paid")
                                <?php $property_type = "School House"; ?>
                                @endif
                                {{$property_type}}


                            </td>

            
                            <td>
                                {!! isset($booking->cancelled)?$booking->cancelled:"" !!}</td>
                            <td>

                                
                                @if($booking->cancelled == "No")
                                <?php
                                $check_in = date("Y-m-d", strtotime($booking->checkin));
                                $check_out = date("Y-m-d", strtotime($booking->checkout));
                                if ($check_in == $check_out) {
                                    ?>
                                    <a href="{{ URL::to('admin/bookedJobs/cancelBooking/' . $booking->id) }}">
                                        <i class="glyphicon glyphicon-remove" title="Cancel booking">Cancel Booking</i>
                                    </a>
                                <?php } else { ?>
                                    <a href="{{ URL::to('admin/bookedJobs/cancelBooking/' . $booking->id) }}">
                                        <i class="glyphicon glyphicon-remove" title="Cancel booking">Cancel Dates</i>
                                    </a>
                                <?php } ?>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    <?php //} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
    $(document).ready(function() {
    $('#table').DataTable({
        "pageLength": 100,
        "order": [[ 3, "desc" ]]
    });
    });</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<script>
    $(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
    });
    });
</script>
@stop