@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->

<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                        {{$bredCrum}}
                    </h3>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>
                <div class="panel-body">

                    <!-- errors -->
                    <div class="has-error">
                        @include('partials.errors')
                    </div>
                    @include('layouts.errorsuccess')

                    <!--main content-->
                    <div class="col-md-12">

                        <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                        {!! Form::open(array('url' => 'admin/bookedJobs/cancelBookingForm', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <section>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">User who has booked *</label>
                                <div class="col-sm-10">
                                    <select name="user_id" class="form-control required" required="" disabled="">
                                        <option value="">Select User</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}" @if(isset($booking->user->id) && $booking->user->id == $user->id) selected="" @endif>{{$user->email}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Jobs / School Houses *</label>
                                <div class="col-sm-10">
                                    <select name="job_id" class="form-control required" required="" disabled="">
                                        <option value="">Select Jobs / School Houses / Services</option>
                                        @foreach($jobs as $job)
                                        <option value="{{$job->id}}" @if(isset($booking->property->id) && $booking->property->id == $job->id) selected="" @endif>{{$job->user->orgname}} ({{$job->user->name}} {{$job->user->lastname}}) ({{$job->user->email}}) ({{$job->exptitle}}) ({{$job->user->raddress18}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <?php
                            $check_in = date("Y-m-d", strtotime($booking->checkin));
                            $check_out = date("Y-m-d", strtotime($booking->checkout));
                            if ($check_in == $check_out) {
                                ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Start Date *</label>
                                    <div class="col-sm-10">
                                        {{isset($booking->checkin)?date('m/d/Y', strtotime($booking->checkin)):''}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">End Date *</label>
                                    <div class="col-sm-10">
                                        {{isset($booking->checkout)?date('m/d/Y', strtotime($booking->checkout)):''}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class=" text-right">
                                        <input type="hidden" name="cancel_type" value="cancel_booking" />
                                        <button type="submit" class="btn btn-responsive btn-primary">Cancel Booking</button>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Start Date *</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="c_startdate" name="startdate" readonly class="form-control" placeholder="MM/DD/YYYY" value="{{isset($booking->checkin)?date('m/d/Y', strtotime($booking->checkin)):''}}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">End Date *</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="c_enddate" name="enddate" readonly class="form-control" placeholder="MM/DD/YYYY" value="{{isset($booking->checkout)?date('m/d/Y', strtotime($booking->checkout)):''}}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class=" text-right">
                                        <input type="hidden" name="cancel_type" value="cancel_save_booking" />
                                        <button type="submit" class="btn btn-responsive btn-primary">Cancel and Save Dates</button>
                                    </div>
                                </div>                                
                            <?php } ?>

                        </section>
                        <input type="hidden" name="booking_id" value="{{$booking->id}}" />

                        </form>
                        <!-- END FORM WIZARD WITH VALIDATION -->
                    </div>
                    <!--main content end-->
                </div>
            </div>
        </div>
    </div>
    <!--row end-->
</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

<script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
    var dateFormat = "mm/dd/yy";
    $("#c_startdate").datepicker({changeMonth: true, changeYear: true}).on("change", function() {
    $("#c_enddate").datepicker("option", "minDate", getDate(this));
    });
    $("#c_enddate").datepicker({changeMonth: true, changeYear: true}).on("change", function() {
    $("#c_startdate").datepicker("option", "maxDate", getDate(this));
    });
    function getDate(element) {
    var date;
    try {
    date = $.datepicker.parseDate(dateFormat, element.value);
    } catch (error) {
    date = null;
    }

    return date;
    }
    });
</script>

@stop