@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    @include('admin.partials.bredcrum')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                            {{$bredCrum}}
                        </h3>
                                <span class="pull-right clickable">
                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                </span>
                    </div>
                    <div class="panel-body">

                        <!-- errors -->
                        <div class="has-error">
                            @include('partials.errors')
                        </div>

                        <!--main content-->
                        <div class="col-md-12">

                            <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                             {!! Form::open(array('url' => 'admin/amenities/'.$editObj->id, 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                             <input type="hidden" name="_method" value="put" />
                                <!-- CSRF Token -->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <section>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Name *</label>
                                        <div class="col-sm-10">
                                        <input id="name" name="name" value="{{$editObj->name}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="country" class="col-sm-2 control-label">Type</label>
                                        <div class="col-sm-10">
                                           
    {!! Form::select('type', $codeArr, $editObj->type, ['class'=>'form-control', 'name' => 'type', 'id' => 'type','required']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="pic" class="col-sm-2 control-label">Icon</label>
                                        <div class="col-sm-10">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;">
                                                    
                                                        <img  src="{{asset('images/amenity/'.$editObj->icon)}}" alt="icon">
                                                     
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input id="pic" name="icon" type="file" class="form-control" />
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Status</label>
                                        <label class="radio-inline">
                                            <input name="status" id="status" value="1" 
                                            <?php if ($editObj->status == 1) { ?> checked="checked" <?php }?> type="radio">Active</label>
                                        <label class="radio-inline">
                                            <input name="status" id="status" value="2" type="radio" <?php if ($editObj->status == 2) { ?> checked="checked" <?php }?> >DeActive</label>
                                       
                                    </div>
                                <div class="form-group">
                                    <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                    </div>
                                    </div>

                                 </section>
                                

                            </form>
                            <!-- END FORM WIZARD WITH VALIDATION -->
                        </div>
                        <!--main content end-->
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop