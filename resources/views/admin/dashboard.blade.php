@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
   {{$bredCrum}}
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    
    
    <link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/only_dashboard.css') }}"/>
    <meta name="_token" content="{{ csrf_token() }}">
    

@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Welcome to Dashboard</h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="#">
                    <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
                    Dashboard
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
         <div class="col-lg-4 col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
                <!-- Trans label pie charts strats here-->
                <div class="palebluecolorbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>Users</span>

                                    <div class="number" id="">{{$usersCount}}</div>
                                </div>
                                <i class="livicon pull-right" data-name="users" data-l="true" data-c="#fff"
                                   data-hc="#fff" data-s="70"></i>
                            </div>
                                <div class="row">
                                        <div class="col-xs-6">
                                            <small class="stat-label">Active</small>
                                            <h4 id="myTargetElement1.1">{{$activeusers}}</h4>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <small class="stat-label">DeActive</small>
                                            <h4 id="myTargetElement1.2">{{$deactiveusers}}</h4>
                                        </div>
                                    </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
           <div class="col-lg-4 col-sm-6 col-md-6 margin_10 animated fadeInDownBig">
                <!-- Trans label pie charts strats here-->
                <div class="goldbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>properties</span>

                                    <div class="number">{{$propertyCount}}</div>
                                </div>
                                <i class="livicon pull-right" data-name="archive-add" data-l="true" data-c="#fff"
                                   data-hc="#fff" data-s="70"></i>
                            </div>
                            <div class="row">
                                        <div class="col-xs-6">
                                            <small class="stat-label">Active</small>
                                            <h4 id="myTargetElement1.1">{{$activepro}}</h4>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <small class="stat-label">DeActive</small>
                                            <h4 id="myTargetElement1.2">{{$deactivepro}}</h4>
                                        </div>
                                    </div>
                             
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-md-6 margin_10 animated fadeInDownBig">
                <!-- Trans label pie charts strats here-->
                <div class="redbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span><a href="{{url('admin/reservation')}}" >Reservations</a></span>

                                    <div class="number">{{$reservation}}</div>
                                </div>
                                <i class="livicon pull-right" data-name="archive-add" data-l="true" data-c="#fff"
                                   data-hc="#fff" data-s="70"></i>
                            </div>
                                <div class="row">
                                        <div class="col-xs-4">
                                            <small class="stat-label">Pending </small>
                                            <h4 id="myTargetElement1.1">{{$preservation}}</h4>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <small class="stat-label">Accepted</small>
                                            <h4 id="myTargetElement1.2">{{$areservation}}</h4>
                                        </div>
                                         <div class="col-xs-4 text-right">
                                            <small class="stat-label">Declined</small>
                                            <h4 id="myTargetElement1.2">{{$dreservation}}</h4>
                                        </div>
                                    </div>
                             
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <!--/row-->
         
        <div class="clearfix"></div>
        <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 margin_10 animated fadeInRightBig" style="min-height:100px;">
                <!-- Trans label pie charts strats here-->
                <div class="palebluecolorbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>Transactions</span>

                                    <div class="number" id="">{{$ttransactions}}</div>
                                </div>
                                <i class="livicon pull-right" data-name="users" data-l="true" data-c="#fff"
                                   data-hc="#fff" data-s="70"></i>
                            </div>
                                <div class="row">
                                        <div class="col-xs-6">
                                            <small class="stat-label">Completed Transactions</small>
                                            <h4 id="myTargetElement1.1">{{$ctransactions}}</h4>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <small class="stat-label">Pending Transactions</small>
                                            <h4 id="myTargetElement1.2">{{$ptransactions}}</h4>
                                        </div>
                                         
                                    </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
           
         
           
        </div>
         
    </section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
     

     
    <script src="{{ asset('assets/js/dashboard.js') }}" type="text/javascript"></script>


@stop