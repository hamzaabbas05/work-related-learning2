@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                        {{$bredCrum}}
                    </h3>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>
                <div class="panel-body">

                    <!-- errors -->
                    <div class="has-error">
                        @include('partials.errors')
                    </div>

                    <!--main content-->
                    <div class="col-md-12">

                        <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                        {!! Form::model($school_group, ['action' => ['SecondarySchoolGroupsController@update', $school_group] , 'class' => 'form-wizard form-horizontal','files' => 'true']) !!}
                        <!-- CSRF Token -->
                        <input type="hidden" name="_method" value="put" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <section>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Group Name</label>
                                <div class="col-sm-10">
                                    <input id="group_name" name="group_name" placeholder="DateTo/DateBack - Name of School - Provincia. Eg. 03/03/18 to 10/03/18 IS Valleseriana (BG)" value="{{$school_group->group_name}}" type="text" class="form-control" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Group Date In</label>
                                <div class="col-sm-10">
                                    <input id="startdate" name="group_date_in" placeholder="Group Date In/Arrivo in the UK - Eg. mm/dd/yyyy ovvero mm/gg/aaaa (da calendario o usando esattamente il formato richiesto)" value="{{$school_group->group_date_in}}" type="text" class="form-control" required>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">Group Date Out</label>
                                <div class="col-sm-10">
                                    <input type="text" id="enddate" name="group_date_out" placeholder="Group Date Out/Ritorno - Eg. mm/dd/yyyy ovvero mm/gg/aaaa (da calendario o usando esattamente il formato richiesto)" type="text" class="form-control" value="{{$school_group->group_date_out}}" required> 
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Pick up Point</label>
                                <div class="col-sm-10">
                                    <input id="pick_up_where" name="pick_up_where" placeholder="Pick up Point - Eg: 5 Victoria Embankment, Nottingham NG2 2JY, Regno Unito" value="{{$school_group->pick_up_where}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Drop Off Point</label>
                                <div class="col-sm-10">
                                    <input id="drop_off_where" name="drop_off_where" placeholder="Where Students are taken last day to come back - Eg: 5 Victoria ecc." value="{{$school_group->drop_off_where}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Pick Up Time</label>
                                <div class="col-sm-10">
                                    <input id="pick_up_time" name="pick_up_time" placeholder="Pick Up Time - eg. if on a Sat: 5.30 pm (check skylink and Ryanair)" value="{{$school_group->pick_up_time}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Drop Off Time</label>
                                <div class="col-sm-10">
                                    <input id="drop_off_time" name="drop_off_time" placeholder="Drop Off Time - eg. if on a Tue: 8.50 am; if on a Sat: 11.00 am  (check on Ryanair and skylink express timetable to get to Airport 3 hours earlier)" value="{{$school_group->drop_off_time}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Route from Airport - Gmaps link (eg. https://goo.gl/maps/vDENDSaYdwu)</label>
                                <div class="col-sm-10">
                                    <input id="link_to_route_from_meeting_point" placeholder="Route from Airport as URL, eg. https://www.google.it/maps/dir/Aeroporto+delle+Midlands+Orientali,+Castle+Donington,+Derby+DE74+2SA,+Regno+Unito/County+Hall,+West+Bridgford,+Nottingham+NG2+7QP,+Regno+Unito/@52.8806951,-1.3735212,35387m/am=t/data=!3m2!1e3!4b1!4m18!4m17!1m5!1m1!1s0x4879e5b2f87b0e53:0x32972cb4ce3df85!2m2!1d-1.332134!2d52.829374!1m5!1m1!1s0x4879c3c6377ece1d:0x1af110f4f15af706!2m2!1d-1.134744!2d52.936428!2m3!6e1!7e2!8j1518888600!3e3" name="link_to_route_from_meeting_point" value="{{$school_group->link_to_route_from_meeting_point}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Route back to Airport - Gmaps link</label>
                                <div class="col-sm-10">
                                    <input id="link_to_route_to_meeting_point" placeholder="Route back to Airport again as URL" name="link_to_route_to_meeting_point" value="{{$school_group->link_to_route_to_meeting_point}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Students will be getting there (using bus or taxi)</label>
                                <div class="col-sm-10">
                                    <input type="text" id="catching_the_what_bus_to" placeholder="Students will be getting there (using bus or taxi)" name="catching_the_what_bus_to" type="text" class="form-control" value="{{$school_group->catching_the_what_bus_to}}"  > 
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Students will be getting there at - Pick up time</label>
                                <div class="col-sm-10">
                                    <input id="catching_the_what_time_bus_to" placeholder="Eg. at 5.30 pm" name="catching_the_what_time_bus_to" value="{{$school_group->catching_the_what_time_bus_to}}" type="text" class="form-control"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Students will be catching: the bus/taxi Back</label>
                                <div class="col-sm-10">
                                    <input id="catching_the_what_bus_back" placeholder="Eg. the Skylnk back to the ariport" name="catching_the_what_bus_back" value="{{$school_group->catching_the_what_bus_back}}" type="text" class="form-control"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Catching The Bus Back Time</label>
                                <div class="col-sm-10">
                                    <input id="catching_the_what_time_bus_back" placeholder="Eg. at 9.07 am" name="catching_the_what_time_bus_back" value="{{$school_group->catching_the_what_time_bus_back}}" type="text" class="form-control"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Flight To</label>
                                <div class="col-sm-10">
                                    <input id="flight_to" name="flight_to" placeholder="Flight to Nottingham - Eg. Ryanair flight leaving BGY at 11.00 am and landing in EMA at 12.05 am (local times)" value="{{$school_group->flight_to}}" type="text" class="form-control"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Flight Back</label>
                                <div class="col-sm-10">
                                    <input id="flight_back" name="flight_back" placeholder="Flight back to Italy - Eg. Ryanair flight leaving EMA at 11.00 am and landing in BGY at 11.05 am (local times)" value="{{$school_group->flight_back}}" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                </div>
                            </div>

                        </section>

                        {!! Form::close() !!}
                        <!-- END FORM WIZARD WITH VALIDATION -->
                    </div>
                    <!--main content end-->
                </div>
            </div>
        </div>
    </div>
    <!--row end-->
</section>
<script>
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    var places1 = new google.maps.places.Autocomplete(document.getElementById('legal_address'));
    google.maps.event.addListener(places1, 'place_changed', function () {
        var place = places1.getPlace();


        for (var component in componentForm) {
            var cstr = "l";
            document.getElementById(cstr + component).value = '';
            document.getElementById(cstr + component).disabled = false;
        }

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var cstr = "l";
            if (componentForm[addressType]) {

                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(cstr + addressType).value = val;
            }

        }

    });

    var places2 = new google.maps.places.Autocomplete(document.getElementById('operative_address'));
    google.maps.event.addListener(places2, 'place_changed', function () {
        var place = places2.getPlace();


        for (var component in componentForm) {
            var cstr = "o";
            document.getElementById(cstr + component).value = '';
            document.getElementById(cstr + component).disabled = false;
        }

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var cstr = "o";
            if (componentForm[addressType]) {

                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(cstr + addressType).value = val;
            }

        }

    });
</script> 
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop
