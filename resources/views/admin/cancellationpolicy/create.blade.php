@extends('admin/layouts/default') 
{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop
@inject('settings', 'App\Settings')
<?php $settingsObj = $settings->settingsInfo();?>
{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    @include('admin.partials.bredcrum')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                            {{$bredCrum}}
                        </h3>
                                <span class="pull-right clickable">
                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                </span>
                    </div>
                    <div class="panel-body">

                        <!-- errors -->
                        <div class="has-error">
                            @include('partials.errors')
                        </div>

                        <!--main content-->
                        <div class="col-md-12">

                            <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                            {!! Form::open(array('url' => 'admin/cancellation', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                                <!-- CSRF Token -->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <section>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Cancellation Name *</label>
                                        <div class="col-sm-10">
                                        <input id="name" name="name" value="{{Input::old('name')}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                     <div class="form-group {{ $errors->first('currency_id', 'has-error') }}">
                                        <label for="country" class="col-sm-2 control-label">Currency</label>
                                        <div class="col-sm-10">
                                           {!! Form::label('Currency','Currency') !!}
    {!! Form::select('currency_name', $currencies, $settingsObj->currency_id, ['class'=>'form-control', 'name' => 'currency_id', 'id' => 'currency_id','required', 'disabled' =>'true']) !!}
    <input type="hidden" name="currency_id" value="{{$settingsObj->currency_id}}">
                                        </div>
                                        <span class="help-block">{{ $errors->first('currency_id', ':message') }}</span>
                                    </div>
                                     <div class="form-group">
                                        <label for="country" class="col-sm-2 control-label">Type</label>
                                        <div class="col-sm-10">
                                         {!! Form::select('cancellation_type', $codeArr, ['class'=>'form-control', 'name' => 'cancellation_type', 'id' => 'cancellation_type','required']) !!}
                                        </div>
                                      
                                    </div>
                                      <div class="form-group">
                                        <label class="col-sm-2 control-label">Cancellation Charge *</label>
                                        <div class="col-sm-10">
                                        <input id="name" name="cancellation_charge" value="{{Input::old('cancellation_charge')}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Description *</label>
                                        <div class="col-sm-10">
                                           <textarea class="form-control required" name="desc" rows="3">{{Input::old('cancellation_charge')}}</textarea>
                                        </div>
                                    </div>
                                  <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-10">
                                        <label class="radio-inline">
                                            <input name="status" id="optionsRadiosInline1" value="1" checked="checked" type="radio">Active</label>
                                        <label class="radio-inline">
                                            <input name="status" id="optionsRadiosInline2" value="2" type="radio">DeActive</label></div>
                                       
                                    </div>
                                <div class="form-group">
                                    <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                    </div>
                                    </div>

                                 </section>
                                

                            </form>
                            <!-- END FORM WIZARD WITH VALIDATION -->
                        </div>
                        <!--main content end-->
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop