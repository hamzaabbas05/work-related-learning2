@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                        {{$bredCrum}}
                    </h3>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>
                <div class="panel-body">

                    <!-- errors -->
                    <div class="has-error">
                        @include('partials.errors')
                    </div>

                    <!--main content-->
                    <div class="col-md-12">

                        <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                        {!! Form::open(array('url' => 'admin/school/'.$editObj->id, 'class' => 'form-wizard form-horizontal', 'method' => 'POST')) !!}
                        <!-- CSRF Token -->
                        <input type="hidden" name="_method" value="put" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <section>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"> Name *</label>
                                <div class="col-sm-10">
                                    <input id="name" name="name" value="{{$editObj->name}}" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">School Email</label>
                                <div class="col-sm-10">
                                    <input id="school_email" name="school_email" value="{{$editObj->school_email}}" type="text" class="form-control" required>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">Legal Address - Start typing, add postal code, and select from list</label>
                                <div class="col-sm-10">
                                    <input type="text" id="legal_address"   name="legal_address" type="text" class="form-control" value="{{$editObj->legal_address}}" required> 
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Street Address Number</label>
                                <div class="col-sm-10">
                                    <input id="lstreet_number" name="lstreet_number" value="{{$editObj->lstreet_number}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Street Address Name</label>
                                <div class="col-sm-10">
                                    <input id="lroute" name="lroute" value="{{$editObj->lroute}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Town/City</label>
                                <div class="col-sm-10">
                                    <input id="llocality" name="llocality" value="{{$editObj->llocality}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">State/Region</label>
                                <div class="col-sm-10">
                                    <input id="ladministrative_area_level_1" name="ladministrative_area_level_1" value="{{$editObj->ladministrative_area_level_1}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">ZIP / Postal Code</label>
                                <div class="col-sm-10">
                                    <input id="lpostal_code" name="lpostal_code" value="{{$editObj->lpostal_code}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Country</label>
                                <div class="col-sm-10">
                                    <input id="lcountry" name="lcountry" value="{{$editObj->lcountry}}" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2"> </div>
                                <div class="col-sm-10">
                                    <input <?php if ($editObj->same_address == 0) { ?> checked <?php } ?> type="checkbox"   name="same" id="same"><p>check Operative address is same as LegalAddress?
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">operative Address -  Start typing, add postal code, and select from list</label>
                                <div class="col-sm-10">
                                    <input type="text" id="operative_address"   name="operative_address" type="text" class="form-control" value="{{$editObj->operative_address}}"  > 
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Street Address Number</label>
                                <div class="col-sm-10">
                                    <input id="ostreet_number" name="ostreet_number" value="{{$editObj->ostreet_number}}" type="text" class="form-control"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Street Address Name</label>
                                <div class="col-sm-10">
                                    <input id="oroute" name="oroute" value="{{$editObj->oroute}}" type="text" class="form-control"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Town/City</label>
                                <div class="col-sm-10">
                                    <input id="olocality" name="olocality" value="{{$editObj->olocality}}" type="text" class="form-control"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">State/Region</label>
                                <div class="col-sm-10">
                                    <input id="oadministrative_area_level_1" name="oadministrative_area_level_1" value="{{$editObj->oadministrative_area_level_1}}" type="text" class="form-control"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">ZIP / Postal Code</label>
                                <div class="col-sm-10">
                                    <input id="opostal_code" name="opostal_code" value="{{$editObj->opostal_code}}" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Country</label>
                                <div class="col-sm-10">
                                    <input id="ocountry" name="ocountry" value="{{$editObj->ocountry}}" type="text" class="form-control"  >
                                </div>
                            </div>





                            <div class="form-group">
                                <label class="col-sm-2 control-label">Mecanographic code (Codice Meccanografico)</label>
                                <div class="col-sm-10">
                                    <input id="Mecanographiccode" name="Mecanographiccode" value="{{$editObj->Mecanographiccode}}" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Phone number</label>
                                <div class="col-sm-10">
                                    <input id="phonenumber" name="phonenumber" value="{{$editObj->phonenumber}}" type="number" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fax number</label>
                                <div class="col-sm-10">
                                    <input id="faxnumber" name="faxnumber" value="{{$editObj->faxnumber}}" type="number" class="form-control" required>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">HeadMaster (Dirigente Scolastico)</label>
                                <div class="col-sm-10">
                                    <input id="HeadMaster" name="HeadMaster" value="{{$editObj->HeadMaster}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Insurance - with Company</label>
                                <div class="col-sm-10">
                                    <input id="insurancewithcompany" name="insurancewithcompany" value="{{$editObj->insurancewithcompany}}" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Insurance - Number of insurance policy
                                </label>
                                <div class="col-sm-10">
                                    <input id="noofpolicy" name="noofpolicy" value="{{$editObj->noofpolicy}}" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Insurance - Number of insurance contract for Third Party Liability
                                </label>
                                <div class="col-sm-10">
                                    <input id="noofpolicythirdparty" name="noofpolicythirdparty" value="{{$editObj->noofpolicythirdparty}}" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Insurance - Legal expences cover number
                                </label>
                                <div class="col-sm-10">
                                    <input id="legalexpcovernumber" name="legalexpcovernumber" value="{{$editObj->legalexpcovernumber}}" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">
                                    Insurance - contact details <br />
                                    <span style="font-size: 12px">Insurance - contact details of person/organisation Student has to report: Injuries and sick leave and any problems relating to their Work experience</span>
                                </label>
                                <div class="col-sm-10">
                                    <textarea id="insurance_contact_detail" name="insurance_contact_detail" class="form-control" required="" style="height: 200px; resize: none;">{{isset($editObj->insurance_contact_detail)?$editObj->insurance_contact_detail:""}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">
                                    Insurance - procedure to report <br />
                                    <span style="font-size: 12px">Insurance - procedure to report: Injuries and sick leave and any problems relating to their Work experience</span>
                                </label>
                                <div class="col-sm-10">
                                    <textarea id="insurance_procedure_report" name="insurance_procedure_report" class="form-control" required="" style="height: 200px; resize: none;">{{isset($editObj->insurance_procedure_report)?$editObj->insurance_procedure_report:""}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">School Representative for Work - Name
                                </label>
                                <div class="col-sm-10">
                                    <input id="tutor_name" name="tutor_name" value="{{$editObj->tutor_name}}" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">School Representative for Work - Place of Birth
                                </label>
                                <div class="col-sm-10">
                                    <input id="tutor_pob" name="tutor_pob" value="{{$editObj->tutor_pob}}" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">School Representative for Work - Surname
                                </label>
                                <div class="col-sm-10">
                                    <input id="tutor_surname" name="tutor_surname" value="{{$editObj->tutor_surname}}" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Personal Tax identification number
                                </label>
                                <div class="col-sm-10">
                                    <input id="tutor_pan" name="tutor_pan" value="{{$editObj->tutor_pan}}" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">School Representative for Work - e-mail
                                </label>
                                <div class="col-sm-10">
                                    <input id="tutor_email" name="tutor_email" value="{{$editObj->tutor_email}}" type="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">School Representative for Work - Telephone Number
                                </label>
                                <div class="col-sm-10">
                                    <input id="tutor_phone" name="tutor_phone" value="{{$editObj->tutor_phone}}" type="number" class="form-control" required> 
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">School Representative for Work - Emergency telephone Number
                                </label>
                                <div class="col-sm-10">
                                    <input id="tutor_emergency_phone" name="tutor_emergency_phone" value="{{$editObj->tutor_emergency_phone}}" type="number" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <label class="radio-inline">
                                    <input name="status" id="status" value="1" 
                                           <?php if ($editObj->status == 1) { ?> checked="checked" <?php } ?> type="radio">Active</label>
                                <label class="radio-inline">
                                    <input name="status" id="status" value="2" type="radio" <?php if ($editObj->status == 2) { ?> checked="checked" <?php } ?> >DeActive</label>

                            </div>
                            <div class="form-group">
                                <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                </div>
                            </div>

                        </section>


                        </form>
                        <!-- END FORM WIZARD WITH VALIDATION -->
                    </div>
                    <!--main content end-->
                </div>
            </div>
        </div>
    </div>
    <!--row end-->
</section>
<script>
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    var places1 = new google.maps.places.Autocomplete(document.getElementById('legal_address'));
    google.maps.event.addListener(places1, 'place_changed', function () {
        var place = places1.getPlace();


        for (var component in componentForm) {
            var cstr = "l";
            document.getElementById(cstr + component).value = '';
            document.getElementById(cstr + component).disabled = false;
        }

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var cstr = "l";
            if (componentForm[addressType]) {

                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(cstr + addressType).value = val;
            }

        }

    });

    var places2 = new google.maps.places.Autocomplete(document.getElementById('operative_address'));
    google.maps.event.addListener(places2, 'place_changed', function () {
        var place = places2.getPlace();


        for (var component in componentForm) {
            var cstr = "o";
            document.getElementById(cstr + component).value = '';
            document.getElementById(cstr + component).disabled = false;
        }

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var cstr = "o";
            if (componentForm[addressType]) {

                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(cstr + addressType).value = val;
            }

        }

    });
</script> 
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop
