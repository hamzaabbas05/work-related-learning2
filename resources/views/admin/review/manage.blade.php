@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
           
           
 <section class="content-header">
 </section>
<!-- Main content -->

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> 
                   {{$bredCrum}}
                </h4>
            </div>
            
            <br />
            <div class="panel-body">

                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                          <th>SNO</th>

                            <th>Reviewer User</th>
                            <th>Reviewed User</th>
                            <th>Property</th>
                            <th>BookingNo</th>
                            <th>Description</th>
                            <th>Date Reviewed</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $url = "reviewstatus"; $i =1;?>
                    @foreach ($reviews as $obj)
                    <tr>
                    <td>{!! $i !!}</td>
                           <td>{!! $obj->reviewer->name !!}</td>
                            <td>{!! $obj->revieweduser->name !!}</td>
                            <td>{!! $obj->property->title !!}</td>
                            <td>{!! $obj->enquiry->Bookingno !!}</td>
                            <td>{!! $obj->description !!}</td>
                            <td>{!! $obj->dateAdded !!}</td>
                            <td >
                                @if($obj->status == 1)
                                     <span class="statustextactive">Active</span>
                                @elseif($obj->status == 2)
                                    <span class="statustextpending">DeActive</span>
                               @endif
                                
                            </td>
                            <td class="ui-group-buttons"> 
                                           <a  onclick="changeStatus(1, {{$obj->id}}, '{{$url}}')" class="btn btn-success" role="button">
                                                <span class="glyphicon glyphicon-thumbs-up"></span>
                                            </a>
                                            
                                            <a onclick="changeStatus(2, {{$obj->id}}, '{{$url}}')" class="btn btn-default" role="button">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                        </td>
            			  

            			</tr>
                        <?php $i++;?>
                    @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
$(document).ready(function() {
	$('#table').DataTable();
});
</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop