@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                        {{$bredCrum}}
                    </h3>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>
                <div class="panel-body">

                    <!-- errors -->
                    <div class="has-error">
                        @include('partials.errors')
                    </div>

                    <!--main content-->
                    <div class="col-md-12">

                        <!-- BEGIN FORM WIZARD WITH VALIDATION -->

                        {!! Form::open(array('url' => 'admin/voucherCredit/'.$editObj->id, 'class' => 'form-wizard form-horizontal', 'files'=>true, 'method' => 'POST')) !!}
                        <input type="hidden" name="_method" value="put" />

                        <section>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">User who use it </label>
                                <div class="col-sm-10 control-label" style="float: left; text-align: left;">
                                    @if(isset($editObj->user->id))
                                    {{$editObj->user->name}} {{$editObj->user->lastname}}
                                    @else
                                    Not used yet
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Title </label>
                                <div class="col-sm-10">
                                    <input id="voucher_code" name="voucher_code" type="text" class="form-control" value="{{$editObj->voucher_code}}" required="" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Amount </label>
                                <div class="col-sm-10">
                                    <input id="amount" name="amount" type="number" class="form-control" required="" value="{{$editObj->amount}}" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Send to Unpaid </label>
                                <div class="col-sm-10">
                                    <input id="send_to_unpaid" name="send_to_unpaid" type="number" class="form-control" value="{{$editObj->send_to_unpaid}}" required="" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Book Unpaid </label>
                                <div class="col-sm-10">
                                    <input id="voucher_code" name="book_unpaid" type="number" class="form-control" value="{{$editObj->book_unpaid}}" required="" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Send to Schoolhouse </label>
                                <div class="col-sm-10">
                                    <input id="voucher_code" name="send_to_schoolhouse" type="number" class="form-control" value="{{$editObj->send_to_schoolhouse}}" required="" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Book Paid </label>
                                <div class="col-sm-10">
                                    <input id="voucher_code" name="book_paid" type="number" class="form-control" required="" value="{{$editObj->book_paid}}" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                </div>
                            </div>

                        </section>


                        </form>
                        <!-- END FORM WIZARD WITH VALIDATION -->
                    </div>
                    <!--main content end-->
                </div>
            </div>
        </div>
    </div>
    <!--row end-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop