@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                        {{$bredCrum}}
                    </h3>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>
                <div class="panel-body">

                    <!-- errors -->
                    <div class="has-error">
                        @include('partials.errors')
                    </div>

                    <!--main content-->
                    <div class="col-md-12">

                        <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                        {!! Form::open(array('url' => 'admin/voucherCredit', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                        <!-- CSRF Token -->
                        <section>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Voucher Code </label>
                                <div class="col-sm-10">
                                    <input id="voucher_code" name="voucher_code" type="text" class="form-control" required="" autocomplete="off" />

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Amount </label>
                                <div class="col-sm-10">
                                    <input id="amount" name="amount" type="number" class="form-control" required="" value="0" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Send to Unpaid </label>
                                <div class="col-sm-10">
                                    <input id="send_to_unpaid" name="send_to_unpaid" type="number" class="form-control" value="3" required="" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Book Unpaid </label>
                                <div class="col-sm-10">
                                    <input id="voucher_code" name="book_unpaid" type="number" class="form-control" value="1" required="" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Send to Schoolhouse </label>
                                <div class="col-sm-10">
                                    <input id="voucher_code" name="send_to_schoolhouse" type="number" class="form-control" value="0" required="" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Book Paid </label>
                                <div class="col-sm-10">
                                    <input id="voucher_code" name="book_paid" type="number" class="form-control" required="" value="8" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">User Invitation Email </label>
                                <div class="col-sm-10">
                                    <input id="voucher_code" name="email" type="email" class="form-control" value="" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">User *</label>
                                <div class="col-sm-10">
                                    <?php

                                    /*if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                                      */
                                    ?>
                                    <select name="user_id" class="form-control" >
                                        <option value="">Select User</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}">
                                            ({{$user->id}}) ({{$user->email}}) ({{$user->name}} {{$user->lastname}}) ({{$user->ssemail16}}) ({{$user->sname16}} {{$user->ssurname16}})  ({{$user->school['name']}})
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Send to NEW user NOT Pre-Enrolled</button>
                                </div>
                            </div>

                        </section>


                        </form>
                        <!-- END FORM WIZARD WITH VALIDATION -->
                    </div>
                    <!--main content end-->
                </div>
            </div>
        </div>
    </div>
    <!--row end-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop