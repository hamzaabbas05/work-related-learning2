<div class="col-xs-12 col-sm-3 margin_top30 margin_bottom30 sdecls"
     style="width: 18%;">
    <ul class="profile_left list-unstyled" id="listpropul">

        <li   {!! (Request::is('admin/property/basic/*') ? 'style="background: #ddd"' : '') !!}  id="showBasi"><a href="{{URL::to('admin/property/basic/'.$propertyObj->id)}}">Basics</a></li>
        <li  {!! (Request::is('admin/property/description/*') ? 'style="background: #ddd"' : '') !!} id="showDesc"><a href="{{URL::to('admin/property/description/'.$propertyObj->id)}}">Description</a></li>
        <!-- <li  {!! (Request::is('admin/property/amenities/*') ? 'style="background: #ddd"' : '') !!} id="showAmenities"><a href="{{URL::to('admin/property/amenities/'.$propertyObj->id)}}">Amenities</a></li> -->
        <li  {!! (Request::is('admin/property/photos/*') ? 'style="background: #ddd"' : '') !!} id="showPhoto"><a href="{{URL::to('admin/property/photos/'.$propertyObj->id)}}">Photos</a></li>
        <li  {!! (Request::is('admin/property/location/*') ? 'style="background: #ddd"' : '') !!} id="showLoc"><a href="{{URL::to('admin/property/location/'.$propertyObj->id)}}">Location</a></li>
        <!-- <li  {!! (Request::is('admin/property/pricing/*') ? 'style="background: #ddd"' : '') !!} id="showPricing"><a href="{{URL::to('admin/property/pricing/'.$propertyObj->id)}}">Pricing</a></li> -->
        <li {!! (Request::is('admin/property/bookings/*') ? 'style="background: #ddd"' : '') !!}  id="showBooking"><a href="{{URL::to('admin/property/booking/'.$propertyObj->id)}}">Workplace Rules</a></li>
        <li  {!! (Request::is('admin/property/calendar/*') ? 'style="background: #ddd"' : '') !!} id="showCalendar"><a href="{{URL::to('admin/property/calendar/'.$propertyObj->id)}}">Calendar</a></li>
    </ul>

</div>