<aside class="left-side sidebar-offcanvas">
<section class="sidebar ">
<div class="page-sidebar  sidebar-nav">
<!-- BEGIN SIDEBAR MENU -->
<ul class="page-sidebar-menu" id="menu">
<!--<li>
<a href="index-2.html">
<i class="material-icons text-primary leftsize">home</i>
<span class="title">Dashboard</span>
</a>
</li>-->
<li>
<a href="#">
<i class="material-icons text-danger leftsize">group</i>
<span class="title">Admin Mangement</span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li >
<a href="{{url('admin')}}">
<i class="material-icons">keyboard_arrow_right</i>Admin Profile
</a>
</li>
<li >
<a href="{{url('changepassword')}}">
<i class="material-icons">keyboard_arrow_right</i>Change Password
</a>
</li>
<li >
<a href="{{url('roles')}}">
<i class="material-icons">keyboard_arrow_right</i>Roles and Permissions
</a>
</li>
<li>
<a href="{{url('subadmin')}}">
<i class="material-icons">keyboard_arrow_right</i>
Create SubAdmin
</a>
</li>
</ul>
</li>
<li >
<a href="#">
<i class="material-icons text-primary leftsize">settings</i>
<span class="title">Site Settings</span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li>
<a href="{{url('admin/generalsettings')}}">
<i class="material-icons">keyboard_arrow_right</i>General Settings
</a>
</li>
<li  >
<a href="{{url('admin/emailsettings')}}">
<i class="material-icons">keyboard_arrow_right</i>
Email & Contact Settings
</a>
</li>
<li  >
<a href="{{url('admin/socialmediasettings')}}">
<i class="material-icons">keyboard_arrow_right</i>Social Media Settings
</a>
</li>
<li  >
<a href="{{url('admin/appsettings')}}">
<i class="material-icons">keyboard_arrow_right</i>API credentials
</a>
</li>
<li  >
<a href="{{url('admin/paymentsettings')}}">
<i class="material-icons">keyboard_arrow_right</i>Payment Settings
</a>
</li>
<li >
<a href="{{url('admin/noimagesettings')}}">
<i class="material-icons">keyboard_arrow_right</i>No Image Settings
</a>
</li>
</ul>
</li>

<!--<li>
<a href="#">
<i class="material-icons text-success leftsize">group</i>
<span class="title">User Mangement</span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i>Manage Users
</a>
</li>
</ul>
</li>
<li>
<a href="#">
<i class="material-icons text-success leftsize">camera enhance</i>
<span class="title">Property Mangement</span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i>Manage Properties
</a>
</li>
</ul>
</li>-->
<li >
<a href="#">
<i class="material-icons text-danger leftsize">input</i>
<span class="title">Property Attributes</span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li >
<a href="{{url('baseattributes')}}">
<i class="material-icons">keyboard_arrow_right</i>Base Attributes
</a>
</li>

<li >
<a href="{{url('propertyattributes')}}">
<i class="material-icons">keyboard_arrow_right</i>Manage Property Types
</a>
</li>
<li  >
<a href="{{url('propertyattributes')}}">
<i class="material-icons">keyboard_arrow_right</i>Manage Room Types
</a>
</li>
<li>
<a href="{{url('propertyattributes')}}">
<i class="material-icons">keyboard_arrow_right</i>Manage Bed TYpes
</a>
</li>
<li>
<a href="{{url('specialattributes')}}">
<i class="material-icons">keyboard_arrow_right</i>Manage Amenities
</a>
</li>
<li>
<a href="{{url('specialattributes')}}">
<i class="material-icons">keyboard_arrow_right</i>Safety Checklists
</a>
</li>
<li>
<a href="{{url('specialattributes')}}">
<i class="material-icons">keyboard_arrow_right</i>Special Features
</a>
</li>
<li >
<a href="{{url('cancellation')}}">
<i class="material-icons">keyboard_arrow_right</i>Cancellation Policy
</a>
</li>

</ul>
</li>
<li>
<a href="#">
<i class="material-icons text-danger leftsize">brush</i>
<span class="title">Image Mangement</span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li >
<a href="{{url('banners')}}">
<i class="material-icons">keyboard_arrow_right</i>Banner Images
</a>
</li>
<li >
<a href="{{url('destinationbanners')}}">
<i class="material-icons">keyboard_arrow_right</i>Home Destination Banners
</a>
</li>
</ul>
</li>
<li>
<a href="#">
<i class="material-icons text-danger leftsize">description</i>
<span class="title">Language And Currency</span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li >
<a href="{{url('currency')}}">
<i class="material-icons">keyboard_arrow_right</i>Manage Currency
</a>
</li>
<li >
<a href="{{url('language')}}">
<i class="material-icons">keyboard_arrow_right</i>Manage Language
</a>
</li>

</ul>
</li>
<li>
<a href="{{url('coupon')}}">
<i class="material-icons text-warning leftsize">description</i>
<span class="title">Manage Coupons</span>

</a>

</li>

<li>
<a href="{{url('destination')}}">
<i class="material-icons text-warning leftsize">launch</i>
<span class="title">Manage Destinations</span>

</a>

</li>
<!--
<li>
<a href="#">
<i class="material-icons text-success leftsize">payment</i>
<span class="title">Commission & Penalty Mangement</span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i>Commission List
</a>
</li>
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i>Host Penalty
</a>
</li>
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i>Commission Tracking
</a>
</li>
</ul>
</li>
<li>
<a href="#">
<i class="material-icons text-success leftsize">payment</i>
<span class="title">Account Mangement</span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i>Receivable & Payable
</a>
</li>


</ul>
</li>
<li>
<a href="#">
<i class="material-icons text-success leftsize">payment</i>
<span class="title">Finanace Mangement</span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i>Paid Payment
</a>
</li>
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i>Failed Payment
</a>
</li>

</ul>
</li>
<li>
<a href="#">
<i class="material-icons text-success leftsize">schedule</i>
<span class="title">Booking & Reservation </span>
<span class="fa arrow"></span>
</a>
<ul class="sub-menu">
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i> New Booking
</a>
</li>
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i>Completed Booking
</a>
</li>
<li >
<a href="form_examples.html">
<i class="material-icons">keyboard_arrow_right</i>Expired Booking
</a>
</li>

</ul>
</li>
<li>
<a href="#">
<i class="material-icons text-success leftsize">flag</i>
<span class="title">Review Management</span>

</a>

</li>-->
<li>
<a href="{{url('staticpage')}}">
<i class="material-icons text-success leftsize">flag</i>
<span class="title">Manage Static Pages</span>

</a>

</li>
<li>
<a href="{{url('faq')}}">
<i class="material-icons text-success leftsize">flag</i>
<span class="title">Manage FAQ</span>

</a>

</li>
<!--<li>
<a href="#"> <i class="material-icons text-warning leftsize">chat_bubble</i> <span class="title">Blog</span> <span class="fa arrow"></span> </a>
<ul class="sub-menu">
<li>
<a href="blog_list.html"> <i class="material-icons">keyboard_arrow_right</i> Blog Category List </a>
</li>
<li>
<a href="blog_list2.html"> <i class="material-icons">keyboard_arrow_right</i> Blog List </a>
</li>
<li>
<a href="add_newblog.html"> <i class="material-icons">keyboard_arrow_right</i> Add New Blog </a>
</li>
</ul>
</li>-->
</ul>
<!-- END SIDEBAR MENU -->
</div>
</section>
<!-- /.sidebar -->
</aside>