<div class="col-xs-12 col-sm-3 margin_top30 margin_bottom30 sdecls"
     style="width: 18%;">
    <ul class="profile_left list-unstyled" id="listpropul">

        <li   {!! (Request::is('admin/property/basicPaid/*') ? 'style="background: #ddd"' : '') !!}  id="showBasi"><a href="{{URL::to('admin/property/basicPaid/'.$propertyObj->id)}}">Basics</a></li>
        <li  {!! (Request::is('admin/property/descriptionPaid/*') ? 'style="background: #ddd"' : '') !!} id="showDesc"><a href="{{URL::to('admin/property/descriptionPaid/'.$propertyObj->id)}}">Description</a></li>
        <!-- <li  {!! (Request::is('admin/property/amenities/*') ? 'style="background: #ddd"' : '') !!} id="showAmenities"><a href="{{URL::to('admin/property/amenities/'.$propertyObj->id)}}">Amenities</a></li> -->
        <li  {!! (Request::is('admin/property/photosPaid/*') ? 'style="background: #ddd"' : '') !!} id="showPhoto"><a href="{{URL::to('admin/property/photosPaid/'.$propertyObj->id)}}">Photos</a></li>
        <li  {!! (Request::is('admin/property/locationPaid/*') ? 'style="background: #ddd"' : '') !!} id="showLoc"><a href="{{URL::to('admin/property/locationPaid/'.$propertyObj->id)}}">Location</a></li>
        <li  {!! (Request::is('admin/property/pricing/*') ? 'style="background: #ddd"' : '') !!} id="showPricing"><a href="{{URL::to('admin/property/pricing/'.$propertyObj->id)}}">Pricing</a></li> 
        <li {!! (Request::is('admin/property/bookingsPaid/*') ? 'style="background: #ddd"' : '') !!}  id="showBooking"><a href="{{URL::to('admin/property/bookingPaid/'.$propertyObj->id)}}">Rules</a></li>
        <li  {!! (Request::is('admin/property/calendarPaid/*') ? 'style="background: #ddd"' : '') !!} id="showCalendar"><a href="{{URL::to('admin/property/calendarPaid/'.$propertyObj->id)}}">Calendar</a></li>
    </ul>

</div>