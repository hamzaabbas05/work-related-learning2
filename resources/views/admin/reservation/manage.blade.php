@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
<section class="content-header">
    <a target="_blank" href='{{ URL::to("admin/reservation/export/$status") }}' class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;float:right;"  >Export</a>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    {{$bredCrum}}
                </h4>
            </div>
            <br />
            <style type="text/css">
                tr.odd td, tr.even td{
                    max-width: 30%;
                    word-break: break-word;
                }
            </style>
            <div class="panel-body"  style="overflow: scroll;">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>ID</th>
                            <th>UserDetails</th>
                            <th>HostDetails</th>
                            <th>Booking Details</th>
                            <th>Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $counter = 0;?>
                        @foreach ($reserveObj as $obj)
                        <tr>
                            <td>{!! $obj->id !!}</td>

                            <td>
                                <?php 
                                /*if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                                // if($counter >= 35){
                                    echo "School Info :".$obj->user->school_info->name;
                                echo "<pre>";
                                print_r($obj->user->group_info->group_name);
                                // print_r($obj);
                                echo "</pre>";
                                // if($counter == 36){
                                    exit;
                                    
                                // }
                                // }
                                // $counter++;
                                }*/
                                ?>
                                {!! $obj->user->name !!}<br>
                                {!! $obj->user->lastname !!}<br>
                                {!! $obj->user->email !!}<br>
                                {!! $obj->user->phonenumber !!}<br />
                                {!! $obj->user->sname16 !!}<br />                                
                                {!! $obj->user->ssurname16 !!}<br />                                
                                {!! $obj->user->semail16 !!}<br />
{{isset($obj->user->group_info->group_name) ?$obj->user->group_info->group_name:'N/Agroup'}}
{{isset($obj->user->school_info->name)?$obj->user->school_info->name:'N/Aname'}}
                                <?php
                                if (!empty($obj->user->sphone16) && false === strpos($obj->user->sphone16, '0000')) {
                                    $obj->user->sphone16 = '0000' . $obj->user->sphone16;
                                }
                                ?>
                                {!! $obj->user->sphone16 !!}
                                <?php //echo $obj->school_group->name;?>
                            </td>


                            
                            <td>
                                @if($obj->host != null)
                                {!! $obj->host->name !!}<br>
                                {!! $obj->host->lastname !!}<br>
                                {!! $obj->host->email !!}<br>
                                {!! $obj->host->phonenumber !!}<br />
                                {!! $obj->host->orgname !!}<br />
                                {!! $obj->host->orgemail !!}<br />
                                {!! $obj->host->orgphone !!}<br />
                                @endif
                                {!! $obj->location_email !!}<br />
                                {!! $obj->location_telephone !!}
                            </td>


                            <td>{!! $obj->Bookingno !!}<br>
                                @if($obj->property != null)
                                {!! $obj->property->represent_email !!}<br>
                                {!! $obj->property->exptitle !!},<br>
                                @endif
                                @if($obj->user != null)
                                {!! $obj->user->school_tutor_email !!}<br>
                                @endif
                                
                                {!! $obj->checkin !!}<br>
                                {!! $obj->checkout !!}<br>
                            </td>

                            <td>{!! $obj->dateAdded !!}</td>

                            <td>{!! ($obj->cancelled == 'Yes' ? "Cancel" : $obj->approval) !!}</td>
                             
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
    @if($status == "Pending")
    $(document).ready(function() {
    $('#table').DataTable({
    "ordering": false
});
    });
    @endif

    @if($status != "Pending")
    $(document).ready(function() {
    $('#table').DataTable();
    });
    @endif
  

</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<script>
    $(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
    });
    });
</script>
@stop