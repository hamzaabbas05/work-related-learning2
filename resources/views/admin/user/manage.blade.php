@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
           
           
 <section class="content-header">
 <a target="_blank" href='{{ URL::to("admin/userlimitedinfo") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export-User-Limited-Info</a>
 <a target="_blank" href='{{ URL::to("admin/allstudentexport") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export-All-User</a>
 <a target="_blank" href='{{ URL::to("admin/userexport/4") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export-Organization</a>
 <a target="_blank" href='{{ URL::to("admin/userexport/5") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export Parent-Under18</a>
  <a target="_blank" href='{{ URL::to("admin/userexport/6") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export Student-Over18</a>
            </section>
<!-- Main content -->

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> 
                   {{$bredCrum}}
                </h4>
            </div>
            
            <br />
            <div class="panel-body" style="overflow: scroll;">

                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                          <!-- <th>SNO</th> -->
                            <!-- <th>ID</th> -->
                            <!-- <th>Name</th>
                            <th>Email</th> -->
                            <th width="10%">User Details</th>
                            <th>User Address <br> (RAddress18)</th>
                            <th>
                                Organisation (orgaddress)<br> 
                                School House Address (houseaddress)
                            </th> <!-- orgaddress -->
                            <th>
                                Listing (pro_lat / pro_lan)<br> 
                                Listing Other Address (workplace_address)<br>
                                Property Address (legal_address)<br>
                            </th>
                            <!-- <th>School House Address <br> (legal_address)</th> -->
                            <!-- <th>Student's Secondary School Address <br>(legal_address)</th> -->
                            <!-- <th>PhoneNumber</th> -->
                            <th>Email Confirmed<br>
                                Voucher Credits<br>
                                Status <br>
                                Created<br>
                            </th>
                            <!-- <th>Voucher Credits</th>
                            <th>Status</th>
                            <th>Created_at</th> -->
                            <th>Action</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                    <?php $url = "userstatus"; $i =1;?>
                    @foreach ($userObj as $obj)

                    <tr>
                            <?php /*<td>{!! $i !!}</td>
                            <td>{!! $obj->id !!}</td>
                    		<td>{!! $obj->name !!}</td>
                            <td>{!! $obj->email !!}</td>*/ ?>
                            <td>
                                ({{$i}})<br> 
                                ({{$obj->id}}){!! $obj->name !!}<br>
                                {!! $obj->lastname !!}<br>
                                {!! $obj->email !!}<br>
                                {!! $obj->phonenumber !!}<br />
                                {!! $obj->sname16 !!}<br />                                
                                {!! $obj->ssurname16 !!}<br />                                
                                {!! $obj->semail16 !!}<br />
                                <?php
                                if (!empty($obj->sphone16) && false === strpos($obj->sphone16, '0000')) {
                                    $obj->sphone16 = '0000' . $obj->sphone16;
                                }
                                ?>
                                {!! $obj->sphone16 !!}
                            </td>
                            <td>{!! $obj->raddress18 !!}</td>
                            <td>
                                {!! $obj->orgaddress !!}<br><hr><br>
                                {!! $obj['paid_service']['houseaddress']  !!}<br><hr><br>
                            </td>
                            <!-- <td> -->
                            <?php 
                            //if($obj->id == 37){
                                //$houseAddr = \App\UserPaidService::select('houseaddress')->where('user_id',$obj->id);
                               // echo "<pre>";print_r($obj->paid_service->houseaddress); echo "</pre>";
                                

                            //}
                             //echo "(".$obj->id.")";
                            // if($obj->paid_service->houseaddress){
                                // echo  $obj->paid_service->houseaddress;
                            // }
                            // echo $obj['paid_service']['houseaddress'];
                            /*
                            ?>
                             {!! $obj['paid_service']['houseaddress']  !!} </td>
                            <td><?php echo  ($obj->pro_lat != '' ? $obj->pro_lat." , ".$obj->pro_lan : '');  ?></td>
                            <?php  */
                            // if($_SERVER['REMOTE_ADDR'] == '93.33.44.160' &&  $obj->id == 605){
                            //     echo "<pre>";
                            //         print_r($obj);
                            //     echo "</pre>";
                            //     exit;
                            // }
                            ?>
                            <td> {{$obj->pro_lat != '' ? $obj->pro_lat." , ".$obj->pro_lan : ''}} <br><hr><br>
                                {!! $obj->workplace_address !!} <br><hr><br>
                                {!! $obj->legal_address !!}
                            </td>
                            <?php /*<td>{!! $obj['school']['legal_address'] !!}</td>
                            <td>{!! $obj->phonenumber !!}</td>*/?>
                            <td> 
                            @if($obj->email_confirmed == 1)
                                     <span class="statustextactive">yes</span>
                                @elseif($obj->email_confirmed == 0)
                                    <span class="statustextpending">No</span>
                               @endif
                               <br>
                            <!-- </td>
                            <td> -->
                            <?php //echo $obj->userWallet['amount']; 
                            // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                            //     echo "<pre>";
                            //     var_dump(isset($obj->userWallet[0]));
                            //     if(!isset($obj->userWallet[0])){
                            //         print_r($obj->userWallet);

                            //     }
                            //         // print_r($obj->userWallet[0]['vouchaercredits_id']);

                            //     echo "</pre>";
                            //     // exit;
                            // } 
                            //amount, send to unpaid ,boo_unpaid, sendto pai a,d bookpaid
                            // if(!isset($obj->userWallet[0]) && $obj->userWallet[0]->amount >= 0){
                                //calculate amount total
                                //$get_credits_amount = App\UserCredit::where("user_id", $obj->id)->where("credit_amount", ">", "0")->count();

                                $get_credits_amount_all = App\UserCredit::where("user_id", $obj->id)->where("credit_amount", ">", "0")->get();
                                //calculating nights herer
                                $total_credit_amount_sent = 0;
                                foreach ($get_credits_amount_all as $key => $credit_amount) {
                                    $total_credit_amount_sent += $credit_amount->credit_amount;
                                }
                               // echo "<pre>";

                                $wallet_amount = (isset($obj->userWallet[0])?$obj->userWallet[0]['amount']:0);
                                $credit_amount = $wallet_amount - $total_credit_amount_sent;
                                $credit_amount = ($credit_amount > 0 ? $credit_amount : 0);
                                // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                                // echo "Credit Amount ".$credit_amount."<br>";
                                // // echo "<pre>";
                                // //     print_r($obj->userWallet);
                                // //     print_r($obj->userWallet[0]['vouchaercredits_id']);

                                // // echo "</pre>";
                                // // exit;
                                // }

                            // }
                               
                                    //calculate send_to_unpaid count.
                               // $get_credits_sendtounpaid = App\UserCredit::where("user_id", $obj->id)->where("send_to_unpaid", ">", "0")->count();
                                
                                $get_credits_sendtounpaid_all = App\UserCredit::where("user_id", $obj->id)->where("send_to_unpaid", ">", "0")->get();
                                //calculating nights herer
                                $total_send_to_unpaid_sent = 0;
                                foreach ($get_credits_sendtounpaid_all as $key => $send_to_unpaid) {
                                    $total_send_to_unpaid_sent += $send_to_unpaid->send_to_unpaid;
                                }
                               // echo "<pre>";
                                $get_credits_sendtounpaid = $total_send_to_unpaid_sent;

                                $wallet_sendtounpaid = (isset($obj->userWallet[0])?$obj->userWallet[0]['send_to_unpaid']:0);
                                $credit_sendtounpaid = $wallet_sendtounpaid - $get_credits_sendtounpaid;
                                $credit_sendtounpaid = ($credit_sendtounpaid > 0 ? $credit_sendtounpaid : 0);
                                // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                                //     echo "User Id :".$obj->id."<pre>";
                                //         print_r($obj->userWallet);
                                //     echo "</pre>";
                                //     echo "<br> Wallet sendtounpaid ".$wallet_sendtounpaid."<br>";
                                //  echo "Send To Unpaid :".$credit_sendtounpaid."<br>";
                                //  // exit;
                                //  }

                                //calculate book_unpaid count.
                                //$get_credits_bookunpaid = App\UserCredit::where("user_id", $obj->id)->where("book_unpaid", ">", "0")->count();
                                $get_credits_bookunpaid_all = App\UserCredit::where("user_id", $obj->id)->where("book_unpaid", ">", "0")->get();
                                //calculating nights herer
                                $total_book_unpaid_sent = 0;
                                foreach ($get_credits_bookunpaid_all as $key => $book_unpaid) {
                                    $total_book_unpaid_sent += $book_unpaid->book_unpaid;
                                }
                               // echo "<pre>";
                                $get_credits_bookunpaid = $total_book_unpaid_sent;

                                $wallet_bookunpaid = (isset($obj->userWallet[0])?$obj->userWallet[0]['book_unpaid']:0);
                                $credit_bookunpaid = $wallet_bookunpaid - $get_credits_bookunpaid;
                                $credit_bookunpaid = ($credit_bookunpaid > 0 ? $credit_bookunpaid : 0);
                                // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                                // echo "<br> Credit Book Unpaid :".$credit_bookunpaid."<br>";
                                // // exit;
                                // }


                                //calculate send_to_schoolhouse count.

                                //$get_credits_sendschoolhouse = App\UserCredit::where("user_id", $obj->id)->where("send_to_schoolhouse", ">", "0")->count();
                                $get_credits_sendschoolhouse_all = App\UserCredit::where("user_id", $obj->id)->where("send_to_schoolhouse", ">", "0")->get();
                                //calculating nights herer
                                $total_schoolhouse_sent = 0;
                                foreach ($get_credits_sendschoolhouse_all as $key => $send_to_schoolhouse) {
                                    $total_schoolhouse_sent += $send_to_schoolhouse->send_to_schoolhouse;
                                }
                               // echo "<pre>";
                                $get_credits_sendschoolhouse = $total_schoolhouse_sent;
                                $wallet_sendschoolhouse = (isset($obj->userWallet[0])?$obj->userWallet[0]['send_to_schoolhouse']:0);
                                $credit_sendschoolhouse = $wallet_sendschoolhouse - $get_credits_sendschoolhouse;
                                $credit_sendschoolhouse = ($credit_sendschoolhouse > 0 ? $credit_sendschoolhouse : 0);
                                // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                                //     echo "<br> Credit Send School House :".$credit_sendschoolhouse."<br>";
                                //     // exit;
                                // }

                                //calculate bookpaid count.
                               // $get_credits_bookpaid = App\UserCredit::where("user_id", $obj->id)->where("book_paid", ">", "0")->count();
                                $get_credits_bookpaid_all = App\UserCredit::where("user_id", $obj->id)->where("book_paid", ">", "0")->get();
                                //calculating nights herer
                                $total_book_paid_sent = 0;
                                foreach ($get_credits_bookpaid_all as $key => $book_paid) {
                                    $total_book_paid_sent += $book_paid->book_paid;
                                } 
                               // echo "<pre>";
                                $get_credits_bookpaid = $total_book_paid_sent;

                                $wallet_bookpaid = (isset($obj->userWallet[0])?$obj->userWallet[0]['book_paid']:0);
                                $credit_bookpaid = $wallet_bookpaid - $get_credits_bookpaid;
                                $credit_bookpaid = ($credit_bookpaid > 0 ? $credit_bookpaid : 0);
                                // if($_SERVER['REMOTE_ADDR'] == '93.42.44.112'){
                                //     echo "<br> Credit Book Paid  :".$credit_bookpaid."<br>";
                                //     // exit;
                                // }

                                echo $credit_amount.",".$credit_sendtounpaid.",".$credit_bookunpaid.",".$credit_sendschoolhouse.",".$credit_bookpaid;
                               // if(!isset($obj->userWallet[0]) && $obj->userWallet[0]->send_to_unpaid >= 0){
                               //      $sendToUnpaid = $obj->userWallet[0]->send_to_unpaid;
                               //  }
                                // echo "<pre>";
                                //     print_r($obj->userWallet[0]);
                                // echo "</pre>";
                                // exit;
                                // echo $obj['userWallet']['amount'];
                                ?>
                           <!--  </td>
                            <td > -->
                                <br>

                                @if($obj->status == 1)
                                     <span class="statustextactive">Active</span>
                                @elseif($obj->status == 2)
                                    <span class="statustextpending">DeActive</span>
                               @endif
                            <!-- </td>
                            <td> -->
                                <br>
                                {!! $obj->created_at !!}
                            </td>
                            <td class="ui-group-buttons"> 
                               <a  onclick="changeStatus(1, {{$obj->id}}, '{{$url}}')" class="btn btn-success" role="button">
                                    <span class="glyphicon glyphicon-thumbs-up"></span>
                                </a>
                                
                                <a onclick="changeStatus(2, {{$obj->id}}, '{{$url}}')" class="btn btn-default" role="button">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a><br>
                                <a target="_blank" href="{{ URL::to('admin/view/user/'.$obj->id) }}">View User</a>
                            </td>
            			    <!-- <td> -->
                           <!--  <a href="{{ URL::to('admin/property/basic/' . $obj->id) }}"><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit PropertyType"></i></a> -->
                              <!-- </td> -->

            			</tr>
                       
                        <?php $i++;?>
                    @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
$(document).ready(function() {
	$('#table').DataTable();
});
</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop