@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
  <link href="{{ asset('assets/css/pages/form_layouts.css')}}" rel="stylesheet" type="text/css"/>
 
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    @include('admin.partials.bredcrum')
       <section class="content">
                <div class="row">
                    <div class="col-lg-12">
 <div  class="tab-content mar-top">
 <div id="tab1" class="tab-pane fade active in">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i>
                                                   Basic Information
                                                </h3>
                                                <span class="pull-right">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                                </span>
                                            </div>
                                            <div class="panel-body">
<form action="#" class="form-horizontal">

<div class="form-body">

<div class="form-group">
<label class="col-sm-2 control-label">Name</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->name}}" type="text" class="form-control" disabled="true">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Last Name</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->lastname}}" type="text" class="form-control" disabled="true">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Email</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->email}}" type="text" class="form-control" disabled="true">
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Phone Number</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->phonenumber}}" type="text" class="form-control" disabled="true">
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Date-Of-Birth</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->dob}}" type="text" class="form-control" disabled="true">
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Place-Of-Birth</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->basicpob}}" type="text" class="form-control" disabled="true">
</div>
</div>

 
 

<div class="form-group">
<label class="col-sm-2 control-label">National Insurance Number (SSN in the USA, Codice Fiscale in Italy)</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->NIN18}}" type="text" class="form-control" disabled="true">
</div>
</div>

 
 
 
 
</div>
</form>
</div>
</div>
</div>
<?php if ($userobject->hasRole('Organization')) { ?>  

<div class="col-lg-6">
<div class="panel panel-success">
<div class="panel-heading">
<h3 class="panel-title">
<i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i>
Organization Information
</h3>
<span class="pull-right">
<i class="fa fa-fw fa-chevron-up clickable"></i>
<i class="fa fa-fw fa-times removepanel clickable"></i>
</span>
</div>
<div class="panel-body">
<form action="#" class="form-horizontal">
<div class="form-body">
 
 <div class="form-group">
<label class="col-sm-2 control-label">Organization Name</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->orgname}}" type="text" class="form-control" disabled="true">
</div>
</div>
<div class="form-group">
<label for="inputContent" class="col-md-3 control-label">About Organization</label>
<div class="col-md-9">
<textarea id="inputContent" rows="3" class="form-control">{{$userobject->aboutorg}}</textarea>
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Legal Nature</label>


<?php $ln = "";
if($userobject->legal_nature == 0) { $ln = "private";} if($userobject->legal_nature == 1) { $ln = "public";} ?>
<div class="col-sm-10">
<input id="name" name="name" value="{{$ln}}" type="text" class="form-control" disabled="true">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Human Resources</label>
 <?php $hr = "";if(!is_null($userobject->roomtype)) { $hr = $userobject->roomtype->name;}?>

<div class="col-sm-10">
<input id="name" name="name" value="{{$hr}}" type="text" class="form-control" disabled="true">
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Organization Email</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->orgemail}}" type="text" class="form-control" disabled="true">
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Organization Phone</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->orgphone}}" type="text" class="form-control" disabled="true">
</div>
</div>

<div class="form-group">
<label for="inputContent" class="col-md-3 control-label">Legal Address</label>
<div class="col-md-9">
<textarea id="inputContent" rows="3" class="form-control">{{$userobject->raddress18}}</textarea>
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Work Tutor -Relation</label>
<div class="col-sm-10">
<?php //$tr = "";if(!is_null($userobject->tutorrelation)) { $tr = $userobject->tutorrelation->name;}?>
<?php $tr = "";if(!is_null($userobject->tutorrelation)) { $tutorFetchName = \App\Tutor::where('id',$userobject->tutorrelation)->get(); $tr = $tutorFetchName[0]->name;}?>

<input id="name" name="name" value="{{$tr}}" type="text" class="form-control" disabled="true">
</div>
</div>
<div class="form-group">
<label for="inputContent" class="col-md-3 control-label">Work-Tutor Fit</label>
<div class="col-md-9">
<textarea id="inputContent" rows="3" class="form-control">{{$userobject->why_fit}}</textarea>
</div>
</div>


 
  
</div>
 
</form>
</div>
</div>
</div> 

<!-- Col-lg-6  End -->
                                    
                                    <?php } ?>

<?php if ($userobject->hasRole('Parent')) { ?>  

<div class="col-lg-6">
<div class="panel panel-success">
<div class="panel-heading">
<h3 class="panel-title">
<i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i>
Parent Information
</h3>
<span class="pull-right">
<i class="fa fa-fw fa-chevron-up clickable"></i>
<i class="fa fa-fw fa-times removepanel clickable"></i>
</span>
</div>
<div class="panel-body">
<form action="#" class="form-horizontal">
<div class="form-body">
 <?php 
$rel = "";
 if($userobject->student_relationship == "1") { $rel = "Mother";} else if($userobject->student_relationship == "2") { $rel = "Father";} else if($userobject->student_relationship == "3") { $rel = "Legal Gaurdian";}?>
 <div class="form-group">
<label class="col-sm-2 control-label">Relationship with Student</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$rel}}" type="text" class="form-control" disabled="true">
</div>
</div>
<div class="form-group">
<label for="inputContent" class="col-md-3 control-label">Residency Address</label>
<div class="col-md-9">
<textarea id="inputContent" rows="3" class="form-control">{{$userobject->raddress18}}</textarea>
</div>
</div>

<div class="form-group">
<label for="inputContent" class="col-md-3 control-label">Domicile Address</label>
<div class="col-md-9">
<textarea id="inputContent" rows="3" class="form-control">{{$userobject->daddress18}}</textarea>
</div>
</div>

 
</div>
 
</form>
</div>
</div>
</div> 

<!-- Col-lg-6  End -->
                                    
                                    <?php } ?>





<?php if ($userobject->hasRole('Student') || $userobject->hasRole('Parent')) { ?> 
<div class="col-lg-6">
<div class="panel panel-success">
<div class="panel-heading">
<h3 class="panel-title">
<i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i>Student Information
</h3>
<span class="pull-right">
<i class="fa fa-fw fa-chevron-up clickable"></i>
<i class="fa fa-fw fa-times removepanel clickable"></i>
</span>
</div>
<div class="panel-body">
<form action="#" class="form-horizontal">
<div class="form-body">

<?php if ($userobject->hasRole('Parent')) { ?>
 <div class="form-group">
<label class="col-sm-2 control-label">Student Name</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->sname16}}" type="text" class="form-control" disabled="true">
</div>
</div>

 <div class="form-group">
<label class="col-sm-2 control-label">Student SurName</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->ssurname16}}" type="text" class="form-control" disabled="true">
</div>
</div>


 <div class="form-group">
<label class="col-sm-2 control-label">Date-Of-Birth</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->dob16}}" type="text" class="form-control" disabled="true">
</div>
</div>


 <div class="form-group">
<label class="col-sm-2 control-label">Place-Of-Birth</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->pob16}}" type="text" class="form-control" disabled="true">
</div>
</div>


 <div class="form-group">
<label class="col-sm-2 control-label">Student Email</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->semail16}}" type="text" class="form-control" disabled="true">
</div>
</div>

 <div class="form-group">
<label class="col-sm-2 control-label">Student Phone</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->sphone16}}" type="text" class="form-control" disabled="true">
</div>
</div>

 <div class="form-group">
<label class="col-sm-2 control-label">NIN</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->NIN16}}" type="text" class="form-control" disabled="true">
</div>
</div>

<div class="form-group">
<label for="inputContent" class="col-md-3 control-label">Residency Address</label>
<div class="col-md-9">
<textarea id="inputContent" rows="3" class="form-control">{{$userobject->raddress16}}</textarea>
</div>
</div>

<div class="form-group">
<label for="inputContent" class="col-md-3 control-label">Domicile Address</label>
<div class="col-md-9">
<textarea id="inputContent" rows="3" class="form-control">{{$userobject->daddress16}}</textarea>
</div>
</div>
 <?php } ?>

 <?php $schoolname = "";if(!is_null($userobject->school)) { $schoolname = $userobject->school->name;}?>
 <div class="form-group">
<label class="col-sm-2 control-label">School Name</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$schoolname}}" type="text" class="form-control" disabled="true">
</div>
</div>
 

<div class="form-group">
<label class="col-sm-2 control-label">Class Letter</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->class_letter}}" type="text" class="form-control" disabled="true">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Class Number</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->class_number}}" type="text" class="form-control" disabled="true">
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Matricule Student Number</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->student_number}}" type="text" class="form-control" disabled="true">
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Academic Qualifications eg. Middle school diploma/licenza media</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->academic_qualifications}}" type="text" class="form-control" disabled="true">
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Date of attainment</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->date_of_attainment}}" type="text" class="form-control" disabled="true">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Accompanying you - Name and Surname</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->school_tutor_name}}" type="text" class="form-control" disabled="true">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Accompanying you - Email</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->school_tutor_email}}" type="text" class="form-control" disabled="true">
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Accompanying you - Number</label>
<div class="col-sm-10">
<input id="name" name="name" value="{{$userobject->school_tutor_number}}" type="text" class="form-control" disabled="true">
</div>
</div>
<div class="form-group">
<label for="inputContent" class="col-md-3 control-label">Residency Address</label>
<div class="col-md-9">
<textarea id="inputContent" rows="3" class="form-control">{{$userobject->raddress18}}</textarea>
</div>
</div>

<div class="form-group">
<label for="inputContent" class="col-md-3 control-label">Domicile Address</label>
<div class="col-md-9">
<textarea id="inputContent" rows="3" class="form-control">{{$userobject->daddress18}}</textarea>
</div>
</div>
  
</div>
 
</form>
</div>
</div>
</div> 


<?php } ?>






















                                   
                                </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop