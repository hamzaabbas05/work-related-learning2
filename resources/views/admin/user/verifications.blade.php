@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{Asset('fancybox/jquery.fancybox.min.css')}}">
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')


<!-- Main content -->

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> 
                    {{$bredCrum}}
                </h4>
            </div>

            <br />
            <div class="panel-body" style="overflow-y:scroll;">
                @include('layouts.errorsuccess')
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>Action</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Personal ID</th>
                            <th>Expiry date</th>
                            <th>Image - Front</th>
                            <th>Image - Back</th>
                            <th>Criminal Record</th>
                            <th>Last Done (year)</th>
                            <th>Criminal Image</th>
                            <th>Id Verify</th>
                            <th>Criminal Verify</th>
                            <th>Verfify by Flash</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users_verifications as $obj)
                        <tr>
                            <td class="ui-group-buttons"> 
                                @if($obj->is_active == 1)
                                <a title="Personal Id Verification" href="{{URL::to('admin/user/verifiy_approval/byId/'.$obj->id)}}" class="btn btn-success" role="button" onclick="return confirm('Are you sure you don\'t want to approve Personal Id  Verification?')">
                                    <span class="glyphicon glyphicon-thumbs-down"></span>
                                </a>
                                @else
                                <a title="Personal Id Verification"  href="{{URL::to('admin/user/verifiy_approval/byId/'.$obj->id)}}" class="btn btn-success" role="button" onclick="return confirm('Are you sure you want to approve Personal Id Verification?')">
                                    <span class="glyphicon glyphicon-thumbs-up"></span>
                                </a>
                                @endif

                                @if($obj->is_criminal_verified == 'yes')
                                <a  title="Criminal Verification" href="{{URL::to('admin/user/verifiy_approval/bycriminal/'.$obj->id)}}" class="btn btn-success" role="button" onclick="return confirm('Are you sure you don\'t want to approve Criminal Verification?')">
                                    <span class="glyphicon glyphicon-thumbs-down"></span>
                                </a>
                                @else
                                <a  title="Criminal Verification"  href="{{URL::to('admin/user/verifiy_approval/bycriminal/'.$obj->id)}}" class="btn btn-success" role="button" onclick="return confirm('Are you sure you want to approve Criminal Verification?')">
                                    <span class="glyphicon glyphicon-thumbs-up"></span>
                                </a>
                                @endif

                                 @if($obj->is_verfied_by_flash == 'yes')
                                <a  title="Verified by Flash Verification"  href="{{URL::to('admin/user/verifiy_approval/byflash/'.$obj->id)}}" class="btn btn-success" role="button" onclick="return confirm('Are you sure you don\'t want to approve Verification by Flash Verification?')">
                                    <span class="glyphicon glyphicon-thumbs-down"></span>
                                </a>
                                @else
                                <a  title="Verified by Flash Verification"  href="{{URL::to('admin/user/verifiy_approval/byflash/'.$obj->id)}}" class="btn btn-success" role="button" onclick="return confirm('Are you sure you want to approve Verification by Flash Verification?')">
                                    <span class="glyphicon glyphicon-thumbs-up"></span>
                                </a>
                                @endif
                                <a target="_blank" href="{{ URL::to('admin/view/user/'.$obj->user->id) }}">View User</a>
                            </td>                            
                            <td>{!! $obj->id !!}</td>
                            <td>{!! isset($obj->user->name)?$obj->user->name:'' !!}</td>
                            <td>{!! isset($obj->user->email)?$obj->user->email:'' !!}</td>
                            <td>{!! $obj->personal_id_kind !!}</td>
                            <td>{!! $obj->personal_id_expiry_date !!}</td>
                            <td>
                                @if(isset($obj->personal_id_picture_front) && !empty($obj->personal_id_picture_front))
                                <a href="{{Asset('images/extra_info/'.$obj->personal_id_picture_front)}}" data-fancybox="images">
                                    <img src="{{Asset('images/extra_info/'.$obj->personal_id_picture_front)}}" alt="" style="width: 50%;"/>
                                </a>                                
                                @endif
                            </td>
                            <td>
                                @if(isset($obj->personal_id_picture_back) && !empty($obj->personal_id_picture_back))
                                <a href="{{Asset('images/extra_info/'.$obj->personal_id_picture_back)}}" data-fancybox="images">
                                    <img src="{{Asset('images/extra_info/'.$obj->personal_id_picture_back)}}" alt="" style="width: 50%;"/>
                                </a>
                                @endif
                            </td>
                            <td>
                                @if($obj->criminal_check_done == 1)
                                <span class="statustextactive">Done</span>
                                @elseif($obj->criminal_check_done == 2)
                                <span class="statustextpending">Not Done</span>
                                @endif
                            </td>
                            <td>{!! $obj->criminal_check_last_done_year !!}</td>
                            <td>
                                @if(isset($obj->criminal_check_image) && !empty($obj->criminal_check_image))
                                <a href="{{Asset('images/extra_info/'.$obj->criminal_check_image)}}" data-fancybox="images">
                                    <img src="{{Asset('images/extra_info/'.$obj->criminal_check_image)}}" alt="" style="width: 50%;"/>
                                </a>
                                @endif
                            </td>
                            <td >
                                @if($obj->is_active == 1)
                                <span class="statustextactive">Approved</span>
                                @elseif($obj->is_active == 0)
                                <span class="statustextpending">Not Approved</span>
                                @endif
                            </td>
                            <td >                               

                                @if($obj->is_criminal_verified == 'yes' && $obj->is_criminal_verified != '')
                                <span class="statustextactive">Approved</span>
                                @elseif($obj->is_criminal_verified == 'no')
                                <span class="statustextpending">Not Approved</span>
                                @endif
                            </td>
                            <td >
                                @if($obj->is_verfied_by_flash == 'yes' && $obj->is_verfied_by_flash != '')
                                <span class="statustextactive">Approved</span>
                                @elseif($obj->is_verfied_by_flash == 'no')
                                <span class="statustextpending">Not Approved</span>
                                @endif
                            </td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
    $(document).ready(function() {
    $('#table').DataTable();
    });</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<script>
    $(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
    });
    });</script>
<script src="{{Asset('fancybox/jquery.fancybox.min.js')}}"></script>
@stop