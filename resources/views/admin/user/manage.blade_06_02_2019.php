@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
           
           
 <section class="content-header">
    <a target="_blank" href='{{ URL::to("admin/userlimitedinfo") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export-User-Limited-Info</a>
 <a target="_blank" href='{{ URL::to("admin/allstudentexport") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export-All-User</a>
 <a target="_blank" href='{{ URL::to("admin/userexport/4") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export-Organization</a>
 <a target="_blank" href='{{ URL::to("admin/userexport/5") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export Parent-Under18</a>
  <a target="_blank" href='{{ URL::to("admin/userexport/6") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export Student-Over18</a>
            </section>
<!-- Main content -->

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> 
                   {{$bredCrum}}
                </h4>
            </div>
            
            <br />
            <div class="panel-body"  style="overflow: scroll;">

                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                          <th>SNO</th>

                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <!-- <th>Address</th>
                            <th>RAddress18</th>
                            <th>Street Address</th>
                            <th>Property Address</th> -->
                            <th>Organisation <br> (orgaddress)</th>
                            <th>User Address <br> (RAddress18)</th>
                            <th>School House Address <br> (houseaddress)</th> <!-- orgaddress -->
                            <th>Listing  <br> (pro_lat / pro_lan)</th>
                            <th>Listing Other Address <br> (workplace_address)</th>
                            <th>Property Address <br> (legal_address)</th>
                            <th>School House Address <br> (legal_address)</th>
                            <th>PhoneNumber</th>
                            <th>Email_confirmed</th>
                            <th>Status</th>
                            <th>Address</th>
                            <th>RAddress18</th>
                            <th>Created_at</th>
                            <th>Action</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $url = "userstatus"; $i =1;?>
                    @foreach ($userObj as $obj)
                    <tr>
                    <td>{!! $i !!}</td>
                            <td>{!! $obj->id !!}</td>
                    		<td>{!! $obj->name !!}</td>
                            <td>{!! $obj->email !!}</td>
                            <!-- <td>{!! $obj->address !!}</td>
                            <td>{!! $obj->houseaddress !!}</td>
                            <td>{!! $obj->streetAddress !!}</td>
                            <td>{!! $obj->legal_address !!}</td> -->
                            <td>{!! $obj->orgaddress !!}</td>
                            <td>{!! $obj->raddress18 !!}</td>
                            <td>
                            {!! $obj['paid_service']['houseaddress']  !!} </td>
                            <td><?php echo  ($obj->pro_lat != '' ? $obj->pro_lat." , ".$obj->pro_lan : '');  ?></td>
                            <td>{!! $obj->workplace_address !!}</td>
                            <td>{!! $obj->legal_address !!}</td>
                            <td>{!! $obj['school']['legal_address'] !!}</td>
                            <td>{!! $obj->phonenumber !!}</td>
                            <td> 
                            @if($obj->email_confirmed == 1)
                                     <span class="statustextactive">yes</span>
                                @elseif($obj->email_confirmed == 0)
                                    <span class="statustextpending">No</span>
                               @endif</td>
                            <td >
                                @if($obj->status == 1)
                                     <span class="statustextactive">Active</span>
                                @elseif($obj->status == 2)
                                    <span class="statustextpending">DeActive</span>
                               @endif
                                
                            </td>
                            <td>{!! $obj->address !!}</td>
                            <td>{!! $obj->raddress18 !!}</td>
                            <td>{!! $obj->created_at !!}</td>
                            <td class="ui-group-buttons"> 
                                           <a  onclick="changeStatus(1, {{$obj->id}}, '{{$url}}')" class="btn btn-success" role="button">
                                                <span class="glyphicon glyphicon-thumbs-up"></span>
                                            </a>
                                            
                                            <a onclick="changeStatus(2, {{$obj->id}}, '{{$url}}')" class="btn btn-default" role="button">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                        </td>
            			    <td>
                           <!--  <a href="{{ URL::to('admin/property/basic/' . $obj->id) }}"><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit PropertyType"></i></a> -->

                             <a target="_blank" href="{{ URL::to('admin/view/user/'.$obj->id) }}">View User</a>
                              </td>

            			</tr>
                        <?php $i++;?>
                    @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
$(document).ready(function() {
	$('#table').DataTable();
});
</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop