@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->

<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                        {{$bredCrum}}
                    </h3>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>
                <div class="panel-body">

                    <!-- errors -->
                    <div class="has-error">
                        @include('partials.errors')
                    </div>
                    @include('layouts.errorsuccess')

                    <!--main content-->
                    <div class="col-md-12">

                        <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                        {!! Form::open(array('url' => 'admin/booking/acceptcreate', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true,'id'=>'booking_form')) !!}
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <section>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">User *</label>
                                <div class="col-sm-10">
                                    <select name="user_id" id="user_id" class="form-control required" required="">
                                        <option value="">Select User</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->orgname}}({{$user->name}})({{$user->lastname}})({{$user->email}})({{$user->phonenumber}})
                                       ({{$user->raddress18}})({{$user->sphone16}})
                                    ({{$user->sname16}})({{$user->ssurname16}})({{$user->semail16}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Jobs / School Houses *</label>
                                <div class="col-sm-10">
                                    <select name="job_id" id="job_id" class="form-control required" required="">
                                        <option value="">Select Jobs / School Houses / Services</option>
                                        @foreach($jobs as $job)
                                        <option value="{{$job->id}}">{{$job->user->orgname}} ({{$job->user->name}} {{$job->user->lastname}})({{$job->user->email}}) ({{$job->user->phonenumber}})({{$job->exptitle}}) ({{$job->user->raddress18}})({{$job->user->sphone16}})
                                        ({{$job->user->sname16}})({{$job->user->ssurname16}})({{$job->user->semail16}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Start Date *</label>
                                <div class="col-sm-10">
                                    <input type="text" id="accept_startdate" name="startdate" readonly class="form-control" placeholder="DD-MM-YYYY">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">End Date *</label>
                                <div class="col-sm-10">
                                    <input type="text" id="accept_enddate" name="enddate" readonly class="form-control" placeholder="DD-MM-YYYY">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class=" text-right">
                                    <!-- <button type="submit" id ="btn_submit" class="btn btn-responsive btn-primary">Submit</button> -->
                                    <input id="btn_submit" type="submit" onclick="return checkforacc1(event)" class="btn btn-responsive btn-primary" value="Submit">
                                </div>
                            </div>

                        </section>


                        </form>
                        <!-- END FORM WIZARD WITH VALIDATION -->
                    </div>
                    <!--main content end-->
                </div>
            </div>
        </div>
    </div>
    <!--row end-->
</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function(){
        var dateFormat = "mm/dd/yy";
        $("#accept_startdate").datepicker().on( "change", function() {
          $("#accept_enddate").datepicker( "option", "minDate", getDate( this ) );
        });
        $("#accept_enddate").datepicker().on( "change", function() {
            $("#accept_startdate").datepicker( "option", "maxDate", getDate( this ) );
          });
         function getDate( element ) {
            var date;
            try {
              date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
              date = null;
            }

            return date;
          }

        // jQuery("#btn_submit").on("click",function(event){
    });

function checkforacc1(event)
    {
        event.preventDefault();
            var uid = jQuery("#user_id").val();
            var prpId = jQuery("#job_id").val();
            // if(confirm("Are you sure want to book?") == true){
                 
                // alert("Press Okay");

                // return false;
                
                // return false;
                // $("#statuschange").css('display', 'block');
                if (uid == "")
                {
                    alert("Choose Atleast any one");
                    window.location.reload();
                    return false;
                }

                $.ajax({
                type: 'POST',
                // url: baseurl + '/admin/booking/balancecheck',
                url: baseurl + '/admin/booking/checkforcredit',
                //async: false,
                data: {
                   // status: status,
                    uid: uid,
                    pid: prpId,
                    balance_check: 1
                },
                success: function (data) {

                // return false;
                    // alert(data);
                    // return false;

                    if (data == 'success') {
                        // $("#approveerr").css('display', 'none');
                         //$("#approveerr").html("");
                         jQuery("#booking_form").submit();
                        // alert("Successfully updated");
                        // return false;
                        // $("#statuschange").css('display', 'none');
                        // window.location.reload();
                    } else if(data == 'noCredits'){
                        if(confirm("This user has no credits to be accepted by a Work Tutor, continue anyway?")){
                        // if(confirm("The user does not have any credits for the request to be Approved, he or she has probably already been accepted elsewhere, that is found a place.") == true){
                            jQuery("#booking_form").submit();
                        }else{

                        }
                        // $("#statuschange").css('display', 'none');
                    } else {

                        // //  $("#approveerr").css('display', 'block');
                        //  $("#approveerr").html("Already you Approved one Request which is synced with same date");
                        //  //
                        // alert("Already you Approved one Request which is synced with same date");
                        return false;
                        // $("#statuschange").css('display', 'none');
                        //window.location.reload();
                    }

                }
            });
            // }else{
            //     alert("Press Cancel");
            //     return false;
            // }
        }
    function checkforacc1_no(event){
            event.preventDefault();
            // alert(jQuery("#user_id").val());
            var uid = jQuery("#user_id").val();
            var pid = jQuery("#job_id").val();
            $.ajax({
            url: baseurl + '/admin/booking/checkforcredit',
            type: "post",
            // dataType: "html",
            data: {
                user_id: uid,
                pr_id:pid,
            },
            beforeSend: function () {
                $("#paypalloadingimg").show();
            },
            success: function (responce) {
                // alert(responce);
                if ($.trim(responce) == 'disallow'){
                    if(confirm("This user has no credits to be accepted by a Work Tutor, continue anyway?")){
                        // alert("clicked Okay");
                        jQuery("#booking_form").submit();
                    }else{
                        // alert("clicked Cancel");
                    }
                    // alert("User doesn't have enough balance in his/her wallet");
                }else if($.trim(responce) == 'allow'){
                    // alert("Else Res :" + responce);
                    jQuery("#booking_form").submit();
                }else if($.trim(responce) == 'paidjob'){
                    jQuery("#booking_form").submit();
                }
                
                    return false;
                // if ($.trim(responce) == "error")
                // {}else {
                //     //alert(responce);
                //     // window.location = baseurl + '/request/booking/' + responce;
                //     /*$('.payment-form').html(responce);
                //      $('.payment-form').submit();*/ 
                // }
                // }
            },
    });
            return false;

          }

    // function checkforCredit(){

    // }
</script>
<script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>


@stop