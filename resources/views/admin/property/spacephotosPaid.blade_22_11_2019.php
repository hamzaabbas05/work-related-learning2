@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
Property
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<!-- <link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/> -->
<!--end of page level css-->

<link href="{{ asset('frontendassets/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/jquery-ui.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/pickmeup.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/style1.css')}}" rel="stylesheet">

<!-- common js -->
<script src="{{ asset('frontendassets/js/jquery.js')}}"></script>
<script src="{{ asset('frontendassets/js/bootstrap.js')}}"></script>
<script src="{{ asset('frontendassets/js/front.js')}}"></script></head>
@stop
{{-- content --}}
@section('content')
<div class="bg_gray1">
    <div class="container">
        <div class="text-center margin_top20">
            <h1>List Your Workexperience</h1>

        </div>
    </div> 
    <div class="container mrgnset">
        @include('admin.partials.spacesidebarPaid')
        <!--col-sm-3 end -->
        <div class="col-xs-12 col-sm-9 sdecls" style="width: 80%;">

            <div class="panel panel-default panelcls">
                <div class="panel-body">
                    <div class="row">

                        <div class="col-xs-12 margin_top10 margin_bottom20  commcls" id="photosdiv">
                            <h3>Photos can bring your space to life</h3>
                            Add at least 1 photo of areas guests have access to. You can always come back later and add more.							<hr />
                            <br />

                            <div class="col-sm-6 coldiv">
                                <div class="col-xs-12 col-sm-4 airfcfx-xs-center">
                                    <a href="javascript:void(0);" style="top: 10px;position: relative;color:#ff5a5f;text-decoration:none;cursor:pointer;"><i class="fa fa-camera"></i>Add Photo </a>
                                    <input type="file" name="XUploadForm[file][]" onchange="update_file_name();" multiple="true" id="uploadfile" accept=".png, .jpg, .jpeg" style="opacity:0;width:100%;height:35px;margin-top:-20px;cursor:pointer;">
                                </div>
                                <input type="hidden" value="{{$propertyObj->id}}" id="listingid" name="listingid">
                                <div class="col-xs-12 col-sm-4 airfcfx-xs-center">
                                    <input type="button" class="btn btn-success" value="Start Upload" onclick="start_file_upload()" id="startuploadbtn">
                                    <img id="loadingimg" src="{{asset('frontendassets/images/load.gif')}}" class="loading" style="margin-top:0px;">							</div>
                                <div id="imagenames" class="clear col-sm-12"></div>
                                <div id="imagepreview" class="clear col-sm-12">
                                    <?php
                                    $str = "[";
                                    foreach ($propertyObj->propertyimages as $img) {
                                        $str .= '"' . $img->img_name . '",';
                                        ?>
                                        <div class="listimgdiv"><img src="{{asset('images/rentals/'.$img->property_id.'/'.$img->img_name)}}" style="width:70px;height:70px;">
                                            <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$img->id3_get_frame_long_name(frameId)" onclick="remove_image(this, '{{$img->img_name}}')"></i>
                                        </div>


                                    <?php
                                    }

                                    $str1 = rtrim($str, ',');
                                    $str1 .= "]";
                                    ?>



                                </div>
                                <input type="hidden" value='{{$str1}}' name="uploadedfiles" id="uploadedfiles">

                                <div class="clear"></div>
                                <div class="photoerrcls" style="clear: both;"></div><br/>
                                <input type="button" value="Save" class="btn btn_email nextbtn" onclick="show_safety_paid();">

                            </div>
                        </div>



                    </div>
                </div>
                <!--row end -->
            </div>
        </div>
        <!--Panel end -->





    </div>
    <!--col-sm-9 end -->

</div>
<!-- container end -->



@include('partials.spacefooter')
@stop