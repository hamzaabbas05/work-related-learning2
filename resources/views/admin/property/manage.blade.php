@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Property
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')


<section class="content-header">
    <a href="{{ URL::to('admin/property/create') }}" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;float:right;"  >Add New</a>
    <a target="_blank" href='{{ URL::to("admin/propertyexport") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export</a>
</section>
<!-- Main content -->

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> 
                    {{$bredCrum}}
                </h4>
            </div>

            <br />
            <div class="panel-body" style="overflow: scroll;">

                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <!-- <th>ID</th> -->
                            <th>Organization</th>
                            <!-- <th>Work Experience Title</th> -->
                            <!-- <th></th> -->
                            <th>
                                Human Resource<br>
                                Work Category
                            </th>
                            <th>User Contact</th>
                            <th>
                                Listing (pro_lat/pro_lan)<br>
                                Property Address (legal_address)
                            </th>
                            <th>Status</th>
                            <th>Action</th>
                            <!-- <th>Edit</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php $url = "propertystatus"; ?>
                        @foreach ($currentObj as $obj)
                        <tr>
                            <!-- <td></td> -->
                            <td>
                                {{$obj->id}}<br>
                                <?php
                                $ptitle = "";
                                if (!empty($obj->title)) {
                                    $ptitle = $obj->title;
                                } elseif (isset($obj->user->orgname)) {
                                    $ptitle = $obj->user->orgname;
                                }
                                ?>
                                {!! $ptitle !!}<br> {{isset($obj->exptitle)?$obj->exptitle:''}}<br> 
                                {{isset($obj->user->aboutorg)?$obj->user->aboutorg:''}}<br>{{isset($obj->user->organisation_tax_registration_number)?$obj->user->organisation_tax_registration_number:''}}<br> {{isset($obj->user->orgemail)?$obj->user->orgemail:''}}<br> {{isset($obj->user->orgphone)?$obj->user->orgphone:''}}
                            </td> 
                            <?php /*<td>{!! $obj->exptitle !!}</td>*/ ?>
                            <td>
                                {!! isset($obj->propertytype->name)?$obj->propertytype->name:"" !!}
                            <!-- </td> -->
                            <?php if (!is_null($obj->roomtype)) { ?> 
                                <!-- <td> -->
                                    {!! $obj->roomtype->name !!}
                                <!-- </td> -->
                            <?php } else { ?>
                                <!-- <td></td> -->
                            <?php } ?>
                        </td>
                             <td>
                                {{isset($obj->user->id)?$obj->user->id:''}}<br>
                                {{isset($obj->user->name)?$obj->user->name:''}}<br>
                                {{isset($obj->user->lastname)?$obj->user->lastname:''}}<br>
                                {{isset($obj->user->phonenumber)?$obj->user->phonenumber:''}}<br />
                                {{isset($obj->user->email)?$obj->user->email:''}}
                            </td>
                            <td>
                                {{($obj->pro_lat != '' ? $obj->pro_lat." , ".$obj->pro_lon : '')}}<br>
                                {{ $obj->legal_address }}
                            </td>
                            <td >
                                @if($obj->status == 1)
                                <span class="statustextactive">Active</span>
                                @elseif($obj->status == 2)
                                <span class="statustextpending">DeActive</span>
                                @elseif($obj->status == 0)
                                <span class="statustextnoapproval">Approval Pending</span>
                                @endif

                            </td>
                            <td class="ui-group-buttons"> 
                                <a  onclick="changeStatus(1, {{$obj->id}}, '{{$url}}')" class="btn btn-success" role="button">
                                    <span class="glyphicon glyphicon-thumbs-up"></span>
                                </a>

                                <a  onclick="changeStatus(2, {{$obj->id}}, '{{$url}}')" class="btn btn-default" role="button">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>

                                <a onclick="changeStatus(3, {{$obj->id}}, '{{$url}}')" class="btn btn-info" role="button">
                                    <span class="fa fa-fw fa-star"></span>
                                </a>
                                <a onclick="changeStatus(4, {{$obj->id}}, '{{$url}}')" class="btn btn-info" role="button">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            <!-- </td>
                            <td> --><br>
                                <a href="{{ URL::to('admin/property/basic/' . $obj->id) }}"><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit PropertyType"></i></a><br>

                                <a target="_blank" href="{{ URL::to('admin/view/property/' . $obj->id) }}">View</a>
                            </td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
                                    $(document).ready(function() {
                                    // $('#table').DataTable({"order": [[ 5, "desc" ]]});
                                    $('#table').DataTable({"ordering": false});
                                    });</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<script>
    $(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
    });
    });
</script>
@stop