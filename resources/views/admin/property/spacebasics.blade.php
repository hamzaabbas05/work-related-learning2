@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
Property
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<!-- <link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/> -->
<!--end of page level css-->

<link href="{{ asset('frontendassets/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/jquery-ui.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/pickmeup.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/style1.css')}}" rel="stylesheet">

<!-- common js -->
<script src="{{ asset('frontendassets/js/jquery.js')}}"></script>
<script src="{{ asset('frontendassets/js/bootstrap.js')}}"></script>
<script src="{{ asset('frontendassets/js/front.js?v=1.45')}}"></script></head>
@stop
@section('content')
@inject('propertySettings', 'App\PropertySettings')
<?php 
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<div class="bg_gray1">
	<div class="container">
    <div class="text-center margin_top20">
        <h1>List Your Workexperience</h1>
         
</div>   <!-- container end -->
	<div class="container mrgnset">
		  
		 @include('admin.partials.spacesidebar')
		<!--col-sm-3 end -->
				<div class="col-xs-12 col-sm-12 sdecls" style="width: 70%;">
 				 @include('partials.rentals.basics')
			</div>
			<!--Panel end -->





		</div>
		<!--col-sm-9 end -->

	</div>
	<!-- container end -->

 
 
@include('partials.spacefooter')
@stop