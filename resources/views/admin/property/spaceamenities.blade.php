@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
Property
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<!-- <link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/> -->
<!--end of page level css-->

<link href="{{ asset('frontendassets/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/jquery-ui.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/pickmeup.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/style1.css')}}" rel="stylesheet">

<!-- common js -->
<script src="{{ asset('frontendassets/js/jquery.js')}}"></script>
<script src="{{ asset('frontendassets/js/bootstrap.js')}}"></script>
<script src="{{ asset('frontendassets/js/front.js')}}"></script></head>
@stop
{{-- content --}}
@section('content')
<div class="bg_gray1">
	<div class="container">
    <div class="text-center margin_top20">
        <h1>List Your Space</h1>
        <p>Holidaysiles lets you make money renting out your place.  </p>
    </div>
</div> 
	<div class="container mrgnset">
		@include('admin.partials.spacesidebar')
		<!--col-sm-3 end -->
				<div class="col-xs-12 col-sm-9 sdecls" style="width: 70%;">

			<div class="panel panel-default panelcls">
				<div class="panel-body">
					<div class="row">
					
					<div class="col-xs-12 margin_top10 margin_bottom20  commcls" id="amenitiesdiv">
							<h3>Tell travelers about your space</h3>
							Every space on Holidaysiles is unique. Highlight what makes your listing welcoming so that it stands out to
							guests who want to stay in your area.							<hr />
							<br />
							<input type="hidden" value="{{$propertyObj->id}}" id="listingid" name="listingid">
							<div class="col-sm-6 coldiv">
								<h3>Common Amenities</h3>
								<?php foreach($commonAmenities as $obj) { ?>
									<input type="checkbox" name="commonamenities[]" id="<?php echo "commonamenities".$obj->id?>" value="{{$obj->id}}"
									<?php if(in_array($obj->id, $property_amenity)) { ?> checked <?php } ?>>
 

 								<label style="display:block"  class="airfcfx-search-checkbox-text">{{$obj->name}}</label><br />
 								<?php } ?>
								
 								<hr />
								<h3>Additional Amenities</h3>
								<?php foreach($additionalAmenities as $obj) { ?>
									<input type="checkbox" name="additionalamenities[]" id="<?php echo "additionalamenities".$obj->id?>" value="{{$obj->id}}" <?php if(in_array($obj->id, $property_amenity)) { ?> checked <?php } ?>>
 								<label style="display:block"  class="airfcfx-search-checkbox-text">{{$obj->name}}</label><br />
 								<?php } ?>

 								<hr />
								<h3>Special Features</h3>
								<?php foreach($specialFeatures as $obj) { ?>
									<input type="checkbox" name="specialfeatures[]" id="<?php echo "specialfeatures".$obj->id?>" value="{{$obj->id}}" <?php if(in_array($obj->id, $property_amenity)) { ?> checked <?php } ?>>
 								<label style="display:block"  class="airfcfx-search-checkbox-text">{{$obj->name}}</label><br />
 								<?php } ?>
 								<hr />
								<h3>Safety Checklists</h3>
								<?php foreach($safetyCheck as $obj) { ?>
									<input type="checkbox" name="safetycheck[]" id="<?php echo "safetycheck".$obj->id?>" value="{{$obj->id}}" <?php if(in_array($obj->id, $property_amenity)) { ?> checked <?php } ?>>
 								<label style="display:block"  class="airfcfx-search-checkbox-text">{{$obj->name}}</label><br />
 								<?php } ?>
 								<div class="amentierrcls" style="clear: both;"></div><br/>
								<input type="button" value="Submit" class="btn btn_email nextbtn" onclick="show_photos();">
								 
							</div>
						</div>
					
					
					
					</div>
					</div>
					<!--row end -->
				</div>
			</div>
			<!--Panel end -->





		</div>
		<!--col-sm-9 end -->

	</div>
	<!-- container end -->


 @include('partials.spacefooter')
@stop