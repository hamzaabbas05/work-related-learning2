@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
Property
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<!-- <link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/> -->
<!--end of page level css-->

<link href="{{ asset('frontendassets/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/jquery-ui.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/pickmeup.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/style1.css')}}" rel="stylesheet">

<!-- common js -->
<script src="{{ asset('frontendassets/js/jquery.js')}}"></script>
<script src="{{ asset('frontendassets/js/bootstrap.js')}}"></script>
<script src="{{ asset('frontendassets/js/front.js')}}"></script></head>
@stop
{{-- content --}}
@section('content')
@inject('settings', 'App\Settings')
@inject('currecy', 'App\Currency')
<?php
$settingsObj = $settings->settingsInfo();
$currencies = $currecy->getCurrency(true);
?>
<div class="bg_gray1">
    <div class="container">
        <div class="text-center margin_top20">
            <h1>List Your Workexperience</h1>

        </div>
    </div> 
    <div class="container mrgnset">
        @include('admin.partials.spacesidebarPaid')
        <!--col-sm-3 end -->
        <div class="col-xs-12 col-sm-9 sdecls" style="width: 70%;">
            @include('partials.rentals.bookings_paid')
        </div>
        <!--Panel end -->





    </div>
    <!--col-sm-9 end -->

</div>
<!-- container end -->


@include('partials.spacefooter')
@stop

