@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
      <div class="container">    
        @include('partials.profile_sidebar')
        <div class="col-xs-12 col-sm-9 margin_top20">        
        
        <div class="airfcfx-panel panel panel-default">
          <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
            <h3 class="airfcfx-panel-title panel-title">Add More Verifications</h3>
          </div>
          
          <div class="airfcfx-panel-padding panel-body">
            <div class="row">        
            <?php 
                $verify = "Your Email is Not Verified";
                if (Auth::user()->email_confirmed == 1)  { 
                    $verify = "Your email address has been verified successfully.";
                } 
                $phverify = "Your Phone is Not Verified";
                if (Auth::user()->phone_confirmed == 1)  { 
                    $phverify = "Your Phone Number has been verified successfully";
                } 
                        ?>           
                    <div class="">
                    
                        <div class="col-xs-12 trust">
                        <h4>Email Address</h4>
                                {{$verify}}             
                        </div>  
                        <div id="succmsg"></div>

                        <div class="col-xs-12 trust margin_top20">
                        <h4>Phone Number</h4>                        
                        <p class="font_size13">Make it easier to communicate with a verified phone number. We’ll send you a code by SMS or read it to you over the phone. Enter the code below to confirm that you’re the person on the other end. </p> 
                        <br>
                        </div>
                        <div class="col-xs-12 trust"><p>{{$phverify}}</p></div>                        
                         
                    </div> <!--col-xs-12 end -->
                
                 </div> <!--row end -->

          </div>
          
        </div> <!--Panel end -->
        
         
        
        
        
    </div>
    </div> <!--container end -->
</div>

  
<script>
$(document).ready(function(){    
    $(".show_ph").click(function(){
        $(".add_phone").show();
        $(".show_ph").hide();
    });
    $(".add_cont").click(function(){
        $(".add_contact").toggle();     
    });
    $(".add_ship").click(function(){
        $(".add_shipping").toggle();        
    });
});
</script>  
@stop