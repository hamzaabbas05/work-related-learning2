@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Property
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')


<section class="content-header">
    <a href="{{ URL::to('admin/property/create') }}" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;float:right;"  >Add New</a>
    <a target="_blank" href='{{ URL::to("admin/propertyexport") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export</a>
</section>
<!-- Main content -->

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> 
                    {{$bredCrum}}
                </h4>
            </div>

            <br />
            <div class="panel-body">

                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>ID</th>
                            <th>Title</th>
                            <!--<th>Work Experience Title</th>-->
                            <th>Service Type</th>
                            <th>User Details</th>
                            <!-- <th>User Contact</th> -->
                            <th>status</th>
                            <th>Action</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $url = "propertystatus"; ?>
                        @foreach ($currentObj as $obj)
                        <tr>
                            <td>{!! $obj->id !!}</td>
                            <td>{!! $obj->exptitle !!}
                                {{isset($obj->user->country_id)?$obj->user->country_id:'country_id'}}
                                {{isset($obj->user->raddress18)?$obj->user->raddress18:'raddress18'}}
                                {{isset($obj->user->paid_service->house_name)?$obj->user->paid_service->house_name:'house_name'}}
                                {{isset($obj->user->paid_service->house_slogan)?$obj->user->paid_service->house_slogan:'house_slogan'}}
                                {{isset($obj->user->paid_service->house_desc)?$obj->user->paid_service->house_desc:'house_desc'}}
                                {{isset($obj->user->paid_service->houseaddress)?$obj->user->paid_service->houseaddress:'houseaddress'}}
                                {{isset($obj->user->paid_service->host_preference)?$obj->user->paid_service->host_preference:'host_preference'}}
                                {{isset($obj->user->paid_service->service_included)?$obj->user->paid_service->service_included:'service_included'}}
                                {{isset($obj->user->paid_service->house_rules)?$obj->user->paid_service->house_rules:'house_rules'}}
                                {{isset($obj->user->paid_service->class_desc)?$obj->user->paid_service->class_desc:'class_desc'}}
                                {{isset($obj->user->paid_service->activity_desc)?$obj->user->paid_service->activity_desc:'activity_desc'}}
                                {{isset($obj->user->paid_service->transfer_desc)?$obj->user->paid_service->transfer_desc:'transfer_desc'}}                                     
                                {{isset($obj->user->paid_service->tour_desc)?$obj->user->paid_service->tour_desc:'tour_desc'}}
                            </td> 
                            <td>
                                @if( (isset($obj->property_type) && empty($obj->property_type)) || (isset($obj->property_type) && $obj->property_type == "0" ))
                                First Listing
                                @else
                                {!! isset($obj->propertytype->name)?$obj->propertytype->name:"" !!}
                                @endif
                            </td>
                            <td>
                                
                                {{isset($obj->user->lastname)?$obj->user->lastname:''}}
                                {{isset($obj->user->name)?$obj->user->name." ":''}} ({{isset($obj->user->id)?$obj->user->id." ":'user_id'}})
                           <!--  </td>
                            <td> -->
                                <br />
                                {{isset($obj->user->phonenumber)?$obj->user->phonenumber:''}}
                                <br />
                                {{isset($obj->user->email)?$obj->user->email:''}}
                            </td>
                            <td >
                                @if($obj->status == 1)
                                <span class="statustextactive">Active</span>
                                @elseif($obj->status == 2)
                                <span class="statustextpending">DeActive</span>
                                @elseif($obj->status == 0)
                                <span class="statustextnoapproval">Approval Pending</span>
                                @endif

                            </td>
                            <td class="ui-group-buttons"> 
                                <a  onclick="changeStatus(1, {{$obj->id}}, '{{$url}}')" class="btn btn-success" role="button">
                                    <span class="glyphicon glyphicon-thumbs-up"></span>
                                </a>

                                <a  onclick="changeStatus(2, {{$obj->id}}, '{{$url}}')" class="btn btn-default" role="button">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>

                                <a onclick="changeStatus(3, {{$obj->id}}, '{{$url}}')" class="btn btn-info" role="button">
                                    <span class="fa fa-fw fa-star"></span>
                                </a>
                                <a onclick="changeStatus(4, {{$obj->id}}, '{{$url}}')" class="btn btn-info" role="button">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            </td>
                            <td>
                                <a href="{{ URL::to('admin/property/basicPaid/' . $obj->id) }}"><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit PropertyType"></i></a>

                                <a target="_blank" href="{{ URL::to('admin/view/property/' . $obj->id) }}">View</a>
                            </td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
                                    $(document).ready(function() {
                                    $('#table').DataTable({"ordering": false});
                                    // $('#table').DataTable({
                                    //                           "ordering": false,
                                    //                           "autoWidth": false,
                                    //                           "columnDefs": [
                                    //                             { "width": "60%", "targets": 1},
                                    //                             { "width": "5%", "targets": 3},
                                    //                           ]
                                    //                         } );
                                    });</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<script>
    $(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
    });
    });
</script>
@stop