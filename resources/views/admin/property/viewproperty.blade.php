@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link href="{{ asset('assets/css/pages/form_layouts.css')}}" rel="stylesheet" type="text/css"/>

<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div  class="tab-content mar-top">
                <div id="tab1" class="tab-pane fade active in">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i>
                                        Work-Experience Information
                                    </h3>
                                    <span class="pull-right">
                                        <i class="fa fa-fw fa-chevron-up clickable"></i>
                                        <i class="fa fa-fw fa-times removepanel clickable"></i>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <form action="#" class="form-horizontal">

                                        <div class="form-body">



                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Title</label>
                                                <div class="col-sm-10">
                                                    <input id="name" name="name" value="{{$propertyObj->title}}" type="text" class="form-control" disabled="true">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Work Experience Title</label>
                                                <div class="col-sm-10">
                                                    <input id="name" name="name" value="{{$propertyObj->exptitle}}" type="text" class="form-control" disabled="true">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Work Category</label>
                                                <div class="col-sm-10">
                                                    <input id="name" name="name" value="{{isset($propertyObj->propertytype->name)?$propertyObj->propertytype->name:''}}" type="text" class="form-control" disabled="true">
                                                </div>
                                            </div>

                                            <?php
// '{{$propertyObj->roomtype->name}}';
//userprint_r($propertyObj);die;
                                            ?>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Human Resources</label>
                                                <div class="col-sm-10">
                                                    <input id="name" name="name" value="{{isset($propertyObj->roomtype->name)?$propertyObj->roomtype->name:''}}" type="text" class="form-control" disabled="true">
                                                </div>
                                            </div>
                                            <?php
                                            $ln = "";
                                            if ($propertyObj->legal_nature == 0) {
                                                $ln = "private";
                                            } if ($propertyObj->legal_nature == 1) {
                                                $ln = "public";
                                            }
                                            ?>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Legal Nature</label>
                                                <div class="col-sm-10">
                                                    <input id="name" name="name" value="{{$ln}}" type="text" class="form-control" disabled="true">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputContent" class="col-md-3 control-label">Description</label>
                                                <div class="col-md-9">
                                                    <textarea id="inputContent" rows="3" class="form-control">{{$propertyObj->description}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputContent" class="col-md-3 control-label">Optional Description</label>
                                                <div class="col-md-9">
                                                    <textarea id="inputContent" rows="3" class="form-control">{{$propertyObj->optional_description}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputContent" class="col-md-3 control-label">work_schedule</label>
                                                <div class="col-md-9">
                                                    <textarea id="inputContent" rows="3" class="form-control">{{$propertyObj->work_schedule}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputContent" class="col-md-3 control-label">work_hours</label>
                                                <div class="col-md-9">
                                                    <textarea id="inputContent" rows="3" class="form-control">{{$propertyObj->work_hours}}</textarea>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="inputContent" class="col-md-3 control-label">Work Place Rules</label>
                                                <div class="col-md-9">
                                                    <textarea id="inputContent" rows="3" class="form-control">{{$propertyObj->houserules}}</textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i>
                                        Organization Information
                                    </h3>
                                    <span class="pull-right">
                                        <i class="fa fa-fw fa-chevron-up clickable"></i>
                                        <i class="fa fa-fw fa-times removepanel clickable"></i>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <form action="#" class="form-horizontal">
                                        <div class="form-body">

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Organization Name</label>
                                                <div class="col-sm-10">
                                                    <input id="name" name="name" value="{{$propertyObj->user->orgname}}" type="text" class="form-control" disabled="true">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputContent" class="col-md-3 control-label">About Organization</label>
                                                <div class="col-md-9">
                                                    <textarea id="inputContent" rows="3" class="form-control">{{$propertyObj->user->aboutorg}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Organization Email</label>
                                                <div class="col-sm-10">
                                                    <input id="name" name="name" value="{{$propertyObj->user->orgemail}}" type="text" class="form-control" disabled="true">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Organization Phone</label>
                                                <div class="col-sm-10">
                                                    <input id="name" name="name" value="{{$propertyObj->user->orgphone}}" type="text" class="form-control" disabled="true">
                                                </div>
                                            </div>


                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div> 
                        <?php if ($propertyObj->work_org_same == 1) { ?> 
                            <!-- Col-lg-6  End -->
                            <div class="col-lg-6">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i>
                                            Work-Tutor Information
                                        </h3>
                                        <span class="pull-right">
                                            <i class="fa fa-fw fa-chevron-up clickable"></i>
                                            <i class="fa fa-fw fa-times removepanel clickable"></i>
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <form action="#" class="form-horizontal">
                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Represent Name</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->user->name}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Represent SurName</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->user->lastname}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Represent Born</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->user->dob}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Represent Tax</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->user->NIN18}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Represent Email</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->user->email}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Tutor Relation</label>
                                                    <div class="col-sm-10">

                                                        <?php
                                                        $tnam = "";
                                                        if (!is_null($propertyObj->tutorrelation)) {
                                                            $tnam = $propertyObj->tutorrelation->name;
                                                        }
                                                        ?>


                                                        <input id="name" name="name" value="{{$tnam}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="inputContent" class="col-md-3 control-label">Why Fit?</label>
                                                    <div class="col-md-9">
                                                        <textarea id="inputContent" rows="3" class="form-control">{{$propertyObj->user->why_fit}}</textarea>
                                                    </div>
                                                </div>




                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>                                     
                        <?php } ?> 

                        <?php if ($propertyObj->work_org_same == 0) { ?> 
                            <!-- Col-lg-6  End -->
                            <div class="col-lg-6">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i>
                                            Work-Tutor Information
                                        </h3>
                                        <span class="pull-right">
                                            <i class="fa fa-fw fa-chevron-up clickable"></i>
                                            <i class="fa fa-fw fa-times removepanel clickable"></i>
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <form action="#" class="form-horizontal">
                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Represent Name</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->represent_name}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Represent SurName</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->represent_surname}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Represent Born</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->represent_born}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Represent Tax</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->represent_tax_no}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Represent Email</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->represent_email}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <?php
                                                    $tnam = "";
                                                    if (!is_null($propertyObj->tutorrelation)) {
                                                        $tnam = $propertyObj->tutorrelation->name;
                                                    }
                                                    ?>
                                                    <label class="col-sm-2 control-label">Tutor Relation</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$tnam}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="inputContent" class="col-md-3 control-label">Work Tutor Fit</label>
                                                    <div class="col-md-9">
                                                        <textarea id="inputContent" rows="3" class="form-control">{{$propertyObj->work_tutor_fit}}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>                                     
                        <?php } ?>    




                        <?php if ($propertyObj->addr_work_org_same == 0) { ?>  

                            <div class="col-lg-6">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i>
                                            WorkPlace Information
                                        </h3>
                                        <span class="pull-right">
                                            <i class="fa fa-fw fa-chevron-up clickable"></i>
                                            <i class="fa fa-fw fa-times removepanel clickable"></i>
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <form action="#" class="form-horizontal">
                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label for="inputContent" class="col-md-3 control-label">workplace Address</label>
                                                    <div class="col-md-9">
                                                        <textarea id="inputContent" rows="3" class="form-control">{{$propertyObj->workplace_address}}</textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Location Email</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->location_email}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Location Telephone</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->location_telephone}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Other Work-Address</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->location_other_address}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div> 

                            <!-- Col-lg-6  End -->

                        <?php } ?>


                        <?php if ($propertyObj->addr_work_org_same == 1) { ?>  

                            <div class="col-lg-6">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i>
                                            WorkPlace Information
                                        </h3>
                                        <span class="pull-right">
                                            <i class="fa fa-fw fa-chevron-up clickable"></i>
                                            <i class="fa fa-fw fa-times removepanel clickable"></i>
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <form action="#" class="form-horizontal">
                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label for="inputContent" class="col-md-3 control-label">workplace Address</label>
                                                    <div class="col-md-9">
                                                        <textarea id="inputContent" rows="3" class="form-control">{{$propertyObj->user->raddress18}}</textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Location Email</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->user->orgemail}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Location Telephone</label>
                                                    <div class="col-sm-10">
                                                        <input id="name" name="name" value="{{$propertyObj->user->orgphone}}" type="text" class="form-control" disabled="true">
                                                    </div>
                                                </div>




                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div> 

                            <!-- Col-lg-6  End -->

                        <?php } ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop