@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
Property
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<!-- <link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/> -->
<!--end of page level css-->

<link href="{{ asset('frontendassets/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/jquery-ui.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/pickmeup.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/style1.css')}}" rel="stylesheet">

<!-- common js -->
<script src="{{ asset('frontendassets/js/jquery.js')}}"></script>
<script src="{{ asset('frontendassets/js/bootstrap.js')}}"></script>
<script src="{{ asset('frontendassets/js/front.js')}}"></script></head>
@stop
{{-- content --}}
@section('content')
@inject('settings', 'App\Settings')
@inject('currecy', 'App\Currency')
<?php
$settingsObj = $settings->settingsInfo();
$currencies = $currecy->getCurrency(true);
?>
<div class="bg_gray1">
    <div class="container">
        <div class="text-center margin_top20">
            <h1>List Your Space</h1>
            <p>Holidaysiles lets you make money renting out your place.  </p>
        </div>
    </div> 
    <div class="container mrgnset">
        @include('admin.partials.spacesidebarPaid')
        <!--col-sm-3 end -->
        <div class="col-xs-12 col-sm-9 sdecls" style="width: 70%;">
            <div class="panel panel-default panelcls">
                <div class="panel-body">
                    <div class="row">

                        <div class="col-xs-12 margin_top10 margin_bottom20 commcls" id="pricediv">
                            <h3>Set a nightly price for your space</h3>
                            You can set a price to reflect the space, amenities, and hospitality you’ll be providing.							<hr />
                            <br />
                            {!! Form::open(array('url' => 'rentals/savePricing', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                            <div class="col-sm-6 coldiv">
                                <h3>Base Price</h3><br />
                                <input type="hidden" value="{{$propertyObj->id}}" id="listingid" name="listingid" />
                                <input type="hidden" value="{{isset($pricing->id)?$pricing->id:''}}" id="pricing_id" name="pricing_id" />
                                <label>Currency</label>
                                <div class="form-group field-listing-currency">

                                    {!! Form::select('currency_select', $currencies, $settingsObj->currency_id, ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'currency', 'disabled' => 'true']) !!}

                                    <p class="help-block help-block-error"></p>
                                </div>
                                <label>Select Payment Type</label>
                                <div class="form-group field-listing-currency">
                                    <label>
                                        <input type="radio" name="payment_type" id="weekly" value="weekly" class="form-control" style="width: 3%; float: left;" @if(isset($pricing->payment_type) && $pricing->payment_type == "weekly") checked="" @endif/>
                                        <span style="float: left; margin-left: 24px; margin-top: 12px;">Weekly</span>
                                    </label>
                                    <label style="margin-left: 20px;">
                                        <input type="radio" name="payment_type" id="fixed" value="fixed" class="form-control" style="width: 3%; float: left;" @if(isset($pricing->payment_type) && $pricing->payment_type == "fixed") checked="" @endif/>
                                        <span style="float: left; margin-left: 24px; margin-top: 12px;">Fixed</span>
                                    </label>
                                    <p class="help-block help-block-error"></p>
                                </div>
                                <div id="show_weekly_payment" style="@if(isset($pricing->payment_type) && $pricing->payment_type == 'weekly') display: block; @else display: none; @endif">
                                    <label>Weekly Payment Amount</label>
                                    <div class="form-group field-listing-currency">
                                        <input type="number" maxlength="10" name="weekly_amount" value="{{isset($pricing->amount)?$pricing->amount:''}}" class="form-control margin_bottom10 smalltext" id="weekly_amount" />
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                </div>
                                <div id="show_fixed_payment" style="@if(isset($pricing->payment_type) && $pricing->payment_type == 'fixed') display: block; @else display: none; @endif">
                                    <label>Fixed Payment Amount</label>
                                    <div class="form-group field-listing-currency">
                                        <input type="number" maxlength="10" name="fix_amount" value="{{isset($pricing->amount)?$pricing->amount:''}}" class="form-control margin_bottom10 smalltext" id="fix_amount" />
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                </div>
                                <input type="submit" value="Save Pricing" class="btn btn_email nextbtn" />
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!--row end -->
            </div>
        </div>
        <!--Panel end -->





    </div>
    <!--col-sm-9 end -->

</div>
<!-- container end -->




@include('partials.spacefooter')
@stop