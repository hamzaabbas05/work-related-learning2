@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Property
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')


<section class="content-header">
    <!---<a href="{{ URL::to('admin/property/create') }}" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;float:right;"  >Add New</a>-->
    <a target="_blank" href='{{ URL::to("admin/exportTransferwiseSheet") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Export</a>
</section>
<!-- Main content -->

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> 
                    {{$bredCrum}}
                </h4>
            </div>

            <br />
            <div class="panel-body">

                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>Name</th>
                            <th>Recipient Email</th>
                            <th>Payment Reference</th>
                            <th>Receiver Type</th>
                            <th>Amount Currency</th>
                            <th>Amount</th>
                            <th>Source Currency</th>
                            <th>Target Currency</th>
                            <th>Sort Code</th>
                            <th>Account Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(sizeof($rentalPaidJobs) > 0)
                        @foreach ($rentalPaidJobs as $job)
                        <tr>
                            <td>{!! isset($job->host->name)?$job->host->name:"" !!}</td>
                            <td>{!! isset($job->host->email)?$job->host->email:"" !!}</td>
                            <td>{!! isset($job->Bookingno)?$job->Bookingno:"" !!}</td>
                            <td>{!! isset($job->host->bank_detail->receiverType)?$job->host->bank_detail->receiverType:"" !!}</td>
                            <td>{!! isset($job->host->bank_detail->amountCurrency)?$job->host->bank_detail->amountCurrency:"" !!}</td>
                            <td>200</td>
                            <td>{!! isset($job->host->bank_detail->sourceCurrency)?$job->host->bank_detail->sourceCurrency:"" !!}</td>
                            <td>{!! isset($job->host->bank_detail->targetCurrency)?$job->host->bank_detail->targetCurrency:"" !!}</td>
                            <td>{!! isset($job->host->bank_detail->sortCode)?$job->host->bank_detail->sortCode:"" !!}</td>
                            <td>{!! isset($job->host->bank_detail->accountNumber)?$job->host->bank_detail->accountNumber:"" !!}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
    $(document).ready(function() {
    $('#table').DataTable();
    });</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<script>
    $(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
    });
    });
</script>
@stop