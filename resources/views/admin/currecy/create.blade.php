@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    @include('admin.partials.bredcrum')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                            {{$bredCrum}}
                        </h3>
                                <span class="pull-right clickable">
                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                </span>
                    </div>
                    <div class="panel-body">

                        <!-- errors -->
                        <div class="has-error">
                            @include('partials.errors')
                        </div>

                        <!--main content-->
                        <div class="col-md-12">

                            <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                          {!! Form::open(array('url' => 'admin/currency', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                                 <!-- CSRF Token -->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <section>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Currency Name *</label>
                                        <div class="col-sm-10">
                                        <input id="currency_name" name="currency_name" type="text" class="form-control">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Currency Code *</label>
                                        <div class="col-sm-10">
                                       <input id="currency_code" name="currency_code" type="text" class="form-control">

                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-sm-2 control-label">Currency Symbol *</label>
                                        <div class="col-sm-10">
                                      <input id="currency_symbol" name="currency_symbol" type="text" class="form-control">


                                        </div>
                                    </div>
<div class="form-group">
                                        <label class="col-sm-2 control-label">Currency Rate *</label>
                                        <div class="col-sm-10">
                                      <input id="currency_rate" name="currency_rate" type="text" class="form-control">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Status</label>
                                        <label class="radio-inline">
                                            <input name="status" id="optionsRadiosInline1" value="1" checked="checked" type="radio">Active</label>
                                        <label class="radio-inline">
                                            <input name="status" id="optionsRadiosInline2" value="2" type="radio">DeActive</label>
                                       
                                    </div>
                                <div class="form-group">
                                    <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                    </div>
                                    </div>

                                 </section>
                                

                            </form>
                            <!-- END FORM WIZARD WITH VALIDATION -->
                        </div>
                        <!--main content end-->
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop