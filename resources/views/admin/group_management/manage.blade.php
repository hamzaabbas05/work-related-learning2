@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Property
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{Asset('fancybox/jquery.fancybox.min.css')}}">
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')


<section class="content-header">
    <!---<a href="{{ URL::to('admin/property/create') }}" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;float:right;"  >Add New</a>-->
    <a target="_blank" href='{{ URL::to("admin/userlimitedinfo") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;">Export-User-Limited-Info</a>
    <a target="_blank" href='{{ URL::to("admin/allstudentexport") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;">Export-All-User</a>
    <a target="_blank" href='{{ URL::to("admin/workPlaceSchoolHouseExport") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >Group Workplaces and School Houses</a>

    <!--    <div class="col-md-12">
            <div class="col-md-4">
                <select name="group_id" class="form-control">
                    <option value="">Select Group</option>
                </select>
            </div>
            <div class="col-md-4">
                <select name="order_by" class="form-control">
                    <option value="">Order By</option>
                    <option value="email_asc">Email ASC</option>
                                    <option value="email_desc">Email DESC</option>
                </select>
            </div>
        </div>-->

    <a data-fancybox data-src="#export-form" href="javascript:;" class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >
        is Confirmed School House / Work
    </a>
    {!! Form::open(array('url' => 'admin/is_confirm_group', 'id' => "export-form", 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true, 'style' => 'display: none;width:100%;max-width:660px;')) !!}
    <h2 class="mb-3">
        Select Group or, 
        <br />
        for individual departures, select "No Group specified"
    </h2>
    <p>
        <select name="group_id" class="form-control">
            <option value="">Select Group</option>
            @if(sizeof($groups) > 0)
            @foreach($groups as $group)
            <option value="{{$group->id}}">{{$group->group_name}}</option>
            @endforeach
            @endif
            <option value="no_group">No Group Specified</option>
        </select>
    </p>
    <p class="mb-0 text-right">
        <input type="submit" class="btn btn-primary" value="Submit" />
    </p>
    {!! Form::close() !!}
    <a target="_blank" href='{{ URL::to("admin/groupexport") }}' class="btn btn-responsive button-alignment btn-success" style="margin-right:20px;margin-bottom:7px;float:right;"  >1st 2nd and 3rd Choice / Work</a>

</section>
<!-- Main content -->

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> 
                    {{$bredCrum}}
                </h4>
            </div>

            <br />
            <div class="panel-body">

                <table class="table table-bordered " id="table" style="overflow-x: auto !important;">
                    <thead>
                        <tr class="filters">
                            <th>Rental ID</th>
                            <th>B No</th>
                            <th style="width: 10% !important;">Email</th>
                            <th>Name</th>
                            <th>List Type</th>
                            <th>Is Sent (to user listing / work tutor)</th>
                            <th>Is Confirmed (by user listing / work tutor)</th>
                            <th>P_Is Sent (to user booking (type of booking is defined in column "List Type") / Parent and Students)</th>
                            <th>P_Is Confirmed (by user booking / Parent and Students)</th>
                            <th>S_Is Sent (user listing / School House)</th>
                            <th>S_Is Confirmed (user listing / School House)</th>
                            <th>Student_Is Sent</th>
                            <th>Student_Is Confirmed</th>
                            <th>First Choice</th>
                        </tr>
<!--                        <tr class="filters">
                            <th>Date In</th>
                            <th>Date Out</th>
                            <th>School Name</th>
                            <th>List Type</th>
                            <th>Status</th>
                            <th>First Choice</th>
                            <th>2nd Choice</th>
                            <th>3rd Choice</th>
                            <th>Admin 4th Choice</th>
                            <th>DOB</th>
                            <th>DOB16</th>
                            <th>Gender</th>
                            <th>Class</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Student Desp.</th>
                        </tr>-->
                    </thead>
                    <tbody>
                        @foreach ($users_info as $booking)
                        <tr>
                            <td>{!! $booking->id !!}</td>
                            <td>{!! $booking->Bookingno !!}</td>
                            <td style="width: 10% !important;">{!! isset($booking->user->email)?$booking->user->email:"" !!}</td>
                            <td>{!! isset($booking->user->name)?$booking->user->name:"" !!} {{$booking->user_id}}</td>
                            <td>
                                <?php $property_type = ""; ?>
                                @if(isset($booking->property->property_status) && $booking->property->property_status == "unpaid")
                                <?php $property_type = "Work Placement"; ?>
                                @elseif(isset($booking->property->property_status) && $booking->property->property_status == "unpaidflash")
                                <?php $property_type = "Work Placement"; ?>
                                @elseif(isset($booking->property->property_status) && $booking->property->property_status == "paid")
                                <?php $property_type = "School House"; ?>
                                @endif
                                {{$property_type}}
                            </td>
                            <?php
                            $is_sent = "not found";
                            $is_confirm = "not found";
                            if (isset($booking->property->id)) {
                                $email_detail = \App\EmailSent::where(["user_id" => $booking->user_id, "property_id" => $booking->prd_id, "sent_to" => "Work Tutor Email"])->first();
                                if (isset($email_detail->id)) {
                                    $is_sent = $email_detail->is_sent;
                                    if ($email_detail->is_confirm == "1") {
                                        $is_confirm = "Confirmed";
                                    }
                                }
                            }
                            $parent_is_sent = "not found";
                            $parent_is_confirm = "not found";
                            if (isset($booking->property->id)) {
                                $email_detail = \App\EmailSent::where(["user_id" => $booking->user->id, "property_id" => $booking->property->id, "sent_to" => "Parent Email"])->first();
                                if (isset($email_detail->id)) {
                                    $parent_is_sent = $email_detail->is_sent;
                                    if ($email_detail->is_confirm == "1") {
                                        $parent_is_confirm = "Confirmed";
                                    }
                                }
                            }
                            $paid_is_sent = "not found";
                            $paid_is_confirm = "not found";
                            if (isset($booking->property->id)) {
                                $email_detail = \App\EmailSent::where(["user_id" => $booking->user->id, "property_id" => $booking->property->id, "sent_to" => "Paid Service school houses Email"])->first();
                                if (isset($email_detail->id)) {
                                    $paid_is_sent = $email_detail->is_sent;
                                    if ($email_detail->is_confirm == "1") {
                                        $paid_is_confirm = "Confirmed";
                                    }
                                }
                            }
                            $student_is_sent = "not found";
                            $student_is_confirm = "not found";
                            if (isset($booking->property->id)) {
                                $email_detail = \App\EmailSent::where(["user_id" => $booking->user->id, "property_id" => $booking->property->id, "sent_to" => "Student Email"])->first();
                                if (isset($email_detail->id)) {
                                    $student_is_sent = $email_detail->is_sent;
                                    if ($email_detail->is_confirm == "1") {
                                        $student_is_confirm = "Confirmed";
                                    }
                                }
                            }
                            ?>
                            <td>{{$is_sent}}</td>
                            <td>{{$is_confirm}}</td>
                            <td>{{$parent_is_sent}}</td>
                            <td>{{$parent_is_confirm}}</td>
                            <td>{{$paid_is_sent}}</td>
                            <td>{{$paid_is_confirm}}</td>
                            <td>{{$student_is_sent}}</td>
                            <td>{{$student_is_confirm}}</td>

                            <td>
                                <a href='{{url("view/rentals/".$booking->property->id)}}' target="_blank">
                                    {!! isset($booking->property->exptitle)?$booking->property->exptitle:"" !!}
                                </a>
                            </td>
                        </tr>
                        <?php /*
                        <tr style="display: none;">
                            <td>{!! isset($booking->checkin)?date("F j, Y", strtotime($booking->checkin)):"" !!}</td>
                            <td>{!! isset($booking->checkout)?date("F j, Y", strtotime($booking->checkout)):"" !!}</td>
                            <td>{!! isset($booking->user_detail->school_info->name)?$booking->user_detail->school_info->name:"" !!}</td>
                            <td>
                                <?php $property_type = ""; ?>
                                @if(isset($booking->property->property_status) && $booking->property->property_status == "unpaid")
                                <?php $property_type = "Work Placement"; ?>
                                @elseif(isset($booking->property->property_status) && $booking->property->property_status == "unpaidflash")
                                <?php $property_type = "Work Placement"; ?>
                                @elseif(isset($booking->property->property_status) && $booking->property->property_status == "paid")
                                <?php $property_type = "School House"; ?>
                                @endif
                                {{$property_type}}
                            </td>
                            <td>{!! isset($booking->approval)?$booking->approval:"" !!}</td>

                            <!--user 1st choice starts-->
                            <td>
                                <a href='{{url("view/rentals/".$booking->property->id)}}' target="_blank">
                                    {!! isset($booking->property->exptitle)?$booking->property->exptitle:"" !!}
                                </a>
                            </td>
                            <!--user 1st choice ends-->
                            <!--user 2nd choice starts-->
                            <?php
                            $second_choice_checkin = '';
                            $second_choice = App\RentalsEnquiry::where('dateAdded', '>', $booking->dateAdded)->where('user_id', $booking->user_id)->orderBy('dateAdded', 'ASC')->first();
                            if (isset($second_choice->id)) {
                                $second_choice_checkin = isset($second_choice->dateAdded) ? $second_choice->dateAdded : "";
                                $rental_array[] = $second_choice->id;
                                ?>
                                <td>
                                    <a href='{{url("view/rentals/".$second_choice->property->id)}}' target="_blank">
                                        {!! isset($second_choice->property->exptitle)?$second_choice->property->exptitle:"" !!}
                                    </a>
                                </td>
                            <?php } else { ?>
                                <td>&nbsp;</td>
                            <?php } ?>
                            <!--user 2nd choice ends-->
                            <!--user 3rd choice starts-->
                            <?php
                            if (!empty($second_choice_checkin)) {
                                $third_choice = App\RentalsEnquiry::where('dateAdded', '>', $second_choice_checkin)->where('user_id', $booking->user_id)->orderBy('dateAdded', 'ASC')->first();
                                if (isset($third_choice->id)) {
                                    $rental_array[] = $third_choice->id;
                                    ?>
                                    <td>
                                        <a href='{{url("view/rentals/".$third_choice->property->id)}}' target="_blank">
                                            {!! isset($third_choice->property->exptitle)?$third_choice->property->exptitle:"" !!}
                                        </a>
                                    </td>
                                <?php } else { ?>
                                    <td>&nbsp;</td>
                                    <?php
                                }
                            } else {
                                ?>
                                <td>&nbsp;</td>
                            <?php } ?>
                            <!--user 3rd choice ends-->
                            <!--user Admin choice starts-->
                            <?php
                            $admin_choice = App\RentalsEnquiry::select("fc_rentalsenquiry.*")
                                            ->join("property", "fc_rentalsenquiry.prd_id", "=", "property.id")
                                            ->where('fc_rentalsenquiry.user_id', $booking->user_id)
                                            ->where('fc_rentalsenquiry.is_admin_booked', '1')
                                            ->where('property.property_status', 'paid')
                                            ->orderBy('fc_rentalsenquiry.id', 'DESC')->first();
                            if (isset($admin_choice->id)) {
                                ?>
                                <td>
                                    <a href='{{url("view/rentals/".$admin_choice->property->id)}}' target="_blank">
                                        {!! isset($admin_choice->property->exptitle)?$admin_choice->property->exptitle:"" !!}
                                    </a>
                                </td>
                            <?php } else { ?>
                                <td>&nbsp; no admin choice</td>
                            <?php } ?>
                            <!--user Admin choice ends-->

                            <td>{!! isset($booking->user_detail->dob)?$booking->user_detail->dob:"" !!}</td>
                            <td>{!! isset($booking->user_detail->dob16)?$booking->user_detail->dob16:"" !!}</td>
                            <td>
                                @if(isset($booking->user_detail->gender) && $booking->user_detail->gender == "1")
                                Male
                                @elseif(isset($booking->user_detail->gender) && $booking->user_detail->gender == "2")
                                Female
                                @elseif(isset($booking->user_detail->gender) && $booking->user_detail->gender == "3")
                                Other
                                @endif
                            </td>
                            <td>{!! isset($booking->user_detail->class_letter)?$booking->user_detail->class_letter:"" !!} {!! isset($booking->user_detail->class_number)?$booking->user_detail->class_number:"" !!}</td>
                            <td>{!! isset($booking->user_detail->lastname)?$booking->user_detail->lastname:"" !!} {!! isset($booking->user_detail->name)?$booking->user_detail->name:"" !!}</td>
                            <td>{!! isset($booking->user_detail->phonenumber)?$booking->user_detail->phonenumber:"" !!}</td>
                            <td>{!! isset($booking->user_detail->email)?$booking->user_detail->email:"" !!}</td>
                            <td>{!! isset($booking->user_detail->about_yourself)?$booking->user_detail->about_yourself:"" !!}</td>
                        </tr>
                        */?>
                        @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
    $(document).ready(function() {
    $('#table').DataTable();
    });</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<script>
    $(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
    });
    });</script>
<script src="{{Asset('fancybox/jquery.fancybox.min.js')}}"></script>
@stop