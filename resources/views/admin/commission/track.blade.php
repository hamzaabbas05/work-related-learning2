@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
 <section class="content-header">
   <a target="_blank" href='{{ URL::to("admin/commisionexport") }}' class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;float:right;"  >Export</a>
            </section>
<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                   {{$bredCrum}}
                </h4>
            </div>
            <br />
            <div class="panel-body">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>ID</th>
                            <th>HostEmail</th>
                            <th>Booking No</th>
                            <th>Total Amount</th>
                            <th>Guest Fee</th>
                            <th>Host Fee</th>
                            <th>Payable Amount</th>
                            <th>Paid Status</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($trackObj as $obj)
                    	<tr>
                            <td>{!! $obj->id !!}</td>
                    		<td>{!! $obj->host_email !!}</td>
                            <td>{!! $obj->booking_no !!}</td>
                          <td>{!! $obj->total_amount !!}</td>
                          <td>{!! $obj->guest_fee !!}</td>
                          
                              <td>{!! $obj->host_fee !!}</td>
                              <td>{!! $obj->payable_amount !!}</td>
            				 <td>{!! $obj->paid_status !!}</td>
                             <td>{!! $obj->dateAdded !!}</td>
            			</tr>
                    @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
$(document).ready(function() {
	$('#table').DataTable();
});
</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop