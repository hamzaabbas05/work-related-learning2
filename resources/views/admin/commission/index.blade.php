@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
@include('admin.partials.bredcrum')
 <section class="content-header">
                <a href="{{ URL::to('admin/commission/create') }}" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;float:right;"  >Add New</a>
            </section>
<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                   {{$bredCrum}}
                </h4>
            </div>
            <br />
            <div class="panel-body">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($currentObj as $obj)
                    	<tr>
                            <td>{!! $obj->id !!}</td>
                    		<td>{!! $obj->name !!}</td>
                            <?php $type= "Percentage";$symbol = "%";
                                if($obj->cancellation_type == 1) {
                                    $type= "Fixed";$symbol = "";
                                }
                            ?>
            				 <td>{{$type}}</td>
                             <td>{!! $obj->amount.$symbol !!}</td>
            				<td>
            					@if($obj->status == 1)
            						Active
            					@else
            						DeActive
            					@endif
            				</td>
            			    <td>
                                <a href="{{ URL::to('admin/commission/' . $obj->id . '/edit') }}"><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Cancellation"></i></a>
                            </td>
            			</tr>
                    @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
$(document).ready(function() {
	$('#table').DataTable();
});
</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop