@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
@inject('settings', 'App\Settings')
<?php
$settingsObj = $settings->settingsInfo();
?>
     @include('admin.partials.bredcrum')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                            {{$bredCrum}}
                        </h3>
                                <span class="pull-right clickable">
                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                </span>
                    </div>
                    <div class="panel-body">

                        <!-- errors -->
                        <div class="has-error">
                             @include('partials.errors')
                        </div>

                        <!--main content-->
                        <div class="col-md-12">

                            <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                            {!! Form::open(['action'=>'SettingsController@storeGeneral', 'method' => 'post', 'files'=>true, 'class' => 'form-wizard form-horizontal']) !!} 
                             {{ csrf_field() }}  
                            <section>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Site Title *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control required" name="site_title" type="text" value="{{$settingsObj->site_title}}" />
                                        </div>
                                    </div>

                                      <div class="form-group">
                                        <label class="col-sm-2 control-label">Site Slogan *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control required" name="site_slogan" type="text" value="{{$settingsObj->site_slogan}}" />
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-sm-2 control-label">Site Meta Title *</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control required" name="site_meta_title">{{$settingsObj->site_meta_title}}"</textarea>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-sm-2 control-label">Site Meta Keyword *</label>
                                        <div class="col-sm-10">
                                           <textarea class="form-control required" name="site_meta_keywords" rows="3">{{$settingsObj->site_meta_keywords}}</textarea>
                                        </div>
                                    </div>
                                      
                                      <div class="form-group">
                                        <label class="col-sm-2 control-label">Site Meta Description *</label>
                                        <div class="col-sm-10">
                                           <textarea class="form-control required" name="site_meta_description" rows="3">{{$settingsObj->site_meta_description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->first('language_id', 'has-error') }}">
                                        <label for="country" class="col-sm-2 control-label">Language</label>
                                        <div class="col-sm-10">
                                            {!! Form::select('language', $languages, $settingsObj->language_id, ['class'=>'form-control', 'name' => 'language_id', 'id' => 'language_id','required']) !!}
                                        </div>
                                        <span class="help-block">{{ $errors->first('language_id', ':message') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->first('currency_id', 'has-error') }}">
                                        <label for="country" class="col-sm-2 control-label">Currency</label>
                                        <div class="col-sm-10">
                                           {!! Form::label('Currency','Currency') !!}
    {!! Form::select('currency', $currencies, $settingsObj->currency_id, ['class'=>'form-control', 'name' => 'currency_id', 'id' => 'currency_id','required']) !!}
                                        </div>
                                        <span class="help-block">{{ $errors->first('currency_id', ':message') }}</span>
                                    </div>

                                <div class="form-group">
                                        <label for="pic" class="col-sm-2 control-label">Logo</label>
                                        <div class="col-sm-10">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;">
                                                    
                                                        <img  src="{{asset('images/logo/'.$settingsObj->logo)}}" alt="profile pic">
                                                     
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input id="pic" name="logo" type="file" class="form-control" />
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="pic" class="col-sm-2 control-label">Fav_Icon</label>
                                        <div class="col-sm-10">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;">
                                                    
                                                        <img src="{{asset('images/favicon/'.$settingsObj->favicon)}}" alt="profile pic">
                                                     
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input id="pic" name="favicon" type="file" class="form-control" />
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <p>(*) Mandatory</p>
                                    <div class="form-group">
                                    <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                    </div>
                                    </div>

                                 </section>
                         </form>
                            <!-- END FORM WIZARD WITH VALIDATION -->
                        </div>
                        <!--main content end-->
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop