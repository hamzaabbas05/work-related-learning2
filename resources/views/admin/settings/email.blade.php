@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$bredCrum}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/select2/select2-bootstrap.min.css') }}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
@inject('settings', 'App\Settings')
<?php
$settingsObj = $settings->settingsInfo();
?>
     @include('admin.partials.bredcrum')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                            {{$bredCrum}}
                        </h3>
                                <span class="pull-right clickable">
                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                </span>
                    </div>
                    <div class="panel-body">

                        <!-- errors -->
                        <div class="has-error">
                             @include('partials.errors')
                        </div>

                        <!--main content-->
                        <div class="col-md-12">

                            <!-- BEGIN FORM WIZARD WITH VALIDATION -->
                            {!! Form::open(['action'=>'SettingsController@storeEmail', 'method' => 'post', 'files'=>true, 'class' => 'form-wizard form-horizontal']) !!} 
                             {{ csrf_field() }}  
                            <section>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">SuperAdmin Email</label>
                                        <div class="col-sm-10">
                                        <input id="superadmin_email" name="superadmin_email" value="{{$settingsObj->superadmin_email}}" type="text" class="form-control">
                                        </div>
                                    </div>

                                      <div class="form-group">
                                        <label class="col-sm-2 control-label">Support Email</label>
                                        <div class="col-sm-10">
                                            <input id="support_email" name="support_email" value="{{$settingsObj->support_email}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-sm-2 control-label">No_Reply Email</label>
                                        <div class="col-sm-10">
                                          <input id="noreply_email" name="noreply_email" value="{{$settingsObj->noreply_email}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-sm-2 control-label">Contact Email</label>
                                        <div class="col-sm-10">
                                           <input id="contact_email" name="contact_email" value="{{$settingsObj->contact_email}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Contact Skype</label>
                                        <div class="col-sm-10">
                                          <input id="contact_skype" name="contact_skype" value="{{$settingsObj->contact_skype}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-sm-2 control-label">Contact Phone</label>
                                        <div class="col-sm-10">
                                          <input id="contact_phone" name="contact_phone" value="{{$settingsObj->contact_phone}}" type="text" class="form-control">
                                        </div>
                                    </div>


                                    
                                       <div class="form-group">
                                        <label class="col-sm-2 control-label">Open Timings</label>
                                        <div class="col-sm-10">
                                          <input id="open_timings" name="open_timings" value="{{$settingsObj->open_timings}}" type="text" class="form-control">
                                        </div>
                                    </div>

                                      <div class="form-group">
                                        <label class="col-sm-2 control-label">Address</label>
                                        <div class="col-sm-10">
                                           <textarea class="form-control required" name="contact_address" rows="3">{{$settingsObj->contact_address}}</textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                    <div class=" text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                    </div>
                                    </div>

                                 </section>
                         </form>
                            <!-- END FORM WIZARD WITH VALIDATION -->
                        </div>
                        <!--main content end-->
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>
@stop