@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
      <div class="container">    
	<div class="row">
    	
        <div class="col-xs-12 margin_top20 margin_bottom20">
        	<div class="col-sm-12">
                <div class="panel panel-default margin_top30">

                  <div class="panel-body padding10">
	
          		<center class="payment-success"><p>Booking Information</p></center>     				   </div>  <!--Panel end -->       
     
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">Booking No</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->Bookingno}}</label> 
                       </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">Booking Date</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->dateAdded}}</label> 
                       </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">WorkExperience Title</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$propertyObj->exptitle}}</label> 
                       </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">Start Date</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->checkin}}</label> 
                       </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">End Date</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->checkout}}</label> 
                       </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                        <label class="profile_label">Approval Status</label> 
                        </div>
                        <div class="col-xs-12 col-sm-6">
                        <label class="profile_label">{{$enquiryObj->approval}}</label> 
                       </div>
                        </div>
                         <div class="row">
                        <div class="col-xs-12 col-sm-6 text-right">
                       <a href="{{url('user/messages')}}" class="btn btn-danger" style="align:right">GoBack</a>
                        </div>
                        </div>  
                      <div class="margin_bottom20"></div>
  </div>  
  </div> 
</div>
</div>
</div><!--container end -->
</div>

@stop