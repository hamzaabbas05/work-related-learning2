<html>
    <head>
        <style>
            .divbottom 
            {
                margin-bottom:10px;
            }

            .margintop 
            {
                margin-top:50px;
            }


        </style>
    </head>
    <body>
        <div style="margin-left:60px;">
            <div>
                <!-- {!! html_entity_decode($cmsObj->page_desc) !!} --->

                <!--<h2>1) INSTITUTE OF HIGHER EDUCATION - PROMOTER</h2></div>-->
                <div class="divbottom">Secondary Education Institute (name of student's school): 
                    <span style="text-decoration:underline">{{isset($rentalObj->user->school->name)?$rentalObj->user->school->name:""}}</span>
                </div>
                <div style="font-size:12px;">Address: 
                    <span style="text-decoration:underline">{{isset($rentalObj->user->school->legal_address)?$rentalObj->user->school->legal_address:""}}</span>
                </div>

                <div style="margin:0 auto;position: relative;font-size: 20px;color:green;margin: 20px;width: 100%;text-align:center;">
                    Work Related Learning activity - Register (certified hours))
                </div>


                <?php if (is_null($rentalObj->user->student_relationship)) { ?>
                    <div class="divbottom">Student: {{$rentalObj->user->name}} {{$rentalObj->user->lastname}}</div>
                <?php } else { ?>
                    <div class="divbottom">Student: {{$rentalObj->user->sname16}} {{$rentalObj->user->ssurname16}}</div>     
                <?php } ?>

                    
                <?php if (is_null($rentalObj->user->student_relationship)) { ?>
                    <div class="divbottom">Class: {{$rentalObj->user->class_number}} {{$rentalObj->user->class_letter}}</div>
                <?php } else { ?>
                    <div class="divbottom">Class: {{$rentalObj->user->class_number16}} {{$rentalObj->user->class_letter16}}</div>     
                <?php } ?>

                <!--Class: class_letter16, class_number16 or class_letter class_number that I think are same in DBokay--> 

                <div class="divbottom">Hosting Organisation: {{$rentalObj->host->orgname}}</div>



                <table border="1">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Morning working hours</th>
                            <th>Afternoon working hours</th>
                            <th>Number of hours</th>
                            <th>Daily activity performed</th>
                            <th>(Work Tutor’s) Signature</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowspan="1"></td>
                            <td rowspan="2"></td>
                            <td rowspan="2"></td>
                            <td rowspan="1"></td>
                            <td rowspan="4"></td>
                            <td rowspan="1"></td>
                        </tr>
                        <tr>
                            <td rowspan="1"></td>
                            <td rowspan="2"></td>
                            <td rowspan="2"></td>
                            <td rowspan="1"></td>
                            <td rowspan="4"></td>
                            <td rowspan="1"></td>
                        </tr>
                        
                    </tbody>
                </table>


            </div>
    </body>
</html>