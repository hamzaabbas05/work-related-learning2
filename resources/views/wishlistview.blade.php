@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
<div class="container">
	<div class="col-sm-12 no-hor-padding">
		          		
        <div class="col-xs-12 margin_top20 margin_bottom20">
                	<div class="col-sm-1 text-center imgleft">
						<span class="airfcfx-user-icon profile_pict inlinedisplay" style="width:60px;background-image:url('{{asset('images/profile/'.$userObj->profile_img_name)}}');"></span>                    </div>
                    <div class="col-sm-9">
                    <a href="#" class="text-danger"><h4 class="margin_top0"><b>{{$userObj->name}}'s Wish Lists</b></h4></a>
                    <?php if (!is_null($userObj->wishlists) ){ ?> 
                    <p> Wishlists: <b>{{$userObj->wishlists->count()}}</b></p>
                    <?php } else { ?><p> Wishlists: <b>0</b></p> <?php } ?>
                    </div> 
                   	<!--div class="col-sm-2">
                    <button class="btn btn_google pull-right " data-toggle="modal" data-target="#myModal">Create a New Lists</button>
                    </div-->
        </div>
    </div> <!-- row end -->
     <div class="col-sm-12">  
<?php foreach($userObj->wishlists as $wishObj) { 
  $id = base64_encode($wishObj->id);

  ?>
       <div class="col-xs-12 col-sm-4  margin_bottom20 "> 
                  <a href='{{url("user/wishlist/$id")}}' class="wish_list">
                  <div style="" class=" bg_img mywish text-center "> 
                  <h2 class="padding_top120 text_white pos_rel z_1">{{$wishObj->name}}</h2> 
                  <button class="btn btn_wish pos_rel z_1">{{$wishObj->properties->count()}} Listings</button>         
                  </div>                  
                  </a>
				</div>

   
   

<?php } ?> </div> 
     <!-- row end -->

</div> <!-- container end -->
</div>
@stop