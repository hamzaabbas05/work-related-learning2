@extends('layouts.searchdefault')
@section('nav_bar')
@inject('settings', 'App\Settings')
<?php
$settingsObj = $settings->settingsInfo();
$logoname = $settingsObj->logo;
$profile_noimage = $settingsObj->no_image_profile;
//{{ Request::get('a') }}
?>

<style>
    div.search_sec input#check-in, div.search_sec input#check-out{
        padding-left:5px;
        border-left:1px solid #ccc ! important;
        border-right:0px ! important;
    }
    div.search_sec input{
        width:130px;
    }
    div.search_sec #where-to-go{
        width:190px;
    }
</style>
<div id="divLoading" style="z-index: 999999; display: none;background-image : url('{{asset('images/load.gif')}}');"> </div>
<nav class="navbar navbar-default norm_nav ">
    <div class="navbar-header">
        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="airfcfx-mobile-navbar navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="{{url('/')}}" class="navbar-brand"> 
            <img
                src="<?php echo url('images/logo/' . $logoname) ?>" style="height:75px;" alt="holidaysiles"
                alt="holidaysiles" class="img-responsive margin_top10" />
        </a>
        <div class="pos_rel pull-left search_sec">
            <!-- <input id="where-to-go" type="text" class="form-control" placeholder="Search" 
                   value="{{ (Request::segment(1) == 'search') ? Request::segment(2) : 'UK' }}">
            <i class="fa fa-search over_input"></i> -->
            <?php 
            // echo "<pre>";
            //     print_r();
            // echo "</pre>";
            ?>
            <!-- <input id="check-in" type="text" class="airfcfx-right-border form-control" placeholder="Check In" value="{{ Request::get('checkin') }}" onchange="filterDetails();"> -->
<input id="check-in" type="text" class="airfcfx-right-border form-control" placeholder="Check In" value="{{ date('d-m-Y',strtotime(\Session::get('checkin'))) }}">
            <!-- <input id="check-out" type="text" class="airfcfx-right-border form-control" placeholder="Check Out" value="{{ Request::get('checkout') }}" onchange="filterDetails();"> -->
            <input id="check-out" type="text" class="airfcfx-right-border form-control" placeholder="Check Out" value="{{ date('d-m-Y',strtotime(\Session::get('checkout'))) }}">

        </div>                
    </div>
    <input id="place-lng" type="hidden" value="0">
    <input id="place-lat" type="hidden" value="0">

    <div class="navbar-collapse collapse" id="navbar">

        <ul class="nav navbar-nav navbar-right">
            <!--  <li class="dropdown">
                <form action="/language" method="GET">
                  <select name="language">
  <option value="fr">French</option>
  <option value="en" selected>English</option>countryid
  </select>               <button type="submit">Change</button>             </form>       
              </li>  -->    
            <li class="dropdown"><a href="{{url('faq')}}" 
                                    class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-question-icon"></span><span class="airfcfx-menu">Help</span></a>
            </li>
            @if (Auth::guest())
            <li class="dropdown"><a href="{{url('signup')}}" aria-expanded="false"
                                    aria-haspopup="true" role="button" data-toggle="modal"
                                    class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-signup-icon"></span><span class="airfcfx-menu">Sign Up</span></a>
            </li>
            <li class="dropdown"><a href="{{url('signin')}}" aria-expanded="false"
                                    aria-haspopup="true" role="button" data-toggle="modal"
                                    class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-login-icon"></span><span class="airfcfx-menu">Login</span></a>
            </li>

            @else
            <?php if (!is_null(Auth::user()->properties) && Auth::user()->properties->count() > 0) { ?>
                <li class="dropdowns"><a href="#" aria-expanded="false"
                                         aria-haspopup="true" role="button" data-toggle="dropdown"
                                         class="airfcfx-menu-link dropdown-toggle pos_rel"><span class="airfcfx-menu-host-icon"></span>
                        <span class="airfcfx-menu">Host</span> 
                    </a>

                    <ul class="dropdown-menu padding20 profil_menu">
                        <a href="{{URL::to('user/listing/rentals/active')}}" class="rm_text_deco"><li
                                class="margin_top10 margin_bottom10">Your Listings</li> </a>          
                        <div class="border_bottom"></div>

                        <a href="{{URL::to('user/reservations')}}" class="rm_text_deco"><li
                                class="margin_top10 margin_bottom10">Your Reservations</li> </a>
                        <div class="border_bottom"></div>
                        <a href="{{URL::to('rentals/add')}}" class="rm_text_deco"><li
                                class="margin_top10 margin_bottom10">List Your Space</li> </a>
                        <div class="border_bottom"></div>
                        <a href="{{URL::to('user/transactions')}}" class="rm_text_deco">
                            <li class="margin_top10 margin_bottom10">Transaction History</li>
                        </a>
                    </ul>         
                </li>  <?php } ?>
            <li class="dropdowns "><a href="" aria-expanded="false"
                                      aria-haspopup="true" role="button" data-toggle="dropdown"
                                      class="airfcfx-menu-link dropdown-toggle pos_rel"><span class="airfcfx-menu-message-icon"></span><span class="airfcfx-menu">Messages</span>  
                </a>

                <ul class="dropdown-menu padding20 profil_menu">
                    <a href="{{URL::to('user/messages')}}" class="rm_text_deco"><li
                            class="margin_top10 margin_bottom10">Inbox</li> </a>          
                    <div class="border_bottom"></div>



                </ul>         
            </li>

            <li class="dropdowns">
                <a href="/dashboard"aria-expanded="false"
                   aria-haspopup="true" role="button" data-toggle="dropdown"
                   class="airfcfx-menu-link dropdown-toggle pos_rel margin_right padd_right_60"> <span class="airfcfx-profilename">{{Auth::user()->name}}</span>
                       <?php
                       $imgpath = asset('images/profile/' . Auth::user()->profile_img_name);
                       if (is_null(Auth::user()->profile_img_name)) {
                           $imgpath = asset('images/noimage/' . $settingsObj->no_image_profile);
                       }
                       ?>


                    <span class="profile_pict pos_abs" style="background-image:url({{$imgpath}}));height:40px;width:40px;"></span>        </a>
                <ul class="dropdown-menu padding20 profil_menu">

                    <a href="{{URL::to('user/view')}}"
                       class="rm_text_deco"><li class=" margin_bottom10">My Profile</li>
                    </a>
                    <div class="border_bottom"></div>
                    <a href="{{URL::to('user/dashboard')}}" class="rm_text_deco">
                        <li class="margin_top10 margin_bottom10">View Dashboard</li>
                    </a><div class="border_bottom"></div>
                    <a href="{{URL::to('user/wishlist')}}" class="rm_text_deco">
                        <li class="margin_top10 margin_bottom10">My Wishlists</li>
                    </a><div class="border_bottom"></div>
                    <a href="{{URL::to('user/listing/rentals/active')}}"
                       class="rm_text_deco"><li class="margin_top10 margin_bottom10">Your Listings</li>
                    </a>
                    <div class="border_bottom"></div>

                    <!-- <a href="{{URL::to('user/listing/wishlists')}}" class="rm_text_deco"><li
                      class="margin_top10 margin_bottom10">Wish List</li> </a>
                    <div class="border_bottom"></div> -->
                    <a href="{{URL::to('user/edit')}}" class="rm_text_deco"><li
                            class="margin_top10 margin_bottom10">Edit Profile</li> </a>

                    <div class="border_bottom"></div>
                    <a href="{{URL::to('user/changepassword')}}" class="rm_text_deco">
                        <li class="margin_top10 margin_bottom10">Account Settings</li>
                    </a>
                    <div class="border_bottom"></div>
                    <a href="{{URL::to('logout')}}" class="rm_text_deco"><li
                            class="margin_top10">Logout</li> </a>
                </ul>
            </li>
            @endif


        </ul>
    </div>
    <!--/.nav-collapse -->
    <!--/.nav-collapse -->

</nav>

@stop
@inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>
{{-- content --}}
@section('content')

<div class="split_cell_1 bg_white pos_rel" >
    <div class="listbucketitem" onClick="showbucket();" style="padding-top: 12px; cursor: pointer;">View Bucket</div>
            <div calss="viewbucket" id="viewbucket"></div>

    <div class="filter_menu2">

        <div class="padd_lf_rg_20" >

            <div class="row">
                <?php
                // echo "Count :".count($propertyObj). "<br> Count 1 :".count($propertyObj1)."<br>";
               /* $mergePropertyData = array();
                foreach($propertyObj as $row){
                    $mergePropertyData[] = $row;
                }

                foreach($propertyObj1 as $row1){
                    $mergePropertyData[] = $row1;
                }*/
                    // echo "ID :".$pob1->id;
                    // echo "ID :".$pob1['exptitle'];
                /*echo "Count Merge :".count($mergePropertyData);
                    echo "<pre>";
                       print_r($mergePropertyData);
                    echo "</pre>";
                exit;*/
                    // echo "<pre>";
                    //     print_r($propertyObj1);
                    // echo "</pre>";
                    // exit;
                $buildServiceDetails = array();
                foreach($propertyObj as $obj){
                    // echo "<pre>";
                    //     print_r($obj);
                    // echo "</pre>";

                    $buildServiceDetails[$obj->propertytype->name][] = $obj;
                    // $buildServiceDetails[$obj->propertytype['name']][] = $obj;
                    
                    // echo "<br> Name :".$obj->propertytype->name." >>  Listing Type :".$obj->propertytype->listing_type."<br>";
                }
                        // echo "<pre>";
                        //   print_r($buildServiceDetails);
                        // echo "</pre>";
                        // exit;
                
                $htmlServices = '';
                if(count($buildServiceDetails)){
                    $imgIndex = 0;
                    // echo "<br>Row :".@$row."<br>";
                    $htmlServices = '<div class="row">';
                    foreach($buildServiceDetails as $service => $row){
                        // echo "<br> SErvice Name :".$service."<br> Row count :".count($row)."<br>";


                        // echo "Img Name :<pre>";
                // $propertyImag = $row[$j]['propertyimages'][0]->img_name;
                // print_r($row[0]['propertyimages'][0]->img_name);
// exit;
                // echo "</pre>";
                // foreach($row[0]['propertyimages'] as $key=>$val){
                //     echo "<br> Img Name :".$val->img_name."<br>";
                //     echo "<pre> Key :";
                //     print_r($key);
                //     echo "<Val >";
                //     print_r($val);
                //     echo "</pre>";
                // }
                // exit; C:\laravel\work-related-learning\public/images/ren/0-2018-12-27-16-19-41-1545927581.png
                       if(count($row) > 0){
                            for($j=0;$j<count($row);$j++){ 
                                $propertyImag = 'http://localhost:8000/images/no_prop_image.png';
                                if(count($row[$j]['propertyimages']) > 0){

                                    if(file_exists("public/images/".$row[$j]->user_id."/".$row[$j]['propertyimages'][$imgIndex]->img_name)){
                                        $propertyImag = "public/images/".$row[$j]->user_id."/".$row[$j]['propertyimages'][$imgIndex]->img_name;
                                    }
                                }
            $htmlServices .= '<div class="col-xs-12 col-sm-6 margin_top10 detailsContainer" data-proptitle="Semi-detached house with independent annex and three bed : Twin with sofabed en-suite and kitchenette, with spare bedroom in the house. Rooms: 1 in annex, 1 in the house" data-propid="144" data-itemtype="school" data-lat="45.75972388" data-lng="9.79973547">
            <div id="carousel-example-generic0" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox" onmouseover="showme(0)" onmouseout="hideme(0)">
                    <a href="'.url('view/school_house/'.$row[$j]['id']).'" target="_blank" title="no image found" class="item bg_img active" style="height:250px;width:370px;background-image:url(\''.$propertyImag.'\');">
                    </a>
                </div>';
            $htmlServices .= '<!-- Controls -->
                                
                <a href="'.url('view/school_house/'.$row[$j]['id']).'" target="_blank" title="">
                    <div class="bg_img1" style="margin-right:50px;height:56;width:56;background-image:url(\''.$propertyImag.'\');"></div>
                </a> 
                <!-- Wish List -->
                                <a href="http://localhost:8000/signup">
                    <div class="favorite" style="margin-right:55px;"><i class="fa fa-heart-o"></i><i class="fa fa-heart fav_bg"></i></div>
                </a>
                                <div class="favorite" style="margin-right:0px; left: 30px; width: 10%;">
                                        <img src="http://localhost:8000/images/School_icon.png" style="width: 35px;" alt="">
                                    </div>
                <!-- Wish List -->
            </div><!--carousel-example-generic end-->

            <!-- Property Name -->
            <a href="'.url('view/school_house/'.$row[$j]['id']).'" target="_blank" title="Semi-detached house with independent annex and three bed : Twin with sofabed en-suite and kitchenette, with spare bedroom in the house. Rooms: 1 in annex, 1 in the house">
                <p class="airfcfx-search-listing-name margin_top10 margin_left10 fa-1x text_gray1">'.$service.'</p></a>


            <a href="'.url('view/school_house/'.$row[$j]['id']).'" target="_blank" title="Semi-detached house with independent annex and three bed : Twin with sofabed en-suite and kitchenette, with spare bedroom in the house. Rooms: 1 in annex, 1 in the house">
                                                <p class="small margin_left10 text_gray1">
                    <b>'.$row[$j]->exptitle.'('.$row[$j]->property_type.')</b>

                    
                    <span class="text-warning">
                                            </span>
                </p>
                                <!--<p></p>-->
                            </a>
                            ';
                            if($row[$j]['booking_style'] == 1){
                                $today = date("Y-m-d");
                                $userId = $row[$j]->user_id;
                                $checkin = date("Y-m-d", strtotime(\Session::get('checkin')));
                                $checkout = date("Y-m-d", strtotime(\Session::get('checkout')));
                                // \Session::put('checkout', $checkout);
                                $cartData =  App\Cartlists::with(array('user'))->where('user_id',$userId)->where('property_id',$row[$j]->id)->where(function($whr) use ($today, $checkin, $checkout){
                                        $whr->where("start_date","<=",$checkin)->where("end_date",">=",$checkout)->orwhere("start_date",">=",$checkin)->where("end_Date",">=",$checkout);
                                })->get();


                                // $cartData = App\Cartlists::where('user_id',$row[$j]->user_id)->where('property_id',$row[$j]->id)->where("start_date","<=",$today)->where("end_date",">=",$today)->orwhere("start_date",">=",$today)->where("end_Date",">=",$today)->get();
                                if(count($cartData) > 0){
                                    $htmlServices .= '<button type="button" data-id="'.$row[$j]->id.'" id="requestbtn_'.$row[$j]->id.'" class="btn-pad btn btn-danger full_width" disabled=""><b>Add to bucket</b></button>';
                                }else{
                                    
                                    $htmlServices .= '<button type="button" data-id="'.$row[$j]->id.'" onclick="add_to_cart('.$row[$j]->id.');" id="requestbtn_'.$row[$j]->id.'" class="btn-pad btn btn-danger full_width"><b>Add to bucket</b></button>';
                                }

                            }
        $htmlServices .= '</div><!--col-sm-6 end-->';


        }
        }

     /*$htmlServices .= '<div style="border: 1px solid #dce0e0; padding: 10px; margin-top: 10px;">';
        $htmlServices .= '<div class="row" id="description">
                                        <div class="col-xs-12 col-sm-4">
                                            <p class="margin_top20 headtxt text_gray1">Service Title</p>
                                        </div><!--col-sm-4 end-->
                                        <div class="col-xs-12 col-sm-8">
                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                '.$row[0]->exptitle.'
                                            </p>
                                        </div><!--col-sm-8 end-->
                                    </div>'; 
        $htmlServices .= '<div class="row" id="description">
                                        <div class="col-xs-12 col-sm-4">
                                            <p class="margin_top20 headtxt text_gray1">Service Description</p>
                                        </div><!--col-sm-4 end-->
                                        <div class="col-xs-12 col-sm-8">
                                            <p class="margin_top20" style="word-wrap:break-word;">
                                                '.$row[0]->description.'
                                            </p>
                                        </div><!--col-sm-8 end-->
                                    </div>'; 

        $htmlServices .=  '</div>';*/
                        /*echo '<div class="row">';
                        echo '<div class="col-xs-12 col-sm-6 margin_top10 detailsContainer">';
                        echo '<div id="carousel-example-generic0" class="carousel slide" data-ride="carousel">';
                        echo '<div class="carousel-inner" role="listbox" onmouseover="showme(0)" onmouseout="hideme(0)">';
                        echo "<div class='margin_top10'>".$service."</div>";
                        if(count($row) > 0){
                            for($j=0;$j<count($row);$j++){
                                echo "<div>".$row[$j]->exptitle."</div>";
                                echo "<div>".$row[$j]->description."</div>";
                                echo "<div>".$row[$j]->optional_description."</div>";
                                // echo "<div>".$row[$j]->exptitle."</div>";
                            }
                        }
                        echo "</div>"; //carousel-inner end here;
                        echo "</div>"; //carousel-inner end here;
                        echo "</div>"; //carousel-example-generic0 end here;
                        echo "</div>"; //detailsContainer end here;*/
                    }
                    $htmlServices .= '</div>';
                
                    //  echo "<pre>";
                    //     print_r($buildServiceDetails);
                    // echo "</pre>";
                }
                echo $htmlServices;
                // 

                // exit;
                // if ($propertyObj) {
                //     echo '<strong style="font-size:17px;">Your currently accepted job: </strong>' . $propertyObj->exptitle . '';
                // }

                ?>
                <!--                <div class="col-xs-12 col-sm-3 margin_top30">
                                    <label class="margin_top10 one">Dates</label>
                                </div>col-sm-3 end-->
                <!--<div class="col-xs-6 col-sm-3 margin_top30">
                    <label class="">Start Date</label>
                    <input id="check-in" type="text" class="airfcfx-right-border form-control" placeholder="Check In" value="" onchange="filterDetails();">
                </div>--><!--col-sm-3 end-->

                <!--<div class="col-xs-6 col-sm-3 margin_top30">
                    <label class="">End Date</label>
                    <input id="check-out" type="text" class="airfcfx-right-border form-control" placeholder="Check Out" value="" onchange="filterDetails();">
                </div>--><!--col-sm-3 end-->
                <!--<div class="col-xs-6 col-sm-6 margin_top30">
                    <label class="">Address</label>
                    <input id="searchAddress" type="text" class="airfcfx-right-border form-control" placeholder="Address" value="" >
                </div>--><!--col-sm-3 end-->


            </div><!--row end-->

            <div class="border_bottom2 margin_top20"></div>





            <div class="border_bottom2 margin_top20"></div>
            <input type="hidden" id="countryid" value="">




        </div>

        <div class="filter_menu4 margin_bottom30">
            <div class="bg_gray1 margin_top50">


                <div class="airfcfx-mobile-morefilter-hide bg_gray1 padd_lf_rg_20 fix_pls">

                </div><!--bg_gray1 end-->



                <div id="search-data" class="bg_gray1 padd_lf_rg_20 padd_bottom30">
                    <!-- <div class="alert alert-danger text-center"><strong>Please search for something..</strong></div> -->
                    <div class="border_bottom"></div>


                </div><!--padd_lf_rg_20 end-->
            </div> <!--bg_gray1 end-->
        </div><!--filter_menu4 end-->



    </div>  <!--cell_1 end-->
</div>

<script>
        /*************************/
        function urldecode(str) {
        return decodeURIComponent((str + '').replace(/\+/g, '%20'));
        }
        $(document).ready(function(){
        var getAddress = $('#where-to-go').val();
        if (getAddress != ''){
        $('#where-to-go').val(urldecode(getAddress));
        }

        //filterDetails();
        });
        function ajaxloaderToggle(event){
            // alert(event);
            if (event == 'show'){
                $('#divLoading').show();
                $("#divLoading").attr("data-id",event);
            } else{
                $("#divLoading").attr("data-id",event);
                $('#divLoading').hide();
            }
        }
        // To filter and search result , show on left sidebar
        function filterDetails1(){
            var location = $('#where-to-go').val();
            if (location == ''){
                location = 'UK';
            }
            var checkinDate = $('#check-in').val();
            var checkoutDate = $('#check-out').val();
            var getFilter = [];
            $('#customFilters').find('input[type="checkbox"]').each(function(){
                var chkStatus = $(this).is(':checked');
                if (chkStatus){
                    var getVal = $(this).val();
                    getFilter.push(getVal);
                }
            });
        /*if(getFilter.l Work,ength == 0){
         // to remove the data in result panel
         $('#search-data').html('<div class="alert alert-danger text-center">No records found. Please search for something..</div>');
         return false;
         }*/
            ajaxloaderToggle('show');
        $.ajax({
        url: '{{ url('/ajaxSearch') }}',
                type: 'post',
                data:'search=' + getFilter + '&location=' + location + '&checkinDate=' + checkinDate + '&checkoutDate=' + checkoutDate,
                success: function(data){
                    var html = data + '<div class="border_bottom"></div>'
                    $('#search-data').html(html);
                    
                    setTimeout(function(){
                        ajaxloaderToggle('hide');
                        var chkResultItems = $('.detailsContainer').length;
                        $('span.total-count-detail').text(chkResultItems);
                        setMapData();
                        bindHoverEvents();
                    }, 600);
                },
                error: function (request, status, error) {
                //alert(request.responseText);
                $('#search-data').html('<div class="alert alert-danger text-center">Error occurred. Please search for a service: Work, Learn, Live or Enjoy</div>');
                }
        });
        }

    var markerIconSchool = '{{ url('') }}'+'/images/homepageicons/live-gmapmarker.png';// icon link/path for school icon
    var markerIconWork = '{{ url('') }}'+'/images/homepageicons/work-gmapmarker.png';// icon link/path for work icon
    
    // to set the markers on map, after getting the results from ajax.
    function setMapData(){ 
        // icon for school with scalling size .
        var iconSchool = {
            url: markerIconSchool,
            scaledSize: new google.maps.Size(35, 35), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };
        // icon for work with scalling size.
        var iconWork = {
            url: markerIconWork,
            scaledSize: new google.maps.Size(35, 35), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };
        clearMapMarkers(); // clear marker before setting new markers.
        bounds = new google.maps.LatLngBounds();//to reset the bound for markers
        $('.detailsContainer').each(function(){
            var getLat = $(this).attr('data-lat');
            var getLng = $(this).attr('data-lng');
            var itemtype = $(this).attr('data-itemtype');
            var getTitle = $(this).attr('data-proptitle');
            
            if(getLat != '' && getLng != ''){
                if(itemtype == 'work'){
                    pinIcon = iconWork; // if the item type is work, show this icon.
                }else{
                    pinIcon = iconSchool; // if the item type is school show this icon.
                }
                
                var myLatlng = new google.maps.LatLng(getLat, getLng); // to set marker position
                bounds.extend(myLatlng);
                var marker = new google.maps.Marker({
                    position: myLatlng, //marker position/location on map
                    map: map,
                    title: getTitle,
                    icon: pinIcon
                });
                bindInfowindow(marker);
                markersArr.push(marker);
            }
        });
        if(markersArr.length > 0){
            map.fitBounds(bounds);

        }
    }
    function bindInfowindow(marker){
        google.maps.event.addListener(marker, 'click', function() {
            var html = '<div style="width:300px;text-align:center;color:#4285F4;font-weight:bold;font-size:12px;line-height:18px;padding:2px;">'+this.title+'</div>';
            infowindow.setContent(html);
            infowindow.open(map, this);
        });
    }
    // to bind hover on leftside item with map markers.
    function bindHoverEvents(){
        // to bind the hover on items at left side and bounce the related marker/location on map.
        $(".detailsContainer").mouseenter(function() { //this will activate whenmouseenter into item.
            var getIndex = $(this).index();
            var getLat = $(this).attr('data-lat');
            var getLng = $(this).attr('data-lng');
            if(getLat != '' && getLng != ''){
                markersArr[getIndex].setAnimation(google.maps.Animation.BOUNCE); // to set marker animation bounce effect
            }
        }).mouseleave(function() { // this will call when mouse leave from item.
            var getIndex = $(this).index();
            var getLat = $(this).attr('data-lat');
            var getLng = $(this).attr('data-lng');
            if(getLat != '' && getLng != ''){
                markersArr[getIndex].setAnimation(null); //to remove marker mounce animation effect
            }
        });
    }
    function clearMapMarkers(){
        if(markersArr.length > 0){
            for(i in markersArr){
                markersArr[i].setMap(null);
            }
            markersArr = [];
        }
    }
    
        /*************************/
//more filter
        $(document).ready(function() {
        var project3 = $('.fix_pls_span').offset();
        var $window = $('.split_cell_1');
        $window.scroll(function() {
//start fix
        /*if ( $window.scrollTop() >= project3.top) {
         $(".fix_pls").addClass("fixed1");
         }
         else {
         $(".fix_pls").removeClass("fixed1");
         } */
        $(".fix_pls").removeClass("fixed1");
        });
        });
//slideer stop
        $('.carousel').carousel({
        interval: false
        })

                $(document).ready(function(){
        $(".toggle_foot, .close_x").click(function(){
        $(".toggle_foot1").toggleClass("foot_ads");
        });
        });
//Range slider  
        $(document).ready(function(){
        $(".filter_menu_btn").click(function(){
        $(".filter_menu").toggleClass("filter_menu1");
        $(".filter_menu2").toggleClass("filter_menu3");
        $(".filter_menu4").toggleClass("filter_menu5");
        $(".bottom_filter").toggleClass("filter_menu1");
        });
        });
        /*$(function () {
         $("#check-in").datepicker({
         minDate:new Date(),
         onSelect: function (selected) {
         var dt = new Date(selected);
         dt.setDate(dt.getDate() + 1);
         $("#check-out").datepicker("option", "minDate", dt);
         updateSearchList('#check-in', 'indate');
         },
         onClose: function (selectedDate) {
         $("#check-out").datepicker('show');
         }   
         });
         edate = new Date();
         edate.setDate(edate.getDate()+1); 
         $("#check-out").datepicker({
         minDate:edate,
         onSelect: function (selected) {
         var dt = new Date(selected);
         dt.setDate(dt.getDate() - 1);
         $("#check-in").datepicker("option", "maxDate", dt);
         updateSearchList('#check-out', 'outdate');
         }
         });
         });*/

        var baseLat = 0;
        var baselng = 10;
        var zoomval = 5;</script>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAz0HqeAgq5xNt9DSUSMPIVlmLK_uuAoDc&libraries=places" ></script> -->
<!-- <script async defer type="text/javascript" src="{{ asset('frontendassets/js/map_actions.js')}}"></script> -->
<script async defer type="text/javascript">

    var markerPoints = new Array();
    var infoMarker = new Array();
    var baseLat = 0;
    var baselng = 10;
    var locations = {lat: 0, lng: 10};
    //addMarker(locations); 
<?php /*if(isset($searchPropertyObj)){ foreach ($searchPropertyObj as $property) { ?>
        markerPoints.push(locations);
        infoMarker.push('<a href= "{{url("view/rentals/$property->id")}}"> <p>{{$property->title}}</p><span>{{isset($property->propertytype->name)?$property->propertytype->name:""}}</span><br><b>€{{$property->price_night}}</b></a>');
        //pausecontent.push(''); Pls leave comments.. okay fine..are the aprox lat logs showing here_ yes its using the same but only property latlongs.. the same of what_.. same means here its using what saved intodb, not the approx then. right_here not approx but below code may hvae need to chk that. k .
        var locations = {lat: <?php echo $property->pro_lat; ?>, lng: <?php echo $property->pro_lon; ?>};
<?php }} */ ?>


    //google.maps.event.addListener(map, 'click', function(event) {

    //});
    //showMarkers();
</script>
@if( Session::has("success") )
<div class="alert alert-success alert-block" role="alert">
    <button class="close" data-dismiss="alert"></button>
    {{ Session::get("success") }}
</div>
@endif
<div class="flash-message"></div>

@if (Session::has("success"))
   {{ Session::get("success") }}
 @endif



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog listpopupwidth" role="document">
        <div class="modal-content">

            <div class="modal-body padding0">
                <div class="toplistdiv" style="display:none;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>   
                    <h3>Save to Wish List</h3>          
                    <hr />
                </div>
                <div class="airfcfx-leftlistdiv leftlistdiv">
                    <div class="banner2 banner2hgt" id="listimage" ></div>
                </div>
                <input type="hidden" value="" id="listingid">
                <div class="airfcfx-rightlistdiv-cnt">
                    <div class="airfcfx-rightlistdiv rightlistdiv padding20 wishlisthgt">

                        <div class="airfcfx-topfullviewdiv topfullviewdiv">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
                            <h3>Save to Wish List</h3><hr />
                        </div>

                        <div class="airfcfx-wishlist-contianer" id="listsdiv"> 
                            <div id="listnames"></div>
                        </div>
                    </div>
                    <div class="airfcfx-wish-createlist-cnt">
                        <input type="text" id="newlistname" class="airfcfx-listtxt listtxt" value="" placeholder="Create New List" maxlength="20">
                        <input type="button" value="Create" class="airfcfx-createbtn btn btn-danger createbtn" onclick="create_new_list();">
                    </div> 
                    <div class="airfcfx-wishlist-btn-cnt">
                        <input type="button" value="Cancel" class="airfcfx-cancelsze btn btn_email cancelsze cancelbtn " data-dismiss="modal">
                        <!--<input type="button" value="Save" data-dismiss="modal" class="airfcfx-savebtn btn btn-primary savebtn pull-right" onclick="save_lists();">-->
                        <input type="button" value="Save" class="airfcfx-savebtn btn btn-primary savebtn pull-right" onclick="save_lists();">
                        <div class="errcls listerr"></div>
                    </div>
                </div> 

            </div>
            <div class="clear">

            </div>            
        </div>
    </div>
</div>  

<script>
    $(document).ready(function(){
    $("body").css("overflow", "hidden");
    $(".alert-success").addClass("flashcss");
    $("#closebutton").click(function(){
    $(".alert-success").removeClass("flashcss");
    });
    });
//         function showbucket(){
//     ajaxloaderToggle('show');
//     $.ajax({
//         type: 'GET',
//         url: baseurl + '/getcartlist',
//         async: false,
//         data: {},
//         success: function (data) {
//             ajaxloaderToggle('hide');
//             $("#viewbucket").html(data);
//             // alert(data);
//             // if(data != '' && data == 'add to cart'){
//             //     $("#requestbtn_"+propertyId+" b").html("Added to bucket");
//             //     $("#requestbtn_"+propertyId).attr('onclick','');
//             //     $("#requestbtn_"+propertyId).attr("disabled","disabled");
//             //     // alert("Text Name " + $("#requestbtn_"+propertyId).attr("click"));
//             // }
//             //$('#myModal').modal('toggle');
//         }
//     });    
// }
</script>



<script>



    $(document).ready(function(){
    $("#check-in").keydown(function(event){
    if (event.which == 13) {
    $("#check-in").readonlyDatepicker(true);
    }
    });
    $("#check-out").keydown(function(event){
    if (event.which == 13) {
    $("#check-out").readonlyDatepicker(true);
    }
    });
    /*$(function () {
     $("#check-in").datepicker({
     minDate:new Date(),
     onSelect: function (selected) {
     var dt = new Date(selected);
     dt.setDate(dt.getDate() + 1);
     $("#check-out").datepicker("option", "minDate", dt);
     updateSearchList('#check-in', 'indate');
     },
     onClose: function (selectedDate) {
     $("#check-out").datepicker('show');
     }   
     });
     edate = new Date();
     edate.setDate(edate.getDate()+1); 
     $("#check-out").datepicker({
     minDate:edate,
     onSelect: function (selected) {
     var dt = new Date(selected);
     dt.setDate(dt.getDate() - 1);
     $("#check-in").datepicker("option", "maxDate", dt);
     updateSearchList('#check-out', 'outdate');
     }
     });
     });*/

    $("#check-in").datepicker({
    dateFormat: "dd-mm-yy",
            minDate: 0,
            onSelect: function (date) {
            var date2 = $('#check-in').datepicker('getDate');
            date2.setDate(date2.getDate() + 1);
            $('#check-out').datepicker('setDate', date2);
            //sets minDate to dt1 date + 1
            $('#check-out').datepicker('option', 'minDate', date2);
            //updateSearchList('#check-in', 'indate');
           // filterDetails();
            }
    });
    $('#check-out').datepicker({
    dateFormat: "dd-mm-yy",
            onClose: function () {
            var dt1 = $('#check-in').datepicker('getDate');
            console.log(dt1);
            var dt2 = $('#check-out').datepicker('getDate');
            if (dt2 <= dt1) {
            var minDate = $('#check-out').datepicker('option', 'minDate');
            $('#check-out').datepicker('setDate', minDate);
            //updateSearchList('#check-out', 'outdate');
           // filterDetails();
            }
            }
    });
    $("#check-in-main").datepicker({
    dateFormat: "dd-mm-yy",
            minDate: 0,
            onSelect: function (date) {
            var date2 = $('#check-in-main').datepicker('getDate');
            date2.setDate(date2.getDate() + 1);
            $('#check-out-main').datepicker('setDate', date2);
            //sets minDate to dt1 date + 1
            $('#check-out-main').datepicker('option', 'minDate', date2);
            }
    });
    $('#check-out-main').datepicker({
    dateFormat: "dd-mm-yy",
            onClose: function () {
            var dt1 = $('#check-in-main').datepicker('getDate');
            console.log(dt1);
            var dt2 = $('#check-out-main').datepicker('getDate');
            if (dt2 <= dt1) {
            var minDate = $('#check-out-main').datepicker('option', 'minDate');
            $('#check-out-main').datepicker('setDate', minDate);
            }
            }
    });

    /*$("#check-in").datepicker({
     minDate:new Date(),
     onSelect: function (selected) {
     var dt = new Date(selected);
     dt.setDate(dt.getDate() + 1);
     $("#check-out").datepicker("option", "minDate", dt);
     },
     onClose: function (selectedDate) {
     $("#check-out").datepicker('show');
     }   
     });
     edate = new Date();
     edate.setDate(edate.getDate()+1); 
     $("#check-out").datepicker({
     minDate:edate,
     onSelect: function (selected) {
     var dt = new Date(selected);
     dt.setDate(dt.getDate() - 1);
     $("#check-in").datepicker("option", "maxDate", dt);
     }
     });*/

    /*$("#check-in-main").datepicker({
     minDate:new Date(),
     onSelect: function (selected) {
     var dt = new Date(selected);
     dt.setDate(dt.getDate() + 1);
     $("#check-out-main").datepicker("option", "minDate", dt);
     },
     onClose: function (selectedDate) {
     $("#check-out-main").datepicker('show');
     }   
     });
     edate = new Date();
     edate.setDate(edate.getDate()+1); 
     $("#check-out-main").datepicker({
     minDate:edate,
     onSelect: function (selected) {
     var dt = new Date(selected);
     dt.setDate(dt.getDate() - 1);
     $("#check-in-main").datepicker("option", "maxDate", dt);
     }
     });   */
    });</script>

</body>
</html>

<script type="text/javascript" src="{{ asset('frontendassets/js/owl.carousel.js')}}"></script>
<script type="text/javascript" src="{{ asset('frontendassets/js/jquery-ui.min.js')}}"></script>

@stop