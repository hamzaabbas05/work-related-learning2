@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('settings', 'App\Settings')
<?php
$settingsObj = $settings->settingsInfo();
?>
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">    
        @include('partials.listing_sidebar')
        <div class="col-xs-12 col-sm-9 margin_top20">        

            <div class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="panel-title">Your School House Listing</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <?php
                        if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                            // echo "Pro Images check <pre>";
                            // var_dump(is_array($first_listing->propertyimages));
                            // echo "<br> Size of :".sizeof($first_listing->propertyimages);
                            // // print_r($first_listing[0]->propertyimages);
                            // // print_r($first_listing[1]->propertyimages[0]->img_name);
                            // echo "<br> Above Image Name :<br> ";
                            // // print_r($first_listing[1]->propertyimages);
                            // // echo "First Listing Data : <pre>";
                            // var_dump(is_array($first_listing));
                            // echo "<br> size of :".var_dump(!empty($first_listing));
                            // echo "<br> size of :".var_dump(sizeof($first_listing));
                                // print_r($first_listing);
                            // echo "</pre>";
                            // exit;
                        }
                        // @if(isset($first_listing) && sizeof($first_listing) > 0 && isset($first_listing->id)) 
                        // if(is_array($first_listing) || sizeof($first_listing) > 0){
                        if(!empty($first_listing)){
                        ?>
                        <div class="airfcfx-listing-row clear">

                            <?php
                            $imgPath = asset('images/noimage/' . $settingsObj->no_image_property);
                            // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                            //     // $first_listing = $first_listing[0];
                            //     // if(sizeof($first_listing->propertyimages) > 0){
                            //     if(!empty($first_listing->propertyimages)){
                            //         foreach ($first_listing->propertyimages as $img) {
                            //             $imgPath = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
                            //         }
                                    
                            //     }
                            //     // exit;
                            // }else{
                                foreach ($first_listing->propertyimages()->take(1)->get() as $img) {
                                    $imgPath = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
                                }
                                
                            // }
                            ?>

                            <div class="col-xs-12 col-sm-5 ">
                                <?php /*
                                if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){ ?>
                                    <a href="{{URL::to('rentals/paid_firt_listing/'.$first_listing->id)}}">
                                        <div class="airfcfx-listing_img listing_img" style="background-image:url({{$imgPath}});"></div>
                                    </a>
                                <?php }else{*/?>

                                <a href="{{URL::to('rentals/paid_firt_listing/'.$first_listing->id)}}">
                                    <div class="airfcfx-listing_img listing_img" style="background-image:url({{$imgPath}});"></div>
                                </a>
                                <?php //} ?>
                            </div>
                            <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){ 
                                // echo "image goes here";
                                // exit;
                            } ?>
                            <div class="col-xs-12 col-sm-7">
                                <a href="{{URL::to('rentals/paid_firt_listing/'.$first_listing->id)}}" class="airfcfx-text_black text_black">
                                    <h4>
                                        @if(isset($userObj->paid_service->house_slogan) && !empty($userObj->paid_service->house_slogan))
                                        {{ isset($userObj->paid_service->house_slogan)?$userObj->paid_service->house_slogan:''}}
                                        @else
                                        {{isset($first_listing->exptitle)?$first_listing->exptitle:''}}
                                        @endif
                                    </h4>
                                </a>
                                <p>
                                    @if(isset($userObj->paid_service->house_desc) && !empty($userObj->paid_service->house_desc))
                                    {{ isset($userObj->paid_service->house_desc)?$userObj->paid_service->house_desc:''}}
                                    @else
                                    {{isset($first_listing->description)?$first_listing->description:''}}
                                    @endif
                                </p>
                                <a class="margin_top_5 text-danger" href="{{URL::to('rentals/first_calendar_paid/'.$first_listing->id)}}">
                                    Managing Listing and Calender
                                    <!--                                    we should allow to manage calendar and other functions to be commented out *so I was wrong saying to redirect to edit profile only..
                                                                        as in this way user can update its first listing so it will be same like updating job or paid services
                                                                        I was thinking of all the info inside normal job or .type's
                                                                        Here we need calendar, and perhaps to duplicate paid.services info for some, but for speeding up, I would allow simply calendar
                                                                        agree_
                                                                        so user can edit only calendar for his first listing ? It is for speedy job. We will comment out rest and work on it later..
                                                                        let me finsh this listing squar box first than i will add what you want for edit . sure
                                    
                                    -->
                                </a>
                            </div>
                            <div class="airfcfx-listing-btn-cnt col-xs-12 col-sm-6" style="margin-bottom: 10px;">
                                <!--and .type empty_ yes will check that dont worry-->
                                <a href="{{URL::to('user/paidServices')}}" class="airfcfx-edit-list-btn">
                                    <button class="btn btn_dashboard margin_top20 pull-right">Edit School House</button>
                                </a>
                                <button onClick="remove_listing(<?php echo $first_listing->id; ?>)" class="btn btn_dashboard margin_top20 pull-right">Delete listing </button>
                                
                            </div>
                            <br />
                        </div>
                        <?php
                            // @endif
                            // @endif
                        } ?>
                    </div>
                </div>
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading" style="margin-top: 10px;">
                    <h3 class="panel-title">Your Optional Services - Listings</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php if (!empty($propertyObj)) { ?>
                        <?php //if (is_array($propertyObj) || sizeof($propertyObj) > 0) { ?>
                        <?php //if (sizeof($propertyObj) > 0) { ?>

                            <?php foreach ($propertyObj as $obj) { ?>
                                <div class="airfcfx-listing-row clear">

                                    <?php
                                    $imgPath = asset('images/noimage/' . $settingsObj->no_image_property);
                                    foreach ($obj->propertyimages()->take(1)->get() as $img) {
                                        $imgPath = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
                                    }
                                    ?>
                                    <div class="col-xs-12 col-sm-5 ">
                                        <a href="#"><div class="airfcfx-listing_img listing_img" style="background-image:url({{$imgPath}});"></div></a>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <a href="#" class="airfcfx-text_black text_black"><h4>{{$obj->exptitle}}</h4></a>
                                        @if($obj->property_status == "paid")
                                        <a class="margin_top_5 text-danger" href="{{URL::to('rentals/basic_paid/'.$obj->id)}}">Managing Listing and Calender</a>
                                        @else
                                        <a class="margin_top_5 text-danger" href="{{URL::to('rentals/basic/'.$obj->id)}}">Managing Listing and Calender</a>
                                        @endif
                                    </div>
                                    <div class="airfcfx-listing-btn-cnt col-xs-12 col-sm-6">
                                        @if($obj->property_status == "paid")
                                        <a href="{{URL::to('rentals/basic_paid/'.$obj->id)}}" class="airfcfx-edit-list-btn">
                                            <button class="btn btn_dashboard margin_top20 pull-right">Edit listing</button>
                                        </a>

                                        <button onClick="remove_listing(<?php echo $obj->id; ?>)" class="btn btn_dashboard margin_top20 pull-right">Delete listing </button>
                                        @else
                                        <a href="{{URL::to('rentals/basic/'.$obj->id)}}" class="airfcfx-edit-list-btn">
                                            <button class="btn btn_dashboard margin_top20 pull-right">Edit listing</button>
                                        </a>
                                        <button onClick="remove_listing(<?php echo $obj->id; ?>)" class="btn btn_dashboard margin_top20 pull-right">Delete listing </button>
                                        @endif
                                        <!--<a href="{{URL::to('rentals/basic/'.$obj->id)}}" class="airfcfx-view-list-btn">
                                        <button class="btn btn_dashboard margin_top20 pull-right">View list</button>
                                        </a>--></div><br>
                                </div> 
                                <?php
                            }
                        }
                        ?>
                        <br><br><br>
                         <!--      <div align="center"><ul class="pagination"><li class="prev disabled"><span>«</span></li>
                }
        <li class="active"><a href="/user/listing/mylistings?page=1&amp;per-page=20" data-page="0">1</a></li>
        <li><a href="/user/listing/mylistings?page=2&amp;per-page=20" data-page="1">2</a></li>
        <li><a href="/user/listing/mylistings?page=3&amp;per-page=20" data-page="2">3</a></li>
        <li class="next"><a href="/user/listing/mylistings?page=2&amp;per-page=20" data-page="1">»</a></li></ul></div>            -->      </div> <!--row end -->

                </div>

            </div> <!--Panel end -->





        </div>
    </div> <!--container end -->
</div>


<script>
    $(document).ready(function () {
        $(".show_ph").click(function () {
            $(".add_phone").show();
            $(".show_ph").hide();
        });
        $(".add_cont").click(function () {
            $(".add_contact").toggle();
        });
        $(".add_ship").click(function () {
            $(".add_shipping").toggle();
        });
    });

</script>  
@stop