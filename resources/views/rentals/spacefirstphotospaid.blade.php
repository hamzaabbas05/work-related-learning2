@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
<div class="bg_gray1">
<!-- DONE -->
    <div class="container mrgnset">
        @include('partials.spacesidebarfirstlistingpaid')
        <!--col-sm-3 end -->
        <div class="col-xs-12 col-sm-9 sdecls" style="width: 60%;">

            <div class="panel panel-default panelcls">
                <div class="panel-body">
                    <div class="row">

                        <div class="col-xs-12 margin_top10 margin_bottom20  commcls" id="photosdiv">
                            <h3>Pictures for a presentable listing</h3>
                            Add at least 1 picture of the Service, this may be real, possibly, or just representative<hr />
                            <br />

                            <div class="col-sm-6 coldiv">
                                <!-- Note: [Upload images in .png or .jpg formats only], Upload Pictures one at a time. -->
                                  Note: 
                                <br>
                                - upload images in .png .jpg .jpeg formats only;
                                <br>
                                - you may upload up to 5 pictures;
                                <br>
                               - use Landscapes and not Portraits (see pic here under):
                                <br>
                                <br>

                                <img src="http://work-related-learning.com/images/imageinfo.png" alt="imageinfo" width="250" height="140"> 
                                <br>
                                <br>

                                <div class="col-xs-12 col-sm-4 airfcfx-xs-center">
                                    <a href="javascript:void(0);" style="top: 10px;position: relative;color:#ff5a5f;text-decoration:none;cursor:pointer;"><i class="fa fa-camera"></i>Add Picture</a>

                                    <?php //if(in_array($_SERVER['REMOTE_ADDR'], array('109.115.16.120','93.36.12.90','93.36.22.19'))){?>

                                    <input type="file" name="XUploadForm[file][]" onchange="start_file_upload_custom('rentals||first_photos_paid');" multiple="true" id="uploadfile" accept=".png, .jpg, .jpeg" style="opacity:0;width:100%;height:35px;margin-top:-20px;cursor:pointer;">

                                <?php /*}else{ ?>
                                    <input type="file" name="XUploadForm[file][]" onchange="update_file_name();" multiple="true" id="uploadfile" accept=".png, .jpg, .jpeg" style="opacity:0;width:100%;height:35px;margin-top:-20px;cursor:pointer;">
                                <?php }*/ ?>
                                </div>
                                <input type="hidden" value="{{$propertyObj->id}}" id="listingid" name="listingid">
                                 <?php /*if(!in_array($_SERVER['REMOTE_ADDR'], array('109.115.16.120','93.36.12.90','93.36.22.19'))){?>
                                <div class="col-xs-12 col-sm-4 airfcfx-xs-center">
                                    <input type="button" class="btn btn-success" value="Start Upload" onclick="start_file_upload()" id="startuploadbtn">
                                    <img id="loadingimg" src="{{asset('frontendassets/images/load.gif')}}" class="loading" style="margin-top:0px;">							</div>
                                <?php } */?>
                                <div id="imagenames" class="clear col-sm-12"></div>
                                <div id="imagepreview" class="clear col-sm-12">
                                    <?php
                                    $str = "[";
                                    foreach ($propertyObj->propertyimages as $img) {
                                        $str .= '"' . $img->img_name . '",';
                                        ?>
                                        <div class="listimgdiv"><img src="{{asset('images/rentals/'.$img->property_id.'/'.$img->img_name)}}" style="width:70px;height:70px;">
                                            <i class="fa fa-remove listclose" style="cursor:pointer;" id="remove_$img->id3_get_frame_long_name(frameId)" onclick="remove_image(this, '{{$img->img_name}}','rentals||first_photos_paid')"></i>
                                        </div>


                                    <?php
                                    }

                                    $str1 = rtrim($str, ',');
                                    $str1 .= "]";
                                    ?>



                                </div>
                                <input type="hidden" value='{{$str1}}' name="uploadedfiles" id="uploadedfiles">

                                <div class="clear"></div>
                                <div class="photoerrcls" style="clear: both;"></div><br/>
                                 <?php /*if(!in_array($_SERVER['REMOTE_ADDR'], array('109.115.16.120','93.36.12.90','93.36.22.19'))){?>
                                <input type="button" value="Save" class="btn btn_email nextbtn" onclick="show_safety_paid();">
                            <?php }*/ ?>

                            </div>
                        </div>



                    </div>
                </div>
                <!--row end -->
            </div>
        </div>
        <!--Panel end -->





    </div>
    <!--col-sm-9 end -->

</div>
<!-- container end -->

<script>
function start_file_upload_custom(returnUrl = '')
{
    var inp = document.getElementById('uploadfile');
    uploadedfiles = $("#uploadedfiles").val();

    if (uploadedfiles != "")
    {
        uploaded = jQuery.parseJSON(uploadedfiles);
        uploadedlen = uploaded.length;
    } else
    {
        uploadedlen = 0;
    }
    imagesarr = [];
    var i = 0, len = inp.files.length, img, reader, file;

    // alert("Len :" + len);
    // return false;
    remainfiles = parseInt(5) - parseInt(uploadedlen);

    if (len > remainfiles)
    {
        $(".photoerrcls").show();
        $(".photoerrcls").html("You can add only 5 images");
        setTimeout(function () {
            $(".photoerrcls").slideUp();
            $('.photoerrcls').html('');
        }, 5000);
        return false;
    }


    if (len == 0) {
        $(".photoerrcls").show();
        $(".photoerrcls").html("Please Select Image");
        setTimeout(function () {
            $(".photoerrcls").slideUp();
            $('.photoerrcls').html('');
        }, 5000);
        return false;
    }
    formdata = new FormData();
    formdata.append('listingid', $("#listingid").val());
    formdata.append('redirect',returnUrl);
    for (; i < len; i++) {
        file = inp.files[i];

        if (!!file.type.match(/image.*/)) {
            if (window.FileReader) {
                reader = new FileReader();
                reader.onloadend = function (e) {
                    //showUploadedItem(e.target.result, file.fileName);
                };
                reader.readAsDataURL(file);
            }
            if (formdata) {
                formdata.append("images[]", file);
            }
        }
    }

// console.log("Form Data :" + formdata);
// return false;
    if (formdata) {
        $.ajax({
            url: baseurl + '/rentals/startfileupload', // point to server-side PHP script 
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#loadingimg").show();
                $("#startuploadbtn").attr("disabled", "true");
            },
            success: function (res) {
                                
                $("#imagenames").html("");
                $("#loadingimg").hide();
                if ($.trim(res) == "error") {
                    $(".photoerrcls").show();
                    $(".photoerrcls").html("File size is large");
                    setTimeout(function () {
                        $(".photoerrcls").slideUp();
                        $('.photoerrcls').html('');
                    }, 5000);
                    $("#startuploadbtn").removeAttr("disabled");
                    $("#uploadfile").val("");
                } else
                {
                    result = res.split("***");
                    // alert(result.length);
                    // console.log(result);
                    // return false;
                    var newfiles = '';
                    var appendFiles = '';
                    inputfiles = $("#uploadedfiles").val();
                    if (inputfiles == "")
                        $("#uploadedfiles").val(result[1]);
                    else
                    {

                        existfiles = $("#uploadedfiles").val();
                        if (existfiles == "[]")
                        {
                            $("#uploadedfiles").val('');
                            /*newfiles = result[1].replace('[',''); 
                             newfiles = result[1].replace(']','');*/
                            
                            for(var j=0;j< (result.length);j++){
                                // console.log(result[j]);
                                if(result[j].indexOf('[') != -1){
                                    var splitVar = result[j].split("]");
                                    // if(typeof(splitVar[1]) == 'undefined'){
                                        // newfiles += splitVar[0];
                                        appendFiles += splitVar[0].replace('[', '');
                                        appendFiles += ",";
                                        // appendFiles += splitVar[0].replace('[', '');
                                        if(typeof(splitVar[1]) != 'undefined'){
                                            newfiles += splitVar[1];
                                        }
                                    // }else{
                                    //     appendFiles += splitVar[0];
                                    //     newfiles += splitVar[1];
                                    // }
                                }else{
                                        newfiles += result[j];

                                }
                            }   
                            appendFiles = appendFiles.replace(/,\s*$/, "");                         
                            $("#uploadedfiles").val("[" + appendFiles + "]");
                        } else {

                            appendFiles = '';
                            for(var j=0;j< (result.length);j++){
                                // console.log(result[j]);
                                if(result[j].indexOf('[') != -1){
                                    var splitVar = result[j].split("]");
                                    // if(typeof(splitVar[1]) == 'undefined'){
                                        // newfiles += splitVar[0];
                                        appendFiles += splitVar[0].replace('[', '');
                                        appendFiles += ",";
                                        if(typeof(splitVar[1]) != 'undefined'){
                                            newfiles += splitVar[1];
                                        }
                                    // }else{
                                    //     appendFiles += splitVar[0];
                                    //     newfiles += splitVar[1];
                                    // }
                                }else{
                                    // var splitVar = result[j].split("]");
                                    // if(typeof(splitVar[1]) == 'undefined'){
                                        newfiles += result[j];
                                    // }else{
                                    //     newfiles +=  
                                    // }

                                    // newfiles = 
                                }

                                
                                // console.log(splitVar);
                                // alert("Split Var : " + splitVar[0] + " Split 1 var" + splitVar[1]);
                                // if(result[j].indexOf('[') != -1){
                                // // if(splitVar[1].indexOf('[') != -1){

                                //     alert("Before fprocess  " + result[j] +  " \n \t New Files var : " + newfiles);
                                //     newfiles += result[j].replace('[', '');
                                //     alert("after process  " + result[j] +  " \n \t New Files var : " + newfiles);
                                // }else if(result[j].indexOf(']') != -1){
                                //     alert("Before fprocess  " + result[j] +  " \n \t New Files var : " + newfiles);
                                //     newfiles += result[j].replace(']', '');
                                //     alert("after process  " + result[j] +  " \n \t New Files var : " + newfiles);
                                // }
                                // alert("NewFiles : "+ newfiles + " \n result of "+j+ " Is : "+ result[j]);
                            }

                            // console.log(newfiles);
                            // alert("New Files :" + newfiles);
                            // newfiles = result[1].replace('[', '');
                            existfiles = existfiles.replace(']', '');
                            appendFiles = appendFiles.replace(/,\s*$/, "");
                            // alert("New Files :" + newfiles + "\t \n  Exsting : "+ existfiles);
                            $("#uploadedfiles").val(existfiles + ',' + appendFiles + "]");
                        }

                    }
                    // $("#imagepreview").append(result[0]);
                    $("#imagepreview").append(newfiles);
                    $("#startuploadbtn").removeAttr("disabled");
                    $("#uploadfile").val("");
                }
            }
        });
    }
}
function remove_image(org, imgname,redirect='',page_name='rentals')
{
   if(confirm("Are you sure? By confirming you will remove this pic.")){
    
    // return false;
    $(org).hide();
    $(org).prev("img").hide();
    $(org).parent().remove();

    uploadedfiles = $("#uploadedfiles").val();
    filesarr = JSON.parse(uploadedfiles);
    filesarr = $.grep(filesarr, function (value) {
        return value != imgname;
    });


    // alert(filesarr.length);return false;
    if (filesarr.length >= 1) {
        files = JSON.stringify(filesarr);
        $("#uploadedfiles").val(files);
        // var uploadimg = $("#uploadedfiles").val();
            var listingid = $("#listingid").val();
            $.ajax({
                type: 'POST',
                url: baseurl + '/rentals/savephotolist',
                async: false,
                data: {
                    uploadimg: imgname,
                    listingid: listingid,
                    type:'remove',
                    page:page_name
                },
                success: function (data) {
                    alert('Photo removed Successfully.');
                    
                        if (data == 'admin') {
                            window.location = baseurl + '/admin/property/photosPaid/' + listingid;
                        } else {
                            if(redirect != ''){
                                redirect = redirect.replace("||","/");
                                // alert("Redirect : " + redirect);

                                window.location = baseurl + '/'+redirect+'/' + listingid;
                            }else{
                                window.location = baseurl + '/rentals/first_photos_paid/' + listingid;                            
                            }
                        }

                    /*$("#showPhoto").css('background','none');
                     $("#showHomesafe").css('background','#ddd');
                     $("#photosdiv").hide();
                     $("#safetydiv").show();
                     $(window).scrollTop(0);*/
                }
            });        
    } else {
        $("#uploadedfiles").val("");
    }
   }

}
</script>

@include('partials.spacefooter')
@stop