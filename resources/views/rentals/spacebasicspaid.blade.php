@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<div class="bg_gray1">

    <div class="container mrgnset">

        @include('partials.spacesidebarpaid')
        <!--col-sm-3 end -->
        <div class="col-xs-12 col-sm-9 sdecls" style="width: 60%;">
            
            @if(isset($propertyObj->propertytype->name) && !empty($propertyObj->propertytype->name))
                @if($propertyObj->propertytype->name == "Work")
                    @include('partials.rentals.basics_paid_work')
                @elseif($propertyObj->propertytype->name == "A Class")
                    @include('partials.rentals.basics_paid_class')
                @elseif($propertyObj->propertytype->name == "Activity")
                    @include('partials.rentals.basics_paid_activity')
                @elseif($propertyObj->propertytype->name == "Bed")
                    @include('partials.rentals.basics_paid_bed')
<!--                $propertyObj->propertytype->name == "Diet"
                    partials.rentals.basics_paid_diet-->
                @elseif($propertyObj->propertytype->name == "Transfer")
                    @include('partials.rentals.basics_paid_transfer')
                @elseif($propertyObj->propertytype->name == "Tour")
                    @include('partials.rentals.basics_paid_tour')
                @elseif($propertyObj->propertytype->name == "Wow")
                    @include('partials.rentals.basics_paid_wow')
                
                @else
                    @include('partials.rentals.basics_paid')
                @endif
            @else
                @include('partials.rentals.basics_paid')
            @endif
        </div>
        <!--Panel end -->
    </div>
    <!--col-sm-9 end -->

</div>
<!-- container end -->



@include('partials.spacefooter')
@stop