@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('settings', 'App\Settings')
@inject('currecy', 'App\Currency')
<?php
$settingsObj = $settings->settingsInfo();
$currencies = $currecy->getCurrency(true);
?>
<div class="bg_gray1">

    <div class="container mrgnset">
        @include('partials.spacesidebarpaid')
        <!--col-sm-3 end -->
        <div class="col-xs-12 col-sm-9 sdecls" style="width: 60%;">
            <div class="panel panel-default panelcls">
                <div class="panel-body">
                    <div class="row">

                        <div class="col-xs-12 margin_top10 margin_bottom20 commcls" id="pricediv">
                            <h3>Set a price for your Service</h3>
                            You can set a price to reflect the place, quality, and hospitality you will be providing.							<hr />
                            <br />
                            {!! Form::open(array('url' => 'rentals/savePricing', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                            <div class="col-sm-6 coldiv">
                                <h3>Base Price</h3><br />
                                <input type="hidden" value="{{$propertyObj->id}}" id="listingid" name="listingid" />
                                <input type="hidden" value="{{isset($pricing->id)?$pricing->id:''}}" id="pricing_id" name="pricing_id" />
                                <label>Currency</label>
                                <div class="form-group field-listing-currency">

                                    {!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'currency']) !!}

                                    <p class="help-block help-block-error"></p>
                                </div>
                                <label>Select Payment Type</label>
                                <div class="form-group field-listing-currency">
                                    <label>
                                        <input type="radio" name="payment_type" id="weekly" value="weekly" class="form-control" style="width: 3%; float: left;" @if(isset($pricing->payment_type) && $pricing->payment_type == "weekly") checked="" @endif/>
                                               <span style="float: left; margin-left: 24px; margin-top: 12px;">Weekly</span>
                                    </label>
                                    <label style="margin-left: 20px;">
                                        <input type="radio" name="payment_type" id="fixed" value="fixed" class="form-control" style="width: 3%; float: left;" @if(isset($pricing->payment_type) && $pricing->payment_type == "fixed") checked="" @endif/>
                                               <span style="float: left; margin-left: 24px; margin-top: 12px;">Fixed</span>
                                    </label>
                                    <p class="help-block help-block-error"></p>
                                </div>
                                <div id="show_weekly_payment" style="@if(isset($pricing->payment_type) && $pricing->payment_type == 'weekly') display: block; @else display: none; @endif">
                                    <label>Weekly Payment Amount</label>
                                    <div class="form-group field-listing-currency">
                                        <input type="number" maxlength="10" name="weekly_amount" value="{{isset($pricing->amount)?$pricing->amount:''}}" class="form-control margin_bottom10 smalltext" id="weekly_amount" />
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                </div>
                                <div id="show_fixed_payment" style="@if(isset($pricing->payment_type) && $pricing->payment_type == 'fixed') display: block; @else display: none; @endif">
                                    <label>Fixed Payment Amount</label>
                                    <div class="form-group field-listing-currency">
                                        <input type="number" maxlength="10" name="fix_amount" value="{{isset($pricing->amount)?$pricing->amount:''}}" class="form-control margin_bottom10 smalltext" id="fix_amount" />
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                </div>
                                <br />
                                <h3>Extra Costs</h3>
                                <div class="col-xs-12 form-group margin_top20">
                                    <label>Expected Extra Costs - Amount</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class=""></i></span>
                                        <input type="number" class="form-control"  id="expected_extra_costs_value" name="expected_extra_costs_value" value="{{isset($pricing->expected_extra_costs_value)?$pricing->expected_extra_costs_value:''}}"/>
                                    </div>
                                    <div id="expected_extra_costs_value_err" class="errcls"></div>
                                </div> 

                                <div class="col-xs-12 form-group margin_top20">
                                    <label>Expected Extra Costs - Description</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class=""></i></span>
                                        <textarea rows="10" class="form-control"  id="expected_extra_costs_description" placeholder="For Example: Entrance and Train tickets, also for person accompanying in case." name="expected_extra_costs_description">{{isset($pricing->expected_extra_costs_description)?$pricing->expected_extra_costs_description:''}}</textarea>
                                    </div>
                                    <div id="expected_extra_costs_description_err" class="errcls"></div>
                                </div>
                                <!--                                <label>Nightly Price</label>
                                                                <div>
                                                                        <div class="symbolcls">
                                                                            <span id="currencysymbol" class="symboltxt">zł</span>
                                                                            </div> 
                                                                    <input type="text" maxlength="10" name="price_night" value="{{$propertyObj->price_night}}" class="form-control margin_bottom10 smalltext" id="nightlyprice">
                                                                    <div class="nightlypriceerr" style="clear: both;"></div><br/>
                                                                </div>
                                                                <div>
                                                                    <label>Security Deposit</label><br />
                                                                    <input type="text" maxlength="10" name="securitydeposit" value="{{$propertyObj->secutity_deposit}}" class="form-control margin_bottom10 smalltext" id="securitydeposit">
                                                                    <div class="securityerrcls" style="clear: both;"></div><br/>
                                                                </div>-->
                                <input type="submit" value="Save Pricing" class="btn btn_email nextbtn" />
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!--row end -->
            </div>
        </div>
        <!--Panel end -->





    </div>
    <!--col-sm-9 end -->

</div>
<!-- container end -->




@include('partials.spacefooter')
@stop