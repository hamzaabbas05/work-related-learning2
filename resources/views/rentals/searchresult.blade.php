@inject('settings', 'App\Settings')
<?php $settingsObj = $settings->settingsInfo();
$logoname = $settingsObj->logo;
$profile_noimage = $settingsObj->no_image_profile;

 ?>
 
@include('partials.searchresult')

<input type="hidden" value="{{$searchPropertyObj->count()}}" class="total-count-value" />
 
<div class="border_bottom"></div>

<script async defer type="text/javascript">
  deleteMarkers();
  var markerPoints = new Array();
  var infoMarker = new Array();
          

          <?php foreach($searchPropertyObj as $property) { ?> 
        //infoMarker.push('');
            var locations = {lat: <?php echo $property->pro_lat?>, lng: <?php echo $property->pro_lon?>};
      addMarker(locations, '<a href= "{{url('view/rentals/'.$property->id)}}"><p>{{$property->exptitle}}</p><span></span><br></a>');  
      //markerPoints.push(locations);
        //pausecontent.push('');

          <?php } ?>
      showMarkers();
</script>
 