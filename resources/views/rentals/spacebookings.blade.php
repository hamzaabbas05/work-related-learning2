@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('settings', 'App\Settings')
@inject('currecy', 'App\Currency')
<?php 
 $settingsObj = $settings->settingsInfo();
$currencies = $currecy->getCurrency(true);
?>
<div class="bg_gray1">

	<div class="container mrgnset">
		@include('partials.spacesidebar')
		<!--col-sm-3 end -->
				<div class="col-xs-12 col-sm-9 sdecls" style="width: 60%;">

			@include('partials.rentals.bookings')
			</div>
			<!--Panel end -->





		</div>
		<!--col-sm-9 end -->

	</div>
	<!-- container end -->

 
@include('partials.spacefooter')
@stop

 