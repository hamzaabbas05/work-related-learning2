@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<div class="bg_gray1">
    <div class="container">
        <div class="text-center margin_top20">
            <h1>"Add Guest Beds" </h1>
            <p>Students will send request for a specific bed in one of your rooms. <br />
                If you haven't specified any Guest Room, please complete your 
                <a href="{{URL::to('user/paidServices')}}">School House profile</a> first.  
            </p>
            <p>Repeat this operation for each Bed you would like to offer Student Guests.</p>
        </div>
    </div>   <!-- container end -->
    <div class="container">
        <div class="row">
            <style type="text/css">
                .showTypeFields {
                    /*margin-left: 0px !important;*/
                }
                .showTypeFields .form-group{
                    margin-left: 0px !important;
                }
            </style>
            {!! Form::open(array('url' => 'rentals/saveSchoolHouse', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
            <div class="col-xs-12 col-sm-12 col-md-9 margin_top20 margin_bottom20" style="display: block;">
                <div class="listing-create">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="col-xs-12 airfcfx-createlist">
                            <label>
                                You are adding one bed.
                                <br />

                            </label><br/>
                            <?php
                            $i = 1;
                            $propertyTypeAdded = array();
                            $bed_id = 0;
                            foreach ($propertyTypes as $obj) {
                                //if ($i <= 3) {
                                array_push($propertyTypeAdded, $obj->id);
                                $bed_id = $obj->id;
                                ?> 
                                <button type="button" class="btn btn_list home activebtn" value="2" data-toggle="tooltip" data-placement="bottom" title="" onclick="showTypeFields('{{strtolower($obj->name)}}')">
                                    <i class="fa fa-building-o list_icon"></i> {{$obj->name}}
                                    <input type="hidden" value="{{$obj->id}}" id="<?php echo "home" . $obj->id ?>">
                                </button>
                                <?php
                                //} 
                                $i++;
                            }
                            ?>
                            <div id="homeerr" class="errcls"></div>
                            <input type="hidden" value="{{$bed_id}}" name="wcategory" id="wcategory" />
                        </div> <!-- col-sm-12 end -->

                        <div class="col-xs-12 form-group margin_top20 showTypeFields" id="bed_type" style="display: block;">
                            <div class="col-xs-12 form-group margin_top20">
                                <label>Guest Room</label>
                                <p style="font-size: 12px; margin-left: 10px;">
                                    Specify in which Guest Room this Bed is. Student Guests of same age (minors and adults) and same gender may share a Room but with a different Nationality from any Student Guest in the School House.
                                    <br />
                                    If you haven't added any Guest Room yet, add them in your profile <a href="{{URL::to('user/paidServices')}}">here</a> under "Property"
                                </p>



                                <div class="form-group">
                                    <select name="bed_guest_room_id" class="form-control" required>
                                        <option value="">Select Guest Room</option>
                                        @if(isset($userObject->user_guest_rooms) && sizeof($userObject->user_guest_rooms) > 0)
                                        @foreach($userObject->user_guest_rooms as $gr_key => $guest_room)
                                        <option value="{{$guest_room->id}}">{{$guest_room->room_name}} {{isset($guest_room->realRoomType->room_type)?"(".$guest_room->realRoomType->room_type.")":''}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 form-group margin_top20">
                                <label>Bed Type</label>
                                <div class="form-group">
                                    <select name="bed_type_id" class="form-control" required>
                                        <option value="">Select Bed Type</option>
                                        @if(isset($bed_types) && sizeof($bed_types) > 0)
                                        @foreach($bed_types as $bed_type)
                                        <option value="{{$bed_type->id}}">{{$bed_type->bed_type}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 form-group margin_top20">
                            <label>Text description of this Bed.</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class=""></i></span>
                                <input type="text" class="form-control"  id="workexp" name="workexp" required/>

                            </div>
                            <div id="workexperr" class="errcls"></div>
                        </div> 

                        <div class="col-xs-12 form-group margin_top20">
                            <label>Describe the service provided related to this Bed.</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class=""></i></span>
                                <textarea rows="10" class="form-control"  id="description" placeholder="Bed spread will be changed weekly." name="description" required></textarea>

                            </div>
                            <div id="descriptionerr" class="errcls"></div>
                        </div>

                        <div class="xs-padding-0 col-xs-12 margin_top20">
                            <input type="hidden" class="form-control"  id="property_status" name="property_status" value="paid"/>
                            <a href="{{URL::to('rentals/add')}}">
                                <button class="airfcfx-panel btn btn_email padding10" type="button">Back</button>
                            </a>
                            <button class="airfcfx-panel btn btn_email padding10" type="submit" onclick="return savelistPaid()">Continue</button>
                            <div id="emailverifyerr" class="errcls"></div>
                        </div>


                    </div> <!-- col-sm-9 end -->

                </div>
            </div>
            {!! Form::close() !!}
        </div> <!-- row end -->

    </div> <!-- container end -->


</div>


@stop