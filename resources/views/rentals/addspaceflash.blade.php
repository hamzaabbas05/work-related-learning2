@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('propertySettings', 'App\PropertySettings')
<?php 
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<div class="bg_gray1">
<div class="container">
    <div class="text-center margin_top20">
        <h1>LIST YOUR "WORK EXPERIENCE BY FLASH" OPPORTUNITY </h1>
        <p>Work-related-learning.org allows Organisations to list Unpaid Job Offers for foreign over 16 high school students. </p>
    </div>
</div>   <!-- container end -->
<div class="container">
    <div class="row">
        @include('partials.rentals.addspaceflash')

</div> <!-- container end -->

    
</div>

    <!-- script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=places"></script-->
    <script type="text/javascript">
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('citynew'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();

                document.getElementById("pro_lat").value = latitude;
                document.getElementById("pro_lon").value = longitude;

                for (var component in componentForm) {
                        var cstr = "l";
                        document.getElementById(cstr+component).value = '';
                        document.getElementById(cstr+component).disabled = false;
                    }
                    for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    var cstr = "l";
                        if (componentForm[addressType]) {
                            
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(cstr+addressType).value = val;
                        }
                    //if (componentForm[addressType]) {
                   /* if(addressType=="locality")
                        document.getElementById("city").value = place.address_components[i].long_name;
                    else if(addressType=="administrative_area_level_1")
                        document.getElementById("state").value = place.address_components[i].long_name;
                    else if(addressType=="country")
                        document.getElementById("country").value = place.address_components[i].long_name;*/
                      //var val = place.address_components[i].long_name;alert(val);
                     // document.getElementById(addressType).value = val;
                    //}
                  }                
                //document.getElementById("citynew").value = place.address_components[0].long_name;
                //document.getElementById("state").value = place.address_components[1].long_name;
                //document.getElementById("country").value = place.address_components[2].long_name;
                //alert(mesg);
            });
        });
    </script>
@stop