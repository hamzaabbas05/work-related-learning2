@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('settings', 'App\Settings')
<?php
$settingsObj = $settings->settingsInfo();
?>
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">    
        @include('partials.listing_sidebar')
        <div class="col-xs-12 col-sm-9 margin_top20">        

            <div class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel airfcfx-panel-padding panel-heading profile_menu1">
                    <h3 class="panel-title">Your Listings</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php if (sizeof($propertyObj) > 0) { ?>

                            <?php foreach ($propertyObj as $obj) { ?>
                                <div class="airfcfx-listing-row clear">

                                    <?php
                                    $imgPath = asset('images/noimage/' . $settingsObj->no_image_property);
                                    foreach ($obj->propertyimages()->take(1)->get() as $img) {
                                        $imgPath = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
                                    }
                                    ?>
                                    <div class="col-xs-12 col-sm-5 ">
                                        <a href="#"><div class="airfcfx-listing_img listing_img" style="background-image:url({{$imgPath}});"></div></a>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <a href="#" class="airfcfx-text_black text_black"><h4>{{$obj->exptitle}}</h4></a>
                                        @if($obj->property_status == "paid")
                                        <a class="margin_top_5 text-danger" href="{{URL::to('rentals/basic_paid/'.$obj->id)}}">Managing Listing and Calender</a>
                                        @else
                                        <a class="margin_top_5 text-danger" href="{{URL::to('rentals/basic/'.$obj->id)}}">Managing Listing and Calender</a>
                                        @endif
                                    </div>
                                    <div class="airfcfx-listing-btn-cnt col-xs-12 col-sm-6">
                                        @if($obj->property_status == "paid")
                                        <a href="{{URL::to('rentals/basic_paid/'.$obj->id)}}" class="airfcfx-edit-list-btn">
                                            <button class="btn btn_dashboard margin_top20 pull-right">Edit listing</button>
                                        </a>

                                        <!-- <a href="{{URL::to('rentals/delete/'.$obj->id)}}" class="airfcfx-edit-list-btn col-xs-12 col-sm-5"> -->
                                            <button onClick="remove_listing(<?php echo $obj->id ?>)" class="btn btn_dashboard margin_top20 pull-right">Delete listing</button>
                                        <!-- </a> -->
                                        @else
                                        <a href="{{URL::to('rentals/basic/'.$obj->id)}}" class="airfcfx-edit-list-btn">
                                            <button class="btn btn_dashboard margin_top20 pull-right">Edit listing</button>
                                        </a>
                                        <!-- <a href="{{URL::to('rentals/delete/'.$obj->id)}}" onClick="return confirm('Are you sure? You are about to delete this listing.')" class="airfcfx-edit-list-btn col-xs-12 col-sm-5"> -->
                                            <button onClick="remove_listing(<?php echo $obj->id ?>)" class="btn btn_dashboard margin_top20 pull-right">Delete listing</button>
                                        <!-- </a> -->
                                        @endif
                                        <!--<a href="{{URL::to('rentals/basic/'.$obj->id)}}" class="airfcfx-view-list-btn">
                                        <button class="btn btn_dashboard margin_top20 pull-right">View list</button>
                                        </a>--></div><br>
                                </div> 
                                <?php
                            }
                        }
                        ?>
                        <br><br><br>
                         <!--      <div align="center"><ul class="pagination"><li class="prev disabled"><span>«</span></li>
                }
        <li class="active"><a href="/user/listing/mylistings?page=1&amp;per-page=20" data-page="0">1</a></li>
        <li><a href="/user/listing/mylistings?page=2&amp;per-page=20" data-page="1">2</a></li>
        <li><a href="/user/listing/mylistings?page=3&amp;per-page=20" data-page="2">3</a></li>
        <li class="next"><a href="/user/listing/mylistings?page=2&amp;per-page=20" data-page="1">»</a></li></ul></div>            -->      </div> <!--row end -->

                </div>

            </div> <!--Panel end -->





        </div>
    </div> <!--container end -->
</div>


<script>
    $(document).ready(function () {
        $(".show_ph").click(function () {
            $(".add_phone").show();
            $(".show_ph").hide();
        });
        $(".add_cont").click(function () {
            $(".add_contact").toggle();
        });
        $(".add_ship").click(function () {
            $(".add_shipping").toggle();
        });

    });
    function remove_listing1(propertyid){
        
            if (confirm("Are you sure? You are about to delete this listing.")) {
                $.ajax({
                // url: baseurl + '/rentals/startfileupload', // point to server-side PHP script 
                // url: baseurl + '/user/paidService', // point to server-side PHP script 
                url: baseurl + '/rentals/delete/?id='+propertyid, // point to server-side PHP script 
                type: "POST",
                // data: {'id':propertyid},
                processData: false,
                contentType: false,
                beforeSend: function () {
                    // $("#loadingimg").show();
                    // $("#startuploadbtn").attr("disabled", "true");
                },
                success: function (res) {
                    // alert(res);
                    if(res == 'success'){
                        alert("Listing deleted successfully.");
                        // location.reload();
                        return true;
                    }else{
                        alert("getting error");
                        return false;
                    }
                    // alert("Response Get : " + res);
                    // $("#property_info_image_display").append(res);
                    // $('input[name=property_image_paid]').val('');
                    return false;
                }
            });
            }
    }
</script>  
@stop