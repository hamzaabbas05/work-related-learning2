@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<div class="bg_gray1">
    <div class="container">
        <div class="text-center margin_top20">
            <h1>SCHOOL HOUSE: Listing on search result's page</h1>
            <p>This is what students will see first, together with rating and distance to workplace on search result's page. </p>
            <p>After Admin approval you will be able to add the services you provide: Activities, Classes, Beds, Trips and so on. </p>
            <p>You will be able to change these details in: edit profile. </p>
        </div>
    </div>   <!-- container end -->
    <div class="container">
        {!! Form::open(array('url' => 'rentals/saveNewSchoolHouse', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-9 margin_top20 margin_bottom20">
                <div class="listing-create">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="col-xs-12 form-group margin_top20">
                            <label>School House: Listing Title</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class=""></i></span>
                                <input type="text" class="form-control"  id="workexp" 
                                       placeholder="What caracterises your School House. Eg. Experienced host now providing all the services required by international students." 
                                       name="workexp" required="" value="{{isset($userObj->paid_service->house_slogan)?(!empty($userObj->paid_service->house_slogan)?$userObj->paid_service->house_slogan:''):''}}"/>

                            </div>
                            <div id="workexperr" class="errcls"></div>
                        </div> <!-- col-sm-12 end -->
                        <div class="col-xs-12 form-group margin_top20">
                            <label>School House: Listing Description</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class=""></i></span>
                                <textarea rows="10" class="form-control"  id="description" 
                                          placeholder="5 lines to give a general overview. Eg. You will feel welcome and part of the family while receiving lessons by expreinced teacher. You will be staying in a semidetached house with garden and family of 4 and a lovely dog. Your room will be big and with en-suite shower room, free wi-fi and many other facilities. On a Sunday we go to church then for nature walks. Ask kindly and we will try to please you." 
                                          required="" name="description">{{isset($userObj->paid_service->house_desc)?(!empty($userObj->paid_service->house_desc)?$userObj->paid_service->house_desc:''):''}}</textarea>
                            </div>
                            <div id="descriptionerr" class="errcls"></div>
                        </div>
                        <div class="xs-padding-0 col-xs-12 margin_top20">
                            <input type="hidden" class="form-control"  id="property_status" name="property_status" value="paid"/>
                            <a href="{{URL::to('rentals/add')}}">
                                <button class="airfcfx-panel btn btn_email padding10" type="button">Back</button>
                            </a>
                            <button class="airfcfx-panel btn btn_email padding10" type="submit" >Continue</button>
                            <div id="emailverifyerr" class="errcls"></div>
                        </div>

                    </div> <!-- col-sm-9 end -->

                </div>
            </div>

        </div> <!-- container end -->
        {!! Form::close() !!}

    </div>

    <!-- script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=places"></script-->
    <script type="text/javascript">
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('citynew'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();

                document.getElementById("pro_lat").value = latitude;
                document.getElementById("pro_lon").value = longitude;

                for (var component in componentForm) {
                    var cstr = "l";
                    document.getElementById(cstr + component).value = '';
                    document.getElementById(cstr + component).disabled = false;
                }
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    var cstr = "l";
                    if (componentForm[addressType]) {

                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(cstr + addressType).value = val;
                    }
                    //if (componentForm[addressType]) {
                    /* if(addressType=="locality")
                     document.getElementById("city").value = place.address_components[i].long_name;
                     else if(addressType=="administrative_area_level_1")
                     document.getElementById("state").value = place.address_components[i].long_name;
                     else if(addressType=="country")
                     document.getElementById("country").value = place.address_components[i].long_name;*/
                    //var val = place.address_components[i].long_name;alert(val);
                    // document.getElementById(addressType).value = val;
                    //}
                }
                //document.getElementById("citynew").value = place.address_components[0].long_name;
                //document.getElementById("state").value = place.address_components[1].long_name;
                //document.getElementById("country").value = place.address_components[2].long_name;
                //alert(mesg);
            });
        });
    </script>
    @stop