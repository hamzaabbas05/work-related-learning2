@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<div class="bg_gray1">
    <div class="container">
        <div class="text-center margin_top20">
            <h1>"Paid Services" </h1>
            <p>Choose what type of service you are about to list </p>
        </div>
    </div>   <!-- container end -->
    <div class="container">
        <div class="row">
            @include('partials.rentals.add_school_house')

        </div> <!-- container end -->


    </div>

    
    @stop