@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
<div class="bg_gray1">

    <div class="container mrgnset">
        @include('partials.spacesidebar')
        <!--col-sm-3 end -->
        <div class="col-xs-12 col-sm-9 sdecls" style="width: 60%;">
            @include('partials.rentals.calendar')
        </div>
        <!--Panel end -->
    </div>
    <!--col-sm-9 end -->

</div>
<!-- container end -->
<script>

    var dateRange = [];
    var beforedate = [];
    var afterdate = [];

    $(function () {
        startdate = '';
        enddate = '';
        listid = <?php echo $propertyObj->id ?>;
        startdates = "";
        enddates = "";
        for (i = 0; i < startdates.length; i++)
        {
            fromdate = new Date(startdates[i] * 1000);
            fromdate.setDate(fromdate.getDate() - 1);
            beforedate.push($.datepicker.formatDate('mm/dd/yy', fromdate));
            todate = new Date(enddates[i] * 1000);
            todate.setDate(todate.getDate());
            afterdate.push($.datepicker.formatDate('mm/dd/yy', todate));
            for (var d = new Date((startdates[i] * 1000));
                    d < new Date(enddates[i] * 1000);
                    d.setDate(d.getDate() + 1)) {
                dateRange.push($.datepicker.formatDate('yy-mm-dd', d));
            }
        }//alert(beforedate);

        var sdate = new Date(startdate * 1000);
        var edate = new Date(enddate * 1000);
        var sedate = new Date(enddate * 1000);
        sedate = new Date(sedate.setDate(sedate.getDate() - 1));
        if (startdate == "") {
            sdate = "";
        }
        if (enddate == "") {
            edate = "";
            sedate = "";
        }
        if (sdate != "") {
            todaydate = new Date();
            if (sdate < todaydate) {
                minimumdate = new Date();
                endminimumdate = new Date();
                endminimumdate.setDate(endminimumdate.getDate() + 1);
            } else
            {
                minimumdate = new Date(sdate.setDate(sdate.getDate()));
                endminimumdate = new Date(sdate.setDate(sdate.getDate() + 1));
            }
        } else
        {
            minimumdate = new Date();
            endminimumdate = new Date();
            endminimumdate.setDate(endminimumdate.getDate() + 1);
        }
        var array1 = <?php echo $forbiddenCheckIn; ?>;
        var array2 = <?php echo $forbiddenCheckOut; ?>;



        $("#startdate").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            beforeShowDay: disableDates,
            minDate: minimumdate,
            dateFormat: 'yy-mm-dd',
            maxDate: sedate,
            beforeShowDay: function (date) {
                var string = $.datepicker.formatDate('yy-mm-dd', date);
                var check = array1.indexOf(string) == -1;
                if (typeof (check) != 'undefined')
                {
                    if (check)
                    {
                        return [true, '', ''];
                    } else
                    {
                        return [false, "bookedDate", "check"];
                    }
                }
            },
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#enddate").datepicker("option", "minDate", dt);
            }

        });

        $("#enddate").datepicker({
            beforeShowDay: disableDates,
            minDate: endminimumdate,
            maxDate: edate,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            numberOfMonths: 1,
            beforeShowDay: function (date) {
                var string = $.datepicker.formatDate('yy-mm-dd', date);
                var check = array2.indexOf(string) == -1;
                if (typeof (check) != 'undefined')
                {
                    if (check)
                    {
                        return [true, '', ''];
                    } else
                    {
                        return [false, "bookedDate", "check"];
                    }
                }
            },
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() - 1);
                $("#startdate").datepicker("option", "maxDate", dt);
            }
        });




    });

    var disableDates = function (dt) {
        var dateString = jQuery.datepicker.formatDate('yy-mm-dd', dt);
        return [dateRange.indexOf(dateString) == -1];
    }


</script>

@include('partials.spacefooter')
@stop