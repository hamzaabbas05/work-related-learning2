@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
<div class="bg_gray1">

	<div class="container mrgnset">
		 @include('partials.spacesidebar')
		<!--col-sm-3 end -->
				<div class="col-xs-12 col-sm-9 sdecls" style="width: 60%;">

			<div class="panel panel-default panelcls">
				<div class="panel-body">
					<div class="row">
					
					<div class="col-xs-12 margin_top10 margin_bottom20 commcls" id="safetydiv">
							<h3>Home Safety</h3>
							Though rare, emergencies happen. Help your guests be prepared. Learn more about Safety.							<hr />
							<br />
							<div class="col-sm-6 coldiv">
								<h3>Safety Checklist</h3>
								<input type="checkbox" name="safetycheck[]" id="safetycheck2" value="2"><label style="display:block" for="safetycheck2" class="airfcfx-search-checkbox-text">Skid Lid</label><br /><input type="checkbox" name="safetycheck[]" id="safetycheck3" value="3"><label style="display:block" for="safetycheck3" class="airfcfx-search-checkbox-text">Carbon Monoxide Detector</label><br /><input type="checkbox" name="safetycheck[]" id="safetycheck4" value="4"><label style="display:block" for="safetycheck4" class="airfcfx-search-checkbox-text">First Aid Kit</label><br /><input type="checkbox" name="safetycheck[]" id="safetycheck5" value="5"><label style="display:block" for="safetycheck5" class="airfcfx-search-checkbox-text">Safety Card</label><br /><input type="checkbox" name="safetycheck[]" id="safetycheck6" value="6"><label style="display:block" for="safetycheck6" class="airfcfx-search-checkbox-text">Fire Extinguisher</label><br /><input type="checkbox" name="safetycheck[]" id="safetycheck7" value="7"><label style="display:block" for="safetycheck7" class="airfcfx-search-checkbox-text">testsdsdsdsdsd</label><br /><input type="checkbox" name="safetycheck[]" id="safetycheck8" value="8"><label style="display:block" for="safetycheck8" class="airfcfx-search-checkbox-text">uytgfdsaz</label><br />								<hr />
								<h3>Safety Card</h3>
								<label>Where are safety features located?</label>
								<br />
								<label class="selectalign">Fire extinguisher</label>
								<div class="mediumlargediv">
								<div class="form-group field-listing-fireextinguisher">

<input type="text" id="fireextinguisher" class="form-control margin_bottom10 divmediumtext" name="Listing[fireextinguisher]" value="">

<p class="help-block help-block-error"></p>
</div>								</div>
								<label class="selectalign">Fire alarm</label>
								<div class="mediumlargediv">
								<div class="form-group field-listing-firealarm">

<input type="text" id="firealarm" class="form-control margin_bottom10 divmediumtext" name="Listing[firealarm]" value="">

<p class="help-block help-block-error"></p>
</div>								</div>
								<label class="selectalign">Gas shutoff valve</label>
								<div class="mediumlargediv">
								<div class="form-group field-listing-gasshutoffvalve">

<input type="text" id="gasshutoffvalve" class="form-control margin_bottom10 divmediumtext" name="Listing[gasshutoffvalve]" value="">

<p class="help-block help-block-error"></p>
</div>								</div>
								<label>Emergency exit instructions</label>
								<div class="form-group field-listing-emergencyexitinstruction">

<textarea id="emergencyexitinstruction" class="form-control margin_bottom10" name="Listing[emergencyexitinstruction]" rows="4"></textarea>

<p class="help-block help-block-error"></p>
</div>								<label>Emergency phone numbers</label><br />
								<input type="button" value="Edit" class="btn btn-default" data-toggle="modal" data-target="#myModal">
								<br />
								<div class="safeerrcls" style="clear: both;"></div><br/>
								<input type="button" value="Next" class="btn btn_email nextbtn" onclick="show_price();">
								<a href="javascript:void(0);" onclick="show_backphotos();" class="backbtn">Back</a>
							</div>
						</div>			
					
					
					
					</div>
					</div>
					<!--row end -->
				</div>
			</div>
			<!--Panel end -->





		</div>
		<!--col-sm-9 end -->

	</div>
	<!-- container end -->


@include('partials.spacefooter')
@stop