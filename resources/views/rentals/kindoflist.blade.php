@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('propertySettings', 'App\PropertySettings')
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<div class="bg_gray1">
    <div class="container">
        <div class="text-center margin_top20">
            <h1>Select Kind of Listing </h1>
            <p>Work-related-learning.org allows Organisations to list Unpaid Job Offers for foreign over 16 high school students and School Houses their paid services. </p>
        </div>
    </div>   <!-- container end -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 margin_top20 margin_bottom20">
                <div class="listing-create">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="xs-padding-0 col-xs-12 margin_top20" style="text-align: center;">
                            <a href="{{URL::to('rentals/add/unpaid-work-experience')}}">
                                <button class="airfcfx-panel btn btn_email padding10" type="button">Unpaid Work Experience</button>
                            </a>
                            <!--                            <a href="{{URL::to('rentals/add/unpaid-work-experience-flash')}}">
                                                            <button class="airfcfx-panel btn btn_email padding10" type="button">Unpaid Work Experience by FLASH</button>
                                                        </a>-->
                            @if(isset($userObj->user_paid_property->id))
                            <a href="{{URL::to('rentals/add/school-house')}}">
                                <button class="airfcfx-panel btn btn_email padding10" type="button">School House - Paid Service</button>
                            </a>
                            @elseif(isset($userObj->user_paid_property_pending->id))
                            <a href="{{URL::to('rentals/add/school-house')}}">
                                <button class="airfcfx-panel btn btn_email padding10" type="button">School House - Paid Service</button>
                            </a>
                            @else
                            <a href="{{URL::to('rentals/add/new-school-house')}}">
                                <button class="airfcfx-panel btn btn_email padding10" type="button">School House - First Listing</button>
                            </a>
                            @endif
                            <div id="emailverifyerr" class="errcls"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- container end -->
    </div>

    @stop