@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('country', 'App\Country')
<?php
$countries = $country->getall();
?>
<div class="bg_gray1">

    <div class="container mrgnset">
        @include('partials.spacesidebarpaid')
        <!--col-sm-3 end -->
        <div class="col-xs-12 col-sm-9 sdecls" style="width: 60%;">
            @include('partials.rentals.address_paid')
        </div>
        <!--Panel end -->
    </div>
    <!--col-sm-9 end -->

</div>
<!-- container end -->
@include('partials.spacefooter')
@stop