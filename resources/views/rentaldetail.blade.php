@extends('layouts.frontdefault')
@section('header_styles')
<link href="{{ asset('frontendassets/css/jslider.css')}}" rel="stylesheet">
@stop
@section('header_js')
<script src="{{ asset('frontendassets/js/jquery.slider.bundle.js')}}"></script>
<script src="{{ asset('frontendassets/js/jquery.slider.js')}}"></script>
@stop
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('settings', 'App\Settings')
@inject('propertySettings', 'App\PropertySettings')
<?php
$settingsObj = $settings->settingsInfo();
$profile_noimage = $settingsObj->no_image_profile;
?>
<?php
$PropertySettingsObj = $propertySettings->propertySettings();
?>
<div class="navbar-fixed-top header_top_invidual">
    <div class="container">

        <div class="row">
            <div class="padding-top10 col-xs-12 col-sm-8">
                <ul class="list-inline">
                    <li><a href="#about" class="text_white rm_text_deco1 text_focus_color highlightcls" id="aboutcls"> About this listing</a></li>
                    <li><a href="#photo" class="text_white rm_text_deco1 text_focus_color highlightcls" id="photocls">Photos </a></li>
                    <!--li><a href="#review" class="text_white rm_text_deco1"> Reviews</a></li-->
                    <li><a href="#host" class="text_white rm_text_deco1 text_focus_color highlightcls" id="hostcls">The Host</a></li>
                    <li><a href="#location" class="text_white rm_text_deco1 text_focus_color highlightcls" id="locationcls">Location</a></li>
                </ul>
            </div><!--col-sm-8 end-->

            <div class="col-xs-12 col-sm-4 padding-right0">

                <div class="bg_black header_adj">

                    <div class="row">

                        <div class="margin_topr10 col-xs-12 col-sm-12">
                            <h3 class="msargin_bottom0">{{isset($propertyObj->exptitle)?$propertyObj->exptitle:''}}</h3>
                        </div><!--col-sm-8 end is this current title_yes so NOT expetitle--> 
                        <button type="button" class="close mobileviewreq" onclick="closereqpopup();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       

                    </div><!--row end-->

                </div><!--header_adj end-->

            </div><!--col-sm-4 end-->

        </div><!--row end-->

    </div><!--container end-->
</div><!--row end-->
<?php
$imgURL = "";
if(isset($propertyObj->propertyimages) && sizeof($propertyObj->propertyimages) > 0) {
    foreach ($propertyObj->propertyimages as $img) {
        $imgURL = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
    }
} else {
    $imgURL = asset('images/no_prop_image.png');
}
?>


<div class="banner2" style='background-image:url(<?php echo $imgURL; ?>);'></div>  
<?php
$imgName = asset('images/noimage/' . $profile_noimage);
if (isset($propertyObj->user->profile_img_name) && $propertyObj->user->profile_img_name != "" && !is_null($propertyObj->user->profile_img_name)) {
    $imgName = asset('images/profile/' . $propertyObj->user->profile_img_name);
}
?>

<div class="bg_white border1 ">

    <div class="container tpp">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-2 text-center">
                <div class="profile margin_top30 center-block" style="background-image:url({{$imgName}});"> </div> 
                <!--<a href="" class="text_gray1 margin_top20 center-block">-->
                    <?php 
                    $ptitle = "";
                    if(!empty($propertyObj->title)) {
                        $ptitle = $propertyObj->title;
                    } elseif(isset($propertyObj->user->orgname)) {
                        $ptitle = $propertyObj->user->orgname;
                    }                        
                        ?>
                    
                <!--</a>-->
            </div><!--col-sm-2 end-->
            <input type="hidden" id="listingid" value="{{isset($propertyObj->id)?$propertyObj->id:''}}"><div class="col-xs-12 col-sm-5 col-md-6 margin_top30 margin_bottom20 listtxtalgn"><h3 class="font-size22"><b>{{$propertyObj->exptitle}}
                    </b></h3>
                <!--<p class="text_gray1">{{$propertyObj->legal_address}}</p>-->



            </div><!--col-sm-6 end-->

            <div class="col-xs-12 col-sm-4 col-md-4 pos_rel wid">

                <div class="pos_abs make_fix">

                    <div class="border1  bg_white">

                        <div class="bg_black padding10 margin_adj margin_adj_rental clearfix ">

                            <div class="col-xs-12 col-sm-12">
                                <h3 class="margin_bottom0">{{$propertyObj->exptitle}} </h3>
                            </div><!--col-sm-8 end--> 
                            <button type="button" class="close mobileviewreq" onclick="closereqpopup();" aria-label="Close"><span aria-hidden="true">&times;</span></button>


                        </div><!--row end-->

                        <?php
                        $checkinsearch = "";
                        $checkoutsearch = "";
                        if (\Session::has('checkin')) {
                            $checkinsearch = \Session::get('checkin');
                            \Session::forget('checkin');
                        }
                        if (\Session::has('checkout')) {
                            $checkoutsearch = \Session::get('checkout');
                            \Session::forget('checkout');
                        }
                        ?>

                        <div class="table1 margin_top10 margin_left5 margin_bottom10 padd_5_10 checkinalgn caltxt">

                            <div class="checkindivcls" style="width:50%">
                                <label class="desktop_lable">Start Date</label>

                                <label class="mobile_lable">In </label>
                                <input type="text" id="startdate"  value="{{$checkinsearch}}" readonly class="font-size12 form-control margin10 cal" placeholder="DD-MM-YYYY">
                            </div>
                            <div class="checkindivcls" style="width:50%">
                                <label class="desktop_lable">End Date </label>
                                <label class="mobile_lable">Out </label>
                                <input type="text" id="enddate" value="{{$checkoutsearch}}" readonly class="font-size12 form-control margin10 cal" placeholder="DD-MM-YYYY">
                            </div>

                        </div><!--table1 end-->
                        <?php $class = "hiddencls"; ?>
                        <?php
                        if ($checkinsearch != "" && $checkoutsearch != "") {
                            $class = "";
                        }
                        ?>
                        <div id="pricediv" class="{{$class}}">
                            <input type="hidden" id="commissionprice">

                            <div class="errcls centertxt" id="maxstayerr"></div>

                            <div class="padding10 margin_left5 margin_right5 margin_bottom10">
                                @if (Auth::guest())
                                <a href="{{url('signin')}}" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" >Login</a>
                                @else
                                <?php
                                //for unpaid listings we check if role student or parent added, which means they have clicked on save in complete edit profile for parents of under 18 and student over 18 pages in edit profile. 
                                 if (!Auth::user()->hasRole('Student') && !Auth::user()->hasRole('Parent')) { ?>
                                    <a href="{{url('user/edit')}}" id="requestbtn" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" >Complete your Profile to send request</a>
                                    <?php
                                } else {
                                    if ($propertyObj->booking_style == 1) {
                                        ?> 
                                        <button type="button" onclick="send_reserve_request({{$propertyObj->id}});" id="requestbtn" class="btn-pad btn btn-danger full_width"><b>Send request</b></button>
                                    <?php } else { ?> 
                                        <button type="button" onclick="instant_book({{$propertyObj->id}});" id="requestbtn" class="btn-pad btn btn-danger full_width"><b>Book Now</b></button>
                                        <?php
                                    }
                                }
                                ?>
                                @endif
                                <center><img id="paypalloadingimg" src="{{asset('frontendassets/images/load.gif')}}" class="loading" style="margin-top:5px;"></center><center><b>This may take a long time, please be patient</b></center>
                                <div class="errcls centertxt" id="emailverifyerr"></div>
                            </div>



                            <div class="payment-form hiddencls"></div>
                        </div><!--border1 end-->
                        <div class="bg_white  padd_lf_rg_15 border1 margin_top50">

                            @if (Auth::guest())
                            <a href="{{url('signin')}}" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" >Login

                            </a>
                            @else
                            <?php
                            $text = "Save To Wishlist";
                            if (in_array($propertyObj->id, $wishProperty)) {
                                $text = "Added in Wishlist";
                            }
                            ?>
                            <button type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20" data-toggle="modal" data-target="#myModal" onclick="show_list_popup(event,<?php echo $propertyObj->id; ?>);">
                                <i class="fa fa-heart-o"></i>{{$text}}
                            </button>
                            @endif






                            <div class="row  text-center margin_top20">
                                <!-- <div class="col-xs-12 col-sm-4   bg_white padding10 border_right">
                                <a href="#" class="text_black"><p> <i class="fa fa-envelope-o"></i> Email</p></a>
                                </div> 
                                <div class="col-xs-12 col-sm-4  bg_white padding10 border_right">
                                <a href="#" class="text_black"><p> <i class="fa fa-comment"></i>Messenger </p></a>
                                </div>
                                <div class="col-xs-12 col-sm-4  bg_white padding10">
                                <a href="#" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle text_black"><p> <i class="fa fa-ellipsis-h"></i>More</p></a>
                                
                                <ul class="dropdown-menu padding20">
                                                            <a href="#" class="text_black"><li class="">Facebook</li></a>
                                                            <a href="#" class="text_black"><li class=" margin_top5">Twitter</li></a>
                                                            <a href="#" class="text_black"><li class=" margin_top5">Embedded this Link</li></a>
                                                            <a href="#" class="text_black"><li class=" margin_top5">Google +</li></a>
                                                            <a href="#" class="text_black"><li class=" margin_top5">Pinterest</li></a>
                                                        </ul>
                                
                                                    
                                </div> -->
                            </div></div><!--bg_white end-->

                    </div><!--pos_end-->

                </div><!--col-sm-4 end-->



            </div><!--row end-->

        </div><!--container end-->
    </div><!--bg_white end-->
    <body class="bg_gray1">
        <div class="container">


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <span id="about" class="scrollhighlight"> </span>
                    <h3 class="margin_top30 "><b>About Work Experience </b></h3>
                    <div id="jRatedetail"></div>
                    <!-- <div class="border_bottom margin_top10">{{$propertyObj->user->aboutorg}}</div>
                    -->

                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <p class="margin_top20 headtxt text_gray1">Job Details</p>
                        </div><!--col-sm-4 end-->
                        <div class="col-xs-12 col-sm-8">
                            <p class="margin_top20">Work experience Category: <b class="font-size14">{{isset($propertyObj->propertytype->name)?$propertyObj->propertytype->name:""}}</b></p>
                            <p>Duration in working hours of the experience:<b class="font-size14">{{$propertyObj->work_hours}} </b></p>
                            <p>Hourly articulation (daily and weekly): <b class="font-size14"> {{$propertyObj->work_schedule}}</b></p>

                        </div><!--col-sm-4 end-->
                    </div><!--row end-->


                    <div class="border_bottom margin_top10"></div>
                    <div class="row" id="description">
                        <div class="col-xs-12 col-sm-4">
                            <p class="margin_top20 headtxt text_gray1">Job Description</p>
                        </div><!--col-sm-4 end-->

                        <div class="col-xs-12 col-sm-8">
                            <p class="margin_top20" style="word-wrap:break-word;">
                                {{$propertyObj->description}}
                            </p>
                        </div><!--col-sm-8 end-->

                    </div><!--row end-->

                    <div class="border_bottom margin_top10"></div>
                    <div class="row" id="description">
                        <div class="col-xs-12 col-sm-4">
                            <p class="margin_top20 headtxt text_gray1">Further optional information</p>
                        </div><!--col-sm-4 end-->

                        <div class="col-xs-12 col-sm-8">
                            <p class="margin_top20" style="word-wrap:break-word;">
                                {{$propertyObj->optional_description}}
                            </p>
                        </div><!--col-sm-8 end-->

                    </div><!--row end-->
                    <div class="border_bottom margin_top10"></div>
                    <div class="row" id="houserules">
                        <div class="col-xs-12 col-sm-4">
                            <p class="margin_top20 headtxt text_gray1">Workplace Rules</p>
                        </div><!--col-sm-4 end-->

                        <div class="col-xs-12 col-sm-8">
                            <p class="margin_top20" style="word-wrap:break-word;">
                                {{$propertyObj->houserules}}
                            </p>
                        </div><!--col-sm-8 end-->

                    </div>
                    <div class="border_bottom margin_top10"></div>
                    <div class="row" id="houserules">
                        <div class="col-xs-12 col-sm-4">
                            <p class="margin_top20 headtxt text_gray1">Link to extra necessary documentation:</p>
                        </div><!--col-sm-4 end-->

                        <div class="col-xs-12 col-sm-8">
                            <p class="margin_top20" style="word-wrap:break-word;">
                                {{$propertyObj->link_to_extra_docs_necessary}}
                            </p>
                        </div><!--col-sm-8 end-->

                    </div>
                    <div class="border_bottom margin_top10"></div>



                    <!--row end--><span class=" scrollhighlight" id="photo">



                        <link rel="stylesheet" href="{{ asset('frontendassets/css/supersized.css')}}" type="text/css" media="screen" />
                        <link rel="stylesheet" href="{{ asset('frontendassets/css/supersized.shutter.css')}}" type="text/css" media="screen" />
                        <script type="text/javascript" src="{{ asset('frontendassets/js/jquery.easing.min.js')}}"></script>
                        <script type="text/javascript" src="{{ asset('frontendassets/js/supersized.3.2.7.min.js')}}"></script>
                        <script type="text/javascript" src="{{ asset('frontendassets/js/supersized.shutter.min.js')}}"></script>




                </div><!--col-sm-8 end-->

            </div><!--row end-->

        </div><!--container end-->
    </body>

    <!-- Review Start -->
    <?php if ($propertyObj->reviews->count() > 0) { ?> 
        <div class="bg_white margin_top30 border1">
            }
            <div class="container">
                <h3 class="margin_top20">{{$propertyObj->reviews->count()}} Reviews <span id="review"></span></h3>


                <div class="row">
                    <div class="col-xs-12 col-sm-8">

                        <?php
                        foreach ($propertyObj->reviews as $key => $review) {
                            if($key > 4) {
                                break;
                            }

                            $rimgName = asset('images/noimage/' . $profile_noimage);
                            if ($review->reviewer->profile_img_name != "" && !is_null($review->reviewer->profile_img_name)) {
                                $rimgName = asset('images/profile/' . $review->reviewer->profile_img_name);
                            }
                            ?> 

                            <div class="row">
                                <div class="col-xs-12 col-sm-4 text-center padd_bottom30">
                                    <div class="profile margin_top20 center-block" style="background-image:url({{$rimgName}});"> </div>
                                    <a class="text_gray1 margin_top10 center-block" href="#">{{$review->reviewer->name}}</a>
                                </div> 

                                <div class="col-xs-12 col-sm-8">
                                    <p>{{$review->description}}</p>
                                    <div class="row margin_top20">
                                        <div class="col-xs-12 col-sm-6"><p class="text-muted margin_top10">{{$review->dateAdded}}</p></div>
                                         <!--<div class="col-xs-12 col-sm-6 text-right"><button type="button" class="btn btn-default"> <i class="fa fa-thumbs-o-up"></i> Helpful</button></div>-->
                                    </div> 
                                </div> 
                            </div> 
                        <?php } ?> 

                    </div> 
                </div> 


            </div> 
        </div>  <?php } else { ?>
        <!-- Review End -->

        <div class="bg_white margin_top30 border1">
            <div class="container">
                <h3 class="margin_top20 margin_bottom10"><b>No reviews yet</b></h3>
                <div class="margin_bottom20">Stay here and you could give this host their first review!</div>
            </div>
        </div>

    <?php } ?>


    <div class="container margin_top30 stopfixer">
        <h3><b>About the Organisation/Work Tutor : {{$propertyObj->user->aboutorg}} <span id="host" class="scrollhighlight"></span></b></h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12  col-md-8">


                <div class="row">

                    <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="profile margin_top20 center-block" style="background-image:url({{$imgName}});"> </div>
                    </div><!--col-sm-4 end-->

                    <div class="col-xs-12 col-sm-8 col-md-8 xs-center-txt">
                        <p class="margin_top40">{{$propertyObj->auto_complete}}<br />Member since {{date_format($propertyObj->user->created_at, 'jS F Y')}}</p>

                        <?php
                        $ln = "Private";
                        if ($propertyObj->legal_nature == 1) {
                            $ln = "Public";
                        }
                        ?>
                        <p class="margin_top20">Legal Nature: <b class="font-size14">{{$ln}}</b></p>
                        <p>Human Resources:<b class="font-size14"><?php
                                if (!is_null($propertyObj->roomtype)) {
                                    echo $propertyObj->roomtype->name;
                                }
                                ?> </b></p>
                        <p> Student's Work Tutor - Relationship in the Company: <b class="font-size14"> 

                                {{(!is_null($propertyObj->tutorrelation)) ? $propertyObj->tutorrelation->name : ""}}</b></p>


                    </div>

                    <!--col-sm-4 end-->

                </div><!--row end-->



                <div class="row hiddencls">

                    <div class="col-xs-12 col-sm-4">
                        <p class="margin_top30">Trust</p>
                    </div><!--col-sm-4 end-->

                    <div class="col-xs-12 col-sm-8">

                        <button type="button" class="btn btn-warning margin_top20"> 1</button>
                        <p>Review</p>
                    </div><!--col-sm-4 end-->

                </div><!--row end-->

            </div><!--col-sm-8 end--></div><!--row end-->


        <!--iframe src="https://www.google.com/maps/@11.0136259,76.8978671,14.25z?hl=en-US"  width="100%" 
        height="450" frameborder="0" style="border:0; " class="margin_top30 map"  ></iframe-->
        <span id="location" class="scrollhighlight"></span>
        <div class="map margin_bottom30">
            <span class="map1"></span>
            <!--span id="location"></span-->
            <div id="map_canvas" frameborder="0" style="border:0; " class="margin_top30 map">



            </div>



        </div>


        <h3 class="margin_top30 hiddencls">Explore other options in and around 17th arrondissement </h3>

        <p class="margin_bottom30 hiddencls">More places to stay in 17th arrondissement: Houses · Bed & Breakfasts · Lofts · Villas · Condominiums<br /><br />
            People also stay in Saint-Ouen · Levallois-Perret · Asnieres-sur-Seine · Neuilly-sur-Seine · Courbevoie
        </p>



    </div><!--container end-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog listpopupwidth" role="document">
            <div class="modal-content">

                <div class="modal-body padding0">
                    <div class="toplistdiv" style="display:none;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>   
                        <h3>Save to Wish List</h3>          
                        <hr />
                    </div>

                    <div class="airfcfx-leftlistdiv leftlistdiv">
                        <div class="banner2 banner2hgt"  id="listimage"></div>
                    </div>
                    <div class="airfcfx-rightlistdiv-cnt">
                        <div class="airfcfx-rightlistdiv rightlistdiv padding20 wishlisthgt">

                            <div class="airfcfx-topfullviewdiv topfullviewdiv">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
                                <h3>Save to Wish List</h3><hr />
                            </div>

                            <div class="airfcfx-wishlist-contianer" id="listnames"> 

                            </div>
                        </div>
                        <div class="airfcfx-wish-createlist-cnt">
                            <input type="text" id="newlistname1" class="airfcfx-listtxt listtxt" value="" placeholder="Create New List" maxlength="20">
                            <input type="button" value="create" class="airfcfx-createbtn btn btn-danger createbtn" onclick="create_new_list();">
                        </div> 
                        <div class="airfcfx-wishlist-btn-cnt">
                            <input type="button" value="Cancel" class="airfcfx-cancelsze btn btn_email cancelsze cancelbtn " data-dismiss="modal">
                            <input type="button" value="Save" data-dismiss="modal" class="airfcfx-savebtn btn btn-primary savebtn pull-right" onclick="save_lists();">
                            <div class="errcls listerr"></div>
                        </div>
                    </div> 

                </div>
                <div class="clear">

                </div>            
            </div>
        </div>
    </div>

    <div class="modal" id="contactform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no_border">
                    <button type="button" class="close" data-dismiss="modal" onclick="checkitclear();" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
                </div>
                <div class="modal-body">

                    <div class="col-xs-12 contact-txtbox-align">
                        <h3>Contact Host</h3>
                        <p>Write your message to Host:</p>
                    </div>
                    <div class="col-xs-12 contact-txtbox-align margin_top20">
                        <textarea id="contactmessage" style="resize:none" cols="50" rows="5" class="form-control" maxlength="250"></textarea>
                        <div id="charNum"></div>
                    </div>
                </div>
                <input type="hidden" id="userid" value="4">
                <input type="hidden" id="hostid" value="4">
                <input type="hidden" id="listingid" value="893">
                <div class="clear"></div><br />
                <div class="modal-footer">
                    <input type="button" id="send_msg" value="Send Message" class="btn btn-danger" onclick="send_contact_message();">
                    <img id="loadingimg" src="/images/load.gif" class="loading" style="margin-top:-1px;">        <div id="succmsg" class="successtxt hiddencls">Message Sent Successfully</div><br/>
                    <div class="msgerrcls"></div>

                </div>
            </div>
            <div class="clear">
            </div>            
        </div>
    </div>  





    <script type="text/javascript">
        function checkitclear(){
        //$('#contactmessage').val('');
        $('.msgerrcls').val('');
        $('.msgerrcls').hide();
        }

    </script>		  
    <!--script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script-->

    <script>


        /*  var mapOptions = {
         zoom: 10,
         mapTypeId: google.maps.MapTypeId.SATELLITE,
         scrollwheel: false      
         }
         map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions); it is fetching from property lat, 

         we had added houselat haouselong vriables for School Houses in user/paid/property table wait

         */
        var map;
        var geocoder;
        var centerChangedLast;
        var reverseGeocodedLast;
        var currentReverseGeocodeResponse;
        function initialize() {
        lat = <?php echo $propertyObj->pro_lat; ?>;
        long = <?php echo $propertyObj->pro_lon; ?>;
        var latlng = new google.maps.LatLng(lat, long);
        var myOptions = {
        zoom: 10,
                center: latlng,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        var myCity = new google.maps.Circle({
        center:latlng,
                radius:7000,
                strokeColor:"#8fe3de",
                strokeOpacity:0.8,
                strokeWeight:2,
                fillColor:"#8cdfd9",
                fillOpacity:0.4
        });
        myCity.setMap(map);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
        // google.maps.event.addDomListener(window, 'load', initialize);    

        //slideer stop
        $('.carousel').carousel({
        interval: false
        })


                //Nav Fixed
                $(document).ready(function() {
        var a = $(".wid").outerWidth();
        $(".make_fix").css('width', a - 15);
        var project2 = $('.tpp').offset();
        var $window = $(window);
        $window.scroll(function() {
        //start fix
        if ($window.scrollTop() >= project2.top) {
        $(".make_fix").addClass("fixed");
        jQuery('.header_top_invidual').addClass('filter_menu1');
        }
        else {
        $(".make_fix").removeClass("fixed");
        jQuery('.header_top_invidual').removeClass('filter_menu1');
        }

        });
        });
        //clouse fix
        $(document).ready(function() {
        var project1 = $('.stopfixer').offset();
        var $window = $(window);
        var valueofmap = project1.top - '100';
        $window.scroll(function() {
        if ($window.scrollTop() >= valueofmap) {
        // alert(project1.top);
        $(".make_fix").addClass("fixed_rm");
        }
        else {
        $(".make_fix").removeClass("fixed_rm");
        }
        });
        });
        //Owl Carasel


        $(document).ready(function() {

        //var owl = $("#owl-demo");

        /*owl.owlCarousel({
         items : 3, //10 items above 1000px browser width
         itemsDesktop : [3,5], //5 items between 1000px and 901px
         itemsDesktopSmall : [3,3], // betweem 900px and 601px
         itemsTablet: [3,2], //2 items between 600 and 0
         itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
         });*/

        // Custom Navigation Events
        $(".next").click(function(){
        owl.trigger('owl.next');
        })
                $(".prev").click(function(){
        owl.trigger('owl.prev');
        })

        });
        //width of filter

        $(document).ready(function(){

        $(window).scroll(function(){
        $("#startdate").datepicker("hide");
        $("#enddate").datepicker("hide");
        $("#startdate").blur();
        $("#enddate").blur();
        $("#startdatemobile").datepicker("hide");
        $("#enddatemobile").datepicker("hide");
        $("#startdatemobile").blur();
        $("#enddatemobile").blur();
        });
        $("#startdate").keydown(function(event){
        if (event.which == 13) {
        $("#startdate").readonlyDatepicker(true);
        }
        });
        $("#enddate").keydown(function(event){
        if (event.which == 13) {
        $("#enddate").readonlyDatepicker(true);
        }
        });
        $("#startdatemobile").keydown(function(event){
        if (event.which == 13) {
        $("#startdatemobile").readonlyDatepicker(true);
        }
        });
        $("#enddatemobile").keydown(function(event){
        if (event.which == 13) {
        $("#enddatemobile").readonlyDatepicker(true);
        }
        });
        });</script>



    <script>

        var dateRange = [];
        var beforedate = [];
        var afterdate = [];
        $(function () {
        startdate = '';
        enddate = '';
        listid = <?php echo $propertyObj->id ?>;
        startdates = "";
        enddates = "";
        for (i = 0; i < startdates.length; i++)
        {
        fromdate = new Date(startdates[i] * 1000);
        fromdate.setDate(fromdate.getDate() - 1);
        beforedate.push($.datepicker.formatDate('mm/dd/yy', fromdate));
        todate = new Date(enddates[i] * 1000);
        todate.setDate(todate.getDate());
        afterdate.push($.datepicker.formatDate('mm/dd/yy', todate));
        for (var d = new Date((startdates[i] * 1000));
        d < new Date(enddates[i] * 1000);
        d.setDate(d.getDate() + 1)) {
        dateRange.push($.datepicker.formatDate('yy-mm-dd', d));
        }
        }//alert(beforedate);

        var sdate = new Date(startdate * 1000);
        var edate = new Date(enddate * 1000);
        var sedate = new Date(enddate * 1000);
        sedate = new Date(sedate.setDate(sedate.getDate() - 1));
        if (startdate == "") {
        sdate = "";
        }
        if (enddate == "") {
        edate = "";
        sedate = "";
        }
        if (sdate != "") {
        todaydate = new Date();
        if (sdate < todaydate) {
        minimumdate = new Date();
        endminimumdate = new Date();
        endminimumdate.setDate(endminimumdate.getDate() + 1);
        }
        else
        {
        minimumdate = new Date(sdate.setDate(sdate.getDate()));
        endminimumdate = new Date(sdate.setDate(sdate.getDate() + 1));
        }
        }
        else
        {
        minimumdate = new Date();
        endminimumdate = new Date();
        endminimumdate.setDate(endminimumdate.getDate() + 1);
        }
        console.log('\n Checkin Dates : <?php echo $forbiddenCheckIn;?>');
        var array1 = <?php echo $forbiddenCheckIn; //do you remeber this as commented out_ checking for error if this is the error then I check for that else uncomment the code ?>;
        // console.log('\n Calender Availability : <?php //echo $propertyObj->calendar_availability;?>');
        console.log('\n Checkout Dates : <?php echo $forbiddenCheckOut;?>');
        var array2 = <?php echo $forbiddenCheckOut; ?>;
<?php if ($propertyObj->calendar_availability == 1) { ?>
            $("#startdate").datepicker({
            changeMonth: true,
                    numberOfMonths: 1,
                    beforeShowDay: disableDates,
                    minDate:minimumdate,
                    maxDate:sedate,
                    beforeShowDay: function(date){
                    <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){ ?>
                    var string = $.datepicker.formatDate('yy-mm-dd', date);
                    string = string + " 11:00:00";
                    console.log("\n selected Dates :" + string + "after date convert :" + date);
                    // console.log("\n Date Hours : " + date.getHours()+": Date Minutes "+date.getMinutes() + "\n ");

                <?php }else{ ?>
                    var string = $.datepicker.formatDate('yy-mm-dd', date);
                <?php }?>
                    var check = array1.indexOf(string) == - 1;
                    if (typeof (check) != 'undefined')
                    {
                    if (check)
                    {
                    return [true, '', ''];
                    }
                    else
                    {
                    return [false, "bookedDate", "check"];
                    }
                    }
                    },
                    onSelect: function (selectedDate) {
                    $("#requestbtn").attr("disabled", false);
                    var orginalDate = new Date(selectedDate);
                    orginalDate.setDate(orginalDate.getDate() + 1);
                    //var monthsAddedDate = new Date(new Date(orginalDate).setMonth(orginalDate.getMonth() + 3));
                    $("#enddate").datepicker("option", 'minDate', orginalDate);
                    $("#enddate").datepicker("option", 'maxDate', edate);
                    stdates = $("#startdate").val();
                    stdate = new Date(stdates);
                    eddates = $("#enddate").val();
                    eddate = new Date(eddates);
                    var seldateRange = [];
                    if (stdates != "" && eddates != "") {
                    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    selstartdate = stdate.getTime();
                    selenddate = eddate.getTime();
                    for (var dates = new Date(selstartdate);
                    dates < new Date(selenddate);
                    dates.setDate(dates.getDate() + 1)) {
                    <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){ ?>
                    seldateRange.push($.datepicker.formatDate('yy-mm-dd H:i:s', dates));
                    console.log("\n Formated Dates : " + $.datepicker.formatDate('yy-mm-dd HH:MM', dates) + " \t Dates :" + dates);
                <?php }else{ ?>
                    seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                <?php } ?>
                    }
                    <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){ ?>
                        console.log("selected range" + seldateRange);
                    <?php } ?>

                    var scnt = 0;
                    for (a = 0; a < seldateRange.length; a++)
                    {
                    if ($.inArray(seldateRange[a], dateRange) >= 0) {
                    scnt++;
                    }
                    }
                    if (scnt > 0) {

                    $("#requestbtn").attr("disabled", true);
                    $("#maxstayerr").html("Can not book those days");
                    }
                    else
                    {
                    scnt = 0;
                    $("#requestbtn").attr("disabled", true);
                    $("#maxstayerr").html("");
                    $.ajax({
                    url: baseurl + '/showpricedetail', // point to server-side PHP script 
                            type: "POST",
                            async: false,
                            data: {
                            listid : listid,
                                    noofdays : diffDays
                            },
                            success: function (res) {
                            $("#pricediv").show();
                            $("#requestbtn").attr("disabled", false);
                            }
                    });
                    }
                    }
                    },
                    onClose: function (selectedDate) {
                    $("#enddate").datepicker('show');
                    }
            });
            $("#enddate").datepicker({
            beforeShowDay: disableDates,
                    minDate:endminimumdate,
                    maxDate:edate,
                    changeMonth: true,
                    numberOfMonths: 1,
                    beforeShowDay: function(date){
                       <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){ ?>
                    var string = $.datepicker.formatDate('yy-mm-dd', date);
                    string = string + " 11:00:00";
                    console.log("\n selected End Dates :" + string + "after End date convert :" + date);
                    // console.log("\n Date Hours : " + date.getHours()+": Date Minutes "+date.getMinutes() + "\n ");
                    <?php } else{ ?> 
                    var string = $.datepicker.formatDate('yy-mm-dd', date);
                    <?php } ?>
                    var check = array2.indexOf(string) == - 1;
                    if (typeof (check) != 'undefined')
                    {
                    if (check)
                    {
                    return [true, '', ''];
                    }
                    else
                    {
                    return [false, "bookedDate", "check"];
                    }
                    }
                    },
                    onSelect: function (selectedDate) {
                    $("#requestbtn").attr("disabled", false);
                    console.log("\n selected END Date  : " + selectedDate);
                     <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){ ?>
                    // var string = $.datepicker.formatDate('yy-mm-dd', date);
                    selectedDate = selectedDate + " 10:00:00";
                    console.log("\n selected End Dates :" + selectedDate);
                    // console.log("\n Date Hours : " + date.getHours()+": Date Minutes "+date.getMinutes() + "\n ");
                    var orginalDate = new Date(selectedDate);
                    <?php } else{ ?> 
                    var orginalDate = new Date(selectedDate);
                    <?php } ?>
                    orginalDate.setDate(orginalDate.getDate() - 1);
                    //var monthsAddedDate = new Date(new Date(orginalDate).setMonth(orginalDate.getMonth() - 3));
                    //$("#startdate").datepicker("option", 'maxDate', orginalDate);
                    stdates = $("#startdate").val();
                    stdate = new Date(stdates);
                    eddates = $("#enddate").val();
                    eddate = new Date(eddates);
                    <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){ ?>
                        eddates = eddates+" 11:00:00";
                    <?php } ?>
                    console.log("\n End Date Selected : " + eddates);
                    var seldateRange = [];
                    if (stdates != "" && eddates != "") {
                    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    selstartdate = stdate.getTime();
                    selenddate = eddate.getTime();
                    console.log("\n Seleendddate : " + selenddate);

                    for (var dates = new Date(selstartdate);
                    dates < new Date(selenddate);
                    dates.setDate(dates.getDate() + 1)) {
                    <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){ ?>
                    seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates)+" 11:00:00");
                    <?php }else{ ?>
                    seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                    <?php } ?>

                    console.log("\n Dates Format : " + $.datepicker.formatDate('yy-mm-dd', dates)+" 11:00:00");
                    }
                    var ecnt = 0;
                     var isCheckInDateBooked = [];
                    var isCheckOutDateBooked = [];
                    for (b = 0; b < seldateRange.length; b++)
                    {
                        if($.inArray(seldateRange[b], array2) >= 0){
                            isCheckInDateBooked.push(seldateRange[b]);
                        }
                        if($.inArray(seldateRange[b], array1) >= 0){
                            isCheckOutDateBooked.push(seldateRange[b]);
                        }
<?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.33.39.83'){ ?>
    console.log("\n seldateRange : " + seldateRange[b] + " \t Date Range : " + seldateRange);
    console.log("\n if In Array or not : " + $.inArray(seldateRange[b], seldateRange));
<?php } ?>                        

                    if ($.inArray(seldateRange[b], seldateRange) >= 0) 
                    {
                    ecnt++;
                    }
                    }
                
                    if((isCheckInDateBooked.length > 0 || isCheckOutDateBooked.length > 0) && ecnt > 0){
                        $("#requestbtn").attr("disabled", true);
                        $("#pricediv").show();
                        $("#maxstayerr").html("Can not book those days");
                    }
                
                    /*if (ecnt > 0){

                    $("#requestbtn").attr("disabled", true);
                    $("#maxstayerr").html("Can not book those days");
                    }*/
                
                    else
                    {
                    ecnt = 0;
                    $("#requestbtn").attr("disabled", true);
                    $("#maxstayerr").html("");
                    $.ajax({
                    url: baseurl + '/showpricedetail', // point to server-side PHP script 
                            type: "POST",
                            async: false,
                            data: {
                            listid : listid,
                                    noofdays : diffDays
                            },
                            success: function (res) {
                            $("#pricediv").show();
                            $("#requestbtn").attr("disabled", false);
                            }
                    });
                    }
                    }
                    }
            });
<?php } ?>


        });
        var disableDates = function(dt) {
        var dateString = jQuery.datepicker.formatDate('yy-mm-dd', dt);
        return [dateRange.indexOf(dateString) == - 1];
        }

        function focus_checkin() {
        var visible = $("#startdate").datepicker("widget").is(":visible");
        $("#startdate").datepicker(visible ? "hide" : "show");
        //$("#startdate").datepicker('show');
        }
    </script>

    <style>
        #map_canvas {
            width: 100%;
            height: 450px;
        }
        .owl-item
        {
            float: left;
        }
    </style>


    <script src="{{ asset('frontendassets/js/jRate.js')}}"></script> 
    <script>

        $(document).ready(function(){

        $("#jRatedetail").jRate({
        rating: <?php echo $rating; ?>,
                readOnly: true
        });
        });
    </script>  
    @stop
