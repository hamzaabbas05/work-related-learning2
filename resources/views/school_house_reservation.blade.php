@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<style type="text/css">
    .tripdesign td {
        border-right: 1px solid #ddd !important;
    }

</style>
<div class="bg_gray1">
    <div class="container" style="width: 92%;">  

        @include('partials.listing_sidebar')
        <div class="col-xs-12 col-sm-10 margin_top20">        
            <div class="airfcfx-panel panel panel-default">
                <div class="airfcfx-panel-padding airfcfx-panel panel-heading profile_menu1">
                    <h3 class="panel-title">Your School House Reservations</h3>
                </div>
                @include('layouts.errorsuccess')
                <div class="panel-body">

                    <div class="row">
                        <div class="trips-cnt margin_top10 margin_bottom20 paddingleft table-responsive col-sm-12 col-lg-12">
                            <img  id="statuschange" src="{{asset('frontendassets/images/load.gif')}}" class="loading" style="display:none;margin-top:5px;margin-left:600px;"> 
                            <table class="table table_no_border tripdesign">
                                <thead>
                                    <tr class="review_tab">
                                        <th style="width: 6%;">Sno</th>
                                        <th style="width: 14%;">Dates</th>
                                        <th style="width: 30%;">Listing Details</th>
                                        <th style="width: 10%;">Booking No</th>
                                        <th style="width: 10%;">First Choice</th>
                                        <th style="width: 10%;">Approval Status</th>
                                        <th style="width: 10%;">Approve/Decline</th>
                                        <th style="width: 10%;">Message / See Profile</th>
                                    </tr>
                                </thead>
                                <tbody> 

                                    <?php
                                    $i = 1;
                                    foreach ($tripsObj as $trips) {
                                        $class = "successtxt";

                                        if ($trips->booking_status == 'Pending') {
                                            $class = "text-danger";
                                        } if ($trips->booking_status == 'Cancelled') {
                                            $class = "text-medium";
                                        }
                                        $class1 = "successtxt";
                                        if ($trips->approval == 'Pending') {
                                            $class1 = "text-danger";
                                        } if ($trips->approval == 'Decline') {
                                            $class1 = "text-medium";
                                        }
                                        if ($trips->approval == 'Unavailable') {
                                            $class1 = "text-medium";
                                        }

                                        $id = "reserve" . $trips->id;
                                        if (isset($trips->property->property_status) && $trips->property->property_status == "paid") {
                                            ?> 
                                            <tr id="{{$id}}">
                                                <td style="border-left: 1px solid #ddd !important;">{{$i}}</td>
                                                <td class="airfcfx-breakword">
                                                    <p class="airfcfx-td-dtnloc-trp" style="width: 100%">
                                                        {{date("F jS, Y", strtotime($trips->checkin))}} <br /> 
                                                        TO <br />
                                                        {{date("F jS, Y", strtotime($trips->checkout))}}
                                                    </p> 
                                                </td>

                                                <td class="airfcfx-breakword">
                                                    <div><span style="color:#330099">Title:</span>{{$trips->property->exptitle}}</div>
                                                    <div><span style="color:#330099">{{!empty($trips->property->represent_name)?"WorkTutor":"User Listing"}}:</span> {{!empty($trips->property->represent_name)?$trips->property->represent_name:$trips->property->user->name}}</div>
                                                    <?php $ssid = $trips->user->id; ?>
                                                    <div><span style="color:#330099">Student:</span><a href='{{url("viewuser/$ssid")}}'>
                                                            <?php echo (!is_null($trips->user->student_relationship)) ? $trips->user->sname16 : $trips->user->name; ?></a></div>
                                                </td>

                                                <td align="center">{{$trips->Bookingno}}</td>
                                                <td align="center">
                                                    <?php
                                                    $user_choices = App\RentalsEnquiry::checkFirstChoice($trips->user_id, $trips->id, "paid");
                                                    if ($user_choices) {
                                                        echo "First Choice";
                                                    } else {
                                                        echo "2nd or 3rd Choice";
                                                    }
                                                    ?>

                                                </td>

                                                <td class="airfcfx-min-width-80px">
                                                    <p class="{{$class1}}">
                                                        <b>
                                                            <?php
                                                            $newDateStart = $trips->checkin;
                                                            $newDateEnd = $trips->checkout;
                                                           // echo "Start Date:".$newDateStart."<br>";
                                                           // echo "End Date:".$newDateEnd."<br>";
                                                           // echo "Prpperty id ".$trips->prd_id;
                                                            $availableCheck = App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $trips->prd_id);
                                                           //echo "<pre>";print_r($availableCheck);die;
                                                            $DisableCalDates = App\Schedule::where('id', $trips->property->id)->get();
                                                            $found_disable_dates = App\Schedule::where('id', $trips->property->id)->count();
                                                            $disable_dates = array();
                                                            if($found_disable_dates > 0){
                                                               foreach (json_decode($DisableCalDates[0]->data) as $disable_date=>$schedule) {
                                                                    $exploaded_str = explode(" ", $disable_date);
                                                                    $disable_dates[] = $exploaded_str[0]; 
                                                                } 
                                                            }  
                                                           
                                                           $exploaded_start_date = explode(" ", $newDateStart);
                                                           $exploaded_end_date = explode(" ", $newDateEnd);

                                                            // if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){
                                                            //     echo "Prp Id :".$trips->prd_id."<br>";
                                                             //    echo "<pre>";
                                                             //   echo "Start Date : ".$newDateStart."<br> End Date : ".$newDateEnd."<br>";
                                                           // print_r($disable_dates);die;
                                                            //     echo "</pre>"; looking good ok, please add for unpaid too
                                                            // }
                                                            //echo " STATUS ".$trips->approval;
                                                            if($trips->booking_status == 'Pending' && $trips->approval == 'Pending'){
																//echo '<pre>';print_r($trips);die;
																if(isset($trips->property->property_type) and $trips->property->property_type == '26'){
																	$userAllreadyAccpeted = App\RentalsEnquiry::userAllreadyAccpetedForPaid($newDateStart, $newDateEnd, $trips->user_id); 
																	
																	if($userAllreadyAccpeted){
																		 echo "User already taken somewhere else.";
																	}else{
																		if(count($availableCheck) > 0){
																			echo "You Approved one Request with overlapping dates.";
																		}else{ 
																		   echo $trips->approval;
																		}
																	}
																}else{
																	if(count($availableCheck) > 0){
																		echo "You Approved one Request with overlapping dates.";
																	}else{ 
																	   echo $trips->approval;
																	}
																		
																}
                                                                
                                                               
                                                            }else{
                                                                //echo "JAMI".$trips->approval;
                                                                if ($trips->approval == "Accept") {
                                                                    echo "Accepted";
                                                                } else if($trips->approval == "Unavailable"){
                                                                          echo "You are not available these dates.";
                                                                }else {
                                                                    echo $trips->approval;
                                                                }
                                                            }
                                                            ?>
                                                        </b>
                                                    </p>
                                                </td>

                                                <td>
                                                    <?php
                                                    // if ($user_choices) {
                                                        $check_in_date = date("Y-m-d", strtotime($trips->checkin));
                                                        $current_date = date("Y-m-d");
                                                        if ($check_in_date >= $current_date) {
                                                            ?>
                                                            <?php if ($trips->approval == "Pending") { 
                                                                    if($trips->booking_status == 'Pending' && $trips->approval == 'Pending'){ 
                                                                $newDateStart = $trips->checkin;
                                                                $newDateEnd = $trips->checkout;
                                                                $availableCheck = App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $trips->prd_id);
                                                                if(isset($trips->property->property_type) and $trips->property->property_type == '26'){
																$userAllreadyAccpeted = App\RentalsEnquiry::userAllreadyAccpetedForPaid($newDateStart, $newDateEnd, $trips->user_id); 
                                                                if($userAllreadyAccpeted){
                                                                    echo "";
                                                                }else{


                                                                    if(count($availableCheck) > 0){
                                                                        echo "";
                                                                        // echo "You have already you Approved one Request with overlapping dates.";
                                                                    }else{
                                                            ?>

<!--                                                                <p class="{{$class1}}">
                                                                    We are temporarily not allowing to accept.

                                                                      / school houses rght_ yes, goOD until we have finished okay
                                                                   </p>-->
                                                                    {!! Form::model('', ['action' => 'RentalsController@updateSchoolHousebookingstatusDev']) !!}
                                                                    <button type="submit" class="btn btn-responsive btn-success" style="background-color: #5cb85c !important;">Accept</button>
                                                                    <input type="hidden" name="enquiryid" value="{{$trips->id}}" />
                                                                    <input type="hidden" name="status" value="Accept" />
                                                                    {!! Form::close() !!}

                                                                    {!! Form::model('', ['action' => 'RentalsController@updateSchoolHousebookingstatusDev']) !!}
                                                                    <button type="submit" class="btn btn-responsive btn-primary" style="margin-top: 10px;">Decline this student only</button>
                                                                    <input type="hidden" name="enquiryid" value="{{$trips->id}}" /> 
                                                                    <input type="hidden" name="status" value="Decline" />
                                                                    {!! Form::close() !!}
                                                                      {!! Form::model('', ['action' => 'RentalsController@disableThesedates']) !!}
                                                                    <button type="submit" class="btn btn-responsive btn-primary" style="margin-top: 10px;">Not available to take any student these dates</button>
                                                                    
                                                                    <input type="hidden" name="listingid" value="{{$trips->property->id}}" />
                                                                     <input type="hidden" name="enquiryid" value="{{$trips->id}}" />
                                                                    <input type="hidden" name="check_in" value="{{$trips->checkin}}" /> 
                                                                    <input type="hidden" name="check_out" value="{{$trips->checkout}}" />
                                                                    <input type="hidden" name="status" value="notavailable" />
                                                                    {!! Form::close() !!}
                                                              
                                                            <?php 
																	}
																}
																}else{
																	  if(count($availableCheck) > 0){
                                                                        echo "";
                                                                        // echo "You have already you Approved one Request with overlapping dates.";
                                                                    }else{
                                                            ?>

<!--                                                                <p class="{{$class1}}">
                                                                    We are temporarily not allowing to accept.

                                                                      / school houses rght_ yes, goOD until we have finished okay
                                                                   </p>-->
                                                                    {!! Form::model('', ['action' => 'RentalsController@updateSchoolHousebookingstatusDev']) !!}
                                                                    <button type="submit" class="btn btn-responsive btn-success" style="background-color: #5cb85c !important;">Accept</button>
                                                                    <input type="hidden" name="enquiryid" value="{{$trips->id}}" />
                                                                    <input type="hidden" name="status" value="Accept" />
                                                                    {!! Form::close() !!}

                                                                    {!! Form::model('', ['action' => 'RentalsController@updateSchoolHousebookingstatusDev']) !!}
                                                                    <button type="submit" class="btn btn-responsive btn-primary" style="margin-top: 10px;">Decline this student only</button>
                                                                    <input type="hidden" name="enquiryid" value="{{$trips->id}}" /> 
                                                                    <input type="hidden" name="status" value="Decline" />
                                                                    {!! Form::close() !!}
                                                                      {!! Form::model('', ['action' => 'RentalsController@disableThesedates']) !!}
                                                                    <button type="submit" class="btn btn-responsive btn-primary" style="margin-top: 10px;">Not available to take any student these dates</button>
                                                                    
                                                                    <input type="hidden" name="listingid" value="{{$trips->property->id}}" />
                                                                     <input type="hidden" name="enquiryid" value="{{$trips->id}}" />
                                                                    <input type="hidden" name="check_in" value="{{$trips->checkin}}" /> 
                                                                    <input type="hidden" name="check_out" value="{{$trips->checkout}}" />
                                                                    <input type="hidden" name="status" value="notavailable" />
                                                                    {!! Form::close() !!}
                                                              
                                                            <?php 
																	}
																}
                                                        } ?>
                                                            <?php } else { ?> 
                                                                <p class="{{$class1}}">
                                                                    <b>
                                                                        <?php
                                                                        if ($trips->approval == "Accept") {
                                                                            echo "Accepted";
                                                                        } else {
                                                                            echo $trips->approval . " -- ";
                                                                        }
                                                                        ?>
                                                                    </b>
                                                                </p>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <p class="{{$class1}}"><b>this is an old request</b></p>
                                                        <?php } ?>
                                                    <?php /*} else { ?>
                                                        <p class="{{$class1}}"><b>You may not approve 2nd or 3rd choice requests, if students is not accepted by his first choice host, admin will contact you to host him or her</b></p>
                                                    <?php }*/ ?>
                                                </td>
                                                <td>
                                                    <!--<p>We are temporarily not allowing to Message</p>-->
                                                    <a target="_blank" href='{{url("user/conversation/$trips->id")}}'>Send Message</a> 
                                                    | 
                                                    <a target="_blank" href='{{url("viewuser/$ssid")}}'>Student Profile</a> 
                                                </td>
                                            </tr>
                                            <tr id="{{$id}}" style="display: none;">
                                                <td>{{$i}}</td>
                                                <td class="airfcfx-breakword"><p class="airfcfx-td-dtnloc-trp">
                                                        {{date("F jS, Y", strtotime($trips->checkin))}} TO {{date("F jS, Y", strtotime($trips->checkout))}}</p> 
                                                </td>

                                                <td class="airfcfx-breakword">
                                                    <div><span style="color:#330099">Title:</span>{{$trips->property->exptitle}}</div>
                                                    <div><span style="color:#330099">{{!empty($trips->property->represent_name)?"WorkTutor":"User Listing"}}:</span> {{!empty($trips->property->represent_name)?$trips->property->represent_name:$trips->property->user->name}}</div>
                                                    <?php $ssid = $trips->user->id; ?>
                                                    <div><span style="color:#330099">Student:</span><a href='{{url("viewuser/$ssid")}}'>
                                                            <?php echo (!is_null($trips->user->student_relationship)) ? $trips->user->sname16 : $trips->user->name; ?></a></div>
                                                </td>

                                                <td align="center">{{$trips->Bookingno}}</td>

                                                <td class="airfcfx-min-width-80px">
                                                    <p class="{{$class1}}"><b>{{$trips->approval}}</b></p>
                                                </td>

                                                <td>
                                                    {!! Form::model('', ['action' => 'RentalsController@updateSchoolHousebookingstatusDev']) !!}
                                                    <?php if ($trips->approval == "Pending") { ?> 
                                                        <select name="status">
                                                            <option value="">Select Status</option>
                                                            <option <?php if ($trips->approval == "Accept") { ?> selected <?php } ?> value="Accept">Accept</option>
                                                            <option <?php if ($trips->approval == "Decline") { ?> selected <?php } ?>  value="Decline">Decline</option>
                                                        </select> 
                                                        <input type="hidden" name="enquiryid" value="{{$trips->id}}" />
                                                        <input type="submit" name="submit" value="Update" class="pull-right airfcfx-panel btn btn_email margin_bottom20" />
                                                    <?php } else { ?> 
                                                        <p class="{{$class1}}"><b>{{$trips->approval}}</b></p>
                                                    <?php } ?>

                                                    {!! Form::close() !!}

                                                </td>
                                                <td><a target="_blank" href='{{url("user/conversation/$trips->id")}}'>Send Message</a> </td>
                                            </tr>

                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>

                                </tbody></table></div>                          

                    </div> <!--row end -->

                </div>

            </div> <!--Panel end -->





        </div>


    </div>
</div>@stop