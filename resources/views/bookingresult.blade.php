@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@include('partials.user_bredcrum')
<div class="bg_gray1">
    <div class="container">    
        <div class="row">

            <div class="col-xs-12 margin_top20 margin_bottom20">
                <div class="col-sm-12">
                    <div class="panel panel-default margin_top30">


                        <div class="panel-body padding10">
                            <?php if (!\Session::has('bookingno')) { ?> 
                                <center class="payment-success">
                                    <p>Thanks for your application!</p>
                                    <p>
                                        <span>
                                            Organisation (for jobs) and School House (for paid services) may confirm or decline your request, 
                                            in the first case you will receive all the details and instructions via email, else we will contact 
                                            organisations according to your second and third choice (this may take weeks), for School Houses we 
                                            will send further invitation to select another.
<!--                                            Organisation may confirm or decline your application, in the first case you will receive all the documentation 
                                            via email and in the latter case an email notifying you. Expect messages, job interview, CV and other 
                                            documentation request (as references) so to reply promptly.-->
                                        </span>.
                                    </p>
                                </center>   
                                    <?php } ?>  				   
                        </div>  <!--Panel end -->       

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 text-right">
                                <label class="profile_label">Booking No</label> 
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label class="profile_label">{{$enquiryObj->Bookingno}}</label> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 text-right">
                                <label class="profile_label">Booking Date</label> 
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label class="profile_label">{{$enquiryObj->dateAdded}}</label> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 text-right">
                                <label class="profile_label">WorkExperience Title</label> 
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label class="profile_label">{{$propertyObj->exptitle}}</label> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 text-right">
                                <label class="profile_label">Start Date</label> 
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label class="profile_label">{{$enquiryObj->checkin}}</label> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 text-right">
                                <label class="profile_label">End Date</label> 
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label class="profile_label">{{$enquiryObj->checkout}}</label> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 text-right">
                                <label class="profile_label">Status</label> 
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label class="profile_label">{{$enquiryObj->booking_status}}</label> 
                            </div>
                        </div>
                        <?php if (\Session::has('bookingno')) {
                            \Session::forget('bookingno'); ?> 
                            {!! Form::open(array('route' => 'getCheckout', 'method' => 'post')) !!}
                            {!! Form::hidden('enquiryId', $enquiryObj->id) !!}
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 text-right">
                                    <label class="profile_label">Checkout</label> 
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <input type="submit" value="Pay With Paypal" class="btn btn-danger">
                                </div>
                            </div>
                            </form>
<?php } ?>


                        <div class="margin_bottom20"></div>
                    </div>  
                </div> 
            </div>
        </div>
    </div><!--container end -->
</div>

@stop