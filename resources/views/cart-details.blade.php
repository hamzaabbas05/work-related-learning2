@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')

<div class="bg_gray1">
    <div class="container">  
		<div class="col-xs-12 col-sm-12 margin_top20">    
			@include('layouts.errorsuccess')
			<div class="">
                <div class="">
                    <h3 class="panel-title">Your Services</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
						<?php

							$html = '';
							$total_price = 0;
							if(count($cartItems) > 0){
								$html .= '<form role="form" action="'.url().'/send-requests" method="post" >';
								$html .= "<table border=1 id='bucket_lists' width='100%' cellpadding='10'>";
								$html .= "<tr>";
								$html .= "<th>Property Name</th>";
								$html .= "<th>Start Date</th>";
								$html .= "<th>End Date</th>";
								$html .= "<th>Number Of Nights</th>";
								$html .= "<th>Per Night Price</th>";
								$html .= "<th>Total Price</th>";
								$html .= "</tr>";
								foreach ($cartItems as $key => $item) {
									$html .= "<tr>";
									// $html .= "<td>".$item['property_id']."</td>";
									$fetchProperty = \App\Property::where('id',$item['property_id'])->first();
									if($fetchProperty->property_type == 26){
										$bed_guest_room_id = $fetchProperty->bed_guest_room_id;
										$bed_type_id = $fetchProperty->bed_type_id;
										$guest_room = \App\UserPaidGuestRoom::where('id',$bed_guest_room_id)->first();
										$room_type_id = $guest_room->room_type_id;
										$room_type = \App\RealRoomTypes::where("is_active", "1")->where("id", $room_type_id)->first();
										
										$bed_type = \App\BedType::where(["is_active" => "1", "id" => $bed_type_id])->first();
										//print_r($bed_type);die;
										
										$room_name = $room_type->room_type.' Room ';
										$bed_name = $bed_type->bed_type.' Bed';
										$service_title = $room_name."(".$bed_name.")";
									}else{
										$service_title = $fetchProperty->exptitle;
									}
									
									$currency_id = $item->pricing->currency_id;
									$currency    = \App\Currency::where(["id" => $currency_id])->first();
									if(isset($currency->currency_symbol) and $currency->currency_symbol != ""){
										$currency_symbol = $currency->currency_symbol;
									}else{
										$currency_symbol = $currency->currency_code;
									}
									
									if($item->pricing->payment_type == 'weekly'){
										
										$number = ($item->pricing->amount / 7);
										$per_day_price = number_format((float)$number, 2, '.', '');
									}else{
										$number = $item->pricing->amount;
										$per_day_price = number_format((float)$number, 2, '.', '');
									}
									
									
									$start_date = strtotime($item['start_date']);
									$end_date = strtotime($item['end_date']);
									$diff = $end_date - $start_date; 
									$number_of_days =  abs(round($diff / 86400)); 
									
									$total_price += $number_of_days*$per_day_price;
									$html .= "<td>".$service_title."</td>";
									$html .= "<td>".date("d-m-Y",$start_date)."</td>";
									$html .= "<td>".date("d-m-Y",$end_date)."</td>";
									$html .= "<td>".$number_of_days."</td>";
									$html .= "<td>".$currency_symbol." ".$per_day_price."</td>";
									$html .= "<td>".$currency_symbol." ".$number_of_days*$per_day_price."</td>";
									$html .= "</tr>";
									$html .= '<input type="hidden" name="service_ids[]" value="'.$item['id'].'" />';
								}
								$html .='<tr><td colspan="5" style="text-align:right;">Total Price</td><td>'.$currency_symbol." ".$total_price.'</td></tr>';
								$html .= "</tbody></table>";
								$html .= "<div class='cart-footer'><a class='btn-pad btn btn-danger ' href='".url()."'>Continue Shoping </a> </td>
										<button class='btn-pad btn btn-danger ' type='submit' >Send Requests</button></div></form>";
							}else{
								echo "<table><tbody><tr><td colspan=4>No Product in you bucket.</td></tr></tbody></table>";
							}
							
							echo $html;

						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>@stop