@extends('layouts.frontdefault')
@section('header_styles')
<link href="{{ asset('frontendassets/css/jslider.css')}}" rel="stylesheet">
@stop
@section('header_js')
<script src="{{ asset('frontendassets/js/jquery.slider.bundle.js')}}"></script>
<script src="{{ asset('frontendassets/js/jquery.slider.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{Asset('fancybox/jquery.fancybox.min.css')}}">
@stop
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
@inject('settings', 'App\Settings')
@inject('propertySettings', 'App\PropertySettings')
<?php
global $checkin_disable_dates;
 $checkin_disable_dates = $forbiddenCheckIn;
global $checkout_disable_dates;
 $checkout_disable_dates = $forbiddenCheckOut; 
$settingsObj = $settings->settingsInfo();
$profile_noimage = $settingsObj->no_image_profile;
?>
<?php
$PropertySettingsObj = $propertySettings->propertySettings();

$property_id = isset($propertyObj->id) ? $propertyObj->id : "";
	

?>

<div class="navbar-fixed-top header_top_invidual">
    <div class="container">

        <div class="row">
            <div class="padding-top10 col-xs-12 col-sm-8">
                <ul class="list-inline">
					<li><a id="basicmenu" href="#school_house_info" class="btn btn-primary ">School House</a></li>
					<li><a id="familyphotomenu" href="#school_house_family_info" style="" class="btn btn-primary ">Family</a></li>
				    <?php //if(isset($userObj->user_guest_rooms) && sizeof($userObj->user_guest_rooms) > 0){ ?>
					<li><a id="familyphotomenu" href="#school_house_property_info" style="" class="btn btn-primary ">Property & Rooms</a></li>
					<?php
					/*                                <?php //} ?>
					<!--<a id="extramenu" href="#school_house_extra_info" style="" class="btn btn-primary ">Extra</a>--->
					<?php //if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){ 
						// echo "Count :".$isOtherPropertyAvailable."<br>";
						if($isOtherPropertyAvailable > 0){
							$bgColor = '#4d4d4d';
							$colorCss = '';
							$hrefLink = url('rentals/optionalpaidservicelist/'.$userObj->id);
						}else{
							$bgColor = '#dce0e0';
							$hrefLink = '#';
							$colorCss = 'color:#333 !important;';
						}


						?>
					<a id="optional_services" href="<?php echo $hrefLink;  ?>" style="" class="btn btn-primary ">Other Paid Services</a>
					<?php //} ?>*/
					?>
					<li><a id="included_services" href="#school_house_included_services" style="" class="btn btn-primary ">Services</a></li>
                                
                   
                </ul>
				<ul class="service_list" style="display:none">
					<li><a id="classes" href="#classes_section" style="" class="btn btn-primary ">Classes</a></li>
					<li><a id="activities" href="#activities_section" style="" class="btn btn-primary ">Activities</a></li>
					<li><a id="diet" href="#diet_section" style="" class="btn btn-primary ">Diet</a></li>
					<li><a id="transfer" href="#transfer_section" style="" class="btn btn-primary ">Transfer</a></li>
					<li><a id="wow" href="#wow_section" style="" class="btn btn-primary ">Wow</a></li>
                    
				</ul>
            </div><!--col-sm-8 end-->

            <div class="col-xs-12 col-sm-4 padding-right0">

                <div class="bg_black header_adj">

                    <div class="row">
						<?php
								$checkinsearch = "";
								$checkoutsearch = "";
								if (\Session::has('checkin')) {
									$checkinsearch = \Session::get('checkin');
									//\Session::forget('checkin');
								}
								if (\Session::has('checkout')) {
									$checkoutsearch = \Session::get('checkout');
									//\Session::forget('checkout');
								}
							?>
							{!! Form::open(array('url' => 'rentals/searchBeds', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                        
                            <div class="col-xs-5 col-sm-5" style="padding:0;">
								<input type="text" id="startdate" name="checkinDate" value="{{$checkinsearch}}" readonly class="font-size12 form-control margin10 cal startdate" placeholder="DD-MM-YYYY">
							</div>
                            <div class="col-xs-5 col-sm-5" style="padding:0;">
								<input type="text" id="enddate" name="checkoutDate" value="{{$checkoutsearch}}" readonly class="font-size12 form-control margin10 cal enddate" placeholder="DD-MM-YYYY">
							</div>
                            <div class="col-xs-2 col-sm-2" style="padding:0;">
                                <?php 
									if (! Auth::guest()) {
										if(isset(\Auth::user()->id)){
										$loged_in_user_id = \Auth::user()->id;
										}else{
											$loged_in_user_id  = 0;
										}
										$today = date("Y-m-d");
										$cart_data = \App\Cartlists::select('property_id')->
																where('user_id',$loged_in_user_id)->
																where(function($whr) use ($today){
																	$whr->where("start_date","<=",$today)->where("end_date",">=",$today)
																	->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
																})->get();
										
										//echo '<pre>';
										$cart_ids = array();
										foreach($cart_data as $cart_item){
											$cart_ids[] = $cart_item->property_id;
										}  
										//echo '<pre>';print_r($cart_ids);die;
										$cart_count = $cart_data->count();
										
								?>
										<div class="basket_mini">
											<div class="mini-basket-title">
												<!--<h4>Your Basket </h4>-->
												<img src="{{Asset('/images/basket-icon.png')}}" class="basket-icon" onclick="showbucket()"  />
												<small class="mini-basket-count"><?php  echo $cart_count; ?></small>
											</div>
											<div class="mini-basket-desc">
												<div id="loading_basket">Loading...</div>
											</div>
										
										</div>
								<?php
										
									}
								?>
                            </div><!--col-sm-8 end--> 
                           
                        </div><!--row end-->

                     
                        {!! Form::close() !!}
                    </div><!--row end-->


                </div><!--header_adj end-->

            </div><!--col-sm-4 end-->

        </div><!--row end-->

    </div><!--container end-->
</div><!--row end-->

<?php
if(isset(\Auth::user()->id)){
$loged_in_user_id = \Auth::user()->id;
}else{
	$loged_in_user_id  = 0;
}
$imgURL = "";

if (isset($propertyObj->propertyimages) && sizeof($propertyObj->propertyimages) > 0) {
    foreach ($propertyObj->propertyimages as $img) {
        $imgURL = asset('images/rentals/' . $img->property_id . '/' . $img->img_name);
    }
} else {
    $imgURL = asset('images/no_prop_image.png');
}

?>

<div class="banner2" style='background-image:url(<?php echo $imgURL; ?>);'></div>  
<?php
$imgName = asset('images/noimage/' . $profile_noimage);
if (isset($propertyObj->user->profile_img_name) && $propertyObj->user->profile_img_name != "" && !is_null($propertyObj->user->profile_img_name)) {
    $imgName = asset('images/profile/' . $propertyObj->user->profile_img_name);
}

?>

<div class="bg_white border1 ">

    <div class="container tpp">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-2 text-center">
                <div class="profile margin_top30 center-block" style="background-image:url({{$imgName}});"> </div> 
                <!--<a href="" class="text_gray1 margin_top20 center-block">-->
                <?php
                $ptitle = "";
                if (isset($propertyObj->title) && !empty($propertyObj->title)) {
                    $ptitle = $propertyObj->title;
                } elseif (isset($propertyObj->user->orgname)) {
                    $ptitle = $propertyObj->user->orgname;
                }
                ?>

                <!--</a>-->
            </div><!--col-sm-2 end-->
            <input type="hidden" id="listingid" name="listingid" value="{{$property_id}}">
            <div class="col-xs-12 col-sm-5 col-md-6 margin_top30 margin_bottom20 listtxtalgn">
                <h3 class="font-size22">
                    <b>{{isset($propertyObj->exptitle)?$propertyObj->exptitle:''}}</b>
                </h3>
            </div><!--col-sm-6 end-->

            <div class="col-xs-12 col-sm-4 col-md-4 pos_rel wid">

                <div class="pos_abs make_fix">

                    <div class="border1  bg_white">

                        <div class="bg_black padding10 margin_adj clearfix ">

                            <div class="col-xs-12 col-sm-12">
                                <h3 class="margin_bottom0">
                                    {{isset($propertyObj->exptitle)?$propertyObj->exptitle:''}} 
                                    @if (isset($propertyObj->property_status) && $propertyObj->property_status == "paid")
                                    <!--                                    <br />
                                                                        <span>{{isset($propertyObj->pricing->amount)?"EUR ".$propertyObj->pricing->amount:'0'}}</span>-->
                                    @endif
                                </h3>
                            </div><!--col-sm-8 end--> 
                            <button type="button" class="close mobileviewreq" onclick="closereqpopup();" aria-label="Close"><span aria-hidden="true">&times;</span></button>


                        </div><!--row end-->

                        <?php
                        $checkinsearch = "";
                        $checkoutsearch = "";
                        if (\Session::has('checkin')) {
                            $checkinsearch = \Session::get('checkin');
                            //\Session::forget('checkin');
                        }
                        if (\Session::has('checkout')) {
                            $checkoutsearch = \Session::get('checkout');
                            //\Session::forget('checkout');
                        }
                        ?>
                        {!! Form::open(array('url' => 'rentals/searchBeds', 'class' => 'form-wizard form-horizontal', 'method' => 'POST', 'files'=>true)) !!}
                        <div class="table1 margin_top10 margin_left5  padd_5_10 checkinalgn caltxt">

                            <div class="checkindivcls" style="width:50%">
                               <label class="desktop_lable">Start Date</label>

                                <label class="mobile_lable">In </label>
                                <input type="text" id="startdate" name="checkinDate" value="{{$checkinsearch}}" readonly class="font-size12 form-control margin10 cal startdate" placeholder="DD-MM-YYYY">
                            </div>
                            <div class="checkindivcls" style="width:50%">
                                <label class="desktop_lable">End Date </label>
                                <label class="mobile_lable">Out</label>
                                <input type="text" id="enddate" name="checkoutDate" value="{{$checkoutsearch}}" readonly class="font-size12 form-control margin10 cal enddate" placeholder="DD-MM-YYYY">
                            </div>

                        </div><!--table1 end-->
                        <div class="padding10 margin_left5 margin_right5 margin_bottom10 ">
                            <button type="submit" id="requestbtn" class="btn-pad btn btn-danger full_width"><b>Refresh Dates </b></button>
                        </div>
						<div class="padding10 margin_left5 margin_right5 margin_bottom10 ">
							<?php 
								if (! Auth::guest()) {
									if(isset(\Auth::user()->id)){
									$loged_in_user_id = \Auth::user()->id;
									}else{
										$loged_in_user_id  = 0;
									}
									$today = date("Y-m-d");
									$cart_data = \App\Cartlists::select('property_id')->
															where('user_id',$loged_in_user_id)->
															where(function($whr) use ($today){
																$whr->where("start_date","<=",$today)->where("end_date",">=",$today)
																->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
															})->get();
									
									//echo '<pre>';
									$cart_ids = array();
									foreach($cart_data as $cart_item){
										$cart_ids[] = $cart_item->property_id;
									}  
									//echo '<pre>';print_r($cart_ids);die;
									$cart_count = $cart_data->count();
									
							?>
									<div class="basket_mini">
										<div class="mini-basket-title">
											<!--<h4>Your Basket </h4>-->
											<img src="{{Asset('/images/basket-icon.png')}}" class="basket-icon" onclick="showbucket()"  />
											<small class="mini-basket-count"><?php  echo $cart_count; ?></small>
										</div>
										<div class="mini-basket-desc">
											<div id="loading_basket">Loading...</div>
										</div>
									
									</div>
							<?php
									
								}
							?>
							
                        </div>
                        {!! Form::close() !!}
                        <?php $class = "hiddencls"; ?>
                        <?php
                        if ($checkinsearch != "" && $checkoutsearch != "") {
                            $class = "";
                        }
                        ?>


                        <div id="pricediv" class="{{$class}}">
                            <input type="hidden" id="commissionprice">

                            <div class="errcls centertxt" id="maxstayerr"></div>

                            <div class="padding10 margin_left5 margin_right5 margin_bottom10">

                            </div>



                            <div class="payment-form hiddencls"></div>
                        </div><!--border1 end-->
                        <!--bg_white end-->

                    </div><!--pos_end-->

                </div><!--col-sm-4 end-->



            </div><!--row end-->

        </div><!--container end-->
    </div><!--bg_white end-->
    <div class="bg_gray1">
        <div class="container">


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 ">
                    <span id="about" class="scrollhighlight"> </span>
                    <h3 class="margin_top30 "><b>About Service</b></h3>
                    <div id="jRatedetail"></div>


                    <div class="row">
                        <style type="text/css">
                            .menumargin{
                                margin-right:4px;
                            }
							/*.col-sm-4 p {
								font-weight: 600;
								color: #fe5771;
							}*/
                        </style>
						<?php 
										
						$first_listing_title = isset($propertyObj->exptitle)?$propertyObj->exptitle:'';
						$first_listing_despc = isset($propertyObj->description)?$propertyObj->description:'';
						
						?>
                        <div class="col-xs-12 col-sm-12 margin_top20">
                            <div class="btn-group" id="school_house_nav">
                                <a id="basicmenu" href="#school_house_info" class="btn btn-primary menumargin">School House</a>
                                <a id="familyphotomenu" href="#school_house_family_info" style="background-color: #4d4d4d !important;" class="btn btn-primary menumargin">Family</a>
                               <?php //if(isset($userObj->user_guest_rooms) && sizeof($userObj->user_guest_rooms) > 0){ ?>
								<a id="familyphotomenu" href="#guest_rooms" style="background-color: #4d4d4d !important;" class="btn btn-primary menumargin">Property & Rooms</a>
								<?php
								/*                                <?php //} ?>
								<!--<a id="extramenu" href="#school_house_extra_info" style="background-color: #4d4d4d !important;" class="btn btn-primary menumargin">Extra</a>--->
                                <?php //if($_SERVER['REMOTE_ADDR'] == '93.32.72.112'){ 
                                    // echo "Count :".$isOtherPropertyAvailable."<br>";
                                    if($isOtherPropertyAvailable > 0){
                                        $bgColor = '#4d4d4d';
                                        $colorCss = '';
                                        $hrefLink = url('rentals/optionalpaidservicelist/'.$userObj->id);
                                    }else{
                                        $bgColor = '#dce0e0';
                                        $hrefLink = '#';
                                        $colorCss = 'color:#333 !important;';
                                    }

       
                                    ?>
                                <a id="optional_services" href="<?php echo $hrefLink;  ?>" style="background-color: #4d4d4d !important;" class="btn btn-primary menumargin">Other Paid Services</a>
                                <?php //} ?>*/
								?>
								 <a id="included_services" href="#school_house_included_services" style="background-color: #4d4d4d !important;" class="btn btn-primary menumargin">Services</a>
                                
								
                            </div>
                        </div>
						<?php 
						
						if($loged_in_user_id == $propertyObj->user->id){ ?>
						<div id="search_result_design" class="col-md-12">
                            <h3 class="margin_top30 sec_head"><b>What Student Guests see among search results</b></h3>
                            <div >
								<div class="col-xs-12 col-sm-12  detailsContainer" style="padding:0;" data-proptitle="{{isset($propertyObj->exptitle)?$propertyObj->exptitle:''}}" data-propid="{{isset($propertyObj->id)?$propertyObj->id:''}}" data-itemtype="work" data-lat="{{isset($propertyObj->exptitle)?$propertyObj->exptitle:''}}" data-lng="{{isset($propertyObj->exptitle)?$propertyObj->exptitle:''}}">
									<div id="carousel-example-generic3" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner" role="listbox" onmouseover="showme(3)" onmouseout="hideme(3)">
											<a href="" target="_blank" title="0-2019-03-18-09-38-32-1552901912.jpg" class="item bg_img active" style="height:250px;width:100%;background-image:url('<?php echo $imgURL; ?>');">
											</a>
										</div> 
										<!-- Controls -->

										<a href="<?php  echo url('view/school_house_dev/'.$propertyObj->user->id); ?>" target="_blank" title="">
											<div class="skills_icon" style="background-image:url('http://work-related-learning.com/images/noimage/profile.png');"></div>
										</a>
										<div class="user_ratings">
											<h5>7.2</h5>
										</div>
										<div class="booked_services_summary">
												<li>
													<img src="{{asset('frontendassets/images/booked-beds/icon-bed--World.png')}}" />
												</li>
												<li>
													<img src="{{asset('frontendassets/images/booked-beds/icon-bedPortugal.png')}}" />
												</li>
												<li>
													<img src="{{asset('frontendassets/images/booked-beds/icon-bed-Russia.png')}}" />
												</li>
												<li>
													<img src="{{asset('frontendassets/images/booked-beds/icon-bed-Russia.png')}}" />
												</li>
												<li>
													<img src="{{asset('frontendassets/images/booked-beds/icon-bed-uk.png')}}" />
												</li>
												<li>
													<img src="{{asset('frontendassets/images/booked-beds/icon-bed-uk.png')}}" />
												</li>
												<li>
													<img src="{{asset('frontendassets/images/booked-beds/icon-bed-italynew.png')}}" />
													
												</li>
											</div> 
										
										<button id="edit_first_listing" ><img src="{{asset('frontendassets/images/edit.png')}}" /></button>
										<ul class="home_type_wit_family_members">
											<?php $room_type = $userObj->paid_service->home_type; ?>
											<li><?php echo $room_type; ?></li>
											<?php 
											foreach($userObj->paidServiceFamilyRelation as $f_relation){
												$member_name = $f_relation->name;
												$birth_year = $f_relation->birth_year;
												$relationship = $f_relation->relationship;
												echo '<li>'.$member_name.'</li>';
											}
											
											?>
											
										</ul>
										<div class="commute_to">
											<h4>22'</h4>
											<p>Commute to work</p>
										</div>
										
										<!-- Wish List -->
									</div>
									<!--carousel-example-generic end-->
									
									<!-- Property Name -->
									<a href="<?php  echo url('view/school_house_dev/'.$propertyObj->user->id); ?>" target="_blank" title="{{$first_listing_title}}">
										<p class="airfcfx-search-listing-name margin_top10 margin_left10 fa-1x text_gray1">{{$first_listing_title}}</p>
									</a>

									<a href="<?php  echo url('view/school_house_dev/'.$propertyObj->user->id); ?>" target="_blank" title="{{$first_listing_title}}">
										<p class="small margin_left10 text_gray1">
											<b>{{$first_listing_despc}}</b>
										</p>
									</a>
								</div>
								
							</div> 
						</div>
						<?php } ?>
                        <div id="school_house_info" class="col-md-12">
                            <h3 class="margin_top30 sec_head"><b>School House</b></h3>
                            <div >
                                <div class="margin_top10">
                                    <h3 class="sub_sec_head">School House - Listing Information</h3>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4 ">
                                        <p class=" headtxt text_gray1 ">School House Name</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{ isset($userObj->paid_service->house_name)?$userObj->paid_service->house_name:''}}
                                        </p>

                                    </div>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">School House Slogan</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{ isset($userObj->paid_service->house_slogan)?$userObj->paid_service->house_slogan:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div><!--row end-->
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">School House Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            @if(isset($userObj->paid_service->house_desc) && !empty($userObj->paid_service->house_desc))
                                            {{ isset($userObj->paid_service->house_desc)?$userObj->paid_service->house_desc:''}}
                                            @else
                                            {{ isset($userObj->user_paid_property->exptitle)?$userObj->user_paid_property->exptitle:''}}
                                            @endif
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div><!--row end-->

                                <div class="margin_top10">
                                    <h3  class="sub_sec_head">Hosting and Hospitality</h3>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Description as a Host</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{ isset($userObj->paid_service->house_host_desc)?$userObj->paid_service->house_host_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div><!--row end-->
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Number of years hosting experience</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{ isset($userObj->paid_service->host_experience)?$userObj->paid_service->host_experience:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div><!--row end-->
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Will Host</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">

                                            {{isset($userObj->paid_service->host_preference)?str_replace(",", ", ", $userObj->paid_service->host_preference):''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div><!--row end-->
                                <div class="margin_top10"></div>
                                <div class="">
                                    <h3 class="sub_sec_head">Teaching and Conversation skills</h3>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Teacher has following skills</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->teaching_skills)?str_replace(",", ", ", $userObj->paid_service->teaching_skills):''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                            </div> 
                        </div>
						
						<div id="school_house_family_info" class="col-md-12">
                            <h3 class="margin_top30 sec_head "><b>Family</b></h3>
                            <div>
                                <div class="margin_top10">
                                    <h3  class="sub_sec_head">Family Images</h3>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-12">
                                        <?php

                                        if (isset($userObj->paidServiceFamilyImage) && sizeof($userObj->paidServiceFamilyImage) > 0) {
                                            foreach ($userObj->paidServiceFamilyImage as $obj) {
                                                $imgname = $obj->img_name;
                                                $imgURL = asset('images/family/' . $obj->user_id . '/' . $imgname);
                                                ?> 
                                                <div class="col-md-3">
                                                    <a href="{{$imgURL}}" data-fancybox="images">
                                                        <img style="max-height:100px" src="{{$imgURL}}" />
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                        }
                                         
                                        ?>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="border_bottom margin_top10"></div>
                                <div class="margin_top10">
                                    <h3 class="sub_sec_head">Family Members</h3>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Language at Home the Country's official</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            @if(isset($userObj->paid_service->is_language) && $userObj->paid_service->is_language == "1")
                                            Yes
                                            @else
                                            No
                                            @endif
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">All Members General Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->member_desc)?$userObj->paid_service->member_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Family situation at Home</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->family_situation)?$userObj->paid_service->family_situation:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @if(isset($userObj->paidServiceFamilyRelation) && sizeof($userObj->paidServiceFamilyRelation) > 0)
                                <div class="border_bottom margin_top10"></div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Family Members/Children (staying at home)</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        @foreach($userObj->paidServiceFamilyRelation as $f_relation)
                                        <p class="" style="word-wrap:break-word;">
                                            <label style="float: left; margin-right: 10px;">Name : </label>
                                            <span style="float: left;">{{isset($f_relation->name)?$f_relation->name:''}}</span>
                                        </p>
                                        <p style="clear: both;"></p>
                                        <p class="" style="word-wrap:break-word;">
                                            <label style="float: left; margin-right: 10px;">Birth Year : </label>
                                            <span style="float: left;">{{isset($f_relation->birth_year)?$f_relation->birth_year:''}}</span>
                                        </p>
                                        <p style="clear: both;"></p>
                                        <p class="" style="word-wrap:break-word;">
                                            <label style="float: left; margin-right: 10px;">Relation : </label>
                                            <span style="float: left;">{{isset($f_relation->relationship)?$f_relation->relationship:''}}</span>
                                        </p>
                                        <p style="clear: both;"></p>
                                        <hr style="border: 1px solid #ddd;"/>
                                        @endforeach
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class=" margin_top10"></div>
                                @else
                                <div class=" margin_top10"></div>
                                @endif

                                <div class="margin_top10">
                                    <h3 class="sub_sec_head">Pets</h3>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Pets at Home</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            @if(isset($userObj->paid_service->pet_types) && !empty($userObj->paid_service->pet_types))
                                            {{isset($userObj->paid_service->pet_types)?str_replace(",", ", ", $userObj->paid_service->pet_types):''}}
                                            @else <p>No Pets have been specified</p>
                                        @endif
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @if(isset($userObj->paid_service->pet_types) && !empty($userObj->paid_service->pet_types))
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Pets Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->pet_desc)?$userObj->paid_service->pet_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif


                            </div>
                        </div>

                        <div id="school_house_property_info" class="col-md-12">
                            <h3 class="margin_top30 sec_head "><b>Property</b></h3>
                            <div>
                                <div class="margin_top10">
                                    <h3 class="sub_sec_head">Property Images</h3>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-12">
                                        <?php
										
                                        if (isset($userObj->paidServicePropertyImage) && sizeof($userObj->paidServicePropertyImage) > 0) {
                                            foreach ($userObj->paidServicePropertyImage as $obj) {
                                                $imgname = $obj->img_name;
                                                $imgURL = asset('images/schoolhouse/' . $obj->user_id . '/' . $imgname);
                                                ?> 
                                                <div class="col-md-3">
                                                    <a href="{{$imgURL}}" data-fancybox="images">
                                                        <img style="max-height:100px" src="{{$imgURL}}" />
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                        }


                                        ?>
                                    </div><!--col-sm-8 end-->
                                </div>
                                
                                <div class="margin_top10"></div>
                                <div class="margin_top10">
                                    <h3 class="sub_sec_head">Basic Information</h3>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Home Type</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->home_type)?$userObj->paid_service->home_type:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->property_desc)?$userObj->paid_service->property_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="margin_top10"></div>
                                <div class="margin_top10">
                                    <h3 class="sub_sec_head">Facilities Included</h3>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Facilities</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->facilities)?str_replace(",", ", ", $userObj->paid_service->facilities):''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->facility_desc)?$userObj->paid_service->facility_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                <div class=" margin_top10"></div>
                                <div class="margin_top10">
                                    <h3 class="sub_sec_head">House Rules</h3>
                                </div>
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Smoking</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            @if(isset($userObj->paid_service->smoking) && $userObj->paid_service->smoking == "1")
                                            Yes
                                            @else
                                            No
                                            @endif
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>

                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Time Limits (Curfew, meal and other time limits)</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            @if(isset($userObj->paid_service->time_limits) && !empty($userObj->paid_service->time_limits))
                                            {{isset($userObj->paid_service->time_limits)?$userObj->paid_service->time_limits:''}}
                                            @else
                                        <p>No Limits have been specified</p>
                                        @endif
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                
                                
								
                                <div id="guest_rooms" class=" margin_top10"></div>
								<!--- Start Add new Room modal--->
								<div class="modal fade " id="add_room_modal" tabindex="-1" role="dialog" aria-labelledby="add_room_modal" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="add_room_modal">Add New Room </h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form id="dropzone-form" action="" method="POST" enctype="multipart/form-data">
													<div class="row">
														<div class="col-md-4 col-xs-12">
															<div class="form-group">

																<label for="edit_bed_type" class="col-form-label">Select Room Type:</label>
																<select name="bed_type_id" class="form-control add_new_room_type_id" required>
																	<option value="">Select Room Type</option>
																	@if(isset($real_room_type) && sizeof($real_room_type) > 0) @foreach($real_room_type as $room_type)
																	<option value="{{$room_type->id}}" <?php if (isset($guest_room->room_type_id) && $guest_room->room_type_id == $room_type->id) { ?> selected
																		<?php } ?>>{{$room_type->room_type}}</option>
																	@endforeach @endif
																</select>
															</div>

														</div>
														<div class="col-md-4 col-xs-12">
															<div class="form-group">

																<label for="edit_bed_type" class="col-form-label">Toilet</label>
																<select name="bed_type_id" class="form-control add_new_room_toilet" required>
																	<option value="">Toilet</option>
																	<option value="En suite - private for the room Guest(s)">En suite - private for the room Guest(s)</option>
																	<option value="Shared with other Guest(s) but outside the Room">Shared with other Guest(s) but outside the Room</option>
																	<option value="Shared with the rest of the family">Shared with the rest of the family</option>
																</select>
															</div>

														</div>
														<div class="col-md-4 col-xs-12">
															<div class="form-group">

																<label for="edit_bed_type" class="col-form-label">Shower</label>
																<select name="bed_type_id" class="form-control add_new_room_shower" required>
																	<option value="">Shower</option>
																	<option value="En suite - private for the room Guest(s)">En suite - private for the room Guest(s)</option>
																	<option value="Shared with other Guest(s) but outside the Room">Shared with other Guest(s) but outside the Room</option>
																	<option value="Shared with the rest of the family">Shared with the rest of the family</option>
																</select>
															</div>

														</div>
													</div>
													<div class="row">
														<div class="col-md-2 colx-xs-12">
															<h5>Facilities</h5>
														</div>
														<div class="col-md-10 colx-xs-12">
															<div class="form-group field-profile-emergencyname room_facility">

																<ul class="unstyled centered"> 
								<?php
																	$facility_array = array();

																	foreach ($room_facilities as $room_facility) {
																		$facility_id = $room_facility->id;
																		if($facility_id == 1){
																			$facility_icon = 'TV.png';
																			$facility_grey_icon = 'TV-grey.png';
																		}else if($facility_id == 2){
																			$facility_icon = 'Airconditioning.png';
																			$facility_grey_icon = 'Airconditioning-grey.png';
																		}else if($facility_id == 3){
																			$facility_icon = 'Hair-drier.png';
																			$facility_grey_icon = 'Hair-drier-grey.png';
																		}else if($facility_id == 4){
																			$facility_icon = 'safe.png';
																			$facility_grey_icon = 'safe-grey.png';
																		}else if($facility_id == 5){
																			$facility_icon = 'Computer.png';
																			$facility_grey_icon = 'Computer-grey.png';
																		}else if($facility_id == 6){
																			$facility_icon = 'Wardrobespace.png';
																			$facility_grey_icon = 'Wardrobespace-grey.png';
																		}else if($facility_id == 7){
																			$facility_icon = 'Manualheating.png';
																			$facility_grey_icon = 'Manualheating-grey.png';
																		}else if($facility_id == 8){
																			$facility_icon = 'independent.png';
																			$facility_grey_icon = 'independent-grey.png';
																		}else if($facility_id == 9){
																			$facility_icon = 'Towels.png';
																			$facility_grey_icon = 'Towels-grey.png';
																		}else if($facility_id == 10){
																			$facility_icon = 'Iron.png';
																			$facility_grey_icon = 'Iron-grey.png';
																		}
								?>
																		<li>
																			<input class="styled-checkbox" name="room_facilities[]" id="styled-checkbox-{{$room_facility->id}}" type="checkbox" value="{{$room_facility->id}}">
																			<label for="styled-checkbox-{{$room_facility->id}}">
																			<img class="grey-icon" src="<?php echo Asset('/frontendassets/images/guest-room-facilities/'.$facility_grey_icon); ?>" alt="{{$room_facility->facility_type}}" />
																			<img class="orange-icon" style="display:none" src="<?php echo Asset('/frontendassets/images/guest-room-facilities/'.$facility_icon); ?>" alt="{{$room_facility->facility_type}}" />
																			</label>
																		</li>

								<?php 								} 
								?>
																</ul>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-2 colx-xs-12">
															<h5>Description</h5>
														</div>
														<div class="col-md-10 colx-xs-12">
															<div class="form-group field-profile-emergencyname room_facility">
																<input type="text" class="form-control" id="new_add_room_description" />
															</div>
														</div>
													</div>
													<div class="row"> 
														<div class="col-md-12">
															<div id="dropzone" class="dropzone"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="add_new_room_section">
																<h3>Add Bed To This Room</h3>
																<button class="btn btn-danger add_room_bed_btn">Add New Bed</button> 
															</div> 
															<div class="room_beds_for_update"> 
														   
															</div>
															
														</div>
													</div>   

												</form>
											</div>
											<div class="modal-footer">
												<span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 70%;">Updating...</span>
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary" id="add_new_room_modal_btn">Add Room</button>
											</div>
										</div>
									</div>
								</div>

								<!--- end Add new room modal--->
								<!--- Start edit room modal--->
								<div class="modal fade " id="edit_room_modal" tabindex="-1" role="dialog" aria-labelledby="add_room_modal" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="add_room_modal">Update Room Details</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form id="update-dropzone-form" action="" method="POST" enctype="multipart/form-data">
													<div class="row">
														<div class="col-md-4 col-xs-12">
															<div class="form-group">
																<input type="hidden" id="update_room_id" value="" />

																<label for="edit_bed_type" class="col-form-label">Select Room Type:</label>
																<select name="update_room_type_id" class="form-control update_room_type_id" required>
																	<option value="">Select Room Type</option>
																	@if(isset($real_room_type) && sizeof($real_room_type) > 0) @foreach($real_room_type as $room_type)
																	<option value="{{$room_type->id}}" <?php if (isset($guest_room->room_type_id) && $guest_room->room_type_id == $room_type->id) { ?> selected
																		<?php } ?>>{{$room_type->room_type}}</option>
																	@endforeach @endif
																</select>
															</div>

														</div>
														<div class="col-md-4 col-xs-12">
															<div class="form-group">

																<label for="edit_bed_type" class="col-form-label">Toilet</label>
																<select name="update_room_toilet" class="form-control update_room_toilet" required>
																	<option value="">Toilet</option>
																	<option value="En suite - private for the room Guest(s)">En suite - private for the room Guest(s)</option>
																	<option value="Shared with other Guest(s) but outside the Room">Shared with other Guest(s) but outside the Room</option>
																	<option value="Shared with the rest of the family">Shared with the rest of the family</option>
																</select>
															</div>

														</div>
														<div class="col-md-4 col-xs-12">
															<div class="form-group">

																<label for="edit_bed_type" class="col-form-label">Shower</label>
																<select name="update_room_shower" class="form-control update_room_shower" required>
																	<option value="">Shower</option>
																	<option value="En suite - private for the room Guest(s)">En suite - private for the room Guest(s)</option>
																	<option value="Shared with other Guest(s) but outside the Room">Shared with other Guest(s) but outside the Room</option>
																	<option value="Shared with the rest of the family">Shared with the rest of the family</option>
																</select>
															</div>

														</div>
													</div>
													<div class="row">
														<div class="col-md-2 colx-xs-12">
															<h5>Facilities</h5>
														</div>
														<div class="col-md-10 colx-xs-12">
															<div class="form-group field-profile-emergencyname room_facility">

																<ul class="unstyled centered">
																	
																	<?php
																	$facility_array = array();

																	foreach ($room_facilities as $room_facility) {
																		$facility_id = $room_facility->id;
																		if($facility_id == 1){
																			$facility_icon = 'TV.png';
																			$facility_grey_icon = 'TV-grey.png';
																		}else if($facility_id == 2){
																			$facility_icon = 'Airconditioning.png';
																			$facility_grey_icon = 'Airconditioning-grey.png';
																		}else if($facility_id == 3){
																			$facility_icon = 'Hair-drier.png';
																			$facility_grey_icon = 'Hair-drier-grey.png';
																		}else if($facility_id == 4){
																			$facility_icon = 'safe.png';
																			$facility_grey_icon = 'safe-grey.png';
																		}else if($facility_id == 5){
																			$facility_icon = 'Computer.png';
																			$facility_grey_icon = 'Computer-grey.png';
																		}else if($facility_id == 6){
																			$facility_icon = 'Wardrobespace.png';
																			$facility_grey_icon = 'Wardrobespace-grey.png';
																		}else if($facility_id == 7){
																			$facility_icon = 'Manualheating.png';
																			$facility_grey_icon = 'Manualheating-grey.png';
																		}else if($facility_id == 8){
																			$facility_icon = 'independent.png';
																			$facility_grey_icon = 'independent-grey.png';
																		}else if($facility_id == 9){
																			$facility_icon = 'Towels.png';
																			$facility_grey_icon = 'Towels-grey.png';
																		}else if($facility_id == 10){
																			$facility_icon = 'Iron.png';
																			$facility_grey_icon = 'Iron-grey.png';
																		}
								?>
																		<li>
																			<input class="styled-checkbox update_room_facility" name="update_room_facility[]" id="update-styled-checkbox-{{$room_facility->id}}" type="checkbox" value="{{$room_facility->id}}">
																			<label for="update-styled-checkbox-{{$room_facility->id}}">
																			<img class="grey-icon" src="<?php echo Asset('/frontendassets/images/guest-room-facilities/'.$facility_grey_icon); ?>" alt="{{$room_facility->facility_type}}" />
																			<img class="orange-icon" style="display:none" src="<?php echo Asset('/frontendassets/images/guest-room-facilities/'.$facility_icon); ?>" alt="{{$room_facility->facility_type}}" />
																			</label>
																		</li>

								<?php 								} 
								?>
																</ul>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-2 colx-xs-12">
															<h5>Description</h5>
														</div>
														<div class="col-md-10 colx-xs-12">
															<div class="form-group field-profile-emergencyname room_facility">
																<input type="text" class="form-control " id="update_room_desc" />
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div id="edit_room_dropzone" class="dropzone"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="add_new_room_section">
																<h3>Add Bed To This Room</h3>
																<button class="btn btn-danger add_room_bed_btn">Add New Bed</button> 
															</div> 
															<div class="room_beds_for_update"> 
														   
															</div>
														</div>
													</div>   
													
													

												</form>
											</div>
											<div class="modal-footer">
												<span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">deleting...</span>
												<button class="btn btn-primary delete_room " title="Delete This Room" style="float:left;">Delete Room</button>
												<button type="button" class="btn btn-primary" id="update_room_modal_btn">Update Room</button>
											</div>
										</div>
									</div>
								</div>

								<!--- end edit room modal--->
								<!-- Edit Bed Modal--->
								<div class="modal fade" id="edit_bed_modal" tabindex="-1" role="dialog" aria-labelledby="edit_bed_modal" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="edit_bed_modal">Edit Bed</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form>
													<div class="form-group">
														<input type="hidden" id="update_bed_id" />
														<input type="hidden" id="update_bed_room_id" />
														<label for="edit_bed_type" class="col-form-label">Select Bed Type:</label>
														<select name="bed_type_id" class="form-control edit_bed_type_id" required>
															<option value="">Select Bed Type</option>
															@if(isset($bed_types) && sizeof($bed_types) > 0) @foreach($bed_types as $bed_type)
															<option value="{{$bed_type->id}}">{{$bed_type->bed_type}}</option>
															@endforeach @endif
														</select>
													</div>

												</form>
											</div>
											<div class="modal-footer">
												<span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">deleting...</span>
												<button class="btn btn-primary delete_bed" title="Delete This Bed">Delete Bed</button>
												<button type="button" class="btn btn-primary" id="update_bed_btn">Update Bed</button>
											</div>
										</div>
									</div>
								</div>
								<!--END UPDATE BED Modal--->
								<!--- Start Add new bed modal--->
								<div class="modal fade" id="add_bed_modal" tabindex="-1" role="dialog" aria-labelledby="add_bed_modal" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="add_bed_modal">Add New Bed To </h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form>
													<div class="form-group">
														<input type="hidden" id="room_id_for_new_bed" />
														<label for="edit_bed_type" class="col-form-label">Select Bed Type:</label>
														<select name="bed_type_id" class="form-control add_new_bed_type_id" required>
															<option value="">Select Bed Type</option>
															@if(isset($bed_types) && sizeof($bed_types) > 0) @foreach($bed_types as $bed_type)
															<option value="{{$bed_type->id}}">{{$bed_type->bed_type}}</option>
															@endforeach @endif
														</select>
													</div>

												</form>
											</div>
											<div class="modal-footer">
												<span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 70%;">Updating...</span>
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary" id="add_new_bed_modal_btn">Add Bed</button>
											</div>
										</div>
									</div>
								</div>

								<!--- end Add new bed modal--->

								<div class="margin_top10 guest_rooms_sec">
									<div class="sec_head">
										<h3 class="">Guest rooms and beds</h3>
									</div>

								</div>
								<?php 
								if(isset($userObj->user_guest_rooms) && sizeof($userObj->user_guest_rooms) > 0){
																 
									foreach($userObj->user_guest_rooms as $gr_key => $guest_room){ 
										
										if($loged_in_user_id != $propertyObj->user->id or  Auth::guest()){
											if($guest_room->is_active == 0 ){ 
												continue;
											}
										}
									
										//only getting service_type = bedroom data for guestrooms these are actually bedrooms
										if($guest_room->service_type != 'bedroom'){
											continue;
										}
										$guest_room_type = '';
										foreach($real_room_type as $room_type_value){
											if($guest_room->room_type_id == $room_type_value->id){
												$guest_room_type = $room_type_value->room_type;
												break;
											}
										}
										if($loged_in_user_id == $propertyObj->user->id){
											$room_beds = \App\Property::where(["user_id" => $guest_room->user_id, "bed_guest_room_id" => $guest_room->id])
															->where(["property_type" => "26", "property_status" => "paid"])->get();
										}else{
											$room_beds = \App\Property::where(["user_id" => $guest_room->user_id, "bed_guest_room_id" => $guest_room->id])
															->where(["property_type" => "26", "status" => "1", "property_status" => "paid"])->get();
										}
										
										$newDateStart = date("Y-m-d 11:00:00", strtotime($checkinsearch));
										$newDateEnd = date("Y-m-d  10:00:00", strtotime($checkoutsearch));
										$beds_available = false;
										foreach($room_beds as $room_bed){
											$room_bed_id = $room_bed->id;
																		
											$availableCheck = \App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $room_bed_id);

											//return $availableCheck;
											if (count($availableCheck) > 0) {                   
												   continue;                  
											}else{
												// Bed room available so add to cart now 
												$beds_available = true;
												break;
											}

										}

										$disabledDates = array();
								?>
									<div class="room_item " id="room_index_{{$guest_room->id}}" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
										
										<div class="row" id="room_title">
											<div class="col-xs-7 col-md-7">
												<h4>
								<?php  

													if(isset($guest_room->realRoomType->id)){
														if($guest_room->realRoomType->id == 8 or $guest_room->realRoomType->id == 9){ 
															$bed_room_type_icon = 'Quadruple.png';
														}else if($guest_room->realRoomType->id == 7){
															$bed_room_type_icon = 'triple.png';
														}else if($guest_room->realRoomType->id == 4 or $guest_room->realRoomType->id == 5 or $guest_room->realRoomType->id == 6){
															$bed_room_type_icon = 'twin-share.png';
														}else if($guest_room->realRoomType->id == 3){
															$bed_room_type_icon = 'double.png';
														}else{
															$bed_room_type_icon = 'single-room.png';
														}
													}else{
														$bed_room_type_icon = 'single-room.png';
													}
								?>
													<img src="{{Asset('frontendassets/images/bed-room-type/'.$bed_room_type_icon)}}" class="bed_room_type_icon" />
													{{$guest_room_type}} 
												</h4>
											</div>
											<!--col-sm-8 end-->
											<?php 
											if($loged_in_user_id == $propertyObj->user->id){
											
									
												if($guest_room->is_active){
													?>
													<div class="col-xs-5 col-md-5">
														<div class="room_action_icons">
										<?php
															if($loged_in_user_id == $propertyObj->user->id){
										?>
																<button class="edit_room" title="Edit This Room" room_facilities="{{$guest_room->room_facility}}" data-toggle="modal" data-target="#edit_room_modal" room_id="{{$guest_room->id}}" room_type_id="{{$guest_room->realRoomType->id}}" room_desc="{{isset($guest_room->room_description)?$guest_room->room_description:''}}" room_toilet="{{$guest_room->room_guest_toilet}}" room_shower="{{$guest_room->room_guest_shower}}">
																	<img src="{{asset('frontendassets/images/edit.png')}}" />
																</button>
										<?php				}else{
																
															}
										?>
														</div>
													</div>
											<?php 
												}else{
													?>
													<div class="col-xs-4 col-md-4 price_or_status">
														<span class="pending-status">Pending admin approval</span>
													</div>
													<div class="col-xs-1 col-md-1">
														<div class="room_action_icons">
										<?php
															if($loged_in_user_id == $propertyObj->user->id){
										?>
																<button class="edit_room" title="Edit This Room" room_facilities="{{$guest_room->room_facility}}" data-toggle="modal" data-target="#edit_room_modal" room_id="{{$guest_room->id}}" room_type_id="{{$guest_room->realRoomType->id}}" room_desc="{{isset($guest_room->room_description)?$guest_room->room_description:''}}" room_toilet="{{$guest_room->room_guest_toilet}}" room_shower="{{$guest_room->room_guest_shower}}">
																	<img src="{{asset('frontendassets/images/edit.png')}}" />
																</button>
										<?php				}else{
																
															}
										?>
														</div>
													</div>
											<?php 
												}
												
											}else{
												if (sizeof($room_beds) > 0) {
													if (Auth::guest()) {
														
						?>
													    <div class="col-xs-5 col-md-5">
														<div class="room_action_icons">
														<a href="{{url('signin')}}" type="button" class="guest_room_login_btn">Login</a>
														</div>
														</div>
						<?php 
													}else{
														
															if($beds_available){
						?>										<div class="col-xs-5 col-md-5">
																<div class="room_action_icons">
																<button class="send_bed_request add_room_to_basket" data-room_id="{{$guest_room->id}}" title="Add to basket">Add To Basket </button>
																</div>
																</div>
						<?php 					
															}
														
													}
												}
											}
											
								?>
											
											
										</div>

										<div class="room_description">
												<div class="row">
												<div class="col-md-6">
										
								<?php 
												$guest_room_images = \App\GuestRoomImages::where(["room_id" => $guest_room->id])->get();
												if(count($guest_room_images) > 0){
								?>
												
													<div class="guest_room_images">
								<?php  
													$img_count=0;
													$total_images=count($guest_room_images);
													foreach($guest_room_images as $room_image){
														$img_count++;
														$room_image_name = $room_image->img_name;

														$img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
														$image_size = filesize($img_path);
														//$image_size = 10000;
														//var_dump($image_size);die;  
														if($img_count==1){
															$dd="style='display:block;'";
														}else{
															$dd="style='display:none;'";
														}
								?>
															<div class="services_images_slides service-fade-image room_slides_<?php echo $guest_room->id; ?>" <?php echo $dd; ?>>
															  <div class="numbertext"><?php  echo $img_count.' / '.$total_images; ?></div>
															  <a href="<?php echo Asset('images/rentals/'.$property_id.'/rooms/'.$room_image_name); ?>" data-fancybox="images"><img src="<?php echo Asset('images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name); ?>" data-size="<?php  echo $image_size; ?>" style="width:100%" /></a>
															</div>
														
														
								<?php 
													}
								?>
														<a class="service_img_prev" onclick='plusSlides(-1,"room_slides_<?php echo $guest_room->id; ?>")'>&#10094;</a>
														<a class="service_img_next" onclick='plusSlides(1,"room_slides_<?php echo $guest_room->id; ?>")'>&#10095;</a>
													</div>
								<?php 			}else{
													echo '<div class="guest_room_images"></div>';
												}
								?>
												</div>
												<div class="col-md-6">
													<ul class="shower_toilets_icons">
														<li>
								<?php 
															if($guest_room->room_guest_toilet =='En suite - private for the room Guest(s)'){
																$bath_icon = 'Toilet - En-suite.png';
															}else if($guest_room->room_guest_toilet == 'Shared with other Guest(s) but outside the Room'){
																$bath_icon = 'Toilet - Guests only.png';
															}else{
																$bath_icon = 'Toilet - shared with family.png';
															}
																					
								?>					        <label>Toilet</label>
															<img src="{{Asset('frontendassets/images/toilet-shower/'.$bath_icon)}}" class="bed_room_type_icon" />
														</li>
														<li>
								<?php 
															if($guest_room->room_guest_shower == 'En suite - private for the room Guest(s)'){
																$shower_icon = 'Bathroom shower en-suite .png';
															}else if($guest_room->room_guest_shower == 'Shared with other Guest(s) but outside the Room'){
																$shower_icon = 'Bathroom shower guests only.png';
															}else{
																$shower_icon = 'Bathroom shower shared with family.png';
															}

								?>							<label>Shower</label>
															<img src="{{Asset('frontendassets/images/toilet-shower/'.$shower_icon)}}" class="bed_room_type_icon" />
														</li>
													</ul>
													<h4 class="margin_bottom0">Facilities</h4>
													<ul class="room_facilities">
														
															
														
								<?php

														$facilities = "";
														if (isset($guest_room->room_facility) && !empty($guest_room->room_facility)) {

															$facilities = App\UserPaidService::getRoomFacilityNames($guest_room->room_facility);
														}

														
														if (!empty($facilities)) {
															foreach ($facilities as $facility) {
																echo '<li><span class="facilities facility-'.$facility->id.' active"></span></li>';
															}
														}

								?>
													</ul>
												</div> 
											</div> 
										   
											

								<?php
											if (sizeof($room_beds) > 0) {
								?>
												<div class="room_beds_sec  room_{{$guest_room->id}}_beds">
													<div class="margin_top10" style="position:relative;    border-bottom: 1px solid #ddd;">
														<h3 class="sub_sec_head">Beds available in this room </h3>

													</div>

													<table class="beds_table" style="width:100%">
														<tbody>
								<?php

														$disabledDates= array();
														if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
															$fetchDisabledDates = App\Booking::disabledDatesForProperty($propertyObj->id);
															$disabledDates= array();
															if(sizeof($fetchDisabledDates) > 0){
																foreach($fetchDisabledDates as $dDate){
																	array_push($disabledDates,$dDate->the_date);
																}
															}

														}

														foreach ($room_beds as $bed) {

															$bed_rental_booked = App\Property::getBookedPropertyByDates($bed->id, $checkinsearch, $checkoutsearch);

															$calendarObj = App\Booking::bookingForProperty($bed->id);
															
															list($disable_checkinDates, $disable_CheckOutDates) = App\Property::prepareRestrictedDays($calendarObj);
															
															if (!empty($disable_checkinDates) && !empty($disable_CheckOutDates)) {
															   $checkin_disable_dates = $checkin_disable_dates . $disable_checkinDates; //here values are in this var. but not in rentaldetail view page seen? yes, so for unpaid it has been deleted_ dont hknow because I am checking the script and logic in rental but couldn't found . So we have it here but not in unpaid, let-s add in unpaid again, then, correct_ yes okay I'll add it.good
															   $checkout_disable_dates = $checkout_disable_dates . $disable_CheckOutDates;
															}
															  
															if(isset($bed->bedtype->id)){
																if($bed->bedtype->id == 8 or $bed->bedtype->id == 6 or $bed->bedtype->id == 5){ 
																	$bed_icon = 'bunk-bed.png';
																}else if($bed->bedtype->id == 4){
																	$bed_icon = 'futon.png';
																}else if($bed->bedtype->id == 3){
																	$bed_icon = 'futon.png';
																}else if($bed->bedtype->id == 2){
																	$bed_icon = 'double.png';
																}else{
																	$bed_icon = 'single-bed.png';
																}
															}else{
																$bed_icon = 'single-bed.png';
															}
															
															if($bed->status == 0){
																$pending_bed_status = '<span class="pending-status">Pending admin approval</span>';
															}else{
																$pending_bed_status = '';
															}
								?>
															<tr class="bed_{{$bed->id}}">
																<td>
																	<img src="{{Asset('frontendassets/images/bed-type/'.$bed_icon)}}" class="bed_room_type_icon" />
																</td>
																<td>
																	<h5>{{isset($bed->bedtype->bed_type)?$bed->bedtype->bed_type:''}} Bed <?php echo $pending_bed_status; ?></h5>
																</td>
																<td style="text-align: right;">
								<?php  
																	if(isset($bed_rental_booked->id) && $bed_rental_booked->id != ''){
																		if (isset($bed_rental_booked->user->sname16) && !empty($bed_rental_booked->user->sname16)) {
																			$dob = isset($bed_rental_booked->user->dob16) ? $bed_rental_booked->user->dob16 : "";
																			$gender = isset($bed_rental_booked->user->gender16) ? $bed_rental_booked->user->gender16 : "";
																			$gender16 = true;
																			//sname16
																			$name = isset($bed_rental_booked->user->sname16) ? $bed_rental_booked->user->sname16 : "";
																			// about_yorself is correctly "Student Description"
																		} else {
																			$dob = isset($bed_rental_booked->user->dob) ? $bed_rental_booked->user->dob : "";
																			$gender = isset($bed_rental_booked->user->gender) ? $bed_rental_booked->user->gender : "";
																			$gender16 = false;
																			//name
																			$name = isset($bed_rental_booked->user->name) ? $bed_rental_booked->user->name : "";
																		}
																		if($gender16 == false){
																			if($gender == "1"){
																				$gender = 'Male';
																			}elseif($gender == "2"){
																				$gender = 'Female';
																			}elseif($gender == "3"){
																				$gender = 'Other';
																			}else{
																				$gender = $gender;
																			}

																		}

																		$new_dob = "";
																		if (isset($dob) && !empty($dob)) {
																			$exp_dob = explode("/", $dob);
																			if (is_array($exp_dob)) {
																				$new_dob = $exp_dob[2] . "-" . $exp_dob[1] . "-" . $exp_dob[0];
																			}
																		}
																		if (!empty($new_dob)) {
																			//echo date("Y-m-d", strtotime($new_dob)) . "<br />";
																			if (isset($checkinsearch) && !empty($checkinsearch)) {
																				$booked_checkin_date = date("Y-m-d", strtotime($checkinsearch));
																			} else {
																				$booked_checkin_date = isset($bed_rental_booked->checkin) ? date("Y-m-d", strtotime($bed_rental_booked->checkin)) : "";
																			}
																			//echo $booked_checkin_date."<br />";
																			$birthdate = new DateTime($new_dob);
																			$today = new DateTime($booked_checkin_date);
																			$age = $birthdate->diff($today)->y;
																			//echo $age." Years";
								//                                                                          
																		}
																		$language = "";
																		if (isset($bed_rental_booked->user->user_extrainfo->native_language_speaker) && !empty($bed_rental_booked->user->user_extrainfo->native_language_speaker)) {
																			$language = App\Language::where("id", $bed_rental_booked->user->user_extrainfo->native_language_speaker)->first();
																			$user_language = ($language->language_name)?$language->language_name:'';
																		}

																		
																		//echo 'Already Booked';
																		echo $name.' : '.$gender.' : '.$age.' Years old'; 
																	}else{
																		
																		$newDateStart = date("Y-m-d 11:00:00", strtotime($checkinsearch));
																		$newDateEnd = date("Y-m-d  10:00:00", strtotime($checkoutsearch));
																		$available_dates_check = \App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $bed->id);
																		if(count($available_dates_check) > 0){
																			echo 'Not Availble in these dates';
																		}else{
																			
																		
																		
																			if (Auth::guest()) {
								?>
																				<a href="{{url('signin')}}" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20">Login</a>
								<?php 
																			}else{
																				if (!Auth::user()->hasRole('Student') && !Auth::user()->hasRole('Parent')) { 
								?>
																					<a href="{{url('user/edit')}}" id="requestbtn" type="button" class="airfcfx-panel btn-pad btn btn-default full_width margin_top20">Complete your Profile to send request</a>
								<?php 
																				} else {
																					if (isset($bed->booking_style) && $bed->booking_style == 1) {
																						if($loged_in_user_id == $propertyObj->user->id){
								?>
																							<?php /*<button class="edit_bed" data-bed-id="{{$bed->id}}" title="Edit This Bed" data-toggle="modal" data-target="#edit_bed_modal" data-bed_id="{{$bed->id}}" data-room_id="{{$guest_room->id}}" data-bed_type_id="{{$bed->bedtype->id}}"><img src="{{asset('frontendassets/images/edit.png')}}" /></button> */ ?>
								<?php 													}else{
									
																							$cart_data = \App\Cartlists::select('property_id')->
																							where('user_id',$loged_in_user_id)->
																							where(function($whr) use ($today){
																								$whr->where("start_date","<=",$today)->where("end_date",">=",$today)
																								->orwhere("start_date",">=",$today)->where("end_Date",">=",$today);
																							})->get();
																	
																							//echo '<pre>';
																							$cart_ids = array();
																							foreach($cart_data as $cart_item){
																								$cart_ids[] = $cart_item->property_id;
																							}  
																							//print_r($cart_ids);
																							if(!in_array($bed->id,$cart_ids)){
								?>															
																							<button  data-bed-id="{{$bed->id}}" id="requestbtn" class="add_to_basket btn-pad btn btn-danger full_width" title="Send Request or Add to basket">Add To Basket </button>
								<?php 												        }else{
								?>
																							<span class="already_added">Already Added</span>
								<?php
									
																							}
																						} 
																					} else {
								?>
																						<button type="button" onclick="instant_book('{{$bed->id}}');" id="requestbtn" class="btn-pad btn btn-danger full_width"><b>Book Now</b></button>
								<?php
																					}
																				}
																			}
																		}
																	}
								?>
																</td>
															</tr>

								<?php 
														}  
								?> 
														</tbody>
													</table>

												</div>
								<?php 		}else{
												
								?>
													<div class="room_beds_sec room_{{$guest_room->id}}_beds">
														<div class="margin_top10">
															<h3 class="sub_sec_head">Beds available in this room</h3>
														</div>
														<table class="beds_table" style="width:100%">
															<tbody>
																<tr><td><p>No Bed Availble in this room yet</p></td></tr>
															</tbody>
														</table>
														
													</div>

								<?php 
											}
								?>
											<div class="guest_room_calendar_info">
												<h3>See calendar and other info</h3>
											
												<div class="guest_room_other_details" style="display:none;">
													<div class="calendar_for_bed">
														<div class="start_date_box" style="max-width:47%;float:left;">
														<label style="display:block">Check In</label>
														<input type="text"   value="{{$checkinsearch}}"  readonly class="font-size12 form-control margin10 cal startdate" placeholder="DD-MM-YYYY" > 
														</div>
														<div class="end_date_box" style="max-width:47%;float:left;">
														<label style="display:block">Check Out</label>
														<input type="text"   value="{{$checkoutsearch}}"  readonly class="font-size12 form-control margin10 cal enddate" placeholder="DD-MM-YYYY" >  
														</div>
													</div>
													<div class="bed_descp">
														<h4>Description</h4>
														<p>{{isset($guest_room->room_description)?$guest_room->room_description:''}}</p>
													</div>
												</div> 
											</div>
										</div>
									</div>
								<?php 
									}
									if($loged_in_user_id == $propertyObj->user->id){
								?>
										<button class="add_new_room" title="Add another Guest Room" data-toggle="modal" data-target="#add_room_modal">Add another room</button>
								<?php 
									}
								}else{
									if($loged_in_user_id == $propertyObj->user->id){
								?>
										<button class="add_new_room" title="Add New Guest Room" data-toggle="modal" data-target="#add_room_modal">Add new room</button>
								<?php 
									}

								}
								?> 
								
								
							</div>	
                           
                        </div>
                        
                        <div id="school_house_extra_info" class="col-md-12" style="display:none">
                            <h3 class="margin_top30 sec_head"><b>Extra Info</b></h3>
                            @if( (isset($userObj->paid_service->urban_location) && !empty($userObj->paid_service->urban_location)) 
                            || (isset($userObj->paid_service->property_region) && !empty($userObj->paid_service->property_region))
                            || (isset($userObj->paid_service->local_amenity) && !empty($userObj->paid_service->local_amenity))
                            || (isset($userObj->paid_service->amenity_desc) && !empty($userObj->paid_service->amenity_desc))
                            )
                            <div>
                                <div class="margin_top10">
                                    <h3 class="sub_sec_head">Area And Amenities</h3>
                                </div>
                                @if(isset($userObj->paid_service->urban_location) && !empty($userObj->paid_service->urban_location))
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Urban Location</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->urban_location)?$userObj->paid_service->urban_location:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->property_region) && !empty($userObj->paid_service->property_region))
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Region</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->property_region)?$userObj->paid_service->property_region:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->local_amenity) && !empty($userObj->paid_service->local_amenity))
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Local Amenities</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->local_amenity)?str_replace(",", ", ", $userObj->paid_service->local_amenity):''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                                @if(isset($userObj->paid_service->amenity_desc) && !empty($userObj->paid_service->amenity_desc))
                                <div class="row" id="description">
                                    <div class="col-xs-12 col-sm-4">
                                        <p class=" headtxt text_gray1">Local Amenities Description</p>
                                    </div><!--col-sm-4 end-->
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="" style="word-wrap:break-word;">
                                            {{isset($userObj->paid_service->amenity_desc)?$userObj->paid_service->amenity_desc:''}}
                                        </p>
                                    </div><!--col-sm-8 end-->
                                </div>
                                @endif
                            </div>
                            @else <p>No extra information has been added</p>
                            @endif
                        </div>

                        <div id="school_house_included_services" class="col-md-12">
                            <h3 class="margin_top30 sec_head"><b>Included Services</b></h3>
						</div>
						<div id="classes_section" class=" margin_top10 col-md-12">
							<!--- Start Add new Room modal--->
							<div class="modal fade " id="add_classroom_modal" tabindex="-1" role="dialog" aria-labelledby="add_classroom_modal" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="add_classroom_modal">Add New Class Room </h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<form id="dropzone-form-classs-rooms" action="" method="POST" enctype="multipart/form-data">
												<div class="row">
													
													<div class="col-md-6 colx-xs-12">
														<div class="form-group">
															 <label for="classroom_title" class="col-form-label">Class Room Title</label>
															<input type="text" class="form-control" id="new_add_classroom_title" />
														</div>
													</div>
												   
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
															<label for="classroom_title" class="col-form-label">Number of places
																<span class="general_tooltip" >	
																	<img src="{{Asset('images/tooltip.png')}}" class="general_tool_tip_icon" />
																	<span class="custom_tooltiptext">how many students you can facilitate</span>												
																</span>
															</label>
															<input type="number" class="form-control add_class_room_number_places" value="" /> 
														</div>

													</div>
													
												</div>
												<div class="row">
													<div class="col-md-4 col-xs-12">
														<div class="form-group"> 

															<label for="class_room_currency" class="col-form-label">Select Basic Price Currency</label>
															
															{!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'add_class_room_currency']) !!}
														</div>

													</div>
													 <div class="col-md-4 col-xs-12">
														<div class="form-group"> 

															<label for="add_class_room_payment_type" class="col-form-label">Select Payment Type</label>
															<select name="add_class_room_payment_type" class="form-control add_class_room_payment_type" required>
																<option value="">Select Payment Type</option>
																<option value="weekly">Weekly</option>
																<option value="fixed">Fixed</option>
															</select>
														</div>

													</div>
													<div class="col-md-4 colx-xs-12">
														<div class="form-group">
															 <label for="class_room_amount" class="col-form-label">Amount</label>
															<input type="number" class="form-control" id="class_room_amount" />
														</div>
													</div>
												   
												   
													
												</div>
												<div class="row">
													<div class="col-md-2 colx-xs-12">
														<h5>Facilities</h5>
													</div>
													<div class="col-md-10 colx-xs-12">
														<div class="form-group field-profile-emergencyname classroom_facility">

															<ul class="unstyled centered">
																<?php
																
																foreach ($classroom_facilities as $facility_id) {
																	if($facility_id == 1){
																		$facility_type = 'Computer';
																		$facility_icon = 'Computer.png';
																		$facility_grey_icon = 'Computer-grey.png';
																	}else if($facility_id == 2){
																		$facility_type = 'exam-preparation';
																		$facility_icon = 'exam-preparation.png';
																		$facility_grey_icon = 'exam-preparation-grey.png';
																	}else if($facility_id == 3){
																		$facility_type = 'lesson-material-books';
																		$facility_icon = 'lesson-material-books.png';
																		$facility_grey_icon = 'lesson-material-books-grey.png';
																	}else if($facility_id == 4){
																		$facility_type = 'Projector';
																		$facility_icon = 'Projector.png';
																		$facility_grey_icon = 'Projector-grey.png';
																	}else if($facility_id == 5){
																		$facility_type = 'writing-board';
																		$facility_icon = 'writing-board.png';
																		$facility_grey_icon = 'writing-board-grey.png';
																	
																	}
							?>
																	<li>  
																		<input class="styled-checkbox classroom_facility" name="classroom_facilities[]" id="classroom-checkbox-{{$facility_id}}" type="checkbox" value="{{$facility_id}}">
																		<label for="classroom-checkbox-{{$facility_id}}">
																		<img class="grey-icon" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_grey_icon); ?>" alt="{{$facility_type}}" />
																		<img class="orange-icon" style="display:none" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_icon); ?>" alt="{{$facility_type}}" />
																		</label>
																	</li>

							<?php 								} 
							?>                                   
															</ul>
														</div>
													</div>
												</div>

												
												 <div class="row"> 
													<div class="col-md-2 colx-xs-12">
														<h5>Description</h5>
													</div>
													<div class="col-md-10 colx-xs-12">
														<div class="form-group field-profile-emergencyname room_facility">
															<input type="text" class="form-control" id="new_add_classroom_description" />
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div id="classroomdropzone" class="dropzone"></div>
													</div>
												</div>

											</form>
										</div>
										<div class="modal-footer">
											<span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 70%;">adding classroom...</span>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-primary" id="add_new_classroom_modal_btn">Add Class Room</button>
										</div>
									</div>
								</div>
							</div>

							<!--- end Add new room modal--->
							<!--- Start edit room modal--->
							<div class="modal fade " id="edit_classroom_modal" tabindex="-1" role="dialog" aria-labelledby="edit_classroom_modal" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="">Update Room Details</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<form id="update-dropzone-form-classrooms" action="" method="POST" enctype="multipart/form-data">
												<div class="row">
													<input type="hidden" id="update_classroom_id" value="" />
													
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
															<label for="classroom_title" class="col-form-label">Class Room Title</label>
															<input type="text" class="form-control " id="update_classroom_title" value="" /> 
														</div>

													</div>
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
															<label for="classroom_title" class="col-form-label">Number of places
																<span class="general_tooltip" >	
																	<img src="{{Asset('images/tooltip.png')}}" class="general_tool_tip_icon" />
																	<span class="custom_tooltiptext">how many students you can facilitate</span>												
																</span>
															</label> 
															<input type="number" class="form-control " id="update_class_room_number_places" value="" /> 
														</div>

													</div>
												</div>
												<div class="row">
													<div class="col-md-4 col-xs-12">
														<div class="form-group"> 

															<label for="class_room_currency" class="col-form-label">Select Basic Price Currency</label>
															
															{!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'update_class_room_currency']) !!}
														</div>

													</div>
													 <div class="col-md-4 col-xs-12">
														<div class="form-group"> 

															<label for="add_class_room_payment_type" class="col-form-label">Select Payment Type</label>
															<select name="add_class_room_payment_type" class="form-control update_class_room_payment_type" required>
																<option value="">Select Payment Type</option>
																<option value="weekly">Weekly</option>
																<option value="fixed">Fixed</option>
															</select>
														</div>

													</div>
													<div class="col-md-4 colx-xs-12">
														<div class="form-group">
															 <label for="class_room_amount" class="col-form-label">Amount</label>
															<input type="number" class="form-control" id="update_class_room_amount" />
														</div>
													</div>
												   
												   
													
												</div>
												
												<div class="row">
													<div class="col-md-2 colx-xs-12">
														<h5>Facilities</h5>
													</div>
													<div class="col-md-10 colx-xs-12">
														<div class="form-group field-profile-emergencyname classroom_facility">

															<ul class="unstyled centered">
																<?php
																
																foreach ($classroom_facilities as $facility_id) {
																	if($facility_id == 1){
																		$facility_type = 'Computer';
																		$facility_icon = 'Computer.png';
																		$facility_grey_icon = 'Computer-grey.png';
																	}else if($facility_id == 2){
																		$facility_type = 'exam-preparation';
																		$facility_icon = 'exam-preparation.png';
																		$facility_grey_icon = 'exam-preparation-grey.png';
																	}else if($facility_id == 3){
																		$facility_type = 'lesson-material-books';
																		$facility_icon = 'lesson-material-books.png';
																		$facility_grey_icon = 'lesson-material-books-grey.png';
																	}else if($facility_id == 4){
																		$facility_type = 'Projector';
																		$facility_icon = 'Projector.png';
																		$facility_grey_icon = 'Projector-grey.png';
																	}else if($facility_id == 5){
																		$facility_type = 'writing-board';
																		$facility_icon = 'writing-board.png';
																		$facility_grey_icon = 'writing-board-grey.png';
																	
																	}
							?>
																	<li>  
																		<input class="styled-checkbox update_classroom_facility" name="update_classroom_facilities[]" id="class-update_styled-checkbox-{{$facility_id}}" type="checkbox" value="{{$facility_id}}">
																		<label for="class-update_styled-checkbox-{{$facility_id}}">
																		<img class="grey-icon" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_grey_icon); ?>" alt="{{$facility_type}}" />
																		<img class="orange-icon" style="display:none" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_icon); ?>" alt="{{$facility_type}}" />
																		</label>
																	</li>

							<?php 								} 
							?>                                  
															</ul>
														</div>
													</div>
												</div>

											   
												<div class="row">
													<div class="col-md-2 colx-xs-12">
														<h5>Description</h5>
													</div>
													<div class="col-md-10 colx-xs-12">
														<div class="form-group field-profile-emergencyname ">
															<input type="text" class="form-control " id="update_classroom_desc" />
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div id="edit_classroom_dropzone" class="dropzone"></div>
													</div>
												</div>
							 
											</form>
										</div>
										<div class="modal-footer">
											<span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">deleting...</span>
											<button class="btn btn-primary delete_classroom" title="Delete This Room" style="float:left;">Delete Class Room</button>
											<button type="button" class="btn btn-primary" id="update_classroom_modal_btn">Update Class Room</button>
										</div>
									</div>
								</div>
							</div>

							<div class="modal fade " id="pre_maid_classroom_modal" tabindex="-1" role="dialog" aria-labelledby="pre_maid_classroom_modal" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="">Class Room Details</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<form id="premade-dropzone-form-classrooms" action="" method="POST" enctype="multipart/form-data">
												<div class="row">
													
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
															<label for="classroom_title" class="col-form-label">Class Room Title</label>
															<input type="text" class="form-control " id="premade_classroom_title" value="" /> 
															<input type="hidden" name="pre_made_class_id" id="pre_made_class_id" value="" />
														</div>

													</div>
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
															<label for="classroom_title" class="col-form-label">Number of places
																<span class="general_tooltip" >	
																	<img src="{{Asset('images/tooltip.png')}}" class="general_tool_tip_icon" />
																	<span class="custom_tooltiptext">how many students you can facilitate</span>												
																</span>
															</label> 
															<input type="number" class="form-control " id="premade_class_room_number_places" value="" /> 
														</div>

													</div>
												</div>
												<div class="row">
													<div class="col-md-4 col-xs-12">
														<div class="form-group"> 

															<label for="class_room_currency" class="col-form-label">Select Basic Price Currency</label>
															
															{!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'premade_class_room_currency']) !!}
														</div>

													</div>
													 <div class="col-md-4 col-xs-12">
														<div class="form-group"> 

															<label for="add_class_room_payment_type" class="col-form-label">Select Payment Type</label>
															<select name="add_class_room_payment_type" class="form-control premade_class_room_payment_type" required>
																<option value="">Select Payment Type</option>
																<option value="weekly">Weekly</option>
																<option value="fixed">Fixed</option>
															</select>
														</div>

													</div>
													<div class="col-md-4 colx-xs-12">
														<div class="form-group">
															 <label for="class_room_amount" class="col-form-label">Amount</label>
															<input type="number" class="form-control" id="premade_class_room_amount" />
														</div>
													</div>
												   
												   
													
												</div>
												
												<div class="row">
													<div class="col-md-2 colx-xs-12">
														<h5>Facilities</h5>
													</div>
													<div class="col-md-10 colx-xs-12">
														<div class="form-group field-profile-emergencyname classroom_facility">

															<ul class="unstyled centered">
							<?php
																

																foreach ($classroom_facilities as $facility_id) {
																	if($facility_id == 1){
																		$facility_type = 'Computer';
																		$facility_icon = 'Computer.png';
																		$facility_grey_icon = 'Computer-grey.png';
																	}else if($facility_id == 2){
																		$facility_type = 'exam-preparation';
																		$facility_icon = 'exam-preparation.png';
																		$facility_grey_icon = 'exam-preparation-grey.png';
																	}else if($facility_id == 3){
																		$facility_type = 'lesson-material-books';
																		$facility_icon = 'lesson-material-books.png';
																		$facility_grey_icon = 'lesson-material-books-grey.png';
																	}else if($facility_id == 4){
																		$facility_type = 'Projector';
																		$facility_icon = 'Projector.png';
																		$facility_grey_icon = 'Projector-grey.png';
																	}else if($facility_id == 5){
																		$facility_type = 'writing-board';
																		$facility_icon = 'writing-board.png';
																		$facility_grey_icon = 'writing-board-grey.png';
																	
																	}
							?>
																	<li>  
																		<input class="styled-checkbox premade_classroom_facility" name="premade_classroom_facility[]" id="premade_classroom-checkbox-{{$facility_id}}" type="checkbox" value="{{$facility_id}}">
																		<label for="premade_classroom-checkbox-{{$facility_id}}">
																		<img class="grey-icon" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_grey_icon); ?>" alt="{{$facility_type}}" />
																		<img class="orange-icon" style="display:none" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_icon); ?>" alt="{{$facility_type}}" />
																		</label>
																	</li>

							<?php 								} 
							?>                                   
															   
															</ul>
														</div>
													</div>
												</div>

											   
												<div class="row">
													<div class="col-md-2 colx-xs-12">
														<h5>Description</h5>
													</div>
													<div class="col-md-10 colx-xs-12">
														<div class="form-group field-profile-emergencyname ">
															<input type="text" class="form-control " id="premade_classroom_desc" />
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div id="premade_classroom_dropzone" class="dropzone"></div>
													</div>
												</div>
							 
											</form>
										</div>
										<div class="modal-footer">
											<span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">deleting...</span>
											<button type="button" class="btn btn-primary" id="premade_classroom_modal_btn">Add Class Room</button>
										</div>
									</div>
								</div>
							</div>

							<!--- end edit room modal--->
							
							<div class="margin_top10 class_rooms_sec">
								<div class="sec_head">
									<h3 class="">Classes</h3>
								</div>
							</div>
							<?php
                                
							if(in_array(1,$pre_made_class_ids)){ ?>
							<div class="classroom_item "  id="pre-made-class-1" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
								<div class="row" id="classroom_title">
									<div class="col-xs-8 col-md-7">
										<h4  data-toggle="tooltip" title="Five 30 minute conversations"> 
										<img src="{{Asset('frontendassets/images/class-types/Individual lesson class - grey.png')}}" class="class_room_type_icon" />
											Five 30 minute conversations 
										</h4>
									</div>
									<!--col-sm-8 end-->
									<div class="col-xs-4 col-md-5">
										<div class="classroom_action_icons">
											<span class="included">Included</span> 
										</div>
									</div>
								</div>
								<div class="classroom_description">
									<ul class="class_room_images">
								
											<li>
												<a href="<?php echo Asset('frontendassets/images/pre-made-classes/School House with 16 to 18.png'); ?>" data-fancybox="images"><img src="<?php echo Asset('frontendassets/images/pre-made-classes/School House with 16 to 18.png'); ?>" /></a>
											</li>
											
									</ul> 
											
									<p class="room_desp" style="word-wrap:break-word;">
										Included in the price, to speak about work or anything you like, to make sure the host engages professionally specifying what time to start the sessions, to make sure the students don't run out or off to their rooms.
									</p>
									
								
								</div>
							</div> 
							<?php 
							}if(!in_array(1,$pre_made_class_ids) && ($loged_in_user_id == $propertyObj->user->id)){ ?>
							<div class="classroom_item "  id="pre-made-class-1" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
								<div class="row" id="classroom_title">
									<div class="col-xs-8 col-md-7">
										<h4  data-toggle="tooltip" title="Five 30 minute conversations"> 
										<img src="{{Asset('frontendassets/images/class-types/Individual lesson class - grey.png')}}" class="class_room_type_icon" />
											Five 30 minute conversations 
										</h4>
									</div>
									<!--col-sm-8 end-->
									<div class="col-xs-4 col-md-5">
										<div class="classroom_action_icons">
											<button class="add-pre-made-class" pre-made="1">Add this class</button>
										</div>
									</div>
								</div>
								<div class="classroom_description">
									<ul class="class_room_images">
								
											<li>
												<a href="<?php echo Asset('frontendassets/images/pre-made-classes/School House with 16 to 18.png'); ?>" data-fancybox="images"><img src="<?php echo Asset('frontendassets/images/pre-made-classes/School House with 16 to 18.png'); ?>" /></a>
											</li>
											
									</ul> 
											
									<p class="room_desp" style="word-wrap:break-word;">
										Included in the price, to speak about work or anything you like, to make sure the host engages professionally specifying what time to start the sessions, to make sure the students don't run out or off to their rooms.
									</p>
									
								
								</div>
							</div> 
							<?php 
							}
							 if(!in_array(2,$pre_made_class_ids) && ($loged_in_user_id == $propertyObj->user->id)){ ?>
							<div class="classroom_item " id="pre-made-class-2" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
								<div class="row" id="classroom_title">
									<div class="col-xs-8 col-md-7">
										<h4  data-toggle="tooltip" title="Five 30 minute conversations"> 
										<img src="{{Asset('frontendassets/images/class-types/Individual lesson class - grey.png')}}" class="class_room_type_icon" />
											10 hours of individual lessons
										</h4>
									</div>
									<!--col-sm-8 end-->
									<div class="col-xs-4 col-md-5">
										<div class="classroom_action_icons">
											<button class="add-pre-made-class" pre-made="2" price_type="weekly" amount="180" currency_id="5" room_facilities="1,2,3" seats="1" >Add this class</button>
										</div>
									</div>
								</div>
								<div class="classroom_description">
									<ul class="class_room_images">
								
											<li> 
												<a href="<?php echo Asset('frontendassets/images/pre-made-classes/Maute-Student-signup.png'); ?>" data-fancybox="images">
												<img src="<?php echo Asset('frontendassets/images/pre-made-classes/Maute-Student-signup.png'); ?>" />
												</a>
											</li>
											
									</ul> 
									<ul class="classroom_facilities">
										<li>
											Facilities
										</li>
										<li><span class="facilities calss_facility-1 active"></span></li>
										<li><span class="facilities calss_facility-2 active"></span></li>
										<li><span class="facilities calss_facility-3 active"></span></li>
										<li><span class="facilities calss_facility-4 active"></span></li>
										<li><span class="facilities calss_facility-5 active"></span></li>
									</ul>
									<p class="room_desp" style="word-wrap:break-word;">
										These individual lessons may be done in the morning or afternoon, suggested times are 9.30 am to 10.30 am , half hour break, and again 11.00 am to 12.00 am from Monday to Friday.

									</p>
									
								
								</div>
							</div> 
							<?php 
							}
							 if(!in_array(3,$pre_made_class_ids) && ($loged_in_user_id == $propertyObj->user->id)){ ?>
							<div class="classroom_item " id="pre-made-class-3" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
								<div class="row" id="classroom_title">
									<div class="col-xs-8 col-md-7">
										<h4  data-toggle="tooltip" title="Five 30 minute conversations"> 
										<img src="{{Asset('frontendassets/images/class-types/Class for up to 4 students - grey.png')}}" class="class_room_type_icon" />
											15 hours of individual lessons
										</h4>
									</div>
									<!--col-sm-8 end-->
									<div class="col-xs-4 col-md-5">
										<div class="classroom_action_icons">
											<button class="add-pre-made-class" pre-made="3"  price_type="weekly" amount="270" room_facilities="1,2,3" currency_id="5" seats="4" >Add this class</button>
										</div>
									</div>
								</div>
								<div class="classroom_description">
									<ul class="class_room_images">
								
											<li>
												<a href="<?php echo Asset('frontendassets/images/pre-made-classes/School-House-under-16.png'); ?>" data-fancybox="images">
												<img src="<?php echo Asset('frontendassets/images/pre-made-classes/School-House-under-16.png'); ?>" />
												</a>
											</li> 
											
									</ul> 
									<ul class="classroom_facilities">
										<li>
											Facilities
										</li>
										<li><span class="facilities calss_facility-1 active"></span></li>
										<li><span class="facilities calss_facility-2 active"></span></li>
										<li><span class="facilities calss_facility-3 active"></span></li>
										<li><span class="facilities calss_facility-4 active"></span></li>
										<li><span class="facilities calss_facility-5 active"></span></li>
									</ul>
									<p class="room_desp" style="word-wrap:break-word;">
										These small group sessions shared by up to 4 students may be done in the morning or afternoon, suggested times are two sessions from 9.00 am to 10.30 am, break, and again 11.00 am to 12.30 am for a total of 20 lessons a week.
									</p>
									
								
								</div>
							</div> 
							<?php  
							}
							?>
							<?php


								if(isset($userObj->user_guest_rooms) && sizeof($userObj->user_guest_rooms) > 0){
									
									foreach($userObj->user_guest_rooms as $gr_key => $class_room){ 
										if($loged_in_user_id == $propertyObj->user->id){
											$class_room_places = \App\Property::where(["user_id" => $class_room->user_id, "bed_guest_room_id" => $class_room->id])
																->where(["property_type" => "24",  "property_status" => "paid"])->get();
										}else{
											$class_room_places = \App\Property::where(["user_id" => $class_room->user_id, "bed_guest_room_id" => $class_room->id])
																->where(["property_type" => "24", "status" => "1", "property_status" => "paid"])->get();
										}
										$newDateStart = date("Y-m-d 11:00:00", strtotime($checkinsearch));
										$newDateEnd = date("Y-m-d  10:00:00", strtotime($checkoutsearch));
										$places_available = false;
										foreach($class_room_places as $place){
											$place_id = $place->id;
																		
											$availableCheck = \App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $place_id);

											//return $availableCheck;
											if (count($availableCheck) > 0) {                   
												   continue;                  
											}else{
												// Bed room available so add to cart now 
												$places_available = true;
												break;
											}

										}
										
										
										
										$total_places = count($class_room_places);
										if($class_room->service_type != 'classroom'){
											continue;
										}
										if($loged_in_user_id != $propertyObj->user->id or  Auth::guest()){
											if($guest_room->is_active == 0 ){ 
												continue;
											}
										}  
										
										if($class_room->pre_made_id != NULL && $class_room->pre_made_id == 1 ){
											continue;
										}
										$guest_room_type = '';
										foreach($class_types as $class_type_value){
											if($class_room->room_type_id == $class_type_value->id){
												$guest_room_type = $class_type_value->class_type;
												break;
											}
										}
									?>
									
										<div class="classroom_item " id="classroom_index_{{$class_room->id}}" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
											<div class="row" id="classroom_title">
											<?php   
												$payment_currency = 5;
												$payment_amount = 0;
												$payment_type = 'GBP';
												if($loged_in_user_id == $propertyObj->user->id){
													
											
													if($class_room->is_active){
														
											?>
															<div class="col-xs-8 col-md-8">
																<h4 title="{{$class_room->room_name}}">
																	<?php  
																	$total_places = count($class_room_places);
																	if($total_places > 1 && $total_places <= 4){
																		$class_type_icon = 'Class for up to 4 students.png';
																	}else if($total_places > 4){
																		$class_type_icon = 'Class for more than 4 students.png';
																	}else{
																		$class_type_icon = 'Individual lesson class.png';
																	} 
																	?>
																	<img src="{{Asset('frontendassets/images/class-types/'.$class_type_icon)}}" class="class_room_type_icon" />
																	{{$class_room->room_name}} 
																</h4>
															</div>
															<div class="col-xs-3 col-md-3 price_or_status">
											<?php 
																if (sizeof($class_room_places) > 0) {
												
																	$place_id = $class_room_places->first()->id;
																	$pricing = \App\PropertyPricing::where("property_id", $place_id)->first();
																	
																	if(isset($pricing->payment_type)){
																		
																		$payment_type = $pricing->payment_type;
																		
																		$payment_currency = $currencies[$pricing->currency_id];
																		$payment_amount = $pricing->amount;
																		
																	
																	?> 
																	
																	<span class="place_pricing">{{$payment_currency}} {{$payment_amount}}  {{$payment_type}}</span>
																	
																	<?php 
																	}
																}
											?>
															</div>
															<div class="col-xs-1 col-md-1">
																<div class="classroom_action_icons">
																	
																	<button class="edit_classroom" title="Edit This Class Room" payment_currency="{{$payment_currency}}" payment_type="{{$payment_type}}" payment_amount="{{$payment_amount}}" room_facilities="{{$class_room->room_facility}}" data-toggle="modal" data-target="#edit_classroom_modal" room_id="{{$class_room->id}}"  room_desc="{{isset($class_room->room_description)?$class_room->room_description:''}}" room_title="{{$class_room->room_name}}" number_places="{{$total_places}}">
																		<img src="{{asset('frontendassets/images/edit.png')}}" />
																	</button> 
																</div>
															</div>
												
											<?php
														
													}else{ 
											?>			<div class="col-xs-6 col-md-7">
															<h4 title="{{$class_room->room_name}}">
																<?php  
																if (sizeof($class_room_places) > 0) {
												
																	$place_id = $class_room_places->first()->id;
																	$pricing = \App\PropertyPricing::where("property_id", $place_id)->first();
																	
																	if(isset($pricing->payment_type)){
																		
																		$payment_type = $pricing->payment_type;
																		
																		//$payment_currency = $currencies[$pricing->currency_id];
																		$payment_amount = $pricing->amount;
																		
																	
																	
																	}
																}
																$total_places = count($class_room_places);
																if($total_places > 1 && $total_places <= 4){
																	$class_type_icon = 'Class for up to 4 students.png';
																}else if($total_places > 4){
																	$class_type_icon = 'Class for more than 4 students.png';
																}else{
																	$class_type_icon = 'Individual lesson class.png';
																} 
																?>
																<img src="{{Asset('frontendassets/images/class-types/'.$class_type_icon)}}" class="class_room_type_icon" />
																{{$class_room->room_name}} 
															</h4>
														</div>
														<div class="col-xs-4 col-md-4 price_or_status">
															<span class="pending-status">Pending admin approval</span> 
														</div>
														<div class="col-xs-4 col-md-1">
															<div class="classroom_action_icons">
																
																<button class="edit_classroom" title="Edit This Class Room" payment_currency="{{$payment_currency}}" payment_type="{{$payment_type}}" payment_amount="{{$payment_amount}}" room_facilities="{{$class_room->room_facility}}" data-toggle="modal" data-target="#edit_classroom_modal" room_id="{{$class_room->id}}"  room_desc="{{isset($class_room->room_description)?$class_room->room_description:''}}" room_title="{{$class_room->room_name}}" number_places="{{$total_places}}">
																	<img src="{{asset('frontendassets/images/edit.png')}}" />
																</button> 
															</div>
														</div>
										<?php 
													}
													
										?>
												
											
													
											<?php   	
												}else{
											?>
												<div class="col-xs-6 col-md-6">
													<h4 title="{{$class_room->room_name}}">
														<?php  
														$total_places = count($class_room_places);
														if($total_places > 1 && $total_places <= 4){
															$class_type_icon = 'Class for up to 4 students.png';
														}else if($total_places > 4){
															$class_type_icon = 'Class for more than 4 students.png';
														}else{
															$class_type_icon = 'Individual lesson class.png';
														} 
														?>
														<img src="{{Asset('frontendassets/images/class-types/'.$class_type_icon)}}" class="class_room_type_icon" />
														{{$class_room->room_name}} 
													</h4>
												</div>
											<?php 
													if (sizeof($class_room_places) > 0) {
														$place_id = $class_room_places->first()->id;
														$pricing = \App\PropertyPricing::where("property_id", $place_id)->first();
														if(isset($pricing->payment_type)){
															$payment_type = $pricing->payment_type;
															
															$payment_currency = $currencies[$pricing->currency_id];
															$payment_amount = $pricing->amount;
															
														
														?> 
														<div class="col-xs-3 col-md-3">
														<span class="place_pricing">{{$payment_currency}} {{$payment_amount}}  {{$payment_type}}</span>
														</div>
														<?php 
														}
														if (Auth::guest()) {
							?>
															<div class="col-xs-3 col-md-3">
																<div class="classroom_action_icons">
																	<a href="{{url('signin')}}" type="button" class="guest_room_login_btn">Login</a>
																</div>
															</div>
							<?php 
														}else{
															if($places_available){
							?>
																<div class="col-xs-3 col-md-3">
																	<div class="classroom_action_icons">
																		<button class="send_bed_request add_classsroom_to_basket" data-room_id="{{$guest_room->id}}" title="Send Request or Add to basket">Add To Basket </button>
																	</div>
																</div>
							<?php 
															}
														}
													}
												} 
														?>
													
											</div>
								
											<div class="classroom_description">
												
										
										<?php 
												$guest_room_images = \App\GuestRoomImages::where(["room_id" => $class_room->id])->get();
												if(count($guest_room_images) > 0){ 
										?>
													<ul class="class_room_images">
														<?php  
														foreach($guest_room_images as $room_image){
															$room_image_name = $room_image->img_name;

															$img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
															$image_size = filesize($img_path);  
															//$image_size = 78000;
															//var_dump($image_size);die;  
															?>
															<li>
																<a href="<?php echo Asset('images/rentals/'.$property_id.'/rooms/'.$room_image_name); ?>" data-fancybox="images"><img src="<?php echo Asset('images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name); ?>" data-size="<?php  echo $image_size; ?>" /></a>
															</li>
															<?php 
														}
														
														?>
													</ul>
										<?php 	}else{
													echo '<ul class="class_room_images"></ul>';
												} 
												?>
												<ul class="classroom_facilities">
													<li>
														Facilities
													</li>
													<?php

														$facilities = "";
														if (isset($class_room->room_facility) && !empty($class_room->room_facility)) {

															$facilities = App\UserPaidService::getRoomFacilityNames($class_room->room_facility);
														}

														if (!empty($facilities)) {
															foreach ($facilities as $facility) {
																echo '<li><span class="facilities calss_facility-'.$facility->id.' active"></span></li>';
															}
														}

													?>
												</ul>
												<p class="room_desp" style="word-wrap:break-word;">
													{{isset($class_room->room_description)?$class_room->room_description:''}}
												</p>
												<?php 
												$disabledDates = array();

												if (sizeof($class_room_places) > 0) {
													
													$place_id = $class_room_places->first()->id;
													$pricing = \App\PropertyPricing::where("property_id", $place_id)->first();
													//var_dump($currencies);die;
													
											?>  
											
													<div class="classroom_places_sec  classroom_{{$class_room->id}}_places">
														<div class="margin_top10" style="position:relative;    border-bottom: 1px solid #ddd;">
															<h3 class="sub_sec_head">Places available in this classroom </h3>
															
														</div>

														<div class="places_box" style="width:100%">
														   
																<?php

															$disabledDates= array();
															if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
																$fetchDisabledDates = App\Booking::disabledDatesForProperty($propertyObj->id);
																$disabledDates= array();
																if(sizeof($fetchDisabledDates) > 0){
																	foreach($fetchDisabledDates as $dDate){
																		array_push($disabledDates,$dDate->the_date);
																	}
																}

															}
															
															foreach ($class_room_places as $place) {

																$bed_rental_booked = App\Property::getBookedPropertyByDates($place->id, $checkinsearch, $checkoutsearch);

																$calendarObj = App\Booking::bookingForProperty($place->id);

																list($disable_checkinDates, $disable_CheckOutDates) = App\Property::prepareRestrictedDays($calendarObj);
																if (!empty($disable_checkinDates) && !empty($disable_CheckOutDates)) {
																   $checkin_disable_dates = $checkin_disable_dates . $disable_checkinDates; //here values are in this var. but not in rentaldetail view page seen? yes, so for unpaid it has been deleted_ dont hknow because I am checking the script and logic in rental but couldn't found . So we have it here but not in unpaid, let-s add in unpaid again, then, correct_ yes okay I'll add it.good
																   $checkout_disable_dates = $checkout_disable_dates . $disable_CheckOutDates; 
																}
															
																

																?> 
																	<div class="class_room_place">
																		<img src="{{Asset('images/class-icon.png')}}" class="class_room_type_icon" />
																		
																			<div class="custom_tooltip" >	
																				<img src="{{Asset('images/available-icon.png')}}" class="tool_tip_icon" />
																				<span class="custom_tooltiptext">Place is available</span>												
																			</div>
																			
																	</div>
																		<?php  
																	
																		if(isset($bed_rental_booked->id) && $bed_rental_booked->id != ''){
																			if (isset($bed_rental_booked->user->sname16) && !empty($bed_rental_booked->user->sname16)) {
																				$dob = isset($bed_rental_booked->user->dob16) ? $bed_rental_booked->user->dob16 : "";
																				$gender = isset($bed_rental_booked->user->gender16) ? $bed_rental_booked->user->gender16 : "";
																				$gender16 = true;
																				//sname16
																				$name = isset($bed_rental_booked->user->sname16) ? $bed_rental_booked->user->sname16 : "";
																				// about_yorself is correctly "Student Description"
																			} else {
																				$dob = isset($bed_rental_booked->user->dob) ? $bed_rental_booked->user->dob : "";
																				$gender = isset($bed_rental_booked->user->gender) ? $bed_rental_booked->user->gender : "";
																				$gender16 = false;
																				//name
																				$name = isset($bed_rental_booked->user->name) ? $bed_rental_booked->user->name : "";
																			}
																			if($gender16 == false){
																				if($gender == "1"){
																					$gender = 'Male';
																				}elseif($gender == "2"){
																					$gender = 'Female';
																				}elseif($gender == "3"){
																					$gender = 'Other';
																				}else{
																					$gender = $gender;
																				}

																			}

																			$new_dob = "";
																			if (isset($dob) && !empty($dob)) {
																				$exp_dob = explode("/", $dob);
																				if (is_array($exp_dob)) {
																					$new_dob = $exp_dob[2] . "-" . $exp_dob[1] . "-" . $exp_dob[0];
																				}
																			}
																			if (!empty($new_dob)) {
																				//echo date("Y-m-d", strtotime($new_dob)) . "<br />";
																				if (isset($checkinsearch) && !empty($checkinsearch)) {
																					$booked_checkin_date = date("Y-m-d", strtotime($checkinsearch));
																				} else {
																					$booked_checkin_date = isset($bed_rental_booked->checkin) ? date("Y-m-d", strtotime($bed_rental_booked->checkin)) : "";
																				}
																				//echo $booked_checkin_date."<br />";
																				$birthdate = new DateTime($new_dob);
																				$today = new DateTime($booked_checkin_date);
																				$age = $birthdate->diff($today)->y;
																				echo $age." Years";
							//                                                                          
																			}
																			$language = "";
																			if (isset($bed_rental_booked->user->user_extrainfo->native_language_speaker) && !empty($bed_rental_booked->user->user_extrainfo->native_language_speaker)) {
																				$language = App\Language::where("id", $bed_rental_booked->user->user_extrainfo->native_language_speaker)->first();
																				$user_language = ($language->language_name)?$language->language_name:'';
																			}

																			echo $name.' : '.$gender.' : '.$age.' Years old'; 
																		}
																	?>
													
										<?php   			}
										?>
														</div>
													</div>
										<?php 
												}else{			
										?>
														<div>
															<div class="margin_top10">
																<h3 class="sub_sec_head">Place available in {{isset($class_room->room_name)?$class_room->room_name:''}} </h3>
															</div>
															<div class="add_new_bed">
																<p>No  places Availble in this room yet</p>

															</div>
														</div>
										<?php 

													
												} ?>
											</div>
											<div class="guest_room_calendar_info">
												<h3>See calendar</h3>													
												<div class="guest_room_other_details" style="display:none;">
													<div class="calendar_for_bed">
														<div class="start_date_box" style="max-width:47%;float:left;">
														<label style="display:block">Check In</label>
														<input type="text"   value="{{$checkinsearch}}"  readonly class="font-size12 form-control margin10 cal startdate" placeholder="DD-MM-YYYY" > 
														</div>
														<div class="end_date_box" style="max-width:47%;float:left;">
														<label style="display:block">Check Out</label>
														<input type="text"   value="{{$checkoutsearch}}"  readonly class="font-size12 form-control margin10 cal enddate" placeholder="DD-MM-YYYY" >  
														</div>
													</div>
													  
												</div> 
											</div>
										</div>  
									<?php 
									}
										if($loged_in_user_id == $propertyObj->user->id){
									?>
											<button class="add_new_classroom" title="Add New Class Room" data-toggle="modal" data-target="#add_classroom_modal">Add new class room</button>
									<?php 
										}
							?>
									
							<?php 
								}else{
									if($loged_in_user_id == $propertyObj->user->id){
								?>
										<button class="add_new_classroom" title="Add New Class Room" data-toggle="modal" data-target="#add_classroom_modal">Add new class room</button>
								<?php 
									}
								}
								
								
								?>     
						</div>


                        <!--activities start here-->


                        <div id="activities_section" class=" margin_top10 col-md-12">
                            <!--- Activity New Modal --->
                            <div class="modal fade " id="add_activity_modal" tabindex="-1" role="dialog" aria-labelledby="add_activity_modal" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="add_activity_modal">Add New Activity </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="dropzone-form-classs-rooms" action="" method="POST" enctype="multipart/form-data">
                                                <div class="row">

                                                    <div class="col-md-6 colx-xs-12">
                                                        <div class="form-group">
                                                            <label for="classroom_title" class="col-form-label">Activity Room Title</label>
                                                            <input type="text" class="form-control" id="new_add_activity_title" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="classroom_title" class="col-form-label">Number of places
                                                                <span class="general_tooltip" >
																	<img src="{{Asset('images/tooltip.png')}}" class="general_tool_tip_icon" />
																	<span class="custom_tooltiptext">how many students you can facilitate</span>
																</span>
                                                            </label>
                                                            <input type="number" class="form-control add_activity_number_places" value="" />
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 col-xs-12">
                                                        <div class="form-group">

                                                            <label for="class_room_currency" class="col-form-label">Select Basic Price Currency</label>

                                                            {!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'add_activity_currency']) !!}
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 col-xs-12">
                                                        <div class="form-group">

                                                            <label for="add_class_room_payment_type" class="col-form-label">Select Payment Type</label>
                                                            <select name="add_class_room_payment_type" class="form-control add_activity_payment_type" required>
                                                                <option value="">Select Payment Type</option>
                                                                <option value="weekly">Weekly</option>
                                                                <option value="fixed">Fixed</option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 colx-xs-12">
                                                        <div class="form-group">
                                                            <label for="class_room_amount" class="col-form-label">Amount</label>
                                                            <input type="number" class="form-control" id="activity_room_amount" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-xs-10">
                                                        <div class="form-group">
                                                            <label for="class_room_currency" class="col-form-label">Expected Cost</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-xs-2">
                                                        <div class="form-group">
                                                            <input type="checkbox" id="expectedCost">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-xs-12 expectedCostSection">
                                                        <div class="form-group">
                                                            <label for="add_class_room_payment_type" class="col-form-label">Expected Cost Amount</label>
                                                            <input type="number" class="form-control" id="expectedCostAmount" />
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 colx-xs-12 expectedCostSection">
                                                        <div class="form-group">
                                                            <label for="class_room_amount" class="col-form-label">Expected Cost Description</label>
                                                            <input type="number" class="form-control" id="expectedCostDescription" />
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-2 colx-xs-12">
                                                        <h5>Facilities</h5>
                                                    </div>
                                                    <div class="col-md-10 colx-xs-12">
                                                        <div class="form-group field-profile-emergencyname classroom_facility">

                                                            <ul class="unstyled centered">
                                                                <?php

                                                                foreach ($classroom_facilities as $facility_id) {
                                                                if($facility_id == 1){
                                                                    $facility_type = 'Computer';
                                                                    $facility_icon = 'Computer.png';
                                                                    $facility_grey_icon = 'Computer-grey.png';
                                                                }else if($facility_id == 2){
                                                                    $facility_type = 'exam-preparation';
                                                                    $facility_icon = 'exam-preparation.png';
                                                                    $facility_grey_icon = 'exam-preparation-grey.png';
                                                                }else if($facility_id == 3){
                                                                    $facility_type = 'lesson-material-books';
                                                                    $facility_icon = 'lesson-material-books.png';
                                                                    $facility_grey_icon = 'lesson-material-books-grey.png';
                                                                }else if($facility_id == 4){
                                                                    $facility_type = 'Projector';
                                                                    $facility_icon = 'Projector.png';
                                                                    $facility_grey_icon = 'Projector-grey.png';
                                                                }else if($facility_id == 5){
                                                                    $facility_type = 'writing-board';
                                                                    $facility_icon = 'writing-board.png';
                                                                    $facility_grey_icon = 'writing-board-grey.png';

                                                                }
                                                                ?>
                                                                <li>
                                                                    <input class="styled-checkbox activity_facility" name="activity_facilities[]" id="activity-checkbox-{{$facility_id}}" type="checkbox" value="{{$facility_id}}">
                                                                    <label for="activity-checkbox-{{$facility_id}}">
                                                                        <img class="grey-icon" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_grey_icon); ?>" alt="{{$facility_type}}" />
                                                                        <img class="orange-icon" style="display:none" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_icon); ?>" alt="{{$facility_type}}" />
                                                                    </label>
                                                                </li>

                                                                <?php 								}
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-2 colx-xs-12">
                                                        <h5>Description</h5>
                                                    </div>
                                                    <div class="col-md-10 colx-xs-12">
                                                        <div class="form-group field-profile-emergencyname room_facility">
                                                            <input type="text" class="form-control" id="new_activity_description" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="activitydropzone" class="dropzone"></div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 70%;">Adding Activity...</span>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary" id="add_new_activity_modal_btn">Add Activity</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--- Activity Edit Modal --->
                            <div class="modal fade " id="edit_activity_modal" tabindex="-1" role="dialog" aria-labelledby="edit_activity_modal" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="">Update Activity Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="update-dropzone-form-activity" action="" method="POST" enctype="multipart/form-data">
                                                <div class="row">
                                                    <input type="hidden" id="update_activity_id" value="" />

                                                    <div class="col-md-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="classroom_title" class="col-form-label">Activity Room Title</label>
                                                            <input type="text" class="form-control " id="update_activity_title" value="" />
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="classroom_title" class="col-form-label">Number of places
                                                                <span class="general_tooltip" >
																	<img src="{{Asset('images/tooltip.png')}}" class="general_tool_tip_icon" />
																	<span class="custom_tooltiptext">how many students you can facilitate</span>
																</span>
                                                            </label>
                                                            <input type="number" class="form-control " id="update_activity_room_number_places" value="" />
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 col-xs-12">
                                                        <div class="form-group">

                                                            <label for="class_room_currency" class="col-form-label">Select Basic Price Currency</label>

                                                            {!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'update_activity_room_currency']) !!}
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 col-xs-12">
                                                        <div class="form-group">

                                                            <label for="add_class_room_payment_type" class="col-form-label">Select Payment Type</label>
                                                            <select name="add_class_room_payment_type" class="form-control update_activity_room_payment_type" required>
                                                                <option value="">Select Payment Type</option>
                                                                <option value="weekly">Weekly</option>
                                                                <option value="fixed">Fixed</option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 colx-xs-12">
                                                        <div class="form-group">
                                                            <label for="class_room_amount" class="col-form-label">Amount</label>
                                                            <input type="number" class="form-control" id="update_activity_room_amount" />
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-3 col-xs-10">
                                                        <div class="form-group">
                                                            <label for="class_room_currency" class="col-form-label">Expected Cost</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-xs-2">
                                                        <div class="form-group">
                                                            <input type="checkbox" id="update_expectedCost">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-xs-12 expectedCostSection">
                                                        <div class="form-group">
                                                            <label for="add_class_room_payment_type" class="col-form-label">Expected Cost Amount</label>
                                                            <input type="number" class="form-control" id="update_cost_amount" />
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 colx-xs-12 expectedCostSection">
                                                        <div class="form-group">
                                                            <label for="class_room_amount" class="col-form-label">Expected Cost Description</label>
                                                            <input type="number" class="form-control" id="update_cost_description" />
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-2 colx-xs-12">
                                                        <h5>Facilities</h5>
                                                    </div>
                                                    <div class="col-md-10 colx-xs-12">
                                                        <div class="form-group field-profile-emergencyname classroom_facility">

                                                            <ul class="unstyled centered">
                                                                <?php

                                                                foreach ($classroom_facilities as $facility_id) {
                                                                if($facility_id == 1){
                                                                    $facility_type = 'Computer';
                                                                    $facility_icon = 'Computer.png';
                                                                    $facility_grey_icon = 'Computer-grey.png';
                                                                }else if($facility_id == 2){
                                                                    $facility_type = 'exam-preparation';
                                                                    $facility_icon = 'exam-preparation.png';
                                                                    $facility_grey_icon = 'exam-preparation-grey.png';
                                                                }else if($facility_id == 3){
                                                                    $facility_type = 'lesson-material-books';
                                                                    $facility_icon = 'lesson-material-books.png';
                                                                    $facility_grey_icon = 'lesson-material-books-grey.png';
                                                                }else if($facility_id == 4){
                                                                    $facility_type = 'Projector';
                                                                    $facility_icon = 'Projector.png';
                                                                    $facility_grey_icon = 'Projector-grey.png';
                                                                }else if($facility_id == 5){
                                                                    $facility_type = 'writing-board';
                                                                    $facility_icon = 'writing-board.png';
                                                                    $facility_grey_icon = 'writing-board-grey.png';

                                                                }
                                                                ?>
                                                                <li>
                                                                    <input class="styled-checkbox update_activity_facility" name="update_activity_facilities[]" id="activity-update_styled-checkbox-{{$facility_id}}" type="checkbox" value="{{$facility_id}}">
                                                                    <label for="activity-update_styled-checkbox-{{$facility_id}}">
                                                                        <img class="grey-icon" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_grey_icon); ?>" alt="{{$facility_type}}" />
                                                                        <img class="orange-icon" style="display:none" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_icon); ?>" alt="{{$facility_type}}" />
                                                                    </label>
                                                                </li>

                                                                <?php 								}
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-2 colx-xs-12">
                                                        <h5>Description</h5>
                                                    </div>
                                                    <div class="col-md-10 colx-xs-12">
                                                        <div class="form-group field-profile-emergencyname ">
                                                            <input type="text" class="form-control " id="update_activity_desc" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="edit_activity_dropzone" class="dropzone"></div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">Processing...</span>
                                            <button class="btn btn-primary delete_activity" title="Delete This Activity" style="float:left;">Delete Activity</button>
                                            <button type="button" class="btn btn-primary" id="update_activity_modal_btn">Update Activity</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
							<div class="modal fade " id="pre_maid_activity_modal" tabindex="-1" role="dialog" aria-labelledby="pre_maid_activity_modal" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="">Activity Details</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<form id="premade-dropzone-form-activity" action="" method="POST" enctype="multipart/form-data">
												<div class="row">
													
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
															<label for="classroom_title" class="col-form-label">Activity Title</label>
															<input type="text" class="form-control " id="premade_activity_title" value="" /> 
															<input type="hidden" name="pre_made_activity_id" id="pre_made_activity_id" value="" />
														</div>

													</div>
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
															<label for="activity_number_places" class="col-form-label">Number of places
																<span class="general_tooltip" >	
																	<img src="{{Asset('images/tooltip.png')}}" class="general_tool_tip_icon" />
																	<span class="custom_tooltiptext">how many students you can facilitate</span>												
																</span>
															</label> 
															<input type="number" class="form-control " id="premade_activity_number_places" value="" /> 
														</div>

													</div>
												</div>
												<div class="row">
													<div class="col-md-4 col-xs-12">
														<div class="form-group"> 

															<label for="pre_made_activity_currency" class="col-form-label">Select Basic Price Currency</label>
															
															{!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'premade_activity_currency']) !!}
														</div>

													</div>
													 <div class="col-md-4 col-xs-12">
														<div class="form-group"> 

															<label for="add_activity_payment_type" class="col-form-label">Select Payment Type</label>
															<select name="add_activity_payment_type" class="form-control premade_activity_payment_type" required>
																<option value="">Select Payment Type</option>
																<option value="weekly">Weekly</option>
																<option value="fixed">Fixed</option>
															</select>
														</div>

													</div>
													<div class="col-md-4 colx-xs-12">
														<div class="form-group">
															 <label for="activity_amount" class="col-form-label">Amount</label>
															<input type="number" class="form-control" id="premade_activity_amount" />
														</div>
													</div>
												   
												   
													
												</div>
												
												<div class="row">
													<div class="col-md-2 colx-xs-12">
														<h5>Possibilities</h5>
													</div>
													<div class="col-md-10 colx-xs-12">
														<div class="form-group field-profile-emergencyname classroom_facility">

															<ul class="unstyled centered">
							<?php
																

																foreach ($classroom_facilities as $facility_id) {
																	if($facility_id == 1){
																		$facility_type = 'Computer';
																		$facility_icon = 'Computer.png';
																		$facility_grey_icon = 'Computer-grey.png';
																	}else if($facility_id == 2){
																		$facility_type = 'exam-preparation';
																		$facility_icon = 'exam-preparation.png';
																		$facility_grey_icon = 'exam-preparation-grey.png';
																	}else if($facility_id == 3){
																		$facility_type = 'lesson-material-books';
																		$facility_icon = 'lesson-material-books.png';
																		$facility_grey_icon = 'lesson-material-books-grey.png';
																	}else if($facility_id == 4){
																		$facility_type = 'Projector';
																		$facility_icon = 'Projector.png';
																		$facility_grey_icon = 'Projector-grey.png';
																	}else if($facility_id == 5){
																		$facility_type = 'writing-board';
																		$facility_icon = 'writing-board.png';
																		$facility_grey_icon = 'writing-board-grey.png';
																	
																	}
							?>
																	<li>  
																		<input class="styled-checkbox premade_activity_facility" name="premade_activity_facility[]" id="premade_activity-checkbox-{{$facility_id}}" type="checkbox" value="{{$facility_id}}">
																		<label for="premade_activity-checkbox-{{$facility_id}}">
																		<img class="grey-icon" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_grey_icon); ?>" alt="{{$facility_type}}" />
																		<img class="orange-icon" style="display:none" src="<?php echo Asset('/frontendassets/images/class-room-facilities/'.$facility_icon); ?>" alt="{{$facility_type}}" />
																		</label>
																	</li>

							<?php 								} 
							?>                                   
															   
															</ul>
														</div>
													</div>
												</div>

											   
												<div class="row">
													<div class="col-md-2 colx-xs-12">
														<h5>Description</h5>
													</div>
													<div class="col-md-10 colx-xs-12">
														<div class="form-group field-profile-emergencyname ">
															<input type="text" class="form-control " id="premade_activity_desc" />
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div id="premade_activity_dropzone" class="dropzone"></div>
													</div>
												</div>
							 
											</form>
										</div>
										<div class="modal-footer">
											<span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">deleting...</span>
											<button type="button" class="btn btn-primary" id="premade_activity_modal_btn">Add Activity</button>
										</div>
									</div>
								</div>
							</div>

                            <div class="margin_top10 class_rooms_sec">
                                <div class="sec_head">
                                    <h3 class="">Activity - Sports & Visiting</h3>
                                </div>
                            </div>
							
							<?php
                                
							if(in_array(1,$pre_made_actitvity_ids)){ ?>
							<div class="activity_item "  id="pre-made-activity-1" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
								<div class="row" id="premade_activity_title">
									<div class="col-xs-8 col-md-7">
										<h4  data-toggle="tooltip" title="First day activity together "> 
										<img src="{{Asset('frontendassets/images/activity/Activity - Sunday together - ask the host.png')}}" class="class_room_type_icon" />
											First day activity together 
										</h4>
									</div>
									<!--col-sm-8 end-->
									<div class="col-xs-4 col-md-5">
										<div class="classroom_action_icons">
											<span class="included">Included</span> 
										</div>
									</div>
								</div>
								<div class="classroom_description">
									<ul class="activity_images">
								
											<li>
												<a href="<?php echo Asset('frontendassets/images/pre-made-classes/School House with 16 to 18.png'); ?>" data-fancybox="images"><img src="<?php echo Asset('frontendassets/images/pre-made-classes/School House with 16 to 18.png'); ?>" /></a>
											</li>
											
									</ul> 
											
									<p class="room_desp" style="word-wrap:break-word;">
										Included in the price the Host will do something with the Student Guest. There are no restrictions not to have the Host do the same things week after week. Typical activities are: day out with the family, visit to town, shopping, cooking..

									</p>
									
								
								</div> 
							</div> 
							<?php 
							}if(!in_array(1,$pre_made_actitvity_ids) && ($loged_in_user_id == $propertyObj->user->id)){ ?>
							<div class="classroom_item "  id="pre-made-class-1" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
								<div class="row" id="classroom_title">
									<div class="col-xs-8 col-md-7">
										<h4  data-toggle="tooltip" title="First day activity together "> 
										<img src="{{Asset('frontendassets/images/activity/Activity - Sunday together - ask the host.png')}}" class="class_room_type_icon" />
											First day activity together 
										</h4>
									</div>
									<!--col-sm-8 end-->
									<div class="col-xs-4 col-md-5">
										<div class="classroom_action_icons">
											<button class="add-pre-made-activity" pre-made="1">Add this Activity</button>
										</div>
									</div>
								</div>
								<div class="classroom_description">
									<ul class="class_room_images">
								
											<li>
												<a href="<?php echo Asset('frontendassets/images/pre-made-classes/School House with 16 to 18.png'); ?>" data-fancybox="images"><img src="<?php echo Asset('frontendassets/images/pre-made-classes/School House with 16 to 18.png'); ?>" /></a>
											</li>
											
									</ul> 
											
									<p class="room_desp" style="word-wrap:break-word;">
										Included in the price the Host will do something with the Student Guest. There are no restrictions not to have the Host do the same things week after week. Typical activities are: day out with the family, visit to town, shopping, cooking..

									</p>
									
								
								</div>
							</div> 
							<?php 
							}
							 if(!in_array(2,$pre_made_actitvity_ids) && ($loged_in_user_id == $propertyObj->user->id)){ ?>
							<div class="classroom_item " id="pre-made-activity-2" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
								<div class="row" id="classroom_title">
									<div class="col-xs-8 col-md-7">
										<h4  data-toggle="tooltip" title="Sports at the local gym, we will book it for you."> 
										<img src="{{Asset('frontendassets/images/activity/(!) Icon cup - Sports activity - available place.png')}}" class="class_room_type_icon" />
											Sports at the local gym, we will book it for you.
										</h4>
									</div>
									<!--col-sm-8 end-->
									<div class="col-xs-4 col-md-5">
										<div class="classroom_action_icons">
											<button class="add-pre-made-activity" pre-made="2" price_type="weekly" amount="0" currency_id="5" room_facilities="1,2,3" seats="1" >Add this Activity</button>
										</div>
									</div>
								</div>
								<div class="classroom_description">
									<ul class="class_room_images">
								
											<li> 
												<a href="<?php echo Asset('frontendassets/images/activity/Sports centre.png'); ?>" data-fancybox="images">
												<img src="<?php echo Asset('frontendassets/images/activity/Sports centre.png'); ?>" />
												</a>
											</li>
											
									</ul> 
									<ul class="classroom_facilities">
										<li>
											Possibilities
										</li>
										<li><span class="facilities activity_possibility-1 active"></span></li>
										<li><span class="facilities activity_possibility-2 active"></span></li>
										<li><span class="facilities activity_possibility-3 active"></span></li>
										<li><span class="facilities activity_possibility-4 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
									</ul>
									<p class="room_desp" style="word-wrap:break-word;">
										You may go to a gym which is only a few minutes by bus from our house, they have facilities and classe, ask via messaging for details. The weekly pass to the gym is specified in the “extra costs” section.

									</p>
									
								
								</div>
							</div> 
							<?php 
							}
							 if(!in_array(3,$pre_made_actitvity_ids) && ($loged_in_user_id == $propertyObj->user->id)){ ?>
							<div class="classroom_item " id="pre-made-activity-3" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
								<div class="row" id="classroom_title">
									<div class="col-xs-8 col-md-7">
										<h4  data-toggle="tooltip" title="Sports at local gym - daily pick up and drop off."> 
										<img src="{{Asset('frontendassets/images/activity/(!) Icon cup - Sports activity - available place.png')}}" class="class_room_type_icon" />
											Sports at local gym - daily pick up and drop off.
										</h4>
									</div>
									<!--col-sm-8 end-->
									<div class="col-xs-4 col-md-5">
										<div class="classroom_action_icons">
											<button class="add-pre-made-activity" pre-made="3"  price_type="weekly" amount="0" room_facilities="1,2,3" currency_id="5" seats="4" >Add this activity</button>
										</div>
									</div>
								</div>
								<div class="classroom_description">
									<ul class="class_room_images">
								
											<li> 
												<a href="<?php echo Asset('frontendassets/images/activity/Sports centre - daily pick up and drop off circle.png'); ?>" data-fancybox="images">
												<img src="<?php echo Asset('frontendassets/images/activity/Sports centre - daily pick up and drop off circle.png'); ?>" />
												</a>
											</li>
											
									</ul> 
									<ul class="classroom_facilities">
										<li>
											Possibilities
										</li>
										<li><span class="facilities activity_possibility-1 active"></span></li>
										<li><span class="facilities activity_possibility-2 active"></span></li>
										<li><span class="facilities activity_possibility-3 active"></span></li>
										<li><span class="facilities activity_possibility-4 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
									</ul>
									<p class="room_desp" style="word-wrap:break-word;">
										We will book the pass and take you to a gym which is only few minutes by car from our house, they have facilities and classes, ask via messaging for details. The weekly pass to the gym is specified in the “extra costs” section. 

									</p>
									
								
								</div>
							</div> 
							<?php  
							} if(!in_array(4,$pre_made_actitvity_ids) && ($loged_in_user_id == $propertyObj->user->id)){ ?>
							<div class="classroom_item " id="pre-made-activity-3" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
								<div class="row" id="classroom_title">
									<div class="col-xs-8 col-md-7">
										<h4  data-toggle="tooltip" title="Golf at local course, we will book it for you."> 
										<img src="{{Asset('frontendassets/images/activity/Activity - sports - golf.png')}}" class="class_room_type_icon" />
											Golf at local course, we will book it for you.
										</h4>
									</div>
									<!--col-sm-8 end-->
									<div class="col-xs-4 col-md-5">
										<div class="classroom_action_icons">
											<button class="add-pre-made-activity" pre-made="3"  price_type="weekly" amount="0" room_facilities="1,2,3" currency_id="5" seats="4" >Add this activity</button>
										</div>
									</div>
								</div>
								<div class="classroom_description">
									<ul class="class_room_images">
								
											<li> 
												<a href="<?php echo Asset('frontendassets/images/activity/Executive Student signup.png'); ?>" data-fancybox="images">
												<img src="<?php echo Asset('frontendassets/images/activity/Executive Student signup.png'); ?>" />
												</a>
											</li>
											
									</ul> 
									<ul class="classroom_facilities">
										<li>
											Possibilities
										</li>
										<li><span class="facilities activity_possibility-1 active"></span></li>
										<li><span class="facilities activity_possibility-2 active"></span></li>
										<li><span class="facilities activity_possibility-3 active"></span></li>
										<li><span class="facilities activity_possibility-4 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
									</ul>
									<p class="room_desp" style="word-wrap:break-word;">
										We will book the pass and take you to a gym which is only few minutes by car from our house, they have facilities and classes, ask via messaging for details. The weekly pass to the gym is specified in the “extra costs” section. 

									</p>
									
								
								</div>
							</div> 
							<?php  
							}if(!in_array(5,$pre_made_actitvity_ids) && ($loged_in_user_id == $propertyObj->user->id)){ ?>
							<div class="classroom_item " id="pre-made-activity-3" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">
								<div class="row" id="classroom_title">
									<div class="col-xs-8 col-md-7">
										<h4  data-toggle="tooltip" title="Visiting with guidance, for 1 to 4 people."> 
										<img src="{{Asset('frontendassets/images/activity/(!) Camera NO MAP.png')}}" class="class_room_type_icon" />
											Visiting with guidance, for 1 to 4 people.
										</h4>
									</div>
									<!--col-sm-8 end-->
									<div class="col-xs-4 col-md-5">
										<div class="classroom_action_icons">
											<button class="add-pre-made-activity" pre-made="3"  price_type="weekly" amount="30" room_facilities="1,2,3" currency_id="5" seats="4" >Add this activity</button>
										</div>
									</div>
								</div>
								<div class="classroom_description">
									<ul class="class_room_images">
								
											<li> 
												<a href="<?php echo Asset('frontendassets/images/activity/Daily visits - Nottingham - Robinhood.png'); ?>" data-fancybox="images">
												<img src="<?php echo Asset('frontendassets/images/activity/Daily visits - Nottingham - Robinhood.png'); ?>" />
												</a>
											</li>
											
									</ul> 
									<ul class="activity_possibilities" style="">
										<li>
											Possibilities
										</li>
										<li><span class="facilities activity_possibility-1 active"></span></li>
										<li><span class="facilities activity_possibility-2 active"></span></li>
										<li><span class="facilities activity_possibility-3 active"></span></li>
										<li><span class="facilities activity_possibility-4 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
										<li><span class="facilities activity_possibility-5 active"></span></li>
									</ul>
									<span class="more_possiblities">...</span>
									<p class="room_desp" style="word-wrap:break-word;clear:both;">
										Every day we will go visiting a local attraction, like the Wollaton park, town centre and other places. You will have to pay for fares and tickets.

									</p>
									
								
								</div>
							</div> 
							<?php  
							}
							?>
							
                            <?php
                            if(isset($userObj->user_guest_rooms) && sizeof($userObj->user_guest_rooms) > 0){

                            foreach($userObj->user_guest_rooms as $gr_key => $class_room){
                            if($loged_in_user_id == $propertyObj->user->id){
                                $class_room_places = \App\Property::where(["user_id" => $class_room->user_id, "bed_guest_room_id" => $class_room->id])
                                    ->where(["property_type" => "24",  "property_status" => "paid"])->get();
                            }else{
                                $class_room_places = \App\Property::where(["user_id" => $class_room->user_id, "bed_guest_room_id" => $class_room->id])
                                    ->where(["property_type" => "24", "status" => "1", "property_status" => "paid"])->get();
                            }
                            $newDateStart = date("Y-m-d 11:00:00", strtotime($checkinsearch));
                            $newDateEnd = date("Y-m-d  10:00:00", strtotime($checkoutsearch));
                            $places_available = false;
                            foreach($class_room_places as $place){
                                $place_id = $place->id;

                                $availableCheck = \App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $place_id);

                                //return $availableCheck;
                                if (count($availableCheck) > 0) {
                                    continue;
                                }else{
                                    // Bed room available so add to cart now
                                    $places_available = true;
                                    break;
                                }

                            }

                            $total_places = count($class_room_places);
                            if($class_room->service_type != 'activity'){
                                continue;
                            }
                            if($loged_in_user_id != $propertyObj->user->id or  Auth::guest()){
                                if($guest_room->is_active == 0 ){
                                    continue;
                                }
                            }

                            if($class_room->pre_made_id != NULL && $class_room->pre_made_id == 1 ){
                                continue;
                            }
                            $guest_room_type = '';
                            foreach($class_types as $class_type_value){
                                if($class_room->room_type_id == $class_type_value->id){
                                    $guest_room_type = $class_type_value->class_type;
                                    break;
                                }
                            }
                            ?>

                            <div class="activity_item " id="activity_index_{{$class_room->id}}" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both;position: relative;display: block;overflow: hidden;padding-bottom: 10px;">
                                <div class="row" id="classroom_title">
                                    <?php
                                    $payment_currency = 5;
                                    $payment_amount = 0;
                                    $payment_type = 'GBP';
                                    if($loged_in_user_id == $propertyObj->user->id){


                                    if($class_room->is_active){

                                    ?>
                                    <div class="col-xs-8 col-md-8">
                                        <h4 title="{{$class_room->room_name}}">
                                            <?php
                                            $total_places = count($class_room_places);
                                            if($total_places > 1 && $total_places <= 4){
                                                $class_type_icon = 'Class for up to 4 students.png';
                                            }else if($total_places > 4){
                                                $class_type_icon = 'Class for more than 4 students.png';
                                            }else{
                                                $class_type_icon = 'Individual lesson class.png';
                                            }
                                            ?>
                                            <img src="{{Asset('frontendassets/images/class-types/'.$class_type_icon)}}" class="class_room_type_icon" />
                                            {{$class_room->room_name}}
                                        </h4>
                                    </div>
                                    <div class="col-xs-3 col-md-3 price_or_status">
                                        <?php
                                        if (sizeof($class_room_places) > 0) {

                                        $place_id = $class_room_places->first()->id;
                                        $pricing = \App\PropertyPricing::where("property_id", $place_id)->first();

                                        if(isset($pricing->payment_type)){

                                        $payment_type = $pricing->payment_type;

                                        $payment_currency = $currencies[$pricing->currency_id];
                                        $payment_amount = $pricing->amount;


                                        ?>

                                        <span class="place_pricing">{{$payment_currency}} {{$payment_amount}}  {{$payment_type}}</span>

                                        <?php
                                        }
                                        }
                                        ?>
                                    </div>
                                    <div class="col-xs-1 col-md-1">
                                        <div class="classroom_action_icons">

                                            <button class="edit_classroom" title="Edit This Activity" payment_currency="{{$payment_currency}}" payment_type="{{$payment_type}}" payment_amount="{{$payment_amount}}" room_facilities="{{$class_room->room_facility}}" data-toggle="modal" data-target="#edit_activity_modal" room_id="{{$class_room->id}}"  room_desc="{{isset($class_room->room_description)?$class_room->room_description:''}}" room_title="{{$class_room->room_name}}" number_places="{{$total_places}}">
                                                <img src="{{asset('frontendassets/images/edit.png')}}" />
                                            </button>
                                        </div>
                                    </div>

                                    <?php

                                    }else{
                                    ?>			<div class="col-xs-6 col-md-7">
                                        <h4 title="{{$class_room->room_name}}">
                                            <?php
                                            if (sizeof($class_room_places) > 0) {

                                                $place_id = $class_room_places->first()->id;
                                                $pricing = \App\PropertyPricing::where("property_id", $place_id)->first();

                                                if(isset($pricing->payment_type)){

                                                    $payment_type = $pricing->payment_type;

                                                    //$payment_currency = $currencies[$pricing->currency_id];
                                                    $payment_amount = $pricing->amount;



                                                }
                                            }
                                            $total_places = count($class_room_places);
                                            if($total_places > 1 && $total_places <= 4){
                                                $class_type_icon = 'Class for up to 4 students.png';
                                            }else if($total_places > 4){
                                                $class_type_icon = 'Class for more than 4 students.png';
                                            }else{
                                                $class_type_icon = 'Individual lesson class.png';
                                            }
                                            ?>
                                            <img src="{{Asset('frontendassets/images/class-types/'.$class_type_icon)}}" class="class_room_type_icon" />
                                            {{$class_room->room_name}}
                                        </h4>
                                    </div>
                                    <div class="col-xs-4 col-md-4 price_or_status">
                                        <span class="pending-status">Pending admin approval</span>
                                    </div>
                                    <div class="col-xs-4 col-md-1">
                                        <div class="classroom_action_icons">

                                            <button class="edit_classroom" title="Edit This Activity" payment_currency="{{$payment_currency}}" payment_type="{{$payment_type}}" payment_amount="{{$payment_amount}}" room_facilities="{{$class_room->room_facility}}" data-toggle="modal" data-target="#edit_activity_modal" room_id="{{$class_room->id}}"  room_desc="{{isset($class_room->room_description)?$class_room->room_description:''}}" room_title="{{$class_room->room_name}}" number_places="{{$total_places}}">
                                                <img src="{{asset('frontendassets/images/edit.png')}}" />
                                            </button>
                                        </div>
                                    </div>
                                    <?php
                                    }

                                    ?>



                                    <?php
                                    }else{
                                    ?>
                                    <div class="col-xs-6 col-md-6">
                                        <h4 title="{{$class_room->room_name}}">
                                            <?php
                                            $total_places = count($class_room_places);
                                            if($total_places > 1 && $total_places <= 4){
                                                $class_type_icon = 'Class for up to 4 students.png';
                                            }else if($total_places > 4){
                                                $class_type_icon = 'Class for more than 4 students.png';
                                            }else{
                                                $class_type_icon = 'Individual lesson class.png';
                                            }
                                            ?>
                                            <img src="{{Asset('frontendassets/images/class-types/'.$class_type_icon)}}" class="class_room_type_icon" />
                                            {{$class_room->room_name}}
                                        </h4>
                                    </div>
                                    <?php
                                    if (sizeof($class_room_places) > 0) {
                                    $place_id = $class_room_places->first()->id;
                                    $pricing = \App\PropertyPricing::where("property_id", $place_id)->first();
                                    if(isset($pricing->payment_type)){
                                    $payment_type = $pricing->payment_type;

                                    $payment_currency = $currencies[$pricing->currency_id];
                                    $payment_amount = $pricing->amount;


                                    ?>
                                    <div class="col-xs-3 col-md-3">
                                        <span class="place_pricing">{{$payment_currency}} {{$payment_amount}}  {{$payment_type}}</span>
                                    </div>
                                    <?php
                                    }
                                    if (Auth::guest()) {
                                    ?>
                                    <div class="col-xs-3 col-md-3">
                                        <div class="classroom_action_icons">
                                            <a href="{{url('signin')}}" type="button" class="guest_room_login_btn">Login</a>
                                        </div>
                                    </div>
                                    <?php
                                    }else{
                                    if($places_available){
                                    ?>
                                    <div class="col-xs-3 col-md-3">
                                        <div class="classroom_action_icons">
                                            <button class="send_bed_request add_classsroom_to_basket" data-room_id="{{$guest_room->id}}" title="Send Request or Add to basket">Add To Basket </button>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    }
                                    }
                                    }
                                    ?>

                                </div>

                                <div class="classroom_description">


                                    <?php
                                    $guest_room_images = \App\GuestRoomImages::where(["room_id" => $class_room->id])->get();
                                    if(count($guest_room_images) > 0){
                                    ?>
                                    <ul class="class_room_images">
                                        <?php
                                        foreach($guest_room_images as $room_image){
                                        $room_image_name = $room_image->img_name;

                                        $img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
                                        $image_size = filesize($img_path);
                                        //$image_size = 78000;
                                        //var_dump($image_size);die;
                                        ?>
                                        <li>
                                            <a href="<?php echo Asset('images/rentals/'.$property_id.'/rooms/'.$room_image_name); ?>" data-fancybox="images">
                                                <img src="<?php echo Asset('images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name); ?>" data-size="<?php  echo $image_size; ?>" /></a>
                                        </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                    <?php 	}else{
                                        echo '<ul class="activity_images"></ul>';
                                    }
                                    ?>
                                    <ul class="classroom_facilities">
                                        <li>
                                            Facilities
                                        </li>
                                        <?php

                                        $facilities = "";
                                        if (isset($class_room->room_facility) && !empty($class_room->room_facility)) {

                                            $facilities = App\UserPaidService::getRoomFacilityNames($class_room->room_facility);
                                        }

                                        if (!empty($facilities)) {
                                            foreach ($facilities as $facility) {
                                                echo '<li><span class="facilities calss_facility-'.$facility->id.' active"></span></li>';
                                            }
                                        }

                                        ?>
                                    </ul>
                                    <p class="room_desp" style="word-wrap:break-word;">
                                        {{isset($class_room->room_description)?$class_room->room_description:''}}
                                    </p>
                                    <?php
                                    $disabledDates = array();

                                    if (sizeof($class_room_places) > 0) {

                                    $place_id = $class_room_places->first()->id;
                                    $pricing = \App\PropertyPricing::where("property_id", $place_id)->first();
                                    //var_dump($currencies);die;

                                    ?>

                                    <div class="classroom_places_sec  classroom_{{$class_room->id}}_places">
                                        <div class="margin_top10" style="position:relative;    border-bottom: 1px solid #ddd;">
                                            <h3 class="sub_sec_head">Places available in this activity </h3>

                                        </div>

                                        <div class="places_box" style="width:100%">

                                            <?php

                                            $disabledDates= array();
                                            if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
                                                $fetchDisabledDates = App\Booking::disabledDatesForProperty($propertyObj->id);
                                                $disabledDates= array();
                                                if(sizeof($fetchDisabledDates) > 0){
                                                    foreach($fetchDisabledDates as $dDate){
                                                        array_push($disabledDates,$dDate->the_date);
                                                    }
                                                }

                                            }

                                            foreach ($class_room_places as $place) {

                                            $bed_rental_booked = App\Property::getBookedPropertyByDates($place->id, $checkinsearch, $checkoutsearch);

                                            $calendarObj = App\Booking::bookingForProperty($place->id);

                                            list($disable_checkinDates, $disable_CheckOutDates) = App\Property::prepareRestrictedDays($calendarObj);
                                            if (!empty($disable_checkinDates) && !empty($disable_CheckOutDates)) {
                                                $checkin_disable_dates = $checkin_disable_dates . $disable_checkinDates; //here values are in this var. but not in rentaldetail view page seen? yes, so for unpaid it has been deleted_ dont hknow because I am checking the script and logic in rental but couldn't found . So we have it here but not in unpaid, let-s add in unpaid again, then, correct_ yes okay I'll add it.good
                                                $checkout_disable_dates = $checkout_disable_dates . $disable_CheckOutDates;
                                            }



                                            ?>
                                            <div class="class_room_place">
                                                <img src="{{Asset('images/class-icon.png')}}" class="class_room_type_icon" />

                                                <div class="custom_tooltip" >
                                                    <img src="{{Asset('images/available-icon.png')}}" class="tool_tip_icon" />
                                                    <span class="custom_tooltiptext">Place is available</span>
                                                </div>

                                            </div>
                                            <?php

                                            if(isset($bed_rental_booked->id) && $bed_rental_booked->id != ''){
                                                if (isset($bed_rental_booked->user->sname16) && !empty($bed_rental_booked->user->sname16)) {
                                                    $dob = isset($bed_rental_booked->user->dob16) ? $bed_rental_booked->user->dob16 : "";
                                                    $gender = isset($bed_rental_booked->user->gender16) ? $bed_rental_booked->user->gender16 : "";
                                                    $gender16 = true;
                                                    //sname16
                                                    $name = isset($bed_rental_booked->user->sname16) ? $bed_rental_booked->user->sname16 : "";
                                                    // about_yorself is correctly "Student Description"
                                                } else {
                                                    $dob = isset($bed_rental_booked->user->dob) ? $bed_rental_booked->user->dob : "";
                                                    $gender = isset($bed_rental_booked->user->gender) ? $bed_rental_booked->user->gender : "";
                                                    $gender16 = false;
                                                    //name
                                                    $name = isset($bed_rental_booked->user->name) ? $bed_rental_booked->user->name : "";
                                                }
                                                if($gender16 == false){
                                                    if($gender == "1"){
                                                        $gender = 'Male';
                                                    }elseif($gender == "2"){
                                                        $gender = 'Female';
                                                    }elseif($gender == "3"){
                                                        $gender = 'Other';
                                                    }else{
                                                        $gender = $gender;
                                                    }

                                                }

                                                $new_dob = "";
                                                if (isset($dob) && !empty($dob)) {
                                                    $exp_dob = explode("/", $dob);
                                                    if (is_array($exp_dob)) {
                                                        $new_dob = $exp_dob[2] . "-" . $exp_dob[1] . "-" . $exp_dob[0];
                                                    }
                                                }
                                                if (!empty($new_dob)) {
                                                    //echo date("Y-m-d", strtotime($new_dob)) . "<br />";
                                                    if (isset($checkinsearch) && !empty($checkinsearch)) {
                                                        $booked_checkin_date = date("Y-m-d", strtotime($checkinsearch));
                                                    } else {
                                                        $booked_checkin_date = isset($bed_rental_booked->checkin) ? date("Y-m-d", strtotime($bed_rental_booked->checkin)) : "";
                                                    }
                                                    //echo $booked_checkin_date."<br />";
                                                    $birthdate = new DateTime($new_dob);
                                                    $today = new DateTime($booked_checkin_date);
                                                    $age = $birthdate->diff($today)->y;
                                                    echo $age." Years";
                                                    //
                                                }
                                                $language = "";
                                                if (isset($bed_rental_booked->user->user_extrainfo->native_language_speaker) && !empty($bed_rental_booked->user->user_extrainfo->native_language_speaker)) {
                                                    $language = App\Language::where("id", $bed_rental_booked->user->user_extrainfo->native_language_speaker)->first();
                                                    $user_language = ($language->language_name)?$language->language_name:'';
                                                }

                                                echo $name.' : '.$gender.' : '.$age.' Years old';
                                            }
                                            ?>

                                            <?php   			}
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                    }else{
                                    ?>
                                    <div>
                                        <div class="margin_top10">
                                            <h3 class="sub_sec_head">Place available in {{isset($class_room->room_name)?$class_room->room_name:''}} </h3>
                                        </div>
                                        <div class="add_new_bed">
                                            <p>No  places Availble in this room yet</p>

                                        </div>
                                    </div>
                                    <?php


                                    } ?>
                                </div>
                                <div class="guest_room_calendar_info">
                                    <h3>See calendar</h3>
                                    <div class="guest_room_other_details" style="display:none;">
                                        <div class="calendar_for_bed">
                                            <div class="start_date_box" style="max-width:47%;float:left;">
                                                <label style="display:block">Check In</label>
                                                <input type="text"   value="{{$checkinsearch}}"  readonly class="font-size12 form-control margin10 cal startdate" placeholder="DD-MM-YYYY" >
                                            </div>
                                            <div class="end_date_box" style="max-width:47%;float:left;">
                                                <label style="display:block">Check Out</label>
                                                <input type="text"   value="{{$checkoutsearch}}"  readonly class="font-size12 form-control margin10 cal enddate" placeholder="DD-MM-YYYY" >
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?php
                            }
                            if($loged_in_user_id == $propertyObj->user->id){
                            ?>
                            <button class="add_new_classroom" title="Add New Activity" data-toggle="modal" data-target="#add_activity_modal">Add new activity</button>
                            <?php
                            }
                            ?>

                            <?php
                            }else{
                            if($loged_in_user_id == $propertyObj->user->id){
                            ?>
                            <button class="add_new_classroom" title="Add New Activity" data-toggle="modal" data-target="#add_activity_modal">Add new activity</button>
                            <?php
                            }
                            }


                            ?>
                        </div>


                        <!--activities end here-->


                        <!--diet section start here-->

                        <div id="diet_section" class=" margin_top10 col-md-12">
                            <!--- Diet New Modal --->
                            <div class="modal fade " id="add_diet_modal" tabindex="-1" role="dialog" aria-labelledby="add_diet_modal" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="add_diet_modal">Add New Diet </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="dropzone-form-classs-rooms" action="" method="POST" enctype="multipart/form-data">
                                                <div class="row">

                                                    <div class="col-md-6 colx-xs-12">
                                                        <div class="form-group">
                                                            <label for="classroom_title" class="col-form-label">Diet Title</label>
                                                            <input type="text" class="form-control" id="new_add_diet_title" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="classroom_title" class="col-form-label">Number of places
                                                                <span class="general_tooltip" >
																	<img src="{{Asset('images/tooltip.png')}}" class="general_tool_tip_icon" />
																	<span class="custom_tooltiptext">how many students you can facilitate</span>
																</span>
                                                            </label>
                                                            <input type="number" class="form-control add_diet_number_places" value="" />
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 col-xs-12">
                                                        <div class="form-group">

                                                            <label for="class_room_currency" class="col-form-label">Select Basic Price Currency</label>

                                                            {!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'add_diet_currency']) !!}
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 col-xs-12">
                                                        <div class="form-group">

                                                            <label for="add_class_room_payment_type" class="col-form-label">Select Payment Type</label>
                                                            <select name="add_diet_payment_type" class="form-control add_diet_payment_type" required>
                                                                <option value="">Select Payment Type</option>
                                                                <option value="weekly">Weekly</option>
                                                                <option value="fixed">Fixed</option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 colx-xs-12">
                                                        <div class="form-group">
                                                            <label for="class_room_amount" class="col-form-label">Amount</label>
                                                            <input type="number" class="form-control" id="diet_amount" />
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-2 colx-xs-12">
                                                        <h5>Facilities</h5>
                                                    </div>
                                                    <div class="col-md-10 colx-xs-12">
                                                        <div class="form-group field-profile-emergencyname classroom_facility">

                                                            <ul class="unstyled centered">
                                                                <?php
                                                                foreach ($room_facilities as $fac) {
                                                                $facility_type = "";
                                                                $facility_id = $fac->id;
                                                                if($facility_id == 11){
                                                                    $facility_type = 'Dairy Free';
                                                                    $facility_icon = 'Dairy Free.png';
                                                                    $facility_grey_icon = 'Dairy Free - grey.png';
                                                                }else if($facility_id == 12){
                                                                    $facility_type = 'Gluten Free';
                                                                    $facility_icon = 'Gluten Free.png';
                                                                    $facility_grey_icon = 'Gluten Free - grey.png';
                                                                }else if($facility_id == 13){
                                                                    $facility_type = 'GMO Free';
                                                                    $facility_icon = 'GMO Free.png';
                                                                    $facility_grey_icon = 'GMO Free - grey.png';
                                                                }else if($facility_id == 14){
                                                                    $facility_type = 'Halal';
                                                                    $facility_icon = 'Halal.png';
                                                                    $facility_grey_icon = 'Halal - grey.png';
                                                                }else if($facility_id == 15){
                                                                    $facility_type = 'Kosher';
                                                                    $facility_icon = 'Kosher.png';
                                                                    $facility_grey_icon = 'Kosher - grey.png';
                                                                }else if($facility_id == 16){
                                                                    $facility_type = 'Low carb';
                                                                    $facility_icon = 'Low carb.png';
                                                                    $facility_grey_icon = 'Low carb - grey.png';
                                                                }else if($facility_id == 17){
                                                                    $facility_type = 'Nut free';
                                                                    $facility_icon = 'Nut free.png';
                                                                    $facility_grey_icon = 'Nut free - grey.png';
                                                                }else if($facility_id == 18){
                                                                    $facility_type = 'Organic _ Bio';
                                                                    $facility_icon = 'Organic _ Bio.png';
                                                                    $facility_grey_icon = 'Organic _ Bio - grey.png';
                                                                }else if($facility_id == 19){
                                                                    $facility_type = 'Sugar Free';
                                                                    $facility_icon = 'Sugar Free.png';
                                                                    $facility_grey_icon = 'Sugar Free - grey.png';
                                                                }else if($facility_id == 20){
                                                                    $facility_type = 'Vegan';
                                                                    $facility_icon = 'Vegan.png';
                                                                    $facility_grey_icon = 'Vegan - grey.png';
                                                                }else if($facility_id == 21){
                                                                    $facility_type = 'Vegetarian';
                                                                    $facility_icon = 'Vegetarian.png';
                                                                    $facility_grey_icon = 'Vegetarian - grey.png';
                                                                }
                                                                if($facility_type != "")
                                                                {
                                                                    ?>
                                                                    <li>
                                                                        <input class="styled-checkbox diet_facility" name="diet_facilities[]" id="diet-checkbox-{{$facility_id}}" type="checkbox" value="{{$facility_id}}">
                                                                        <label for="diet-checkbox-{{$facility_id}}">
                                                                            <img class="grey-icon" src="<?php echo Asset('/frontendassets/images/diet/'.$facility_grey_icon); ?>" alt="{{$facility_type}}" />
                                                                            <img class="orange-icon" style="display:none" src="<?php echo Asset('/frontendassets/images/diet/'.$facility_icon); ?>" alt="{{$facility_type}}" />
                                                                        </label>
                                                                    </li>
                                                                    <?php
                                                                }
                                                                    ?>
                                                                <?php
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-2 colx-xs-12">
                                                        <h5>Description</h5>
                                                    </div>
                                                    <div class="col-md-10 colx-xs-12">
                                                        <div class="form-group field-profile-emergencyname room_facility">
                                                            <input type="text" class="form-control" id="new_diet_description" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="dietdropzone" class="dropzone"></div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 70%;">Adding Diet...</span>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary" id="add_new_diet_modal_btn">Add Diet</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--- Diet Edit Modal --->
                            <div class="modal fade " id="edit_diet_modal" tabindex="-1" role="dialog" aria-labelledby="edit_diet_modal" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="">Update Diet Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="update-dropzone-form-activity" action="" method="POST" enctype="multipart/form-data">
                                                <div class="row">
                                                    <input type="hidden" id="update_diet_id" value="" />

                                                    <div class="col-md-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="classroom_title" class="col-form-label">Diet Title</label>
                                                            <input type="text" class="form-control " id="update_diet_title" value="" />
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="classroom_title" class="col-form-label">Number of places
                                                                <span class="general_tooltip" >
																	<img src="{{Asset('images/tooltip.png')}}" class="general_tool_tip_icon" />
																	<span class="custom_tooltiptext">how many students you can facilitate</span>
																</span>
                                                            </label>
                                                            <input type="number" class="form-control " id="update_diet_number_places" value="" />
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 col-xs-12">
                                                        <div class="form-group">

                                                            <label for="class_room_currency" class="col-form-label">Select Basic Price Currency</label>

                                                            {!! Form::select('currency_select', $currencies, isset($pricing->currency_id)?$pricing->currency_id:'', ['class'=>'airfcfx-timezone-dd form-control listselect selectsze', 'name' => 'currency_select', 'id' => 'update_diet_currency']) !!}
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 col-xs-12">
                                                        <div class="form-group">

                                                            <label for="add_class_room_payment_type" class="col-form-label">Select Payment Type</label>
                                                            <select name="add_class_room_payment_type" class="form-control update_diet_payment_type" required>
                                                                <option value="">Select Payment Type</option>
                                                                <option value="weekly">Weekly</option>
                                                                <option value="fixed">Fixed</option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 colx-xs-12">
                                                        <div class="form-group">
                                                            <label for="class_room_amount" class="col-form-label">Amount</label>
                                                            <input type="number" class="form-control" id="update_diet_amount" />
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-2 colx-xs-12">
                                                        <h5>Facilities</h5>
                                                    </div>
                                                    <div class="col-md-10 colx-xs-12">
                                                        <div class="form-group field-profile-emergencyname classroom_facility">
                                                            <ul class="unstyled centered">
                                                                    <?php
                                                                    foreach ($room_facilities as $fac) {
                                                                    $facility_type = "";
                                                                    $facility_id = $fac->id;
                                                                    if($facility_id == 11){
                                                                        $facility_type = 'Dairy Free';
                                                                        $facility_icon = 'Dairy Free.png';
                                                                        $facility_grey_icon = 'Dairy Free - grey.png';
                                                                    }else if($facility_id == 12){
                                                                        $facility_type = 'Gluten Free';
                                                                        $facility_icon = 'Gluten Free.png';
                                                                        $facility_grey_icon = 'Gluten Free - grey.png';
                                                                    }else if($facility_id == 13){
                                                                        $facility_type = 'GMO Free';
                                                                        $facility_icon = 'GMO Free.png';
                                                                        $facility_grey_icon = 'GMO Free - grey.png';
                                                                    }else if($facility_id == 14){
                                                                        $facility_type = 'Halal';
                                                                        $facility_icon = 'Halal.png';
                                                                        $facility_grey_icon = 'Halal - grey.png';
                                                                    }else if($facility_id == 15){
                                                                        $facility_type = 'Kosher';
                                                                        $facility_icon = 'Kosher.png';
                                                                        $facility_grey_icon = 'Kosher - grey.png';
                                                                    }else if($facility_id == 16){
                                                                        $facility_type = 'Low carb';
                                                                        $facility_icon = 'Low carb.png';
                                                                        $facility_grey_icon = 'Low carb - grey.png';
                                                                    }else if($facility_id == 17){
                                                                        $facility_type = 'Nut free';
                                                                        $facility_icon = 'Nut free.png';
                                                                        $facility_grey_icon = 'Nut free - grey.png';
                                                                    }else if($facility_id == 18){
                                                                        $facility_type = 'Organic _ Bio';
                                                                        $facility_icon = 'Organic _ Bio.png';
                                                                        $facility_grey_icon = 'Organic _ Bio - grey.png';
                                                                    }else if($facility_id == 19){
                                                                        $facility_type = 'Sugar Free';
                                                                        $facility_icon = 'Sugar Free.png';
                                                                        $facility_grey_icon = 'Sugar Free - grey.png';
                                                                    }else if($facility_id == 20){
                                                                        $facility_type = 'Vegan';
                                                                        $facility_icon = 'Vegan.png';
                                                                        $facility_grey_icon = 'Vegan - grey.png';
                                                                    }else if($facility_id == 21){
                                                                        $facility_type = 'Vegetarian';
                                                                        $facility_icon = 'Vegetarian.png';
                                                                        $facility_grey_icon = 'Vegetarian - grey.png';
                                                                    }
                                                                    if($facility_type != "")
                                                                    {
                                                                    ?>
                                                                    <li>
                                                                        <input class="styled-checkbox update_diet_facility" name="update_diet_facilities[]" id="diet-update_styled-checkbox-{{$facility_id}}" type="checkbox" value="{{$facility_id}}">
                                                                        <label for="diet-update_styled-checkbox-{{$facility_id}}">
                                                                            <img class="grey-icon" src="<?php echo Asset('/frontendassets/images/diet/'.$facility_grey_icon); ?>" alt="{{$facility_type}}" />
                                                                            <img class="orange-icon" style="display:none" src="<?php echo Asset('/frontendassets/images/diet/'.$facility_icon); ?>" alt="{{$facility_type}}" />
                                                                        </label>
                                                                    </li>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-2 colx-xs-12">
                                                        <h5>Description</h5>
                                                    </div>
                                                    <div class="col-md-10 colx-xs-12">
                                                        <div class="form-group field-profile-emergencyname ">
                                                            <input type="text" class="form-control " id="update_diet_desc" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="edit_diet_dropzone" class="dropzone"></div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <span class="ajax_processor" style="line-height: 33px;text-align:left; margin-left: 0px;display:none;  width: 55%;">Processing...</span>
                                            <button class="btn btn-primary delete_diet" title="Delete This Diet" style="float:left;">Delete Diet</button>
                                            <button type="button" class="btn btn-primary" id="update_diet_modal_btn">Update Diet</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="margin_top10 class_rooms_sec">
                                <div class="sec_head">
                                    <h3 class="">Diet</h3>
                                </div>
                            </div>

                            <?php
                            if(isset($userObj->user_guest_rooms) && sizeof($userObj->user_guest_rooms) > 0){

                                foreach($userObj->user_guest_rooms as $gr_key => $class_room){
                                    if($loged_in_user_id == $propertyObj->user->id){
                                        $class_room_places = \App\Property::where(["user_id" => $class_room->user_id, "bed_guest_room_id" => $class_room->id])
                                            ->where(["property_type" => "24",  "property_status" => "paid"])->get();
                                    }else{
                                        $class_room_places = \App\Property::where(["user_id" => $class_room->user_id, "bed_guest_room_id" => $class_room->id])
                                            ->where(["property_type" => "24", "status" => "1", "property_status" => "paid"])->get();
                                    }
                                    $newDateStart = date("Y-m-d 11:00:00", strtotime($checkinsearch));
                                    $newDateEnd = date("Y-m-d  10:00:00", strtotime($checkoutsearch));
                                    $places_available = false;
                                    foreach($class_room_places as $place){
                                        $place_id = $place->id;

                                        $availableCheck = \App\Booking::availabilityCheckForProperty($newDateStart, $newDateEnd, $place_id);

                                        //return $availableCheck;
                                        if (count($availableCheck) > 0) {
                                            continue;
                                        }else{
                                            // Bed room available so add to cart now
                                            $places_available = true;
                                            break;
                                        }

                                    }

                                    $total_places = count($class_room_places);
                                    if($class_room->service_type != 'diet'){
                                        continue;
                                    }
                                    if($loged_in_user_id != $propertyObj->user->id or  Auth::guest()){
                                        if($guest_room->is_active == 0 ){
                                            continue;
                                        }
                                    }

                                    if($class_room->pre_made_id != NULL && $class_room->pre_made_id == 1 ){
                                        continue;
                                    }
                                    $guest_room_type = '';
                                    foreach($class_types as $class_type_value){
                                        if($class_room->room_type_id == $class_type_value->id){
                                            $guest_room_type = $class_type_value->class_type;
                                            break;
                                        }
                                    }
                                    ?>

                                    <div class="diet_item " id="diet_index_{{$class_room->id}}" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both;position: relative;display: block;overflow: hidden;padding-bottom: 10px;">
                                        <div class="row" id="classroom_title">
                                            <?php
                                            $payment_currency = 5;
                                            $payment_amount = 0;
                                            $payment_type = 'GBP';
                                            if($loged_in_user_id == $propertyObj->user->id){


                                                if($class_room->is_active){

                                                    ?>
                                                    <div class="col-xs-8 col-md-8">
                                                        <h4 title="{{$class_room->room_name}}">
                                                            <?php
                                                            $total_places = count($class_room_places);
                                                            if($total_places > 1 && $total_places <= 4){
                                                                $class_type_icon = 'Class for up to 4 students.png';
                                                            }else if($total_places > 4){
                                                                $class_type_icon = 'Class for more than 4 students.png';
                                                            }else{
                                                                $class_type_icon = 'Individual lesson class.png';
                                                            }
                                                            ?>
                                                            <img src="{{Asset('frontendassets/images/class-types/'.$class_type_icon)}}" class="class_room_type_icon" />
                                                            {{$class_room->room_name}}
                                                        </h4>
                                                    </div>
                                                    <div class="col-xs-3 col-md-3 price_or_status">
                                                        <?php
                                                        if (sizeof($class_room_places) > 0) {

                                                            $place_id = $class_room_places->first()->id;
                                                            $pricing = \App\PropertyPricing::where("property_id", $place_id)->first();

                                                            if(isset($pricing->payment_type)){

                                                                $payment_type = $pricing->payment_type;

                                                                $payment_currency = $currencies[$pricing->currency_id];
                                                                $payment_amount = $pricing->amount;


                                                                ?>

                                                                <span class="place_pricing">{{$payment_currency}} {{$payment_amount}}  {{$payment_type}}</span>

                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-1 col-md-1">
                                                        <div class="classroom_action_icons">

                                                            <button class="edit_classroom" title="Edit This Diet" payment_currency="{{$payment_currency}}" payment_type="{{$payment_type}}" payment_amount="{{$payment_amount}}" room_facilities="{{$class_room->room_facility}}" data-toggle="modal" data-target="#edit_diet_modal" room_id="{{$class_room->id}}"  room_desc="{{isset($class_room->room_description)?$class_room->room_description:''}}" room_title="{{$class_room->room_name}}" number_places="{{$total_places}}">
                                                                <img src="{{asset('frontendassets/images/edit.png')}}" />
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <?php

                                                }else{
                                                    ?>			<div class="col-xs-6 col-md-7">
                                                        <h4 title="{{$class_room->room_name}}">
                                                            <?php
                                                            if (sizeof($class_room_places) > 0) {

                                                                $place_id = $class_room_places->first()->id;
                                                                $pricing = \App\PropertyPricing::where("property_id", $place_id)->first();

                                                                if(isset($pricing->payment_type)){

                                                                    $payment_type = $pricing->payment_type;

                                                                    //$payment_currency = $currencies[$pricing->currency_id];
                                                                    $payment_amount = $pricing->amount;



                                                                }
                                                            }
                                                            $total_places = count($class_room_places);
                                                            if($total_places > 1 && $total_places <= 4){
                                                                $class_type_icon = 'Class for up to 4 students.png';
                                                            }else if($total_places > 4){
                                                                $class_type_icon = 'Class for more than 4 students.png';
                                                            }else{
                                                                $class_type_icon = 'Individual lesson class.png';
                                                            }
                                                            ?>
                                                            <img src="{{Asset('frontendassets/images/class-types/'.$class_type_icon)}}" class="class_room_type_icon" />
                                                            {{$class_room->room_name}}
                                                        </h4>
                                                    </div>
                                                    <div class="col-xs-4 col-md-4 price_or_status">
                                                        <span class="pending-status">Pending admin approval</span>
                                                    </div>
                                                    <div class="col-xs-4 col-md-1">
                                                        <div class="classroom_action_icons">

                                                            <button class="edit_classroom" title="Edit This Diet" payment_currency="{{$payment_currency}}" payment_type="{{$payment_type}}" payment_amount="{{$payment_amount}}" room_facilities="{{$class_room->room_facility}}" data-toggle="modal" data-target="#edit_diet_modal" room_id="{{$class_room->id}}"  room_desc="{{isset($class_room->room_description)?$class_room->room_description:''}}" room_title="{{$class_room->room_name}}" number_places="{{$total_places}}">
                                                                <img src="{{asset('frontendassets/images/edit.png')}}" />
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }

                                                ?>



                                                <?php
                                            }else{
                                                ?>
                                                <div class="col-xs-6 col-md-6">
                                                    <h4 title="{{$class_room->room_name}}">
                                                        <?php
                                                        $total_places = count($class_room_places);
                                                        if($total_places > 1 && $total_places <= 4){
                                                            $class_type_icon = 'Class for up to 4 students.png';
                                                        }else if($total_places > 4){
                                                            $class_type_icon = 'Class for more than 4 students.png';
                                                        }else{
                                                            $class_type_icon = 'Individual lesson class.png';
                                                        }
                                                        ?>
                                                        <img src="{{Asset('frontendassets/images/class-types/'.$class_type_icon)}}" class="class_room_type_icon" />
                                                        {{$class_room->room_name}}
                                                    </h4>
                                                </div>
                                                <?php
                                                if (sizeof($class_room_places) > 0) {
                                                    $place_id = $class_room_places->first()->id;
                                                    $pricing = \App\PropertyPricing::where("property_id", $place_id)->first();
                                                    if(isset($pricing->payment_type)){
                                                        $payment_type = $pricing->payment_type;

                                                        $payment_currency = $currencies[$pricing->currency_id];
                                                        $payment_amount = $pricing->amount;


                                                        ?>
                                                        <div class="col-xs-3 col-md-3">
                                                            <span class="place_pricing">{{$payment_currency}} {{$payment_amount}}  {{$payment_type}}</span>
                                                        </div>
                                                        <?php
                                                    }
                                                    if (Auth::guest()) {
                                                        ?>
                                                        <div class="col-xs-3 col-md-3">
                                                            <div class="classroom_action_icons">
                                                                <a href="{{url('signin')}}" type="button" class="guest_room_login_btn">Login</a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }else{
                                                        if($places_available){
                                                            ?>
                                                            <div class="col-xs-3 col-md-3">
                                                                <div class="classroom_action_icons">
                                                                    <button class="send_bed_request add_classsroom_to_basket" data-room_id="{{$guest_room->id}}" title="Send Request or Add to basket">Add To Basket </button>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>

                                        </div>

                                        <div class="classroom_description">


                                            <?php
                                            $guest_room_images = \App\GuestRoomImages::where(["room_id" => $class_room->id])->get();
                                            if(count($guest_room_images) > 0){
                                                ?>
                                                <ul class="class_room_images">
                                                    <?php
                                                    foreach($guest_room_images as $room_image){
                                                        $room_image_name = $room_image->img_name;

                                                        $img_path =  public_path() .'/images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name;
                                                        $image_size = filesize($img_path);
                                                        //$image_size = 78000;
                                                        //var_dump($image_size);die;
                                                        ?>
                                                        <li>
                                                            <a href="<?php echo Asset('images/rentals/'.$property_id.'/rooms/'.$room_image_name); ?>" data-fancybox="images">
                                                                <img src="<?php echo Asset('images/rentals/'.$property_id.'/rooms/thumbnail/'.$room_image_name); ?>" data-size="<?php  echo $image_size; ?>" /></a>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>
                                                </ul>
                                            <?php 	}else{
                                                echo '<ul class="diet_images"></ul>';
                                            }
                                            ?>
                                            <ul class="classroom_facilities">
                                                <li>
                                                    Facilities
                                                </li>
                                                <?php
                                                $facilities = "";
                                                if (isset($class_room->room_facility) && !empty($class_room->room_facility)) {

                                                    $facilities = App\UserPaidService::getRoomFacilityNames($class_room->room_facility);
                                                }

                                                if (!empty($facilities)) {
                                                    foreach ($facilities as $facility) {
                                                        echo '<li><span class="facilities calss_facility-'.$facility->id.' active"></span></li>';
                                                    }
                                                }

                                                ?>
                                            </ul>
                                            <p class="room_desp" style="word-wrap:break-word;">
                                                {{isset($class_room->room_description)?$class_room->room_description:''}}
                                            </p>
                                            <?php
                                            $disabledDates = array();

                                            if (sizeof($class_room_places) > 0) {

                                                $place_id = $class_room_places->first()->id;
                                                $pricing = \App\PropertyPricing::where("property_id", $place_id)->first();
                                                //var_dump($currencies);die;

                                                ?>

                                                <div class="classroom_places_sec  classroom_{{$class_room->id}}_places">
                                                    <div class="margin_top10" style="position:relative;    border-bottom: 1px solid #ddd;">
                                                        <h3 class="sub_sec_head">Places available in this activity </h3>

                                                    </div>

                                                    <div class="places_box" style="width:100%">

                                                        <?php

                                                        $disabledDates= array();
                                                        if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){
                                                            $fetchDisabledDates = App\Booking::disabledDatesForProperty($propertyObj->id);
                                                            $disabledDates= array();
                                                            if(sizeof($fetchDisabledDates) > 0){
                                                                foreach($fetchDisabledDates as $dDate){
                                                                    array_push($disabledDates,$dDate->the_date);
                                                                }
                                                            }

                                                        }

                                                        foreach ($class_room_places as $place) {

                                                            $bed_rental_booked = App\Property::getBookedPropertyByDates($place->id, $checkinsearch, $checkoutsearch);

                                                            $calendarObj = App\Booking::bookingForProperty($place->id);

                                                            list($disable_checkinDates, $disable_CheckOutDates) = App\Property::prepareRestrictedDays($calendarObj);
                                                            if (!empty($disable_checkinDates) && !empty($disable_CheckOutDates)) {
                                                                $checkin_disable_dates = $checkin_disable_dates . $disable_checkinDates; //here values are in this var. but not in rentaldetail view page seen? yes, so for unpaid it has been deleted_ dont hknow because I am checking the script and logic in rental but couldn't found . So we have it here but not in unpaid, let-s add in unpaid again, then, correct_ yes okay I'll add it.good
                                                                $checkout_disable_dates = $checkout_disable_dates . $disable_CheckOutDates;
                                                            }



                                                            ?>
                                                            <div class="class_room_place">
                                                                <img src="{{Asset('images/class-icon.png')}}" class="class_room_type_icon" />

                                                                <div class="custom_tooltip" >
                                                                    <img src="{{Asset('images/available-icon.png')}}" class="tool_tip_icon" />
                                                                    <span class="custom_tooltiptext">Place is available</span>
                                                                </div>

                                                            </div>
                                                            <?php

                                                            if(isset($bed_rental_booked->id) && $bed_rental_booked->id != ''){
                                                                if (isset($bed_rental_booked->user->sname16) && !empty($bed_rental_booked->user->sname16)) {
                                                                    $dob = isset($bed_rental_booked->user->dob16) ? $bed_rental_booked->user->dob16 : "";
                                                                    $gender = isset($bed_rental_booked->user->gender16) ? $bed_rental_booked->user->gender16 : "";
                                                                    $gender16 = true;
                                                                    //sname16
                                                                    $name = isset($bed_rental_booked->user->sname16) ? $bed_rental_booked->user->sname16 : "";
                                                                    // about_yorself is correctly "Student Description"
                                                                } else {
                                                                    $dob = isset($bed_rental_booked->user->dob) ? $bed_rental_booked->user->dob : "";
                                                                    $gender = isset($bed_rental_booked->user->gender) ? $bed_rental_booked->user->gender : "";
                                                                    $gender16 = false;
                                                                    //name
                                                                    $name = isset($bed_rental_booked->user->name) ? $bed_rental_booked->user->name : "";
                                                                }
                                                                if($gender16 == false){
                                                                    if($gender == "1"){
                                                                        $gender = 'Male';
                                                                    }elseif($gender == "2"){
                                                                        $gender = 'Female';
                                                                    }elseif($gender == "3"){
                                                                        $gender = 'Other';
                                                                    }else{
                                                                        $gender = $gender;
                                                                    }

                                                                }

                                                                $new_dob = "";
                                                                if (isset($dob) && !empty($dob)) {
                                                                    $exp_dob = explode("/", $dob);
                                                                    if (is_array($exp_dob)) {
                                                                        $new_dob = $exp_dob[2] . "-" . $exp_dob[1] . "-" . $exp_dob[0];
                                                                    }
                                                                }
                                                                if (!empty($new_dob)) {
                                                                    //echo date("Y-m-d", strtotime($new_dob)) . "<br />";
                                                                    if (isset($checkinsearch) && !empty($checkinsearch)) {
                                                                        $booked_checkin_date = date("Y-m-d", strtotime($checkinsearch));
                                                                    } else {
                                                                        $booked_checkin_date = isset($bed_rental_booked->checkin) ? date("Y-m-d", strtotime($bed_rental_booked->checkin)) : "";
                                                                    }
                                                                    //echo $booked_checkin_date."<br />";
                                                                    $birthdate = new DateTime($new_dob);
                                                                    $today = new DateTime($booked_checkin_date);
                                                                    $age = $birthdate->diff($today)->y;
                                                                    echo $age." Years";
                                                                    //
                                                                }
                                                                $language = "";
                                                                if (isset($bed_rental_booked->user->user_extrainfo->native_language_speaker) && !empty($bed_rental_booked->user->user_extrainfo->native_language_speaker)) {
                                                                    $language = App\Language::where("id", $bed_rental_booked->user->user_extrainfo->native_language_speaker)->first();
                                                                    $user_language = ($language->language_name)?$language->language_name:'';
                                                                }

                                                                echo $name.' : '.$gender.' : '.$age.' Years old';
                                                            }
                                                            ?>

                                                        <?php   			}
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                            }else{
                                                ?>
                                                <div>
                                                    <div class="margin_top10">
                                                        <h3 class="sub_sec_head">Place available in {{isset($class_room->room_name)?$class_room->room_name:''}} </h3>
                                                    </div>
                                                    <div class="add_new_bed">
                                                        <p>No  places Availble in this room yet</p>

                                                    </div>
                                                </div>
                                                <?php


                                            } ?>
                                        </div>
                                        <div class="guest_room_calendar_info">
                                            <h3>See calendar</h3>
                                            <div class="guest_room_other_details" style="display:none;">
                                                <div class="calendar_for_bed">
                                                    <div class="start_date_box" style="max-width:47%;float:left;">
                                                        <label style="display:block">Check In</label>
                                                        <input type="text"   value="{{$checkinsearch}}"  readonly class="font-size12 form-control margin10 cal startdate" placeholder="DD-MM-YYYY" >
                                                    </div>
                                                    <div class="end_date_box" style="max-width:47%;float:left;">
                                                        <label style="display:block">Check Out</label>
                                                        <input type="text"   value="{{$checkoutsearch}}"  readonly class="font-size12 form-control margin10 cal enddate" placeholder="DD-MM-YYYY" >
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if($loged_in_user_id == $propertyObj->user->id){
                                    ?>
                                    <button class="add_new_classroom" title="Add New Diet" data-toggle="modal" data-target="#add_diet_modal">Add new Diet</button>
                                    <?php
                                }
                                ?>

                                <?php
                            }else{
                                if($loged_in_user_id == $propertyObj->user->id){
                                    ?>
                                    <button class="add_new_classroom" title="Add New Diet" data-toggle="modal" data-target="#add_diet_modal">Add new Diet</button>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <!--diet section end here-->

						<div id="transfer_section" class=" margin_top10 col-md-12">
							@if(isset($userObj->paid_service->school_house_transfers) && !empty($userObj->paid_service->school_house_transfers))
							<div class="border_bottom margin_top10"></div>

							<div class="margin_top10">
								<h3 class="sub_sec_head">Transfers</h3>
							</div>
							<div class="row" id="description">
								<div class="col-xs-12 col-sm-4">
									<p class=" headtxt text_gray1">Transfers</p>
								</div><!--col-sm-4 end-->
								<div class="col-xs-12 col-sm-8">
									<p class="" style="word-wrap:break-word;">
										<?php
										$transfers = "";
										if (isset($userObj->paid_service->school_house_transfers) && !empty($userObj->paid_service->school_house_transfers)) {
											$transfers = App\UserPaidService::getTransferNames($userObj->paid_service->school_house_transfers);
										}
										if (sizeof($transfers) > 0) {
											foreach ($transfers as $transfer) {
												echo $transfer->transfer_type . ", ";
											}
										}
										?>
									</p>
								</div><!--col-sm-8 end-->
							</div>
							<div class="row" id="description">
								<div class="col-xs-12 col-sm-4">
									<p class=" headtxt text_gray1">Transfer Description</p>
								</div><!--col-sm-4 end-->
								<div class="col-xs-12 col-sm-8">
									<p class="" style="word-wrap:break-word;">
										{{isset($userObj->paid_service->transfer_desc)?$userObj->paid_service->transfer_desc:''}}
									</p>
								</div><!--col-sm-8 end-->
							</div>
							@endif
						
							@if(isset($userObj->paid_service->school_house_tours) && !empty($userObj->paid_service->school_house_tours))
							<div class="border_bottom margin_top10"></div>
							<div class="margin_top10"> 
								<h3  class="sub_sec_head">Tours</h3>
							</div>
							<div class="row" id="description">
								<div class="col-xs-12 col-sm-4">
									<p class=" headtxt text_gray1">Tours</p>
								</div><!--col-sm-4 end-->
								<div class="col-xs-12 col-sm-8">
									<p class="" style="word-wrap:break-word;">
										<?php
										$tours = "";
										if (isset($userObj->paid_service->school_house_tours) && !empty($userObj->paid_service->school_house_tours)) {
											$tours = App\UserPaidService::getTourNames($userObj->paid_service->school_house_tours);
										}
										if (sizeof($tours) > 0) {
											foreach ($tours as $tour) {
												echo $tour->tour_type . ", ";
											}
										}
										?>
									</p>
								</div><!--col-sm-8 end-->
							</div>
							<div class="row" id="description">
								<div class="col-xs-12 col-sm-4">
									<p class=" headtxt text_gray1">Tour Description</p>
								</div><!--col-sm-4 end-->
								<div class="col-xs-12 col-sm-8">
									<p class="" style="word-wrap:break-word;">
										{{isset($userObj->paid_service->tour_desc)?$userObj->paid_service->tour_desc:''}}
									</p>
								</div><!--col-sm-8 end-->
							</div>
							@endif 
						</div> 
						<div id="wow_section" class=" margin_top10 col-md-12">
							@if(isset($userObj->paid_service->school_house_wows) && !empty($userObj->paid_service->school_house_wows))
							<div class="border_bottom margin_top10"></div>
							<div class="margin_top10">
								<h3 class="sub_sec_head">WOW</h3>
							</div>
							<div class="row" id="description">
								<div class="col-xs-12 col-sm-4">
									<p class=" headtxt text_gray1">WOW</p>
								</div><!--col-sm-4 end-->
								<div class="col-xs-12 col-sm-8">
									<p class="" style="word-wrap:break-word;">
										<?php
										$wows = "";
										if (isset($userObj->paid_service->school_house_wows) && !empty($userObj->paid_service->school_house_wows)) {
											$wows = App\UserPaidService::getWowNames($userObj->paid_service->school_house_wows);
										}
										if (sizeof($wows) > 0) {
											foreach ($wows as $wow) {
												echo $wow->wow_type . ", ";
											}
										}
										?>
									</p>
								</div><!--col-sm-8 end-->
							</div>
							<div class="row" id="description">
								<div class="col-xs-12 col-sm-4">
									<p class=" headtxt text_gray1">Wow Description</p>
								</div><!--col-sm-4 end-->
								<div class="col-xs-12 col-sm-8">
									<p class="" style="word-wrap:break-word;">
										{{isset($userObj->paid_service->wow_desc)?$userObj->paid_service->wow_desc:''}}
									</p>
								</div><!--col-sm-8 end-->
							</div>
							@endif
							
						</div>
                        
                        <!--col-sm-4 end-->
                    </div><!--row end-->
                    <div class="border_bottom margin_top10"></div>



                    <!--row end--><span class=" scrollhighlight" id="photo">



                        <link rel="stylesheet" href="{{ asset('frontendassets/css/supersized.css')}}" type="text/css" media="screen" />
                        <link rel="stylesheet" href="{{ asset('frontendassets/css/supersized.shutter.css')}}" type="text/css" media="screen" />
                        <script type="text/javascript" src="{{ asset('frontendassets/js/jquery.easing.min.js')}}"></script>
                        <script type="text/javascript" src="{{ asset('frontendassets/js/supersized.3.2.7.min.js')}}"></script>
                        <script type="text/javascript" src="{{ asset('frontendassets/js/supersized.shutter.min.js')}}"></script>




                </div><!--col-sm-8 end-->
                <!--<div class="col-xs-12 col-sm-12 col-md-4">Right listing section</div>-->
                <div class="col-xs-12 col-sm-12 col-md-4">&nbsp;</div>
            </div><!--row end-->

        </div><!--container end-->
    </div>

    <!-- Review Start -->
    <?php if (isset($propertyObj->reviews) && $propertyObj->reviews->count() > 0) { ?> 
        <div class="bg_white margin_top30 border1">
            }
            <div class="container">
                <h3 class="margin_top20 sec_head">{{$propertyObj->reviews->count()}} Reviews <span id="review"></span></h3>


                <div class="row">
                    <div class="col-xs-12 col-sm-8">

                        <?php
                        foreach ($propertyObj->reviews as $review) {


                            $rimgName = asset('images/noimage/' . $profile_noimage);
                            if ($review->reviewer->profile_img_name != "" && !is_null($review->reviewer->profile_img_name)) {
                                $rimgName = asset('images/profile/' . $review->reviewer->profile_img_name);
                            }
                            ?> 

                            <div class="row">
                                <div class="col-xs-12 col-sm-4 text-center padd_bottom30">
                                    <div class="profile  center-block" style="background-image:url({{$rimgName}});"> </div>
                                    <a class="text_gray1 margin_top10 center-block" href="#">{{$review->reviewer->name}}</a>
                                </div> 

                                <div class="col-xs-12 col-sm-8">
                                    <p>{{$review->description}}</p>
                                    <div class="row ">
                                        <div class="col-xs-12 col-sm-6"><p class="text-muted margin_top10">{{$review->dateAdded}}</p></div>
                                         <!--<div class="col-xs-12 col-sm-6 text-right"><button type="button" class="btn btn-default"> <i class="fa fa-thumbs-o-up"></i> Helpful</button></div>-->
                                    </div> 
                                </div> 
                            </div> 
                        <?php } ?> 

                    </div> 
                </div> 


            </div> 
        </div>  <?php } else { ?>
        <!-- Review End -->

        <div class="bg_white margin_top30 border1">
            <div class="container">
                <h3 class=" margin_bottom10"><b>No reviews yet</b></h3>
                <div class="margin_bottom20">You could give this Service its first review!</div>
            </div>
        </div>

    <?php } ?>


    <div class="container margin_top30 stopfixer">
        <h3><b>About the Member: {{isset($propertyObj->title)?$propertyObj->title:''}} <span id="host" class="scrollhighlight"></span></b></h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12  col-md-8">


                <div class="row">

                    <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="profile margin_top20 center-block" style="background-image:url({{$imgName}});"> </div>
                    </div><!--col-sm-4 end-->

                    <div class="col-xs-12 col-sm-8 col-md-8 xs-center-txt">
                        <p class="margin_top40">{{isset($propertyObj->auto_complete)?$propertyObj->auto_complete:''}}
                            <br />Member since {{isset($propertyObj->user->created_at)?date_format($propertyObj->user->created_at, 'jS F Y'):''}}</p>

                        <?php
                        $ln = "Private";
                        if (isset($propertyObj->legal_nature) && $propertyObj->legal_nature == 1) {
                            $ln = "Public";
                        }
                        ?>

                    </div>

                    <!--col-sm-4 end-->

                </div><!--row end-->



                <div class="row hiddencls">

                    <div class="col-xs-12 col-sm-4">
                        <p class="margin_top30">Trust</p>
                    </div><!--col-sm-4 end-->

                    <div class="col-xs-12 col-sm-8">

                        <button type="button" class="btn btn-warning margin_top20"> 1</button>
                        <p>Review</p>
                    </div><!--col-sm-4 end-->

                </div><!--row end-->

            </div><!--col-sm-8 end--></div><!--row end-->


        <!--iframe src="https://www.google.com/maps/@11.0136259,76.8978671,14.25z?hl=en-US"  width="100%" 
        height="450" frameborder="0" style="border:0; " class="margin_top30 map"  ></iframe-->
        <span id="location" class="scrollhighlight"></span>
        <div class="map margin_bottom30">
            <span class="map1"></span>
            <!--span id="location"></span-->
            <div id="map_canvas" frameborder="0" style="border:0; " class="margin_top30 map">



            </div>



        </div>


        <h3 class="margin_top30 hiddencls">Explore other options in and around</h3>

        <p class="margin_bottom30 hiddencls">More Services to see<br /><br />
        </p>



    </div><!--container end-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog listpopupwidth" role="document">
            <div class="modal-content">

                <div class="modal-body padding0">
                    <div class="toplistdiv" style="display:none;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>   
                        <h3>Save to Wish List</h3>          
                        <hr />
                    </div>

                    <div class="airfcfx-leftlistdiv leftlistdiv">
                        <div class="banner2 banner2hgt"  id="listimage"></div>
                    </div>
                    <div class="airfcfx-rightlistdiv-cnt">
                        <div class="airfcfx-rightlistdiv rightlistdiv padding20 wishlisthgt">

                            <div class="airfcfx-topfullviewdiv topfullviewdiv">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
                                <h3>Save to Wish List</h3><hr />
                            </div>

                            <div class="airfcfx-wishlist-contianer" id="listnames"> 

                            </div>
                        </div>
                        <div class="airfcfx-wish-createlist-cnt">
                            <input type="text" id="newlistname1" class="airfcfx-listtxt listtxt" value="" placeholder="Create New List" maxlength="20">
                            <input type="button" value="create" class="airfcfx-createbtn btn btn-danger createbtn" onclick="create_new_list();">
                        </div> 
                        <div class="airfcfx-wishlist-btn-cnt">
                            <input type="button" value="Cancel" class="airfcfx-cancelsze btn btn_email cancelsze cancelbtn " data-dismiss="modal">
                            <input type="button" value="Save" data-dismiss="modal" class="airfcfx-savebtn btn btn-primary savebtn pull-right" onclick="save_lists();">
                            <div class="errcls listerr"></div>
                        </div>
                    </div> 

                </div>
                <div class="clear">

                </div>            
            </div>
        </div>
    </div>

    <div class="modal" id="contactform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no_border">
                    <button type="button" class="close" data-dismiss="modal" onclick="checkitclear();" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
                </div>
                <div class="modal-body">

                    <div class="col-xs-12 contact-txtbox-align">
                        <h3>Contact Provider</h3>
                        <p>Write your message:</p>
                    </div>
                    <div class="col-xs-12 contact-txtbox-align margin_top20">
                        <textarea id="contactmessage" style="resize:none" cols="50" rows="5" class="form-control" maxlength="250"></textarea>
                        <div id="charNum"></div>
                    </div>
                </div>
                <input type="hidden" id="userid" value="4">
                <input type="hidden" id="hostid" value="4">
                <input type="hidden" id="listingid" value="893">
                <div class="clear"></div><br />
                <div class="modal-footer">
                    <input type="button" id="send_msg" value="Send Message" class="btn btn-danger" onclick="send_contact_message();">
                    <img id="loadingimg" src="/images/load.gif" class="loading" style="margin-top:-1px;">        <div id="succmsg" class="successtxt hiddencls">Message Sent Successfully</div><br/>
                    <div class="msgerrcls"></div>

                </div>
            </div>
            <div class="clear">
            </div>            
        </div>
    </div>  

    <script src="{{Asset('fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{Asset('frontendassets/js/dropzone.js')}}"></script>
    <script type="text/javascript">
	
	$(document).ready(function () {
		if($(".activity_possibilities").last().find('li').length > 7 ){
			var total_possibilities = $(".activity_possibilities").last().find('li').length;
			var i = 9;
			while(i <= total_possibilities){
			  $(".activity_possibilities").last().find('li:nth-child('+i+')').hide();
			  i++;
			}
		}
		
		$(".more_possiblities").on('click',function(){
		   var total_possibilities = $(".activity_possibilities li").length;
		   if(total_possibilities > 7){
				var c = 1;
				while(c <= total_possibilities){
					if($(".activity_possibilities li:nth-child("+c+")").is(":visible")){
						$(".activity_possibilities li:nth-child("+c+")").hide();
						$(".activity_possibilities li:nth-child("+(c+7)+")").show();   
						c = 100;             
					}
					c++;            
				}
		   }
		});
		
		
		
		$(document).on("scroll", onScroll);
		
		//smoothscroll
		$('a[href^="#"]').on('click', function (e) {
			e.preventDefault();
			$(document).off("scroll");
			
			$('a').each(function () {
				$(this).removeClass('active');
			})
			$(this).addClass('active');
		  
			var target = this.hash,
				menu = target;
			$target = $(target);
			$('html, body').stop().animate({
				'scrollTop': $target.offset().top+2
			}, 500, 'swing', function () {
				window.location.hash = target;
				$(document).on("scroll", onScroll);
			});
		});
	});

	function onScroll(event){
		var scrollPos = $(document).scrollTop();
		//if($(".list-inline").is(":visible")){
			$('.list-inline a').each(function () {
				var currLink = $(this);
				var refElement = $(currLink.attr("href"));
				
				if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
					//$('.list-inline ul li a').removeClass("active");
					currLink.addClass("active");
					if(currLink.attr("href") == '#school_house_included_services'){
						$(".service_list").show();
						$(".list-inline").hide();
					}else{
						$(".service_list").hide();
						$(".list-inline").show();
					}
				}else{
					currLink.removeClass("active");
				}
			
				
			});
		//}
		
		//if($(".service_list").is(":visible")){
			$('.service_list a').each(function () {
				var currLink = $(this);
				var refElement = $(currLink.attr("href"));
				
				if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
					$('.service_list ul li a').removeClass("active");
					currLink.addClass("active");
				}else{
					currLink.removeClass("active");
				}				
				
			});
		//}
		
	}
	
	$(document).on("click",".guest_room_calendar_info h3",function(){
		$(this).toggleClass("active");
		$(this).parent().find(".guest_room_other_details").toggle();
	});  
	 
	
	$('input[name="room_facilities[]"]').click(function(e){
		if ($(this).prop('checked')==true){ 
			$(this).closest("li").find(".grey-icon").hide();
			$(this).closest("li").find(".orange-icon").show();
		}else{
			$(this).closest("li").find(".grey-icon").show();
			$(this).closest("li").find(".orange-icon").hide();  
		}
	});
	$('input[name="update_room_facility[]"]').click(function(e){
		if ($(this).prop('checked')==true){ 
			$(this).closest("li").find(".grey-icon").hide();
			$(this).closest("li").find(".orange-icon").show();
		}else{
			$(this).closest("li").find(".grey-icon").show();
			$(this).closest("li").find(".orange-icon").hide();  
		}
	});  
	
	$('input[name="premade_classroom_facility[]"]').click(function(e){ 
		
		if ($(this).prop('checked')==true){ 
			$(this).closest("li").find(".grey-icon").hide();
			$(this).closest("li").find(".orange-icon").show();
		}else{
			$(this).closest("li").find(".grey-icon").show();
			$(this).closest("li").find(".orange-icon").hide();  
		}
	});
	
	$('input[name="update_classroom_facilities[]"]').click(function(e){ 
		
		if ($(this).prop('checked')==true){ 
			$(this).closest("li").find(".grey-icon").hide();
			$(this).closest("li").find(".orange-icon").show();
		}else{
			$(this).closest("li").find(".grey-icon").show();
			$(this).closest("li").find(".orange-icon").hide();  
		}
	});
	
	$('input[name="classroom_facilities[]"]').click(function(e){ 
		
		if ($(this).prop('checked')==true){ 
			$(this).closest("li").find(".grey-icon").hide();
			$(this).closest("li").find(".orange-icon").show();
		}else{
			$(this).closest("li").find(".grey-icon").show();
			$(this).closest("li").find(".orange-icon").hide();  
		}
	});

    $('input[name="activity_facilities[]"]').click(function(e){

        if ($(this).prop('checked')==true){
            $(this).closest("li").find(".grey-icon").hide();
            $(this).closest("li").find(".orange-icon").show();
        }else{
            $(this).closest("li").find(".grey-icon").show();
            $(this).closest("li").find(".orange-icon").hide();
        }
    });
    $('input[name="diet_facilities[]"]').click(function(e){

        if ($(this).prop('checked')==true){
            $(this).closest("li").find(".grey-icon").hide();
            $(this).closest("li").find(".orange-icon").show();
        }else{
            $(this).closest("li").find(".grey-icon").show();
            $(this).closest("li").find(".orange-icon").hide();
        }
    });
    $('input[name="update_activity_facilities[]"]').click(function(e){

        if ($(this).prop('checked')==true){
            $(this).closest("li").find(".grey-icon").hide();
            $(this).closest("li").find(".orange-icon").show();
        }else{
            $(this).closest("li").find(".grey-icon").show();
            $(this).closest("li").find(".orange-icon").hide();
        }
    });
    $('input[name="update_diet_facilities[]"]').click(function(e){

        if ($(this).prop('checked')==true){
            $(this).closest("li").find(".grey-icon").hide();
            $(this).closest("li").find(".orange-icon").show();
        }else{
            $(this).closest("li").find(".grey-icon").show();
            $(this).closest("li").find(".orange-icon").hide();
        }
    });

	
	 
	$(".add_room_bed_btn").click(function(){

	   var new_bed_elem =  '<div class="new_beds_for_guest_room" style="">\
									<div class="form-group">\
										<label for="edit_bed_type" class="col-form-label">Select Bed Type:<span class="remove-bed-sec">-</span></label>\
										<select name="add_new_bed_type_id[]" class="form-control " required="">\
											<option value="">Select Bed Type</option>\
											<option value="1">Single</option>\
											<option value="2">Double</option>\
											<option value="3">King size</option>\
											<option value="4">Queen size</option>\
											<option value="5">Bunk bed</option>\
											<option value="6">Futon</option>\
											<option value="7">Other</option>\
										</select> \
									</div>\
								</div>';  
	   $(".room_beds_for_update").append(new_bed_elem);  
	   return false;
	}); 
	
	var slideIndex = 1;
	function plusSlides(n,room_slides) {
	  showSlides(slideIndex += n,room_slides);
	}

	function currentSlide(n) {
	  showSlides(slideIndex = n);
	}

	function showSlides(n,room_slides) {
	
	  var ccc = '"'+room_slides+'"';
	  	console.log(room_slides);
	  var i;
	  
	  var slides = document.getElementsByClassName(room_slides); 
	  console.log(slides);
	  if (n > slides.length) {slideIndex = 1}    
	  if (n < 1) {slideIndex = slides.length}
	  for (i = 0; i < slides.length; i++) {
		  slides[i].style.display = "none";  
	  }
	  
	  slides[slideIndex-1].style.display = "block";  
	}
	
		Dropzone.autoDiscover = false;

		var myDropzone = new Dropzone("#dropzone", {
			url: baseurl + '/rentals/uploadroomimages', 
			method: "POST",
			paramName: "file",
			autoProcessQueue : true,
			acceptedFiles: "image/*",
			maxFiles: 5,
			maxFilesize: 20, // MB
			uploadMultiple: true,
			parallelUploads: 100, // use it with uploadMultiple
			createImageThumbnails: true,
			thumbnailWidth: 120,
			thumbnailHeight: 95,
			addRemoveLinks: true,
			timeout: 180000,
			dictRemoveFileConfirmation: "Are you Sure?", // ask before removing file
			// Language Strings
			dictFileTooBig: "File is to big  Max allowed file size is 20MB",
			dictInvalidFileType: "Invalid File Type",
			dictCancelUpload: "Cancel",
			dictRemoveFile: "Remove",
			dictMaxFilesExceeded: "Only 5 images are allowed",
			dictDefaultMessage: "Drop room images here to upload",
		});

		myDropzone.on("addedfile", function(file) {
			//console.log(file);
		});

		myDropzone.on("removedfile", function(file) {
			// console.log(file);
		});

		// Add mmore data to send along with the file as POST data. (optional)
		myDropzone.on("sending", function(file, xhr, formData) {
			formData.append("listingid", $("input[name='listingid']").val()); // $_POST["dropzone"]
		});

		myDropzone.on("error", function(file, response) {
			console.log(response);
		});

		// on success
		myDropzone.on("successmultiple", function(file, response) {
			// get response from successful ajax request
			console.log(response);
			// submit the form after images upload
			// (if u want yo submit rest of the inputs in the form)
			//document.getElementById("dropzone-form").submit();
		});
		
		var classroomdropzone = new Dropzone("#classroomdropzone", {
			url: baseurl + '/rentals/uploadclassroomimages', 
			method: "POST",
			paramName: "file",
			autoProcessQueue : true,
			acceptedFiles: "image/*",
			maxFiles: 5,
			maxFilesize: 20, // MB
			uploadMultiple: true,
			parallelUploads: 100, // use it with uploadMultiple
			createImageThumbnails: true,
			thumbnailWidth: 120,
			thumbnailHeight: 95,
			addRemoveLinks: true,
			timeout: 180000,
			dictRemoveFileConfirmation: "Are you Sure?", // ask before removing file
			// Language Strings
			dictFileTooBig: "File is to big  Max allowed file size is 20MB",
			dictInvalidFileType: "Invalid File Type",
			dictCancelUpload: "Cancel",
			dictRemoveFile: "Remove",
			dictMaxFilesExceeded: "Only 5 images are allowed",
			dictDefaultMessage: "Drop room images here to upload",
		});

		classroomdropzone.on("addedfile", function(file) {
			//console.log(file);
		});

		classroomdropzone.on("removedfile", function(file) {
			// console.log(file);
		});

		// Add mmore data to send along with the file as POST data. (optional)
		classroomdropzone.on("sending", function(file, xhr, formData) {
			formData.append("listingid", $("input[name='listingid']").val()); // $_POST["dropzone"]
		});

		classroomdropzone.on("error", function(file, response) {
			console.log(response);
		});

		// on success
		classroomdropzone.on("successmultiple", function(file, response) {
			// get response from successful ajax request
			console.log(response);
			// submit the form after images upload
			// (if u want yo submit rest of the inputs in the form)
			//document.getElementById("dropzone-form").submit();
		});

    <!-- Activity dropzone -->
    var activitydropzone = new Dropzone("#activitydropzone", {
        url: baseurl + '/rentals/uploadactivityimages',
        method: "POST",
        paramName: "file",
        autoProcessQueue : true,
        acceptedFiles: "image/*",
        maxFiles: 5,
        maxFilesize: 20, // MB
        uploadMultiple: true,
        parallelUploads: 100, // use it with uploadMultiple
        createImageThumbnails: true,
        thumbnailWidth: 120,
        thumbnailHeight: 95,
        addRemoveLinks: true,
        timeout: 180000,
        dictRemoveFileConfirmation: "Are you Sure?", // ask before removing file
        // Language Strings
        dictFileTooBig: "File is to big  Max allowed file size is 20MB",
        dictInvalidFileType: "Invalid File Type",
        dictCancelUpload: "Cancel",
        dictRemoveFile: "Remove",
        dictMaxFilesExceeded: "Only 5 images are allowed",
        dictDefaultMessage: "Drop room images here to upload",
    });

    activitydropzone.on("addedfile", function(file) {
        //console.log(file);
    });

    activitydropzone.on("removedfile", function(file) {
        // console.log(file);
    });

    // Add mmore data to send along with the file as POST data. (optional)
    activitydropzone.on("sending", function(file, xhr, formData) {
        formData.append("listingid", $("input[name='listingid']").val()); // $_POST["dropzone"]
    });

    activitydropzone.on("error", function(file, response) {
        console.log(response);
    });

    // on success
    activitydropzone.on("successmultiple", function(file, response) {
        // get response from successful ajax request
        console.log(response);
        // submit the form after images upload
        // (if u want yo submit rest of the inputs in the form)
        //document.getElementById("dropzone-form").submit();
    });


    var activityEditDropZone = new Dropzone("#edit_activity_dropzone", {
        url: baseurl + '/rentals/uploadactivityimages',
        method: "POST",
        paramName: "file",
        autoProcessQueue : true,
        acceptedFiles: "image/*",
        maxFiles: 5,
        maxFilesize: 20, // MB
        uploadMultiple: true,
        parallelUploads: 100, // use it with uploadMultiple
        createImageThumbnails: true,
        thumbnailWidth: 120,
        thumbnailHeight: 95,
        addRemoveLinks: true,
        timeout: 180000,
        dictRemoveFileConfirmation: "Are you Sure?", // ask before removing file
        // Language Strings
        dictFileTooBig: "File is to big  Max allowed file size is 20MB",
        dictInvalidFileType: "Invalid File Type",
        dictCancelUpload: "Cancel",
        dictRemoveFile: "Remove",
        dictMaxFilesExceeded: "Only 5 images are allowed",
        dictDefaultMessage: "Drop room images here to upload",
    });


    activityEditDropZone.on("addedfile", function(file) {
        //console.log(file);
    });

    activityEditDropZone.on("removedfile", function(file) {
        // console.log(file);
    });

    // Add mmore data to send along with the file as POST data. (optional)
    activityEditDropZone.on("sending", function(file, xhr, formData) {
        formData.append("listingid", $("input[name='listingid']").val()); // $_POST["dropzone"]
    });

    activityEditDropZone.on("error", function(file, response) {
        console.log(response);
    });

    // on success
    activityEditDropZone.on("successmultiple", function(file, response) {
        // get response from successful ajax request
        console.log(response);
        // submit the form after images upload
        // (if u want yo submit rest of the inputs in the form)
        //document.getElementById("dropzone-form").submit();
    });



    <!-- Activity dropzone ends-->

    <!-- Diet dropzone starts-->

    var dietDropZone = new Dropzone("#dietdropzone", {
        url: baseurl + '/rentals/uploaddietimages',
        method: "POST",
        paramName: "file",
        autoProcessQueue : true,
        acceptedFiles: "image/*",
        maxFiles: 5,
        maxFilesize: 20, // MB
        uploadMultiple: true,
        parallelUploads: 100, // use it with uploadMultiple
        createImageThumbnails: true,
        thumbnailWidth: 120,
        thumbnailHeight: 95,
        addRemoveLinks: true,
        timeout: 180000,
        dictRemoveFileConfirmation: "Are you Sure?", // ask before removing file
        // Language Strings
        dictFileTooBig: "File is to big  Max allowed file size is 20MB",
        dictInvalidFileType: "Invalid File Type",
        dictCancelUpload: "Cancel",
        dictRemoveFile: "Remove",
        dictMaxFilesExceeded: "Only 5 images are allowed",
        dictDefaultMessage: "Drop room images here to upload",
    });

    dietDropZone.on("addedfile", function(file) {
        //console.log(file);
    });

    dietDropZone.on("removedfile", function(file) {
        // console.log(file);
    });

    // Add mmore data to send along with the file as POST data. (optional)
    dietDropZone.on("sending", function(file, xhr, formData) {
        formData.append("listingid", $("input[name='listingid']").val()); // $_POST["dropzone"]
    });

    dietDropZone.on("error", function(file, response) {
        console.log(response);
    });

    // on success
    dietDropZone.on("successmultiple", function(file, response) {
        // get response from successful ajax request
        console.log(response);
        // submit the form after images upload
        // (if u want yo submit rest of the inputs in the form)
        //document.getElementById("dropzone-form").submit();
    });


    var dietEditDropZone = new Dropzone("#edit_diet_dropzone", {
        url: baseurl + '/rentals/uploaddietimages',
        method: "POST",
        paramName: "file",
        autoProcessQueue : true,
        acceptedFiles: "image/*",
        maxFiles: 5,
        maxFilesize: 20, // MB
        uploadMultiple: true,
        parallelUploads: 100, // use it with uploadMultiple
        createImageThumbnails: true,
        thumbnailWidth: 120,
        thumbnailHeight: 95,
        addRemoveLinks: true,
        timeout: 180000,
        dictRemoveFileConfirmation: "Are you Sure?", // ask before removing file
        // Language Strings
        dictFileTooBig: "File is to big  Max allowed file size is 20MB",
        dictInvalidFileType: "Invalid File Type",
        dictCancelUpload: "Cancel",
        dictRemoveFile: "Remove",
        dictMaxFilesExceeded: "Only 5 images are allowed",
        dictDefaultMessage: "Drop room images here to upload",
    });


    dietEditDropZone.on("addedfile", function(file) {
        //console.log(file);
    });

    dietEditDropZone.on("removedfile", function(file) {
        // console.log(file);
    });

    // Add mmore data to send along with the file as POST data. (optional)
    dietEditDropZone.on("sending", function(file, xhr, formData) {
        formData.append("listingid", $("input[name='listingid']").val()); // $_POST["dropzone"]
    });

    dietEditDropZone.on("error", function(file, response) {
        console.log(response);
    });

    // on success
    dietEditDropZone.on("successmultiple", function(file, response) {
        // get response from successful ajax request
        console.log(response);
        // submit the form after images upload
        // (if u want yo submit rest of the inputs in the form)
        //document.getElementById("dropzone-form").submit();
    });

    <!-- Diet dropzone ends-->




		var roomEditDropzone = new Dropzone("#edit_room_dropzone", {
			url: baseurl + '/rentals/uploadroomimages', 
			method: "POST",
			paramName: "file",
			autoProcessQueue : true,
			acceptedFiles: "image/*",
			maxFiles: 5,
			maxFilesize: 20, // MB
			uploadMultiple: true,
			parallelUploads: 100, // use it with uploadMultiple
			createImageThumbnails: true,
			thumbnailWidth: 120,
			thumbnailHeight: 95,
			addRemoveLinks: true,
			timeout: 180000,
			dictRemoveFileConfirmation: "Are you Sure?", // ask before removing file
			// Language Strings
			dictFileTooBig: "File is to big  Max allowed file size is 20MB",
			dictInvalidFileType: "Invalid File Type",
			dictCancelUpload: "Cancel",
			dictRemoveFile: "Remove",
			dictMaxFilesExceeded: "Only 5 images are allowed",
			dictDefaultMessage: "Drop room images here to upload",
		});
		
		
		roomEditDropzone.on("addedfile", function(file) {
			//console.log(file);
		});

		roomEditDropzone.on("removedfile", function(file) {
			// console.log(file);
		});

		// Add mmore data to send along with the file as POST data. (optional)
		roomEditDropzone.on("sending", function(file, xhr, formData) {
			formData.append("listingid", $("input[name='listingid']").val()); // $_POST["dropzone"]
		});

		roomEditDropzone.on("error", function(file, response) {
			console.log(response);
		});

		// on success
		roomEditDropzone.on("successmultiple", function(file, response) {
			// get response from successful ajax request
			console.log(response);
			// submit the form after images upload
			// (if u want yo submit rest of the inputs in the form)
			//document.getElementById("dropzone-form").submit();
		});
		
		var classroomEditDropzone = new Dropzone("#edit_classroom_dropzone", {
			url: baseurl + '/rentals/uploadclassroomimages', 
			method: "POST",
			paramName: "file",
			autoProcessQueue : true,
			acceptedFiles: "image/*",
			maxFiles: 5,
			maxFilesize: 20, // MB
			uploadMultiple: true,
			parallelUploads: 100, // use it with uploadMultiple
			createImageThumbnails: true,
			thumbnailWidth: 120,
			thumbnailHeight: 95,
			addRemoveLinks: true,
			timeout: 180000,
			dictRemoveFileConfirmation: "Are you Sure?", // ask before removing file
			// Language Strings
			dictFileTooBig: "File is to big  Max allowed file size is 20MB",
			dictInvalidFileType: "Invalid File Type",
			dictCancelUpload: "Cancel",
			dictRemoveFile: "Remove",
			dictMaxFilesExceeded: "Only 5 images are allowed",
			dictDefaultMessage: "Drop room images here to upload",
		});
		
		
		classroomEditDropzone.on("addedfile", function(file) {
			//console.log(file);
		});

		classroomEditDropzone.on("removedfile", function(file) {
			// console.log(file);
		});

		// Add mmore data to send along with the file as POST data. (optional)
		classroomEditDropzone.on("sending", function(file, xhr, formData) {
			formData.append("listingid", $("input[name='listingid']").val()); // $_POST["dropzone"]
		});

		classroomEditDropzone.on("error", function(file, response) {
			console.log(response);
		});

		// on success
		classroomEditDropzone.on("successmultiple", function(file, response) {
			// get response from successful ajax request
			console.log(response);
			// submit the form after images upload
			// (if u want yo submit rest of the inputs in the form)
			//document.getElementById("dropzone-form").submit();
		});


		function checkitclear(){
		//$('#contactmessage').val('');
		$('.msgerrcls').val('');
		$('.msgerrcls').hide();
		}
		
		
		var premadeclassroomEditDropzone = new Dropzone("#premade_classroom_dropzone", {
			url: baseurl + '/rentals/uploadclassroomimages', 
			method: "POST",
			paramName: "file",
			autoProcessQueue : true,
			acceptedFiles: "image/*",
			maxFiles: 5,
			maxFilesize: 20, // MB
			uploadMultiple: true,
			parallelUploads: 100, // use it with uploadMultiple
			//createImageThumbnails: true,
			thumbnailWidth: 120,
			thumbnailHeight: 95,
			addRemoveLinks: true,
			timeout: 180000,
			dictRemoveFileConfirmation: "Are you Sure?", // ask before removing file
			// Language Strings
			dictFileTooBig: "File is to big  Max allowed file size is 20MB",
			dictInvalidFileType: "Invalid File Type",
			dictCancelUpload: "Cancel",
			dictRemoveFile: "Remove",
			dictMaxFilesExceeded: "Only 5 images are allowed",
			dictDefaultMessage: "Drop room images here to upload",
		});
		
		
		premadeclassroomEditDropzone.on("addedfile", function(file) {
			//console.log(file);
		});

		premadeclassroomEditDropzone.on("removedfile", function(file) {
			// console.log(file);
		});

		// Add mmore data to send along with the file as POST data. (optional)
		premadeclassroomEditDropzone.on("sending", function(file, xhr, formData) {
			formData.append("listingid", $("input[name='listingid']").val()); // $_POST["dropzone"]
		});

		premadeclassroomEditDropzone.on("error", function(file, response) {
			console.log(response);
		});

		// on success
		premadeclassroomEditDropzone.on("successmultiple", function(file, response) {
			// get response from successful ajax request
			console.log(response);
			// submit the form after images upload
			// (if u want yo submit rest of the inputs in the form)
			//document.getElementById("dropzone-form").submit();
		});


		function checkitclear(){
		//$('#contactmessage').val('');
		$('.msgerrcls').val('');
		$('.msgerrcls').hide();
		}  

    </script>		  
    <!--script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script-->
    <?php
    $prop_latlng = $propertyObj;
    if (isset($prop_latlng->addr_work_org_same) && $prop_latlng->addr_work_org_same == 0) {
        $lat = $prop_latlng->pro_lat;
        $lng = $prop_latlng->pro_lon;
    } elseif (isset($prop_latlng->user->paid_service->house_property_differ) && $prop_latlng->user->paid_service->house_property_differ == 1) {
        $lat = isset($prop_latlng->user->paid_service->proplatbox) ? $prop_latlng->user->paid_service->proplatbox : "";
        $lng = isset($prop_latlng->user->paid_service->proplonbox) ? $prop_latlng->user->paid_service->proplonbox : "";
    } elseif (isset($prop_latlng->user->paid_service->house_property_differ) && $prop_latlng->user->paid_service->house_property_differ == 0) {
        $lat = isset($prop_latlng->user->paid_service->houselatbox) ? $prop_latlng->user->paid_service->houselatbox : "";
        $lng = isset($prop_latlng->user->paid_service->houselonbox) ? $prop_latlng->user->paid_service->houselonbox : "";
    } else {
        $lat = isset($prop_latlng->org_lat) ? $prop_latlng->org_lat : "";
        $lng = isset($prop_latlng->org_lan) ? $prop_latlng->org_lan : '';
    }
    ?>
    <script>
		
		
		//EDIT Guest Room Modal
		$(document).on('show.bs.modal','#edit_room_modal', function (event) {
			var button = $(event.relatedTarget); // Button that triggered the modal
			var room_id      = button.attr('room_id');
			var room_desc    = button.attr('room_desc');
			var room_toilet  = button.attr('room_toilet');
			var room_shower  = button.attr('room_shower');
			var room_type_id = button.attr('room_type_id');
			
			var room_facilities = button.attr('room_facilities').split(",");
			//console.log("EDIT BUTTON CLICK DATA room_id:"+room_id+" room_desc:"+room_desc+" room_toilet:"+room_toilet+" room_shower:"+room_shower+" room_type_id:"+room_type_id+" room_facilities:"+room_facilities);
			var room_images     = button.closest(".room_item").find(".guest_room_images img");
			var existingFileCount = room_images.length;
			var dynamic_response = '';
			
			room_images.each(function( room_image ){
				var img_url = $(this).attr('src'); 
				var img_size = $(this).data('size'); 
				dynamic_response += img_url+",";
				var mockFile = { name: "Room Image "+room_image, size: img_size,accepted: true,already_uploaded:true,img_url:img_url};
								
				// Call the default addedfile event handler
				roomEditDropzone.emit("addedfile", mockFile);

				// And optionally show the thumbnail of the file:
				roomEditDropzone.emit("thumbnail",mockFile, img_url);
				
				roomEditDropzone.emit("success", mockFile);
				
				roomEditDropzone.emit("complete", mockFile);
				roomEditDropzone.files.push(mockFile);  
		
			});
		
			$.ajax({
				type: 'POST',
				url: baseurl + '/rentals/loadbedsforupdate',
				async: false,
				data: {
					room_id: room_id
				},
				success: function (data) { 
					if(data != ''){
						$(".room_beds_for_update").empty().html(data); 
					}else{
						$(".room_beds_for_update").empty();
					}
				}
			});
			
			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			var modal = $(this)

			modal.find(".ajax_processor").hide();
			modal.find('.modal-body input[id="update_room_id"]').val(room_id);
			modal.find('.modal-body input[id="update_room_desc"]').val(room_desc);
			modal.find('.modal-body input[id="update_room_id"]').val(room_id);
			modal.find('.modal-body .update_room_type_id option[value="'+ room_type_id +'"]').prop('selected', true);
			modal.find('.modal-body .update_room_shower option[value="'+ room_shower +'"]').prop('selected', true);
			modal.find('.modal-body .update_room_toilet option[value="'+ room_toilet +'"]').prop('selected', true);
			$('.update_room_facility').prop('checked', false); 
			$('.update_room_facility').closest("li").find(".grey-icon").show(); 
			$('.update_room_facility').closest("li").find(".orange-icon").hide();
			$.each(room_facilities,function(index,facility){
				$('.update_room_facility[value="'+facility+'"]').prop('checked', true);
				$('.update_room_facility[value="'+facility+'"]').closest("li").find(".grey-icon").hide();
				$('.update_room_facility[value="'+facility+'"]').closest("li").find(".orange-icon").show();
			});
			modal.find('.modal-footer .delete_room').attr('data-room_id',room_id); 
		  
		});
		
		// Edit Class Room Modal
		
		$(document).on('show.bs.modal','#edit_classroom_modal', function (event) {
			var button = $(event.relatedTarget); // Button that triggered the modal
			var room_id      = button.attr('room_id');
			var room_desc    = button.attr('room_desc');
			var room_title  = button.attr('room_title');
			var number_places  = button.attr('number_places');
			//var room_type_id = button.attr('room_type_id');
			var payment_type = button.attr('payment_type');
			var payment_currency = button.attr('payment_currency');
			var payment_amount = button.attr('payment_amount');
			
			var room_facilities = button.attr('room_facilities').split(",");
			//console.log("EDIT BUTTON CLICK DATA room_id:"+room_id+" room_desc:"+room_desc+" room_toilet:"+room_toilet+" room_shower:"+room_shower+" room_type_id:"+room_type_id+" room_facilities:"+room_facilities);
			var room_images     = button.closest(".classroom_item").find(".class_room_images img");
			var existingFileCount = room_images.length;
			var dynamic_response = '';
			
			room_images.each(function( room_image ){
				var img_url = $(this).attr('src'); 
				var img_size = $(this).data('size'); 
				dynamic_response += img_url+",";
				var mockFile = { name: "Class Room Image "+room_image, size: img_size,accepted: true,already_uploaded:true,img_url:img_url};
								
				// Call the default addedfile event handler
				classroomEditDropzone.emit("addedfile", mockFile);

				// And optionally show the thumbnail of the file:
				classroomEditDropzone.emit("thumbnail",mockFile, img_url);
				
				classroomEditDropzone.emit("success", mockFile); 
				
				classroomEditDropzone.emit("complete", mockFile);

				classroomEditDropzone.files.push(mockFile); 
				
		
			});
		
			
			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			var modal = $(this)
			modal.find(".ajax_processor").hide();
			modal.find('.modal-body input[id="update_classroom_id"]').val(room_id);
			modal.find('.modal-body input[id="update_classroom_title"]').val(room_title);
			modal.find('.modal-body input[id="update_classroom_desc"]').val(room_desc);
			modal.find('.modal-body #update_class_room_number_places').val(number_places);
			modal.find('.modal-body #update_class_room_number_places').attr('old_value',number_places);
			//modal.find('.modal-body .update_classroom_type_id option[value="'+ room_type_id +'"]').prop('selected', true);
			modal.find('.modal-body #update_class_room_currency option[value="'+ payment_currency +'"]').prop('selected', true);
			modal.find('.modal-body .update_class_room_payment_type option[value="'+ payment_type +'"]').prop('selected', true);
			modal.find('.modal-body input[id="update_class_room_amount"]').val(payment_amount);
			
			$.each(room_facilities,function(index,facility){
				$('.update_classroom_facility[value="'+facility+'"]').prop('checked', true);
				$('.update_classroom_facility[value="'+facility+'"]').closest("li").find(".grey-icon").hide();
				$('.update_classroom_facility[value="'+facility+'"]').closest("li").find(".orange-icon").show();
			});
			
			modal.find('.modal-footer .delete_classroom').attr('data-room_id',room_id); 
		  
		});



        // Edit Activity Modal

        $(document).on('show.bs.modal','#edit_activity_modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var room_id      = button.attr('room_id');
            var room_desc    = button.attr('room_desc');
            var room_title  = button.attr('room_title');
            var number_places  = button.attr('number_places');
            //var room_type_id = button.attr('room_type_id');
            var payment_type = button.attr('payment_type');
            var payment_currency = button.attr('payment_currency');
            var payment_amount = button.attr('payment_amount');

            var room_facilities = button.attr('room_facilities').split(",");
            //console.log("EDIT BUTTON CLICK DATA room_id:"+room_id+" room_desc:"+room_desc+" room_toilet:"+room_toilet+" room_shower:"+room_shower+" room_type_id:"+room_type_id+" room_facilities:"+room_facilities);
            var room_images     = button.closest(".activity_item").find(".class_room_images img");
            var existingFileCount = room_images.length;
            var dynamic_response = '';

            room_images.each(function( room_image ){
                var img_url = $(this).attr('src');
                var img_size = $(this).data('size');
                dynamic_response += img_url+",";
                var mockFile = { name: "Activity Room Image "+room_image, size: img_size,accepted: true,already_uploaded:true,img_url:img_url};

                // Call the default addedfile event handler
                activityEditDropZone.emit("addedfile", mockFile);

                // And optionally show the thumbnail of the file:
                activityEditDropZone.emit("thumbnail",mockFile, img_url);

                activityEditDropZone.emit("success", mockFile);

                activityEditDropZone.emit("complete", mockFile);

                activityEditDropZone.files.push(mockFile);
            });

            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            modal.find(".ajax_processor").hide();
            modal.find('.modal-body input[id="update_activity_id"]').val(room_id);
            modal.find('.modal-body input[id="update_activity_title"]').val(room_title);
            modal.find('.modal-body input[id="update_activity_desc"]').val(room_desc);
            modal.find('.modal-body #update_activity_room_number_places').val(number_places);
            modal.find('.modal-body #update_activity_room_number_places').attr('old_value',number_places);
            //modal.find('.modal-body .update_classroom_type_id option[value="'+ room_type_id +'"]').prop('selected', true);
            modal.find('.modal-body #update_acitivity_room_currency option[value="'+ payment_currency +'"]').prop('selected', true);
            modal.find('.modal-body .update_activity_room_payment_type option[value="'+ payment_type +'"]').prop('selected', true);
            modal.find('.modal-body input[id="update_activity_room_amount"]').val(payment_amount);

            $.each(room_facilities,function(index,facility){
                $('.update_activity_facility[value="'+facility+'"]').prop('checked', true);
                $('.update_activity_facility[value="'+facility+'"]').closest("li").find(".grey-icon").hide();
                $('.update_activity_facility[value="'+facility+'"]').closest("li").find(".orange-icon").show();
            });

            modal.find('.modal-footer .delete_activity').attr('data-room_id',room_id);

        });

        // Edit Diet Modal

        $(document).on('show.bs.modal','#edit_diet_modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var room_id      = button.attr('room_id');
            var room_desc    = button.attr('room_desc');
            var room_title  = button.attr('room_title');
            var number_places  = button.attr('number_places');
            //var room_type_id = button.attr('room_type_id');
            var payment_type = button.attr('payment_type');
            var payment_currency = button.attr('payment_currency');
            var payment_amount = button.attr('payment_amount');

            var room_facilities = button.attr('room_facilities').split(",");
            //console.log("EDIT BUTTON CLICK DATA room_id:"+room_id+" room_desc:"+room_desc+" room_toilet:"+room_toilet+" room_shower:"+room_shower+" room_type_id:"+room_type_id+" room_facilities:"+room_facilities);
            var room_images     = button.closest(".diet_item").find(".class_room_images img");
            var existingFileCount = room_images.length;
            var dynamic_response = '';

            room_images.each(function( room_image ){
                var img_url = $(this).attr('src');
                var img_size = $(this).data('size');
                dynamic_response += img_url+",";
                var mockFile = { name: "Diet Image "+room_image, size: img_size,accepted: true,already_uploaded:true,img_url:img_url};

                // Call the default addedfile event handler
                dietEditDropZone.emit("addedfile", mockFile);

                // And optionally show the thumbnail of the file:
                dietEditDropZone.emit("thumbnail",mockFile, img_url);

                dietEditDropZone.emit("success", mockFile);

                dietEditDropZone.emit("complete", mockFile);

                dietEditDropZone.files.push(mockFile);
            });

            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            modal.find(".ajax_processor").hide();
            modal.find('.modal-body input[id="update_diet_id"]').val(room_id);
            modal.find('.modal-body input[id="update_diet_title"]').val(room_title);
            modal.find('.modal-body input[id="update_diet_desc"]').val(room_desc);
            modal.find('.modal-body #update_diet_number_places').val(number_places);
            modal.find('.modal-body #update_diet_number_places').attr('old_value',number_places);
            //modal.find('.modal-body .update_classroom_type_id option[value="'+ room_type_id +'"]').prop('selected', true);
            modal.find('.modal-body #update_diet_currency option[value="'+ payment_currency +'"]').prop('selected', true);
            modal.find('.modal-body .update_diet_payment_type option[value="'+ payment_type +'"]').prop('selected', true);
            modal.find('.modal-body input[id="update_diet_amount"]').val(payment_amount);

            $.each(room_facilities,function(index,facility){
                $('.update_diet_facility[value="'+facility+'"]').prop('checked', true);
                $('.update_diet_facility[value="'+facility+'"]').closest("li").find(".grey-icon").hide();
                $('.update_diet_facility[value="'+facility+'"]').closest("li").find(".orange-icon").show();
            });

            modal.find('.modal-footer .delete_diet').attr('data-room_id',room_id);

        });



		
		//Reset Bed Room Edit Modal
		$(document).on('hide.bs.modal',"#edit_room_modal", function(){ 
			// Reseting add form data
			$(".edit_room_type_id").prop('selectedIndex', 0);
			$(".update_room_toilet").prop('selectedIndex', 0);
			$(".update_room_shower").prop('selectedIndex', 0);						
			$("#update_room_desc").val("");
			$('.update_room_facility').prop('checked', false);
							
			roomEditDropzone.removeAllFiles(true); 
		}); 
		
		//Reset Classroom edit modal
		$(document).on('hide.bs.modal',"#edit_classroom_modal", function(){ 
			// Reseting add form data
			//$(".update_classroom_type_id").prop('selectedIndex', 0);
			$(".update_classroom_toilet").prop('selectedIndex', 0);
			$(".update_classroom_shower").prop('selectedIndex', 0);						
			$("#update_classroom_desc").val("");
			$('.update_classroom_facility').prop('checked', false);
							
			classroomEditDropzone.removeAllFiles(true); 
		});

        //Reset Activity edit modal
        $(document).on('hide.bs.modal',"#edit_activity_modal", function(){
            // Reseting add form data
            //$(".update_classroom_type_id").prop('selectedIndex', 0);

            $("#update_activity_desc").val("");
            $('.update_activity_facility').find("li.orange-icon").hide();
            $('.update_activity_facility').find("li.grey-icon").show();
            activityEditDropZone.removeAllFiles(true);
        });

        //Reset Diet edit modal
        $(document).on('hide.bs.modal',"#edit_diet_modal", function(){
            // Reseting add form data
            //$(".update_classroom_type_id").prop('selectedIndex', 0);

            $("#update_diet_desc").val("");
            $('.update_diet_facility').find("li.orange-icon").hide();
            $('.update_diet_facility').find("li.grey-icon").show();
            dietEditDropZone.removeAllFiles(true);
        });




        // Add Bed Modal
		$('#add_bed_modal').on('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget); // Button that triggered the modal
		  var room_id = button.data('room_id');
		  var room_name = button.data('type');
		  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		  var modal = $(this)
		
		  modal.find('.modal-body input[id="room_id_for_new_bed"]').val(room_id);
		  modal.find('.modal-title').text('Add new bed to  ' + room_name)
		});
		//Add Class Place Modal
		$('#add_classplace_modal').on('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget); // Button that triggered the modal
		  var room_id = button.data('room_id');
		  var room_name = button.data('type');
		  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		  var modal = $(this)
		
		  modal.find('.modal-body input[id="room_id_for_new_bed"]').val(room_id);
		  modal.find('.modal-title').text('Add new bed to  ' + room_name)
		});
		
		// EDIT Bed Modal
		$(document).on('show.bs.modal','#edit_bed_modal', function (event) {
		  var button = $(event.relatedTarget); // Button that triggered the modal
		  var bed_id = button.data('bed_id');
		  var room_id = button.data('room_id');
		  var bed_type_id = button.data('bed_type_id');
		  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		  var modal = $(this)
		  console.log(bed_id);
		  console.log(bed_type_id);
		  modal.find('.modal-body input[id="update_bed_id"]').val(bed_id);
		  modal.find('.modal-body input[id="update_bed_room_id"]').val(room_id);
		  modal.find('.modal-footer .delete_bed').attr('data-bed-id',bed_id); 
		  modal.find('.modal-body select option[value="'+ bed_type_id +'"]').prop('selected', true);;
		});
		
		//Reset Pre Made Classroom edit modal
		$(document).on('hide.bs.modal',"#pre_maid_classroom_modal", function(){ 
									
			premadeclassroomEditDropzone.removeAllFiles(true); 
		}); 
		
		
		
		$(".add-pre-made-class").on('click',function(){
			if($(this).attr("pre-made") != "1"){
				if($(this).attr("pre-made") == "2"){
					var class_title="10 hours of individual lessons";
					var class_desp = "These individual lessons may be done in the morning or afternoon, suggested times are 9.30 am to 10.30 am , half hour break, and again 11.00 am to 12.00 am from Monday to Friday.";
					var price_type = $(this).attr("price_type");
					var currency_id = $(this).attr("currency_id");
					var amount = $(this).attr("amount");
					var seats   = $(this).attr("seats");
					$("#premade_classroom_title").val(class_title);
					$("#pre_made_class_id").val(2);
					$("#premade_class_room_number_places").val(seats);
					$("#premade_class_room_amount").val(amount);
					$("#premade_classroom_desc").val(class_desp);
					$('#premade_class_room_currency option[value="'+ currency_id +'"]').prop('selected', true);
					$('.premade_class_room_payment_type option[value="'+ price_type +'"]').prop('selected', true);

					$('.premade_classroom_facility').prop('checked', false); 
					$('.premade_classroom_facility').closest("li").find(".grey-icon").show(); 
					$('.premade_classroom_facility').closest("li").find(".orange-icon").hide();
					var premade_classroom_facilities = $(this).attr('room_facilities').split(",");
					$.each(premade_classroom_facilities,function(index,facility){
						$('.premade_classroom_facility[value="'+facility+'"]').prop('checked', true);
						$('.premade_classroom_facility[value="'+facility+'"]').closest("li").find(".grey-icon").hide();
						$('.premade_classroom_facility[value="'+facility+'"]').closest("li").find(".orange-icon").show();
					});
					var img_url = 'http://work-related-learning.com/frontendassets/images/pre-made-classes/Maute-Student-signup.png'; 
					
					
					var mockFile = { name: "10 hours of individual lessons Image", size: 339000,accepted: true,img_url:img_url};
							
					// Call the default addedfile event handler
					premadeclassroomEditDropzone.emit("addedfile", mockFile);

					// And optionally show the thumbnail of the file:
					premadeclassroomEditDropzone.emit("thumbnail",mockFile, img_url);

					premadeclassroomEditDropzone.emit("success", mockFile); 

					premadeclassroomEditDropzone.emit("complete", mockFile);

					premadeclassroomEditDropzone.files.push(mockFile); 
					$('#pre_maid_classroom_modal').modal('show'); 
				}else if($(this).attr("pre-made") == "3"){
					var class_title="15 hours of individual lessons";
					var class_desp = "These small group sessions shared by up to 4 students may be done in the morning or afternoon, suggested times are two sessions from 9.00 am to 10.30 am, break, and again 11.00 am to 12.30 am for a total of 20 lessons a week.";
					var price_type = $(this).attr("price_type");
					var currency_id = $(this).attr("currency_id");
					var amount = $(this).attr("amount");
					var seats   = $(this).attr("seats");
					$("#premade_classroom_title").val(class_title);
					$("#pre_made_class_id").val(3); 
					$("#premade_class_room_number_places").val(seats);
					$("#premade_class_room_amount").val(amount);
					$("#premade_classroom_desc").val(class_desp);
					$('#premade_class_room_currency option[value="'+ currency_id +'"]').prop('selected', true);
					$('.premade_class_room_payment_type option[value="'+ price_type +'"]').prop('selected', true);

									
					$('.premade_classroom_facility').prop('checked', false); 
					$('.premade_classroom_facility').closest("li").find(".grey-icon").show(); 
					$('.premade_classroom_facility').closest("li").find(".orange-icon").hide();
					
					var premade_classroom_facilities = $(this).attr('room_facilities').split(",");
					$.each(premade_classroom_facilities,function(index,facility){
						$('.premade_classroom_facility[value="'+facility+'"]').prop('checked', true);
						$('.premade_classroom_facility[value="'+facility+'"]').closest("li").find(".grey-icon").hide();
						$('.premade_classroom_facility[value="'+facility+'"]').closest("li").find(".orange-icon").show();
					});
					
					var img_url = 'http://work-related-learning.com/frontendassets/images/pre-made-classes/School-House-under-16.png'; 
					
					
					var mockFile = { name: "15 hours of individual lessons Image", size: 317000,accepted: true,img_url:img_url};
							
					// Call the default addedfile event handler
					premadeclassroomEditDropzone.emit("addedfile", mockFile);

					// And optionally show the thumbnail of the file:
					premadeclassroomEditDropzone.emit("thumbnail",mockFile, img_url); 

					premadeclassroomEditDropzone.emit("success", mockFile); 

					premadeclassroomEditDropzone.emit("complete", mockFile);

					premadeclassroomEditDropzone.files.push(mockFile); 
					
					$('#pre_maid_classroom_modal').modal('show'); 
				}
			}else{
				$.ajax({
					type: 'POST',
					url: baseurl + '/rentals/addnewclassroom',
					async: false,
					data: {
						room_title: "Five 30 minute conversations",
						pre_made_id: 1,
						number_places: 1,
						payment_currency: false,
						payment_type: false,
						payment_amount: false,
						class_description: "Included in the price, to speak about work or anything you like, to make sure the host engages professionally specifying what time to start the sessions, to make sure the students don't run out or off to their rooms.",
						classroom_facilities: "",
						classroom_images: ["http://work-related-learning.com/frontendassets/images/pre-made-classes/School-House-with-16-to-18.png"],
					},
					success: function (data) {   
						var splited = data.split(",");
						var room_id = splited[1];
						var place_ids_plited = splited[2].split('==');						 
						var places_available = '';  
						$.each(place_ids_plited,function(index,place){
							places_available += '<img src="'+baseurl+'/images/class-icon.png" class="class_room_type_icon" />\
							<div class="custom_tooltip"  >\
								<img src="'+baseurl+'/images/available-icon.png" class="tool_tip_icon remove_place" place-id="'+place+'" />\
								<span class="custom_tooltiptext">Available Place</span>\
							</div>';
						});
						if(splited[0] == "success"){
							_this.closest('.modal-footer').find(".ajax_processor").show().html('Your Room added successfully.').removeClass('error').delay(5000).fadeOut();
							
							
							
								
							var room_html = '<div class="classroom_item " id="pre-made-class-'+room_id+'" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">\
												<div class="row" id="classroom_title">\
													<div class="col-xs-8 col-md-7">\
														<h4 data-toggle="tooltip" title="Five 30 minute conversations">\
														<img src="http://work-related-learning.com/frontendassets/images/class-types/Individual lesson class - grey.png" class="class_room_type_icon">\
															Five 30 minute conversations \
														</h4>\
													</div>\
													<div class="col-xs-4 col-md-5">\
														<div class="classroom_action_icons">\
															<span class="included">Included</span>\
														</div>\
													</div>\
												</div>\
												<div class="classroom_description">\
													<ul class="class_room_images">\
															<li>\
																<a href="http://work-related-learning.com/frontendassets/images/pre-made-classes/School House with 16 to 18.png" data-fancybox="images"><img src="http://work-related-learning.com/frontendassets/images/pre-made-classes/School House with 16 to 18.png"></a>\
															</li>\
													</ul>\
													<p class="room_desp" style="word-wrap:break-word;">\
														Included in the price, to speak about work or anything you like, to make sure the host engages professionally specifying what time to start the sessions, to make sure the students don\'t run out or off to their rooms.\
													</p>\
												</div>\
											</div>';  
								$("#pre-made-class-"+pre_made_id).remove();   
								if($(".classroom_item").length > 0){
									var last_class_room_id = "#"+$(".classroom_item").last().attr("id");
									$(room_html).insertAfter(last_class_room_id);
									var target = $('.classroom_item').last(); 
											$('html,body').animate({
												scrollTop: target.offset().top - 50
											}, 1000);
								}else{
									$(room_html).insertAfter(".class_rooms_sec");
								}
								
								
								
							$('#pre_maid_classroom_modal').modal('toggle'); 
							
							premadeclassroomEditDropzone.removeAllFiles(true); 
						}else{
							_this.closest('.modal-footer').find(".ajax_processor").show().html('Sorry there is any problem.').addClass('error').delay(5000).fadeOut();
						}
					}
				});
			}
		});
		
		
		
		$(".add-pre-made-activity").on('click',function(){
			if($(this).attr("pre-made") != "1"){
				if($(this).attr("pre-made") == "2"){
					var class_title="10 hours of individual lessons";
					var class_desp = "These individual lessons may be done in the morning or afternoon, suggested times are 9.30 am to 10.30 am , half hour break, and again 11.00 am to 12.00 am from Monday to Friday.";
					var price_type = $(this).attr("price_type");
					var currency_id = $(this).attr("currency_id");
					var amount = $(this).attr("amount");
					var seats   = $(this).attr("seats");
					$("#premade_classroom_title").val(class_title);
					$("#pre_made_class_id").val(2);
					$("#premade_class_room_number_places").val(seats);
					$("#premade_class_room_amount").val(amount);
					$("#premade_classroom_desc").val(class_desp);
					$('#premade_class_room_currency option[value="'+ currency_id +'"]').prop('selected', true);
					$('.premade_class_room_payment_type option[value="'+ price_type +'"]').prop('selected', true);

					$('.premade_classroom_facility').prop('checked', false); 
					$('.premade_classroom_facility').closest("li").find(".grey-icon").show(); 
					$('.premade_classroom_facility').closest("li").find(".orange-icon").hide();
					var premade_classroom_facilities = $(this).attr('room_facilities').split(",");
					$.each(premade_classroom_facilities,function(index,facility){
						$('.premade_classroom_facility[value="'+facility+'"]').prop('checked', true);
						$('.premade_classroom_facility[value="'+facility+'"]').closest("li").find(".grey-icon").hide();
						$('.premade_classroom_facility[value="'+facility+'"]').closest("li").find(".orange-icon").show();
					});
					var img_url = 'http://work-related-learning.com/frontendassets/images/pre-made-classes/Maute-Student-signup.png'; 
					
					
					var mockFile = { name: "10 hours of individual lessons Image", size: 339000,accepted: true,img_url:img_url};
							
					// Call the default addedfile event handler
					premadeclassroomEditDropzone.emit("addedfile", mockFile);

					// And optionally show the thumbnail of the file:
					premadeclassroomEditDropzone.emit("thumbnail",mockFile, img_url);

					premadeclassroomEditDropzone.emit("success", mockFile); 

					premadeclassroomEditDropzone.emit("complete", mockFile);

					premadeclassroomEditDropzone.files.push(mockFile); 
					$('#pre_maid_classroom_modal').modal('show'); 
					
				}else if($(this).attr("pre-made") == "3"){
					var class_title="15 hours of individual lessons";
					var class_desp = "These small group sessions shared by up to 4 students may be done in the morning or afternoon, suggested times are two sessions from 9.00 am to 10.30 am, break, and again 11.00 am to 12.30 am for a total of 20 lessons a week.";
					var price_type = $(this).attr("price_type");
					var currency_id = $(this).attr("currency_id");
					var amount = $(this).attr("amount");
					var seats   = $(this).attr("seats");
					$("#premade_classroom_title").val(class_title);
					$("#pre_made_class_id").val(3); 
					$("#premade_class_room_number_places").val(seats);
					$("#premade_class_room_amount").val(amount);
					$("#premade_classroom_desc").val(class_desp);
					$('#premade_class_room_currency option[value="'+ currency_id +'"]').prop('selected', true);
					$('.premade_class_room_payment_type option[value="'+ price_type +'"]').prop('selected', true);

									
					$('.premade_classroom_facility').prop('checked', false); 
					$('.premade_classroom_facility').closest("li").find(".grey-icon").show(); 
					$('.premade_classroom_facility').closest("li").find(".orange-icon").hide();
					
					var premade_classroom_facilities = $(this).attr('room_facilities').split(",");
					$.each(premade_classroom_facilities,function(index,facility){
						$('.premade_classroom_facility[value="'+facility+'"]').prop('checked', true);
						$('.premade_classroom_facility[value="'+facility+'"]').closest("li").find(".grey-icon").hide();
						$('.premade_classroom_facility[value="'+facility+'"]').closest("li").find(".orange-icon").show();
					});
					
					var img_url = 'http://work-related-learning.com/frontendassets/images/pre-made-classes/School-House-under-16.png'; 
					
					
					var mockFile = { name: "15 hours of individual lessons Image", size: 317000,accepted: true,img_url:img_url};
							
					// Call the default addedfile event handler
					premadeclassroomEditDropzone.emit("addedfile", mockFile);

					// And optionally show the thumbnail of the file:
					premadeclassroomEditDropzone.emit("thumbnail",mockFile, img_url); 

					premadeclassroomEditDropzone.emit("success", mockFile); 

					premadeclassroomEditDropzone.emit("complete", mockFile);

					premadeclassroomEditDropzone.files.push(mockFile); 
					
					$('#pre_maid_classroom_modal').modal('show'); 
				}
			}else{
				$.ajax({
					type: 'POST',
					url: baseurl + '/rentals/addnewclassroom',
					async: false,
					data: {
						room_title: "Five 30 minute conversations",
						pre_made_id: 1,
						number_places: 1,
						payment_currency: false,
						payment_type: false,
						payment_amount: false,
						class_description: "Included in the price, to speak about work or anything you like, to make sure the host engages professionally specifying what time to start the sessions, to make sure the students don't run out or off to their rooms.",
						classroom_facilities: "",
						classroom_images: ["http://work-related-learning.com/frontendassets/images/pre-made-classes/School-House-with-16-to-18.png"],
					},
					success: function (data) {   
						var splited = data.split(",");
						var room_id = splited[1];
						var place_ids_plited = splited[2].split('==');						 
						var places_available = '';  
						$.each(place_ids_plited,function(index,place){
							places_available += '<img src="'+baseurl+'/images/class-icon.png" class="class_room_type_icon" />\
							<div class="custom_tooltip"  >\
								<img src="'+baseurl+'/images/available-icon.png" class="tool_tip_icon remove_place" place-id="'+place+'" />\
								<span class="custom_tooltiptext">Available Place</span>\
							</div>';
						});
						if(splited[0] == "success"){
							_this.closest('.modal-footer').find(".ajax_processor").show().html('Your Room added successfully.').removeClass('error').delay(5000).fadeOut();
							
							
							
								
							var room_html = '<div class="classroom_item " id="pre-made-class-'+room_id+'" style="border: 1px solid #dce0e0;  margin-top: 10px;clear:both">\
												<div class="row" id="classroom_title">\
													<div class="col-xs-8 col-md-7">\
														<h4 data-toggle="tooltip" title="Five 30 minute conversations">\
														<img src="http://work-related-learning.com/frontendassets/images/class-types/Individual lesson class - grey.png" class="class_room_type_icon">\
															Five 30 minute conversations \
														</h4>\
													</div>\
													<div class="col-xs-4 col-md-5">\
														<div class="classroom_action_icons">\
															<span class="included">Included</span>\
														</div>\
													</div>\
												</div>\
												<div class="classroom_description">\
													<ul class="class_room_images">\
															<li>\
																<a href="http://work-related-learning.com/frontendassets/images/pre-made-classes/School House with 16 to 18.png" data-fancybox="images"><img src="http://work-related-learning.com/frontendassets/images/pre-made-classes/School House with 16 to 18.png"></a>\
															</li>\
													</ul>\
													<p class="room_desp" style="word-wrap:break-word;">\
														Included in the price, to speak about work or anything you like, to make sure the host engages professionally specifying what time to start the sessions, to make sure the students don\'t run out or off to their rooms.\
													</p>\
												</div>\
											</div>';  
								$("#pre-made-class-"+pre_made_id).remove();   
								if($(".classroom_item").length > 0){
									var last_class_room_id = "#"+$(".classroom_item").last().attr("id");
									$(room_html).insertAfter(last_class_room_id);
									var target = $('.classroom_item').last(); 
											$('html,body').animate({
												scrollTop: target.offset().top - 50
											}, 1000);
								}else{
									$(room_html).insertAfter(".class_rooms_sec");
								}
								
								
								
							$('#pre_maid_classroom_modal').modal('toggle'); 
							
							premadeclassroomEditDropzone.removeAllFiles(true); 
						}else{
							_this.closest('.modal-footer').find(".ajax_processor").show().html('Sorry there is any problem.').addClass('error').delay(5000).fadeOut();
						}
					}
				});
			}
		});
		
    
        /*  var mapOptions = {
         zoom: 10,
         mapTypeId: google.maps.MapTypeId.SATELLITE,
         scrollwheel: false      
         }
         map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);*/
        var map;
        var geocoder;
        var centerChangedLast;
        var reverseGeocodedLast;
        var currentReverseGeocodeResponse;
        function initialize() {
        lat = <?php echo isset($lat) ? $lat : ''; ?>;
        long = <?php echo isset($lng) ? $lng : ''; ?>;
        var latlng = new google.maps.LatLng(lat, long);
        var myOptions = {
        zoom: 10,
                center: latlng,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        var myCity = new google.maps.Circle({
        center:latlng,
                radius:7000,
                strokeColor:"#8fe3de",
                strokeOpacity:0.8,
                strokeWeight:2,
                fillColor:"#8cdfd9",
                fillOpacity:0.4
        });
        myCity.setMap(map);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
        // google.maps.event.addDomListener(window, 'load', initialize);    

        //slideer stop
        $('.carousel').carousel({
        interval: false
        })

//Nav Fixed
        $(document).ready(function(){ 
			var a = $(".wid").outerWidth();
			$(".make_fix").css('width', a - 15);
			var project2 = $('.tpp').offset();
			var $window = $(window);
			$window.scroll(function(){
				//start fix
				if($window.scrollTop() >= project2.top){
					$(".make_fix").addClass("fixed");
					jQuery('.header_top_invidual').addClass('filter_menu1');
				}else{
					$(".make_fix").removeClass("fixed");
					jQuery('.header_top_invidual').removeClass('filter_menu1');
				}
			});
        });
        //clouse fix
        $(document).ready(function(){
			var project1 = $('.stopfixer').offset();
			var $window = $(window);
			var valueofmap = project1.top - '100';
			$window.scroll(function() {
				if($window.scrollTop() >= valueofmap){
					// alert(project1.top);
					$(".make_fix").addClass("fixed_rm");
				}else{
					$(".make_fix").removeClass("fixed_rm");
				}
			});
        });
		
        //Owl Carasel
        $(document).ready(function() {

			//var owl = $("#owl-demo");

			/*owl.owlCarousel({
			 items : 3, //10 items above 1000px browser width
			 itemsDesktop : [3,5], //5 items between 1000px and 901px
			 itemsDesktopSmall : [3,3], // betweem 900px and 601px
			 itemsTablet: [3,2], //2 items between 600 and 0
			 itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
			 });*/

			// Custom Navigation Events
			$(".next").click(function(){
				owl.trigger('owl.next');
			});
			$(".prev").click(function(){
				owl.trigger('owl.prev');
			});

        });
		
		
        //width of filter
        $(document).ready(function(){
			$(window).scroll(function(){
				$(".startdate").datepicker("hide");
				$(".enddate").datepicker("hide");
				$(".startdate").blur();
				$(".enddate").blur();
				$(".startdatemobile").datepicker("hide");
				$(".enddatemobile").datepicker("hide");
				$(".startdatemobile").blur();
				$(".enddatemobile").blur();
			});
			
			$(".startdate").keydown(function(event){
				if (event.which == 13) {
					$(".startdate").readonlyDatepicker(true);
				}
			});
			$(".enddate").keydown(function(event){
				if (event.which == 13) {
					$(".enddate").readonlyDatepicker(true);
				}
			});
			$(".startdatemobile").keydown(function(event){
				if (event.which == 13) {
					$(".startdatemobile").readonlyDatepicker(true);
				}
			});
			$(".enddatemobile").keydown(function(event){
				if (event.which == 13) {
					$(".enddatemobile").readonlyDatepicker(true);
				}
			});
        });
		
		</script>

    <?php
	
	if (!empty($checkin_disable_dates)) {
        $forbiddenCheckIn = str_replace('"',"",$checkin_disable_dates);
        $forbiddenCheckIn = trim($forbiddenCheckIn,",");
        //$forbiddenCheckIn = '[' . (count(explode(",",$forbiddenCheckIn)) > 1 ? explode(",",$forbiddenCheckIn)[0] : $forbiddenCheckIn) . ']';
       
        /*if(isset($disabledDates) && count($disabledDates) > 0){
            $disabledDates_checkin = implode(",", $disabledDates);
            $forbiddenCheckIn1 .= ",".$disabledDates_checkin;
        }*/
		//$forbiddenCheckIn = '[' . (count(explode(",",$forbiddenCheckIn1)) > 1 ? $forbiddenCheckIn1 : $forbiddenCheckIn1) . ']';
       
   

	}
	if(!empty($checkout_disable_dates)){
		$forbiddenCheckOut = str_replace('"',"",$checkout_disable_dates);
		//$forbiddenCheckOut = str_replace(',]',"]",$forbiddenCheckOut);
        $forbiddenCheckOut = trim($forbiddenCheckOut,",");
        //$forbiddenCheckOut = (count(explode(",",$forbiddenCheckOut1)) > 1 ? $forbiddenCheckOut1 : $forbiddenCheckOut1);
        /*if(count($disabledDates) > 0){
            $disabledDates_checkout = implode(",", $disabledDates);
            $forbiddenCheckOut .= ",".$disabledDates_checkout;
        }*/
        //$forbiddenCheckOut = '[' .$forbiddenCheckOut. ']';
        
	}
	
    ?>

    <script>

        var dateRange = [];
        var beforedate = [];
        var afterdate = [];
        $(function () {
        startdate = '';
        enddate = '';
        listid = <?php echo $property_id ?>;
        startdates = "";
        enddates = "";
        for (i = 0; i < startdates.length; i++)
        {
        fromdate = new Date(startdates[i] * 1000);
        fromdate.setDate(fromdate.getDate() - 1);
        beforedate.push($.datepicker.formatDate('mm/dd/yy', fromdate));
        todate = new Date(enddates[i] * 1000);
        todate.setDate(todate.getDate());
        afterdate.push($.datepicker.formatDate('mm/dd/yy', todate));
        for (var d = new Date((startdates[i] * 1000));
        d < new Date(enddates[i] * 1000);
        d.setDate(d.getDate() + 1)) {
        dateRange.push($.datepicker.formatDate('yy-mm-dd', d));
        }
        }//alert(beforedate);

        var sdate = new Date(startdate * 1000);
        var edate = new Date(enddate * 1000);
        var sedate = new Date(enddate * 1000);
        sedate = new Date(sedate.setDate(sedate.getDate() - 1));
        if (startdate == "") {
        sdate = "";
        }
        if (enddate == "") {
        edate = "";
        sedate = "";
        }
        if (sdate != "") {
        todaydate = new Date();
        if (sdate < todaydate) {
        minimumdate = new Date();
        endminimumdate = new Date();
        endminimumdate.setDate(endminimumdate.getDate() + 1);
        }
        else
        {
        minimumdate = new Date(sdate.setDate(sdate.getDate()));
        endminimumdate = new Date(sdate.setDate(sdate.getDate() + 1));
        }
        }
        else
        {
        minimumdate = new Date();
        endminimumdate = new Date();
        endminimumdate.setDate(endminimumdate.getDate() + 1);
        }
        var array1 = "<?php echo isset($forbiddenCheckIn) ? $forbiddenCheckIn : ''; ?>";
        // var array12 = <?php //var_dump($forbiddenCheckIn);?>

        <?php if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){ ?>
            console.log("Array1 : " + array1);

            // console.log("Array1 : " + array1);            
        <?php } ?>
        var array2 = "<?php echo isset($forbiddenCheckOut) ? $forbiddenCheckOut : ''; ?>";
        <?php if($_SERVER['REMOTE_ADDR'] == '93.36.45.62' || $_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){ ?>
            console.log("\n Array2 : " + array2);
            // console.log("Array1 : " + array1);            
        <?php } ?>
		console.log(disableDates);  
<?php if (isset($propertyObj->calendar_availability) && $propertyObj->calendar_availability == 1) { ?>
            $(".startdate").datepicker({
            changeMonth: true,
                    numberOfMonths: 1,
                    beforeShowDay: disableDates,
                    minDate:minimumdate,
                    maxDate:sedate,
                    beforeShowDay: function(date){
                    var string = $.datepicker.formatDate('yy-mm-dd', date);
                    var check = array1.indexOf(string) == - 1;
                    // console.log("Check : " + array1 + " String : " + string + "Date :" + date);
                    if (typeof (check) != 'undefined')
                    {
                    if (check)
                    {
                    return [true, '', ''];
                    }
                    else
                    {
                    return [false, "bookedDate", "check"];
                    }
                    }
                    },
                    onSelect: function (selectedDate) {
                    $("#requestbtn").attr("disabled", false);
                    var orginalDate = new Date(selectedDate);
                    orginalDate.setDate(orginalDate.getDate() + 1);
                    //var monthsAddedDate = new Date(new Date(orginalDate).setMonth(orginalDate.getMonth() + 3));
                    $(".enddate").datepicker("option", 'minDate', orginalDate);
                    $(".enddate").datepicker("option", 'maxDate', edate);
                    stdates = $(".startdate").val();
                    stdate = new Date(stdates);
                    eddates = $(".enddate").val();
                    eddate = new Date(eddates);
                    var seldateRange = [];
                    if (stdates != "" && eddates != "") {
                    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    selstartdate = stdate.getTime();
                    selenddate = eddate.getTime();
                    for (var dates = new Date(selstartdate);
                    dates < new Date(selenddate);
                    dates.setDate(dates.getDate() + 1)) {
                    seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                    }
                    var scnt = 0;
                    for (a = 0; a < seldateRange.length; a++)
                    {
                    if ($.inArray(seldateRange[a], dateRange) >= 0) {
                    scnt++;
                    }
                    }
                    if (scnt > 0) {

                    $("#requestbtn").attr("disabled", true);
                    $("#maxstayerr").html("Can not book those days");
                    }
                    else
                    {
                    scnt = 0;
                    $("#requestbtn").attr("disabled", true);
                    $("#maxstayerr").html("");
                    $.ajax({
                    url: baseurl + '/showpricedetail', // point to server-side PHP script 
                            type: "POST",
                            async: false,
                            data: {
                            listid : listid,
                                    noofdays : diffDays
                            },
                            success: function (res) {
                            $("#pricediv").show();
                            $("#requestbtn").attr("disabled", false);
                            }
                    });
                    }
                    }
                    },
                    onClose: function (selectedDate) {
                      //$(".enddate").datepicker('show');
                    }
            });
            $(".enddate").datepicker({
            beforeShowDay: disableDates,
                    minDate:endminimumdate,
                    maxDate:edate,
                    changeMonth: true,
                    numberOfMonths: 1,
                    beforeShowDay: function(date){
                        <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){ ?>
                    var string = $.datepicker.formatDate('yy-mm-dd', date);
                    string = string + " 11:00:00";
                    // console.log("\n selected Dates :" + string + "after date convert :" + date);
                    // console.log("\n Date Hours : " + date.getHours()+": Date Minutes "+date.getMinutes() + "\n ");

                <?php }else{ ?>
                    var string = $.datepicker.formatDate('yy-mm-dd', date);
                <?php }?>
                    var check = array2.indexOf(string) == - 1;
                    if (typeof (check) != 'undefined')
                    {
                    if (check)
                    {
                    return [true, '', ''];
                    }
                    else
                    {
                    return [false, "bookedDate", "check"];
                    }
                    }
                    },
                    onSelect: function (selectedDate) {
                    $("#requestbtn").attr("disabled", false);
                    var orginalDate = new Date(selectedDate);
                    orginalDate.setDate(orginalDate.getDate() - 1);
                    //var monthsAddedDate = new Date(new Date(orginalDate).setMonth(orginalDate.getMonth() - 3));
                    //$(".startdate").datepicker("option", 'maxDate', orginalDate);
                    stdates = $(".startdate").val();
                    stdate = new Date(stdates);
                    eddates = $(".enddate").val();
                    eddate = new Date(eddates);
                    var seldateRange = [];
                    if (stdates != "" && eddates != "") {
                    timeDiff = Math.abs(eddate.getTime() - stdate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    selstartdate = stdate.getTime();
                    selenddate = eddate.getTime();
                    for (var dates = new Date(selstartdate);
                    dates < new Date(selenddate);
                    dates.setDate(dates.getDate() + 1)) {
                        <?php if($_SERVER['REMOTE_ADDR'] == '93.32.72.112' || $_SERVER['REMOTE_ADDR'] == '93.36.86.74'){ ?>
                            var formatDate = $.datepicker.formatDate('yy-mm-dd', dates) + " 11:00:00";
                            // formatDate += formatDate+" 11:00:00";
                            // console.log("Formated Dates : " + $.datepicker.formatDate('yy-mm-dd', dates) + " >>>> " + formatDate);
                    seldateRange.push(formatDate);
                    // seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                <?php }else{ ?>
                    seldateRange.push($.datepicker.formatDate('yy-mm-dd', dates));
                    <?php } ?>
                    }
                    var ecnt = 0;
                     var isCheckInDateBooked = [];
                    var isCheckOutDateBooked = [];
                    console.log("Before Loop Seldate Range : " + seldateRange);
                    for (b = 0; b < seldateRange.length; b++)
                    {
                       console.log("\n Date Range : " + seldateRange);
                        console.log("\n Single Date : " + seldateRange[b]);
                        console.log("\n Type of A1 > " + typeof array1);
                        console.log("\n Type of A2 > " + typeof array2);
                        // console.log("\n storedDate  : " + array2);
                        // console.log("\n storedDate  : " + array2);
                        console.log("\n SelecDate Rage Array1  : " + array1.indexOf(seldateRange[b]));
                        console.log("\n  SelecDate Rage Array2 : " + array2.indexOf(seldateRange[b]));
                        // array1.indexOf(string) == - 1
                        // console.log("\n Is available for Array 1 > " + $.inArray(seldateRange[b], array1));
                        // console.log("\n Is available for Array 2 > " + $.inArray(seldateRange[b], array2));
                        console.log( "\n " + seldateRange[b]);
                        // if($.inArray(seldateRange[b], array1) >= 0){
                            var check1 = array1.indexOf(seldateRange[b]) != -1;
                            var check2 = array2.indexOf(seldateRange[b]) != -1;
                        if(check1 > 0){
                            isCheckInDateBooked.push(seldateRange[b]);
                        }
                        // if($.inArray(seldateRange[b], array2) >= 0){
                        if(check2 > 0){
                            isCheckOutDateBooked.push(seldateRange[b]);
                        }
                        <?php //if($_SERVER['REMOTE_ADDR']){ ?>

                        console.log("\n CheckIn Date : " + isCheckInDateBooked);
                        console.log("\n Checkout Date : " + isCheckOutDateBooked);
                        console.log("\n SelectDateRange : " + seldateRange[b]);

                        <?php //} ?>

                    if ($.inArray(seldateRange[b], seldateRange) >= 0) {
                    ecnt++;
                    }
                    }
                    // if (ecnt > 0) {
                   if((isCheckInDateBooked.length > 0 || isCheckOutDateBooked.length > 0) && ecnt > 0){

                    $("#requestbtn").attr("disabled", true);
                    $("#pricediv").show();
                    $("#maxstayerr").html("Can not book those days");
                    }
                    else
                    {
                    ecnt = 0;
                    $("#requestbtn").attr("disabled", true);
                    $("#maxstayerr").html("");
                    $.ajax({
                    url: baseurl + '/showpricedetail', // point to server-side PHP script 
                            type: "POST",
                            async: false,
                            data: {
                            listid : listid,
                                    noofdays : diffDays
                            },
                            success: function (res) {
                            $("#pricediv").show();
                            $("#requestbtn").attr("disabled", false);
                            }
                    });
                    }
                    }
                    }
            });
<?php } ?>


        });
        var disableDates = function(dt) {
        var dateString = jQuery.datepicker.formatDate('yy-mm-dd', dt);
        return [dateRange.indexOf(dateString) == - 1];
        }

        function focus_checkin() {
        var visible = $(".startdate").datepicker("widget").is(":visible");
        $(".startdate").datepicker(visible ? "hide" : "show");
        //$(".startdate").datepicker('show');
        }
    </script>

    <style>
        #map_canvas {
            width: 100%;
            height: 450px;
        }
        .owl-item
        {
            float: left;
        }
    </style>


    <script src="{{ asset('frontendassets/js/jRate.js')}}"></script> 
    <script>

        $(document).ready(function(){

            $(".expectedCostSection").hide();
            $("#expectedCost").on("click" , function(){
                if($(this).prop("checked") == true){
                    $(".expectedCostSection").show();
                }
                else if($(this).prop("checked") == false){
                    $(".expectedCostSection").hide();
                }
            });

        $("#jRatedetail").jRate({
        rating: <?php echo isset($rating) ? $rating : 0; ?>,
                readOnly: true
        });
        $("#school_house_nav a[href^='#']").on('click', function(e) {

        // prevent default anchor click behavior
        e.preventDefault();
        // store hash
        var hash = this.hash;
        // animate
        $('html, body').animate({
        scrollTop: $(hash).offset().top
        }, 300, function(){

        // when done, add hash to url
        // (default click behaviour)
        window.location.hash = hash;
        });
        });
        });
    </script>   
    @stop