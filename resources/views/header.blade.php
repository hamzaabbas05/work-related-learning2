<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="UTF-8">
<meta name="description" content="rooms for rent,apartments for rents">
<meta name="keywords" content="Online website for booking room for rent">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<!-- <script src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyAz0HqeAgq5xNt9DSUSMPIVlmLK_uuAoDc"></script> -->
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyA0BQbSzrX8W4conO6nmF9-LzpH4R-y1jU"></script>

<script>
var baseurl="";
</script>
<style type="text/css">
.alert-success {
	position: absolute;
	right: -50%;
	width: 50%;
	transition: all 0.5s !important;
	overflow: hidden !important;
}

.flashcss {
	right: 0%;
}
</style>
<title>Signup</title>
<!--- common css-->
<link href="{{ asset('frontendassets/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/jquery-ui.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/pickmeup.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/style1.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/jslider.css')}}" rel="stylesheet">

 <script src="{{ asset('frontendassets/js/jquery.js')}}"></script>
<script src="{{ asset('frontendassets/js/bootstrap.js')}}"></script>
<script src="{{ asset('frontendassets/js/jquery.slider.bundle.js')}}"></script>
<script src="{{ asset('frontendassets/js/jquery.slider.js')}}"></script>
<script src="{{ asset('frontendassets/js/front.js')}}"></script></head>


 </head>
<body>
	
	<nav class="navbar navbar-default norm_nav ">
		<div class="navbar-header">
			<button aria-controls="navbar" aria-expanded="false"
				data-target="#navbar" data-toggle="collapse"
				class="airfcfx-mobile-navbar navbar-toggle collapsed" type="button">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a href="index.html" class="navbar-brand">  <img
				src="assets/logo/logo.png"
				alt="airfinch" class="img-responsive margin_top10" />
			</a>
						<div class="pos_rel pull-left search_sec">
				<input id="where-to-go-main" type="text" class="form-control" placeholder="Search"> <i
					class="fa fa-search over_input"></i>
           			<input id="place-lat" type="hidden" value="">
           			<input id="place-lng" type="hidden" value="">				
			</div>
					</div>

		<div class="navbar-collapse collapse" id="navbar">

			<ul class="nav navbar-nav navbar-right">
				 	
			<!-- <li class="dropdown"><a href="" 
							class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-question-icon"></span><span class="airfcfx-menu">Help</span></a>
						</li>
						<li class="dropdown"><a href="register.html" aria-expanded="false"
							aria-haspopup="true" role="button" data-toggle="modal"
							class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-signup-icon"></span><span class="airfcfx-menu">Sign Up</span></a>
						</li>
						<li class="dropdown"><a href="signin.html" aria-expanded="false"
							aria-haspopup="true" role="button" data-toggle="modal"
							class="airfcfx-menu-link dropdown-toggle pos_rel"> <span class="airfcfx-menu-login-icon"></span><span class="airfcfx-menu">Login</span></a>
						</li>
						
					
				</li>-->
				<li class="dropdowns"><a href="#" aria-expanded="false"
					aria-haspopup="true" role="button" data-toggle="dropdown"
					class="airfcfx-menu-link dropdown-toggle pos_rel"><span class="airfcfx-menu-host-icon"></span>
					<span class="airfcfx-menu">Host</span><!--span
						class="pos_abs notifi_up">!</span><i class="fa fa-home text_gray "></i-->
				</a>
					<!--ul class="dropdown-menu">
						<li class="margin_top10 margin_bottom10"><a href="/user/listing/mylistings">Manage Listings</a></li>
						<a href="/user/listing/mylistings" class="rm_text_deco"><li
							class="margin_top10 margin_bottom10">Your Listings</li> </a>					
						<div class="border_bottom"></div>
					</ul-->
					<ul class="dropdown-menu padding20 profil_menu">
						<a href="/user/listing/mylistings" class="rm_text_deco"><li
							class="margin_top10 margin_bottom10">Your Listings</li> </a>					
						<div class="border_bottom"></div>

						<a href="/user/listing/reservations" class="rm_text_deco"><li
							class="margin_top10 margin_bottom10">Your Reservations</li> </a>
						<div class="border_bottom"></div>
						<a href="/user/listing/listcreate" class="rm_text_deco"><li
							class="margin_top10 margin_bottom10">List Your Space</li> </a>
						<div class="border_bottom"></div>
						<a href="/user/listing/completedtransaction" class="rm_text_deco">
							<li class="margin_top10 margin_bottom10">Transaction History</li>
						</a>
					</ul>					
				</li>
				<li class="dropdowns "><a href="/user/messages/inbox" aria-expanded="false"
					aria-haspopup="true" role="button" data-toggle="dropdown"
					class="airfcfx-menu-link dropdown-toggle pos_rel"><span class="airfcfx-menu-message-icon"></span><span class="airfcfx-menu">Messages</span>  
				</a>

					<ul class="dropdown-menu padding20 profil_menu">
						<a href="/user/messages/inbox" class="rm_text_deco"><li
							class="margin_top10 margin_bottom10">Messages</li> </a>					
						<div class="border_bottom"></div>
						<a href="/user/listing/usernotifications" class="rm_text_deco"><li
							class="margin_top10 margin_bottom10">Notification</li> </a>
						<div class="border_bottom"></div>
						<a href="/user/listing/notifications" class="rm_text_deco">
							<li class="margin_top10 margin_bottom10">View Dashboard</li>
						</a>
					</ul>					
				</li>
				<li class="dropdown"><a href="/user/help/index"  class="dropdown-toggle airfcfx-menu-link"><span class="airfcfx-menu-question-icon"></span><span class="airfcfx-menu">Help</span>
				</a>
					</li>
								<li class="dropdowns">
				<a href="/dashboard"aria-expanded="false"
					aria-haspopup="true" role="button" data-toggle="dropdown"
					class="airfcfx-menu-link dropdown-toggle pos_rel margin_right padd_right_60"> <span class="airfcfx-profilename">Ln N</span><span class="profile_pict pos_abs" style="background-image:url(/resized.php?src=http://airfinchscript.com/albums/images/users/usrimg.jpg&w=40&h=40);"></span>				</a>
					<ul class="dropdown-menu padding20 profil_menu">
						<a href="/dashboard"
							class="rm_text_deco"><li class=" margin_bottom10">My Profile</li>
						</a>
						<div class="border_bottom"></div>
						<a href="/user/listing/trips"
							class="rm_text_deco"><li class="margin_top10 margin_bottom10">Your Trips</li>
						</a>
						<div class="border_bottom"></div>

						<a href="/user/listing/mywishlists" class="rm_text_deco"><li
							class="margin_top10 margin_bottom10">Wish List</li> </a>
						<div class="border_bottom"></div>
						<a href="/editprofile" class="rm_text_deco"><li
							class="margin_top10 margin_bottom10">Edit Profile</li> </a>
						 
						<div class="border_bottom"></div>
						<a href="/user/listing/notifications" class="rm_text_deco">
							<li class="margin_top10 margin_bottom10">Account Settings</li>
						</a>
						<div class="border_bottom"></div>
						<a href="/logout" class="rm_text_deco"><li
							class="margin_top10">Logout</li> </a>
					</ul>
				</li>
				
			</ul>
		</div>
		<!--/.nav-collapse -->

	</nav>
	
	<!--search drop down calendar-->
	
	<div class="container hiddencls" id="searchcalendardiv">
	<div class="col-lg-12 airfcfx-dd-calendar-cnt">
	<div class="airfcfx-dd-calendar border1  bg_white caltxt">
	
	<div class="table1 margin_top10 margin_left5 margin_bottom10 padd_5_10 checkinalgn">

	<div class="checkindivcls">
	<label>Check In</label>
	<input id="check-in-main" type="text" class="form-control" placeholder="DD-MM-YYYY" value="">
	</div>
	<div class="checkindivcls">
	<label>Check Out</label>
	<input id="check-out-main" type="text" class="form-control" placeholder="DD-MM-YYYY" value="">
	</div>
	<div class="guestdivcls">
	<label>Guests</label>
	<select class="form-control margin10" id="guest-count"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option><option value="61">61</option><option value="62">62</option><option value="63">63</option><option value="64">64</option><option value="65">65</option><option value="66">66</option><option value="67">67</option><option value="68">68</option><option value="69">69</option><option value="70">70</option><option value="71">71</option><option value="72">72</option><option value="73">73</option><option value="74">74</option><option value="75">75</option><option value="76">76</option><option value="77">77</option><option value="78">78</option><option value="79">79</option><option value="80">80</option><option value="81">81</option><option value="82">82</option><option value="83">83</option><option value="84">84</option><option value="85">85</option><option value="86">86</option><option value="87">87</option><option value="88">88</option><option value="89">89</option><option value="90">90</option><option value="91">91</option><option value="92">92</option><option value="93">93</option><option value="94">94</option><option value="95">95</option><option value="96">96</option><option value="97">97</option><option value="98">98</option><option value="99">99</option></select>
	</div>
	</div><!--table1 end--><div class="airfcfx-dd-calendar-rt-txt col-lg-12">Room Type</div><div class="airfcfx-dd-cal-RT"><div class="pull-left">
	<input id="roomtype1" class="airfcfx-search-drop-checkbox" type="checkbox" name="roomtype" value="1">
	<div class="airfcfx-search-dd-checkbox-text">vila</div></div></div>
	<div class="airfcfx-dd-cal-RT"><div class="pull-left">
	<input id="roomtype2" class="airfcfx-search-drop-checkbox" type="checkbox" name="roomtype" value="2">
	<div class="airfcfx-search-dd-checkbox-text">Apartment</div></div></div>
	<div class="airfcfx-dd-cal-RT"><div class="pull-left">
	<input id="roomtype3" class="airfcfx-search-drop-checkbox" type="checkbox" name="roomtype" value="3">
	<div class="airfcfx-search-dd-checkbox-text">Bangalow</div></div></div><div class="airfcfx-search-dd-findplace-btn"><button class="airfcfx-findplace-btn airfcfx-panel btn btn_email" onclick="searchlistmains()">Find a place</button></div>
	</div><!--border1 end-->
	</div></div>