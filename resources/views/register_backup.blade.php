@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')
@stop
{{-- content --}}
@section('content')
<div class="pos_rel bg_gray1">
    <div class="container">
        @include('layouts.errorsuccess')
        <div class="modal-dialog login_width" role="document">

            <div class="modal-content">
                <div class="modal-body text-center">
                    <p>Signup with </p><br />
                    <!--<a href="#" class="text-danger" style="display: none;">
                        <div id="w0" class="auth-clients">
                            <ul class="auth-clients clear">
                                <li class="auth-client"><a class="auth-link facebook" href="{{URL::to('social/login/redirect/facebook')}}" data-popup-width="800" data-popup-height="500"><span class="auth-icon facebook"></span></a></li>
                                <li class="auth-client"><a class="auth-link google" href="{{URL::to('social/login/redirect/google')}}"><span class="auth-icon google"></span></a></li>
                            </ul>
                        </div>
                    </a>
                    <div class="login_or border_bottom margin_top10"><span>or</span></div>
                    -->
                    <form id="form-signup" action="<?php echo url('user_register') ?>" method="post" role="form">
                        <div class="form-group field-signupform-firstname required">

                            <input type="text" id="firstname" class="form-control margin_top20 margin_bottom10" value="{{old('firstname')}}" name="firstname" maxlength="40" placeholder="First Name(s)" onkeypress="return isAlpha(event)">

                            <p class="help-block help-block-error"></p>
                        </div>                
                        <div class="form-group field-signupform-lastname required">

                            <input type="text" id="lastname" class="form-control margin_bottom10" value="{{old('lastname')}}" name="lastname" maxlength="40" placeholder="Family Name(s)/ Surname(s)" onkeypress="return isAlpha(event)">

                            <p class="help-block help-block-error"></p>
                        </div>
                        <div class="form-group field-signupform-email required">

                            <input type="text" id="email" class="form-control margin_bottom10" value="{{old('email')}}" name="email" placeholder="Email Address" autocomplete="off">

                            <p class="help-block help-block-error"></p>
                        </div>                <!--?= $form->errorSummary($model); ?-->
                        <div class="form-group field-signupform-password required">
<!--title="Yes, make one up now. We suggest to save it in your browser / device."-->
                            <input type="password" id="password" class="form-control margin_bottom10" name="password" placeholder="Make up a Password" autocomplete="off">

                            <p class="help-block help-block-error"></p>
                        </div>                 
                        <div class="bdaycls"> <label>Date of Birth </label><br />

                            <select name="bmonth" class="bdayselcls" id="bmonth">
                                <option value="">Month</option>
                                <option value="1" @if(old('bmonth') == "1") selected="" @endif>January</option>
                                <option value="2" @if(old('bmonth') == "2") selected="" @endif>February</option>
                                <option value="3" @if(old('bmonth') == "3") selected="" @endif>March</option>
                                <option value="4" @if(old('bmonth') == "4") selected="" @endif>April</option>
                                <option value="5" @if(old('bmonth') == "5") selected="" @endif>May</option>
                                <option value="6" @if(old('bmonth') == "6") selected="" @endif>June</option>
                                <option value="7" @if(old('bmonth') == "7") selected="" @endif>July</option>
                                <option value="8" @if(old('bmonth') == "8") selected="" @endif>August</option>
                                <option value="9" @if(old('bmonth') == "9") selected="" @endif>September</option>
                                <option value="10" @if(old('bmonth') == "10") selected="" @endif>October</option>
                                <option value="11" @if(old('bmonth') == "11") selected="" @endif>November</option>
                                <option value="12" @if(old('bmonth') == "12") selected="" @endif>December</option>
                            </select>
                            <select name="bday" class="bdayselcls" id="bday">
                                <option value="0">Day</option>
                                <?php for ($i = 1; $i <= 31; $i++) { ?>
                                    <option value="{{$i}}" @if(old('bday') == $i) selected="" @endif>{{$i}}</option>
                                <?php } ?>
                            </select>
                            <select name="byear" class="bdayselcls" id="byear">
                                <option value="">Year</option>
                                <?php
                                $currentYear = date("Y");
                                $startYear = $currentYear - 15;
                                $baseYear = 1900;
                                for ($i = $startYear; $i >= $baseYear; $i--) {
                                    ?>
                                    <option value="{{$i}}" @if(old('byear') == $i) selected="" @endif>{{$i}}</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="bdaycls" style="margin-top: 1px; @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") display:none; @endif" id="showVoucher">
                            <input type="text" id="voucher_code" class="form-control margin_bottom10 @if(!empty(old('voucher_code'))) error_class @endif" value="{{old('voucher_code')}}" name="voucher_code" placeholder="Voucher Code. If you don't have one, click here under." autocomplete="off" />
                            <p class="help-block help-block-error"></p>
                        </div>
                        <div class="margin_top10 text-left font_size12 margin_bottom10">
                            <input type="checkbox" value="1" id="no_voucher_code" name="no_voucher_code" @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") checked @endif/>
                                   <p style="margin-left:20px;">I have no Voucher Code</p>
                        </div>
                        <div class="bdaycls" style="margin-top: 1px; @if(!empty(old('no_voucher_code')) && old('no_voucher_code') == "1") display:block; @else display:none; @endif" id="showNoVoucher">
                            <label>Select Interest in</label><br />
                            <select name="voucher_interest" class="" style="width: 100%;height: 35px;;padding: 6px;">
                            <option value="">Select</option>
                            <option value="NewSchoolHouse" title="Offering paid services to Student Guests" @if(old('voucher_interest') == "NewSchoolHouse") selected="" @endif>New School House</option>
                            <option value="NewWorkTutor" title="Offering unpaid Work Experience to Secondary School Students" @if(old('voucher_interest') == "NewWorkTutor") selected="" @endif>New Organiation / Work Tutor</option>
                            <option value="NewParentInterested" title="Requesting paid services and unpaid Work Experience" @if(old('voucher_interest') == "NewParentInterested") selected="" @endif>New user as Parent / Student Guest</option>
                        </select>
                            <p class="help-block help-block-error"></p>
                        </div>
                        
                        <div class="bdaycls" style="margin-top: 20px;"> 
                            {!! Recaptcha::render() !!}
                        </div>
                        <div class="has-error"><p id="bdayerr" class="help-block help-block-error"></p></div>
                        <div class="margin_top10 text-left font_size12 margin_bottom10">
                            <input id="agree" type="checkbox">
                            <p style="margin-left:20px;">By signing up, I agree to 
                                <a target="_blank" href="{{url('cms/terms')}}" class="text-danger">Terms and Conditions.</a> 
                            </p>
                        </div>
                        <div class="has-error"><p id="agreeerr" class="help-block help-block-error"></p></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn_email margin_top10 width100" name="signup-button">Sign up</button>                </div>
                        <img alt="loading" id="signuploadimg" src="{{ asset('frontendassets/images/load.gif')}}" class="loading" style="margin-top:-1px;">            </form>  
                </div>  

            </div>
        </div>
    </div> <!-- container end -->
</div> <!-- list_bg end -->

<style type="text/css">
    .error_class {
        border: 1px solid red;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $("#no_voucher_code").click(function(){
            if($(this).is(":checked")) {
                $("#showVoucher").hide();
                $("#showNoVoucher").show();
            } else {
                $("#showVoucher").show();
                $("#showNoVoucher").hide();
            }
        });
    });
</script>
@stop