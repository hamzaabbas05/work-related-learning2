@if(Session::has('message'))
<div class="alert alert-success" style="z-index: 100;">
    {{Session::get('message')}}
    <a style="float: right; border: 1px solid; border-radius: 2px; padding: 0px 4px; text-decoration: none; cursor: pointer;" onclick="closeErrorMsg(this)">X</a>
</div>
@elseif($errors->count()>0)
<div class="alert alert-danger" style="z-index: 100;">
    @foreach($errors->all() as $error)
    {{ $error }}
    <br/>
    @endforeach
</div>
@elseif(Session::has('error'))
<div class="alert alert-danger" style="z-index: 100;">
    {{Session::get('error')}}
    <a style="float: right; border: 1px solid; border-radius: 2px; padding: 0px 4px; text-decoration: none; cursor: pointer;" onclick="closeErrorMsg(this)">X</a>
</div>
@endif					
<script type="text/javascript">
function closeErrorMsg(obj) {
    $(obj).parent().remove();
}
</script>