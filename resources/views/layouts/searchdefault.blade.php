<!DOCTYPE html>
<html lang="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>@inject('settings', 'App\Settings')
<?php 
$settingsObj = $settings->settingsInfo();
?><title>{{$settingsObj->site_title}}</title>
    <meta charset="UTF-8">
<meta name="description" content="{{$settingsObj->site_meta_description}}">
<meta name="keywords" content="{{$settingsObj->site_meta_keywords}}">    
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	    <!--<script src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyAz0HqeAgq5xNt9DSUSMPIVlmLK_uuAoDc"></script>-->
<!--            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAz0HqeAgq5xNt9DSUSMPIVlmLK_uuAoDc&callback=initMap" async defer></script>-->
  
<script>
var baseurl="";
</script> 
<style type="text/css">
.alert-success{
position:absolute;
right:-50%;
width:50%;
transition:all 1s !important;
overflow:hidden !important;
}
.flashcss
{
	right:0%;
}
</style>   
  <title>Holidaysiles</title>
<link href="{{asset('frontendassets/css/style.css')}}" rel="stylesheet">
<link href="{{asset('frontendassets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('frontendassets/css/jquery-ui.min.css')}}" rel="stylesheet">
<link href="{{asset('frontendassets/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('frontendassets/css/pickmeup.css')}}" rel="stylesheet">
<link href="{{asset('frontendassets/css/style1.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/jslider.css')}}" rel="stylesheet">
<link href="{{ asset('frontendassets/css/site.css')}}" rel="stylesheet">
<script src="{{asset('frontendassets/js/jquery.js')}}"></script>
<script src="{{asset('frontendassets/js/bootstrap.js')}}"></script>
<script src="{{ asset('frontendassets/js/jquery.slider.bundle.js')}}"></script>
<script src="{{ asset('frontendassets/js/jquery.slider.js')}}"></script>
<script src="{{asset('frontendassets/js/front.js')}}"></script></head>


</head>
<!--page level css-->
@yield('header_js')
<!--end of page level css-->
<body>

@yield('nav_bar')
<!-- Content -->
@yield('content')
 
 
