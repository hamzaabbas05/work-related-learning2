<!DOCTYPE html>
<html lang="">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        @inject('settings', 'App\Settings')
        @inject('language', 'App\Language')
        @inject('currecy', 'App\Currency')
        <?php
        $settingsObj = $settings->settingsInfo();
        $languages = $language->getLanguage(true);
        $currencies = $currecy->getCurrency(true);
        $fav = $settingsObj->favicon;
        ?>
        <title>{{$settingsObj->site_title}}</title>
        <meta charset="UTF-8">
        <meta name="title" content="{{$settingsObj->site_meta_title}}">
        <meta name="description" content="{{$settingsObj->site_meta_description}}">
        <meta name="keywords" content="{{$settingsObj->site_meta_keywords}}">    
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="icon" href="<?php echo url('images/favicon/' . $fav) ?>" />

        <!-- <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyAz0HqeAgq5xNt9DSUSMPIVlmLK_uuAoDc"></script> -->
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyA0BQbSzrX8W4conO6nmF9-LzpH4R-y1jU"></script>

        <script>
            var baseurl = '<?php echo url(); ?>';
        </script> 
        <style type="text/css">
            .gray {
                background-color: #4d4d4d !important;
            }
            .red {
                background-color: #fe5771 !important;
            }

            .alert-success{
                position:absolute;
                right:-50%;
                width:50%;
                transition:all 1s !important;
                overflow:hidden !important;
            }
            .flashcss
            {
                right:0%;
            }
        </style>   
        <title>Holidaysiles</title>  
        <!--- common css-->
        <link href="<?php  echo asset('frontendassets/css/dropzone.css?v=1May'); ?>" rel="stylesheet">
        <link href="<?php  echo asset('frontendassets/css/style.css?v=jami1May'); ?>" rel="stylesheet">
        
        <link href="{{ asset('frontendassets/css/bootstrap.css')}}" rel="stylesheet">
        <link href="{{ asset('frontendassets/css/jquery-ui.min.css')}}" rel="stylesheet">
        <link href="{{ asset('frontendassets/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{ asset('frontendassets/css/pickmeup.css')}}" rel="stylesheet">
        <link href="{{ asset('frontendassets/css/style1.css')}}" rel="stylesheet">
        <!--page level css-->
        @yield('header_styles')
        <!--end of page level css-->
        <!-- common js -->
        <script src="{{ asset('frontendassets/js/jquery.js')}}"></script>
        <script src="{{ asset('frontendassets/js/bootstrap.js')}}"></script>
        <script src="<?php echo asset('frontendassets/js/front.js?v=jami.1May'); ?>"></script></head>

    <!--page level css-->
    @yield('header_js')
    <!--end of page level css-->
    <body>

        @yield('nav_bar')
        <!-- Content -->
        @include('partials.flashnotification')

        @yield('content')
        <!-- Content-->

        <!-- Footer Section-->
        <input type="hidden" name="my_ip" id="my_ip" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
        <div class="footer">
            <div class="airfcfx-home-cnt-width container margin_bottom30">


                <!-- col-sm-4 end -->

                <div class="col-xs-12 col-sm-3">

                    <div class="col-xs-12 col-sm-12 airfcfx-footer-dd-cnt no-hor-padding">
                        {!! Form::select('language_select', $languages, $settingsObj->language_id, ['class'=>'col-xs-12 col-sm-6 airfcfx-footer-select form-control margin10 no-hor-padding', 'name' => 'language_select', 'id' => 'language_select' ,'disabled' => 'true']) !!}


        <!--<select id="language_select" class="col-xs-12 col-sm-6 airfcfx-footer-select form-control margin10 no-hor-padding" onchange="change_language()">
            <option selected value="en">English</option>
            <option value="fr">French</option>
            <option value="zh">Chinese</option>           
          </select> -->
                    </div>

                    <div class="col-xs-12 col-sm-12 airfcfx-footer-dd-cnt no-hor-padding">
                        {!! Form::select('currency_select', $currencies, $settingsObj->currency_id, ['class'=>'col-xs-12 col-sm-6 airfcfx-footer-select form-control margin10 no-hor-padding', 'name' => 'currency_select', 'id' => 'currency_select', 'disabled' => 'true']) !!}

        <!-- <select id="currency_select" class="col-xs-12 col-sm-6 airfcfx-footer-select form-control margin10 no-hor-padding" onchange="change_currency()"><option value="2">PLN</option><option value="3">SGD</option><option value="4">EUR</option><option value="5">USD</option><option selected value="6">RUB</option><option value="7">CZK</option><option value="11">GBP</option><option selected value="12">RUB</option><option value="13">TRY</option><option value="14">CZK</option><option value="15">BRL</option><option value="16">HKD</option><option value="17">DKK</option></select>       -->


                    </div>    


                </div>


                <div class="airfcfx-footer-info col-xs-12 col-sm-3">
                    <h4 class="text_white">Quick Links</h4>

                    <ul class="airfcfx-footer-ul footer_menu list-unstyled" style="width:100%">
                        <li><a href="{{URL::to('aboutus')}}">About us</a></li>
                        <li><a href="{{URL::to('cms/terms')}}">Terms and condition</a></li>
                        <li><a href="{{URL::to('cms/Netiquette')}}">Netiquette</a></li>
                        <li><a href="{{URL::to('cms/privacypolicy')}}">Privacy Policy</a></li>
                        <li><a target="_blank" href="{{URL::asset('assets/le/LEitaly.pdf')}}">Learning Agreement Italian</a></li>

                        <li><a target="_blank" href="{{URL::asset('assets/le/leenglish.pdf')}}">Learning Agreement English</a></li>


                </div>
                <!-- col-sm-4 end -->

                <div class="airfcfx-footer-info col-xs-12 col-sm-3">
                    <h4 class="text_white">Discover</h4>

                    <ul class="airfcfx-footer-ul footer_menu list-unstyled" style="width:100%"> 
                        <li><a href="{{URL::to('faq/work')}}">FAQ</a></li>
                        <li><a href="{{URL::to('cms/howitworks')}}">How it Works?</a></li>
                        <li><a href="{{URL::to('cms/howdoistart')}}">How Do I Start?</a></li>
                        <li><a href="{{URL::to('cms/presentation')}}">Presentations</a></li>
                        <li><a href="{{URL::to('cms/responsibility')}}">Responsibilities</a></li>


                </div>
                <div class="col-xs-12 col-sm-3">
                    <h4 class="text_white">Need help?</h4>
                    <ul class="footer_menu list-unstyled">
                        <li>Phone: {{$settingsObj->contact_phone}}</li>
                        <li>Contact Email: {{$settingsObj->contact_email}}</li>

                        <li>Skype: {{$settingsObj->contact_skype}}</li>
                        <li>Address: {{$settingsObj->contact_address}}</li>
                    </ul>
                </div>
                <!-- col-sm-4 end -->
            </div>
            <!-- container end -->

            <div class="airfcfx-home-cnt-width container margin_bottom20">
                <div class="airfcfx-footer-border border_bottom1 margin_top20 margin_bottom30"></div>
                <div class="text-center">
                    <div class="airfcfx-joinus-txt margin_bottom20">Join Us On</div>
                    <a target="_blank" class="airfcfx-socialicon-padding" href="{{$settingsObj->facebook_url}}"><i class="fa fa-facebook social_icon text_white"></i></a> 
                    <a target="_blank" class="airfcfx-socialicon-padding" href="{{$settingsObj->twitter_url}}"><i class="fa fa-twitter social_icon text_white"></i></a> 
                    <a target="_blank" class="airfcfx-socialicon-padding" href="{{$settingsObj->googleplus_url}}"><i class="fa fa-google-plus social_icon text_white"></i></a> 

                    <p class="airfcfx-copyright margin_top10">Copyright &copy; www.work-related-learning.com 2017</p>      </div>
            </div>
            <!-- container end -->

        </div>
        <!-- footer end -->


    </body>

</html>
<script type="text/javascript" src="{{asset('frontendassets/js/fileinput.js')}}"></script>
<script
type="text/javascript" src="{{asset('frontendassets/js/owl.carousel.js')}}"></script>

<script
type="text/javascript" src="{{asset('frontendassets/js/jquery-ui.min.js')}}"></script>
<script>
    // google.maps.event.addDomListener(window, 'load', initialize);

    $(document).ready(function () {

        $(".normal_datepicker").datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true
        });
        $("#check-in-main").datepicker({
            dateFormat: "dd-mm-yy",
            minDate: 0,
            onSelect: function (date) {
                var date2 = $('#check-in-main').datepicker('getDate');
                date2.setDate(date2.getDate() + 1);
                $('#check-out-main').datepicker('setDate', date2);
                //sets minDate to dt1 date + 1
                $('#check-out-main').datepicker('option', 'minDate', date2);
            }
        });
        $('#check-out-main').datepicker({
            dateFormat: "dd-mm-yy",
            onClose: function () {
                var dt1 = $('#check-in-main').datepicker('getDate');
                console.log(dt1);
                var dt2 = $('#check-out-main').datepicker('getDate');
                if (dt2 <= dt1) {
                    var minDate = $('#check-out-main').datepicker('option', 'minDate');
                    $('#check-out-main').datepicker('setDate', minDate);
                }
            }
        });

        $("#check-in-mobile").datepicker({
            dateFormat: "dd-mm-yy",
            minDate: 0,
            onSelect: function (date) {
                var date2 = $('#check-in-mobile').datepicker('getDate');
                date2.setDate(date2.getDate() + 1);
                $('#check-out-mobile').datepicker('setDate', date2);
                //sets minDate to dt1 date + 1
                $('#check-out-mobile').datepicker('option', 'minDate', date2);
            }
        });
        $('#check-out-mobile').datepicker({
            dateFormat: "dd-mm-yy",
            onClose: function () {
                var dt1 = $('#check-in-mobile').datepicker('getDate');
                console.log(dt1);
                var dt2 = $('#check-in-mobile').datepicker('getDate');
                if (dt2 <= dt1) {
                    var minDate = $('#check-out-mobile').datepicker('option', 'minDate');
                    $('#check-out-mobile').datepicker('setDate', minDate);
                }
            }
        });





        var docHeight = $(window).height();
        var footerHeight = $('.footer').height();
        var footerTop = $('.footer').position().top + footerHeight;

        if (footerTop < docHeight) {
            $('.footer').css('margin-top', 10 + (docHeight - footerTop) + 'px');
        }

        $("body").css("overflow-x", "hidden");
        $(".alert-success").addClass("flashcss");

        $("#closebutton").click(function () {
            $(".alert-success").removeClass("flashcss");
        });


    }); // Document Ready

    function initMapmain() {
        autocomplete = new google.maps.places.Autocomplete((document
                .getElementById('where-to-go-main')), {
            types: ['geocode']
        });
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            fillInAddress();
        });
    }

    function initMapmobile() {
        autocomplete = new google.maps.places.Autocomplete((document
                .getElementById('where-to-go-mobile')), {
            types: ['geocode']
        });
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            fillInAddress();
        });
    }
    @if(!Request::is('user/edit'))
    google.maps.event.addDomListener(window, 'load', initMapmain);
    google.maps.event.addDomListener(window, 'load', initMapmobile);
    @endif
</script>
