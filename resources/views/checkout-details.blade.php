@extends('layouts.frontdefault')
@section('nav_bar')
@include('partials.navbar')

<div class="bg_gray1">
    <div class="container">  
		<div class="row">
			<div class="col-md-6 col-md-offset-3 margin_top30">
				<div class="panel panel-default credit-card-box">
					<div class="panel-heading display-table" style="background: #fe5771; color: #fff;" >
						<div class="row display-tr" >
							<h3 class="panel-title display-td" style="line-height: 45px; padding: 0px 20px;">Payment Details <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png"></h3>
						</div>                    
					</div>
					<div class="panel-body"> 
						<form role="form" action="{{ route('stripe.post') }}" method="post" class="require-validation"
														 data-cc-on-file="false"
														data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
														id="payment-form">
						<h4>Services</h4>
						<?php 
						$total_price = 0;
						$currency_symbol = '';
						$html = '';
						if(count($cartItems) > 0){
							$html .= "<div class='checkout_items'>";
							
							foreach ($cartItems as $key => $item) {
								$html .= "<div class='checkout_item'>";
								// $html .= "<td>".$item['property_id']."</td>";
								$fetchProperty = \App\Property::where('id',$item['property_id'])->first();
								$bed_guest_room_id = $fetchProperty->bed_guest_room_id;
								$bed_type_id = $fetchProperty->bed_type_id;
								$guest_room = \App\UserPaidGuestRoom::where('id',$bed_guest_room_id)->first();
								$room_type_id = $guest_room->room_type_id;
								$room_type = \App\RealRoomTypes::where("is_active", "1")->where("id", $room_type_id)->first();
								
								$bed_type = \App\BedType::where(["is_active" => "1", "id" => $bed_type_id])->first();
								
								
								$room_name = $room_type->room_type.' Room ';
								$bed_name = $bed_type->bed_type.' Bed';
								
								
								$currency_id = $item->pricing->currency_id;
								$currency    = \App\Currency::where(["id" => $currency_id])->first();
								if(isset($currency->currency_symbol) and $currency->currency_symbol != ""){
									$currency_symbol = $currency->currency_symbol;
								}else{
									$currency_symbol = $currency->currency_code;
								}
								
								if($item->pricing->payment_type == 'weekly'){
									
									$number = ($item->pricing->amount / 7);
									$per_day_price = number_format((float)$number, 2, '.', '');
								}else{
									$number = $item->pricing->amount;
									$per_day_price = number_format((float)$number, 2, '.', '');
								}
								
								
								$start_date = strtotime($item['start_date']);
								$end_date = strtotime($item['end_date']);
								$diff = $end_date - $start_date; 
								$number_of_days =  abs(round($diff / 86400)); 
								
								$total_price += $number_of_days*$per_day_price;
								$html .= "<span>".$room_name."(".$bed_name.")"."</span>";
								$html .= "<span>".date("d-m-Y",$start_date)."</span>";
								$html .= "<span>".date("d-m-Y",$end_date)."</span>";
								$html .= "<span>".$currency_symbol." ".$number_of_days*$per_day_price."</span>";
								$html .= "</div>";
								$html .= '<input type="hidden" name="service_ids[]" value="'.$item['id'].'" />';
							}
							$html .= "</div>";
							
						
						echo $html;

						
						?>
						@if (Session::has('success'))
							<div class="alert alert-success text-center">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
								<p>{{ Session::get('success') }}</p>
							</div>
						@endif
	  
						
							 
							<div class='form-row row'>
								<div class='col-xs-12 form-group required'>
									<label class='control-label'>Name on Card</label> <input
										class='form-control' size='4' type='text'>
								</div>
							</div>
	  
							<div class='form-row row'>
								<div class='col-xs-12 form-group card required'>
									<label class='control-label'>Card Number</label> <input
										autocomplete='off' class='form-control card-number' placeholder="XXXX-XXXX-XXXX-XXXX"
										type='text'>
								</div>
							</div>
	  
							<div class='form-row row'>
								<div class='col-xs-12 col-md-4 form-group cvc required'>
									<label class='control-label'>CVC</label> <input autocomplete='off'
										class='form-control card-cvc' placeholder='ex. 311' size='4'
										type='text'>
								</div>
								<div class='col-xs-12 col-md-4 form-group expiration required'>
									<label class='control-label'>Expiration Month</label> <input
										class='form-control card-expiry-month' placeholder='MM' size='2'
										type='text'>
								</div>
								<div class='col-xs-12 col-md-4 form-group expiration required'>
									<label class='control-label'>Expiration Year</label> <input
										class='form-control card-expiry-year' placeholder='YYYY' size='4'
										type='text'>
								</div>
							</div>
	  
							<div class='form-row row'>
								<div class='col-md-12 error form-group hide'>
									<div class='alert-danger alert'>Please correct the errors and try
										again.</div>
								</div>
							</div>
	  
							<div class="row">
								<div class="col-xs-12">
									<button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now ({{$currency_symbol.' '.$total_price}})</button>
								</div>
							</div>
							  
						</form>
						<?php  
						}else{
							echo '<p>Sorry youn did not added any item to basket please <a href="'.url().'">Check Services</a></p>';
						}
						?>
					</div>
				</div>        
			</div>
		</div> 
	</div>
</div>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$(function() {
    var $form         = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
 
        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });
  
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
  
});

String.prototype.toCardFormat = function() {
  return this.replace(/[^0-9]/g, "").substr(0, 16).split("").reduce(cardFormat, "");

  function cardFormat(str, l, i) {
    return str + ((!i || (i % 4)) ? "" : "-") + l;
  }
};

$(document).ready(function() {
  $(".card-number").keyup(function() {
    $(this).val($(this).val().toCardFormat());
  });
});

</script>@stop